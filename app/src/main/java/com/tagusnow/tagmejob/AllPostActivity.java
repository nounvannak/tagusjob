package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.fragment.EditWidgetQuickPostJobs;
import com.tagusnow.tagmejob.fragment.WidgetQuickPostJobs;
import com.tagusnow.tagmejob.fragment.WidgetQuickPostNormal;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class AllPostActivity extends TagUsJobActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = AllPostActivity.class.getSimpleName();
    public static final String JOBS = WidgetQuickPostJobs.class.getSimpleName();
    public static final String EDIT_JOBS = EditWidgetQuickPostJobs.class.getSimpleName();
    public static final String RESUME = WidgetQuickPostCV.class.getSimpleName();
    public static final String ARTICLE = WidgetQuickPostNormal.class.getSimpleName();
    public static final String FRAGMENT = "fragment";
    private static final int PUBLIC = 0;
    private static final int PRIVATE = 1;
    public static final String FEED = "feed";
    private SmUser user;
    private String fragment;
    private Toolbar toolbar;
    private Button btnPost;
    private RelativeLayout optionPrivacy;
    private ImageView optPrivacyIcon;
    private TextView optPrivacyTitle,toolbarTitle;
    private CircleImageView user_profile;
    private Spinner spnPrivacy;
    private String toolbar_title = "No Title";
    private List<String> privacyList = new ArrayList<String>(){{add("Public");add("Private");}};
    private Feed feed;
    private WidgetQuickPostNormal widgetQuickPostNormal;
    private WidgetQuickPostJobs widgetQuickPostJobs;
    private WidgetQuickPostCV widgetQuickPostCV;
    private EditWidgetQuickPostJobs editWidgetQuickPostJobs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_post);
        initTemp();
        setupFragment();
        initView();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,AllPostActivity.class);
    }

    private void initView(){
        initToolbar();
        initOption();
    }

    private void initOption(){
        btnPost = (Button)findViewById(R.id.btnPost);
        optionPrivacy = (RelativeLayout)findViewById(R.id.optionPrivacy);
        optPrivacyIcon = (ImageView) findViewById(R.id.optPrivacyIcon);
        optPrivacyTitle = (TextView)findViewById(R.id.optPrivacyTitle);
        spnPrivacy = (Spinner)findViewById(R.id.spnPrivacy);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,this.privacyList);
        spnPrivacy.setAdapter(adapter);
        spnPrivacy.setOnItemSelectedListener(this);
        btnPost.setOnClickListener(this);
        optionPrivacy.setOnClickListener(this);
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(this.toolbar_title);
        user_profile = (CircleImageView)findViewById(R.id.user_profile);
        toolbarTitle = (TextView)findViewById(R.id.toolbar_title);
        toolbarTitle.setText(this.toolbar_title);
        if (this.user!=null){
            if (this.user.getImage()!=null){
                if (this.user.getImage().contains("http")){
                    Glide.with(this)
                            .load(this.user.getImage())
                            .centerCrop()
                            .error(R.drawable.ic_person_outline_white_24dp)
                            .into(user_profile);
                }else {
                    Glide.with(this)
                            .load(SuperUtil.getProfilePicture(this.user.getImage()))
                            .centerCrop()
                            .error(R.drawable.ic_person_outline_white_24dp)
                            .into(user_profile);
                }
            }else {
                Glide.with(this)
                        .load(R.drawable.ic_person_outline_white_24dp)
                        .centerCrop()
                        .into(user_profile);
            }
        }
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.fragment = getIntent().getStringExtra(FRAGMENT);
        if (getIntent().getSerializableExtra(FEED)!=null){
            this.feed = (Feed) getIntent().getSerializableExtra(FEED);
        }
    }

    private void setupFragment(){
        if (this.fragment!=null){
            if (this.fragment.equals(JOBS)){
                this.toolbar_title = "Post a jobs";
                this.widgetQuickPostJobs = WidgetQuickPostJobs.newInstance().Activity(this);
                startFragment(this.widgetQuickPostJobs);
            }else if (this.fragment.equals(RESUME)) {
                this.toolbar_title = "Post a resume";
                this.widgetQuickPostCV = WidgetQuickPostCV.newInstance();
                startFragment(this.widgetQuickPostCV);
            }else if (this.fragment.equals(EDIT_JOBS)){
                this.toolbar_title = "Edit Job";
                this.editWidgetQuickPostJobs = EditWidgetQuickPostJobs.newInstance(this.feed).Activity(this);
                startFragment(this.editWidgetQuickPostJobs);
            }else {
                this.toolbar_title = "Post an article";
                startFragment(WidgetQuickPostNormal.newInstance());
            }
        }else {
            Toast.makeText(this,"No Fragment",Toast.LENGTH_SHORT).show();
        }
    }

    private void startFragment(Fragment fragment){
        try {
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.animator.fragment_slide_left_enter,R.animator.fragment_slide_left_exit).replace(R.id.main_frame,fragment).commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updatePrivacyUI(int position){
        if (position==PRIVATE){
            optPrivacyTitle.setText(this.privacyList.get(position));
            optPrivacyIcon.setImageResource(R.drawable.ic_lock_outline_light_blue_24dp);
        }else {
            optPrivacyTitle.setText(this.privacyList.get(position));
            optPrivacyIcon.setImageResource(R.drawable.ic_public_light_blue_24dp);
        }
    }

    private void onPost() throws IOException {
        if (this.fragment.equals(JOBS)) {
            if (widgetQuickPostJobs.isValid()) {
                showProgress();
                if (widgetQuickPostJobs.save().isPost()) {
                    if (!isFinishing()) {
                        dismissProgress();
                        onBackPressed();
                    }
                } else {
                    if (!isFinishing()) {
                        dismissProgress();
                    }
                }
            }
        }else if (this.fragment.equals(EDIT_JOBS)){
            if (editWidgetQuickPostJobs.isValid()) {
                showProgress();
                if (editWidgetQuickPostJobs.save().isPost()) {
                    if (!isFinishing()) {
                        dismissProgress();
                        onBackPressed();
                    }
                } else {
                    if (!isFinishing()) {
                        dismissProgress();
                    }
                }
            }
        }else if(this.fragment.equals(RESUME)){
            if (widgetQuickPostCV.isValid()){
                showProgress();
                if (widgetQuickPostCV.save().isPost()){
                    if (!isFinishing()){
                        dismissProgress();
                        onBackPressed();
                    }
                }else {
                    if (!isFinishing()){
                        dismissProgress();
                    }
                }
            }
        }else {
            if (widgetQuickPostNormal.isValid()){
                showProgress();
                widgetQuickPostNormal.save();
                if (widgetQuickPostNormal.isPost()){
                    if (!isFinishing()){
                        dismissProgress();
                        onBackPressed();
                    }
                }else {
                    if (!isFinishing()){
                        dismissProgress();
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnPost){
            try {
                onPost();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if (view==optionPrivacy){
            spnPrivacy.performClick();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        updatePrivacyUI(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

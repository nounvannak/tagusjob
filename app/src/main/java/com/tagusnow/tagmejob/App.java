package com.tagusnow.tagmejob;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.jaychang.slm.SocialLoginManager;
import org.fuckboilerplate.rx_social_connect.RxSocialConnect;
import java.net.URISyntaxException;
import io.victoralbertos.jolyglot.GsonSpeaker;


public class App extends MultiDexApplication {
    public static final String CHANNEL_ID = "TagUsJobServiceChannel";
    public static final String CHANNEL_NAME = "TagUsJob Service Channel";

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(getBaseContext());
        SocialLoginManager.init(this);
        RxSocialConnect
                .register(this, "myEncryptionKey")
                .using(new GsonSpeaker());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(SuperConstance.SOCKET_IO);
        }catch (URISyntaxException e){
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}

package com.tagusnow.tagmejob;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.isseiaoki.simplecropview.util.Utils;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.Job.ApplyJobFace;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.File;

public class ApplyJobActivity extends TagUsJobActivity {

    private static final String TAG = ApplyJobActivity.class.getSimpleName();
//    private static final String INTENT_TYPE = "image/*|application/vnd.ms-excel|application/msword|application/vnd.openxmlformats-officedocument.wordprocessingml.document|application/vnd.openxmlformats-officedocument.spreadsheetml.sheet|application/pdf|application/x-pdf";
    private static final String INTENT_TYPE = "*/*";
    public static final int PERMISSIONS_REQUEST_CODE_CV = 451;
    public static final int PERMISSIONS_REQUEST_CODE_COVER_LETTER = 452;
    public static final int CV_PICKER_REQUEST_CODE = 453;
    public static final int COVER_LETTER_PICKER_REQUEST_CODE = 454;
    public static final String FEED = "feed";
    private ApplyJobFace applyJobFace;
    private SmUser user;
    private Feed feed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_job);
        this.initTemp();
        applyJobFace = (ApplyJobFace)findViewById(R.id.apply_job_face);
        applyJobFace.setActivity(this);
        applyJobFace.setUser(this.user);
        applyJobFace.setFeed(this.feed);

        EditText cv = applyJobFace.getFile_cv();
        EditText cover_letter = applyJobFace.getFile_cover_letter();
        Button btnCancel = (Button)applyJobFace.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermissionsAndOpenFilePicker(CV_PICKER_REQUEST_CODE);
            }
        });

        cover_letter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermissionsAndOpenFilePicker(COVER_LETTER_PICKER_REQUEST_CODE);
            }
        });
    }

    private void checkPermissionsAndOpenFilePicker(int requestCode) {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        switch (requestCode){
            case CV_PICKER_REQUEST_CODE:
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        showError();
                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE_CV);
                    }
                } else {
                    openFilePicker(requestCode);
                }
                break;
            case COVER_LETTER_PICKER_REQUEST_CODE:
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        showError();
                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE_COVER_LETTER);
                    }
                } else {
                    openFilePicker(requestCode);
                }
                break;
        }
    }

    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE_CV:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker(CV_PICKER_REQUEST_CODE);
                } else {
                    showError();
                }
            break;
            case PERMISSIONS_REQUEST_CODE_COVER_LETTER:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker(COVER_LETTER_PICKER_REQUEST_CODE);
                } else {
                    showError();
                }
                break;
        }
    }

    private void openFilePicker(int requestCode) {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(requestCode)
                .withHiddenFiles(true)
                .withTitle("File Picker")
                .start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CV_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);

            if (path != null) {
                applyJobFace.setFileCV(new File(path));
            }
        }else if (requestCode == COVER_LETTER_PICKER_REQUEST_CODE && resultCode == RESULT_OK){
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);

            if (path != null) {
                applyJobFace.setFileCoverLetter(new File(path));
            }
        }
    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.feed = (Feed) getIntent().getSerializableExtra(FEED);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ApplyJobActivity.class);
    }
}

package com.tagusnow.tagmejob.BaseAdapter;

import android.app.Activity;
import android.support.annotation.Nullable;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.MultipleItemRvAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.tagusnow.tagmejob.BaseProvider.PostNormalProvider;
import java.util.List;

public class HomeAdapter extends MultipleItemRvAdapter<MultiItemEntity, BaseViewHolder> {

    public static final int POST_NORMAL = 100;
    public static final int POST_JOB = 101;
    public static final int POST_RESUME = 102;
    public static final int WIDGET_POST = 103;
    public static final int FOLLOW_SUGGEST = 104;

    private Activity activity;

    public HomeAdapter(Activity activity,List<MultiItemEntity> data){
        super(data);
        this.activity = activity;
    }

    public HomeAdapter(@Nullable List<MultiItemEntity> data) {
        super(data);
    }

    @Override
    protected int getViewType(MultiItemEntity multiItemEntity) {
        return multiItemEntity.getItemType();
    }

    @Override
    public void registerItemProvider() {
        mProviderDelegate.registerProvider(new PostNormalProvider(this.activity));
    }
}

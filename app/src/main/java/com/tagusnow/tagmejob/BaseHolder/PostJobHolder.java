package com.tagusnow.tagmejob.BaseHolder;

import android.app.Activity;
import android.view.View;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.BaseCard.CardJob;

public class PostJobHolder extends BaseViewHolder {

    private CardJob cardJob;

    public PostJobHolder(View view) {
        super(view);
        cardJob = (CardJob)view;
    }
}

package com.tagusnow.tagmejob.BaseHolder;

import android.app.Activity;
import android.view.View;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.MultipleItemRvAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.Card;

public class PostNormalHolder extends BaseViewHolder {

    private Card card;

    public PostNormalHolder(View view) {
        super(view);
        card = (Card)itemView;
    }

    public void bindView(Activity activity, SmUser user, Feed feed){
        card.Activity(activity);
        card.Auth(user);
        card.feed(feed);
    }
}

package com.tagusnow.tagmejob.BaseProvider;

import android.app.Activity;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.tagusnow.tagmejob.BaseAdapter.HomeAdapter;
import com.tagusnow.tagmejob.BaseHolder.PostNormalHolder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;

public class PostNormalProvider extends BaseItemProvider<Feed, BaseViewHolder> {

    private Activity activity;
    private SmUser user;

    public PostNormalProvider(Activity activity){
        this.activity = activity;
        this.user = new Auth(activity).checkAuth().token();
    }

    @Override
    public int viewType() {
        return HomeAdapter.POST_NORMAL;
    }

    @Override
    public int layout() {
        return R.layout.card;
    }

    @Override
    public void convert(BaseViewHolder helper, Feed data, int position) {
        ((PostNormalHolder) helper).bindView(this.activity,this.user,data);
    }
}

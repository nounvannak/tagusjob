package com.tagusnow.tagmejob;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class Camera extends AppCompatActivity {
    private static final int CAPTURE_MEDIA = 368;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        launchCamera();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_MEDIA) {
            if (resultCode == RESULT_OK && data != null) {
                launchCamera();
            }
        }
    }

    // showImagePicker is boolean value: Default is true
    // setAutoRecord() to start recording the video automatically if media action is set to video.
    private void launchCamera() {
    }
}

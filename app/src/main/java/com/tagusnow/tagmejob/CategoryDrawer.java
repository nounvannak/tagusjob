package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.V2.Message.MessageV2;
import com.tagusnow.tagmejob.adapter.CategoryAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.fragment.FollowFragment;
import com.tagusnow.tagmejob.fragment.JobsFragment;
import com.tagusnow.tagmejob.fragment.NofiticationFragment;
import com.tagusnow.tagmejob.fragment.SettingFragment;
import com.tagusnow.tagmejob.fragment.StaffFragment;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.v3.category.FilterCategoryActivity;
import com.tagusnow.tagmejob.v3.page.HomeFragment;
import com.tagusnow.tagmejob.v3.profile.AuthProfileActivity;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;
import io.sentry.Sentry;

public class CategoryDrawer extends TagUsJobActivity implements View.OnClickListener {

    private final static int REQ_PRIVACY = 3729;
    private final static String TAG = CategoryDrawer.class.getSimpleName();
    private SmUser smUser;
    private ListView listView;
    private EditText txtSearch;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private SmTagSubCategory smTagSubCategories;
    private Button btnTryAgain;
    private DrawerLayout drawer;
    private Socket socket;
    private ViewPager HomePage;
    private NavigationTabBar navigationTabBar;
    private ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
    private String fragment = "HomeFragment";
    private String[] colors = {"#1a7ab9","#1a7ab9","#1a7ab9","#1a7ab9","#1a7ab9","#1a7ab9"};
    private int[] Icons;
    private int[] Titles;
    private Fragment[] Pages;

    private void InitNavigatorBar(){

        HomePage = (ViewPager)findViewById(R.id.home_page);
        HomePage.setAdapter(new HomePager(getSupportFragmentManager()));
        navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
        for (int i=0;i<Pages.length;i++){
            models.add(new NavigationTabBar.Model.Builder(getResources().getDrawable(Icons[i]),
                    Color.parseColor(colors[i])
            ).selectedIcon(getResources().getDrawable(Icons[i]))
                    .title(getResources().getString(Titles[i]))
                    .badgeTitle("NTB")
                    .build());
        }

        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(HomePage);
        navigationTabBar.deselect();
        navigationTabBar.setTitleMode(NavigationTabBar.TitleMode.ACTIVE);
        navigationTabBar.setBadgeGravity(NavigationTabBar.BadgeGravity.BOTTOM);
        navigationTabBar.setBadgePosition(NavigationTabBar.BadgePosition.CENTER);
        navigationTabBar.setTypeface("fonts/arial.ttf");
        navigationTabBar.setIsBadged(true);
        navigationTabBar.setIsTitled(true);
        navigationTabBar.setIsTinted(true);
        navigationTabBar.setIsBadgeUseTypeface(true);
        navigationTabBar.setBadgeBgColor(Color.RED);
        navigationTabBar.setBadgeTitleColor(Color.WHITE);
        navigationTabBar.setIsSwiped(true);
        navigationTabBar.setBadgeSize(16);
        navigationTabBar.setTitleSize(16);
        navigationTabBar.setIconSizeFraction((float) 0.5);
        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(NavigationTabBar.Model model, int index) {
                navigationTabBar.getModels().get(index).isBadgeShowed();
            }

            @Override
            public void onEndTabSelected(NavigationTabBar.Model model, int index) {
                navigationTabBar.getModels().get(index).hideBadge();
            }
        });
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                fragment = Pages[position].getClass().getSimpleName();
                HomePage.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initSocket(){
        initTemp();
        socket = new App().getSocket();
        socket.on("New Post",mNewPost);
        if (smUser!=null){
            socket.on("Count Notification "+smUser.getId(),mCountNotification);
            socket.on("Count Notification Message"+smUser.getId(),COUNT_MESSAGE);
        }
        socket.on("Count Job",mCountJob);
        socket.connect();
    }

    private Emitter.Listener COUNT_MESSAGE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj.has("counter")){
                        try {
                            if (obj.getInt("user_id")==smUser.getId()){
//                                navigationTabBar.getModels().get(4).updateBadgeTitle(String.valueOf(obj.getInt("counter")));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener mCountJob = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj!=null){
                            Log.w(TAG,obj.toString());
//                            navigationTabBar.getModels().get(0).updateBadgeTitle("New");
//                            navigationTabBar.getModels().get(1).updateBadgeTitle("New");
//                            navigationTabBar.getModels().get(2).updateBadgeTitle("New");
                            ArrayList<Category> data = new ArrayList<>();
                            for (int i=0;i < obj.length();i++){
                                data.add(new Category(obj.getJSONObject(String.valueOf(i)).getInt("id"),
                                        obj.getJSONObject(String.valueOf(i)).getString("title"),
                                        obj.getJSONObject(String.valueOf(i)).getString("description"),
                                        null,
                                        obj.getJSONObject(String.valueOf(i)).getString("image"),
                                        obj.getJSONObject(String.valueOf(i)).getInt("job_counter")));
                            }
                            smTagSubCategories.setData(data);
                            initCategoryMenu();
                        }
                    }catch (Exception e){
                        Sentry.capture(e);
                        SuperUtil.errorTrancking(TAG+"::mCountJob");
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mCountNotification = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj.has("counter")){
                            if (obj.getInt("counter") > 0){
//                                navigationTabBar.getModels().get(6).updateBadgeTitle(String.valueOf(obj.getInt("counter")));
                            }
                        }else {
                            Log.w(TAG,"No Counter");
                        }
                    }catch (Exception e){
                        SuperUtil.errorTrancking(TAG+"::mCountNotification");
                        Sentry.capture(e);
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mNewPost = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj.has("user_id") && obj.getInt("user_id")!=smUser.getId()){
//                            navigationTabBar.getModels().get(0).updateBadgeTitle("New");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void initTemp(){
        this.smUser = new Auth(this).checkAuth().token();
        this.smTagSubCategories = new Repository(this).restore().getCategory();
        if (this.smUser==null){
            startActivity(LoginActivity.createIntent(this).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            if (this.smUser.getUser_type()==SmUser.FIND_STAFF){
                Icons = new int[]{
                        R.drawable._feed,
                        R.drawable._person,
                        R.drawable._group,
//                        R.drawable._message,
                        R.drawable._notification,
                        R.drawable._setting
                };
                Titles = new int[]{
                        R.string.home,
                        R.string.staff,
                        R.string.follow,
//                        R.string.message,
                        R.string.notification,
                        R.string.setting
                };
                Pages = new Fragment[]{
                        HomeFragment.newInstance(),
                        StaffFragment.newInstance(),
                        FollowFragment.newInstance(),
//                        MessageV2.newInstance(),
                        NofiticationFragment.newInstance(),
                        SettingFragment.newInstance()
                };
            }else {
                Icons = new int[]{
                        R.drawable._feed,
                        R.drawable._suitcase,
                        R.drawable._group,
//                        R.drawable._message,
                        R.drawable._notification,
                        R.drawable._setting
                };
                Titles = new int[]{
                        R.string.home,
                        R.string.jobs,
                        R.string.follow,
//                        R.string.message,
                        R.string.notification,
                        R.string.setting
                };
                JobsFragment jobsFragment = JobsFragment.newInstance();
//                jobsFragment.setCallback(this);
                Pages = new Fragment[]{
                        HomeFragment.newInstance(),
                        jobsFragment,
                        FollowFragment.newInstance(),
//                        MessageV2.newInstance(),
                        NofiticationFragment.newInstance(),
                        SettingFragment.newInstance()
                };
            }
        }
    }

    private void initView(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_menu_white);
        setSupportActionBar(toolbar);
        txtSearch = (EditText)findViewById(R.id.txtSearch);
        txtSearch.setCursorVisible(false);
        txtSearch.setFocusable(false);
        txtSearch.setOnClickListener(this);

        btnTryAgain = (Button)findViewById(R.id.btnTryAgian);
        btnTryAgain.setOnClickListener(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (Pages != null){
            InitNavigatorBar();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_drawer);
        try {
            initTemp();
            initView();
            initCategoryMenu();
            initSocket();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,CategoryDrawer.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initTemp();
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTemp();
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initTemp();
        drawer.closeDrawer(GravityCompat.START);
    }

    private void initCategoryMenu(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        final Menu menu = navigationView.getMenu();
        listView = (ListView)findViewById(R.id.innerListCategory);
        if(this.smTagSubCategories!=null){
            listView.setAdapter(new CategoryAdapter(getApplicationContext(),smTagSubCategories.getData()));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    filterPost(i);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQ_PRIVACY && resultCode==RESULT_OK && data!=null){
            int position = data.getIntExtra("position",-1);
            int privacy = data.getIntExtra(SuperConstance.PRIVACY,-1);
            Log.w(TAG,"position "+position);
            Log.w(TAG,"privacy "+privacy);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.category_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.user_profile) {
            startActivity(AuthProfileActivity.createIntent(this).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }

        return super.onOptionsItemSelected(item);
    }

    private void filterPost(int category){

        startActivity(FilterCategoryActivity
                .createIntent(this)
                .putExtra(FilterCategoryActivity.CATEGORY,new Gson().toJson(this.smTagSubCategories.getData().get(category)))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id==R.id.txtSearch){
            if (this.fragment.equals(MessageV2.class.getSimpleName())){
                startActivity(SearchChatListActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }else{
                startActivity(new Intent(this,SearchActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        }else if (view==btnTryAgain){

            this.smTagSubCategories = new Repository(this).response().getCategory();

            if(this.smTagSubCategories!=null && this.smTagSubCategories.getData()!=null && this.smTagSubCategories.getData().size() > 0){

                if (listView == null){
                    listView = (ListView)findViewById(R.id.innerListCategory);
                }

                listView.setAdapter(
                        new CategoryAdapter(getApplicationContext(),
                                smTagSubCategories.getData()));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        filterPost(i);
                    }
                });
            }
        }
    }

    class HomePager extends FragmentStatePagerAdapter{
        HomePager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return Pages[position];
        }

        @Override
        public int getCount() {
            return Pages!=null ? 5 : 0;
        }
    }
}

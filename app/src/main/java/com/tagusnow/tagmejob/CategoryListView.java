package com.tagusnow.tagmejob;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.adapter.CategoryLoader;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;

import java.util.ArrayList;

public class CategoryListView extends AppCompatActivity implements View.OnFocusChangeListener, SearchView.OnQueryTextListener {

    private static int mCategoryId = 0;
    private final static int REQUEST_SELECT_CATEGORY = 8302;
    static ImageView mCategoryCheck;
    CategoryLoader mCategoryAdapter;
    ArrayList<Category> mSmCategory;
    SmTagSubCategory smTagSubCategory;
    private SearchView mSearchViewCategory;
    private MenuItem mSearchCategoryMenuItem;
    private int mQueryLength;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list_view);

        /*Init category toolbar*/
        Toolbar mCategoryToolbar = (Toolbar)findViewById(R.id.categoryToolbar);
        mCategoryToolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        mCategoryToolbar.setTitle("Job Type List");
        setSupportActionBar(mCategoryToolbar);
        mCategoryToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnSalectCategory();
            }
        });

        /*get category object*/
        Intent mCategoryIntent = getIntent();
        smTagSubCategory = new Repository(this).restore().getCategory();

        mCategoryAdapter = new CategoryLoader(this,smTagSubCategory.getData());

        listView = (ListView)findViewById(R.id.categoryList);
        listView.setAdapter(mCategoryAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mCategoryCheck = (ImageView)view.findViewById(R.id.category_check);
                mCategoryCheck.setImageResource(R.drawable.ic_action_radio_button_unchecked);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mCategoryCheck.setImageTintList(ColorStateList.valueOf(getColor(R.color.colorAccent)));
                    }
                }
                mCategoryCheck.setImageResource(R.drawable.ic_action_check_circle);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mCategoryCheck.setImageTintList(ColorStateList.valueOf(getColor(R.color.colorAccent)));
                    }
                }
                mCategoryId = smTagSubCategory.getData().get(i).getId();
                Toast.makeText(getApplicationContext(),smTagSubCategory.getData().get(i).getTitle(),Toast.LENGTH_SHORT).show();
                returnSalectCategory();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_category,menu);
        mSearchViewCategory = (SearchView)menu.findItem(R.id.action_search).getActionView();
        mSearchCategoryMenuItem = menu.findItem(R.id.action_search);
        mSearchViewCategory.setQueryHint("Search here...");
        mSearchViewCategory.setSubmitButtonEnabled(true);
        mSearchViewCategory.setOnQueryTextFocusChangeListener(this);
        mSearchViewCategory.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    private void returnSalectCategory(){
        Intent intent = getIntent();
        intent.putExtra("category_select",mCategoryId);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Toast.makeText(this,query,Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mQueryLength = newText.length();
        mCategoryAdapter.getFilter().filter(newText);
        return false;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b){
            mSearchCategoryMenuItem.collapseActionView();
            mSearchViewCategory.setQuery("",false);
        }
    }

    private void setListAdapter() {
        mCategoryAdapter = new CategoryLoader(getApplicationContext(),mSmCategory);
        listView.setAdapter(mCategoryAdapter);
    }

    public class SearchCategory extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listView.setVisibility(View.VISIBLE);
            setListAdapter();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listView.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mSmCategory = smTagSubCategory.getData();
            return null;
        }
    }
}

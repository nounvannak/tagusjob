package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChangeCoverActivity extends AppCompatActivity{

    private final static String TAG = ChangeCoverActivity.class.getSimpleName();
    private Uri mUri;
    private ChangeCoverFragment changeCoverFragment;
    private static final String KEY_SOURCE_URI = "SourceUri";
    private SmUser authUser;
    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            Log.w(TAG,"onDenied");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_cover);
        AutoPermissions.Companion.loadAllPermissions(this, 1);
        this.initTemp();
        initToolbar();
        changeCoverFragment = ChangeCoverFragment.newInstance(mUri);
        showFragment(changeCoverFragment);
    }

    private void showFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.change_cover_frame,fragment).commit();
    }

    private void initTemp(){
        File file = new File(getIntent().getStringExtra(KEY_SOURCE_URI));
        this.mUri = Uri.fromFile(file);
        this.authUser = new Auth(this).checkAuth().token();
    }

    private void initToolbar(){
        Toolbar toolbar = findViewById(R.id.change_cover_toolbar);
        toolbar.setTitle("Edit Cover");
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getIntent().getData()!=null){
            mUri = getIntent().getData();
            Log.w(TAG,"uri "+mUri);
        }
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ChangeCoverActivity.class);
    }

    public static Intent createIntent(Activity activity, Uri uri){
        Intent intent = new Intent(activity,ChangeCoverActivity.class);
        intent.setData(uri);
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.btnDone:
                changeCoverFragment.cropImage();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void startResultActivity(Uri uri) {
        if (isFinishing()) return;
        Intent intent = ProfileActivity.createIntent(this,uri);
        intent.putExtra("cover",uri);
        setResult(RESULT_OK,intent);
        finish();
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;
import com.isseiaoki.simplecropview.util.Logger;
import com.isseiaoki.simplecropview.util.Utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import permissions.dispatcher.NeedsPermission;


public class ChangeProfileFragment extends Fragment implements View.OnClickListener{

    private final static String TAG = ChangeProfileFragment.class.getSimpleName();
    private static final int REQUEST_PICK_IMAGE = 10011;
    private static final int REQUEST_SAF_PICK_IMAGE = 10012;
    private static final String PROGRESS_DIALOG = "ProgressDialog";
    private static final String KEY_FRAME_RECT = "FrameRect";
    private static final String KEY_SOURCE_URI = "SourceUri";
    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
    private RectF mFrameRect = null;
    private Uri mSourceUri = null;
    private CropImageView mCoverCrop;

    public ChangeProfileFragment() {
        // Required empty public constructor
    }

    public static ChangeProfileFragment newInstance(Uri uri) {
        ChangeProfileFragment fragment = new ChangeProfileFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_SOURCE_URI,uri);
        fragment.setArguments(args);
        return fragment;
    }

    public static ChangeProfileFragment newInstance(){
        return new ChangeProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSourceUri = getArguments().getParcelable(KEY_SOURCE_URI);
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        mCoverCrop.setDebug(false);
        if (savedInstanceState != null) {
            // restore data
            mFrameRect = savedInstanceState.getParcelable(KEY_FRAME_RECT);
//            mSourceUri = savedInstanceState.getParcelable(KEY_SOURCE_URI);
        }

        // load image
        mCoverCrop.load(mSourceUri)
                .initialFrameRect(mFrameRect)
                .useThumbnail(true)
                .execute(mLoadCallback);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        if (resultCode == Activity.RESULT_OK) {
            // reset frame rect
            mFrameRect = null;
            switch (requestCode) {
                case REQUEST_PICK_IMAGE:
                    mSourceUri = result.getData();
                    mCoverCrop.load(mSourceUri)
                            .initialFrameRect(mFrameRect)
                            .useThumbnail(true)
                            .execute(mLoadCallback);
                    break;
                case REQUEST_SAF_PICK_IMAGE:
                    mSourceUri = Utils.ensureUriPermission(getContext(), result);
                    mCoverCrop.load(mSourceUri)
                            .initialFrameRect(mFrameRect)
                            .useThumbnail(true)
                            .execute(mLoadCallback);
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,@NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void initView(View view){
        mCoverCrop = (CropImageView)view.findViewById(R.id.change_cover_crop_image_view);
        view.findViewById(R.id.buttonDone).setOnClickListener(this);
        view.findViewById(R.id.buttonFitImage).setOnClickListener(this);
        view.findViewById(R.id.button1_1).setOnClickListener(this);
        view.findViewById(R.id.button3_4).setOnClickListener(this);
        view.findViewById(R.id.button4_3).setOnClickListener(this);
        view.findViewById(R.id.button9_16).setOnClickListener(this);
        view.findViewById(R.id.button16_9).setOnClickListener(this);
        view.findViewById(R.id.buttonFree).setOnClickListener(this);
        view.findViewById(R.id.buttonPickImage).setOnClickListener(this);
        view.findViewById(R.id.buttonRotateLeft).setOnClickListener(this);
        view.findViewById(R.id.buttonRotateRight).setOnClickListener(this);
        view.findViewById(R.id.buttonCustom).setOnClickListener(this);
        view.findViewById(R.id.buttonCircle).setOnClickListener(this);
        view.findViewById(R.id.buttonShowCircleButCropAsSquare).setOnClickListener(this);
    }

    public void showProgress() {
        ProgressDialogFragment f = ProgressDialogFragment.getInstance();
        getFragmentManager().beginTransaction().add(f,PROGRESS_DIALOG).commitAllowingStateLoss();
    }

    public void dismissProgress() {
        if (!isResumed()) return;
        FragmentManager manager = getFragmentManager();
        if (manager == null) return;
        ProgressDialogFragment f = (ProgressDialogFragment) manager.findFragmentByTag(PROGRESS_DIALOG);
        if (f != null) {
            getFragmentManager().beginTransaction().remove(f).commitAllowingStateLoss();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_FRAME_RECT, mCoverCrop.getActualCropRect());
        outState.putParcelable(KEY_SOURCE_URI,mCoverCrop.getSourceUri());
    }

    @NeedsPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    public void pickImage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            startActivityForResult(new Intent(Intent.ACTION_GET_CONTENT).setType("image/*"),
                    REQUEST_PICK_IMAGE);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, REQUEST_SAF_PICK_IMAGE);
        }
    }

    @NeedsPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) public void cropImage() {
        showProgress();
        mCoverCrop.crop(mSourceUri).execute(mCropCallback);
    }

    public Uri createSaveUri() {
        return createNewUri(getContext(), mCompressFormat);
    }

    public static String getDirPath() {
        String dirPath = "";
        File imageDir = null;
        File extStorageDir = Environment.getExternalStorageDirectory();
        if (extStorageDir.canWrite()) {
            imageDir = new File(extStorageDir.getPath() + "/TagUsJob");
        }
        if (imageDir != null) {
            if (!imageDir.exists()) {
                imageDir.mkdirs();
            }
            if (imageDir.canWrite()) {
                dirPath = imageDir.getPath();
            }
        }
        return dirPath;
    }

    public static Uri createNewUri(Context context, Bitmap.CompressFormat format) {
        long currentTimeMillis = System.currentTimeMillis();
        Date today = new Date(currentTimeMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH);
        String title = dateFormat.format(today);
        String dirPath = getDirPath();
        String fileName = "TAGUSJOB_" + title + "." + getMimeType(format);
        String path = dirPath + "/" + fileName;
        File file = new File(path);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, title);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/" + getMimeType(format));
        values.put(MediaStore.Images.Media.DATA, path);
        long time = currentTimeMillis / 1000;
        values.put(MediaStore.MediaColumns.DATE_ADDED, time);
        values.put(MediaStore.MediaColumns.DATE_MODIFIED, time);
        if (file.exists()) {
            values.put(MediaStore.Images.Media.SIZE, file.length());
        }

        ContentResolver resolver = context.getContentResolver();
        Uri uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Logger.i("SaveUri = " + uri);
        return uri;
    }

    public static String getMimeType(Bitmap.CompressFormat format) {
        Logger.i("getMimeType CompressFormat = " + format);
        switch (format) {
            case JPEG:
                return "jpeg";
            case PNG:
                return "png";
        }
        return "png";
    }

    private final LoadCallback mLoadCallback = new LoadCallback() {
        @Override public void onSuccess() {
            Log.w(TAG,mCoverCrop.getSourceUri().toString());
        }

        @Override public void onError(Throwable e) {
        }
    };

    private final CropCallback mCropCallback = new CropCallback() {
        @Override public void onSuccess(Bitmap cropped) {
            mCoverCrop.save(cropped)
                    .compressFormat(mCompressFormat)
                    .execute(createSaveUri(), mSaveCallback);
        }

        @Override public void onError(Throwable e) {
            dismissProgress();
        }
    };

    private final SaveCallback mSaveCallback = new SaveCallback() {
        @Override
        public void onSuccess(Uri outputUri) {
            dismissProgress();
            ((ChangeProfileActivity) getActivity()).startResultActivity(outputUri);
        }

        @Override
        public void onError(Throwable e) {
            dismissProgress();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonDone:
                cropImage();
                break;
            case R.id.buttonFitImage:
                mCoverCrop.setCropMode(CropImageView.CropMode.FIT_IMAGE);
                break;
            case R.id.button1_1:
                mCoverCrop.setCropMode(CropImageView.CropMode.SQUARE);
                break;
            case R.id.button3_4:
                mCoverCrop.setCropMode(CropImageView.CropMode.RATIO_3_4);
                break;
            case R.id.button4_3:
                mCoverCrop.setCropMode(CropImageView.CropMode.RATIO_4_3);
                break;
            case R.id.button9_16:
                mCoverCrop.setCropMode(CropImageView.CropMode.RATIO_9_16);
                break;
            case R.id.button16_9:
                mCoverCrop.setCropMode(CropImageView.CropMode.RATIO_16_9);
                break;
            case R.id.buttonCustom:
                mCoverCrop.setCustomRatio(7, 5);
                break;
            case R.id.buttonFree:
                mCoverCrop.setCropMode(CropImageView.CropMode.FREE);
                break;
            case R.id.buttonCircle:
                mCoverCrop.setCropMode(CropImageView.CropMode.CIRCLE);
                break;
            case R.id.buttonShowCircleButCropAsSquare:
                mCoverCrop.setCropMode(CropImageView.CropMode.CIRCLE_SQUARE);
                break;
            case R.id.buttonRotateLeft:
                mCoverCrop.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D);
                break;
            case R.id.buttonRotateRight:
                mCoverCrop.rotateImage(CropImageView.RotateDegrees.ROTATE_90D);
                break;
            case R.id.buttonPickImage:
                pickImage();
                break;
        }
    }
}

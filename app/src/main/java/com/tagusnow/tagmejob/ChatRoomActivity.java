package com.tagusnow.tagmejob;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.adapter.ChatRoomAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.model.v2.feed.ConversationPagination;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.Widget.MessageBox;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatRoomActivity extends TagUsJobActivity{

    private static final String TAG = ChatRoomActivity.class.getSimpleName();
    public static final String USER_TWO = "user_2";
    public static final String CHAT_ID = "chat_id";
    public static final int FILE_PICKER = 849;
    private static final int PERMISSIONS_REQUEST_CODE = 543;
    private Toolbar toolbar;
    private AvatarView profile;
    private AvatarPlaceholder refreshableAvatarPlaceholder;
    private IImageLoader imageLoader;
    private TextView user_name,time;
    private RecyclerView recyclerView;
    private MessageBox messageBox;
    private ChatRoomAdapter adapter;
    private ConversationPagination pagination;
    private SmUser user1,user2;
    private int chat_id;
    private Socket socket;
    private UserService service = ServiceGenerator.createService(UserService.class);

    private void AddMessage(Conversation conversation){
        if (conversation!=null){
            this.chat_id = conversation.getConversation_id();
            this.adapter.AppendItem(conversation);
            this.recyclerView.scrollToPosition(this.adapter.getItemCount() + 1);
        }
    }

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("error", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, Arrays.toString(args));
            }
        });
        socket.on("packet", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, Arrays.toString(args));
            }
        });
        socket.on("New Message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG,new Gson().toJson(args));
                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e(TAG,obj.toString());
                            if (obj.has("message")){
                                Conversation conversation = new Gson().fromJson(new String(Base64.decode(obj.getString("message"),Base64.DEFAULT)),Conversation.class);
                                if (conversation.getConversation_id()==chat_id && conversation.getUser_one()==user1.getId()){
                                    AddMessage(conversation);
                                    PlayMessageSound();
                                }else {
                                    if (conversation.getUser_one()==user1.getId() && conversation.getUser_two()==user2.getId()){
                                        AddMessage(conversation);
                                        PlayMessageSound();
                                    }else if (conversation.getUser_one()==user2.getId() && conversation.getUser_two()==user1.getId()){
                                        AddMessage(conversation);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.on("Count Notification Message" + this.user2.getId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("Count Not Message",new Gson().toJson(obj));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.on("Typing Message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("Typing",new Gson().toJson(obj));
                            String typing = obj.getString("user_name")+" is typing...";
                            if (obj.getInt("conversation_id")==chat_id && obj.getInt("user_id")==user2.getId()){
                                time.setText(typing);
                            }else {
                                if (obj.getInt("user_id")==user2.getId()){
                                    time.setText(typing);
                                }else {
                                    time.setText("last seen recently");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.on("Stop Typing Message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("Stop Typing",new Gson().toJson(obj));
                            if (obj.getInt("conversation_id")==chat_id && obj.getInt("user_id")==user2.getId()){
                                time.setText("last seen recently");
                            }else {
                                if (obj.getInt("user_id")==user2.getId()){
                                    time.setText("last seen recently");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.connect();
    }

    @Override
    protected void onDestroy() {
        if (socket.connected()){
            socket.disconnect();
        }
        Log.e(TAG,"destroy");
        super.onDestroy();
    }

    private void First(){

        service.ChatResource(this.user1.getId(),this.chat_id,10).enqueue(new Callback<ConversationPagination>() {
            @Override
            public void onResponse(Call<ConversationPagination> call, Response<ConversationPagination> response) {
                if (response.isSuccessful()){
                    First(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ConversationPagination> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void First(ConversationPagination pagination){
        this.pagination = pagination;
        if (pagination.getData()!=null && pagination.getData().size() > 0){
            this.chat_id = pagination.getData().get(0).getConversation_id();
            this.messageBox.setConversation_id(this.chat_id);
        }
        this.CheckNext();
        this.adapter = new ChatRoomAdapter(this,pagination,this.user1,this.user2);
        this.recyclerView.setAdapter(this.adapter);
        this.recyclerView.smoothScrollToPosition(pagination.getTo());
    }

    private void Next(){
        int page = this.pagination.getCurrent_page() + 1;
        service.ChatResource(this.user1.getId(),this.chat_id,10,page).enqueue(new Callback<ConversationPagination>() {
            @Override
            public void onResponse(Call<ConversationPagination> call, Response<ConversationPagination> response) {
                if (response.isSuccessful()){
                    Next(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ConversationPagination> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void Next(ConversationPagination pagination){
        this.pagination = pagination;
        this.CheckNext();
        this.adapter.Update(pagination);
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.e("dx",String.valueOf(dx));
                Log.e("dy",String.valueOf(dy));
                if (dy < 0){
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    Log.e(TAG,String.valueOf(lastCompletelyVisibleItemPosition));
                    try {
                        if ((lastCompletelyVisibleItemPosition == (pagination.getTo() - 1)) && (pagination.getTo() < pagination.getTotal())) {
                            if (pagination.getNext_page_url()!=null || !pagination.getNext_page_url().equals("")){
                                if (lastPage!=pagination.getCurrent_page()){
                                    isRequest = true;
                                    if (isRequest){
                                        lastPage = pagination.getCurrent_page();
                                        Next();
                                    }
                                }else{
                                    isRequest = false;
                                }
                            }
                        }
//                        if ((lastCompletelyVisibleItemPosition == (pagination.getTo() - 1)) && (pagination.getTo() < pagination.getTotal())) {
//                            if (pagination.getNext_page_url()!=null || !pagination.getNext_page_url().equals("")){
//                                if (lastPage!=pagination.getCurrent_page()){
//                                    isRequest = true;
//                                    if (isRequest){
//                                        lastPage = pagination.getCurrent_page();
//                                        Next();
//                                    }
//                                }else{
//                                    isRequest = false;
//                                }
//                            }
//                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ChatRoomActivity.class);
    }

    private void InitTemp(){
        this.user1 = new Auth(this).checkAuth().token();
        this.user2 = (SmUser) getIntent().getSerializableExtra(USER_TWO);
        this.chat_id = getIntent().getIntExtra(CHAT_ID,0);
    }

    private void InitView(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        profile = (AvatarView)findViewById(R.id.profile);
        user_name = (TextView)findViewById(R.id.user_name);
        time = (TextView)findViewById(R.id.time);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        messageBox = (MessageBox)findViewById(R.id.message_box);
        messageBox.setActivity(this);
        messageBox.setConversation_id(this.chat_id);
        messageBox.setUser1(this.user1);
        messageBox.setUser2(this.user2);
        ImageButton btnFile = (ImageButton)messageBox.findViewById(R.id.file);

        btnFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissionsAndOpenFilePicker();
            }
        });

        if (this.user2!=null){
            user_name.setText(this.user2.getUser_name());
            time.setText("last seen recently");
            refreshableAvatarPlaceholder = new AvatarPlaceholder(this.user2.getUser_name(), 50);
            imageLoader = new GlideLoader();
            if (this.user2.getImage()!=null){
                if (this.user2.getImage().contains("http")){
                    imageLoader.loadImage(profile, refreshableAvatarPlaceholder,this.user2.getImage());
                }else {
                    imageLoader.loadImage(profile, refreshableAvatarPlaceholder,SuperUtil.getProfilePicture(this.user2.getImage(),"400"));
                }
            }else {
                imageLoader.loadImage(profile, refreshableAvatarPlaceholder,null);
            }
        }
    }

    private void checkPermissionsAndOpenFilePicker() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            openFilePicker();
        }
    }

    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode==PERMISSIONS_REQUEST_CODE){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openFilePicker();
            } else {
                showError();
            }
        }
    }

    private void openFilePicker() {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(FILE_PICKER)
                .withHiddenFiles(true)
                .withTitle("File Picker")
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_PICKER && resultCode == RESULT_OK) {
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            if (path != null) {
                messageBox.setMy_file(new File(path));
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        this.InitTemp();
        this.InitView();
        this.initSocket();
        First();
    }
}

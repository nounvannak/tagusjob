package com.tagusnow.tagmejob;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.adapter.CategoryLoader;
import com.tagusnow.tagmejob.adapter.CityAdapter;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.repository.Repository;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class CityListView extends AppCompatActivity implements View.OnFocusChangeListener, SearchView.OnQueryTextListener {

    private static int mCity = 0;
    private ImageView mCityCheck;
    private SearchView mSearchViewCategory;
    private MenuItem mSearchCategoryMenuItem;
    private int mQueryLength;
    static CityAdapter cityAdapter;
    ArrayList<SmLocation.Data> smLocationData;
    SmLocation smLocation;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list_view);

        /*init toolbar*/
        Toolbar toolbar = (Toolbar)findViewById(R.id.cityToolbar);
        toolbar.setTitle("City/Province List");
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishResult();
            }
        });
        listView = (ListView)findViewById(R.id.cityList);
        responseCity();
    }

    private void responseCity(){
        smLocation = new Repository(this).restore().getLocation();
        cityAdapter = new CityAdapter(getApplicationContext(),smLocation.getData());
        listView.setAdapter(cityAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mCityCheck = (ImageView)view.findViewById(R.id.category_check);
                mCityCheck.setImageResource(R.drawable.ic_action_radio_button_unchecked);
                mCityCheck.setImageTintList(ColorStateList.valueOf(getColor(R.color.colorAccent)));
                mCityCheck.setImageResource(R.drawable.ic_action_check_circle);
                mCityCheck.setImageTintList(ColorStateList.valueOf(getColor(R.color.colorAccent)));
                mCity = smLocation.getData().get(i).getId();
                Toast.makeText(getApplicationContext(),smLocation.getData().get(i).getName(),Toast.LENGTH_SHORT).show();
                finishResult();
            }
        });
    }

    private void finishResult(){
        Intent intent = getIntent();
        intent.putExtra("city",mCity);
        intent.putExtra("city_list",new Gson().toJson(smLocation));
        setResult(RESULT_OK,intent);
        finish();
    }

    private void initQuerySearch(Menu menu){
        mSearchViewCategory = (SearchView)menu.findItem(R.id.action_search_city).getActionView();
        mSearchCategoryMenuItem = menu.findItem(R.id.action_search_city);
        mSearchViewCategory.setQueryHint("Search here...");
        mSearchViewCategory.setSubmitButtonEnabled(true);
        mSearchViewCategory.setOnQueryTextFocusChangeListener(this);
        mSearchViewCategory.setOnQueryTextListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_city,menu);
        initQuerySearch(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_search_city:
                /*code*/
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b){
            mSearchCategoryMenuItem.collapseActionView();
            mSearchViewCategory.setQuery("",false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Toast.makeText(this,query,Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mQueryLength = newText.length();
        cityAdapter.getFilter().filter(newText);
        return false;
    }

    private void setListAdapter() {
        cityAdapter = new CityAdapter(getApplicationContext(),smLocationData);
        listView.setAdapter(cityAdapter);
    }

    public class SearchCategory extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listView.setVisibility(View.VISIBLE);
            setListAdapter();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listView.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            smLocationData = smLocation.getData();
            return null;
        }
    }

}

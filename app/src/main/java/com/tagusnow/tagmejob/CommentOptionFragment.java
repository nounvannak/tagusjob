package com.tagusnow.tagmejob;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.adapter.CommentAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;

public class CommentOptionFragment extends DialogFragment implements View.OnClickListener{

    private static final String TAG = CommentOptionFragment.class.getSimpleName();
    private static final String AUTH_USER = "authUser";
    private static final String COMMENT = "comment";
    private SmUser authUser;
    private Comment comment;
    private LinearLayout btnEditComment,btnDeleteComment;
    private CommentAdapter commentAdapter;

    public CommentOptionFragment() {
        // Required empty public constructor
    }

    public static CommentOptionFragment newInstance(SmUser authUser, Comment comment) {
        CommentOptionFragment fragment = new CommentOptionFragment();
        Bundle args = new Bundle();
        args.putString(AUTH_USER,new Gson().toJson(authUser));
        args.putString(COMMENT,new Gson().toJson(comment));
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null) {
            authUser = new Gson().fromJson(getArguments().getString(AUTH_USER),SmUser.class);
            comment = new Gson().fromJson(getArguments().getString(COMMENT),Comment.class);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_comment_option, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(dialogView);
        initView(dialogView);
        return builder.create();
    }

    public CommentOptionFragment with(CommentAdapter commentAdapter){
        this.commentAdapter = commentAdapter;
        return this;
    }

    private void initView(View view){
        btnEditComment = (LinearLayout)view.findViewById(R.id.btnEditComment);
        btnDeleteComment = (LinearLayout)view.findViewById(R.id.btnDeleteComment);

        btnDeleteComment.setOnClickListener(this);
        btnEditComment.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnEditComment:
                break;
            case R.id.btnDeleteComment:
                break;
        }
    }
}

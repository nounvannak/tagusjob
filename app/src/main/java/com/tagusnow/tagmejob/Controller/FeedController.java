package com.tagusnow.tagmejob.Controller;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.asyn.FacebookAsync;
import com.tagusnow.tagmejob.asyn.OnLoopjCompleted;
import com.tagusnow.tagmejob.asyn.TagusnowAsync;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FacebookFeed;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.feed.TagusnowFeed;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class FeedController{
    private SmTagFeed tag_feed;
    private List<FacebookFeed> facebook;
    private List<TagusnowFeed> tagusjob;
    TagusnowAsync tagusnowAsync;

    public FeedController(SmTagFeed tag_feed){
        this.tag_feed = tag_feed;
    }

     private void deploy(String feed,String type,int i){
        if (type.equals(SuperConstance.PLATFORM_FACEBOOK)){
//            FacebookFeed facebookFeed = deployFacebook(feed);
//            this.facebook.add(i,facebookFeed);
            deployFacebook(feed,i);
        }else if (type.equals(SuperConstance.TAGUSNOW_POST)){
//            TagusnowFeed tagusnowFeed = deployTagusnowFeed(feed);
//            this.tagusjob.add(i,tagusnowFeed);
//            deployTagusnowFeed(feed,i);
            this.tagusjob.add(i,tagusnowAsync.getTagusnowFeed());
        }
    }

    /*private FacebookFeed deployFacebook(String feedParams){
        AsyncHttpClient mTagusnowFeedClient = new AsyncHttpClient();
        RequestParams mTagusnowFeedRequest = new RequestParams();
        mTagusnowFeedRequest.put("feed",feedParams);
        FacebookAsync facebookAsync = new FacebookAsync();
        facebookAsync.setUsePoolThread(false);
        mTagusnowFeedClient.post(SuperConstance.BASE_URL + "/deployFeed", mTagusnowFeedRequest,facebookAsync);
        return  facebookAsync.getFacebookFeed();
    }*/

     private void deployFacebook(String feedParams,final int i){
        AsyncHttpClient mTagusnowFeedClient = new AsyncHttpClient();
        RequestParams mTagusnowFeedRequest = new RequestParams();
        mTagusnowFeedRequest.put("feed",feedParams);
        final FacebookAsync facebookAsync = new FacebookAsync();
        facebookAsync.setUsePoolThread(false);
        mTagusnowFeedClient.post(SuperConstance.BASE_URL + "/deployFeed", mTagusnowFeedRequest,new BaseJsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, Object response) {
                FacebookFeed facebookFeed = new Gson().fromJson(rawJsonResponse,FacebookFeed.class);
                facebook.add(facebookFeed);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, Object errorResponse) {

            }

            @Override
            protected Object parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void deployTagusnowFeed(String feedParams,final int i){
        try {
            AsyncHttpClient mTagusnowFeedClient = new AsyncHttpClient();
            RequestParams mTagusnowFeedRequest = new RequestParams();
            mTagusnowFeedRequest.put("feed",feedParams);
            mTagusnowFeedClient.post(SuperConstance.BASE_URL + "/deployFeed", mTagusnowFeedRequest, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                    Log.d("TagusnowFeed",rawJsonResponse);
                    TagusnowFeed facebookFeed = new Gson().fromJson(rawJsonResponse,TagusnowFeed.class);
                    tagusjob.add(i,facebookFeed);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {

                }

                @Override
                protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                    return null;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private TagusnowFeed deployTagusnowFeed(String feedParams){
        AsyncHttpClient mTagusnowFeedClient = new AsyncHttpClient();
        RequestParams mTagusnowFeedRequest = new RequestParams();
        mTagusnowFeedRequest.put("feed",feedParams);
        tagusnowAsync = new TagusnowAsync();
        tagusnowAsync.setUseSynchronousMode(false);
        mTagusnowFeedClient.post(SuperConstance.BASE_URL + "/deployFeed", mTagusnowFeedRequest,tagusnowAsync);
        this.tagusjob.add(tagusnowAsync.getTagusnowFeed());
        return  tagusnowAsync.getTagusnowFeed();
    }


    public FeedController(){
        super();
    }

    public SmTagFeed getTag_feed() {
        return tag_feed;
    }

    public void setTag_feed(SmTagFeed tag_feed) {
        this.tag_feed = tag_feed;
    }

    public List<FacebookFeed> getFacebook() {
        return facebook;
    }

    public void setFacebook(ArrayList<FacebookFeed> facebook) {
        this.facebook = facebook;
    }

    public List<TagusnowFeed> getTagusjob() {
        return tagusjob;
    }

    public void setTagusjob(TagusnowFeed tagusjob){
        this.tagusjob.add(tagusjob);
    }

    public void setTagusjob(ArrayList<TagusnowFeed> tagusjob) {
        this.tagusjob = tagusjob;
    }
}

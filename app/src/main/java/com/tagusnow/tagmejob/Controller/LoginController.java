package com.tagusnow.tagmejob.Controller;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.SuperConstance;
import cz.msebera.android.httpclient.Header;

public class LoginController {

    public static class Web{

        public static void login(SharedPreferences sharedPreferences, ProgressDialog dialog, String email, String password){
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams requestParams = new RequestParams();
            requestParams.put(SuperConstance.EMAIL,email);
            requestParams.put(SuperConstance.PASSWORD,password);
            client.post(SuperConstance.BASE_URL + "/token", requestParams, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {

                }

                @Override
                protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                    return null;
                }
            });
        }

    }

    public static class Facebook{

    }

    public static class Google{

    }

}

package com.tagusnow.tagmejob;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.andrognito.flashbar.Flashbar;
import com.andrognito.flashbar.anim.FlashAnim;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.REST.Service.PostService;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreatePostActivity extends TagUsJobActivity implements View.OnClickListener{

    private final static int REQ_PRIVACY = 4939;
    private final static String TAG = CreatePostActivity.class.getSimpleName();
    public static final String FEED = "Feed";
    private ImageView optPrivacyIcon;
    private TextView optPrivateTitle,createPostProfileName,categorySelectTitle;
    private SmUser authUser;
    private CircleImageView profileCreatePost;
    ArrayList<File> mFileSelect = new ArrayList<>();
    ArrayList<String> mFileSelectStr = new ArrayList<>();
    ArrayList<String> mDefaultFile = new ArrayList<>();
    ArrayList<String> mTempOldFile = new ArrayList<>();
    private boolean isPost = true;
    private EmojiconEditText txtPostFeed;
    RelativeLayout layout_1_select;
    LinearLayout layout_2_select,layout_3_select,layout_4_select,buttonCategory;
    ImageView image_1_select,image_2_select_1,image_2_select_2,image_3_select_1,image_3_select_2,image_3_select_3,image_4_select_1,image_4_select_2,image_4_select_3,image_4_select_4;
    ImageButton button_remove_1_select,button_remove_2_select_1,button_remove_2_select_2,button_remove_3_select_1,button_remove_3_select_2,button_remove_3_select_3,button_remove_4_select_1,button_remove_4_select_2,button_remove_4_select_3,button_remove_4_select_4;
    LinearLayout layout_more_4_select;
    ImageView image_more_4_select_1,image_more_4_select_2,image_more_4_select_3,image_more_4_select_4;
    ImageButton button_remove_more_4_select_1,button_remove_more_4_select_2,button_remove_more_4_select_3,button_show_more_4_select;
    TextView txt_more_4;
    private final static int BOTH = 1;
    private final static int IMAGE_ONLY = 2;
    private final static int MOVIE_ONLY = 3;
    static final int OPEN_MEDIA_PICKER = 1;
    private final static int REQUEST_SELECT_CATEGORY = 4234;
    private final static int REQUEST_SELECT_MORE_IMAGE = 7439;
    int categoryId = 0;
    int privacy = 0;
    int postId = 0;
    SmTagSubCategory smTagSubCategory;
    private static Feed mSmTagFeed;
    Toolbar toolbar;
    private boolean isEdit = false;
    private boolean isShowCamera = true;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private PostService service = retrofit.create(PostService.class);

    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            Log.w(TAG,"onDenied");
        }
    };
    private Callback<Feed> mSave = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            dismissProgress();
            if (response.isSuccessful()){
                mSmTagFeed = response.body();
            }else {
                mSmTagFeed = null;
                try {
                    ShowAlert("Post Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
            ShowAlert("Post Error!",t.getMessage());
        }
    };
    private Callback<Feed> mUpdate = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            dismissProgress();
            if (response.isSuccessful()){
                mSmTagFeed = response.body();
            }else {
                mSmTagFeed = null;
                try {
                    ShowAlert("Post Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            ShowAlert("Post Error!",t.getMessage());
            t.printStackTrace();
        }
    };

    public static Intent createIntent(Activity activity){
        return new Intent(activity,CreatePostActivity.class);
    }

    private void initTemp(){
        authUser = new Auth(this).checkAuth().token();
        smTagSubCategory = new Repository(this).restore().getCategory();
        /*Request Edit Post*/
        Intent homeIntent = getIntent();
        mSmTagFeed = new Gson().fromJson(homeIntent.getStringExtra(FEED),Feed.class);
    }

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        initTemp();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        initTemp();
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        AutoPermissions.Companion.loadAllPermissions(this, 1);
        initTemp();

        /*Toolbar Init*/
        toolbar = (Toolbar) findViewById(R.id.createPostToolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        toolbar.setTitle("Create Post");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        /*button select category*/
        buttonCategory = (LinearLayout)findViewById(R.id.buttonCategory);
        buttonCategory.setOnClickListener(this);

        /*Init Image Select Post Grid*/
        layout_1_select = (RelativeLayout)findViewById(R.id.layout_1_select);
        layout_2_select = (LinearLayout)findViewById(R.id.layout_2_select);
        layout_3_select = (LinearLayout)findViewById(R.id.layout_3_select);
        layout_4_select = (LinearLayout)findViewById(R.id.layout_4_select);
        layout_more_4_select = (LinearLayout)findViewById(R.id.layout_more_4_select);

        image_1_select = (ImageView)findViewById(R.id.image_1_select);
        image_2_select_1 = (ImageView)findViewById(R.id.image_2_select_1);
        image_2_select_2 = (ImageView)findViewById(R.id.image_2_select_2);
        image_3_select_1 = (ImageView)findViewById(R.id.image_3_select_1);
        image_3_select_2 = (ImageView)findViewById(R.id.image_3_select_2);
        image_3_select_3 = (ImageView)findViewById(R.id.image_3_select_3);
        image_4_select_1 = (ImageView)findViewById(R.id.image_4_select_1);
        image_4_select_2 = (ImageView)findViewById(R.id.image_4_select_2);
        image_4_select_3 = (ImageView)findViewById(R.id.image_4_select_3);
        image_4_select_4 = (ImageView)findViewById(R.id.image_4_select_4);
        image_more_4_select_1 = (ImageView)findViewById(R.id.image_more_4_select_1);
        image_more_4_select_2 = (ImageView)findViewById(R.id.image_more_4_select_2);
        image_more_4_select_3 = (ImageView)findViewById(R.id.image_more_4_select_3);
        image_more_4_select_4 = (ImageView)findViewById(R.id.image_more_4_select_4);

        button_remove_1_select = (ImageButton)findViewById(R.id.button_remove_1_select);
        button_remove_2_select_1 = (ImageButton)findViewById(R.id.button_remove_2_select_1);
        button_remove_2_select_2 = (ImageButton)findViewById(R.id.button_remove_2_select_2);
        button_remove_3_select_1 = (ImageButton)findViewById(R.id.button_remove_3_select_1);
        button_remove_3_select_2 = (ImageButton)findViewById(R.id.button_remove_3_select_2);
        button_remove_3_select_3 = (ImageButton)findViewById(R.id.button_remove_3_select_3);
        button_remove_4_select_1 = (ImageButton)findViewById(R.id.button_remove_4_select_1);
        button_remove_4_select_2 = (ImageButton)findViewById(R.id.button_remove_4_select_2);
        button_remove_4_select_3 = (ImageButton)findViewById(R.id.button_remove_4_select_3);
        button_remove_4_select_4 = (ImageButton)findViewById(R.id.button_remove_4_select_4);
        button_remove_more_4_select_1 = (ImageButton)findViewById(R.id.button_remove_more_4_select_1);
        button_remove_more_4_select_2 = (ImageButton)findViewById(R.id.button_remove_more_4_select_2);
        button_remove_more_4_select_3 = (ImageButton)findViewById(R.id.button_remove_more_4_select_3);
        button_show_more_4_select = (ImageButton)findViewById(R.id.button_show_more_4_select);
        txt_more_4 = (TextView)findViewById(R.id.txt_more_4);

        button_remove_1_select.setOnClickListener(this);
        button_remove_2_select_1.setOnClickListener(this);
        button_remove_2_select_2.setOnClickListener(this);
        button_remove_3_select_1.setOnClickListener(this);
        button_remove_3_select_2.setOnClickListener(this);
        button_remove_3_select_3.setOnClickListener(this);
        button_remove_4_select_1.setOnClickListener(this);
        button_remove_4_select_2.setOnClickListener(this);
        button_remove_4_select_3.setOnClickListener(this);
        button_remove_4_select_4.setOnClickListener(this);
        button_remove_more_4_select_1.setOnClickListener(this);
        button_remove_more_4_select_2.setOnClickListener(this);
        button_remove_more_4_select_3.setOnClickListener(this);
        button_show_more_4_select.setOnClickListener(this);

        /*init post feed text box*/
        txtPostFeed = (EmojiconEditText)findViewById(R.id.txtPostFeed);
        txtPostFeed.setUseSystemDefault(true);

        /*Event Post Feed text change*/
        txtPostFeed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0){
                    isPost = true;
                }else {
                    isPost = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        /*back create post*/
        ImageButton btnBackPost = (ImageButton)findViewById(R.id.btnBackPost);
        btnBackPost.setOnClickListener(this);

        /*change option privacy*/
        LinearLayout optionPrivacy = (LinearLayout)findViewById(R.id.optionPrivacy);
        optionPrivacy.setOnClickListener(this);

        /*take and upload picture to post*/
        RelativeLayout btnSelectPostImage = (RelativeLayout)findViewById(R.id.btnSelectPostImage);
        btnSelectPostImage.setOnClickListener(this);

        /*init option privacy*/
        optPrivacyIcon = (ImageView)findViewById(R.id.optPrivacyIcon);
        optPrivateTitle = (TextView)findViewById(R.id.optPrivacyTitle);

        /*init category option*/
        categorySelectTitle = (TextView)findViewById(R.id.categorySelectTitle);

        profileCreatePost = (CircleImageView)findViewById(R.id.profileCreatePost);
        createPostProfileName = (TextView)findViewById(R.id.createPostProfileName);

        if (authUser!=null){
            createPostProfileName.setText(authUser.getUser_name());
            if (authUser.getImage()!=null){
                if (authUser.getImage().contains("http")){
                    Glide.with(this)
                            .load(authUser.getImage())
                            .centerCrop()
                            .error(R.drawable.ic_user_male_icon)
                            .into(profileCreatePost);
                }else{
                    Glide.with(this)
                            .load(SuperUtil.getProfilePicture(authUser.getImage(),"200"))
                            .centerCrop()
                            .error(R.drawable.ic_user_male_icon)
                            .into(profileCreatePost);
                }
            }else {
                Glide.with(this)
                        .load(R.drawable.ic_user_male_icon)
                        .centerCrop()
                        .error(R.drawable.ic_user_male_icon)
                        .into(profileCreatePost);
            }
        }

        /*edit UI*/
        if (isEdit){
            onEditPost();
        }
        selectPrivacy(SuperConstance.PRIVACY_VALUE);

    }

    private void selectPrivacy(int privacy){
        if (privacy==SuperConstance.PRIVACY_PUBLIC){
            optPrivacyIcon.setBackgroundResource(R.drawable.ic_action_language);
            optPrivateTitle.setText(R.string.publics);
        }else if(privacy==SuperConstance.PRIVACY_PRIVATE){
            optPrivacyIcon.setBackgroundResource(R.drawable.ic_action_lock);
            optPrivateTitle.setText(R.string.privates);
        }
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        initTemp();
//    }

//    @Override
//    protected void onPostResume() {
//        super.onPostResume();
//    }

    private void onEditPost(){
        ArrayList<String> files = new ArrayList<>();
        if (mSmTagFeed!=null){
            categoryId = mSmTagFeed.getSm_tag_id();
            postId = mSmTagFeed.getId();
            for (int i=0;i<smTagSubCategory.getData().size();i++){
                if (smTagSubCategory.getData().get(i).getId()==categoryId){
                    categorySelectTitle.setText(smTagSubCategory.getData().get(i).getTitle());
                }
            }
            toolbar.setTitle("Edit Post");
            if (mSmTagFeed.getType().equals(SuperConstance.TAGUSNOW_POST)){
                txtPostFeed.setText(mSmTagFeed.getFeed().getHashtag_text());
                if (mSmTagFeed.getFeed().getAlbum()!=null) {
                    for (int i = 0; i < mSmTagFeed.getFeed().getAlbum().size(); i++) {
                        if (mSmTagFeed.getFeed().getAlbum().get(i).getMedia().getImage().getSrc().contains("http")) {
                            Uri uri = Uri.parse(mSmTagFeed.getFeed().getAlbum().get(i).getMedia().getImage().getSrc());
                            File file = new File(uri.getPath());
                            mFileSelect.add(file);
                            mTempOldFile.add(file.getName());
                            mDefaultFile.add(file.getName());
                            mFileSelectStr.add(mSmTagFeed.getFeed().getAlbum().get(i).getMedia().getImage().getSrc());
                            files.add(mSmTagFeed.getFeed().getAlbum().get(i).getMedia().getImage().getSrc());
                        } else {
                            Uri uri = Uri.parse(SuperUtil.getAlbumPicture(mSmTagFeed.getFeed().getAlbum().get(i).getMedia().getImage().getSrc(), "400"));
                            File file = new File(uri.getPath());
                            mFileSelect.add(file);
                            mTempOldFile.add(file.getName());
                            mDefaultFile.add(file.getName());
                            mFileSelectStr.add(SuperUtil.getAlbumPicture(mSmTagFeed.getFeed().getAlbum().get(i).getMedia().getImage().getSrc(), "400"));
                            files.add(SuperUtil.getAlbumPicture(mSmTagFeed.getFeed().getAlbum().get(i).getMedia().getImage().getSrc(), "400"));
                        }
                    }
                    updateFileSelectUI(files);
                }
            }
        }
    }

    private void updateFileSelectUI(ArrayList<String> files){
        layout_1_select.setVisibility(View.GONE);
        layout_2_select.setVisibility(View.GONE);
        layout_3_select.setVisibility(View.GONE);
        layout_4_select.setVisibility(View.GONE);
        layout_more_4_select.setVisibility(View.GONE);

        if (files.size() > 0 && (files.get(0)!=null || !files.get(0).equals(""))){
            if (files.size()==1){
                layout_1_select.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(files.get(0))
                        .centerCrop()
                        .into(image_1_select);
            }else if(files.size()==2){
                layout_2_select.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(files.get(0))
                        .centerCrop()
                        .into(image_2_select_1);
                Glide.with(this)
                        .load(files.get(1))
                        .centerCrop()
                        .into(image_2_select_2);
            }else if(files.size()==3){
                layout_3_select.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(files.get(0))
                        .centerCrop()
                        .into(image_3_select_1);
                Glide.with(this)
                        .load(files.get(1))
                        .centerCrop()
                        .into(image_3_select_2);
                Glide.with(this)
                        .load(files.get(2))
                        .centerCrop()
                        .into(image_3_select_3);
            }else if(files.size()==4){
                layout_4_select.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(files.get(0))
                        .centerCrop()
                        .into(image_4_select_1);
                Glide.with(this)
                        .load(files.get(1))
                        .centerCrop()
                        .into(image_4_select_2);
                Glide.with(this)
                        .load(files.get(2))
                        .centerCrop()
                        .into(image_4_select_3);
                Glide.with(this)
                        .load(files.get(3))
                        .centerCrop()
                        .into(image_4_select_4);
            }else if(files.size() > 4){
                layout_more_4_select.setVisibility(View.VISIBLE);
                int img = files.size() - 4;
                txt_more_4.setText(getString(R.string.select_more_image,img));
                Glide.with(this)
                        .load(files.get(0))
                        .centerCrop()
                        .into(image_more_4_select_1);
                Glide.with(this)
                        .load(files.get(1))
                        .centerCrop()
                        .into(image_more_4_select_2);
                Glide.with(this)
                        .load(files.get(2))
                        .centerCrop()
                        .into(image_more_4_select_3);
                Glide.with(this)
                        .load(files.get(3))
                        .centerCrop()
                        .into(image_more_4_select_4);
            }else{
                layout_1_select.setVisibility(View.GONE);
                layout_2_select.setVisibility(View.GONE);
                layout_3_select.setVisibility(View.GONE);
                layout_4_select.setVisibility(View.GONE);
                layout_more_4_select.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==OPEN_MEDIA_PICKER && resultCode==RESULT_OK && data!=null){
            ArrayList<String> selectionResult=data.getStringArrayListExtra("result");
            for (int i=0;i<selectionResult.size();i++){
                mFileSelect.add(new File(selectionResult.get(i)));
                mDefaultFile.add(mFileSelect.get(i).getName());
                mFileSelectStr.add(selectionResult.get(i));
            }
            if (mFileSelect.size() > 0){
                updateFileSelectUI(mFileSelectStr);
            }
        }else if (requestCode==REQUEST_SELECT_CATEGORY && resultCode==RESULT_OK && data!=null){
            categoryId = data.getIntExtra("category_select",-1);
            for (int i=0;i<smTagSubCategory.getData().size();i++){
                if (smTagSubCategory.getData().get(i).getId()==categoryId){
                    categorySelectTitle.setText(smTagSubCategory.getData().get(i).getTitle());
                }
            }
        }else if(requestCode==REQUEST_SELECT_MORE_IMAGE && resultCode==RESULT_OK && data!=null){
            ArrayList<String> selectionResult=data.getStringArrayListExtra("more_image_result");
//            Toast.makeText(this,selectionResult.size()+"",Toast.LENGTH_SHORT).show();
            mDefaultFile.clear();
            mFileSelect.clear();
            mFileSelectStr.clear();
            for (int i=0;i<selectionResult.size();i++){
                mFileSelect.add(new File(selectionResult.get(i)));
                mDefaultFile.add(mFileSelect.get(i).getName());
                mFileSelectStr.add(selectionResult.get(i));
            }
            if (mFileSelect.size() > 0){
                updateFileSelectUI(mFileSelectStr);
            }
        }else if(requestCode==REQ_PRIVACY && resultCode==RESULT_OK && data!=null){
            privacy = data.getIntExtra(SuperConstance.PRIVACY,-1);
            selectPrivacy(privacy);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isPost){
            getMenuInflater().inflate(R.menu.post_menu,menu);
        }else {
            return false;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void openGallery(){
        Intent mCameraIntent = new Intent(getApplicationContext(),Gallery.class);
        mCameraIntent.putExtra(OPEN_MEDIA_TITLE,"Gallery");
        mCameraIntent.putExtra(OPEN_MEDIA_MODE,IMAGE_ONLY);
        mCameraIntent.putExtra(OPEN_MEDIA_MAX,(4 - mFileSelect.size()));
        mCameraIntent.putExtra("file_selected",mFileSelectStr);
        startActivityForResult(mCameraIntent,OPEN_MEDIA_PICKER);
    }

    private void OpenImagePicker(){
        AndroidImagePicker.getInstance().pickMulti(CreatePostActivity.this, isShowCamera, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                if(items != null && items.size() > 0){
                    Log.i(TAG,"=====selected："+items.get(0).path);
                    for (int i=0; i < items.size(); i++){
                        mFileSelect.add(new File(items.get(i).path));
                        mDefaultFile.add(mFileSelect.get(i).getName());
                        mFileSelectStr.add(items.get(i).path);
                    }
                    if (mFileSelect.size() > 0){
                        updateFileSelectUI(mFileSelectStr);
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id==R.id.btnBackPost){
            super.onBackPressed();
        }else if(id==R.id.optionPrivacy){
            Intent privacyIntent = new Intent(getApplicationContext(),PrivacyActivity.class);
            privacyIntent.putExtra(SuperConstance.PRIVACY,SuperConstance.PRIVACY_VALUE);
            startActivityForResult(privacyIntent,REQ_PRIVACY);
        }else if(id==R.id.btnSelectPostImage){
            OpenImagePicker();
        }else if (id==R.id.button_remove_1_select){
            mFileSelect.remove(0);
            mFileSelectStr.remove(0);
            mDefaultFile.remove(0);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_2_select_1){
            mFileSelect.remove(0);
            mFileSelectStr.remove(0);
            mDefaultFile.remove(0);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_2_select_2){
            mFileSelect.remove(1);
            mFileSelectStr.remove(1);
            mDefaultFile.remove(1);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_3_select_1){
            mFileSelect.remove(0);
            mFileSelectStr.remove(0);
            mDefaultFile.remove(0);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_3_select_2){
            mFileSelect.remove(1);
            mFileSelectStr.remove(1);
            mDefaultFile.remove(1);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_3_select_3){
            mFileSelect.remove(2);
            mFileSelectStr.remove(2);
            mDefaultFile.remove(2);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_4_select_1){
            mFileSelect.remove(0);
            mFileSelectStr.remove(0);
            mDefaultFile.remove(0);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_4_select_2){
            mFileSelect.remove(1);
            mFileSelectStr.remove(1);
            mDefaultFile.remove(1);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_4_select_3){
            mFileSelect.remove(2);
            mFileSelectStr.remove(2);
            mDefaultFile.remove(2);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_4_select_4){
            mFileSelect.remove(3);
            mFileSelectStr.remove(3);
            mDefaultFile.remove(3);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_more_4_select_1){
            mFileSelect.remove(0);
            mFileSelectStr.remove(0);
            mDefaultFile.remove(0);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_more_4_select_2){
            mFileSelect.remove(1);
            mFileSelectStr.remove(1);
            mDefaultFile.remove(1);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_remove_more_4_select_3){
            mFileSelect.remove(2);
            mFileSelectStr.remove(2);
            mDefaultFile.remove(2);
            updateFileSelectUI(mFileSelectStr);
        }else if (id==R.id.button_show_more_4_select){
            Intent mMoreImageIntent = new Intent(getApplicationContext(),ViewMoreImageSelectActivity.class);
            mMoreImageIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
            mMoreImageIntent.putExtra("Feed",new Gson().toJson(mSmTagFeed));
            mMoreImageIntent.putExtra("SmTagSubCategory",new Gson().toJson(smTagSubCategory));
            mMoreImageIntent.putExtra("mFileSelectStr",mFileSelectStr);
            startActivityForResult(mMoreImageIntent,REQUEST_SELECT_MORE_IMAGE);
        }else if(id==R.id.buttonCategory){
            Intent mSelectCategory = new Intent(getApplicationContext(),CategoryListView.class);
            mSelectCategory.putExtra("SmTagSubCategory",new Gson().toJson(smTagSubCategory));
            startActivityForResult(mSelectCategory,REQUEST_SELECT_CATEGORY);
        }
    }

    private Flashbar backgroundDrawable() {
        return new Flashbar.Builder(this)
                .gravity(Flashbar.Gravity.BOTTOM)
                .title("Post")
                .titleTypeface(Typeface.createFromAsset(getAssets(), "BeautifulAndOpenHearted.ttf"))
                .titleSizeInSp(32)
                .message("Please write & select something!")
                .backgroundDrawable(R.color.fbutton_color_alizarin)
                .enterAnimation(FlashAnim.with(this)
                        .animateBar()
                        .duration(400)
                        .slideFromLeft()
                        .overshoot())
                .exitAnimation(FlashAnim.with(this)
                        .animateBar()
                        .duration(250)
                        .slideFromLeft()
                        .accelerateDecelerate())
                .duration(3000)
                .build();
    }

    private CreatePostActivity postFeed(){
        if (postId == 0){
            RequestBody user_id = createPartFromString(String.valueOf(authUser.getId()));
            RequestBody text    = createPartFromString(txtPostFeed.getText().toString());
            RequestBody privacy = createPartFromString(String.valueOf(this.privacy));
            RequestBody cat_id  = createPartFromString(String.valueOf(0));
            List<MultipartBody.Part> mFile = new ArrayList<>();
            if (this.mFileSelect!=null){
                for (File file : this.mFileSelect){
                    mFile.add(prepareFilePart("feed_image[]",file));
                }
                service.Save(user_id,cat_id,text,privacy,mFile).enqueue(mSave);
            }else {
                service.Save(user_id,cat_id,text,privacy).enqueue(mSave);
            }
        }else {
            RequestBody user_id = createPartFromString(String.valueOf(authUser.getId()));
            RequestBody text    = createPartFromString(txtPostFeed.getText().toString());
            RequestBody privacy = createPartFromString(String.valueOf(this.privacy));
            RequestBody cat_id  = createPartFromString(String.valueOf(0));
            RequestBody post_id = createPartFromString(String.valueOf(postId));
            List<MultipartBody.Part> mFile = new ArrayList<>();
            if (this.mFileSelect!=null && this.mDefaultFile!=null){
                for (File file : this.mFileSelect){
                    mFile.add(prepareFilePart("feed_image[]",file));
                }
                String old_file = "";
                for (int i=0;i<mDefaultFile.size();i++){
                    if (i==0){
                        old_file = mDefaultFile.get(i);
                    }else {
                        old_file +=","+mDefaultFile.get(i);
                    }
                }
                RequestBody mOldFile = createPartFromString(old_file);
                service.Update(post_id,user_id,cat_id,text,privacy,mFile,mOldFile).enqueue(mUpdate);
            }else {
                if (this.mFileSelect!=null){
                    for (File file : this.mFileSelect){
                        mFile.add(prepareFilePart("feed_image[]",file));
                    }
                    service.Update(post_id,user_id,cat_id,text,privacy,mFile).enqueue(mUpdate);
                }else if (this.mDefaultFile!=null){
                    String old_file = "";
                    for (int i=0;i<mDefaultFile.size();i++){
                        if (i==0){
                            old_file = mDefaultFile.get(i);
                        }else {
                            old_file +=","+mDefaultFile.get(i);
                        }
                    }
                    RequestBody mOldFile = createPartFromString(old_file);
                    service.Update(post_id,user_id,cat_id,text,privacy,mOldFile).enqueue(mUpdate);
                }else {
                    service.Update(post_id,user_id,cat_id,text,privacy).enqueue(mUpdate);
                }
            }
        }

//        try {
//            String postUrl = SuperUtil.getBaseUrl(SuperConstance.POST_FEED);
//            AsyncHttpClient mPostClient = new AsyncHttpClient();
//            RequestParams mPostParam = new RequestParams();
//            mPostParam.put("auth_user",authUser.getId());
//            mPostParam.put("feed_text",txtPostFeed.getText());
//            mPostParam.put("privacy",privacy);
//            if (categoryId > 0){
//                mPostParam.put("job_category",categoryId);
//            }
//
//            if (postId > 0){
//                mPostParam.put("post_id",postId);
//                postUrl = SuperUtil.getBaseUrl(SuperConstance.UPDATE_FEED);
//            }
//
//            ArrayList<File> finalFile = new ArrayList<>();
//            for (int i=0;i < mFileSelect.size();i++){
//                Log.w("compare",mFileSelect.get(i).getName()+"!="+(new File(mFileSelectStr.get(i)).getName())+"\n");
//                if (postId==0){
//                    finalFile.add(mFileSelect.get(i));
//                    mDefaultFile.remove(mFileSelect.get(i).getName());
//                }else{
//                    if (mTempOldFile.size() > 0){
//                        if (!mTempOldFile.contains(mFileSelect.get(i).getName())){
//                            finalFile.add(mFileSelect.get(i));
//                            mDefaultFile.remove(mFileSelect.get(i).getName());
//                        }
//                    }else{
//                        finalFile.add(mFileSelect.get(i));
//                        mDefaultFile.remove(mFileSelect.get(i).getName());
//                    }
//                }
//            }
//
//            File [] files = new File[finalFile.size()];
//            for (int j=0;j<finalFile.size();j++){
//                files[j] = finalFile.get(j);
//                mDefaultFile.remove(finalFile.get(j).getName());
//            }
//
//            if (postId > 0){
//                String old_file = "";
//                for (int i=0;i<mDefaultFile.size();i++){
//                    if (i==0){
//                        old_file = mDefaultFile.get(i);
//                    }else {
//                        old_file +=","+mDefaultFile.get(i);
//                    }
//                }
//                mPostParam.put("old_file",old_file);
//            }
//
//            mPostParam.put("feed_image[]",files,"multipart/form-data","feed_image.png");
//            mPostClient.post(postUrl, mPostParam, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
//                    Log.w("onSuccess",rawJsonResponse);
//                    dismissProgress();
//                    onBackPressed();
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
//                    Log.w("onFailure",rawJsonData);
//                    dismissProgress();
//                    Flashbar flashbar = backgroundDrawable();
//                    flashbar.show();
//                }
//
//                @Override
//                protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
//                    return null;
//                }
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        return this;
    }

    private boolean isPost(){
        return mSmTagFeed!=null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.btnPost){
            if (mFileSelect.size()==0 && (txtPostFeed.getText().toString().equals("") || txtPostFeed.getText()==null)){
                Flashbar flashbar = backgroundDrawable();
                flashbar.show();
            }else{
                showProgress();
                if (this.postFeed().isPost()){
                    onBackPressed();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }
}

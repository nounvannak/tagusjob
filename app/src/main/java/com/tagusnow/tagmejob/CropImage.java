package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.io.File;

public class CropImage extends AppCompatActivity {
    private static final String TAG = CropImage.class.getSimpleName();
    private static final String KEY_SOURCE_URI = "SourceUri";
    private Uri uri = null;

    public static Intent createIntent(Activity activity) {
        return new Intent(activity, CropImage.class);
    }

    public static Intent createIntent(Activity activity, Uri uri){
        Intent intent = new Intent(activity,CropImage.class);
        intent.setData(uri);
        return intent;
    }

    // Lifecycle Method ////////////////////////////////////////////////////////////////////////////

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        File file = new File(intent.getStringExtra(KEY_SOURCE_URI));
        uri = Uri.fromFile(file);

        setContentView(R.layout.activity_crop_image);

//        uri = getIntent().getData();

        if (savedInstanceState!=null){
            savedInstanceState.putParcelable(KEY_SOURCE_URI,uri);
        }

        if(savedInstanceState == null){
//            Log.w(TAG,intent.getStringExtra(KEY_SOURCE_URI));
            getSupportFragmentManager().beginTransaction().add(R.id.container_crop_image, CropImageFragment.newInstance(uri)).commit();
        }
        initToolbar();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_SOURCE_URI,uri);
    }

    @Override public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_crop_image);
        toolbar.setTitle("Crop Image");
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSupportNavigateUp();
            }
        });
    }

    private void changeCover(){

    }

    public void startResultActivity(Uri uri) {
        if (isFinishing()) return;
        // Start ResultActivity
//        startActivity(ProfileActivity.createIntent(this, uri));
        Intent intent = ProfileActivity.createIntent(this,uri);
        intent.putExtra("cover",uri);
        setResult(RESULT_OK,intent);
        finish();
    }
}

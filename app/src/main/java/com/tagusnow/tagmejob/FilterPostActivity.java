package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.adapter.FeedAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.LoadingView;

import java.util.ArrayList;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import cz.msebera.android.httpclient.Header;

public class FilterPostActivity extends AppCompatActivity  implements View.OnFocusChangeListener, SearchView.OnQueryTextListener,SwipeRefreshLayout.OnRefreshListener, LoadingListener<LoadingView> {

    private static final String TAG = FilterPostActivity.class.getSimpleName();
    private static final int REQUEST_FILTER = 8350;
    private int category_id = 0;
    private int location = 0;
    private String sLocation;
    private String search;
    private SmUser authUser;
    private SmTagSubCategory category;
    private FeedResponse feedResponse;
    private FeedAdapter feedAdapter;
    private Toolbar toolbar;
    private MenuItem mMenuItem;
    private LinearLayout no_result,layoutTag;
    private SwipeRefreshLayout refresh;
    private RecyclerView recyclerFilterPost;
    RecyclerView.LayoutManager mLayoutManager;
    private EditText txtSearch;
    private SearchView searchView;
    private TagContainerLayout searchFilterTag;
    private List<String> tags = new ArrayList<>();
    int lastPage = 0;
    boolean isRequest = true;
    private LoadingView loadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_post);
        initTemp();
        initView();
        condView();
        resposePost();
        onScroll();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initTemp();
        onScroll();
        initSearchFilterTag();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTemp();
        onScroll();
        initSearchFilterTag();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initTemp();
        onScroll();
        initSearchFilterTag();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,FilterPostActivity.class);
    }

    private void initTemp(){
        authUser = new Auth(this).checkAuth().token();
        category = new Repository(this).restore().getCategory();
        category_id = getIntent().getIntExtra("category",0);
    }

    private String getToolbarTitle(){
        String cate = "";
        for (int i=0;i < category.getData().size();i++){
            if (category.getData().get(i).getId()==category_id){
                cate = category.getData().get(i).getTitle();
            }
        }
        return cate;
    }

    private void initView(){
        initToolbar();
        initRecycler();
        searchView = (SearchView)findViewById(R.id.searchView);
        searchView.setQueryHint("Search here...");
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextFocusChangeListener(this);
        searchView.setOnQueryTextListener(this);
        no_result = (LinearLayout)findViewById(R.id.no_result);
        layoutTag = (LinearLayout)findViewById(R.id.layoutTag);
        refresh = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refresh.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        refresh.setOnRefreshListener(this);
        initSearchFilterTag();
    }

    private void initRecycler(){
        recyclerFilterPost = (RecyclerView)findViewById(R.id.recyclerFilterPost);
        recyclerFilterPost.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerFilterPost.setLayoutManager(mLayoutManager);
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getToolbarTitle());
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void condView(){

    }

    private void filter(){
        startActivityForResult(FilterSearchActivity.createIntent(this).putExtra("isCategory",false),REQUEST_FILTER);
    }

    private void onScroll(){
        /*recycler view event*/
        recyclerFilterPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    nextFeed(feedResponse.getNext_page_url());
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void next(FeedResponse smTagFeed){
        feedAdapter.update(this,this.feedResponse,this.authUser).notifyDataSetChanged();
        if (feedAdapter.getItemCount()==0){
            no_result.setVisibility(View.VISIBLE);
        }else {
            no_result.setVisibility(View.GONE);
        }
    }

    private void initCard(){
        feedAdapter = new FeedAdapter(this,this.feedResponse,this.authUser,this);
        if (feedAdapter.getItemCount()==0){
            no_result.setVisibility(View.VISIBLE);
        }else {
            no_result.setVisibility(View.GONE);
        }
        recyclerFilterPost.setAdapter(feedAdapter);
    }

    private void resposePost(){
        AsyncHttpClient mFeedClient = new AsyncHttpClient();
        RequestParams mFeedParams = new RequestParams();
        if (authUser.getId() > 0){
            mFeedParams.put("auth_user",authUser.getId());
        }
        mFeedParams.put("category_id",category_id);
        if (location!=0){
            mFeedParams.put("location",location);
        }
        if (search!=null){
            mFeedParams.put("search_query",search);
        }
        mFeedParams.put(SuperConstance.LIMIT,SuperConstance.LIMIT_VALUE);
        mFeedClient.post(SuperUtil.getBaseUrl(SuperConstance.FILTER_POST),mFeedParams, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                try {
                    Log.w(TAG,rawJsonResponse);
                    feedResponse = new Gson().fromJson(rawJsonResponse,FeedResponse.class);
                    initCard();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w("onFailure",rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    public void nextFeed(String resposeUrl){
        AsyncHttpClient mFeedClient = new AsyncHttpClient();
        RequestParams mFeedParams = new RequestParams();
        if (authUser.getId() > 0){
            mFeedParams.put("auth_user",authUser.getId());
        }
        mFeedParams.put("category_id",category_id);
        if (location!=0){
            mFeedParams.put("location",location);
        }
        if (search!=null){
            mFeedParams.put("search_query",search);
        }
        mFeedParams.put(SuperConstance.LIMIT,SuperConstance.LIMIT_VALUE);
        mFeedClient.post(resposeUrl,mFeedParams, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                Log.w("mSmTagFeed",rawJsonResponse);
                try {
                    feedResponse = new Gson().fromJson(rawJsonResponse,FeedResponse.class);
                    next(feedResponse);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w("onFailure",rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQUEST_FILTER && resultCode==RESULT_OK && data!=null){
            location = data.getIntExtra("location",0);
            sLocation = data.getStringExtra("location_name");
            if (sLocation!=null){
                tags.add(sLocation);
            }
            searchFilterTag.setTags(tags);
            layoutTag.setVisibility(View.VISIBLE);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initSearchFilterTag(){
        searchFilterTag = (TagContainerLayout)findViewById(R.id.searchFilterTag);
        if (tags.size() > 0){
            layoutTag.setVisibility(View.VISIBLE);
        }else{
            layoutTag.setVisibility(View.GONE);
        }
        searchFilterTag.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {

            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {
                if (tags.size()==1){
                    tags.remove(position);
                    layoutTag.setVisibility(View.GONE);
                }else if(tags.size() > 1){
                    tags.remove(position);
                    searchFilterTag.setTags(tags);
                    layoutTag.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter_post,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.btnFilter:
                filter();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query!=null){
            search = query;
        }
        resposePost();
        onScroll();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        search = newText;
        resposePost();
        onScroll();
        return false;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b){
            searchView.onActionViewCollapsed();
            searchView.setQuery("",false);
        }else{
            resposePost();
            onScroll();
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                resposePost();
                onScroll();
                new Task().execute();
            }
        },2000);
    }

    @Override
    public void reload(LoadingView view) {

    }

    private class Task extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            refresh.setRefreshing(false);
            super.onPostExecute(aVoid);
        }
    }
}

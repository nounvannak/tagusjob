package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;

import java.util.ArrayList;

public class FilterSearchActivity extends AppCompatActivity implements View.OnClickListener{

    private final static String TAG = FilterSearchActivity.class.getSimpleName();
    private final static int REQUEST_CATEGORY = 798;
    private final static int REQUEST_LOCATION = 799;
    private Toolbar toolbarFilter;
    private RelativeLayout selectCategory,selectLocation;
    private LinearLayout layout_category;
    private TextView txtCategory,txtLocation;
    private int mCategory = 0;
    private int mLocation = 0;
    private String sCategory;
    private String sLocation;
    private boolean isCategory = true;
    private SmTagSubCategory category;
    private ArrayList<SmLocation.Data> listLocation = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_search);
        gotIntent();
        initView();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        gotIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gotIntent();
    }

    @Override
    protected void onStart() {
        super.onStart();
        gotIntent();
    }

    private void gotIntent(){
        category = new Repository(this).restore().getCategory();
        isCategory = getIntent().getBooleanExtra("isCategory",true);
    }

    private void initView(){
        initToolbar();
        selectCategory = (RelativeLayout)findViewById(R.id.selectCategory);
        selectLocation = (RelativeLayout)findViewById(R.id.selectLocation);
        layout_category = (LinearLayout)findViewById(R.id.layout_category);
        txtCategory = (TextView)findViewById(R.id.txtCategory);
        txtLocation = (TextView)findViewById(R.id.txtLocation);
        selectCategory.setOnClickListener(this);
        selectLocation.setOnClickListener(this);
        if (!isCategory){
            layout_category.setVisibility(View.GONE);
        }
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,FilterSearchActivity.class);
    }

    private void initToolbar(){
        toolbarFilter = (Toolbar)findViewById(R.id.toolbarFilterSearch);
        setSupportActionBar(toolbarFilter);
        toolbarFilter.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra("category",mCategory);
        intent.putExtra("category_name",sCategory);
        intent.putExtra("location",mLocation);
        intent.putExtra("location_name",sLocation);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_CATEGORY && resultCode==RESULT_OK && data!=null){
            mCategory = data.getIntExtra("category_select",0);
            setCategory();
        }else if(requestCode==REQUEST_LOCATION && resultCode==RESULT_OK && data!=null){
            mLocation = data.getIntExtra("city",0);
            SmLocation smLocation = new Gson().fromJson(data.getStringExtra("city_list"),SmLocation.class);
            listLocation.addAll(smLocation.getData());
            setLocation();
        }
    }

    private void setCategory(){
        if (mCategory > 0) {
            for (int i = 0; i < category.getData().size(); i++) {
                if (category.getData().get(i).getId() == mCategory) {
                    txtCategory.setText(category.getData().get(i).getTitle());
                    sCategory = category.getData().get(i).getTitle();
                }
            }
        }else {
            txtCategory.setText(R.string.select_category);
        }
    }

    private void setLocation(){
        if (mLocation > 0) {
            for (int i = 0; i < listLocation.size(); i++) {
                if (listLocation.get(i).getId() == mLocation) {
                    txtLocation.setText(listLocation.get(i).getName());
                    sLocation = listLocation.get(i).getName();
                }
            }
        }else {
            txtLocation.setText(R.string.select_location);
        }
    }

    private void selectCategory(){
        Intent intent = new Intent(this,CategoryListView.class);
        intent.putExtra("SmTagSubCategory",new Gson().toJson(category));
        startActivityForResult(intent,REQUEST_CATEGORY);
    }

    private void selectLocation(){
        Intent intent = new Intent(this,CityListView.class);
        startActivityForResult(intent,REQUEST_LOCATION);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.selectCategory:
                selectCategory();
                break;
            case R.id.selectLocation:
                selectLocation();
                break;
        }
    }
}

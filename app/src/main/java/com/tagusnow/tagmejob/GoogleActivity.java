package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.github.scribejava.core.model.Token;
import com.github.scribejava.core.oauth.OAuthService;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.fuckboilerplate.rx_social_connect.internal.services.Service;

import io.reactivex.functions.Consumer;

public class GoogleActivity extends TagUsJobActivity {

    private WebView webView;
    private static final String AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";
    public static final String KEY_RESULT = "key_result";
    public static Service<? extends Token, ? extends OAuthService> service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google);

        webView = (WebView)findViewById(R.id.webview);
        webView.getSettings().setUserAgentString(AGENT);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (service == null || !url.startsWith(service.callbackUrl())) return super.shouldOverrideUrlLoading(view, url);

                webView.setVisibility(View.GONE);

                service.oResponse(url).subscribe(new Consumer<Token>() {
                    @Override public void accept(Token token) throws Exception {
                        finishWithToken(token);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable error) throws Exception {
                        finishWithError(error);
                    }
                });

                return true;
            }

            @Override public void onPageFinished(WebView view, String url) {}
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        service.oAuthUrl().subscribe(new Consumer<String>() {
            @Override
            public void accept(String url) throws Exception {
                webView.loadUrl(url);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable error) throws Exception {
                finishWithError(error);
            }
        });
    }

    private void finishWithError(Throwable error) {
        Intent intent = new Intent();

        String message = error.getMessage();
        if (message == null && error.getCause() != null) message = error.getCause().getMessage();

        intent.putExtra(KEY_RESULT, message);
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    private void finishWithToken(Token token) {
        Intent intent = new Intent();
        intent.putExtra(KEY_RESULT, token);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}

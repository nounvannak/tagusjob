package com.tagusnow.tagmejob;

import java.util.List;

public class GoogleClientSecret {

    private Installed installed;

    public GoogleClientSecret(){
        super();
    }

    public Installed getInstalled() {
        return installed;
    }

    public void setInstalled(Installed installed) {
        this.installed = installed;
    }

    public class Installed{
        private String client_id;
        private String project_id;
        private String auth_id;
        private String token_uri;
        private String auth_provider_x509_cert_url;
        private List<String> redirect_uris;

        public Installed(){
            super();
        }

        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getAuth_id() {
            return auth_id;
        }

        public void setAuth_id(String auth_id) {
            this.auth_id = auth_id;
        }

        public String getToken_uri() {
            return token_uri;
        }

        public void setToken_uri(String token_uri) {
            this.token_uri = token_uri;
        }

        public String getAuth_provider_x509_cert_url() {
            return auth_provider_x509_cert_url;
        }

        public void setAuth_provider_x509_cert_url(String auth_provider_x509_cert_url) {
            this.auth_provider_x509_cert_url = auth_provider_x509_cert_url;
        }

        public List<String> getRedirect_uris() {
            return redirect_uris;
        }

        public void setRedirect_uris(List<String> redirect_uris) {
            this.redirect_uris = redirect_uris;
        }
    }
}

package com.tagusnow.tagmejob.Handler.Feed;

import com.tagusnow.tagmejob.Handler.Base.MainFeed;
import com.tagusnow.tagmejob.auth.SmUser;

public interface FeedHandler {

    void save(MainFeed mainFeed, SmUser user,int position);
    void update(MainFeed mainFeed, SmUser user,int position);
    void hide(MainFeed mainFeed, SmUser user,int position);
    void remove(MainFeed mainFeed, SmUser user,int position);

}

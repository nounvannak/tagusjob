package com.tagusnow.tagmejob.Handler.User;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestFace;

public interface UserHandler {
    void follow(FollowSuggestFace followSuggestFace, SmUser user, int position);
    void unfollow(FollowSuggestFace followSuggestFace,SmUser user,int position);
    void remove(FollowSuggestFace followSuggestFace,SmUser user,int position);
}

package com.tagusnow.tagmejob;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.adapter.FeedAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import cz.msebera.android.httpclient.Header;

public class HomeProfileFragment extends Fragment {
    private static final String ARG_PARAM1 = "SmUser";
    private static final String ARG_PARAM2 = "SmTagFeed";
    private static final String ARG_PARAM3 = "SmTagSubCategory";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    SmTagSubCategory smTagSubCategories;
    FeedAdapter feedAdapter;
    FeedResponse mSmTagFeed;
    SmUser authUser;
    int lastPage = 0;
    boolean isRequest = true;
    int lastTo = 0;

    public HomeProfileFragment() {
        // Required empty public constructor
    }

    public static HomeProfileFragment newInstance(SmUser authUser,SmTagSubCategory smTagSubCategory) {
        HomeProfileFragment fragment = new HomeProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, new Gson().toJson(authUser));
        args.putString(ARG_PARAM3, new Gson().toJson(smTagSubCategory));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            /*init user session*/
            String userJson = getArguments().getString(ARG_PARAM1,SuperConstance.MISSING);
            String category = getArguments().getString(ARG_PARAM3,SuperConstance.MISSING);
            authUser = new Gson().fromJson(userJson,SmUser.class);
            smTagSubCategories = new Gson().fromJson(category,SmTagSubCategory.class);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_profile, container, false);
        /*Init recycler view*/
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerProfileHome);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        resposePost(SuperUtil.getBaseUrl(SuperConstance.GET_FEED));
        onScroll();
        return view;
    }

    private void onScroll(){
        /*recycler view event*/
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (mSmTagFeed.getTo() - 1)) && (mSmTagFeed.getTo() < mSmTagFeed.getTotal())) {
                        if (mSmTagFeed.getNext_page_url()!=null || !mSmTagFeed.getNext_page_url().equals("")){
                            if (lastPage!=mSmTagFeed.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = mSmTagFeed.getCurrent_page();
                                    nextFeed(mSmTagFeed.getNext_page_url());
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void next(FeedResponse smTagFeed){
        feedAdapter.update(getActivity(),this.mSmTagFeed,this.authUser).notifyDataSetChanged();
    }

    private void initCard(){
//        feedAdapter = new FeedAdapter(getActivity(),this.mSmTagFeed,this.authUser);
//        mRecyclerView.setAdapter(feedAdapter);
    }

    private void resposePost(String resposeUrl){
        AsyncHttpClient mFeedClient = new AsyncHttpClient();
        RequestParams mFeedParams = new RequestParams();
        if (authUser.getId() > 0){
            mFeedParams.put("auth_user",authUser.getId());
            mFeedParams.put("profile_user",authUser.getId());
        }
        mFeedParams.put(SuperConstance.LIMIT,SuperConstance.LIMIT_VALUE);
        mFeedClient.post(resposeUrl,mFeedParams, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                try {
                    mSmTagFeed = new Gson().fromJson(rawJsonResponse,FeedResponse.class);
                    initCard();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {

            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    public void nextFeed(String resposeUrl){
        AsyncHttpClient mFeedClient = new AsyncHttpClient();
        RequestParams mFeedParams = new RequestParams();
        if (authUser.getId() > 0){
            mFeedParams.put("auth_user",authUser.getId());
            mFeedParams.put("profile_user",authUser.getId());
        }
        mFeedParams.put(SuperConstance.LIMIT,SuperConstance.LIMIT_VALUE);
        mFeedClient.post(resposeUrl,mFeedParams, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                try {
                    mSmTagFeed = new Gson().fromJson(rawJsonResponse,FeedResponse.class);
                    next(mSmTagFeed);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }
}

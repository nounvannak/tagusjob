package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.JobDetailView;
import com.tagusnow.tagmejob.view.JobDetailViewAction;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class JobDetail extends TagUsJobActivity implements JobDetailViewAction {

    private static final String TAG = JobDetail.class.getSimpleName();
    public static final String FEED = "feed_id";
    public static final String USER = "user_id";
    private Toolbar toolbar;
    private JobDetailView jobDetailView;
    private int feed_id,user_id;
    private SmUser user;
    private Feed feed;
    /*Service*/
    private JobsService service = ServiceGenerator.createService(JobsService.class);
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        init();
    }

    private void init(){
        initTemp();
        initView();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,JobDetail.class);
    }

    private void initTemp(){
        this.feed = (Feed) getIntent().getSerializableExtra(FEED);
        this.user_id = getIntent().getIntExtra(USER,0);
    }

    private void initView(){
        /*Toolbar*/
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        jobDetailView = (JobDetailView)findViewById(R.id.jobDetailView);
        jobDetailView.setActivity(this);
        jobDetailView.Feed(this.feed);
        jobDetailView.setJobDetailViewAction(this);
    }

    private void ApplyJob() {
        if (this.feed==null){
            Toast.makeText(this,"No Feed",Toast.LENGTH_SHORT).show();
        }
        startActivity(ApplyJobActivity.createIntent(this).putExtra(ApplyJobActivity.FEED,this.feed));
    }

    @Override
    public void onApply(Feed feed) {
        this.ApplyJob();
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.RepositoryService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.JobListAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.v3.category.SubcribeListener;
import com.tagusnow.tagmejob.v3.profile.AuthProfileActivity;
import com.tagusnow.tagmejob.view.Subcribe.SubcribeLayout;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import java.io.IOException;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobListActivity extends TagUsJobActivity implements SubcribeListener<SubcribeLayout> {

    private static final String TAG = JobListActivity.class.getSimpleName();
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private SmUser user;
    private JobListAdapter adapter;
    private List<Category> categories;
    private RepositoryService service = ServiceGenerator.createService(RepositoryService.class);
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);
    private Callback<Object> UNSUBSCRIBE = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.isSuccessful()){
                Log.d(TAG,response.message());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Object> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Object> SUBSCRIBE = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.isSuccessful()){
                Log.d(TAG,response.message());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Object> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void InitTemp(){
        this.user = new Auth(this).checkAuth().token();
    }

    private void CategoryByUser(){
        service.CategoryByUser(this.user.getId()).enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()){
                    Setup(response.body());
                }else {
                    try {
                        ShowAlert("Error",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void InitUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this,2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
    }

    private void Setup(List<Category> categories){
        this.categories = categories;
        this.adapter = new JobListAdapter(this,categories,this);
        recyclerView.setAdapter(this.adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_list);
        InitTemp();
        InitUI();
        CategoryByUser();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,JobListActivity.class);
    }

    @Override
    public void subcribe(SubcribeLayout view, Category category) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(category.isSubscribe() ? "Unsubscribe" : "Subscribe").setMessage(category.isSubscribe() ? "Do you want to unsubscribe this position?" : "Do you want to subscribe this position?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (category.isSubscribe()){
                    for (Category category1: categories){
                        if (category1.getId() == category.getId()){
                            int index = categories.indexOf(category);
                            category1.setSubscribe(0);
                            categories.set(index,category1);
                            adapter.notifyDataSetChanged();
                        }
                    }
                    feedService.JobUnsubscribe(user.getId(),category.getId()).enqueue(UNSUBSCRIBE);
                }else {
                    for (Category category1: categories){
                        if (category1.getId() == category.getId()){
                            int index = categories.indexOf(category);
                            category1.setSubscribe(1);
                            categories.set(index,category1);
                            adapter.notifyDataSetChanged();
                        }
                    }
                    feedService.JobSubscribe(user.getId(),category.getId()).enqueue(SUBSCRIBE);
                }
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void position(SubcribeLayout view, Category category) {
        view.OpenFilterCategory(category);
    }
}

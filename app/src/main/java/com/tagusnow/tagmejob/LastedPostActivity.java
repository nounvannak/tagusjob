package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.CV.Adapter.LastedResumeAdapter;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LastedPostActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    public static final String CITY = "city";
    public static final String CATEGORY = "category";
    public static final String CITY_NAME = "city_name";
    public static final String CATEGORY_NAME = "category_name";
    private SmUser user;
    private int city;
    private int category;
    private String city_name;
    private String category_name;
    private SwipeRefreshLayout refreshLayout;
    private Toolbar toolbar;
    private RecyclerView recycler;
    private TextView title,sub_title;
    private Spinner spnCategory,spnCity;
    private Button btnSearch;
    private LinearLayout layout_setting;
    private SmLocation cityObj;
    private SmTagSubCategory categoryObj;
    private RelativeLayout layout_info;
    private FeedResponse feedResponse;
    private LastedResumeAdapter adapter;
    private FeedService service = ServiceGenerator.createService(FeedService.class);

    private void First(){
        service.Search(this.user.getId(),2,this.category,this.city,10).enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()){
                    First(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void First(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.CheckNext();
        this.adapter = new LastedResumeAdapter(this,feedResponse);
        this.recycler.setAdapter(this.adapter);
        this.Setup();
    }

    private void Next(){
        int page = this.feedResponse.getCurrent_page() + 1;
        service.Search(this.user.getId(),2,this.category,this.city,10,page).enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()){
                    Next(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void Next(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.adapter.Update(feedResponse).notifyDataSetChanged();
    }

    private void CheckNext(){
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    Next();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lasted_post);
        this.initTemp();
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        title = (TextView)findViewById(R.id.title);
        sub_title = (TextView)findViewById(R.id.sub_title);
        layout_info = (RelativeLayout)findViewById(R.id.layout_info);
        layout_setting = (LinearLayout)findViewById(R.id.layout_setting);
        spnCategory = (Spinner)findViewById(R.id.spnCategory);
        spnCity = (Spinner)findViewById(R.id.spnCity);
        btnSearch = (Button)findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);
        recycler = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this,2,LinearLayoutManager.VERTICAL,false);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(manager);

        if (this.cityObj!=null){
            List<String> listCity = new ArrayList<String>();
            listCity.add("All");
            for (SmLocation.Data data : this.cityObj.getData()){
                listCity.add(data.getName());
            }
            spnCity.setAdapter(new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,listCity));
            spnCity.setSelection(listCity.indexOf(city_name));
        }

        if (this.categoryObj!=null){
            List<String> listCategory = new ArrayList<String>();
            listCategory.add("All");
            for (Category data : this.categoryObj.getData()){
                listCategory.add(data.getTitle());
            }
            spnCategory.setAdapter(new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,listCategory));
            spnCategory.setSelection(listCategory.indexOf(category_name));
        }

        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){
                    city = cityObj.getData().get(position - 1).getId();
                    city_name = cityObj.getData().get(position - 1).getName();
                }else {
                    city = position;
                    city_name = "All";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){
                    category = categoryObj.getData().get(position - 1).getId();
                    category_name = categoryObj.getData().get(position - 1).getTitle();
                }else {
                    category = position;
                    category_name = "All";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        this.Setup();
        onRefresh();

    }

    private void Setup(){
        if (layout_setting.getVisibility()==View.VISIBLE){
            layout_setting.setVisibility(View.GONE);
            layout_info.setVisibility(View.VISIBLE);
        }else {
            layout_setting.setVisibility(View.VISIBLE);
            layout_info.setVisibility(View.GONE);
        }
        title.setText("New "+this.category_name+" resume in "+this.city_name);
        sub_title.setText("Based on your info");
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,LastedPostActivity.class);
    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.city = getIntent().getIntExtra(CITY,0);
        this.category = getIntent().getIntExtra(CATEGORY,0);
        this.city_name =  getIntent().getStringExtra(CITY_NAME);
        this.category_name = getIntent().getStringExtra(CATEGORY_NAME);
        this.categoryObj = new Repository(this).restore().getCategory();
        this.cityObj = new Repository(this).restore().getLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_resume,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.btnSetting:
                    this.Setup();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        new Handler().postDelayed(mRun,2000);
    }

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(false);
            First();
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        if (v==btnSearch){
            First();
        }
    }
}

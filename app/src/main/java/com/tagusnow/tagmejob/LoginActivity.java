package com.tagusnow.tagmejob;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.gson.Gson;
import com.jaychang.slm.SocialLoginManager;
import com.jaychang.slm.SocialUser;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.Error;
import com.tagusnow.tagmejob.auth.GoogleResponse;
import com.tagusnow.tagmejob.auth.Helper;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.Token;
import com.tagusnow.tagmejob.auth.ValidUser;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.UserTypeActivity;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import org.fuckboilerplate.rx_social_connect.RxSocialConnect;
import java.io.IOException;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import studios.codelight.smartloginlibrary.LoginType;
import studios.codelight.smartloginlibrary.SmartLogin;
import studios.codelight.smartloginlibrary.SmartLoginCallbacks;
import studios.codelight.smartloginlibrary.SmartLoginConfig;
import studios.codelight.smartloginlibrary.SmartLoginFactory;
import studios.codelight.smartloginlibrary.UserSessionManager;
import studios.codelight.smartloginlibrary.users.SmartUser;
import studios.codelight.smartloginlibrary.util.SmartLoginException;

public class LoginActivity extends TagUsJobActivity implements View.OnClickListener,SmartLoginCallbacks {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 1001;
    private EditText email;
    private EditText password;
    private TextView btnRegister;
    private Button btnLogin;
    private ImageButton loginFacebook;
    private ImageButton loginGoogle;
    private SmartLoginConfig config;
    private SmartUser currentUser;
    private SmartLogin smartLogin;
    private SocialUser socialUser;
    private SmUser user;
    private Helper helper;
    private GoogleSignInClient mGoogleSignInClient;
    /*Service*/
    private AuthService service = ServiceGenerator.createService(AuthService.class);

    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            Log.w(TAG,"onDenied");
        }
    };

    private Callback<Object> AUTH = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.isSuccessful()){
                GoogleResponse user = new Gson().fromJson(new Gson().toJson(response.body()),GoogleResponse.class);
                Log.e("user",new Gson().toJson(user));
                SocialUser socialUser = new SocialUser();
                socialUser.userId = user.getId();
                socialUser.photoUrl = user.getPicture();
                socialUser.profile = new SocialUser.Profile();
                socialUser.profile.fullName = user.getName();
                socialUser.profile.name = user.getName();
                socialUser.profile.pageLink = user.getLink();
                if (user.getEmail()!=null){
                    socialUser.profile.email = user.getEmail();
                }
                LoginSocial(socialUser);
            }else {
//                dismissProgress();
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Object> call, Throwable t) {
//            dismissProgress();
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private Callback<Token> LOGIN = new Callback<Token>() {
        @Override
        public void onResponse(Call<Token> call, Response<Token> response) {
            if (response.isSuccessful()){
                GetUser(response.body());
            }else {
//                dismissProgress();
                try {
                    Error error = new Gson().fromJson(response.errorBody().string(),Error.class);
                    ShowAlert("Login",error.getError());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Token> call, Throwable t) {
//            dismissProgress();
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private Callback<ValidUser> VALID = new Callback<ValidUser>() {
        @Override
        public void onResponse(Call<ValidUser> call, Response<ValidUser> response) {
            if (response.isSuccessful()){
                if (socialUser.profile.email==null){
                    service.loginWeb("","",socialUser.userId,SuperConstance.PLATFORM_FACEBOOK).enqueue(LOGIN);
                }else {
                    service.loginWeb(socialUser.profile.email,SuperConstance.PLATFORM_FACEBOOK).enqueue(LOGIN);
                }
            }else {
                if (socialUser.profile.email==null){
//                    dismissProgress();
                    ShowAlert("Error!","No email.");
                }else {
                    service.registerUser(SuperConstance.PLATFORM_FACEBOOK,socialUser.profile.email,socialUser.profile.fullName,"","","").enqueue(REGISTER);
                }
            }
        }

        @Override
        public void onFailure(Call<ValidUser> call, Throwable t) {
//            dismissProgress();
            t.printStackTrace();
        }
    };

    private Callback<SmUser> REGISTER = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                user = response.body();
                currentUser = GetSmartUser(user);
            }else {
//                dismissProgress();
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
//            dismissProgress();
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private void refreshLayout() {
        currentUser = UserSessionManager.getCurrentUser(this);
        user = new Auth(this).checkAuth().token();
        if (user!=null) {
            if (user.getUser_type()==SmUser.NO_TYPE){
                startActivity(UserTypeActivity.createIntent(this));
            }else {
                startActivity(CategoryDrawer.createIntent(this));
            }
        } else {
//            dismissProgress();
            Log.e("Login","User not found");
        }
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        helper = new Helper(LoginActivity.this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);

        config = new SmartLoginConfig(this,this);
        config.setFacebookAppId(getString(R.string.facebook_app_id));
        config.setFacebookPermissions(SmartLoginConfig.getDefaultFacebookPermissions());
        config.setGoogleApiClient(mGoogleSignInClient.asGoogleApiClient());

        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        btnRegister = (TextView)findViewById(R.id.btnSignup);

        loginFacebook = (ImageButton) findViewById(R.id.btnFacebookLogin);
        loginGoogle = (ImageButton)findViewById(R.id.btnGoogleLogin);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        loginGoogle.setOnClickListener(this);
        loginFacebook.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (smartLogin != null) {
            smartLogin.onActivityResult(requestCode, resultCode, data, config);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    @SuppressLint("CheckResult")
    @Override
    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET})
    public void onClick(View view) {
//        showProgress();
        if (view==loginFacebook){
            loginByFacebook();
        }else if (view==loginGoogle){
            RxSocialConnect.with(LoginActivity.this, helper.googleService())
                    .subscribe(response -> showResponse(response.token()),
                            error -> helper.showError(error));
        }else if (view==btnLogin){
            smartLogin = SmartLoginFactory.build(LoginType.CustomLogin);
            smartLogin.login(config);
        }else if (view==btnRegister){
            startActivity(RegisterActivity.createIntent(this));
        }
    }

    private void showResponse(OAuth2AccessToken token) {
        AuthService service = ServiceGenerator.createServices(AuthService.class);
        Log.e("token",String.valueOf(token));
        service.AuthGoogle(token.getTokenType() + " " + token.getAccessToken(),"application/json").enqueue(AUTH);
    }

    @Override
    public void onLoginSuccess(SmartUser user) {
        Log.e(TAG,String.valueOf(user.getEmail()!=null));
        this.LoginSocial(user);
    }

    @Override
    public void onLoginFailure(SmartLoginException e) {
        Log.e("Login failure",e.getMessage());
    }

    private void GetUser(Token token){
        service.Authenticate(token.getToken()).enqueue(new Callback<SmUser>() {
            @Override
            public void onResponse(Call<SmUser> call, Response<SmUser> response) {
                if (response.isSuccessful()){
                    user = response.body();
                    currentUser = GetSmartUser(user);
                }else {
//                    dismissProgress();
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SmUser> call, Throwable t) {
//                dismissProgress();
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private SmartUser GetSmartUser(SmUser user){
        showProgress();
        SmartUser smartUser = new SmartUser();
        smartUser.setEmail(user.getEmail());
        smartUser.setBirthday(user.getDob()!=null ? user.getDob() : "");
        smartUser.setProfileLink(user.getProfile_link()!=null ? user.getProfile_link() : "");
        smartUser.setUserId(user.getProfile_id()!=null ? user.getProfile_id() : "");
        smartUser.setUsername(user.getUser_name()!=null ? user.getUser_name() : "");
        smartUser.setFirstName(user.getFirs_name()!=null ? user.getFirs_name() : "");
        smartUser.setLastName(user.getLast_name()!=null ? user.getLast_name() : "");
        new Auth(this).save(user);
        this.user = user;
        refreshLayout();
        return smartUser;
    }

    private void LoginWeb(){
        if (password.getText().toString().length() > 0){
            service.loginWeb(email.getText().toString(),password.getText().toString(),SuperConstance.PLATFORM_WEB).enqueue(LOGIN);
        }else {
            service.loginWeb(email.getText().toString(),SuperConstance.PLATFORM_WEB,1).enqueue(LOGIN);
        }
    }

    private void LoginSocial(SmartUser user){
        Log.e(TAG,new Gson().toJson(user));
        this.currentUser = user;
        if (user!=null){
            if (user.getEmail()!=null){
                service.validUser(SuperConstance.PLATFORM_FACEBOOK,user.getEmail(),user.getUserId()).enqueue(VALID);
            }else{
                service.validUser(SuperConstance.PLATFORM_FACEBOOK,"",user.getUserId()).enqueue(VALID);
            }
        }else {
            Log.e(TAG,"SmartUser is null");
        }
    }

    private void LoginSocial(SocialUser user){
        Log.e(TAG,new Gson().toJson(user));
        this.socialUser = user;
        if (user!=null){
            if (user.profile.email!=null){
                service.validUser(SuperConstance.PLATFORM_FACEBOOK,user.profile.email,user.userId).enqueue(VALID);
            }else{
                service.validUser(SuperConstance.PLATFORM_FACEBOOK,"",user.userId).enqueue(VALID);
            }
        }else {
            Log.e(TAG,"SmartUser is null");
        }
    }

    private void loginByFacebook() {
        SocialLoginManager.getInstance(this)
                .facebook()
                .login()
                .subscribe(this::LoginSocial,
                        error -> {
                            ShowAlert("Facebook",error.getMessage());
                        });
    }

    private boolean isValid(){
        boolean isValid = true;
        if (email.getText().toString().length() == 0 || !email.getText().toString().contains("@")){
            isValid = false;
        }

        if (password.getText().toString().length() == 0 || password.getText().toString().length() < 6){
            isValid = false;
        }
        return isValid;
    }

    @Override
    public SmartUser doCustomLogin() {
        if (isValid()){
            this.LoginWeb();
        }else {
//            dismissProgress();
            ShowAlert("Login","Email and password not valid.");
        }
        return currentUser;
    }

    @Override
    public SmartUser doCustomSignup() {
        return null;
    }
}


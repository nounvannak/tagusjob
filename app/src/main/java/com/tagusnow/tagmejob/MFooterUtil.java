package com.tagusnow.tagmejob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener;
import android.view.Menu;
import android.view.MenuItem;

public class MFooterUtil{

    public static int getSelectedItem(BottomNavigationView bottomNavigationView){
        Menu menu = bottomNavigationView.getMenu();
        for (int i=0;i<bottomNavigationView.getMenu().size();i++){
            MenuItem menuItem = menu.getItem(i);
            if (menuItem.isChecked()){
                return menuItem.getItemId();
            }
        }
        return 0;
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;

public class MoreFragment extends DialogFragment implements View.OnClickListener{

    private LinearLayout btnMoreChangeProfile,btnMoreChangeCover,btnMoreCopyLink,btnLogout;
    private SmUser authUser;
    private final static String PROFILE_LINK = "tagusjob_profile_link";
    private ProfileActivity profileActivity;
    private final static String TAG = MoreFragment.class.getSimpleName();

    public MoreFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_more, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(dialogView);

        if (getArguments()!=null){
            authUser = new Gson().fromJson(getArguments().getString(SuperConstance.USER_SESSION),SmUser.class);
        }

        btnMoreChangeProfile = (LinearLayout)dialogView.findViewById(R.id.btnMoreChangeProfile);
        btnMoreChangeCover = (LinearLayout)dialogView.findViewById(R.id.btnMoreChangeCover);
        btnMoreCopyLink = (LinearLayout)dialogView.findViewById(R.id.btnMoreChangeCopyLink);
        btnLogout = (LinearLayout)dialogView.findViewById(R.id.btnLogout);

        btnMoreChangeProfile.setOnClickListener(this);
        btnMoreChangeCover.setOnClickListener(this);
        btnMoreCopyLink.setOnClickListener(this);
        btnLogout.setOnClickListener(this);

        return builder.create();
    }

    public void newInstance(Activity activity){
        this.profileActivity = (ProfileActivity) activity;
    }

    private void copyLink(){
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(PROFILE_LINK,authUser.getProfile_link());
        clipboard.setPrimaryClip(clip);
        dismiss();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id==R.id.btnMoreChangeProfile){
            this.profileActivity.adapter.getProfileHeader().getBtnEditProfilePicture().callOnClick();
            dismiss();
        }else if(id==R.id.btnMoreChangeCover){
            this.profileActivity.adapter.getProfileHeader().getBtnEditCover().callOnClick();
            dismiss();
        }else if(id==R.id.btnMoreChangeCopyLink){
            copyLink();
            Toast.makeText(getContext(),"Copied",Toast.LENGTH_SHORT).show();
        }else if (id==R.id.btnLogout){
            authUser = new Auth(getContext()).checkAuth().destroy().logout();
            dismiss();
            Log.w(TAG,authUser.getId()+"");
            if (authUser.getId()==0){
                Intent mHomeIntent = new Intent(getContext(),LoginActivity.class);
                startActivity(mHomeIntent);
            }
        }
    }
}

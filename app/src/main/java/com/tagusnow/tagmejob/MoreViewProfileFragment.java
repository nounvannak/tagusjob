package com.tagusnow.tagmejob;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.auth.SmUser;

public class MoreViewProfileFragment extends DialogFragment implements View.OnClickListener{

    private final static String TAG = MoreViewProfileFragment.class.getSimpleName();
    private final static String PROFILE_LINK = "tagusjob_profile_link";
    private final static String AUTH = "authUser";
    private final static String PROFILE = "profileUser";
    private SmUser authUser,profileUser;
    /*View*/
    private LinearLayout btnViewProfile,btnViewCover,btnReportUser,btnCopyLink,btnClose;

    public MoreViewProfileFragment() {
        // Required empty public constructor
    }
    public static MoreViewProfileFragment newInstance(SmUser authUser,SmUser profileUser) {
        MoreViewProfileFragment fragment = new MoreViewProfileFragment();
        Bundle args = new Bundle();
        args.putString(AUTH,new Gson().toJson(authUser));
        args.putString(PROFILE,new Gson().toJson(profileUser));
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_more_view_profile, null);
        initView(dialogView);
        builder.setView(dialogView);
        if (getArguments()!=null){
            authUser = new Gson().fromJson(getArguments().getString(AUTH),SmUser.class);
            profileUser = new Gson().fromJson(getArguments().getString(PROFILE),SmUser.class);
        }
        return builder.create();
    }

    private void copyLink(){
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(PROFILE_LINK,profileUser.getProfile_link());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getContext(),"Copied",Toast.LENGTH_SHORT).show();
        dismiss();
    }

    private void initView(View view){
        btnViewProfile = (LinearLayout)view.findViewById(R.id.btnViewProfile);
        btnViewCover = (LinearLayout)view.findViewById(R.id.btnViewCover);
        btnReportUser = (LinearLayout)view.findViewById(R.id.btnReportUser);
        btnCopyLink = (LinearLayout)view.findViewById(R.id.btnCopyLink);
        btnClose = (LinearLayout)view.findViewById(R.id.btnClose);

        /*Event click*/
        btnViewProfile.setOnClickListener(this);
        btnViewCover.setOnClickListener(this);
        btnReportUser.setOnClickListener(this);
        btnCopyLink.setOnClickListener(this);
        btnClose.setOnClickListener(this);
    }

    private void viewProfile(){}

    private void viewCover(){}

    private void reportUser(){}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnViewProfile:
                viewProfile();
                break;
            case R.id.btnViewCover:
                viewCover();
                break;
            case R.id.btnReportUser:
                reportUser();
                break;
            case R.id.btnCopyLink:
                copyLink();
                break;
            case R.id.btnClose:
                dismiss();
                break;
                default:
                    dismiss();
                    break;
        }
    }
}

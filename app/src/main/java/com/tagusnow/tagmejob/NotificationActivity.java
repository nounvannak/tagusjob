package com.tagusnow.tagmejob;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.tagusnow.tagmejob.REST.Service.NotificationService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.NotificationAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.feed.NotificationResponse;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    private Toolbar toolbarNotification;
    private SwipeRefreshLayout refresh;
    private ProgressBar progressBar;
    private RecyclerView recyclerNotification;
    private RecyclerView.LayoutManager mLayoutManager;
    private NotificationAdapter notificationAdapter;
    private NotificationResponse notificationResponse;
    private SmUser authUser;
    int lastPage = 0;
    boolean isRequest = true;
    private static final String TAG = NotificationActivity.class.getSimpleName();
    private NotificationService service = ServiceGenerator.createService(NotificationService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        authUser = new Auth(this).checkAuth().token();
        initView();
        responseData();
    }

    private void onScroll(){
        /*recycler view event*/
        recyclerNotification.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (notificationResponse.getTo() - 1)) && (notificationResponse.getTo() < notificationResponse.getTotal())) {
                        if (notificationResponse.getNext_page_url()!=null || !notificationResponse.getNext_page_url().equals("")){
                            if (lastPage!=notificationResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = notificationResponse.getCurrent_page();
                                    nextFeed(notificationResponse.getNext_page_url());
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void nextFeed(String next_page_url) {
        int page = notificationResponse.getCurrent_page() + 1;
        service.getNotificationList(authUser.getId(),10,page).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                if (response.isSuccessful()){
                    next(response.body());
                }else {
                    try {
                        call.execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void next(NotificationResponse notificationResponse) {
        notificationAdapter.with(notificationResponse).setActivity(this).notifyDataSetChanged();
    }

    private void initView(){
        initToolbar();
        progressBar = (ProgressBar)findViewById(R.id.progress);
        refresh = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refresh.setOnRefreshListener(this);
        refresh.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        recyclerNotification = (RecyclerView)findViewById(R.id.recyclerNotification);
        recyclerNotification.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerNotification.setLayoutManager(mLayoutManager);
        /*init scroll view notification*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        authUser = new Auth(this).checkAuth().token();
        onScroll();
    }

    @Override
    protected void onResume() {
        super.onResume();
        authUser = new Auth(this).checkAuth().token();
        onScroll();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        authUser = new Auth(this).checkAuth().token();
        onScroll();
    }

    private void initToolbar(){
        toolbarNotification = (Toolbar)findViewById(R.id.toolbarNotification);
        toolbarNotification.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void responseData(){
        AsyncHttpClient syncHttpClient = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("auth_user",authUser.getId());
        service.getNotificationList(authUser.getId(),10).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                if (response.isSuccessful()){
                    notificationResponse = response.body();
                    initCard();
                }else {
                    try {
                        call.execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void initCard() {
        notificationAdapter = new NotificationAdapter(this).with(this.notificationResponse).setActivity(this);
        recyclerNotification.setAdapter(notificationAdapter);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                responseData();
                onScroll();
                new Async().execute();
            }
        }, 2000);
    }

    private class Async extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            refresh.setRefreshing(false);
            super.onPostExecute(aVoid);
        }
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostJobActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = PostJobActivity.class.getSimpleName();
    public static final String FEED = "Feed";
    private final static int REQ_PRIVACY = 4939;
    private final static int REQUEST_SELECT_CATEGORY = 4234;
    private Toolbar toolbar;
    private String toolbarTitle;
    private CircleImageView profileCreatePost;
    private ImageView optPrivacyIcon;
    private TextView optPrivateTitle,createPostProfileName,categorySelectTitle;
    private LinearLayout buttonCategory,optionPrivacy;
    private SmUser authUser;
    private Feed feed;
    private SmTagSubCategory category;
    private boolean isNew;
    private ProfilePostFragment profilePostFragment;
    int categoryId = 0;
    int privacy = 0;
    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            Log.w(TAG,"onDenied");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_job);
        AutoPermissions.Companion.loadActivityPermissions(this, 1);
        getIntentValue();
        initView();
        init();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    private void init(){
        if (this.isNew){
            profilePostFragment = ProfilePostFragment.newInstance(authUser,category);
            showFragment(profilePostFragment);
        }else{
            profilePostFragment = ProfilePostFragment.newInstance(feed,authUser,category).setType(feed.getFeed().getType());
            showFragment(profilePostFragment);
        }
    }

    @Override
    protected void onStart() {
        getIntentValue();
        init();
        super.onStart();
    }

    @Override
    protected void onRestart() {
        getIntentValue();
        init();
        super.onRestart();
    }

    @Override
    protected void onResume() {
        getIntentValue();
        init();
        super.onResume();
    }

    private void selectPrivacy(int privacy){
        if (privacy==SuperConstance.PRIVACY_PUBLIC){
            optPrivacyIcon.setBackgroundResource(R.drawable.ic_action_language);
            optPrivateTitle.setText(R.string.publics);
        }else if(privacy==SuperConstance.PRIVACY_PRIVATE){
            optPrivacyIcon.setBackgroundResource(R.drawable.ic_action_lock);
            optPrivateTitle.setText(R.string.privates);
        }
    }

    private void selectCategory(int categoryId){
        for (int i=0;i<category.getData().size();i++){
            if (category.getData().get(i).getId()==categoryId){
                categorySelectTitle.setText(category.getData().get(i).getTitle());
            }
        }
    }

    public PostJobActivity isNew(boolean b){
        this.isNew = b;
        return this;
    }

    private void getIntentValue(){
        Intent intent = getIntent();

        if (!this.isNew){
            feed = new Gson().fromJson(intent.getStringExtra(FEED),Feed.class);
            privacy = intent.getIntExtra(SuperConstance.PRIVACY,0);
        }

        authUser = new Auth(this).checkAuth().token();
        category = new Repository(this).restore().getCategory();
    }

    public PostJobActivity setTitle(String toolbarTitle){
        this.toolbarTitle = toolbarTitle;
        return this;
    }

    public Intent createIntent(Activity activity){
        return new Intent(activity,PostJobActivity.class);
    }

    private void showFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.post_job_main_frame,fragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.btnPost:
                if (isNew){
                    profilePostFragment.postJob();
                }else{
                    profilePostFragment.editJob();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void initView(){
        toolbar = (Toolbar)findViewById(R.id.toolbar_post_job);
        toolbar.setTitle("Edit post job");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        /*button select category*/
        buttonCategory = (LinearLayout)findViewById(R.id.buttonCategory);
        buttonCategory.setOnClickListener(this);

        /*change option privacy*/
        optionPrivacy = (LinearLayout)findViewById(R.id.optionPrivacy);
        optionPrivacy.setOnClickListener(this);

        /*init option privacy*/
        optPrivacyIcon = (ImageView)findViewById(R.id.optPrivacyIcon);
        optPrivateTitle = (TextView)findViewById(R.id.optPrivacyTitle);

        /*init category option*/
        categorySelectTitle = (TextView)findViewById(R.id.categorySelectTitle);

        profileCreatePost = (CircleImageView)findViewById(R.id.profileCreatePost);
        createPostProfileName = (TextView)findViewById(R.id.createPostProfileName);

        if (authUser!=null){
            createPostProfileName.setText(authUser.getUser_name());
            if (authUser.getImage().contains("http")){
                SuperUtil.setImageResource(profileCreatePost,authUser.getImage());
            }else{
                SuperUtil.setImageResource(profileCreatePost,SuperUtil.getProfilePicture(authUser.getImage(),"200"));
            }
        }

        selectPrivacy(privacy);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQ_PRIVACY && resultCode==RESULT_OK && data!=null){
            privacy = data.getIntExtra(SuperConstance.PRIVACY,0);
            selectPrivacy(privacy);
        }else if (requestCode==REQUEST_SELECT_CATEGORY && resultCode==RESULT_OK && data!=null){
            categoryId = data.getIntExtra("category_select",-1);
            selectCategory(categoryId);
        }
    }

    private void reqCategory(){
        Intent mSelectCategory = new Intent(getApplicationContext(),CategoryListView.class);
        mSelectCategory.putExtra("SmTagSubCategory",new Gson().toJson(category));
        startActivityForResult(mSelectCategory,REQUEST_SELECT_CATEGORY);
    }

    private void reqPrivacy(){
        Intent privacyIntent = new Intent(getApplicationContext(),PrivacyActivity.class);
        privacyIntent.putExtra(SuperConstance.PRIVACY,SuperConstance.PRIVACY_VALUE);
        startActivityForResult(privacyIntent,REQ_PRIVACY);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.optionPrivacy:
                reqPrivacy();
                break;
            case R.id.buttonCategory:
                reqCategory();
                break;
        }
    }
}

package com.tagusnow.tagmejob;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class PrivacyActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageView ivPublic,ivPrivate;
    private static final String TAG = PrivacyActivity.class.getSimpleName();
    private int position = -1;
    private int privacy = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbarPrivacy);
        toolbar.setTitle("Select Privacy");
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        /*option of privacy*/
        LinearLayout optionPublic = (LinearLayout)findViewById(R.id.optionPublic);
        optionPublic.setOnClickListener(this);

        LinearLayout optionPrivate = (LinearLayout)findViewById(R.id.optionPrivate);
        optionPrivate.setOnClickListener(this);

        /*image view of selected*/
        ivPublic = (ImageView)findViewById(R.id.ivPublic);
        ivPrivate = (ImageView)findViewById(R.id.ivPrivate);
        position = getIntent().getIntExtra("position",-1);
        initSelectedPrivacy(ivPublic,ivPrivate,getIntent().getIntExtra(SuperConstance.PRIVACY,SuperConstance.PRIVACY_VALUE));
    }

    private void initSelectedPrivacy(ImageView ivPublic,ImageView ivPrivate,int privacyValue){
        if (privacyValue==SuperConstance.PRIVACY_PUBLIC){
            ivPublic.setBackgroundResource(R.drawable.ic_action_check_circle);
            ivPublic.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorHeader)));
            ivPrivate.setBackgroundResource(R.drawable.ic_action_radio_button_unchecked);
            ivPrivate.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorBorder)));
        }else if(privacyValue==SuperConstance.PRIVACY_PRIVATE){
            ivPrivate.setBackgroundResource(R.drawable.ic_action_check_circle);
            ivPrivate.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorHeader)));
            ivPublic.setBackgroundResource(R.drawable.ic_action_radio_button_unchecked);
            ivPublic.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorBorder)));
        }
    }

    private void selectPrivacy(){
        Log.w(TAG,privacy+"");
        Intent intent = getIntent();
        intent.putExtra(SuperConstance.PRIVACY,SuperConstance.PRIVACY_VALUE);
        intent.putExtra("position",position);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id==R.id.optionPublic){
            SuperConstance.PRIVACY_VALUE = SuperConstance.PRIVACY_PUBLIC;
            ivPublic.setBackgroundResource(R.drawable.ic_action_check_circle);
            ivPublic.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorHeader)));
            ivPrivate.setBackgroundResource(R.drawable.ic_action_radio_button_unchecked);
            ivPrivate.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorBorder)));
            selectPrivacy();
        }else if(id==R.id.optionPrivate){
            SuperConstance.PRIVACY_VALUE = SuperConstance.PRIVACY_PRIVATE;
            ivPrivate.setBackgroundResource(R.drawable.ic_action_check_circle);
            ivPrivate.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorHeader)));
            ivPublic.setBackgroundResource(R.drawable.ic_action_radio_button_unchecked);
            ivPublic.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorBorder)));
            selectPrivacy();
        }
    }
}

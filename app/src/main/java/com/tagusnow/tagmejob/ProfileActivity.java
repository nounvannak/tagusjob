package com.tagusnow.tagmejob;

import android.Manifest;
import android.app.Activity;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import com.google.gson.Gson;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabSelectListener;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.ProfileAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.ProfileHeader;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.yalantis.ucrop.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends TagUsJobActivity implements OnTabSelectListener, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    public static final String PROFILEA_USER = "profileUser";
    private MoreFragment moreFragment;
    private SmUser authUser;
    private Toolbar toolbar;
    private EditText toolbar_search_box;
    private static final int CHANGE_PROFILE = 1894;
    private static final int CHANGE_COVER = 1893;
    private static final int OPEN_CROP = 1895;
    private static final int CROP_PROFILE = 1896;
    private static final String KEY_SOURCE_URI = "SourceUri";
    public Uri sourceUri;
    private final static String TAG = ProfileActivity.class.getSimpleName();
    ProfileAdapter adapter;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private FeedResponse feedResponse;
    private ProfileHeader profileHeader;
    private FloatingActionButton fabTop;
    private static int type;
    /*Service*/
    private AuthService service = ServiceGenerator.createService(AuthService.class);
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);
    private Callback<SmUser> mUpdateImageCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                authUser = new Auth(ProfileActivity.this).putAuth(response.body()).save();
                adapter.notifyDataSetChanged();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<SmUser> mUpdateCoverCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                authUser = new Auth(ProfileActivity.this).putAuth(response.body()).save();
                adapter.notifyDataSetChanged();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            if (type==ProfileAdapter.HOME){
                getNewFeed();
            }else {
                initNewInfo();
            }
            refreshLayout.setRefreshing(false);
        }
    };
    private Callback<FeedResponse> mNewCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> mNextCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private BottomBar barProfile;
    private BottomBarTab tabHome;
    private BottomBarTab tabInfo;
    private BottomBarTab tabMore;
    private int mParallaxImageHeight;

    private void initNewFeed(FeedResponse feedResponse){
//        feedResponse.setTo(feedResponse.getTo() + ProfileAdapter.MORE_VIEW);
//        this.feedResponse = feedResponse;
//        this.checkNext();
//        this.adapter = new ProfileAdapter(this,this.feedResponse,this.authUser,ProfileAdapter.HOME);
//        this.initBottomBar();
//        recyclerView.setAdapter(this.adapter);
    }

    private void initNextFeed(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.checkNext();
        this.adapter.update(this,this.feedResponse,this.authUser,ProfileAdapter.HOME).notifyDataSetChanged();
    }

    private void getNewFeed(){
        int auth_user = this.authUser!=null ? this.authUser.getId() : 0;
        int limit = 10;
        feedService.Profile(auth_user,auth_user,limit).enqueue(mNewCallback);
    }

    private void initNewInfo(){
        this.adapter.update(this,this.authUser,ProfileAdapter.INFO);
        recyclerView.setAdapter(this.adapter);
    }

    int lastPage = 0;
    boolean isRequest = true;
    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    getNextFeed();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void getNextFeed(){
        int auth_user = this.authUser!=null ? this.authUser.getId() : 0;
        int limit = 10;
        int page = this.feedResponse.getCurrent_page() + 1;
        feedService.Profile(auth_user,auth_user,limit,page).enqueue(mNextCallback);
    }

    private void initToolbar(){
        /*Init Toolbar*/
        toolbar = (Toolbar)findViewById(R.id.profile_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        toolbar_search_box = (EditText)findViewById(R.id.profile_toolbar_search_box);
        toolbar_search_box.setHint(getString(R.string.search_in_profile,authUser.getFirs_name()));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initFab(){
        fabTop = (FloatingActionButton)findViewById(R.id.fabTop);
        fabTop.setOnClickListener(this);
    }

    private void initView(){
        initToolbar();
        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);
        initSwipe();
        initRecycler();
        initFab();
    }

    private void initBottomBar(){
        this.profileHeader = this.adapter.getProfileHeader();
        barProfile = this.profileHeader.getBarProfile();
        tabHome = this.profileHeader.getTabHome();
        tabInfo = this.profileHeader.getTabInfo();
        tabMore = this.profileHeader.getTabMore();
        moreFragment = new MoreFragment();
        barProfile.setOnTabSelectListener(this);
    }

    private void initSwipe(){
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            refreshLayout.setColorSchemeColors(getColor(R.color.colorAccent));
        }
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy >= 0 && fabTop.getVisibility() == View.VISIBLE) {
                    fabTop.hide();
                } else if (dy < 0 && fabTop.getVisibility() != View.VISIBLE) {
                    fabTop.show();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getTemp();
        initView();
        type = ProfileAdapter.HOME;
        refreshLayout.setRefreshing(true);
        onRefresh();
    }

    private void getTemp(){
        /*user session*/
        authUser = new Auth(this).checkAuth().token();
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    public void openCover(){
        Intent intent = ChangeCoverActivity.createIntent(this,sourceUri);
        intent.putExtra(KEY_SOURCE_URI,sourceUri.getPath());
        intent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
        startActivityForResult(intent,OPEN_CROP);
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    public void openProfile(){
        Intent intent = ChangeProfileActivity.createIntent(this,sourceUri);
        intent.putExtra(KEY_SOURCE_URI,sourceUri.getPath());
        intent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
        startActivityForResult(intent,CROP_PROFILE);
    }

    public static Intent createIntent(Activity activity,Uri uri){
        Intent intent = new Intent(activity,ProfileActivity.class);
        intent.setData(uri);
        return intent;
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ProfileActivity.class);
    }

    public void showMoreFragment(){
        FragmentManager fm = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
        moreFragment.newInstance(this);
        moreFragment.setArguments(bundle);
        moreFragment.show(fm,SuperConstance.FAGMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == CHANGE_PROFILE && data!=null){
            ArrayList<String> results = data.getStringArrayListExtra("result");
            if (results.size() > 0){
                sourceUri = Uri.parse(results.get(0));
                openProfile();
            }
        }else if(resultCode == RESULT_OK && requestCode == CHANGE_COVER && data!=null){
            ArrayList<String> results = data.getStringArrayListExtra("result");
            if (results.size() > 0){
                sourceUri = Uri.parse(results.get(0));
                openCover();
            }
        }else if (resultCode == RESULT_OK && requestCode == OPEN_CROP && data!=null){
            sourceUri = data.getParcelableExtra("cover");
            requestChangeCover();
        }else if (resultCode == RESULT_OK && requestCode == CROP_PROFILE && data!=null){
            sourceUri = data.getParcelableExtra("profile");
            requestChangeProfile();
        }
    }

    private void requestChangeProfile(){
        MultipartBody.Part image = this.prepareFilePart("image",this.sourceUri);
        service.updateAccount(this.authUser.getId(),image).enqueue(mUpdateImageCallback);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        File file = new File(FileUtils.getPath(this,fileUri));
        RequestBody requestFile = RequestBody.create(MediaType.parse(Objects.requireNonNull(this.getContentResolver().getType(fileUri))),file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    private void requestChangeCover(){
        MultipartBody.Part image = this.prepareFilePart("cover",this.sourceUri);
        service.updateAccount(this.authUser.getId(),image).enqueue(mUpdateCoverCallback);
    }

    @Override
    public void onTabSelected(int tabId) {
        switch (tabId){
            case R.id.tab_home:
                type = ProfileAdapter.HOME;
                if (feedResponse==null){
                    getNewFeed();
                }
                if (fabTop.getVisibility() != View.VISIBLE){
                    fabTop.show();
                }
                break;
            case R.id.tab_info:
                /*code*/
                type = ProfileAdapter.INFO;
                fabTop.hide();
                feedResponse = null;
                initNewInfo();
                break;
            case R.id.tab_more:
                /*code*/
                showMoreFragment();
                break;
            default:
                break;
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRun,2000);
    }

    @Override
    public void onClick(View view) {
        if (view==fabTop){
            fabTop.hide();
            recyclerView.smoothScrollToPosition(0);
        }
    }
}

package com.tagusnow.tagmejob;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.Error;
import com.tagusnow.tagmejob.auth.SmUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class ProfileInfoFragment extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener {
    private static final String ARG_PARAM1 = "AuthUser";
    private static final String ARG_PARAM2 = "SmUser";
    private SmUser authUser,proUser;
    private Calendar mCalendar;
    private String mGender;
    String[] mGenderList = new String[]{"male","female"};
    private TextView info_first_name,info_last_name,info_full_name,info_dob,info_gender,info_location,info_phone;
    private ImageButton info_edit_button,button_edit_info_last_name,button_edit_info_full_name,button_edit_info_gender,button_edit_info_dob,button_edit_info_location,button_edit_info_phone;
    private ImageButton button_no_info_first_name,button_no_info_last_name,button_no_info_full_name,button_no_info_dob,button_no_info_gender,button_no_info_phone,button_no_info_location;
    private ImageButton button_yes_info_first_name,button_yes_info_last_name,button_yes_info_full_name,button_yes_info_dob,button_yes_info_gender,button_yes_info_phone,button_yes_info_location;
    private Spinner txt_info_gender;
    private EditText txt_info_first_name,txt_info_last_name,txt_info_full_name,txt_info_dob,txt_info_location,txt_info_phone;

    public ProfileInfoFragment() {
        // Required empty public constructor
    }

    public static ProfileInfoFragment newInstance(SmUser param1, SmUser param2) {
        ProfileInfoFragment fragment = new ProfileInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, new Gson().toJson(param1));
        args.putString(ARG_PARAM2, new Gson().toJson(param2));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String authUserStr = getArguments().getString(ARG_PARAM1);
            String proUserStr = getArguments().getString(ARG_PARAM2);
            authUser = new Gson().fromJson(authUserStr,SmUser.class);
            proUser = new Gson().fromJson(proUserStr,SmUser.class);
        }
    }

    private void initView(View view){
        info_first_name = (TextView)view.findViewById(R.id.info_first_name);
        info_last_name = (TextView)view.findViewById(R.id.info_last_name);
        info_full_name = (TextView)view.findViewById(R.id.info_full_name);
        info_dob = (TextView)view.findViewById(R.id.info_dob);
        info_gender = (TextView)view.findViewById(R.id.info_gender);
        info_location = (TextView)view.findViewById(R.id.info_location);
        info_phone = (TextView)view.findViewById(R.id.info_phone);
        info_edit_button = (ImageButton)view.findViewById(R.id.button_edit_info_first_name);
        txt_info_first_name = (EditText)view.findViewById(R.id.txt_info_first_name);
        txt_info_last_name = (EditText)view.findViewById(R.id.txt_info_last_name);
        txt_info_full_name = (EditText)view.findViewById(R.id.txt_info_full_name);
        txt_info_dob = (EditText)view.findViewById(R.id.txt_info_dob);
        txt_info_gender = (Spinner)view.findViewById(R.id.txt_info_gender);
        txt_info_location = (EditText)view.findViewById(R.id.txt_info_location);
        txt_info_phone = (EditText)view.findViewById(R.id.txt_info_phone);
        button_edit_info_last_name = (ImageButton)view.findViewById(R.id.button_edit_info_last_name);
        button_edit_info_full_name = (ImageButton)view.findViewById(R.id.button_edit_info_full_name);
        button_edit_info_dob = (ImageButton)view.findViewById(R.id.button_edit_info_dob);
        button_edit_info_gender = (ImageButton)view.findViewById(R.id.button_edit_info_gender);
        button_edit_info_location = (ImageButton)view.findViewById(R.id.button_edit_info_location);
        button_edit_info_phone = (ImageButton)view.findViewById(R.id.button_edit_info_phone);
        button_no_info_first_name = (ImageButton)view.findViewById(R.id.button_no_info_first_name);
        button_no_info_full_name = (ImageButton)view.findViewById(R.id.button_no_info_full_name);
        button_no_info_dob = (ImageButton)view.findViewById(R.id.button_no_info_dob);
        button_no_info_last_name = (ImageButton)view.findViewById(R.id.button_no_info_last_name);
        button_no_info_gender = (ImageButton)view.findViewById(R.id.button_no_info_gender);
        button_no_info_location = (ImageButton)view.findViewById(R.id.button_no_info_location);
        button_no_info_phone = (ImageButton)view.findViewById(R.id.button_no_info_phone);
        button_yes_info_first_name = (ImageButton)view.findViewById(R.id.button_yes_info_first_name);
        button_yes_info_full_name = (ImageButton)view.findViewById(R.id.button_yes_info_full_name);
        button_yes_info_dob = (ImageButton)view.findViewById(R.id.button_yes_info_dob);
        button_yes_info_last_name = (ImageButton)view.findViewById(R.id.button_yes_info_last_name);
        button_yes_info_gender = (ImageButton)view.findViewById(R.id.button_yes_info_gender);
        button_yes_info_location = (ImageButton)view.findViewById(R.id.button_yes_info_location);
        button_yes_info_phone = (ImageButton)view.findViewById(R.id.button_yes_info_phone);

        button_yes_info_phone.setOnClickListener(this);
        button_yes_info_location.setOnClickListener(this);
        button_yes_info_gender.setOnClickListener(this);
        button_yes_info_last_name.setOnClickListener(this);
        button_yes_info_dob.setOnClickListener(this);
        button_yes_info_full_name.setOnClickListener(this);
        button_yes_info_first_name.setOnClickListener(this);
        button_no_info_phone.setOnClickListener(this);
        button_no_info_location.setOnClickListener(this);
        button_no_info_gender.setOnClickListener(this);
        button_no_info_last_name.setOnClickListener(this);
        button_no_info_dob.setOnClickListener(this);
        button_no_info_full_name.setOnClickListener(this);
        button_no_info_first_name.setOnClickListener(this);
        button_edit_info_phone.setOnClickListener(this);
        button_edit_info_location.setOnClickListener(this);
        button_edit_info_gender.setOnClickListener(this);
        button_edit_info_dob.setOnClickListener(this);
        button_edit_info_full_name.setOnClickListener(this);
        button_edit_info_last_name.setOnClickListener(this);
        info_edit_button.setOnClickListener(this);

        List<String> listGender = new ArrayList<String>();
        listGender.add("Male");
        listGender.add("Female");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,listGender);
        txt_info_gender.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        txt_info_gender.setOnItemSelectedListener(this);
        txt_info_dob.setOnClickListener(this);
    }

    private void onStartUI(){
        /*GONE*/
        txt_info_first_name.setVisibility(View.GONE);
        txt_info_last_name.setVisibility(View.GONE);
        txt_info_full_name.setVisibility(View.GONE);
        txt_info_dob.setVisibility(View.GONE);
        txt_info_gender.setVisibility(View.GONE);
        txt_info_location.setVisibility(View.GONE);
        txt_info_phone.setVisibility(View.GONE);
        button_no_info_first_name.setVisibility(View.GONE);
        button_no_info_last_name.setVisibility(View.GONE);
        button_no_info_full_name.setVisibility(View.GONE);
        button_no_info_dob.setVisibility(View.GONE);
        button_no_info_gender.setVisibility(View.GONE);
        button_no_info_location.setVisibility(View.GONE);
        button_no_info_phone.setVisibility(View.GONE);
        button_yes_info_first_name.setVisibility(View.GONE);
        button_yes_info_last_name.setVisibility(View.GONE);
        button_yes_info_full_name.setVisibility(View.GONE);
        button_yes_info_dob.setVisibility(View.GONE);
        button_yes_info_gender.setVisibility(View.GONE);
        button_yes_info_location.setVisibility(View.GONE);
        button_yes_info_phone.setVisibility(View.GONE);
        /*VISIBLE*/
        info_edit_button.setVisibility(View.VISIBLE);
        button_edit_info_last_name.setVisibility(View.VISIBLE);
        button_edit_info_full_name.setVisibility(View.VISIBLE);
        button_edit_info_dob.setVisibility(View.VISIBLE);
        button_edit_info_gender.setVisibility(View.VISIBLE);
        button_edit_info_location.setVisibility(View.VISIBLE);
        button_edit_info_phone.setVisibility(View.VISIBLE);
        info_first_name.setVisibility(View.VISIBLE);
        info_last_name.setVisibility(View.VISIBLE);
        info_full_name.setVisibility(View.VISIBLE);
        info_dob.setVisibility(View.VISIBLE);
        info_gender.setVisibility(View.VISIBLE);
        info_location.setVisibility(View.VISIBLE);
        info_phone.setVisibility(View.VISIBLE);
    }

    private void restoreAuth(){
        authUser = new Auth(getContext()).putAuth(authUser).save().token();
    }

    @Override
    public void onStart() {
        super.onStart();
        onStartUI();
        setTempUser();
        restoreAuth();
    }

    @Override
    public void onResume() {
        super.onResume();
        onStartUI();
        setTempUser();
        restoreAuth();
    }

    private void onEditUI(){}

    private void onYes(){
        String dob = "";
        if (mCalendar!=null){
            dob = mCalendar.get(Calendar.YEAR)+"-"+(mCalendar.get(Calendar.MONTH)+1)+"-"+mCalendar.get(Calendar.DAY_OF_MONTH);
        }
        authUser.setFirs_name(txt_info_first_name.getText().toString());
        authUser.setLast_name(txt_info_last_name.getText().toString());
        authUser.setUser_name(txt_info_full_name.getText().toString());
        authUser.setDob(dob);
        authUser.setGender(mGender.toLowerCase().equals("male") ? "1" : "2");
        authUser.setLocation(txt_info_location.getText().toString());
        authUser.setPhone(txt_info_phone.getText().toString());
        onEditInfo();
    }

    private void initDatePicker(){
        if (mCalendar==null){
            mCalendar = Calendar.getInstance();
        }
        new DatePickerDialog(getActivity(), dateSetListener, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void onFailure(String error){
        final PrettyDialog pDialog = new PrettyDialog(getContext());
        pDialog.setTitle("Update Account")
                .setMessage(error)
                .setIcon(R.drawable.pdlg_icon_close)
                .setIconTint(R.color.pdlg_color_red)
                .addButton(
                        "OK",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        }
                )
                .show();
    }

    private void onEditInfo(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("auth_user",authUser.getId());
        params.put("first_name",authUser.getFirs_name());
        params.put("last_name",authUser.getLast_name());
        params.put("user_name",authUser.getUser_name());
        params.put("dob",authUser.getDob());
        params.put("gender",mGender);
        params.put("location",authUser.getLocation());
        params.put("phone",authUser.getPhone());
        client.post(SuperUtil.getBaseUrl(SuperConstance.UPDATE_ACCOUNT), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                ProfileInfoFragment.this.onStart();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Error error = new Gson().fromJson(rawJsonData,Error.class);
                ProfileInfoFragment.this.onFailure(error.getError());
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void onNo(){
        onStart();
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int month, int day) {
            mCalendar = Calendar.getInstance();
            mCalendar.set(Calendar.YEAR,year);
            mCalendar.set(Calendar.MONTH,month);
            mCalendar.set(Calendar.DAY_OF_MONTH,day);
            setDob();
        }
    };

    private void setDob(){
        String format = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        if (mCalendar.getTime()!=null) {
            txt_info_dob.setText(simpleDateFormat.format(mCalendar.getTime()));
        }
    }

    private void setTempUser(){
        if (authUser.getFirs_name()!=null){
            info_first_name.setText(authUser.getFirs_name());
            txt_info_first_name.setText(authUser.getFirs_name());
        }
        if (authUser.getLast_name()!=null){
            info_last_name.setText(authUser.getLast_name());
            txt_info_last_name.setText(authUser.getLast_name());
        }
        if (authUser.getUser_name()!=null){
            info_full_name.setText(authUser.getUser_name());
            txt_info_full_name.setText(authUser.getUser_name());
        }
        if (authUser.getDob()!=null){
            info_dob.setText(authUser.getDateOfBirth());
            txt_info_dob.setText(authUser.getDateOfBirth());
            String format = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format,Locale.ENGLISH);
            try {
                mCalendar = Calendar.getInstance();
                mCalendar.setTimeInMillis(simpleDateFormat.parse(authUser.getDateOfBirth()).getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (authUser.getGender()!=null){
            info_gender.setText(authUser.getGender());
            txt_info_gender.setPrompt(authUser.getGender());
            mGender = authUser.getGender().toLowerCase();
        }
        if (authUser.getLocation()!=null){
            info_location.setText(authUser.getLocation());
            txt_info_location.setText(authUser.getLocation());
        }
        if (authUser.getPhone()!=null){
            info_phone.setText(authUser.getPhone());
            txt_info_phone.setText(authUser.getPhone());
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_info, container, false);
        initView(view);
        setTempUser();
        return view;
    }

    private void checkValid(String value){
        if (value.length() > 0){
            onYes();
        }else {
            onFailure("Invalid input");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_edit_info_first_name:
                onStartUI();
                info_first_name.setVisibility(View.GONE);
                info_edit_button.setVisibility(View.GONE);
                button_no_info_first_name.setVisibility(View.VISIBLE);
                button_yes_info_first_name.setVisibility(View.VISIBLE);
                txt_info_first_name.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_last_name:
                onStartUI();
                info_last_name.setVisibility(View.GONE);
                button_edit_info_last_name.setVisibility(View.GONE);
                button_no_info_last_name.setVisibility(View.VISIBLE);
                button_yes_info_last_name.setVisibility(View.VISIBLE);
                txt_info_last_name.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_full_name:
                onStartUI();
                info_full_name.setVisibility(View.GONE);
                button_edit_info_full_name.setVisibility(View.GONE);
                button_no_info_full_name.setVisibility(View.VISIBLE);
                button_yes_info_full_name.setVisibility(View.VISIBLE);
                txt_info_full_name.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_dob:
                onStartUI();
                info_dob.setVisibility(View.GONE);
                button_edit_info_dob.setVisibility(View.GONE);
                button_no_info_dob.setVisibility(View.VISIBLE);
                button_yes_info_dob.setVisibility(View.VISIBLE);
                txt_info_dob.setVisibility(View.VISIBLE);
                txt_info_dob.setFocusable(false);
                break;
            case R.id.button_edit_info_gender:
                onStartUI();
                info_gender.setVisibility(View.GONE);
                button_edit_info_gender.setVisibility(View.GONE);
                button_no_info_gender.setVisibility(View.VISIBLE);
                button_yes_info_gender.setVisibility(View.VISIBLE);
                txt_info_gender.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_location:
                onStartUI();
                info_location.setVisibility(View.GONE);
                button_edit_info_location.setVisibility(View.GONE);
                button_no_info_location.setVisibility(View.VISIBLE);
                button_yes_info_location.setVisibility(View.VISIBLE);
                txt_info_location.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_phone:
                onStartUI();
                info_phone.setVisibility(View.GONE);
                button_edit_info_phone.setVisibility(View.GONE);
                button_no_info_phone.setVisibility(View.VISIBLE);
                button_yes_info_phone.setVisibility(View.VISIBLE);
                txt_info_phone.setVisibility(View.VISIBLE);
                break;
            case R.id.txt_info_dob:
                initDatePicker();
                break;
            case R.id.button_yes_info_first_name:
                checkValid(txt_info_first_name.getText().toString());
                break;
            case R.id.button_yes_info_last_name:
                checkValid(txt_info_last_name.getText().toString());
                break;
            case R.id.button_yes_info_full_name:
                checkValid(txt_info_full_name.getText().toString());
                break;
            case R.id.button_yes_info_dob:
                checkValid(txt_info_dob.getText().toString());
                break;
            case R.id.button_yes_info_gender:
                onYes();
                break;
            case R.id.button_yes_info_location:
                checkValid(txt_info_location.getText().toString());
                break;
            case R.id.button_yes_info_phone:
                checkValid(txt_info_phone.getText().toString());
                break;
            default:
                onStart();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.w("select gender",adapterView.getItemAtPosition(i).toString());
        mGender = mGenderList[i];
        Log.w("after gender",mGender);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

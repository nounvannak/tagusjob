package com.tagusnow.tagmejob;
import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import cz.msebera.android.httpclient.Header;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import permissions.dispatcher.NeedsPermission;

public class ProfilePostFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "SmUser";
    private static final String ARG_PARAM2 = "SmTagSubCategory";
    private static final String ARG_PARAM3 = "Feed";
    private final static String OPEN_MEDIA_TITLE = "title";
    private final static String OPEN_MEDIA_MODE = "mode";
    private final static String OPEN_MEDIA_MAX = "maxSelection";
    private final static int OPEN_GALLERY = 3542;
    private final static int OPEN_JOB_TYPE = 6575;
    private final static int OPEN_JOB_CLOSE_DATE = 4325;
    private final static int OPEN_JOB_CITY = 6536;
    private final static String TAG = ProfilePostFragment.class.getSimpleName();
    private Activity activity;
    private Spinner spnCategory,spnCity;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String job_type;
    private SmTagSubCategory smTagSubCategory;
    private ArrayList<SmLocation.Data> mCityList = new ArrayList<>();
    private SmUser authUser;
    private Feed feed;
    private ImageView job_type_validate_icon,job_title_validate_icon,job_desc_validate_icon,job_upload_file_validate_icon,job_upload_file,
            job_salary_validate_icon,job_close_date_validate_icon,job_name_validate_icon,job_phone_validate_icon,job_phone2_validate_icon,
            job_email_validate_icon,job_city_validate_icon,job_location_validate_icon;
    private EmojiconEditText job_title,job_description,job_location_detail;
    private EditText job_salary,job_name,job_phone,job_phone2,job_email;
    private TextView job_type_title,job_close_date_title,job_city_province_title;
    private RelativeLayout job_type_select,job_close_date_select,job_city_province_select;
    private ImageButton job_upload_file_remove;
    private File mUploadFile;
    private int mJobType = 0;
    private int mCity = 0;
    private String mCloseDate;
    private Calendar mCalendar;
    private boolean isValidate = true;
    private SmLocation location;

    private void initView(View view){
        spnCategory = (Spinner)view.findViewById(R.id.spnCategory);
        spnCity = (Spinner)view.findViewById(R.id.spnCity);
        job_type_validate_icon = (ImageView)view.findViewById(R.id.job_type_validate_icon);
        job_type_select = (RelativeLayout)view.findViewById(R.id.job_type_select);
        job_type_title = (TextView)view.findViewById(R.id.job_type_select_title);
        job_type_select.setOnClickListener(this);
        job_title_validate_icon = (ImageView)view.findViewById(R.id.job_title_validate_icon);
        job_title = (EmojiconEditText)view.findViewById(R.id.job_title);
        job_desc_validate_icon = (ImageView)view.findViewById(R.id.job_description_validate_icon);
        job_description = (EmojiconEditText)view.findViewById(R.id.job_description);
        job_upload_file_validate_icon = (ImageView)view.findViewById(R.id.job_upload_file_validate_icon);
        job_upload_file_remove = (ImageButton)view.findViewById(R.id.job_upload_file_remove);
        job_upload_file = (ImageView)view.findViewById(R.id.job_upload_file);
        job_upload_file_remove.setOnClickListener(this);
        job_upload_file.setOnClickListener(this);
        job_salary_validate_icon = (ImageView)view.findViewById(R.id.job_salary_validate_icon);
        job_salary = (EditText) view.findViewById(R.id.job_salary);
        job_close_date_validate_icon = (ImageView)view.findViewById(R.id.job_close_date_validate_icon);
        job_close_date_select = (RelativeLayout)view.findViewById(R.id.job_close_date_select);
        job_close_date_title = (TextView)view.findViewById(R.id.job_close_date_title);
        job_close_date_select.setOnClickListener(this);
        job_name_validate_icon = (ImageView)view.findViewById(R.id.job_username_validate_icon);
        job_name = (EditText)view.findViewById(R.id.job_username);
        job_phone_validate_icon = (ImageView)view.findViewById(R.id.job_phone_validate_icon);
        job_phone = (EditText)view.findViewById(R.id.job_phone);
        job_phone2_validate_icon = (ImageView)view.findViewById(R.id.job_phone2_validate_icon);
        job_phone2 = (EditText)view.findViewById(R.id.job_phone2);
        job_email_validate_icon = (ImageView)view.findViewById(R.id.job_email_validate_icon);
        job_email = (EditText)view.findViewById(R.id.job_email);
        job_city_validate_icon = (ImageView)view.findViewById(R.id.job_city_validate_icon);
        job_city_province_select = (RelativeLayout)view.findViewById(R.id.job_city_select);
        job_city_province_title = (TextView)view.findViewById(R.id.job_city_select_title);
        job_city_province_select.setOnClickListener(this);
        job_location_validate_icon = (ImageView)view.findViewById(R.id.job_location_detail_validate_icon);
        job_location_detail = (EmojiconEditText) view.findViewById(R.id.job_location_detail);
    }

    public ProfilePostFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfilePostFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfilePostFragment newInstance(SmUser param1, SmTagSubCategory param2) {
        ProfilePostFragment fragment = new ProfilePostFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1,new Gson().toJson(param1));
        args.putString(ARG_PARAM2, new Gson().toJson(param2));
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfilePostFragment newInstance(Feed feed,SmUser authUser,SmTagSubCategory category){
        ProfilePostFragment fragment = new ProfilePostFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1,new Gson().toJson(authUser));
        args.putString(ARG_PARAM2,new Gson().toJson(category));
        args.putString(ARG_PARAM3,new Gson().toJson(feed));
        fragment.setArguments(args);
        return fragment;
    }

    public ProfilePostFragment setActivity(Activity activity){
        this.activity =  getActivity();
        return this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            authUser = new Gson().fromJson(mParam1,SmUser.class);
            smTagSubCategory = new Gson().fromJson(mParam2,SmTagSubCategory.class);

            if (this.job_type!=null && this.job_type.equals(SuperConstance.POST_JOB)){
                String feedStr = getArguments().getString(ARG_PARAM3);
                feed = new Gson().fromJson(feedStr,Feed.class);
            }

        }
    }

    public ProfilePostFragment setType(String type){
        this.job_type = type;
        return this;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_post, container, false);
        initView(view);
        superTextChanged();
        initTempData();
        hideValidate();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.w(TAG,requestCode+"");
        if (requestCode==OPEN_GALLERY && resultCode==Activity.RESULT_OK && data!=null){
            ArrayList<String> results = data.getStringArrayListExtra("result");
            reloadImage(results.get(0));
        }else if(requestCode==OPEN_JOB_TYPE && resultCode==Activity.RESULT_OK && data!=null){
            mJobType = data.getIntExtra("category_select",0);
            Log.w(TAG,mJobType+"");
            selectJobType();
        }else if (requestCode==OPEN_JOB_CITY && resultCode==Activity.RESULT_OK && data!=null){
            mCity = data.getIntExtra("city",0);
            SmLocation smLocation = new Gson().fromJson(data.getStringExtra("city_list"),SmLocation.class);
            mCityList.addAll(smLocation.getData());
            selectCity();
        }
        completeValidate();
        super.onActivityResult(requestCode,resultCode,data);
    }

    public void postJob(){
        completeValidate();
        if (isValidate){
            requestPostJob();
        }else {
            showAlert();
        }
    }

    public void editJob(){
        completeValidate();
        if (isValidate){
            requestEditJob();
        }else {
            showAlert();
        }
    }

    private void clearForm(){
        mCity = 0;
        mJobType = 0;
        mCalendar = Calendar.getInstance();
        mUploadFile = null;
        mCloseDate = null;
        job_type_title.setText(R.string.select_job_type);
        job_title.getText().clear();
        job_description.getText().clear();
        removeUploadFile();
        job_salary.getText().clear();
        job_close_date_title.setText(R.string.pick_close_date);
        job_name.getText().clear();
        job_phone.getText().clear();
        job_phone2.getText().clear();
        job_email.getText().clear();
        job_city_province_title.setText(R.string.select_city_province);
        job_location_detail.getText().clear();
        completeValidate();
    }

    private void requestPostJob(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        requestParams.put("auth_user",authUser.getId());
        requestParams.put("job_type",mJobType);
        requestParams.put("job_title",job_title.getText().toString());
        requestParams.put("job_desc",job_description.getText().toString());
        String sMonth = "";
        if (mCalendar.get(Calendar.MONTH) >= 9){
            sMonth = String.valueOf(mCalendar.get(Calendar.MONTH) + 1);
        }else{
            sMonth = "0" + String.valueOf(mCalendar.get(Calendar.MONTH) + 1);
        }
        String cls_date = mCalendar.get(Calendar.YEAR)+"-"+sMonth+"-"+mCalendar.get(Calendar.DAY_OF_MONTH);
        requestParams.put("job_close_date",cls_date);
        requestParams.put("job_name",job_name.getText().toString());
        requestParams.put("job_phone",job_phone.getText().toString());
        requestParams.put("job_email",job_email.getText().toString());
        requestParams.put("job_city",mCity);
        if (mUploadFile!=null){
            try {
                requestParams.put("job_upload_file[]",mUploadFile,"multipart/form-data","upload_file.png");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (job_salary.getText().toString().length() > 0){
            requestParams.put("job_salary",job_salary.getText().toString());
        }
        if (job_phone2.getText().toString().length() > 0){
            requestParams.put("job_phone2",job_phone2.getText().toString());
        }
        if (job_location_detail.getText().toString().length() > 0){
            requestParams.put("job_location",job_location_detail.getText().toString());
        }
        client.post(SuperUtil.getBaseUrl(SuperConstance.POST_FEED_JOB), requestParams, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                Log.w("onSuccess",rawJsonResponse);
                clearForm();
                showSuccess();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w("onFailure",rawJsonData);
                showAlert();
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void requestEditJob(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        requestParams.put("feed_id",feed.getId());
        requestParams.put("auth_user",authUser.getId());
        requestParams.put("job_type",mJobType);
        requestParams.put("job_title",job_title.getText().toString());
        requestParams.put("job_desc",job_description.getText().toString());
        String cls_date = mCalendar.get(Calendar.YEAR)+"-"+mCalendar.get(Calendar.MONTH)+"-"+mCalendar.get(Calendar.DAY_OF_MONTH);
        requestParams.put("job_close_date",cls_date);
        requestParams.put("job_name",job_name.getText().toString());
        requestParams.put("job_phone",job_phone.getText().toString());
        requestParams.put("job_email",job_email.getText().toString());
        requestParams.put("job_city",mCity);
        if (mUploadFile!=null){
            try {
                requestParams.put("job_upload_file[]",mUploadFile,"multipart/form-data","upload_file.png");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (job_salary.getText().toString().length() > 0){
            requestParams.put("job_salary",job_salary.getText().toString());
        }
        if (job_phone2.getText().toString().length() > 0){
            requestParams.put("job_phone2",job_phone2.getText().toString());
        }
        if (job_location_detail.getText().toString().length() > 0){
            requestParams.put("job_location",job_location_detail.getText().toString());
        }
        client.post(SuperUtil.getBaseUrl(SuperConstance.EDIT_POST_JOB), requestParams, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                Log.w(TAG,rawJsonResponse);
                clearForm();
                showSuccess();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
                showAlert();
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void superTextChanged(){

        job_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                completeValidate();
            }
        });

        job_description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                completeValidate();
            }
        });

        job_salary.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                completeValidate();
            }
        });

        job_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                completeValidate();
            }
        });

        job_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                completeValidate();
            }
        });

        job_phone2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                completeValidate();
            }
        });

        job_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                completeValidate();
            }
        });

        job_location_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                completeValidate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                completeValidate();
            }
        });
    }

    private void hideValidate(){
        job_upload_file_validate_icon.setVisibility(View.GONE);
        job_salary_validate_icon.setVisibility(View.GONE);
        job_phone2_validate_icon.setVisibility(View.GONE);
        job_location_validate_icon.setVisibility(View.GONE);
        job_upload_file_remove.setVisibility(View.GONE);
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void openGalleryIntent(){
//        Intent intent = new Intent(getActivity(),Gallery.class);
//        intent.putExtra(OPEN_MEDIA_MODE,2);
//        intent.putExtra(OPEN_MEDIA_MAX,1);
//        intent.putExtra(OPEN_MEDIA_TITLE,"Upload File");
//        startActivityForResult(intent,OPEN_GALLERY);
        AndroidImagePicker.getInstance().pickSingle(this.activity, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                reloadImage(items.get(0).path);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showAlert(){
        final PrettyDialog pDialog = new PrettyDialog(getContext());
        pDialog.setTitle("Post job invalid")
                .setMessage("Please, complete all fields!")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorAccent)
                .addButton(
                        "OK",
                        R.color.pdlg_color_white,
                        R.color.colorAccent,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        }
                )
                .show();
    }

    private void showSuccess(){
        final PrettyDialog pDialog = new PrettyDialog(getContext());
        pDialog.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Job posted success!")
                .setIcon(R.drawable.ic_action_check_circle)
                .setIconTint(android.R.color.holo_green_dark)
                .addButton(
                        "OK",
                        R.color.pdlg_color_white,
                        android.R.color.holo_green_dark,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                                getActivity().finish();
                            }
                        }
                )
                .show();
    }

    private void reloadImage(String file){
        mUploadFile = new File(file);
        job_upload_file_remove.setVisibility(View.VISIBLE);
        Glide.with(getContext())
                .load(file)
                .centerCrop()
                .skipMemoryCache(true)
                .error(R.drawable.ic_crop_original)
                .into(job_upload_file);
    }

    private void removeUploadFile(){
        mUploadFile = null;
        job_upload_file_remove.setVisibility(View.GONE);
        Glide.with(getContext())
                .load(R.drawable.ic_crop_original)
                .fitCenter()
                .into(job_upload_file);
    }

    private void showJobTypeIntent(){
        Intent intent = new Intent(getActivity(),CategoryListView.class);
        intent.putExtra(ARG_PARAM2,mParam2);
        startActivityForResult(intent,OPEN_JOB_TYPE);
    }

    private void selectJobType(){
        if (mJobType > 0) {
            for (int i = 0; i < smTagSubCategory.getData().size(); i++) {
                if (smTagSubCategory.getData().get(i).getId() == mJobType) {
                    job_type_title.setText(smTagSubCategory.getData().get(i).getTitle());
                }
            }
        }else {
            job_type_title.setText(R.string.select_job_type);
        }
    }

    private void selectCity(){
        if (mCity > 0) {
            for (int i = 0; i < mCityList.size(); i++) {
                if (mCityList.get(i).getId() == mCity) {
                    Log.w("mCityList",mCityList.get(i).getName());
                    job_city_province_title.setText(mCityList.get(i).getName());
                }
            }
        }else {
            job_city_province_title.setText(R.string.select_city_province);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        initTempData();
        completeValidate();
    }

    @Override
    public void onStart() {
        super.onStart();
//        initTempData();
        completeValidate();
    }

    private void initCategory(){
        final ArrayList<String> lists = new ArrayList<>();
        for (int i=0;i < smTagSubCategory.getData().size();i++){
            lists.add(smTagSubCategory.getData().get(i).getTitle());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_dropdown_item,lists);
        spnCategory.setAdapter(adapter);
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mJobType = smTagSubCategory.getData().get(i).getId();
                selectJobType();
                completeValidate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void initCity(){
        ArrayList<String> lists = new ArrayList<>();
        location = new Repository(getContext()).restore().getLocation();
        for (int i=0;i < location.getData().size() ;i++){
            lists.add(location.getData().get(i).getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_dropdown_item,lists);
        spnCity.setAdapter(adapter);
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mCity = location.getData().get(i).getId();
                selectCity();
                completeValidate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initTempData(){

        initCategory();
        initCity();

        if (feed!=null){
            mJobType = feed.getSub_tag_id();
            mCity = feed.getFeed().getCity();
            selectCity();
            selectJobType();
        }

        if (authUser.getUser_name()!=null){
            job_name.setText(authUser.getUser_name());
        }

        if (authUser.getPhone()!=null){
            job_phone.setText(authUser.getPhone());
        }

        if (authUser.getLocation()!=null){
            job_location_detail.setText(authUser.getLocation());
        }

        if (authUser.getEmail()!=null){
            job_email.setText(authUser.getEmail());
        }

        if (this.job_type!=null && this.job_type.equals(SuperConstance.POST_JOB)){

            if (feed.getSm_tag_id() > 0){
                job_type_title.setText(feed.getFeed().getCategory());
                mJobType = feed.getSm_tag_id();
            }

            if (feed.getFeed().getTitle()!=null){
                job_title.setText(feed.getFeed().getTitle());
            }

            if (feed.getDescription()!=null){
                job_description.setText(feed.getDescription());
            }

            if (feed.getFeed().getAlbum()!=null){
                if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                    reloadImage(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                    Uri uri = Uri.parse(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                    mUploadFile = new File(uri.getPath());
                }else{
                    reloadImage(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"100"));
                    Uri uri = Uri.parse(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"original"));
                    mUploadFile = new File(uri.getPath());
                }

            }

            if (feed.getFeed().getSalary()!=null){
                job_salary.setText(feed.getFeed().getSalary());
            }

            if (feed.getFeed().getClose_date()!=null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
                try {
                    Date date = format.parse(feed.getFeed().getClose_date());
                    mCalendar = Calendar.getInstance();
                    mCalendar.setTimeInMillis(date.getTime());
                    setCloseDate();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (feed.getFeed().getProfile_username()!=null){
                job_name.setText(feed.getFeed().getProfile_name());
            }

            if (feed.getFeed().getPhone()!=null){
                job_phone.setText(feed.getFeed().getPhone());
            }

            if (feed.getFeed().getPhone2()!=null){
                job_phone2.setText(feed.getFeed().getPhone2());
            }

            if (feed.getFeed().getEmail()!=null){
                job_email.setText(feed.getFeed().getEmail());
            }

            if (feed.getFeed().getCity_name()!=null){
                mCity = feed.getFeed().getCity();
                job_city_province_title.setText(feed.getFeed().getCity_name());
            }

            if (feed.getFeed().getLocation_des()!=null){
                job_location_detail.setText(feed.getFeed().getLocation_des());
            }

        }
    }

    private void completeValidate(){
        isValidate = true;
        if(mJobType > 0){
            job_type_validate_icon.setImageResource(R.drawable.ic_action_check_circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_type_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.fbutton_color_green_sea)));
            }
//            Log.w("mJobType","true");
        }else {
            isValidate = false;
            job_type_validate_icon.setImageResource(R.drawable.ic_action_radio_button_unchecked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_type_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoogle)));
            }
//            Log.w("mJobType","false");
        }

        if (job_title.getText().toString().length() > 0){
            job_title_validate_icon.setImageResource(R.drawable.ic_action_check_circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_title_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.fbutton_color_green_sea)));
            }
//            Log.w("job_title","true");
        }else {
            isValidate = false;
            job_title_validate_icon.setImageResource(R.drawable.ic_action_radio_button_unchecked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_title_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoogle)));
            }
//            Log.w("job_title","false");
        }

        if (job_description.getText().toString().length() > 0){
            job_desc_validate_icon.setImageResource(R.drawable.ic_action_check_circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_desc_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.fbutton_color_green_sea)));
            }
//            Log.w("job_description","true");
        }else {
            isValidate = false;
            job_desc_validate_icon.setImageResource(R.drawable.ic_action_radio_button_unchecked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_desc_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoogle)));
            }
//            Log.w("job_description","false");
        }

        if (mCalendar!=null){
            job_close_date_validate_icon.setImageResource(R.drawable.ic_action_check_circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_close_date_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.fbutton_color_green_sea)));
            }
//            Log.w("mCalendar","true");
        }else {
            isValidate = false;
            job_close_date_validate_icon.setImageResource(R.drawable.ic_action_radio_button_unchecked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_close_date_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoogle)));
            }
//            Log.w("mCalendar","false");
        }

        if (job_name.getText().toString().length() > 0){
            job_name_validate_icon.setImageResource(R.drawable.ic_action_check_circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_name_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.fbutton_color_green_sea)));
            }
//            Log.w("job_name","true");
        }else {
            isValidate = false;
            job_name_validate_icon.setImageResource(R.drawable.ic_action_radio_button_unchecked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_name_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoogle)));
            }
//            Log.w("job_name","false");
        }

        if (job_phone.getText().toString().length() > 0){
            job_phone_validate_icon.setImageResource(R.drawable.ic_action_check_circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_phone_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.fbutton_color_green_sea)));
            }
//            Log.w("job_phone","true");
        }else {
            isValidate = false;
            job_phone_validate_icon.setImageResource(R.drawable.ic_action_radio_button_unchecked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_phone_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoogle)));
            }
//            Log.w("job_phone","false");
        }

        if (job_email.getText().toString().length() > 0 && job_email.getText().toString().contains("@") && job_email.getText().toString().contains(".")){
            job_email_validate_icon.setImageResource(R.drawable.ic_action_check_circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_email_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.fbutton_color_green_sea)));
            }
//            Log.w("job_email","true");
        }else {
            isValidate = false;
            job_email_validate_icon.setImageResource(R.drawable.ic_action_radio_button_unchecked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_email_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoogle)));
            }
//            Log.w("job_email","false");
        }

        if (mCity > 0){
            job_city_validate_icon.setImageResource(R.drawable.ic_action_check_circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_city_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.fbutton_color_green_sea)));
            }
//            Log.w("mCity","true");
        }else {
            isValidate = false;
            job_city_validate_icon.setImageResource(R.drawable.ic_action_radio_button_unchecked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                job_city_validate_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoogle)));
            }
//            Log.w("mCity","false");
        }

    }

    private void initDatePicker(){
        if (mCalendar==null){
            mCalendar = Calendar.getInstance();
        }
        new DatePickerDialog(getActivity(), dateSetListener, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setCloseDate(){
        String format = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        if (mCalendar.getTime()!=null) {
            job_close_date_title.setText(simpleDateFormat.format(mCalendar.getTime()));
        }
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int month, int day) {
            mCalendar = Calendar.getInstance();
            mCalendar.set(Calendar.YEAR,year);
            mCalendar.set(Calendar.MONTH,month);
            mCalendar.set(Calendar.DAY_OF_MONTH,day);
            setCloseDate();
        }
    };

    private void showJobCloseDatePicker(){
        initDatePicker();
        completeValidate();
    }

    private void showJobCityIntent(){
        Intent intent = new Intent(getActivity(),CityListView.class);
        startActivityForResult(intent,OPEN_JOB_CITY);
    }

    @Override
    public void onClick(View view) {
        completeValidate();
        switch (view.getId()){
            case R.id.job_type_select:
                /*code*/
                showJobTypeIntent();
                break;
            case R.id.job_close_date_select:
                /*code*/
                showJobCloseDatePicker();
                break;
            case R.id.job_city_select:
                /*code*/
                showJobCityIntent();
                break;
            case R.id.job_upload_file:
                /*code*/
                openGalleryIntent();
                break;
            case R.id.job_upload_file_remove:
                /*code*/
                removeUploadFile();
                break;
            default:
                    break;
        }
    }
}

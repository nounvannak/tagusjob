package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.Token;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.auth.ValidUser;
import com.tagusnow.tagmejob.social.MFacebook;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AuthService {

    @Multipart
    @POST(SuperConstance.REGISTER_NOTIFICATION)
    Call<String> sendRegistrationToServer(@Part("token") RequestBody token,@Part("user_id") RequestBody user_id);

    @POST(SuperConstance.POST_TOKEN)
    Call<Token> loginWeb(@Query("email") String email,@Query("platform") String platform);

    @POST(SuperConstance.POST_TOKEN)
    Call<Token> loginWeb(@Query("email") String email,@Query("platform") String platform,@Query("cache") int cache);

    @POST(SuperConstance.POST_TOKEN)
    Call<Token> loginWeb(@Query("email") String email,@Query("password") String password,@Query("platform") String platform);

    @POST(SuperConstance.POST_TOKEN)
    Call<Token> loginWeb(@Query("email") String email,@Query("password") String password,@Query("profile_id") String profile_id,@Query("platform") String platform);

    @POST(SuperConstance.POST_AUTHENTICATE)
    Call<SmUser> Authenticate(@Query("token") String token);

    @POST(SuperConstance.POST_REGISTER)
    Call<SmUser> registerUser(@Query(SuperConstance.PLATFORM)String platform,
                              @Query("email") String email,
                              @Query("user_name") String user_name,
                              @Query("phone") String phone,
                              @Query("password") String password,
                              @Query("confirm_password") String confirm_password);

    @Multipart
    @POST(SuperConstance.POST_REGISTER)
    Call<SmUser> saveWeb(@Query(SuperConstance.PLATFORM)String platform,
                              @Part("user_name") RequestBody user_name,
                              @Part("email") RequestBody email,
                              @Part("phone") RequestBody phone,
                              @Part("password") RequestBody password,
                              @Part("confirm_password") RequestBody confirm_password);

    @Multipart
    @POST(SuperConstance.POST_REGISTER)
    Call<SmUser> saveFacebook(@Query(SuperConstance.PLATFORM)String platform,
                              @Part("user_name") RequestBody user_name,
                              @Part("firs_name") RequestBody firs_name,
                              @Part("last_name") RequestBody last_name,
                              @Part("email") RequestBody email,
                              @Part("image") RequestBody image,
                              @Part("password") RequestBody password,
                              @Part("location") RequestBody location,
                              @Part("gender") RequestBody gender,
                              @Part("birthday") RequestBody birthday,
                              @Part("profile_id") RequestBody profile_id);

    @Multipart
    @POST(SuperConstance.POST_REGISTER)
    Call<SmUser> saveGoogle(@Query(SuperConstance.PLATFORM)String platform,
                              @Part("user_name") RequestBody user_name,
                              @Part("firs_name") RequestBody firs_name,
                              @Part("last_name") RequestBody last_name,
                              @Part("email") RequestBody email,
                              @Part("image") RequestBody image,
                              @Part("password") RequestBody password,
                              @Part("profile_id") RequestBody profile_id);

    @POST(SuperConstance.GET_USER_VALID)
    Call<ValidUser> validUser(@Query("platform") String platform,@Query("email") String email);

    @POST(SuperConstance.GET_USER_VALID)
    Call<ValidUser> validUser(@Query("platform") String platform,@Query("email") String email,@Query("profile_id") String profile_id);

    @POST(SuperConstance.POST_REGISTER)
    Call<SmUser> registerUser(@Body SmUser user,@Query(SuperConstance.PLATFORM)String platform);

    @POST(SuperConstance.UPDATE_ACCOUNT)
    Call<SmUser> updateAccount(@Body SmUser user);

    @Multipart
    @POST(SuperConstance.UPDATE_ACCOUNT)
    Call<SmUser> updateAccount(@Query("auth_user") int auth_user, @Part MultipartBody.Part image);

    @Multipart
    @POST(SuperConstance.UPDATE_ACCOUNT)
    Call<SmUser> updateAccount(@Query("auth_user") int auth_user, @Part List<MultipartBody.Part> request);

    @GET("api/v1/setting/{user_id}/check/slug")
    Call<Object> CheckSlug(@Path("user_id") int user_id,@Query("slug") String slug);

    @GET("api/v1/setting/{user_id}/check/phone")
    Call<Object> CheckPhone(@Path("user_id") int user_id,@Query("phone") String phone);

    @POST(SuperConstance.FACEBOOK_GRAPE+"/me?")
    Call<MFacebook> graphFacebook(@Query("access_token") String access_token, @Query("fields") String fields);

    @Multipart
    @POST("api/v1/user/save/{user_id}/first/login")
    Call<SmUser> FirstLogin(@Path("user_id") int user_id, @Query("user_type") int user_type, @Part("Category[]") List<RequestBody> Category, @Part("FollowUser[]") List<RequestBody> FollowUser);

    @Multipart
    @POST("api/v1/user/save/{user_id}/first/login")
    Call<SmUser> FirstLogin(@Path("user_id") int user_id, @Query("user_type") int user_type, @Part("Category[]") List<RequestBody> Category);

    @GET("oauth2/v2/userinfo")
    Call<Object> AuthGoogle(@Header("Authorization") String access_token,@Header("Accept") String accept);

    @GET("oauth2/v2/userinfo")
    Call<Object> AuthGoogle(@Header("Authorization") String access_token);

    @GET("api/v1/user/feed/{feedID}/like/{authID}")
    Call<UserPaginate> UserLikedList(@Path("feedID") int feedID,@Path("authID") int authID,@Query("limit") int limit);

    @GET("api/v1/user/feed/{feedID}/like/{authID}")
    Call<UserPaginate> UserLikedList(@Path("feedID") int feedID,@Path("authID") int authID,@Query("limit") int limit,@Query("page") int page);

    @Multipart
    @POST("api/v1/user/register/phone")
    // user_phone
    // user_name
    // password
    Call<Object> PhoneRegister(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/user/register/email")
    Call<Object> EmailRegister(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/user/send/verify/email")
    Call<Object> SendVerifyCodeViaEmail(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/user/send/verify/phone")
    Call<Object> SendVerifyCodeViaPhone(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/user/verify/email")
    Call<Object> DoVerifyCodeViaEmail(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/user/verify/phone")
    Call<Object> DoVerifyCodeViaPhone(@Part List<MultipartBody.Part> request);

}

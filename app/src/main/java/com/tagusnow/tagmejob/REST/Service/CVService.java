package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.model.v2.feed.Education;
import com.tagusnow.tagmejob.model.v2.feed.Experience;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CVService {

    public static final int FIRST_REQUEST = 0;
    public static final int REQUEST = 1;
    public static final int SORT_ASC = 0;
    public static final int SORT_DESC = 1;

    @GET("api/v1/user/{user_id}/education")
    Call<ArrayList<Education>> GetEducation(@Path("user_id") int user_id);

    @GET("api/v1/user/{user_id}/experience")
    Call<ArrayList<Experience>> GetExperience(@Path("user_id") int user_id);

    @GET("api/v1/cv/{user_id}/relate/{category}/feed/{feed_id}")
    Call<ArrayList<Feed>> GetRelateCV(@Path("user_id") int user_id,@Path("category") int category,@Path("feed_id") int feed_id);

    @GET("api/v1/cv/{user_id}/search/{type}")
    Call<UserPaginate> ResumeSearch(@Path("user_id") int user_id,@Path("type") int type,@Query("q") String query);

    @GET("api/v1/cv/{user_id}/search/{type}")
    Call<UserPaginate> ResumeSearch(@Path("user_id") int user_id, @Path("type") int type,@Query("q") String query, @Query("limit") int limit);

    @GET("api/v1/cv/{user_id}/search/{type}")
    Call<UserPaginate> ResumeSearch(@Path("user_id") int user_id,@Path("type") int type,@Query("q") String query,@Query("limit") int limit,@Query("page") int page);
}

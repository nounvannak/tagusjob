package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CommentService {

    @Multipart
    @POST("api/v1/comment-save")
    Call<Comment> save(@Part("auth_user") RequestBody auth_user,@Part("feed_id") RequestBody feed_id,@Part("des") RequestBody des,@Part MultipartBody.Part image);

    @Multipart
    @POST("api/v1/comment-save")
    Call<Comment> save(@Part("auth_user") RequestBody auth_user,@Part("feed_id") RequestBody feed_id,@Part("des") RequestBody des);

    @Multipart
    @POST("api/v1/comment-save")
    Call<Comment> save(@Part List<MultipartBody.Part> request);

    @GET("api/v1/comment/{id}")
    Call<Comment> get(@Query("id") int id);

    @GET("api/v1/getComment")
    Call<CommentResponse> getList(@Query("feed_id") int feed_id, @Query("limit") int limit);

    @GET("api/v1/getComment")
    Call<CommentResponse> getList(@Query("feed_id") int feed_id, @Query("limit") int limit,@Query("page") int page);

    @Multipart
    @POST("api/v1/comment-edit")
    Call<Comment> update(@Query("auth_user") int user,@Query("feed_id") int feed_id,@Query("comment_id") int comment_id,@Part("comment") String comment,@Part MultipartBody.Part image);

    @Multipart
    @POST("api/v1/comment-edit")
    Call<Comment> update(@Part List<MultipartBody.Part> request);

    @PUT("api/v1/comment-delete")
    Call<Comment> delete(@Query("auth_user") int user,@Query("feed_id") int feed_id,@Query("comment_id") int comment_id);

    @POST("CheckCommenting/{feed_id}")
    Call<Object> checkCommenting(@Path("feed_id") int feed_id,@Query("isType") boolean isType);

}

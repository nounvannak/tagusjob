package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.V2.Model.CompanyPaginate;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.Skill;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.model.v2.feed.Education;
import com.tagusnow.tagmejob.model.v2.feed.Experience;
import com.tagusnow.tagmejob.v3.profile.PhotoPaginate;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FeedService {

    @GET("api/v1/setting/company/{user_id}/recent")
    Call<CompanyPaginate> getCompanyRecentPost(@Path("user_id") int user_id,@Query("limit") int limit);

    @GET("api/v1/setting/company/{user_id}/recent")
    Call<CompanyPaginate> getCompanyRecentPost(@Path("user_id") int user_id, @Query("limit") int limit, @Query("page") int page);

    @GET("api/v1/setting/{user_id}/company")
    Call<CompanyPaginate> getCompany(@Path("user_id") int user_id,@Query("limit") int limit);

    @GET("api/v1/setting/{user_id}/company")
    Call<CompanyPaginate> getCompany(@Path("user_id") int user_id, @Query("limit") int limit, @Query("page") int page);

    @PUT("api/v1/job/{user_id}/subscribe/{category_id}")
    Call<Object> JobSubscribe(@Path("user_id") int user_id,@Path("category_id") int category_id);

    @PUT("api/v1/job/{user_id}/unsubscribe/{category_id}")
    Call<Object> JobUnsubscribe(@Path("user_id") int user_id,@Path("category_id") int category_id);

    @POST(SuperConstance.LIKE_FEED)
    Call<SmTagFeed.Like> LikeFeed(@Query("feed_id") int feed_id,@Query("user_id") int user_id);

    @POST(SuperConstance.LIKE_FEED)
    Call<SmTagFeed.Like> LikeFeed(@Query("feed_id") int feed_id,@Query("user_id") int user_id,@Query("channel_id") String channelID);

    @POST(SuperConstance.HIDE_POST)
    Call<Feed> HideFeed(@Query("post_id") int feed_id,@Query("user_post_id") int user_post_id,@Query("user_hide_id") int user_hide_id);

    @POST(SuperConstance.REMOVE_POST)
    Call<Feed> RemoveFeed(@Query("post_id") int feed_id,@Query("auth_user") int user_id);

    @POST(SuperConstance.SAVE_POST)
    Call<Feed> SaveFeed(@Query("post_id") int post_id,@Query("user_post_id") int user_post_id,@Query("user_save_id") int user_save_id);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Home(@Query("auth_user") int auth_user,@Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Home(@Query("auth_user") int auth_user,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Profile(@Query("auth_user") int auth_user,@Query("profile_user") int profile_user,@Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Profile(@Query("auth_user") int auth_user,@Query("profile_user") int profile_user,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Job(@Query("auth_user") int auth_user,@Query("type") int type,@Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Job(@Query("auth_user") int auth_user,@Query("type") int type,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Staff(@Query("auth_user") int auth_user,@Query("type") int type,@Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Staff(@Query("auth_user") int auth_user,@Query("type") int type,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user,@Query("search_query") String search_query,@Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user,@Query("search_query") String search_query,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("search_query") String search_query, List<RequestBody> category_id, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user,@Query("search_query") String search_query,List<RequestBody> category_id,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("search_query") String search_query, List<RequestBody> category_id,List<RequestBody> city, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user,@Query("search_query") String search_query,List<RequestBody> category_id,List<RequestBody> city,@Query("limit") int limit,@Query("page") int page);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("type") int type, @Query("search_query") String search_query, @Part("category_id") List<RequestBody> category_id, @Part("city") List<RequestBody> city, @Part("level") List<RequestBody> level, @Part("work_time") List<RequestBody> workType, @Part("salary") List<RequestBody> salary, @Query("limit") int limit);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("type") int type, @Query("search_query") String search_query, @Part("category_id") List<RequestBody> category_id, @Part("city") List<RequestBody> city, @Part("level") List<RequestBody> level, @Part("work_time") List<RequestBody> workType, @Part("salary") List<RequestBody> salary,@Query("limit") int limit,@Query("page") int page);

    // Posts
    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> SearchPost(@Query("auth_user") int auth_user,@Query("is_post") int isPost, @Query("type") int type, @Query("search_query") String search_query, @Part("category_id") List<RequestBody> category_id, @Part("city") List<RequestBody> city, @Part("level") List<RequestBody> level, @Part("work_time") List<RequestBody> workType, @Part("salary") List<RequestBody> salary, @Query("limit") int limit);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> SearchPost(@Query("auth_user") int auth_user,@Query("is_post") int isPost, @Query("type") int type, @Query("search_query") String search_query, @Part("category_id") List<RequestBody> category_id, @Part("city") List<RequestBody> city, @Part("level") List<RequestBody> level, @Part("work_time") List<RequestBody> workType, @Part("salary") List<RequestBody> salary,@Query("limit") int limit,@Query("page") int page);

    // Saved
    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> SearchSaved(@Query("auth_user") int auth_user,@Query("is_save") int isSaved, @Query("type") int type, @Query("search_query") String search_query, @Part("category_id") List<RequestBody> category_id, @Part("city") List<RequestBody> city, @Part("level") List<RequestBody> level, @Part("work_time") List<RequestBody> workType, @Part("salary") List<RequestBody> salary, @Query("limit") int limit);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> SearchSaved(@Query("auth_user") int auth_user,@Query("is_save") int isSaved, @Query("type") int type, @Query("search_query") String search_query, @Part("category_id") List<RequestBody> category_id, @Part("city") List<RequestBody> city, @Part("level") List<RequestBody> level, @Part("work_time") List<RequestBody> workType, @Part("salary") List<RequestBody> salary,@Query("limit") int limit,@Query("page") int page);

    // Expire
    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> SearchExpired(@Query("auth_user") int auth_user,@Query("is_expire") int isExpired, @Query("type") int type, @Query("search_query") String search_query, @Part("category_id") List<RequestBody> category_id, @Part("city") List<RequestBody> city, @Part("level") List<RequestBody> level, @Part("work_time") List<RequestBody> workType, @Part("salary") List<RequestBody> salary, @Query("limit") int limit);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> SearchExpired(@Query("auth_user") int auth_user,@Query("is_expire") int isExpired, @Query("type") int type, @Query("search_query") String search_query, @Part("category_id") List<RequestBody> category_id, @Part("city") List<RequestBody> city, @Part("level") List<RequestBody> level, @Part("work_time") List<RequestBody> workType, @Part("salary") List<RequestBody> salary,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("type") int type, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("search_query") String search_query, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("search_query") String search_query, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("type") int type, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("type") int type, @Query("search_query") String search_query, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Search(@Query("auth_user") int auth_user, @Query("type") int type, @Query("search_query") String search_query, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> YourPost(@Query("auth_user") int auth_user, @Query("is_post") int is_post, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> YourPost(@Query("auth_user") int auth_user, @Query("is_post") int is_post, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> YourSavePost(@Query("auth_user") int auth_user, @Query("is_save") int is_save, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> YourSavePost(@Query("auth_user") int auth_user, @Query("is_save") int is_save, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> YourExpirePost(@Query("auth_user") int auth_user, @Query("is_expire") int is_expire, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> YourExpirePost(@Query("auth_user") int auth_user, @Query("is_expire") int is_expire, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Query("auth_user") int auth_user, @Query("is_suggest") int is_suggest,@Query("type") int type, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Query("auth_user") int auth_user, @Query("is_suggest") int is_suggest,@Query("type") int type, @Query("category_id") int category_id, @Query("city") int city, @Query("limit") int limit);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Query("auth_user") int auth_user, @Query("is_suggest") int is_suggest,@Query("type") int type, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Query("auth_user") int auth_user, @Query("is_suggest") int is_suggest,@Query("type") int type, @Query("limit") int limit);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Query("auth_user") int auth_user, @Query("is_suggest") int is_suggest,@Query("type") int type,@Query("search_query") String search_query, @Query("limit") int limit, @Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Query("auth_user") int auth_user, @Query("is_suggest") int is_suggest,@Query("type") int type,@Query("search_query") String search_query, @Query("limit") int limit, @Query("page") int page, @Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Query("auth_user") int auth_user, @Query("is_suggest") int is_suggest,@Query("type") int type,@Query("search_query") String search_query, @Query("limit") int limit);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Query("auth_user") int auth_user, @Query("is_suggest") int is_suggest,@Query("type") int type,@Query("search_query") String search_query, @Query("limit") int limit, @Query("page") int page);

    @Multipart
    @POST("api/v1/getFeed")
    Call<FeedResponse> Suggest(@Part List<MultipartBody.Part> request);

    @GET("api/v1/users/{user_id}/image/{limit}/posts")
    Call<PhotoPaginate> GetUserImagePost(@Path("user_id") int user_id,@Path("limit") int limit);

    @GET("api/v1/users/{user_id}/image/{limit}/posts")
    Call<PhotoPaginate> GetUserImagePost(@Path("user_id") int user_id,@Path("limit") int limit,@Query("page") int page);

    @GET("api/v1/{user_id}/category")
    Call<List<Category>> CategoryByUser(@Path("user_id") int user_id);

    @GET("api/v1/users/{authID}/{userID}/{type}/friends")
    Call<UserPaginate> FriendList(@Path("authID") int authID,@Path("userID") int userID,@Path("type") int type,@Query("search_text") String searchText,@Query("limit") int limit);

    @GET("api/v1/users/{authID}/{userID}/{type}/friends")
    Call<UserPaginate> FriendList(@Path("authID") int authID,@Path("userID") int userID,@Path("type") int type,@Query("search_text") String searchText,@Query("limit") int limit,@Query("page") int page);

    @Multipart
    @POST("api/v1/post-feed")
    Call<Feed> PostNormal(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/update-feed")
    Call<Feed> UpdateNormal(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/post-job")
    Call<Feed> PostJob(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/edit-post-job/{feed_id}")
    Call<Feed> UpdateJob(@Path("feed_id") int feedID, @Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/post-cv")
    Call<Feed> PostCV(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/edit-post-cv/{feed_id}")
    Call<Feed> UpdateCV(@Path("feed_id") int feedID, @Part List<MultipartBody.Part> request);

    @GET("api/v1/setting/{user_id}/company")
    Call<CompanyPaginate> GetCompany(@Path("user_id") int userId,@Query("search") String searchText, @Query("limit") int limit);

    @GET("api/v1/setting/{user_id}/company")
    Call<CompanyPaginate> GetCompany(@Path("user_id") int userId,@Query("search") String searchText, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/user/{user_id}/education")
    Call<List<Education>> GetEducation(@Path("user_id") int userId);

    @GET("api/v1/user/{user_id}/experience")
    Call<List<Experience>> GetExperience(@Path("user_id") int userId);

    @GET("api/v1/user/{user_id}/skill")
    Call<List<Skill>> GetSkill(@Path("user_id") int userId);

}

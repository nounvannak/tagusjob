package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FollowService {

    @GET("api/v1/users/{user}/list/suggest")
    Call<UserPaginate> callSuggest(@Path("user") int user, @Query("limit") int limit, @Query("page") int page);

    @GET("api/v1/users/{user}/list/suggest")
    Call<UserPaginate> callSuggest(@Path("user") int user, @Query("limit") int limit);

    @GET("api/v1/users/{user}/list/suggest")
    Call<UserPaginate> callSuggest(@Path("user") int user, @Query("limit") int limit, @Query("user_type") int user_type,@Query("page") int page);

    @GET("api/v1/users/{user}/follower")
    Call<UserPaginate> callFollower(@Path("user") int user,@Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/users/{user}/follower")
    Call<UserPaginate> callFollower(@Path("user") int user,@Query("limit") int limit);

    @GET("api/v1/users/{user}/following")
    Call<UserPaginate> callFollowing(@Path("user") int user,@Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/users/{user}/following")
    Call<UserPaginate> callFollowing(@Path("user") int user,@Query("limit") int limit);

    @POST("api/v1/users/{follower}/{following}/following")
    Call<SmUser> hasFollow(@Path("follower") int follower,@Path("following") int following);

    @POST("api/v1/users/{follower}/{following}/unfollow")
    Call<SmUser> hasUnfollow(@Path("follower") int follower,@Path("following") int following);

    @POST("api/v1/users/{user}/search/follower")
    Call<UserPaginate> hasSearchFollower(@Path("user") int user,@Query("query_search") String query,@Query("limit") int limit);

    @POST("api/v1/users/{user}/search/follower")
    Call<UserPaginate> hasSearchFollower(@Path("user") int user,@Query("query_search") String query,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/users/{user}/search/following")
    Call<UserPaginate> hasSearchFollowing(@Path("user") int user,@Query("query_search") String query,@Query("limit") int limit);

    @POST("api/v1/users/{user}/search/following")
    Call<UserPaginate> hasSearchFollowing(@Path("user") int user,@Query("query_search") String query,@Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/users/{user}/search/suggest")
    Call<UserPaginate> hasSearchSuggest(@Path("user") int user,@Query("query_search") String query,@Query("limit") int limit);

    @POST("api/v1/users/{user}/search/suggest")
    Call<UserPaginate> hasSearchSuggest(@Path("user") int user, @Query("query_search") String query, @Query("limit") int limit, @Query("page") int page);

}

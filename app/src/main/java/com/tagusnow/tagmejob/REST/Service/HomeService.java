package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HomeService {

    @FormUrlEncoded
    @POST(SuperConstance.GET_FEED)
    Call<FeedResponse> callFeed(@Field("auth_user") int auth_user, @Field("profile_user") int profile_user);

    @FormUrlEncoded
    @POST(SuperConstance.GET_FEED)
    Call<FeedResponse> nextFeed(@Field("auth_user") int auth_user,@Field("profile_user") int profile_user,@Query("page") int page);

    @GET(SuperConstance.USER_SUGGEST)
    Call<List<SmUser>> listFollowSuggest(@Path("user") int user, @Query("limit") int limit);
}

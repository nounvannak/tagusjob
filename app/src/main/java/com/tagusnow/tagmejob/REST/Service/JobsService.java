package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedFindJob;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.model.v2.feed.ApplyJob;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JobsService {

    @GET("api/v1/jobs/{user}/id/{id}")
    Call<Feed> callJob(@Path("user") int user,@Path("id") int id);

    @POST(SuperConstance.GET_FEED)
    Call<FeedResponse> getFeed(@Query("auth_user") int auth_user,@Query("profile_user") int profile_user,@Query("limit") int limit);

    @POST(SuperConstance.GET_FEED)
    Call<FeedResponse> getFeed(@Query("auth_user") int auth_user,@Query("profile_user") int profile_user,@Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/jobs/{user}/{category}/list")
    Call<FeedResponse> jobsList(@Path("user") int user, @Path("category") int category, @Query("limit") int limit);

    @GET("api/v1/jobs/{user}/{category}/list")
    Call<FeedResponse> jobsList(@Path("user") int user, @Path("category") int category, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/jobs/{user}/{category}/list")
    Call<FeedResponse> jobsList(@Path("user") int user, @Path("category") int category,@Query("location") int location, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/jobs/{user}/list")
    Call<FeedResponse> allJobsList(@Path("user") int user, @Query("limit") int limit);

    @GET("api/v1/jobs/{user}/list")
    Call<FeedResponse> allJobsList(@Path("user") int user, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/jobs/{user}/dashboard")
    Call<SmUser> jobsDashboard(@Path("user") int user);

    @GET("api/v1/jobs/{user}/post")
    Call<FeedResponse> getJobsPost(@Path("user") int user, @Query("limit") int limit);

    @GET("api/v1/jobs/{user}/post")
    Call<FeedResponse> getJobsPost(@Path("user") int user, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/jobs/{user}/saved")
    Call<FeedResponse> getSavedJobs(@Path("user") int user, @Query("limit") int limit);

    @GET("api/v1/jobs/{user}/saved")
    Call<FeedResponse> getSavedJobs(@Path("user") int user, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/jobs/{user}/expired")
    Call<FeedResponse> getExpiredJobs(@Path("user") int user, @Query("limit") int limit);

    @GET("api/v1/jobs/{user}/expired")
    Call<FeedResponse> getExpiredJobs(@Path("user") int user, @Query("limit") int limit,@Query("page") int page);

    @PUT("api/v1/jobs/{user}/renew")
    Call<Feed> renewJobs(@Body Feed feed,@Path("user") int user);

    @GET("api/v1/jobs/{location}/user/{user}/search")
    Call<FeedResponse> searchJobs(@Path("location") int location,@Path("user") int user,@Query("search") String search,@Query("limit") int limit);

    @GET("api/v1/jobs/{location}/user/{user}/search")
    Call<FeedResponse> searchJobs(@Path("location") int location,@Path("user") int user,@Query("search") String search,@Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/fast/{user}/{type}/search")
    Call<FeedResponse> fastSearch(@Path("user") int user,@Path("type") int type,@Query("search") String search,@Query("limit") int limit);

    @GET("api/v1/fast/{user}/{type}/search")
    Call<FeedResponse> fastSearch(@Path("user") int user,@Path("type") int type,@Query("search") String search,@Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/fast/{user}/{type}/search")
    Call<FeedResponse> fastSearch(@Path("user") int user,@Path("type") int type,@Query("search") String search,@Query("id") int id,@Query("limit") int limit,@Query("page") int page);

    @Multipart
    @POST("api/v1/post-job")
    Call<Feed> saveJobs(@Part("title") RequestBody job_title,
                        @Part("auth_user") RequestBody auth_user,
                        @Part("close_date") RequestBody job_close_date,
                        @Part("phone") RequestBody job_phone,
                        @Part("city") RequestBody job_city,
                        @Part("email") RequestBody job_email,
                        @Part("salary") RequestBody job_salary,
                        @Part("desc") RequestBody job_desc,
                        @Part("type") RequestBody job_type,
                        @Part("num_employee") RequestBody num_employee,
                        @Part("website") RequestBody website,
                        @Part("level") RequestBody level,
                        @Part("work_time") RequestBody work_time,
                        @Part("requirement") RequestBody requirement,
                        @Part List<MultipartBody.Part> files);

    @Multipart
    @POST("api/v1/edit-post-job/{feed_id}")
    Call<Feed> UpdateJob(@Path("feed_id") int feed_id,
                         @Part("title") RequestBody job_title,
                         @Part("auth_user") RequestBody auth_user,
                         @Part("close_date") RequestBody job_close_date,
                         @Part("phone") RequestBody job_phone,
                         @Part("city") RequestBody job_city,
                         @Part("email") RequestBody job_email,
                         @Part("salary") RequestBody job_salary,
                         @Part("desc") RequestBody job_desc,
                         @Part("type") RequestBody job_type,
                         @Part("num_employee") RequestBody num_employee,
                         @Part("website") RequestBody website,
                         @Part("level") RequestBody level,
                         @Part("work_time") RequestBody work_time,
                         @Part("requirement") RequestBody requirement,
                         @Part List<MultipartBody.Part> files);

    @Multipart
    @POST("api/v1/edit-post-job/{feed_id}")
    Call<Feed> UpdateJob(@Path("feed_id") int feed_id,
                         @Part("title") RequestBody job_title,
                         @Part("auth_user") RequestBody auth_user,
                         @Part("close_date") RequestBody job_close_date,
                         @Part("phone") RequestBody job_phone,
                         @Part("city") RequestBody job_city,
                         @Part("email") RequestBody job_email,
                         @Part("salary") RequestBody job_salary,
                         @Part("desc") RequestBody job_desc,
                         @Part("type") RequestBody job_type,
                         @Part("num_employee") RequestBody num_employee,
                         @Part("website") RequestBody website,
                         @Part("level") RequestBody level,
                         @Part("work_time") RequestBody work_time,
                         @Part("requirement") RequestBody requirement);


    @Multipart
    @POST("api/v1/apply-job")
    Call<ApplyJob> ApplyJob(@Part("feed_id") RequestBody feed_id,
                            @Part("user_id") RequestBody user_id,
                            @Part("name") RequestBody name,
                            @Part("email") RequestBody email,
                            @Part("subject") RequestBody subject,
                            @Part("description") RequestBody description,
                            @Part MultipartBody.Part cv,
                            @Part MultipartBody.Part cover_letter);

    @Multipart
    @POST("api/v1/post-cv")
    Call<Feed> PostCV(@Part("user_id") RequestBody user_id,
                             @Part("category_id") RequestBody category_id,
                             @Part("title") RequestBody title,
                             @Part("phone") RequestBody phone,
                             @Part("email") RequestBody email,
                             @Part("salary") RequestBody salary,
                             @Part("city_id") RequestBody city_id,
                             @Part("description") RequestBody description,
                             @Part List<MultipartBody.Part> photo,
                             @Part MultipartBody.Part cv,
                             @Part MultipartBody.Part cover_letter);

    @Multipart
    @POST("api/v1/post-cv")
    Call<Feed> PostCV(@Part("user_id") RequestBody user_id,
                             @Part("category_id") RequestBody category_id,
                             @Part("title") RequestBody title,
                             @Part("phone") RequestBody phone,
                             @Part("email") RequestBody email,
                             @Part("salary") RequestBody salary,
                             @Part("city_id") RequestBody city_id,
                             @Part("description") RequestBody description,
                             @Part List<MultipartBody.Part> photo,
                             @Part MultipartBody.Part cv);

    @Multipart
    @POST("api/v1/post-cv")
    Call<Feed> PostCV(@Part("user_id") RequestBody user_id,
                             @Part("category_id") RequestBody category_id,
                             @Part("title") RequestBody title,
                             @Part("phone") RequestBody phone,
                             @Part("email") RequestBody email,
                             @Part("salary") RequestBody salary,
                             @Part("city_id") RequestBody city_id,
                             @Part("description") RequestBody description,
                             @Part MultipartBody.Part cv,
                             @Part MultipartBody.Part cover_letter);
}

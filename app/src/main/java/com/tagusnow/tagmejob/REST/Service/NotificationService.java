package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.feed.NotificationResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NotificationService {

    @GET("api/v1/notification/{user}/list")
    Call<NotificationResponse> getNotificationList(@Path("user") int user, @Query("limit") int limit);

    @GET("api/v1/notification/{user}/list")
    Call<NotificationResponse> getNotificationList(@Path("user") int user, @Query("limit") int limit,@Query("page") int page);

    @POST(SuperConstance.GET_NOTIFICATION)
    Call<NotificationResponse> GetNotification();

}

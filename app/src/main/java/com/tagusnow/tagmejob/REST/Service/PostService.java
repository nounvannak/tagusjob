package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.feed.Feed;

import org.json.JSONObject;

import java.util.List;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface PostService {

    @Multipart
    @POST("api/v1/post-feed")
    Call<Feed> Save(@Part("auth_user") RequestBody user_id,
                          @Part("job_category") RequestBody category_id,
                          @Part("feed_text") RequestBody text,
                          @Part("privacy") RequestBody privacy,
                          @Part List<MultipartBody.Part> image);

    @Multipart
    @POST("api/v1/post-feed")
    Call<Feed> Save(@Part("auth_user") RequestBody user_id,
                    @Part("job_category") RequestBody category_id,
                    @Part("feed_text") RequestBody text,
                    @Part("privacy") RequestBody privacy);

    @Multipart
    @POST("api/v1/update-feed")
    Call<Feed> Update(@Part("post_id") RequestBody feed_id,
                      @Part("auth_user") RequestBody user_id,
                      @Part("job_category") RequestBody category_id,
                      @Part("feed_text") RequestBody text,
                      @Part("privacy") RequestBody privacy,
                      @Part List<MultipartBody.Part> image,
                      @Part("old_file") RequestBody OldImage);

    @Multipart
    @POST("api/v1/update-feed")
    Call<Feed> Update(@Part("post_id") RequestBody feed_id,
                      @Part("auth_user") RequestBody user_id,
                      @Part("job_category") RequestBody category_id,
                      @Part("feed_text") RequestBody text,
                      @Part("privacy") RequestBody privacy,
                      @Part("old_file") RequestBody OldImage);

    @Multipart
    @POST("api/v1/update-feed")
    Call<Feed> Update(@Part("post_id") RequestBody feed_id,
                      @Part("auth_user") RequestBody user_id,
                      @Part("job_category") RequestBody category_id,
                      @Part("feed_text") RequestBody text,
                      @Part("privacy") RequestBody privacy);

    @Multipart
    @POST("api/v1/update-feed")
    Call<Feed> Update(@Part("post_id") RequestBody feed_id,
                      @Part("auth_user") RequestBody user_id,
                      @Part("job_category") RequestBody category_id,
                      @Part("feed_text") RequestBody text,
                      @Part("privacy") RequestBody privacy,
                      @Part List<MultipartBody.Part> image);

    @Multipart
    @POST("api/v1/feed/remove")
    Call<Feed> Delete(@Part("auth_user") RequestBody auth_user,@Part("post_id") RequestBody  post_id);

    @Multipart
    @POST("api/v1/feed/hide")
    Call<Feed> Hide(@Part("user_post_id") RequestBody user_post_id,@Part("user_hide_id") RequestBody user_hide_id,@Part("post_id") RequestBody  post_id);

    @Multipart
    @POST("api/v1/feed/save")
    Call<Feed> Pin(@Part("user_post_id") RequestBody user_post_id,@Part("user_save_id") RequestBody user_save_id,@Part("post_id") RequestBody  post_id);
}

package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchHistory;
import com.tagusnow.tagmejob.feed.RecentSearchHistoryPaginate;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RecentSearchService {

    @GET("api/v1/search/{user}/recent")
    Call<RecentSearchPaginate> hasRecentSearch(@Path("user") int user, @Query("limit") int limit);

    @GET("api/v1/search/{user}/recent")
    Call<RecentSearchPaginate> hasRecentSearch(@Path("user") int user, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/search/{user}/{type}/recent")
    Call<RecentSearchPaginate> hasRecentSearch_(@Path("user") int user,@Path("type") int type, @Query("limit") int limit);

    @GET("api/v1/search/{user}/{type}/recent")
    Call<RecentSearchPaginate> hasRecentSearch_(@Path("user") int user,@Path("type") int type, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/search/{user}/recent/history")
    Call<RecentSearchHistoryPaginate> hasRecentSearchHistory(@Path("user") int user, @Query("limit") int limit);

    @GET("api/v1/search/{user}/recent/history")
    Call<RecentSearchHistoryPaginate> hasRecentSearchHistory(@Path("user") int user, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/save/{user}/recent")
    Call<RecentSearch> save(@Body RecentSearch recentSearch,@Path("user") int user);

    @POST("api/v1/save/{user}/all/recent")
    Call<RecentSearchPaginate> saveAll(@Body RecentSearchPaginate recentSearchPaginate,@Path("user") int user);

    @POST("api/v1/update/{user}/recent")
    Call<RecentSearch> update(@Body RecentSearch recentSearch,@Path("user") int user);

    @HTTP(method = "DELETE", path = "api/v1/delete/{user}/recent", hasBody = true)
    Call<RecentSearch> destroy(@Body RecentSearch recentSearch,@Path("user") int user);

    @DELETE("api/v1/delete/{user}/list/recent")
    Call<List<RecentSearch>> destroyAll(@Path("user") int user);
}

package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.model.v2.feed.Language;
import com.tagusnow.tagmejob.model.v2.feed.Setting;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RepositoryService {

    @GET("api/v1/setting/{key}")
    Call<Setting> GetSetting(@Path("key") String key);

    @GET(SuperConstance.GET_CATEGORY)
    Call<SmTagSubCategory> callCategory();

    @GET("api/v1/{user_id}/category")
    Call<List<Category>> CategoryByUser(@Path("user_id") int user_id);

    @GET(SuperConstance.GET_LOCATION)
    Call<SmLocation> callLocation();

    @GET("api/v1/category/{id}")
    Call<Category> GetCategory(@Path("id") int id);

    @GET("api/v1/language")
    Call<List<Language>> GetLanguage();
}

package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.model.v2.feed.Socket.CountMessage;
import com.tagusnow.tagmejob.model.v2.feed.Socket.CountNotification;
import com.tagusnow.tagmejob.model.v2.feed.Socket.MessageTyping;
import com.tagusnow.tagmejob.model.v2.feed.Socket.SocketTyping;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SocketService {

    @POST("/Typing/{feedID}/{user_name}/{user_id}")
    Call<SocketTyping> SOCKET_TYPING(@Path("feedID") int feedID,@Path("user_name") String user_name,@Path("user_id") int user_id);

    @POST("/StopTyping/{feedID}")
    Call<SocketTyping> SOCKET_STOP_TYPING(@Path("feedID") int feedID);

    @POST("/CountNotification/{userID}/{counter}")
    Call<CountNotification> SOCKET_COUNT_NOTIFICATION(@Path("userID") int userID, @Path("counter") int counter);

    @POST("/TypingMessage/{user_id}/{user_name}/{conversation_id}")
    Call<MessageTyping> MessageTyping(@Path("user_id") int user_id,@Path("user_name") String user_name,@Path("conversation_id") int conversation_id);

    @POST("/StopTypingMessage/{user_id}/{conversation_id}")
    Call<MessageTyping> StopMessageTyping(@Path("user_id") int user_id,@Path("conversation_id") int conversation_id);

    @POST("/CountNotificationMessage/{userID}/{counter}")
    Call<CountMessage> CountMessageNotification(@Path("userID") int userID, @Path("counter") int counter);
}

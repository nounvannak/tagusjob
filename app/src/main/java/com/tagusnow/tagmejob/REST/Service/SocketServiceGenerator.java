package com.tagusnow.tagmejob.REST.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.SuperConstance;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SocketServiceGenerator {
    private static Gson gson = new GsonBuilder().setLenient().create();
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static OkHttpClient client = httpClient.connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(SuperConstance.SOCKET_IO_).addConverterFactory(GsonConverterFactory.create(gson)).client(client);
    private static Retrofit retrofit = builder.build();
    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}

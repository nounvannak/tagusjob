package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.model.v2.feed.Photo;
import com.tagusnow.tagmejob.model.v2.feed.PhotoPaginate;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserPostImageService {

    @GET("api/v1/users/{user_id}/image/{limit}/posts")
    Call<PhotoPaginate> getUserImagePost(@Path("user_id") int user_id, @Path("limit") int limit);

    @GET("api/v1/users/{user_id}/image/{limit}/posts")
    Call<PhotoPaginate> getUserImagePost(@Path("user_id") int user_id, @Path("limit") int limit, @Query("page") int page);
}

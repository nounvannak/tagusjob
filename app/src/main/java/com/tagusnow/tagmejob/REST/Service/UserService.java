package com.tagusnow.tagmejob.REST.Service;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserInterest;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.model.v2.feed.ConversationPagination;

import org.json.JSONObject;

import java.util.List;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {

    @GET("api/v1/users/{user_id}/list")
    Call<UserPaginate> UserList(@Path("user_id") int user_id,@Query("type") int type, @Query("limit") int limit);

    @GET("api/v1/users/{user_id}/list")
    Call<UserPaginate> UserList(@Path("user_id") int user_id,@Query("type") int type, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/users/{user_id}/list")
    Call<UserPaginate> UserList(@Path("user_id") int user_id,@Query("type") int type,@Query("search") String search, @Query("limit") int limit);

    @GET("api/v1/users/{user_id}/list")
    Call<UserPaginate> UserList(@Path("user_id") int user_id,@Query("type") int type,@Query("search") String search, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/user/{user_id}/chat/{type}/type")
    Call<UserPaginate> ChatList(@Path("user_id") int user_id,@Path("type") int type, @Query("limit") int limit);

    @GET("api/v1/user/{user_id}/chat/{type}/type")
    Call<UserPaginate> ChatList(@Path("user_id") int user_id,@Path("type") int type, @Query("limit") int limit,@Query("page") int page);

    @GET("api/v1/user/{user_id}/chat/{type}/type")
    Call<UserPaginate> ChatList(@Path("user_id") int user_id,@Path("type") int type,@Query("query_search") String query_search, @Query("limit") int limit);

    @GET("api/v1/user/{user_id}/chat/{type}/type")
    Call<UserPaginate> ChatList(@Path("user_id") int user_id,@Path("type") int type,@Query("query_search") String query_search, @Query("limit") int limit,@Query("page") int page);

    @POST("api/v1/user/{user_one}/chat/{user_two}/room")
    Call<JSONObject> ChatRoom(@Path("user_one") int user_one, @Path("user_two") int user_two);

    @Multipart
    @POST("api/v1/user/{auth_user}/{recipients_id}/chat/{chat_id}/{type}")
    Call<Conversation> Chat(@Path("chat_id") int chat_id,@Path("type") int type, @Path("auth_user") int user_one, @Path("recipients_id") int user_two, @Part("message") RequestBody message);

    @Multipart
    @POST("api/v1/user/{auth_user}/{recipients_id}/chat/{chat_id}/{type}")
    Call<Conversation> Chat(@Path("chat_id") int chat_id,@Path("type") int type, @Path("auth_user") int user_one, @Path("recipients_id") int user_two, @Part MultipartBody.Part file);

    @Multipart
    @POST("api/v1/user/{auth_user}/{recipients_id}/chat/{chat_id}/{type}")
    Call<Conversation> Chat(@Path("chat_id") int chat_id,@Path("type") int type, @Path("auth_user") int user_one, @Path("recipients_id") int user_two, @Part("message") RequestBody message, @Part MultipartBody.Part file);

    @Multipart
    @POST("api/v1/user/{auth_user}/{recipients_id}/chat/{chat_id}/{type}")
    Call<Conversation> Chat(@Path("chat_id") int chat_id,@Path("type") int type, @Path("auth_user") int user_one, @Path("recipients_id") int user_two, @Part("message") RequestBody message, @Part List<MultipartBody.Part> files);

    @Multipart
    @POST("api/v1/user/{auth_user}/{recipients_id}/chat/{chat_id}/{type}")
    Call<Conversation> Chat(@Path("chat_id") int chat_id,@Path("type") int type, @Path("auth_user") int user_one, @Path("recipients_id") int user_two, @Part List<MultipartBody.Part> files);

    @GET("api/v1/user/{user_id}/chat/{chat_id}")
    Call<ConversationPagination> ChatResource(@Path("user_id") int user_id,@Path("chat_id") int chat_id,@Query("limit") int limit);

    @GET("api/v1/user/{user_id}/chat/{chat_id}")
    Call<ConversationPagination> ChatResource(@Path("user_id") int user_id,@Path("chat_id") int chat_id,@Query("limit") int limit,@Query("page") int page);

    @PUT("api/v1/user/{user_id}/seen/chat/{chat_id}")
    Call<JSONObject> SeenChat(@Path("user_id") int user_id,@Path("chat_id") int chat_id);

    @PUT("api/v1/user/{user_id}/seen/chat/{chat_id}/{message_id}")
    Call<JSONObject> SeenMessage(@Path("user_id") int user_id,@Path("chat_id") int chat_id,@Path("message_id") int message_id);

    @GET("api/v1/user/{user_id}/interest")
    Call<List<UserInterest>> UserInterest(@Path("user_id") int user_id);

    @POST(SuperConstance.LIKE_USER)
    Call<Object> LikeUser(@Query("auth_user") int auth_user,@Query("user_id") int user_id);

    @POST(SuperConstance.UNLIKE_USER)
    Call<Object> UnlikeUser(@Query("auth_user") int auth_user,@Query("user_id") int user_id);

    @POST(SuperConstance.SEE_FIRST)
    Call<Object> SeeFirst(@Query("auth_user") int auth_user,@Query("user_id") int user_id);

    @POST(SuperConstance.UNSEE_FIRST)
    Call<Object> UnseeFirst(@Query("auth_user") int auth_user,@Query("user_id") int user_id);

    @POST("api/v1/users/{follower}/{following}/following")
    Call<SmUser> FollowUser(@Path("follower") int follower,@Path("following") int following);

    @POST("api/v1/users/{follower}/{following}/unfollow")
    Call<SmUser> UnfollowUser(@Path("follower") int follower,@Path("following") int following);
}

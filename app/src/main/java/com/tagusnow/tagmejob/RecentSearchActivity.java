package com.tagusnow.tagmejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.adapter.RecentSearchAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchHistory;
import com.tagusnow.tagmejob.feed.RecentSearchHistoryPaginate;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecentSearchActivity extends TagUsJobActivity {

    private static final String TAG = RecentSearchActivity.class.getSimpleName();
    private SmUser authUser;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private RecentSearchHistoryPaginate recentSearchHistoryPaginate;
    private RecentSearchAdapter adapter;
    /*Service*/
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    private RecentSearchService service = retrofit.create(RecentSearchService.class);
    private Callback<RecentSearchHistoryPaginate> mNewCallback = new Callback<RecentSearchHistoryPaginate>() {
        @Override
        public void onResponse(Call<RecentSearchHistoryPaginate> call, Response<RecentSearchHistoryPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewRecentSearch(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearchHistoryPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<RecentSearchHistoryPaginate> mNextCallback = new Callback<RecentSearchHistoryPaginate>() {
        @Override
        public void onResponse(Call<RecentSearchHistoryPaginate> call, Response<RecentSearchHistoryPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextRecentSearch(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearchHistoryPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private DialogInterface.OnClickListener mNo = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    };

    private DialogInterface.OnClickListener mYes = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            showProgress();
            detroyAll();
        }
    };
    private Callback<List<RecentSearch>> mClearAllCallback = new Callback<List<RecentSearch>>() {
        @Override
        public void onResponse(Call<List<RecentSearch>> call, Response<List<RecentSearch>> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                dismissProgress();
                onBackPressed();
            }else{
                try {
                    Log.e(TAG,response.errorBody().string());
                    dismissProgress();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<List<RecentSearch>> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void hasRecentSearch(){
        int user = this.authUser!=null ? this.authUser.getId() : 0;
        int limit = 10;
        service.hasRecentSearchHistory(user,limit).enqueue(mNewCallback);
    }

    private void hasNextRecentSearch(){
        int user = this.authUser!=null ? this.authUser.getId() : 0;
        int limit = 10;
        int page = this.recentSearchHistoryPaginate.getCurrent_page() + 1;
        service.hasRecentSearchHistory(user,limit,page).enqueue(mNextCallback);
    }

    static int lastPage;
    static boolean isRequest = false;

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (recentSearchHistoryPaginate.getTo() - 1)) && (recentSearchHistoryPaginate.getTo() < recentSearchHistoryPaginate.getTotal())) {
                        if (recentSearchHistoryPaginate.getNext_page_url()!=null || !recentSearchHistoryPaginate.getNext_page_url().equals("")){
                            if (lastPage!=recentSearchHistoryPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = recentSearchHistoryPaginate.getCurrent_page();
                                    hasNextRecentSearch();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initNewRecentSearch(RecentSearchHistoryPaginate recentSearchHistoryPaginate){
        this.recentSearchHistoryPaginate = recentSearchHistoryPaginate;
        this.checkNext();
        this.adapter = new RecentSearchAdapter(this,recentSearchHistoryPaginate,this.authUser);
        recyclerView.setAdapter(this.adapter);
    }

    private void initNextRecentSearch(RecentSearchHistoryPaginate recentSearchHistoryPaginate){
        this.recentSearchHistoryPaginate = recentSearchHistoryPaginate;
        this.checkNext();
        this.adapter.update(this,recentSearchHistoryPaginate,this.authUser).notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_search);
        initTemp();
        initView();
        hasRecentSearch();
    }

    private void initView(){
        initToolbar();
        initRecycler();
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,RecentSearchActivity.class);
    }

    private void initTemp(){
        authUser = new Auth(this).checkAuth().token();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recent_searches,menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void onClearAll(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()){
                    new AlertDialog.Builder(RecentSearchActivity.this)
                            .setNegativeButton("No",mNo)
                            .setPositiveButton("Yes",mYes)
                            .setTitle("Clear all of searches histories")
                            .setMessage("Are you sure to clear all of this histories ?")
                            .setCancelable(false)
                            .create()
                            .show();
                }
            }
        });
    }

    private void detroyAll(){
        int user = authUser!=null ? authUser.getId() : 0;
        service.destroyAll(user).enqueue(mClearAllCallback);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.btnClearAll){
           onClearAll();
        }
        return super.onOptionsItemSelected(item);
    }
}

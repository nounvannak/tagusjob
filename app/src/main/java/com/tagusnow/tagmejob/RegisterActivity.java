package com.tagusnow.tagmejob;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.google.gson.Gson;
import com.jaychang.slm.SocialLoginManager;
import com.jaychang.slm.SocialUser;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.Error;
import com.tagusnow.tagmejob.auth.GoogleResponse;
import com.tagusnow.tagmejob.auth.Helper;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.Token;
import com.tagusnow.tagmejob.auth.ValidUser;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.UserTypeActivity;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.fuckboilerplate.rx_social_connect.RxSocialConnect;

import java.io.IOException;

import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import studios.codelight.smartloginlibrary.LoginType;
import studios.codelight.smartloginlibrary.SmartLogin;
import studios.codelight.smartloginlibrary.SmartLoginCallbacks;
import studios.codelight.smartloginlibrary.SmartLoginConfig;
import studios.codelight.smartloginlibrary.SmartLoginFactory;
import studios.codelight.smartloginlibrary.UserSessionManager;
import studios.codelight.smartloginlibrary.users.SmartUser;
import studios.codelight.smartloginlibrary.util.SmartLoginException;

public class RegisterActivity extends TagUsJobActivity implements View.OnClickListener,SmartLoginCallbacks {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private ImageButton btnRegisterFacebook;
    private ImageButton btnRegisterGoogle;
    private EditText mInputName,mInputEmail,mInputPhone,mInputPassword,mInputRePassword;
    private Button btnRegisterSumbit;
    private SmUser user;
    private SmartLoginConfig config;
    private SmartUser currentUser;
    private SmartLogin smartLogin;
    private SocialUser socialUser;
    private Helper helper;
    private AuthService service = ServiceGenerator.createService(AuthService.class);
    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            Log.w(TAG,"onDenied");
        }
    };

    private Callback<Object> AUTH = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.isSuccessful()){
                GoogleResponse user = new Gson().fromJson(new Gson().toJson(response.body()),GoogleResponse.class);
                Log.e("user",new Gson().toJson(user));
                SocialUser socialUser = new SocialUser();
                socialUser.userId = user.getId();
                socialUser.photoUrl = user.getPicture();
                socialUser.profile = new SocialUser.Profile();
                socialUser.profile.fullName = user.getName();
                socialUser.profile.name = user.getName();
                socialUser.profile.pageLink = user.getLink();
                if (user.getEmail()!=null){
                    socialUser.profile.email = user.getEmail();
                }
                LoginSocial(socialUser);
            }else {
//                dismissProgress();
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Object> call, Throwable t) {
//            dismissProgress();
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private Callback<Token> LOGIN = new Callback<Token>() {
        @Override
        public void onResponse(Call<Token> call, Response<Token> response) {
            if (response.isSuccessful()){
                GetUser(response.body());
            }else {
//                dismissProgress();
                try {
                    Error error = new Gson().fromJson(response.errorBody().string(),Error.class);
                    ShowAlert("Sign Up",error.getError());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Token> call, Throwable t) {
//            dismissProgress();
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private Callback<ValidUser> VALID = new Callback<ValidUser>() {
        @Override
        public void onResponse(Call<ValidUser> call, Response<ValidUser> response) {
            if (response.isSuccessful()){
                if (socialUser.profile.email==null){
                    service.loginWeb("","",socialUser.userId,SuperConstance.PLATFORM_FACEBOOK).enqueue(LOGIN);
                }else {
                    service.loginWeb(socialUser.profile.email,SuperConstance.PLATFORM_FACEBOOK).enqueue(LOGIN);
                }
            }else {
                if (socialUser.profile.email==null){
//                    dismissProgress();
                    ShowAlert("Error!","No email.");
                }else {
                    service.registerUser(SuperConstance.PLATFORM_FACEBOOK,socialUser.profile.email,socialUser.profile.fullName,"","","").enqueue(REGISTER);
                }
            }
        }

        @Override
        public void onFailure(Call<ValidUser> call, Throwable t) {
//            dismissProgress();
            t.printStackTrace();
        }
    };

    private Callback<SmUser> REGISTER = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                user = response.body();
                currentUser = GetSmartUser(user);
            }else {
//                dismissProgress();
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
//            dismissProgress();
            ShowAlert("Connection Error!",t.getMessage());
        }
    };
    private String message = "";

    private void GetUser(Token token){
        service.Authenticate(token.getToken()).enqueue(new Callback<SmUser>() {
            @Override
            public void onResponse(Call<SmUser> call, Response<SmUser> response) {
                if (response.isSuccessful()){
                    currentUser = GetSmartUser(response.body());
                }else {
//                    dismissProgress();
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SmUser> call, Throwable t) {
//                dismissProgress();
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private SmartUser GetSmartUser(SmUser user){
        showProgress();
        SmartUser smartUser = new SmartUser();
        smartUser.setEmail(user.getEmail());
        smartUser.setBirthday(user.getDob());
        smartUser.setProfileLink(user.getProfile_link());
        smartUser.setGender(user.getGenderId());
        smartUser.setUserId(user.getProfile_id());
        smartUser.setUsername(user.getUser_name());
        smartUser.setFirstName(user.getFirs_name());
        smartUser.setLastName(user.getLast_name());
        new Auth(this).save(user);
        this.user = user;
        refreshLayout();
        return smartUser;
    }

    private void LoginSocial(SmartUser user){
        Log.e(TAG,new Gson().toJson(user));
        this.currentUser = user;
        if (user!=null){
            if (user.getEmail()!=null){
                service.validUser(SuperConstance.PLATFORM_FACEBOOK,user.getEmail(),user.getUserId()).enqueue(VALID);
            }else{
                service.validUser(SuperConstance.PLATFORM_FACEBOOK,"",user.getUserId()).enqueue(VALID);
            }
        }else {
//            dismissProgress();
            Log.e(TAG,"SmartUser is null");
        }
    }

    private void LoginSocial(SocialUser user){
        Log.e(TAG,new Gson().toJson(user));
        this.socialUser = user;
        if (user!=null){
            if (user.profile.email!=null){
                service.validUser(SuperConstance.PLATFORM_FACEBOOK,user.profile.email,user.userId).enqueue(VALID);
            }else{
                service.validUser(SuperConstance.PLATFORM_FACEBOOK,"",user.userId).enqueue(VALID);
            }
        }else {
//            dismissProgress();
            Log.e(TAG,"SmartUser is null");
        }
    }

    private void Register(String email,String user_name,String phone,String password){
        Log.e("form data","email: "+email+"\nuser_name: "+user_name+"\nphone: "+phone+"\npassword: "+password);
        service.registerUser(SuperConstance.PLATFORM_WEB,email,user_name,phone,password,password).enqueue(REGISTER);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,RegisterActivity.class);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        AutoPermissions.Companion.loadActivityPermissions(this, 1);
        /*Init Toolbar*/
        Toolbar toolbar = (Toolbar)findViewById(R.id.registerToolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                dismissProgress();
                onBackPressed();
            }
        });

        helper = new Helper(RegisterActivity.this);

        config = new SmartLoginConfig(this,this);
        config.setFacebookAppId(getString(R.string.facebook_app_id));
        config.setFacebookPermissions(SmartLoginConfig.getDefaultFacebookPermissions());
        config.setGoogleApiClient(null);

        /*init view components*/
        btnRegisterSumbit = (Button) findViewById(R.id.btnSignup);
        btnRegisterFacebook = (ImageButton)findViewById(R.id.btnFacebookLogin);
        btnRegisterGoogle = (ImageButton)findViewById(R.id.btnGoogleLogin);
        /*input view*/
        mInputName = (EditText)findViewById(R.id.user_name);
        mInputEmail = (EditText)findViewById(R.id.email);
        mInputPhone = (EditText)findViewById(R.id.phone);
        mInputPassword = (EditText)findViewById(R.id.password);
        mInputRePassword = (EditText)findViewById(R.id.repassword);
        /*init click event*/
        btnRegisterSumbit.setOnClickListener(this);
        btnRegisterFacebook.setOnClickListener(this);
        btnRegisterGoogle.setOnClickListener(this);

        /*event text change*/
    }

    private void refreshLayout() {
        currentUser = UserSessionManager.getCurrentUser(this);
        user = new Auth(this).checkAuth().token();
        if (user!=null) {
            if (user.getUser_type()==SmUser.NO_TYPE){
                startActivity(UserTypeActivity.createIntent(this));
            }else {
                startActivity(CategoryDrawer.createIntent(this));
            }
        } else {
//            dismissProgress();
            Log.e("Login","User not found");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (smartLogin != null) {
            smartLogin.onActivityResult(requestCode, resultCode, data, config);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private boolean isSave(){
        boolean isValid = true;
        String inputName = mInputName.getText().toString();
        String inputEmail = mInputEmail.getText().toString();
        String inputPhone = mInputPhone.getText().toString();
        String inputPassword = mInputPassword.getText().toString();
        String inputRePassword = mInputRePassword.getText().toString();

        if (inputName.isEmpty()){
            isValid = false;
            this.message = " * Full Name is required.\n";
        }else if(inputName.length() > 35){
            isValid = false;
            this.message += " * Full Name is limit 35 characters.\n";
        }

        if (inputEmail.isEmpty()){
            isValid = false;
            this.message += " * Email is required.\n";
        }else if(inputEmail.length() > 50){
            isValid = false;
            this.message += " * Email is limit 50 characters.\n";
        }else if(!inputEmail.contains("@")){
            isValid = false;
            this.message += " * Email not valid.\n";
        }

        if (inputPhone.isEmpty()){
            isValid = false;
            this.message += " * Phone is required.\n";
        }else if(inputPhone.length() > 20){
            this.message += " * Phone is limit 20 characters.\n";
            isValid = false;
        }

        if (inputPassword.isEmpty() || inputRePassword.isEmpty()){
            isValid = false;
            this.message += " * Password is required.\n";
        }else if(inputRePassword.length() < 5 || inputPassword.length() < 5){
            isValid = false;
            this.message += " * Password less than 6 characters.\n";
        }else if(!inputRePassword.equals(inputPassword)){
            this.message += " * Password not match.\n";
            isValid = false;
        }

        return  isValid;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    @Override
    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET})
    public void onClick(View view) {
        if(view==btnRegisterSumbit){
            smartLogin = SmartLoginFactory.build(LoginType.CustomSignup);
            smartLogin.signup(config);
        }else if (view==btnRegisterFacebook){
            loginByFacebook();
        }else if (view==btnRegisterGoogle){
            RxSocialConnect.with(RegisterActivity.this, helper.googleService())
                    .subscribe(response -> showResponse(response.token()),
                            error -> helper.showError(error));
        }
    }

    private void showResponse(OAuth2AccessToken token) {
        AuthService service = ServiceGenerator.createServices(AuthService.class);
        Log.e("token",String.valueOf(token));
        service.AuthGoogle(token.getTokenType() + " " + token.getAccessToken(),"application/json").enqueue(AUTH);
    }

    private void LoginWeb(){
        if (mInputEmail.getText().toString().length() > 0){
            service.loginWeb(mInputEmail.getText().toString(),mInputPassword.getText().toString(),SuperConstance.PLATFORM_WEB).enqueue(LOGIN);
        }else {
            service.loginWeb(mInputEmail.getText().toString(),SuperConstance.PLATFORM_WEB,1).enqueue(LOGIN);
        }
    }

    @Override
    public void onLoginSuccess(SmartUser user) {
        this.LoginSocial(user);
        refreshLayout();
    }

    @Override
    public void onLoginFailure(SmartLoginException e) {

        Log.e("Login failure",e.getMessage());
    }

    @Override
    public SmartUser doCustomLogin() {
        this.LoginWeb();
        return null;
    }

    @Override
    public SmartUser doCustomSignup() {
        if (isSave()){
//            showProgress();
            service.validUser(SuperConstance.PLATFORM_WEB,mInputEmail.getText().toString()).enqueue(new Callback<ValidUser>() {
                @Override
                public void onResponse(Call<ValidUser> call, Response<ValidUser> response) {
                    if (!response.isSuccessful()){
                        Register(mInputEmail.getText().toString(),mInputName.getText().toString(),mInputPhone.getText().toString(),mInputPassword.getText().toString());
                    }else {
//                        dismissProgress();
                        ShowAlert("Sign Up","User was registered!");
                    }
                }

                @Override
                public void onFailure(Call<ValidUser> call, Throwable t) {
//                    dismissProgress();
                    ShowAlert("Connection Error!",t.getMessage());
                }
            });
        }else {
            ShowAlert("Sign Up",this.message);
//            dismissProgress();
        }
        return currentUser;
    }

    private void loginByFacebook() {
        SocialLoginManager.getInstance(this)
                .facebook()
                .login()
                .subscribe(this::LoginSocial,
                        error -> {
                            ShowAlert("Facebook",error.getMessage());
                        });
    }

}

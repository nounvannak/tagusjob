package com.tagusnow.tagmejob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.github.ybq.android.spinkit.SpinKitView;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.SavedJobsAdapter;
import com.tagusnow.tagmejob.adapter.YourJobsAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SavedJobsActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    private static final String TAG = SavedJobsActivity.class.getSimpleName();
    public static final String COUNTER = "counter";
    private int counter = 0;
    private SmUser user;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private FloatingActionButton fabTop;
    private SpinKitView loading;
    private FeedResponse feedResponse;
    private SavedJobsAdapter adapter;
    private Button btnSearch;
    private Spinner spnCity,spnCategory;
    private int city_id=0,cate_id = 0;
    private SmLocation city;
    private SmTagSubCategory category;
    /*Service*/
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<FeedResponse> mNewCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initJobsPost(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> mNextCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextJobsPost(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            getJobsPost();
            refreshLayout.setRefreshing(false);
        }
    };

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    getNextJobsPost();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void getJobsPost(){
        int user = this.user!=null ? this.user.getId() : 0;
        int limit = 10;
        service.YourSavePost(user,1,this.cate_id,this.city_id,limit).enqueue(mNewCallback);
    }

    private void getNextJobsPost(){
        int user = this.user!=null ? this.user.getId() : 0;
        int limit = 10;
        int page = this.feedResponse.getCurrent_page() + 1;
        service.YourSavePost(user,1,this.cate_id,this.city_id,limit,page).enqueue(mNextCallback);
    }

    private void initJobsPost(FeedResponse feedResponse){
        if (feedResponse.getTotal() > feedResponse.getTo()){
            feedResponse.setTo(feedResponse.getTo() + SavedJobsAdapter.MORE_VIEW);
        }
        if (feedResponse.getTotal()==0 || feedResponse.getTotal()==feedResponse.getTo()){
            loading.setVisibility(View.GONE);
        }else {
            loading.setVisibility(View.VISIBLE);
        }
        this.feedResponse = feedResponse;
        this.checkNext();
        this.adapter = new SavedJobsAdapter(this,feedResponse,this.user);
        recyclerView.setAdapter(this.adapter);
    }

    private void initNextJobsPost(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        if (feedResponse.getTotal()==0 || feedResponse.getTotal()==feedResponse.getTo()){
            loading.setVisibility(View.GONE);
        }else {
            loading.setVisibility(View.VISIBLE);
        }
        this.checkNext();
        this.adapter.update(this,feedResponse,this.user).notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_jobs);
        initTemp();
        initView();
        getJobsPost();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SavedJobsActivity.class);
    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.counter = getIntent().getIntExtra(COUNTER,0);
        this.city = new Repository(this).restore().getLocation();
        this.category = new Repository(this).restore().getCategory();
    }

    private void initView(){
        fabTop = (FloatingActionButton)findViewById(R.id.fabTop);
        loading = (SpinKitView)findViewById(R.id.loading);
        btnSearch = (Button)findViewById(R.id.btnSearch);
        spnCity = (Spinner)findViewById(R.id.spnCity);
        spnCategory = (Spinner)findViewById(R.id.spnCategory);

        if (this.city!=null){
            List<String> citys = new ArrayList<String>();
            List<Integer> cityId = new ArrayList<>();
            citys.add("All City");
            cityId.add(0);
            for (SmLocation.Data city_ : this.city.getData()){
                citys.add(city_.getName());
                cityId.add(city_.getId());
            }
            spnCity.setAdapter(new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,citys));
            spnCity.setSelection(this.city_id);
            spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    city_id = cityId.get(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        if (this.category!=null){
            List<String> cates = new ArrayList<String>();
            List<Integer> cateID = new ArrayList<>();
            cates.add("All Position");
            cateID.add(0);
            for (Category cate_ : this.category.getData()){
                cates.add(cate_.getDescription());
                cateID.add(cate_.getId());
            }
            spnCategory.setAdapter(new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,cates));
            spnCategory.setSelection(this.cate_id);
            spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    cate_id = cateID.get(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        fabTop.hide();
        fabTop.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        initToolbar();
        initRefresh();
        initRecycler();
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy >= 0 && fabTop.getVisibility() == View.VISIBLE) {
                    fabTop.hide();
                } else if (dy < 0 && fabTop.getVisibility() != View.VISIBLE) {
                    fabTop.show();
                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initRefresh(){
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refreshLayout.setColorSchemeColors(getColor(R.color.colorAccent));
        refreshLayout.setOnRefreshListener(this);
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRunnable,2000);
    }

    @Override
    public void onClick(View view) {
        if (view==fabTop){
            recyclerView.smoothScrollToPosition(0);
            fabTop.hide();
        }else if (view==btnSearch){
            onRefresh();
        }
    }
}

package com.tagusnow.tagmejob;

public class ScreenUtil {
    private static int COMPARE_SCREEN = 1920;
    private static int BIG_SCREEN = 485;
    private static int SMALL_SCREEN = 480;
    private static double LDPI = 0.75;
    private static int MDPI = 1;
    private static double HDPI = 1.5;
    private static int XHDPI = 2;
    private static int XXHDPI = 3;
    private static int XXXHDPI = 4;
    private static int DENSITY_ldpi = 120;
    private static int DENSITY_mdpi = 160;
    private static int DENSITY_hdpi = 240;
    private static int DENSITY_xhdpi = 320;
    private static int DENSITY_xxhdpi = 480;
    private static int DENSITY_xxxhdpi = 560;

    public static double getHeightFeedBody(int density){
        double height = 0;
        if (density==DENSITY_ldpi){
            height = SMALL_SCREEN * LDPI;
        }else if(density==DENSITY_mdpi){
            height = SMALL_SCREEN * MDPI;
        }else if(density==DENSITY_hdpi){
            height = SMALL_SCREEN * HDPI;
        }else if(density==DENSITY_xhdpi){
            height = SMALL_SCREEN * XHDPI;
        }else if(density==DENSITY_xxhdpi){
            height = SMALL_SCREEN * XXHDPI;
        }else if(density==DENSITY_xxxhdpi){
            height = BIG_SCREEN * XXXHDPI;
        }else if(density>DENSITY_xxxhdpi){
            height = BIG_SCREEN * XXXHDPI;
        }
        return height;
    }
}

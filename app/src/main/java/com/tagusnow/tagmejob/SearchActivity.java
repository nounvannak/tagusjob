package com.tagusnow.tagmejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.V2.Model.CompanyPaginate;
import com.tagusnow.tagmejob.adapter.SearchAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.v3.company.CompanyCard;
import com.tagusnow.tagmejob.v3.company.CompanyListener;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedListener;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJob;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.v3.like.LikeResultCell;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.v3.search.OnItemListener;
import com.tagusnow.tagmejob.v3.search.SearchForListener;
import com.tagusnow.tagmejob.v3.search.SearchSuggestRecentHeaderListener;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.v3.search.adapter.JobsSuggestionAdapter;
import com.tagusnow.tagmejob.v3.search.adapter.StaffSuggestionAdapter;
import com.tagusnow.tagmejob.v3.search.result.SearchForCompany;
import com.tagusnow.tagmejob.v3.search.result.SearchForCompanyAdapter;
import com.tagusnow.tagmejob.v3.search.result.SearchForContent;
import com.tagusnow.tagmejob.v3.search.result.SearchForContentAdapter;
import com.tagusnow.tagmejob.v3.search.result.SearchForJob;
import com.tagusnow.tagmejob.v3.search.result.SearchForJobAdapter;
import com.tagusnow.tagmejob.v3.search.result.SearchForPeople;
import com.tagusnow.tagmejob.v3.search.result.SearchForPeopleAdapter;
import com.tagusnow.tagmejob.v3.search.result.SearchForStaff;
import com.tagusnow.tagmejob.v3.search.result.SearchForStaffAdapter;
import com.tagusnow.tagmejob.v3.user.UserCardThree;
import com.tagusnow.tagmejob.v3.user.UserCardThreeListener;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.JobsSuggestionLayout;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.SearchForLayout;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.StaffSuggestionLayout;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.JobsSuggestionView;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.StaffSuggestionView;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestRecentHeader;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends TagUsJobActivity implements TextWatcher, FeedNormalListener, SmallFeedJobListener, GridViewListener, OnItemListener, SearchForListener, SearchSuggestRecentHeaderListener, SuggestionListener, UserCardThreeListener, CompanyListener<CompanyCard>, LikeResultListener, FeedListener, LoadingListener<LoadingView> {

    private final static String TAG = SearchActivity.class.getSimpleName();
    private static final String ChannelID = UUID.randomUUID().toString();
    private Toolbar toolbarSearch;
    private EditText txtSearch;
    private RecyclerView recyclerView;
    private FeedResponse jobsPaginate,staffPaginate,contentPaginate,jobSuggestPaginate,staffSuggestPaginate;
    private CompanyPaginate companyPaginate;
    private UserPaginate userPaginate;
    private List<SmUser> users;
    private List<Company> companies;
    private List<Feed> jobSuggestFeeds,staffSuggestFeeds,jobFeeds,staffFeeds,contentFeeds;
    private List<RecentSearch> recentSearches;
    private RecentSearchPaginate recentSearchPaginate;
    private SearchAdapter adapter;
    private String query_search;
    private LoadingView loadingView;
    private SmUser Auth;

    private static boolean isRequestJobSuggest = false;
    private static int lastPageOfJobSuggest = 0;
    private static boolean isRequestStaffSuggest = false;
    private static int lastPageOfStaffSuggest = 0;

    private JobsSuggestionAdapter jobsSuggestionAdapter;
    private StaffSuggestionAdapter staffSuggestionAdapter;
    private SearchForPeopleAdapter searchForPeopleAdapter;
    private SearchForCompanyAdapter searchForCompanyAdapter;
    private SearchForJobAdapter searchForJobAdapter;
    private SearchForStaffAdapter searchForStaffAdapter;
    private SearchForContentAdapter searchForContentAdapter;

    private JobsSuggestionLayout jobsSuggestionLayout;
    private StaffSuggestionLayout staffSuggestionLayout;
    private SearchForLayout searchForLayout;
    private SearchForPeople searchForPeople;
    private SearchForCompany searchForCompany;
    private SearchForJob searchForJob;
    private SearchForStaff searchForStaff;
    private SearchForContent searchForContent;

    /* Socket.io */
    Socket socket;
    private Emitter.Listener NEW_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
        }
    };
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int feedID = obj.getInt("feed_id");
                    int totalLikes = obj.getInt("count_likes");
                    int userID = obj.getInt("user_id");
                    String likesResult = obj.getString("like_result");
                    String channelID = obj.getString("channel_id");

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        // Recent Search
                        Optional<RecentSearch> recentSearchOptional = recentSearches.stream().filter(r -> r.getFeed() != null
                                && r.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)
                                && r.getFeed().getId() == feedID).findFirst();
                        if (recentSearchOptional.isPresent()){
                            RecentSearch recentSearch = recentSearchOptional.get();
                            int index = recentSearches.indexOf(recentSearch);
                            recentSearch.getFeed().getFeed().setCount_likes(totalLikes);
                            recentSearch.getFeed().getFeed().setLike_result(likesResult);
                            recentSearch.getFeed().getFeed().setIs_like(userID == Auth.getId());
                            recentSearches.set(index,recentSearch);
                            adapter.notifyDataSetChanged();

                        }
                    }else {
                        if (recentSearches != null && recentSearches.size() > 0){
                            for (RecentSearch recentSearch : recentSearches){
                                if (recentSearch.getFeed() != null && recentSearch.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST) && recentSearch.getFeed().getId() == feedID){
                                    int index = recentSearches.indexOf(recentSearch);
                                    recentSearch.getFeed().getFeed().setCount_likes(totalLikes);
                                    recentSearch.getFeed().getFeed().setLike_result(likesResult);
                                    recentSearch.getFeed().getFeed().setIs_like(userID == Auth.getId());
                                    recentSearches.set(index,recentSearch);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }

                    // Result Search
                    if (contentFeeds != null && contentFeeds.size() > 0){
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            Optional<Feed> optionalFeed = contentFeeds.stream().filter(feed -> feed.getId() == feedID).findFirst();
                            if (optionalFeed.isPresent()){
                                Feed feed = optionalFeed.get();
                                int index = contentFeeds.indexOf(feed);
                                feed.getFeed().setIs_like(userID == Auth.getId());
                                feed.getFeed().setLike_result(likesResult);
                                feed.getFeed().setCount_likes(totalLikes);
                                contentFeeds.set(index,feed);
                                searchForContentAdapter.notifyDataSetChanged();
                            }
                        }else {
                            for (Feed feed : contentFeeds){
                                if (feed.getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST) && feed.getId() == feedID){
                                    int index = contentFeeds.indexOf(feed);
                                    feed.getFeed().setIs_like(userID == Auth.getId());
                                    feed.getFeed().setLike_result(likesResult);
                                    feed.getFeed().setCount_likes(totalLikes);
                                    contentFeeds.set(index,feed);
                                    searchForContentAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int feedID = obj.getInt("feed_id");
                    int totalComments = obj.getInt("count_comments");

                    // Recent Search
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        Optional<RecentSearch> recentSearchOptional = recentSearches.stream().filter(r -> r.getFeed() != null
                                && r.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)
                                && r.getFeed().getId() == feedID).findFirst();
                        if (recentSearchOptional.isPresent()){
                            RecentSearch recentSearch = recentSearchOptional.get();
                            int index = recentSearches.indexOf(recentSearch);
                            recentSearch.getFeed().getFeed().setCount_comments(totalComments);
                            recentSearch.getFeed().getFeed().setIs_like(false);
                            recentSearches.set(index,recentSearch);
                            adapter.notifyDataSetChanged();
                        }
                    }else {
                        if (recentSearches != null && recentSearches.size() > 0){
                            for (RecentSearch recentSearch : recentSearches){
                                if (recentSearch.getFeed() != null && recentSearch.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST) && recentSearch.getFeed().getId() == feedID){
                                    int index = recentSearches.indexOf(recentSearch);
                                    recentSearch.getFeed().getFeed().setCount_comments(totalComments);
                                    recentSearch.getFeed().getFeed().setIs_like(false);
                                    recentSearches.set(index,recentSearch);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }

                    // Result Search
                    if (contentFeeds != null && contentFeeds.size() > 0){
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            Optional<Feed> optionalFeed = contentFeeds.stream().filter(feed -> feed.getId() == feedID).findFirst();
                            if (optionalFeed.isPresent()){
                                Feed feed = optionalFeed.get();
                                int index = contentFeeds.indexOf(feed);
                                feed.getFeed().setCount_comments(totalComments);
                                contentFeeds.set(index,feed);
                                searchForContentAdapter.notifyDataSetChanged();
                            }
                        }else {
                            for (Feed feed : contentFeeds){
                                if (feed.getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST) && feed.getId() == feedID){
                                    int index = contentFeeds.indexOf(feed);
                                    feed.getFeed().setCount_comments(totalComments);
                                    contentFeeds.set(index,feed);
                                    searchForContentAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                    if (feedJson != null){
                        // Recent Search
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            Optional<RecentSearch> recentSearchOptional = recentSearches.stream().filter(r -> r.getFeed() != null
//                                    && r.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)
                                    && r.getFeed().getId() == feedJson.getId()).findFirst();
                            if (recentSearchOptional.isPresent()){
                                RecentSearch recentSearch = recentSearchOptional.get();
                                int index = recentSearches.indexOf(recentSearch);
                                recentSearch.getFeed().setFeed(feedJson.getFeed());
                                recentSearches.set(index,recentSearch);
                                adapter.notifyDataSetChanged();
                            }
                        }else {
                            if (recentSearches != null && recentSearches.size() > 0){
                                for (RecentSearch recentSearch : recentSearches){
                                    if (recentSearch.getFeed() != null
//                                            && recentSearch.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)
                                            && recentSearch.getFeed().getId() == feedJson.getId()){
                                        int index = recentSearches.indexOf(recentSearch);
                                        recentSearch.getFeed().setFeed(feedJson.getFeed());
                                        recentSearches.set(index,recentSearch);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }

                        /*Suggest Job*/
                        if (jobSuggestFeeds != null && jobSuggestFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = jobSuggestFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    int index = jobSuggestFeeds.indexOf(feed);
                                    Feed updateFeed = jobSuggestFeeds.get(index);
                                    updateFeed.setFeed(feedJson.getFeed());
                                    jobSuggestFeeds.set(index,updateFeed);
                                }
                            }else {
                                for (Feed feed : jobSuggestFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        int index = jobSuggestFeeds.indexOf(feed);
                                        Feed updateFeed = jobSuggestFeeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        jobSuggestFeeds.set(index,updateFeed);
                                    }
                                }
                            }

                            jobsSuggestionAdapter.notifyDataSetChanged();
                        }
                        /*Suggest Staff*/
                        if (staffSuggestFeeds != null && staffSuggestFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = staffSuggestFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    int index = staffSuggestFeeds.indexOf(feed);
                                    Feed updateFeed = staffSuggestFeeds.get(index);
                                    updateFeed.setFeed(feedJson.getFeed());
                                    staffSuggestFeeds.set(index,updateFeed);
                                }
                            }else {
                                for (Feed feed : staffSuggestFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        int index = staffSuggestFeeds.indexOf(feed);
                                        Feed updateFeed = staffSuggestFeeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        staffSuggestFeeds.set(index,updateFeed);
                                    }
                                }
                            }

                            staffSuggestionAdapter.notifyDataSetChanged();
                        }

                        // Result Search
                        /*NORMAL*/
                        if (contentFeeds != null && contentFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = contentFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    int index = contentFeeds.indexOf(feed);
                                    Feed updateFeed = contentFeeds.get(index);
                                    updateFeed.setFeed(feedJson.getFeed());
                                    contentFeeds.set(index,updateFeed);
                                }
                            }else {
                                for (Feed feed : contentFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        int index = contentFeeds.indexOf(feed);
                                        Feed updateFeed = contentFeeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        contentFeeds.set(index,updateFeed);
                                    }
                                }
                            }
                            searchForContentAdapter.notifyDataSetChanged();
                        }
                        /*JOB*/
                        if (jobFeeds != null && jobFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = jobFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    int index = jobFeeds.indexOf(feed);
                                    Feed updateFeed = jobFeeds.get(index);
                                    updateFeed.setFeed(feedJson.getFeed());
                                    jobFeeds.set(index,updateFeed);
                                }
                            }else {
                                for (Feed feed : jobFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        int index = jobFeeds.indexOf(feed);
                                        Feed updateFeed = jobFeeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        jobFeeds.set(index,updateFeed);
                                    }
                                }
                            }
                            searchForJobAdapter.notifyDataSetChanged();
                        }
                        /*STAFF*/
                        if (staffFeeds != null && staffFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = staffFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    int index = staffFeeds.indexOf(feed);
                                    Feed updateFeed = staffFeeds.get(index);
                                    updateFeed.setFeed(feedJson.getFeed());
                                    staffFeeds.set(index,updateFeed);
                                }
                            }else {
                                for (Feed feed : staffFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        int index = staffFeeds.indexOf(feed);
                                        Feed updateFeed = staffFeeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        staffFeeds.set(index,updateFeed);
                                    }
                                }
                            }
                            searchForStaffAdapter.notifyDataSetChanged();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                    if (feedJson != null){
                        // Recent Search
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            Optional<RecentSearch> recentSearchOptional = recentSearches.stream().filter(r -> r.getFeed() != null
                                    && r.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)
                                    && r.getFeed().getId() == feedJson.getId()).findFirst();
                            if (recentSearchOptional.isPresent()){
                                RecentSearch recentSearch = recentSearchOptional.get();
                                int index = recentSearches.indexOf(recentSearch);
                                recentSearch.setFeed(feedJson);
                                recentSearches.remove(index);
                                adapter.notifyDataSetChanged();
                            }
                        }else {
                            if (recentSearches != null && recentSearches.size() > 0){
                                for (RecentSearch recentSearch : recentSearches){
                                    if (recentSearch.getFeed() != null && recentSearch.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST) && recentSearch.getFeed().getId() == feedJson.getId()){
                                        int index = recentSearches.indexOf(recentSearch);
                                        recentSearch.setFeed(feedJson);
                                        recentSearches.remove(index);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }

                        /*Suggest Job*/
                        if (jobSuggestFeeds != null && jobSuggestFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = jobSuggestFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    jobSuggestFeeds.remove(feed);
                                }
                            }else {
                                for (Feed feed : jobSuggestFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        jobSuggestFeeds.remove(feed);
                                    }
                                }
                            }

                            jobsSuggestionAdapter.notifyDataSetChanged();
                        }
                        /*Suggest Staff*/
                        if (staffSuggestFeeds != null && staffSuggestFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = staffSuggestFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    staffSuggestFeeds.remove(feed);
                                }
                            }else {
                                for (Feed feed : staffSuggestFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        staffSuggestFeeds.remove(feed);
                                    }
                                }
                            }

                            staffSuggestionAdapter.notifyDataSetChanged();
                        }

                        // Result Search
                        /*NORMAL*/
                        if (contentFeeds != null && contentFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = contentFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    contentFeeds.remove(feed);
                                }
                            }else {
                                for (Feed feed : contentFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        contentFeeds.remove(feed);
                                    }
                                }
                            }
                            searchForContentAdapter.notifyDataSetChanged();
                        }
                        /*JOB*/
                        if (jobFeeds != null && jobFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = jobFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    jobFeeds.remove(feed);
                                }
                            }else {
                                for (Feed feed : jobFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        jobFeeds.remove(feed);
                                    }
                                }
                            }
                            searchForJobAdapter.notifyDataSetChanged();
                        }
                        /*STAFF*/
                        if (staffFeeds != null && staffFeeds.size() > 0){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<Feed> optionalFeed = staffFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                if (optionalFeed.isPresent()){
                                    Feed feed = optionalFeed.get();
                                    staffFeeds.remove(feed);
                                }
                            }else {
                                for (Feed feed : staffFeeds){
                                    if (feed.getId() == feedJson.getId()){
                                        staffFeeds.remove(feed);
                                    }
                                }
                            }
                            searchForStaffAdapter.notifyDataSetChanged();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int userID = obj.getInt("user_id");
                    if (Auth.getId() == userID){
                        Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                        if (feedJson != null){
                            // Recent Search
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                Optional<RecentSearch> recentSearchOptional = recentSearches.stream().filter(r -> r.getFeed() != null
                                        && r.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)
                                        && r.getFeed().getId() == feedJson.getId()).findFirst();
                                if (recentSearchOptional.isPresent()){
                                    RecentSearch recentSearch = recentSearchOptional.get();
                                    int index = recentSearches.indexOf(recentSearch);
                                    recentSearch.setFeed(feedJson);
                                    recentSearches.remove(index);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                if (recentSearches != null && recentSearches.size() > 0){
                                    for (RecentSearch recentSearch : recentSearches){
                                        if (recentSearch.getFeed() != null && recentSearch.getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST) && recentSearch.getFeed().getId() == feedJson.getId()){
                                            int index = recentSearches.indexOf(recentSearch);
                                            recentSearch.setFeed(feedJson);
                                            recentSearches.remove(index);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }

                            /*Suggest Job*/
                            if (jobSuggestFeeds != null && jobSuggestFeeds.size() > 0){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    Optional<Feed> optionalFeed = jobSuggestFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                    if (optionalFeed.isPresent()){
                                        Feed feed = optionalFeed.get();
                                        jobSuggestFeeds.remove(feed);
                                    }
                                }else {
                                    for (Feed feed : jobSuggestFeeds){
                                        if (feed.getId() == feedJson.getId()){
                                            jobSuggestFeeds.remove(feed);
                                        }
                                    }
                                }

                                jobsSuggestionAdapter.notifyDataSetChanged();
                            }
                            /*Suggest Staff*/
                            if (staffSuggestFeeds != null && staffSuggestFeeds.size() > 0){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    Optional<Feed> optionalFeed = staffSuggestFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                    if (optionalFeed.isPresent()){
                                        Feed feed = optionalFeed.get();
                                        staffSuggestFeeds.remove(feed);
                                    }
                                }else {
                                    for (Feed feed : staffSuggestFeeds){
                                        if (feed.getId() == feedJson.getId()){
                                            staffSuggestFeeds.remove(feed);
                                        }
                                    }
                                }

                                staffSuggestionAdapter.notifyDataSetChanged();
                            }

                            // Result Search
                            /*NORMAL*/
                            if (contentFeeds != null && contentFeeds.size() > 0){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    Optional<Feed> optionalFeed = contentFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                    if (optionalFeed.isPresent()){
                                        Feed feed = optionalFeed.get();
                                        contentFeeds.remove(feed);
                                    }
                                }else {
                                    for (Feed feed : contentFeeds){
                                        if (feed.getId() == feedJson.getId()){
                                            contentFeeds.remove(feed);
                                        }
                                    }
                                }
                                searchForContentAdapter.notifyDataSetChanged();
                            }
                            /*JOB*/
                            if (jobFeeds != null && jobFeeds.size() > 0){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    Optional<Feed> optionalFeed = jobFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                    if (optionalFeed.isPresent()){
                                        Feed feed = optionalFeed.get();
                                        jobFeeds.remove(feed);
                                    }
                                }else {
                                    for (Feed feed : jobFeeds){
                                        if (feed.getId() == feedJson.getId()){
                                            jobFeeds.remove(feed);
                                        }
                                    }
                                }
                                searchForJobAdapter.notifyDataSetChanged();
                            }
                            /*STAFF*/
                            if (staffFeeds != null && staffFeeds.size() > 0){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    Optional<Feed> optionalFeed = staffFeeds.stream().filter(feed -> feed.getId() == feedJson.getId()).findFirst();
                                    if (optionalFeed.isPresent()){
                                        Feed feed = optionalFeed.get();
                                        staffFeeds.remove(feed);
                                    }
                                }else {
                                    for (Feed feed : staffFeeds){
                                        if (feed.getId() == feedJson.getId()){
                                            staffFeeds.remove(feed);
                                        }
                                    }
                                }
                                searchForStaffAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener FOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int userID = obj.getInt("auth_user");
                    JSONObject json = obj.getJSONObject("user");
                    if (Auth.getId() == userID){
                        if (json != null){
                            SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                            SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                            if (follower != null && following != null){
                                int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        SmUser user = lists.get(0);
                                        int index = users.indexOf(user);
                                        follower.setIs_follow(true);
                                        if (following.getId() == userID){
                                            users.set(index,follower);
                                            searchForPeopleAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    for (SmUser user : users){
                                        if (user.getId() == id){
                                            int index = users.indexOf(user);
                                            follower.setIs_follow(true);
                                            if (following.getId() == userID){
                                                users.set(index,follower);
                                                searchForPeopleAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener UNFOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int userID = obj.getInt("auth_user");
                    JSONObject json = obj.getJSONObject("user");
                    if (Auth.getId() == userID){
                        if (json != null){
                            SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                            SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                            if (follower != null && following != null){
                                int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        SmUser user = lists.get(0);
                                        int index = users.indexOf(user);
                                        follower.setIs_follow(false);
                                        if (following.getId() == userID){
                                            users.set(index,follower);
                                            searchForPeopleAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    for (SmUser user : users){
                                        if (user.getId() == id){
                                            int index = users.indexOf(user);
                                            follower.setIs_follow(false);
                                            if (following.getId() == userID){
                                                users.set(index,follower);
                                                searchForPeopleAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    };
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("New Post",NEW_POST);
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.on("Follow User",FOLLOW_USER);
        socket.on("Unfollow User",UNFOLLOW_USER);
        socket.connect();
    }
    /* End Socket.io */

    /*Service*/
    private RecentSearchService searchService = ServiceGenerator.createService(RecentSearchService.class);
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private UserService userService = ServiceGenerator.createService(UserService.class);
    private Callback<RecentSearchPaginate> FetchRecentCallback = new Callback<RecentSearchPaginate>() {
        @Override
        public void onResponse(Call<RecentSearchPaginate> call, Response<RecentSearchPaginate> response) {
            if (response.isSuccessful()){
                fetchRecent(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearchPaginate> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<RecentSearchPaginate> FetchNextRecentCallback = new Callback<RecentSearchPaginate>() {
        @Override
        public void onResponse(Call<RecentSearchPaginate> call, Response<RecentSearchPaginate> response) {
            if (response.isSuccessful()){
                fetchNextRecent(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearchPaginate> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<FeedResponse> FetchSuggestJobCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchSuggestJob(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchNextSuggestJobCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchNextSuggestJob(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchSuggestStaffCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchSuggestStaff(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchNextSuggestStaffCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchNextSuggestStaff(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> FetchPeopleCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                fetchPeople(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<CompanyPaginate> FetchCompanyCallback = new Callback<CompanyPaginate>() {
        @Override
        public void onResponse(Call<CompanyPaginate> call, Response<CompanyPaginate> response) {
            if (response.isSuccessful()){
                fetchCompany(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CompanyPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchJobCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchJob(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchStaffCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchStaff(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchContentCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchContent(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (recentSearchPaginate.getTo() - 1)) && (recentSearchPaginate.getTo() < recentSearchPaginate.getTotal())) {
                        if (recentSearchPaginate.getNext_page_url()!=null || !recentSearchPaginate.getNext_page_url().equals("")){
                            if (lastPage!=recentSearchPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = recentSearchPaginate.getCurrent_page();
                                    fetchNextRecent();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void CheckNextOfJobSuggest(){
        if (jobsSuggestionLayout != null){
            jobsSuggestionLayout.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (jobSuggestPaginate.getTo() - 1)) && (jobSuggestPaginate.getTo() < jobSuggestPaginate.getTotal())) {
                            if (jobSuggestPaginate.getNext_page_url()!=null || !jobSuggestPaginate.getNext_page_url().equals("")){
                                if (lastPageOfJobSuggest!=jobSuggestPaginate.getCurrent_page()){
                                    lastPageOfJobSuggest = jobSuggestPaginate.getCurrent_page();
                                    fetchNextSuggestJob();
                                    isRequestJobSuggest = true;
                                }else{
                                    isRequestJobSuggest = false;
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void CheckNextOfStaffSuggest(){
        if (staffSuggestionLayout != null){
            staffSuggestionLayout.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (staffSuggestPaginate.getTo() - 1)) && (staffSuggestPaginate.getTo() < staffSuggestPaginate.getTotal())) {
                            if (staffSuggestPaginate.getNext_page_url()!=null || !staffSuggestPaginate.getNext_page_url().equals("")){
                                if (lastPageOfStaffSuggest!=staffSuggestPaginate.getCurrent_page()){
                                    lastPageOfStaffSuggest = staffSuggestPaginate.getCurrent_page();
                                    fetchNextSuggestStaff();
                                    isRequestStaffSuggest = true;
                                }else{
                                    isRequestStaffSuggest = false;
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        InitTemp();
        initSocket();

        toolbarSearch = (Toolbar)findViewById(R.id.toolbarSearch);
        txtSearch = (EditText)findViewById(R.id.txtSearch);
        setSupportActionBar(toolbarSearch);
        toolbarSearch.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        txtSearch.addTextChangedListener(this);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new SearchAdapter(this,Auth,this,this,this,this,this,this,this,this);
        adapter.isRecent(true);
        loadingView = adapter.getLoadingView();
        jobsSuggestionLayout = adapter.getJobsSuggestionLayout();
        staffSuggestionLayout = adapter.getStaffSuggestionLayout();
        searchForLayout = adapter.getSearchForLayout();
        searchForPeople = adapter.getSearchForPeople();
        searchForCompany = adapter.getSearchForCompany();
        searchForJob = adapter.getSearchForJob();
        searchForStaff = adapter.getSearchForStaff();
        searchForContent = adapter.getSearchForContent();

        fetchRecent();

    }

    private void fetchRecent(){
        searchService.hasRecentSearch_(Auth.getId(),RecentSearch.ALL,10).enqueue(FetchRecentCallback);
    }

    private void fetchRecent(RecentSearchPaginate recentSearchPaginate){
        this.recentSearchPaginate = recentSearchPaginate;
        this.recentSearches = recentSearchPaginate.getData();
        CheckNext();
        adapter.NewData(recentSearches);
        recyclerView.setAdapter(adapter);
        loadingView.setLoading(recentSearchPaginate.getTotal() > recentSearchPaginate.getTo());

        fetchSuggestJob();
        fetchSuggestStaff();
    }

    private void fetchSearch(String query_search){
        this.query_search = query_search;
        adapter.isRecent(false);

        fetchPeople();
        fetchCompany();
        fetchJob();
        fetchStaff();
        fetchContent();
    }

    private void fetchNextRecent(){
        boolean isNext = recentSearchPaginate.getCurrent_page() != recentSearchPaginate.getLast_page();
        if (isNext){
            int page = recentSearchPaginate.getCurrent_page() + 1;
            searchService.hasRecentSearch_(Auth.getId(),RecentSearch.ALL,10,page).enqueue(FetchNextRecentCallback);
        }
    }

    private void fetchNextRecent(RecentSearchPaginate recentSearchPaginate){
        this.recentSearchPaginate = recentSearchPaginate;
        this.recentSearches.addAll(recentSearchPaginate.getData());
        CheckNext();
        loadingView.setLoading(recentSearchPaginate.getTotal() > recentSearchPaginate.getTo());
        adapter.NextData(recentSearchPaginate.getData());
    }

    private void fetchSuggestJob(){
        service.Suggest(Auth.getId(),1,1,10).enqueue(FetchSuggestJobCallback);
    }

    private void fetchSuggestJob(FeedResponse jobSuggestPaginate){
        this.jobSuggestPaginate = jobSuggestPaginate;
        this.jobSuggestFeeds = jobSuggestPaginate.getData();
        CheckNextOfJobSuggest();

        jobsSuggestionAdapter = new JobsSuggestionAdapter(this,jobSuggestPaginate.getData(),this);

        if (jobsSuggestionLayout != null){
            jobsSuggestionLayout.setAdapter(jobsSuggestionAdapter);
        }else {
            jobsSuggestionLayout = adapter.getJobsSuggestionLayout();
            jobsSuggestionLayout.setAdapter(jobsSuggestionAdapter);
        }
    }

    private void fetchNextSuggestJob(){
        boolean isNext = jobSuggestPaginate.getCurrent_page() != jobSuggestPaginate.getLast_page();
        if (isNext){
            int page = jobSuggestPaginate.getCurrent_page() + 1;
            service.Suggest(Auth.getId(),1,1,10,page).enqueue(FetchNextSuggestJobCallback);
        }
    }

    private void fetchNextSuggestJob(FeedResponse jobSuggestPaginate){
        this.jobSuggestPaginate = jobSuggestPaginate;
        this.jobSuggestFeeds.addAll(jobSuggestPaginate.getData());
        CheckNextOfJobSuggest();
        jobsSuggestionAdapter.NextData(jobSuggestPaginate.getData());
    }

    private void fetchSuggestStaff(){
        service.Suggest(Auth.getId(),1,2,10).enqueue(FetchSuggestStaffCallback);
    }

    private void fetchSuggestStaff(FeedResponse staffSuggestPaginate){
        this.staffSuggestPaginate = staffSuggestPaginate;
        this.staffSuggestFeeds = staffSuggestPaginate.getData();
        CheckNextOfStaffSuggest();
        staffSuggestionAdapter = new StaffSuggestionAdapter(this,staffSuggestPaginate.getData(),this);

        if (staffSuggestionLayout != null){
            staffSuggestionLayout.setAdapter(staffSuggestionAdapter);
        }else {
            staffSuggestionLayout = adapter.getStaffSuggestionLayout();
            staffSuggestionLayout.setAdapter(staffSuggestionAdapter);
        }
    }

    private void fetchNextSuggestStaff(){
        boolean isNext = staffSuggestPaginate.getCurrent_page() != staffSuggestPaginate.getLast_page();
        if (isNext){
            int page = staffSuggestPaginate.getCurrent_page() + 1;
            service.Suggest(Auth.getId(),1,2,10,page).enqueue(FetchNextSuggestStaffCallback);
        }
    }

    private void fetchNextSuggestStaff(FeedResponse staffSuggestPaginate){
        this.staffSuggestPaginate = staffSuggestPaginate;
        this.staffSuggestFeeds.addAll(staffSuggestPaginate.getData());
        CheckNextOfStaffSuggest();
        staffSuggestionAdapter.NextData(staffSuggestPaginate.getData());
    }

    private void fetchPeople(){
        userService.UserList(Auth.getId(),0,query_search,6).enqueue(FetchPeopleCallback);
    }

    private void fetchPeople(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users = userPaginate.getData();
        searchForPeopleAdapter = new SearchForPeopleAdapter(this,userPaginate.getData(),this);

        if (searchForPeople != null){
            searchForPeople.setTotal(userPaginate.getTotal());
            searchForPeople.setAdapter(searchForPeopleAdapter);
        }else {
            searchForPeople = adapter.getSearchForPeople();
            searchForPeople.setTotal(userPaginate.getTotal());
            searchForPeople.setAdapter(searchForPeopleAdapter);
        }
    }

    private void fetchCompany(){
        service.GetCompany(Auth.getId(),query_search,5).enqueue(FetchCompanyCallback);
    }

    private void fetchCompany(CompanyPaginate companyPaginate){
        this.companyPaginate = companyPaginate;
        this.companies = companyPaginate.getData();
        searchForCompanyAdapter = new SearchForCompanyAdapter(this,companyPaginate.getData(),this);

        if (searchForCompany != null){
            searchForCompany.setTotal(companyPaginate.getTotal());
            searchForCompany.setAdapter(searchForCompanyAdapter);
        }else {
            searchForCompany = adapter.getSearchForCompany();
            searchForCompany.setTotal(companyPaginate.getTotal());
            searchForCompany.setAdapter(searchForCompanyAdapter);
        }
    }

    private void fetchJob(){
        service.Search(Auth.getId(),1,query_search,0,0,5).enqueue(FetchJobCallback);
    }

    private void fetchJob(FeedResponse jobsPaginate){
        this.jobsPaginate = jobsPaginate;
        this.jobFeeds = jobsPaginate.getData();
        searchForJobAdapter = new SearchForJobAdapter(this,jobsPaginate.getData(),this);

        if (searchForJob != null){
            searchForJob.setTotal(jobsPaginate.getTotal());
            searchForJob.setAdapter(searchForJobAdapter);
        }else {
            searchForJob = adapter.getSearchForJob();
            searchForJob.setTotal(jobsPaginate.getTotal());
            searchForJob.setAdapter(searchForJobAdapter);
        }
    }

    private void fetchStaff(){
        service.Search(Auth.getId(),2,query_search,0,0,6).enqueue(FetchStaffCallback);
    }

    private void fetchStaff(FeedResponse staffPaginate){
        this.staffPaginate = staffPaginate;
        this.staffFeeds = staffPaginate.getData();
        searchForStaffAdapter = new SearchForStaffAdapter(this,staffPaginate.getData(),this);

        if (searchForStaff != null){
            searchForStaff.setTotal(staffPaginate.getTotal());
            searchForStaff.setAdapter(searchForStaffAdapter);
        }else {
            searchForStaff = adapter.getSearchForStaff();
            searchForStaff.setTotal(staffPaginate.getTotal());
            searchForStaff.setAdapter(searchForStaffAdapter);
        }
    }

    private void fetchContent(){
        service.Search(Auth.getId(),3,query_search,0,0,5).enqueue(FetchContentCallback);
    }

    private void fetchContent(FeedResponse contentPaginate){
        this.contentPaginate = contentPaginate;
        this.contentFeeds = contentPaginate.getData();
        searchForContentAdapter = new SearchForContentAdapter(this,contentPaginate.getData(),this,this);

        if (searchForContent != null){
            searchForContent.setTotal(contentPaginate.getTotal());
            searchForContent.setAdapter(searchForContentAdapter);
        }else {
            searchForContent = adapter.getSearchForContent();
            searchForContent.setTotal(contentPaginate.getTotal());
            searchForContent.setAdapter(searchForContentAdapter);
        }
    }

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchActivity.class);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        fetchSearch(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {}

    private void LikeFeed(SmUser Auth, Feed feed){
        if (!feed.getFeed().isIs_like()){
            int index = contentFeeds.indexOf(feed);
            if (index >= 0){
                Feed feed1 = feed;
                feed1.getFeed().setIs_like(true);
                feed1.getFeed().setCount_likes(1);
                feed1.getFeed().setLike_result("You liked this post.");
                contentFeeds.set(index,feed1);
                adapter.notifyDataSetChanged();
            }
            service.LikeFeed(feed.getId(),Auth.getId(),ChannelID).enqueue(new Callback<SmTagFeed.Like>() {
                @Override
                public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                    if (response.isSuccessful()){
                        Log.d(TAG,response.message());
                    }else {
                        try {
                            Log.e(TAG,response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onLike(TagUsJobRelativeLayout view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    @Override
    public void onComment(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onShare(TagUsJobRelativeLayout view, Feed feed) {
        view.ShareFeed(feed);
    }

    @Override
    public void onMore(TagUsJobRelativeLayout view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void showDetail(TagUsJobRelativeLayout view, Feed feed) {
        view.DetailFeedNormal(feed);
    }

    @Override
    public void viewProfile(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(TagUsJobRelativeLayout view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void openWebsite(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenWebsite(feed.getFeed().getSource_url());
    }

    @Override
    public void onMore(SmallFeedJob view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void viewDetail(SmallFeedJob view, Feed feed) {
        view.DetailFeedJob(Auth,feed);
    }

    @Override
    public void showGallery(List<Album> albums, int index) {
        ArrayList<String> images = new ArrayList<>();
        for (Album album: albums){
            images.add(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"original"));
        }
        DisplayImage(images,index);
    }

    @Override
    public void onItemSelected(TagUsJobRelativeLayout view, Object object) {
        RecentSearch recentSearch = (RecentSearch)object;
        if (recentSearch.getSearch_type() == RecentSearch.USER) {
            view.OpenProfile(Auth, recentSearch.getUser());
        }else{
            if (recentSearch.getFeed() != null){
                if (recentSearch.getFeed().getFeed().getFeed_type().equals(FeedDetail.STAFF_POST)){
                    view.DetailFeedCV(recentSearch.getFeed());
                }
            }else {
                fetchSearch(recentSearch.getSearch_text());
            }
        }
    }

    @Override
    public void SearchForPeople(TagUsJobRelativeLayout view) {
        view.SearchForPeople();
    }

    @Override
    public void SearchForCompany(TagUsJobRelativeLayout view) {
        view.SearchForCompany();
    }

    @Override
    public void SearchForStaffAndJob(TagUsJobRelativeLayout view) {
        if (Auth.getUser_type() == SmUser.FIND_JOB){
            view.SearchForJob();
        }else {
            view.SearchForStaff();
        }
    }

    @Override
    public void SearchForContent(TagUsJobRelativeLayout view) {
        view.SearchForContent();
    }

    @Override
    public void showMore(TagUsJobRelativeLayout view) {
        if (view == searchForPeople){
            view.SearchForPeople(query_search);
        }else if (view == searchForCompany){
            view.SearchForCompany(query_search);
        }else if (view == searchForJob){
            view.SearchForJob(query_search);
        }else if (view == searchForStaff){
            view.SearchForStaff(query_search);
        }else if (view == searchForContent){
            view.SearchForContent(query_search);
        }else if (view == jobsSuggestionLayout){
            view.JobSuggestion();
        }else if (view == staffSuggestionLayout){
            view.StaffSuggestion();
        }
    }

    @Override
    public void applyJob(TagUsJobRelativeLayout view, Feed feed) {
        view.ApplyJob(feed);
    }

    @Override
    public void download(TagUsJobRelativeLayout view, Feed feed) {
        view.DisplayDownloadBox(feed);
    }

    @Override
    public void chat(TagUsJobRelativeLayout view, Feed feed) {

    }

    @Override
    public void viewDetail(TagUsJobRelativeLayout view, Feed feed) {
        if (JobsSuggestionView.class == view.getClass()){
            view.DetailFeedJob(Auth,feed);
        }else if (StaffSuggestionView.class == view.getClass()){
            view.DetailFeedCV(feed);
        }
    }

    @Override
    public void edit(SearchSuggestRecentHeader view) {

    }

    @Override
    public void connect(UserCardThree view, SmUser user) {
        view.DoFollow(Auth,user);
    }

    @Override
    public void viewProfile(UserCardThree view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void remove(UserCardThree view, SmUser user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Close User").setMessage("Are you sure to close this user?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                users.remove(user);
                searchForPeopleAdapter.notifyDataSetChanged();
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void onSubcribe(CompanyCard view, Company company) {

    }

    @Override
    public void onDetail(CompanyCard view, Company company) {
        view.OpenCompany(company);
    }

    @Override
    public void select(LikeResultCell cell, SmUser user) {
        cell.OpenProfile(Auth,user);
    }

    @Override
    public void connect(LikeResultCell cell, SmUser user) {
        cell.DoFollow(Auth,user);
    }

    @Override
    public void saveFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Save Post").setMessage("Are you sure to save this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (feed.getIs_save() == 0){
                    if (jobFeeds != null && jobFeeds.contains(feed)){
                        int index = jobFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(1);
                        temp.setIs_save(true);
                        feed.setFeed(temp);
                        jobFeeds.add(index,feed);
                        searchForJobAdapter.notifyDataSetChanged();
                    }

                    if (staffFeeds != null && staffFeeds.contains(feed)){
                        int index = staffFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(1);
                        temp.setIs_save(true);
                        feed.setFeed(temp);
                        staffFeeds.add(index,feed);
                        searchForStaffAdapter.notifyDataSetChanged();
                    }

                    if (contentFeeds != null && contentFeeds.contains(feed)){
                        int index = contentFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(1);
                        temp.setIs_save(true);
                        feed.setFeed(temp);
                        contentFeeds.add(index,feed);
                        searchForContentAdapter.notifyDataSetChanged();
                    }

                    if (jobSuggestFeeds != null && jobSuggestFeeds.contains(feed)){
                        int index = jobSuggestFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(1);
                        temp.setIs_save(true);
                        feed.setFeed(temp);
                        jobSuggestFeeds.add(index,feed);
                        jobsSuggestionAdapter.notifyDataSetChanged();
                    }

                    if (staffSuggestFeeds != null && staffSuggestFeeds.contains(feed)){
                        int index = staffSuggestFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(1);
                        temp.setIs_save(true);
                        feed.setFeed(temp);
                        staffSuggestFeeds.add(index,feed);
                        staffSuggestionAdapter.notifyDataSetChanged();
                    }

                    service.SaveFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                        @Override
                        public void onResponse(Call<Feed> call, Response<Feed> response) {
                            if (response.isSuccessful()){
                                Log.d(TAG,response.message());
                            }else {
                                try {
                                    Log.e(TAG,response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Feed> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }else {
                    if (jobFeeds != null && jobFeeds.contains(feed)){
                        int index = jobFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(0);
                        temp.setIs_save(false);
                        feed.setFeed(temp);
                        jobFeeds.add(index,feed);
                        searchForJobAdapter.notifyDataSetChanged();
                    }

                    if (staffFeeds != null && staffFeeds.contains(feed)){
                        int index = staffFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(0);
                        temp.setIs_save(false);
                        feed.setFeed(temp);
                        staffFeeds.add(index,feed);
                        searchForStaffAdapter.notifyDataSetChanged();
                    }

                    if (contentFeeds != null && contentFeeds.contains(feed)){
                        int index = contentFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(0);
                        temp.setIs_save(false);
                        feed.setFeed(temp);
                        contentFeeds.add(index,feed);
                        searchForContentAdapter.notifyDataSetChanged();
                    }

                    if (jobSuggestFeeds != null && jobSuggestFeeds.contains(feed)){
                        int index = jobSuggestFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(0);
                        temp.setIs_save(false);
                        feed.setFeed(temp);
                        jobSuggestFeeds.add(index,feed);
                        jobsSuggestionAdapter.notifyDataSetChanged();
                    }

                    if (staffSuggestFeeds != null && staffSuggestFeeds.contains(feed)){
                        int index = staffSuggestFeeds.indexOf(feed);
                        FeedDetail temp = feed.getFeed();
                        feed.setIs_save(0);
                        temp.setIs_save(false);
                        feed.setFeed(temp);
                        staffSuggestFeeds.add(index,feed);
                        staffSuggestionAdapter.notifyDataSetChanged();
                    }

                    service.SaveFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                        @Override
                        public void onResponse(Call<Feed> call, Response<Feed> response) {
                            if (response.isSuccessful()){
                                Log.d(TAG,response.message());
                            }else {
                                try {
                                    Log.e(TAG,response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Feed> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }

            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void editFeed(SmUser user, Feed feed) {
        EditPostNormal(feed);
    }

    @Override
    public void hideFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Hide Post").setMessage("Are you sure to hide this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (jobFeeds != null && jobFeeds.remove(feed)){
                    searchForJobAdapter.notifyDataSetChanged();
                }

                if (staffFeeds != null && staffFeeds.remove(feed)){
                    searchForStaffAdapter.notifyDataSetChanged();
                }

                if (contentFeeds != null && contentFeeds.remove(feed)){
                    searchForContentAdapter.notifyDataSetChanged();
                }

                if (jobSuggestFeeds != null && jobSuggestFeeds.remove(feed)){
                    jobsSuggestionAdapter.notifyDataSetChanged();
                }

                if (staffSuggestFeeds != null && staffSuggestFeeds.remove(feed)){
                    staffSuggestionAdapter.notifyDataSetChanged();
                }
                service.HideFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.e(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void removeFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Post").setMessage("Are you sure to remove this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (jobFeeds != null && jobFeeds.remove(feed)){
                    searchForJobAdapter.notifyDataSetChanged();
                }

                if (staffFeeds != null && staffFeeds.remove(feed)){
                    searchForStaffAdapter.notifyDataSetChanged();
                }

                if (contentFeeds != null && contentFeeds.remove(feed)){
                    searchForContentAdapter.notifyDataSetChanged();
                }

                if (jobSuggestFeeds != null && jobSuggestFeeds.remove(feed)){
                    jobsSuggestionAdapter.notifyDataSetChanged();
                }

                if (staffSuggestFeeds != null && staffSuggestFeeds.remove(feed)){
                    staffSuggestionAdapter.notifyDataSetChanged();
                }
                service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.e(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void reload(LoadingView view) {
        fetchNextRecent();
    }
}

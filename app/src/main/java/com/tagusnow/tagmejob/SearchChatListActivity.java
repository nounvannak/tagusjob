package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.github.nkzawa.socketio.client.Socket;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.adapter.SearchChatAdatper;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchChatListActivity extends TagUsJobActivity {

    private Toolbar toolbar;
    private EditText search_box;
    private RecyclerView recyclerView;
    private SmUser user;
    private Socket socket;
    private String query_search;
    private UserPaginate paginate;
    private SearchChatAdatper adatper;
    private UserService service = ServiceGenerator.createService(UserService.class);

    private void FirstRecentChat(){
        service.ChatList(user.getId(),0,10).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    FirstRecentChat(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void FirstRecentChat(UserPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adatper = new SearchChatAdatper(this,paginate,this.user,SearchChatAdatper.RECENT_CHAT);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(this.adatper);
    }

    private void NextRecentChat(){
        int page = this.paginate.getCurrent_page() + 1;
        service.ChatList(this.user.getId(),0,10,page).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    NextRecentChat(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void NextRecentChat(UserPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adatper.Update(paginate,SearchChatAdatper.RECENT_CHAT).notifyDataSetChanged();
    }

    private void First(String query_search){
        this.query_search = query_search;
        service.ChatList(user.getId(),1,query_search,10).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    First(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void First(UserPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adatper = new SearchChatAdatper(this,paginate,this.user,SearchChatAdatper.RESULT);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        this.recyclerView.setAdapter(this.adatper);
    }

    private void Next(){
        int page = this.paginate.getCurrent_page() + 1;
        service.ChatList(user.getId(),1,this.query_search,10,page).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    Next(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void Next(UserPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adatper.Update(paginate,SearchChatAdatper.RESULT).notifyDataSetChanged();
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (paginate.getTo() - 1)) && (paginate.getTo() < paginate.getTotal())) {
                        if (paginate.getNext_page_url()!=null || !paginate.getNext_page_url().equals("")){
                            if (lastPage!=paginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = paginate.getCurrent_page();
                                    if (query_search!=null){
                                        Next();
                                    }else {
                                        NextRecentChat();
                                    }
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void InitSocket(){
        socket = new App().getSocket();

        socket.connect();
    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
    }

    private void InitUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        search_box  = (EditText)findViewById(R.id.search_box);
        search_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0){
                    First(charSequence.toString());
                }else {
                    FirstRecentChat();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchChatListActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_chat_list);
        this.initTemp();
        this.InitSocket();
        this.InitUI();
        this.FirstRecentChat();
    }
}

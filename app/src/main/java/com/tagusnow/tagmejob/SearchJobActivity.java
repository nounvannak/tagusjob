package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.SearchJobAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.AbstractSequentialList;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchJobActivity extends TagUsJobActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private EditText category;
    private Spinner location;
    private Button btnSearch,btnClear;
    private RecyclerView recyclerView;
    private SmUser user;
    private SmLocation city;
    private SearchJobAdapter adapter;
    private RecentSearchPaginate paginate;
    private FeedResponse feedResponse;
    private RecentSearchService searchService = ServiceGenerator.createService(RecentSearchService.class);
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private List<String> locationList = new ArrayList<String>();
    private List<Integer> cityID = new ArrayList<>();
    private int city_id;
    private String city_name;
    private TextView label_recent;
    private String search_query;

    private void First(String search_query){
        this.search_query = search_query;
        service.Search(this.user.getId(),1,search_query,0,this.city_id,10).enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()){
                    First(response.body());
                }else {
                    try {
                        ShowAlert("Error",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
                ShowAlert("Connection Error",t.getMessage());
            }
        });
    }

    private void First(FeedResponse feedResponse){
        dismissProgress();
        this.feedResponse = feedResponse;
        this.CheckNext_();
        this.adapter = new SearchJobAdapter(this,feedResponse,this.user,SearchJobAdapter.RESULT);
        this.recyclerView.setAdapter(this.adapter);
    }

    private void Next(){
        int page = feedResponse.getCurrent_page() + 1;
        service.Search(this.user.getId(),1,this.search_query,0,this.city_id,10,page).enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()){
                    Next(response.body());
                }else {
                    try {
                        ShowAlert("Error",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
                ShowAlert("Connection Error",t.getMessage());
            }
        });
    }

    private void Next(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.CheckNext_();
        this.adapter.update(this,feedResponse,this.user,SearchJobAdapter.RESULT).notifyDataSetChanged();
    }

    private void FirstRecent(){
        this.searchService.hasRecentSearch_(this.user.getId(),2,10).enqueue(new Callback<RecentSearchPaginate>() {
            @Override
            public void onResponse(Call<RecentSearchPaginate> call, Response<RecentSearchPaginate> response) {
                if (response.isSuccessful()){
                    FirstRecent(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RecentSearchPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void FirstRecent(RecentSearchPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adapter = new SearchJobAdapter(this,paginate,this.user,SearchJobAdapter.RECENT);
        recyclerView.setAdapter(this.adapter);
    }

    private void NextRecent(){
        int page = this.paginate.getCurrent_page() + 1;
        this.searchService.hasRecentSearch_(this.user.getId(),2,10,page).enqueue(new Callback<RecentSearchPaginate>() {
            @Override
            public void onResponse(Call<RecentSearchPaginate> call, Response<RecentSearchPaginate> response) {
                if (response.isSuccessful()){
                    NextRecent(response.body());
                }else {
                    try {
                        ShowAlert("Error",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RecentSearchPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void NextRecent(RecentSearchPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adapter.update(this,paginate,this.user,SearchJobAdapter.RECENT).notifyDataSetChanged();
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (paginate.getTo() - 1)) && (paginate.getTo() < paginate.getTotal())) {
                        if (paginate.getNext_page_url()!=null || !paginate.getNext_page_url().equals("")){
                            if (lastPage!=paginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = paginate.getCurrent_page();
                                    NextRecent();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void CheckNext_(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    Next();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.city = new Repository(this).restore().getLocation();
    }

    private void Setup(){
        if (this.location!=null){
            this.locationList.add("All Location");
            this.cityID.add(0);
            for (SmLocation.Data data : this.city.getData()){
                this.locationList.add(data.getName());
                this.cityID.add(data.getId());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,this.locationList);
        this.location.setAdapter(adapter);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchJobActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_job);
        this.initTemp();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        category = (EditText)findViewById(R.id.category);
        location = (Spinner)findViewById(R.id.spnLocation);
        location.setOnItemSelectedListener(this);
        btnSearch = (Button)findViewById(R.id.btnSearch);
        btnClear = (Button)findViewById(R.id.btnClear);
        label_recent = (TextView)findViewById(R.id.label_recent);

        btnSearch.setOnClickListener(this);
        btnClear.setOnClickListener(this);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        this.Setup();
        this.FirstRecent();
    }

    private void Search(){
        showProgress();
        this.label_recent.setText("Result searches:");
        this.btnClear.setVisibility(View.GONE);
        this.search_query = category.getText().toString();
        this.First(this.search_query);
    }

    @Override
    public void onClick(View view) {
        if (view==btnSearch){
            this.Search();
        }else if (view==btnClear){
            startActivity(RecentSearchActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        this.city_id = this.cityID.get(i);
        this.city_name = this.locationList.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

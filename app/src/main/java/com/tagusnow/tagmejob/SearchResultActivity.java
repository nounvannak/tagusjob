package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.adapter.SearchResultAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchResultActivity extends AppCompatActivity{

    private static final String TAG = SearchResultActivity.class.getSimpleName();
    public static final String FEED = "feed";
    private Toolbar toolbar;
    private SmUser authUser;
    private Feed feed;
    private FeedResponse feedResponse;
    private SmTagSubCategory category;
    private SearchResultAdapter adapter;
    int lastPage = 0;
    boolean isRequest = true;
    RecyclerView recyclerView;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private JobsService jobsService = retrofit.create(JobsService.class);
    private Callback<FeedResponse> mCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,"mCallback");
                initSearch(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Feed> mJobCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,"mJobCallback");
                feed = response.body();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void callJob(){
        int user = this.authUser!=null ? this.authUser.getId() : 0;
        int id = this.feed!=null ? this.feed.getId() : 0;
        jobsService.callJob(user,id).enqueue(mJobCallback);
    }

    private void newSearch(){
        int user = this.authUser!=null ? this.authUser.getId() : 0;
        int limit = 5;
        int page = 0;
        int id = this.feed!=null ? this.feed.getId() : 0;
        jobsService.fastSearch(user,-1, Objects.requireNonNull(this.feed).getSearch_query(),id,limit,page).enqueue(mCallback);
    }

    private void initSearch(FeedResponse feedResponse){
        feedResponse.setTo(feedResponse.getTo() + SearchResultAdapter.MORE_VIEW);
        this.feedResponse = feedResponse;
        this.adapter = new SearchResultAdapter(this,this.feedResponse,this.feed,this.authUser);
        this.recyclerView.setAdapter(this.adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        initIntent();
        initView();
        newSearch();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchResultActivity.class);
    }

    private void initIntent(){
        this.authUser = new Auth(this).checkAuth().token();
        this.feed = new Gson().fromJson(getIntent().getStringExtra(FEED),Feed.class);
        this.category = new Repository(this).restore().getCategory();
        this.callJob();
    }

    private void initView(){
        initToolbar();
        initRecycler();
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.view_post_detail_title,this.feed.getUser_post().getUser_name()));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

}

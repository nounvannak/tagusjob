package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.REST.Service.HomeService;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.adapter.SearchSuggestFollowAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.fragment.Search.Suggest.User.NoResult;
import com.tagusnow.tagmejob.fragment.Search.Suggest.User.ResultFragment;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchSuggestFollowActivity extends AppCompatActivity implements TextWatcher {

    private static final String TAG = SearchSuggestFollowActivity.class.getSimpleName();
    private Toolbar toolbar;
    private EditText search_box;
    private RecyclerView recyclerView;
    private ResultFragment resultFragment;
    private NoResult noResult;
    private SmUser authUser;
    private UserPaginate userPaginate;
    private RecentSearchPaginate recentSearchPaginate;
    private SearchSuggestFollowAdapter adapter;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService followService = retrofit.create(FollowService.class);
    private RecentSearchService recentSearchService = retrofit.create(RecentSearchService.class);
    private static String search_text;

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchSuggestFollowActivity.class);
    }

    private void initFragment(){
        resultFragment = ResultFragment.newInstance();
        noResult = NoResult.newInstance();
    }

    private void hasRecentSearch(){
        int limit = 10;
        int user = authUser!=null ? authUser.getId() : 0;
        recentSearchService.hasRecentSearch(user,limit).enqueue(new Callback<RecentSearchPaginate>() {
            @Override
            public void onResponse(Call<RecentSearchPaginate> call, Response<RecentSearchPaginate> response) {
                if (response.isSuccessful()){
                    Log.w(TAG,response.message());
                    initRecentSearch(response.body());
                }else {
                    try {
                        Log.w(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RecentSearchPaginate> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void hasSearchSuggest(){
        int limit = 10;
        int user = authUser!=null ? authUser.getId() : 0;
        followService.hasSearchSuggest(user, search_text,limit).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    Log.w(TAG,new Gson().toJson(response.body()));
                    initSuggest(response.body(),search_text);
                }else{
                    try {
                        Log.w(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    this.onResponse(call,response);
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void initRecentSearch(RecentSearchPaginate body){
        if (body.getTotal() > 0){
            body.setTo(body.getTo() + SearchSuggestFollowAdapter.MORE_VIEW);
        }
        this.recentSearchPaginate = body;
        adapter = new SearchSuggestFollowAdapter(this,body).type(SearchSuggestFollowAdapter.RECENT);
        recyclerView.setAdapter(adapter);
    }

    private void initSuggest(UserPaginate body, String search_text) {
        if (body.getTotal() > 0){
            body.setTo(body.getTo() + 1);
        }
        this.userPaginate = body;
        adapter = new SearchSuggestFollowAdapter(this,body,search_text).type(SearchSuggestFollowAdapter.RESULT);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_suggest_follow);
        initTemp();
        initFragment();
        initView();
//        startFragment(resultFragment);
        hasRecentSearch();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTemp();
//        initFragment();
//        startFragment(resultFragment);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initTemp();
//        initFragment();
//        startFragment(resultFragment);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initTemp();
//        initFragment();
//        startFragment(resultFragment);
    }

    private void initTemp(){
        authUser = new Auth(this).checkAuth().token();
    }

    private void initView(){
        initToolbar();
        initRecycler();
    }

    private void startFragment(Fragment fragment){
        try {
//            getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame,fragment).commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        search_box = (EditText)findViewById(R.id.search_box);
        search_box.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        hasRecentSearch();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        search_text = charSequence.toString();
        hasSearchSuggest();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}

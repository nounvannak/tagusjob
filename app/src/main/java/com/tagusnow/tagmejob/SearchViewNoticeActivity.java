package com.tagusnow.tagmejob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.REST.Service.HomeService;
import com.tagusnow.tagmejob.adapter.SearchViewNoticeAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchViewNoticeActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = SearchViewNoticeActivity.class.getSimpleName();
    private String search_text;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refresh;
    private SearchViewNoticeAdapter adapter;
    private UserPaginate userPaginate;
    private SmUser authUser;
    /*Service*/
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    private FollowService service = retrofit.create(FollowService.class);
    /*Service Callback*/
    private Callback<UserPaginate> mSearchSuggestCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initSearchSuggest(response.body());
            }else {
                Log.e(TAG,response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<UserPaginate> mNextSearchSuggestCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextSearchSuggest(response.body());
            }else {
                Log.e(TAG,response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void hasSearchSuggest(){
        int user = authUser!=null ? authUser.getId() : 0;
        int limit = 15;
        service.hasSearchSuggest(user,this.search_text,limit).enqueue(mSearchSuggestCallback);
    }

    private void hasNextSearchSuggest(){
        int user = authUser!=null ? authUser.getId() : 0;
        int limit = 15;
        int page = this.userPaginate.getCurrent_page() + 1;
        service.hasSearchSuggest(user,this.search_text,limit,page).enqueue(mNextSearchSuggestCallback);
    }

    private void initSearchSuggest(UserPaginate userPaginate){
        if (userPaginate.getTotal() > 0){
            userPaginate.setTo(userPaginate.getTo() + SearchViewNoticeAdapter.MORE_VIEW);
        }
        this.userPaginate = userPaginate;
        this.checkNext();
        adapter = new SearchViewNoticeAdapter(this,userPaginate);
        recyclerView.setAdapter(adapter);
    }

    private void initNextSearchSuggest(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.checkNext();
        adapter.update(this,userPaginate).notifyDataSetChanged();
    }

    static int lastPage;
    static boolean isRequest = false;
    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    hasNextSearchSuggest();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view_notice);
        initTemp();
        initView();
        hasSearchSuggest();
    }

    private void initView(){
        initToolbar();
        initSwipeRefresh();
        initRecycler();
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.see_all_results_for_s,this.search_text));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initSwipeRefresh(){
        refresh = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refresh.setColorSchemeColors(getColor(R.color.colorAccent));
        refresh.setOnRefreshListener(this);
    }

    private void initTemp(){
        this.authUser = new Auth(this).checkAuth().token();
        this.search_text = getIntent().getStringExtra("search_text");
    }

    public static Intent createIntent(Activity activity,String search_text){
        return new Intent(activity,SearchViewNoticeActivity.class).putExtra("search_text",search_text);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchViewNoticeActivity.class);
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            new Task().execute();
            hasSearchSuggest();
        }
    };

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRunnable,2000);
    }

    private class Task extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            refresh.setRefreshing(false);
            super.onPostExecute(aVoid);
        }
    }
}

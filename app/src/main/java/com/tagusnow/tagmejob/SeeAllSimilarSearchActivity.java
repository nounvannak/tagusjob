package com.tagusnow.tagmejob;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.adapter.NoticeMainSearchResultJobAdapter;
import com.tagusnow.tagmejob.adapter.NoticeMainSearchResultPeopleAdapter;
import com.tagusnow.tagmejob.adapter.NoticeMainSearchResultPostAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultHeader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SeeAllSimilarSearchActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = SeeAllSimilarSearchActivity.class.getSimpleName();
    public static final String TYPE = "type";
    public static final String QUERY = "query";
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private ImageView icon;
    private TextView title;
    private SmUser user;
    private String query;
    private int type;
    private UserPaginate userPaginate;
    private FeedResponse postResponse, jobResponse;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private NoticeMainSearchResultPeopleAdapter peopleAdapter;
    private NoticeMainSearchResultPostAdapter postAdapter;
    private NoticeMainSearchResultJobAdapter jobAdapter;
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();;
    private JobsService jobsService = retrofit.create(JobsService.class);
    private FollowService followService = retrofit.create(FollowService.class);
    private Callback<UserPaginate> mSuggestCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()) {
                Log.w(TAG, response.message());
                initSearchSuggest(response.body());
            } else {
                try {
                    Log.e(TAG, response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> mJobCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()) {
                Log.w(TAG, response.message());
                initFastSearch(response.body());
            } else {
                try {
                    Log.e(TAG, response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> mNextSuggestCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()) {
                Log.w(TAG, response.message());
                initNextSearchSuggest(response.body());
            } else {
                try {
                    Log.e(TAG, response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> mNextJobCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()) {
                Log.w(TAG, response.message());
                initNextFastSearch(response.body());
            } else {
                try {
                    Log.e(TAG, response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void hasSearchSuggest() {
        int user = this.user != null ? this.user.getId() : 0;
        int limit = 20;
        followService.hasSearchSuggest(user, this.query, limit).enqueue(mSuggestCallback);
    }

    static int lastPage;
    static boolean isRequest = false;

    private void checkNext() {
        if (this.type == NoticeMainSearchResultHeader.POST) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (postResponse.getTo() - 1)) && (postResponse.getTo() < postResponse.getTotal())) {
                            if (postResponse.getNext_page_url() != null || !postResponse.getNext_page_url().equals("")) {
                                if (lastPage != postResponse.getCurrent_page()) {
                                    isRequest = true;
                                    if (isRequest) {
                                        lastPage = postResponse.getCurrent_page();
                                        nextFastSearch();
                                    }
                                } else {
                                    isRequest = false;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else if (this.type == NoticeMainSearchResultHeader.JOB) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (jobResponse.getTo() - 1)) && (jobResponse.getTo() < jobResponse.getTotal())) {
                            if (jobResponse.getNext_page_url() != null || !jobResponse.getNext_page_url().equals("")) {
                                if (lastPage != jobResponse.getCurrent_page()) {
                                    isRequest = true;
                                    if (isRequest) {
                                        lastPage = jobResponse.getCurrent_page();
                                        nextFastSearch();
                                    }
                                } else {
                                    isRequest = false;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else if (this.type == NoticeMainSearchResultHeader.PEOPLE) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                            if (userPaginate.getNext_page_url() != null || !userPaginate.getNext_page_url().equals("")) {
                                if (lastPage != userPaginate.getCurrent_page()) {
                                    isRequest = true;
                                    if (isRequest) {
                                        lastPage = userPaginate.getCurrent_page();
                                        nextHasSearchSuggest();
                                    }
                                } else {
                                    isRequest = false;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                            if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                                if (lastPage!=feedResponse.getCurrent_page()){
                                    isRequest = true;
                                    if (isRequest){
                                        lastPage = feedResponse.getCurrent_page();
                                        getNextJobsPost();
                                    }
                                }else{
                                    isRequest = false;
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });*/
        }
    }

    private void initNextSearchSuggest(UserPaginate userPaginate) {
        this.userPaginate = userPaginate;
        this.checkNext();
        this.peopleAdapter.update(this, this.userPaginate, this.user, this.query, NoticeMainSearchResultPeopleAdapter.TYPE);
    }

    private void initSearchSuggest(UserPaginate userPaginate) {
        this.userPaginate = userPaginate;
        this.checkNext();
        this.peopleAdapter = new NoticeMainSearchResultPeopleAdapter(this, this.userPaginate, this.user, this.query, NoticeMainSearchResultPeopleAdapter.TYPE);
        recyclerView.setAdapter(this.peopleAdapter);
    }

    private void nextHasSearchSuggest() {
        int user = this.user != null ? this.user.getId() : 0;
        int limit = 20;
        int page = userPaginate.getCurrent_page() + 1;
        followService.hasSearchSuggest(user, this.query, limit, page).enqueue(mNextSuggestCallback);
    }

    private void fastSearch() {
        int user = this.user != null ? this.user.getId() : 0;
        int limit = 10;
        jobsService.fastSearch(user, this.type, this.query, limit).enqueue(mJobCallback);
    }

    private void nextFastSearch() {
        int user = this.user != null ? this.user.getId() : 0;
        int limit = 10;
        int page = this.type == 0 ? (jobResponse.getCurrent_page() + 1) : (postResponse.getCurrent_page() + 1);
        jobsService.fastSearch(user, this.type, this.query, limit, page).enqueue(mNextJobCallback);
    }

    private void initFastSearch(FeedResponse feedResponse) {
        if (this.type == NoticeMainSearchResultHeader.JOB) {
            this.jobResponse = feedResponse;
            this.checkNext();
            this.jobAdapter = new NoticeMainSearchResultJobAdapter(this, this.jobResponse, this.user, this.query, NoticeMainSearchResultJobAdapter.TYPE);
            recyclerView.setAdapter(this.jobAdapter);
        } else if (this.type == NoticeMainSearchResultHeader.POST) {
            this.postResponse = feedResponse;
            this.checkNext();
            this.postAdapter = new NoticeMainSearchResultPostAdapter(this, this.postResponse, this.user, this.query, NoticeMainSearchResultPostAdapter.TYPE);
            recyclerView.setAdapter(this.postAdapter);
        }
    }

    private void initNextFastSearch(FeedResponse feedResponse) {
        if (this.type == NoticeMainSearchResultHeader.JOB) {
            this.jobResponse = feedResponse;
            this.checkNext();
            this.jobAdapter.update(this, this.jobResponse, this.user, this.query, NoticeMainSearchResultJobAdapter.TYPE);
        } else if (this.type == NoticeMainSearchResultHeader.POST) {
            this.postResponse = feedResponse;
            this.checkNext();
            this.postAdapter.update(this, this.postResponse, this.user, this.query, NoticeMainSearchResultPostAdapter.TYPE);
        }
    }

    private void initTempData() {
        if (this.type == NoticeMainSearchResultHeader.PEOPLE) {
            hasSearchSuggest();
        }else if (this.type == NoticeMainSearchResultHeader.LOCATION){
            initLocation();
        } else {
            fastSearch();
        }
    }

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            initTempData();
            refreshLayout.setRefreshing(false);
        }
    };

    public static Intent createIntent(Activity activity) {
        return new Intent(activity, SeeAllSimilarSearchActivity.class);
    }

    private void initTemp() {
        this.user = new Auth(this).checkAuth().token();
        this.type = getIntent().getIntExtra(TYPE, 0);
        this.query = getIntent().getStringExtra(QUERY);
    }

    private void initLocation() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.w(TAG,String.valueOf(location.getLatitude()));
                Log.w(TAG,String.valueOf(location.getAltitude()));
                Log.w(TAG,String.valueOf(location.getLongitude()));
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                Log.w(TAG,s);
            }

            @Override
            public void onProviderEnabled(String s) {
                Log.w(TAG,s);
            }

            @Override
            public void onProviderDisabled(String s) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET
                },3909);
            }
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        icon = (ImageView)findViewById(R.id.icon);
        title = (TextView)findViewById(R.id.title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        initTempToolbar();
    }

    private void initTempToolbar(){
        if (this.type== NoticeMainSearchResultHeader.PEOPLE){
            Glide.with(this)
                    .load(R.drawable.ic_user_male_icon)
                    .fitCenter()
                    .into(icon);
            title.setText(R.string.people);
        }else if (this.type==NoticeMainSearchResultHeader.JOB){
            Glide.with(this)
                    .load(R.drawable.ic_bullhorn_icon)
                    .fitCenter()
                    .into(icon);
            title.setText(R.string.jobs);
        }else if(this.type==NoticeMainSearchResultHeader.POST){
            Glide.with(this)
                    .load(R.drawable.ic_heart_icon)
                    .fitCenter()
                    .into(icon);
            title.setText(R.string.post_);
        }else {
            Glide.with(this)
                    .load(R.drawable.ic_map_marker_icon)
                    .fitCenter()
                    .into(icon);
            title.setText(R.string.location);
        }
    }

    private void initRefresh(){
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            refreshLayout.setColorSchemeColors(getColor(R.color.colorAccent));
        }
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initView(){
        this.initToolbar();
        this.initRefresh();
        this.initRecycler();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all_similar_search);
        this.initTemp();
        this.initView();
        refreshLayout.setRefreshing(true);
        this.onRefresh();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRun,2000);
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.adapter.SeeMoreSuggestFollowAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SeeMoreSuggestFollowActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = SeeMoreSuggestFollowActivity.class.getSimpleName();
    private SmUser authUser;
    private Toolbar toolbar;
    private FloatingActionButton fabSearchFollowing;
    private SeeMoreSuggestFollowAdapter adapter;
    private List<SmUser> userList = new ArrayList<>();
    private RecyclerView recyclerView;
    private UserPaginate userPaginate;
    private static final int MORE_VIEW = 2;
    /*Init Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService service = retrofit.create(FollowService.class);

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SeeMoreSuggestFollowActivity.class);
    }

    private void callSuggest(){
        int user = authUser!=null ? authUser.getId() : 0;
        int limit = 20;
        service.callSuggest(user,limit).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                initSuggest(response.body());
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void callNextSuggest(){
        int page = userPaginate!=null ? userPaginate.getCurrent_page()+1 : 1;
        int limit = 20;
        int user = authUser!=null ? authUser.getId() : 0;
        service.callSuggest(user,limit,page).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                initNextSuggest(response.body());
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    static int lastPage;
    static boolean isRequest = false;

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    callNextSuggest();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initNextSuggest(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.userList = userPaginate.getData();
        checkNext();
        adapter.with(userPaginate).notifyDataSetChanged();
    }

    private void initSuggest(UserPaginate userPaginate){
        userPaginate.setTo(userPaginate.getTo() + MORE_VIEW);
        this.userList = userPaginate.getData();
        this.userPaginate = userPaginate;
        checkNext();
        adapter = new SeeMoreSuggestFollowAdapter(this).with(userPaginate);
        recyclerView.setAdapter(adapter);
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initSearchButton(){
        fabSearchFollowing = (FloatingActionButton)findViewById(R.id.fabSearchFollowing);
        fabSearchFollowing.setOnClickListener(this);
    }

    private void initView(){
        initToolbar();
        initSearchButton();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fabSearchFollowing.getVisibility() == View.VISIBLE) {
                    fabSearchFollowing.hide();
                } else if (dy < 0 && fabSearchFollowing.getVisibility() != View.VISIBLE) {
                    fabSearchFollowing.show();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_more_suggest_follow);
        initTemp();
        initView();
        callSuggest();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initTemp();
        checkNext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTemp();
        checkNext();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initTemp();
        checkNext();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initTemp(){
        authUser = new Auth(this).checkAuth().token();
    }

    private void viewSearchSuggestActivity(){
        startActivity(SearchSuggestFollowActivity.createIntent(this));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fabSearchFollowing:
                viewSearchSuggestActivity();
                break;
        }
    }
}

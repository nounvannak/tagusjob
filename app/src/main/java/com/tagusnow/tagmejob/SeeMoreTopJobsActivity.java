package com.tagusnow.tagmejob;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class SeeMoreTopJobsActivity extends TagUsJobActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_more_top_jobs);
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.SeeSingleTopJobsAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import java.io.IOException;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeeSingleTopJobsActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    private static final String TAG = SeeSingleTopJobsActivity.class.getSimpleName();
    public static final String CATEGORY = "category";
    private SmUser user;
    private Toolbar toolbar;
    private TextView title;
    private CircleImageView icon;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private FloatingActionButton fabTop;
    private SpinKitView loading;
    private FeedResponse feedResponse;
    private SeeSingleTopJobsAdapter adapter;
    private Category category;
    private int location_id = 0;
    /*Service*/
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);

    private Callback<FeedResponse> mNewCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initJobsPost(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> mNextCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextJobsPost(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            getJobsPost();
            refreshLayout.setRefreshing(false);
        }
    };

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    getNextJobsPost();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void getJobsPost(){
        int user = this.user!=null ? this.user.getId() : 0;
        int type = this.user.getUser_type()==SmUser.FIND_JOB ? 1 : 2;
        int category = this.category!=null ? this.category.getId() : 0;
        int limit = 10;
        feedService.Search(user,type,category,this.location_id,limit).enqueue(mNewCallback);
    }

    private void getNextJobsPost(){
        int user = this.user!=null ? this.user.getId() : 0;
        int type = this.user.getUser_type()==SmUser.FIND_JOB ? 1 : 2;
        int limit = 10;
        int category = this.category!=null ? this.category.getId() : 0;
        int page = this.feedResponse.getCurrent_page() + 1;
        feedService.Search(user,type,category,this.location_id,limit,page).enqueue(mNextCallback);
    }

    private void initJobsPost(FeedResponse feedResponse){
//        feedResponse.setTo(feedResponse.getTo() + SeeSingleTopJobsAdapter.MORE_VIEW);
//        this.feedResponse = feedResponse;
//        if (feedResponse.getTotal()==0 || feedResponse.getTotal()==(feedResponse.getTo() - SeeSingleTopJobsAdapter.MORE_VIEW)){
//            loading.setVisibility(View.GONE);
//        }else {
//            loading.setVisibility(View.VISIBLE);
//        }
//        this.checkNext();
//        this.adapter = new SeeSingleTopJobsAdapter(this,this.feedResponse,this.category,this.user);
//        this.adapter.setDisplay_type(SeeSingleTopJobsAdapter.SMALL);
//        recyclerView.setAdapter(this.adapter);
    }

    private void initNextJobsPost(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        if (feedResponse.getTotal()==0 || feedResponse.getTotal()==feedResponse.getTo()){
            loading.setVisibility(View.GONE);
        }else {
            loading.setVisibility(View.VISIBLE);
        }
        this.checkNext();
        this.adapter.setDisplay_type(SeeSingleTopJobsAdapter.SMALL);
        this.adapter.update(this,feedResponse,this.category,this.user).notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_single_top_jobs);
        initTemp();
        initView();
        getJobsPost();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SeeSingleTopJobsActivity.class);
    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.category = new Gson().fromJson(getIntent().getStringExtra(CATEGORY),Category.class);
    }

    private void initView(){
        fabTop = (FloatingActionButton)findViewById(R.id.fabTop);
        loading = (SpinKitView)findViewById(R.id.loading);
        fabTop.hide();
        fabTop.setOnClickListener(this);
        initToolbar();
        initRefresh();
        initRecycler();
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        title = (TextView)findViewById(R.id.title);
        icon = (CircleImageView)findViewById(R.id.icon);

        if (this.category!=null){
            title.setText(this.category.getTitle());
            if (this.category.getImage()!=null){
                if (this.category.getImage().contains("http")){
                    Glide.with(this)
                            .load(this.category.getImage())
                            .centerCrop()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(icon);
                }else {
                    Glide.with(this)
                            .load(SuperUtil.getCategoryPath(this.category.getImage()))
                            .centerCrop()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(icon);
                }
            }else {
                Glide.with(this)
                        .load(R.mipmap.ic_launcher_circle)
                        .centerCrop()
                        .error(R.mipmap.ic_launcher_circle)
                        .into(icon);
            }
        }
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy >= 0 && fabTop.getVisibility() == View.VISIBLE) {
                    fabTop.hide();
                } else if (dy < 0 && fabTop.getVisibility() != View.VISIBLE) {
                    fabTop.show();
                }
            }
        });
    }

    private void initRefresh(){
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            refreshLayout.setColorSchemeColors(getColor(R.color.colorAccent));
        }
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRunnable,2000);
    }

    @Override
    public void onClick(View view) {
        if (view==fabTop){
            recyclerView.smoothScrollToPosition(0);
            fabTop.hide();
        }
    }

    public int getLocation_id() {
        return location_id;
    }

    public SwipeRefreshLayout getRefreshLayout() {
        return refreshLayout;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }
}

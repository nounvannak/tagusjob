package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.felipecsl.asymmetricgridview.library.Utils;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stfalcon.frescoimageviewer.ImageViewer;
import com.tagusnow.tagmejob.REST.Service.UserPostImageService;
import com.tagusnow.tagmejob.adapter.UserPostImageAdapter;
import com.tagusnow.tagmejob.model.v2.feed.Photo;
import com.tagusnow.tagmejob.model.v2.feed.PhotoPaginate;
import com.tagusnow.tagmejob.view.TagUsJobImagePostActivity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import me.relex.photodraweeview.PhotoDraweeView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShowAllUserImagePostActivity extends TagUsJobImagePostActivity implements AdapterView.OnItemClickListener {

    private static final String TAG = ShowAllUserImagePostActivity.class.getSimpleName();
    private int USER_ID = 0;
    public static final String USER_S = "user_id";
    private static final int LIMIT = -1;
    private Toolbar toolbar;
    private AsymmetricGridView listView;
    private UserPostImageAdapter adapter;
    private PhotoPaginate photoPaginate;
    private List<Photo> photos;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private UserPostImageService service = retrofit.create(UserPostImageService.class);
    private Callback<PhotoPaginate> mCallback = new Callback<PhotoPaginate>() {
        @Override
        public void onResponse(Call<PhotoPaginate> call, Response<PhotoPaginate> response) {
            if (response.isSuccessful()){
                setPhotoPaginate(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<PhotoPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private PhotoDraweeView mPhotoDraweeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all_user_image_post);
        this.initTemp();
        this.initView();

        service.getUserImagePost(this.USER_ID,LIMIT).enqueue(mCallback);
    }

    private void setPhotoPaginate(PhotoPaginate photoPaginate){
        this.photoPaginate = photoPaginate;
        this.photos = photoPaginate.getData();
        List<Photo> photos = new ArrayList<>();
        if (this.photos!=null){
            if (this.photos.size() > 0){
                this.strPhoto = new String[this.photos.size()];
                for (int i=0;i < this.photos.size();i++){
                    int colSpan1 = Math.random() < 0.2f ? 2 : 1;
                    int rowSpan1 = colSpan1;
                    if (this.photos.get(i).getImage()!=null){
                        if (this.photos.get(i).getImage().contains("http")){
                            this.strPhoto[i] = this.photos.get(i).getImage();
                            photos.add(new Photo(colSpan1,rowSpan1,i,this.photos.get(i).getId(),this.photos.get(i).getFeed_id(),this.photos.get(i).getUser_id(),this.strPhoto[i]));
                        }else {
                            this.strPhoto[i] = SuperUtil.getAlbumPicture(this.photos.get(i).getImage(),"original");
                            photos.add(new Photo(colSpan1,rowSpan1,i,this.photos.get(i).getId(),this.photos.get(i).getFeed_id(),this.photos.get(i).getUser_id(),this.strPhoto[i]));
                        }
                    }
                }
            }
        }
        this.adapter.setItems(photos);
    }

    private void initTemp(){ this.USER_ID = getIntent().getIntExtra(USER_S,0); }

    private void initView(){
        mPhotoDraweeView = new PhotoDraweeView(this);
        initToolbar();
        initList();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ShowAllUserImagePostActivity.class);
    }

    private void initList(){
        listView = (AsymmetricGridView) findViewById(R.id.listView);
        this.adapter = new UserPostImageAdapter(this);
        listView.setRequestedColumnCount(3);
        listView.setRequestedHorizontalSpacing(Utils.dpToPx(this, 3));
        listView.setAllowReordering(true);
        listView.determineColumns();
        listView.setAdapter(getNewAdapter());
        listView.setDebugging(true);
        listView.setOnItemClickListener(this);
    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Photos");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void showPicker(int startPosition) {
        new ImageViewer.Builder<>(this, this.strPhoto)
                .setStartPosition(startPosition)
                .show();
    }

    private void setColumnWidth(int columnWidth) {
        listView.setRequestedColumnWidth(Utils.dpToPx(this, columnWidth));
        listView.determineColumns();
        listView.setAdapter(getNewAdapter());
    }

    private void DISPLAY_PHOTO(int position){
        startActivity(ViewImageActivity.createIntent(this)
                .putExtra(ViewImageActivity.POSITION,position)
                .putExtra(ViewImageActivity.PHOTOS,this.strPhoto)
                .putExtra(ViewImageActivity.PARCEL,this.photoPaginate).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        showPicker(i);
        DISPLAY_PHOTO(i);
    }

    public ListAdapter getNewAdapter() {
        return new AsymmetricGridViewAdapter(this, listView, adapter);
    }
}

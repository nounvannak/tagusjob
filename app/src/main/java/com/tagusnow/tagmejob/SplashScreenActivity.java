package com.tagusnow.tagmejob;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.service.RegisterDeviceService;
import com.tagusnow.tagmejob.v3.login.auth.AuthCallback;
import com.tagusnow.tagmejob.v3.login.auth.AuthReponse;
import com.tagusnow.tagmejob.v3.login.auth.BaseLogin;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import permissions.dispatcher.NeedsPermission;
import retrofit2.Response;

public class SplashScreenActivity extends TagUsJobActivity implements AuthCallback<SmUser, AuthReponse> {

    private SmUser Auth;
    private String access_token,token_type;
    private static final String TAG = SplashScreenActivity.class.getSimpleName();
    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            Log.w(TAG,"onDenied");
        }
    };

    @Override
    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET})
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        AutoPermissions.Companion.loadAllPermissions(this, 1);
        InitTemp();
        BaseLogin baseLogin = new BaseLogin(this);
        baseLogin.addCallback(this);

        if (isNetworkConnected()){
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {
                    if (Auth != null && access_token != null && token_type != null){
                        String token = token_type + " " + access_token;
                        baseLogin.Refresh(token);

//                        if (Auth.getLogin_method().equals(SmUser.PHONE)){
//                            List<MultipartBody.Part> parts = new ArrayList<>();
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("phone",Auth.getPhone()));
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("password",Auth.getPassword()));
//                            baseLogin.Login(parts, AuthType.PHONE);
//                        }else if (Auth.getLogin_method().equals(SmUser.EMAIL)){
//                            List<MultipartBody.Part> parts = new ArrayList<>();
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("email",Auth.getEmail()));
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("password",Auth.getPassword()));
//                            baseLogin.Login(parts, AuthType.EMAIL);
//                        }else if (Auth.getLogin_method().equals(SmUser.FACEBOOK)){
//                            List<MultipartBody.Part> parts = new ArrayList<>();
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("email",Auth.getEmail()));
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("profile_id",Auth.getProfile_id()));
//                            baseLogin.Login(parts, AuthType.FACEBOOK);
//                        }else if (Auth.getLogin_method().equals(SmUser.GOOGLE)){
//                            List<MultipartBody.Part> parts = new ArrayList<>();
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("email",Auth.getEmail()));
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("profile_id",Auth.getProfile_id()));
//                            baseLogin.Login(parts, AuthType.GOOGLE);
//                        }else {
//                            List<MultipartBody.Part> parts = new ArrayList<>();
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("phone",Auth.getPhone()));
//                            parts.add(okhttp3.MultipartBody.Part.createFormData("password",Auth.getPassword()));
//                            baseLogin.Login(parts, AuthType.ANONYMOUS);
//                        }
                    }else{
                        AccessApp(null);
                    }
                }
            }, SuperConstance.SPLASH_DISPLAY_LENGTH);
        }else{
            AlertDialog alertDialog = new AlertDialog.Builder(SplashScreenActivity.this).create();
            alertDialog.setTitle("Internet error");
            alertDialog.setMessage("No internet connection");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            System.exit(0);
                        }
                    });
            alertDialog.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startService(new Intent(this, RegisterDeviceService.class));
        new Repository(this).response().save();
    }

    private void InitTemp(){
        this.Auth = new Auth(this).checkAuth().token();
        this.access_token = SuperUtil.GetPreference(this,SuperUtil.ACCESS_TOKEN,null);
        this.token_type = SuperUtil.GetPreference(this,SuperUtil.TOKEN_TYPE,null);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onSuccess(SmUser user) {
        new Auth(this).save(user);
        AccessApp(user);
        finish();
    }

    @Override
    public void onSent(AuthReponse response) {

    }

    @Override
    public void onLogOut(Response response) {

    }

    @Override
    public void onWarning(Response response) {
        new Auth(this).save(null);
        AccessApp(null);
    }

    @Override
    public void onError(Throwable throwable) {
        new Auth(this).save(null);
        AccessApp(null);
        throwable.printStackTrace();
    }

    @Override
    public void onAccessToken(String access_token, String token_type) {
        SuperUtil.SavePreference(this,SuperUtil.ACCESS_TOKEN,access_token);
        SuperUtil.SavePreference(this,SuperUtil.TOKEN_TYPE,token_type);
    }
}

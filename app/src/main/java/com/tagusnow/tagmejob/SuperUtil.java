package com.tagusnow.tagmejob;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.StringRes;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.extras.Base64;
import io.sentry.Sentry;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;

public class SuperUtil{

    public static final String ACCESS_TOKEN = "access_token";
    public static final String TOKEN_TYPE = "token_type";
    public static final String LOGIN_METHOD = "login_method";
    public static final String FULL_NAME = "full_name";
    public static final String PASSWORD = "password";
    public static final String LOGIN_VALUE = "login_value";

    public static String getFirstLineString(String content){
        String regex = "\n";
        String[] text = content.split(regex);
        return text.length > 0 ? text[0] : "";
    }

    public static String readTextFile(InputStream inputStream){
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    public static void showToast(Context context, @StringRes int text, boolean isLong) {
        showToast(context, context.getString(text), isLong);
    }

    public static void showToast(Context context, String text, boolean isLong) {
        Toast.makeText(context, text, isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }

    public static String EncodeBase64(String text){
        // Sending side
        byte[] data = new byte[0];
        try {
            data = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }

    public static String DecodeBase64(String base64){
        // Receiving side
        byte[] data = Base64.decode(base64, Base64.DEFAULT);
        String text = null;
        try {
            text = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static String FormatNumber(long number){
        String numberString = "";
        if (Math.abs(number / 1000000) > 1) {
            numberString = (number / 1000000) + "M";
        }else if (Math.abs(number / 1000) > 1) {
            numberString = (number / 1000) + "K";
        }else if (Math.abs(number / 1000000000) > 1){
            numberString = (number / 1000) + "B";
        }else {
            numberString = String.valueOf(number);
        }

        return numberString;
    }

    public static void SavePreference(Context context, String preference_name, String object){
        SharedPreferences mAuthPrefs = context.getSharedPreferences(preference_name,Context.MODE_PRIVATE);
        SharedPreferences.Editor mAuthEditor = mAuthPrefs.edit();
        mAuthEditor.putString(preference_name,object);
        mAuthEditor.apply();
        mAuthEditor.commit();
    }

    public static JSONObject GetPreference(Context context,String preference_name){
        SharedPreferences mUserPrefs = context.getSharedPreferences(preference_name,Context.MODE_PRIVATE);
        return new Gson().fromJson(mUserPrefs.getString(preference_name,null),JSONObject.class);
    }

    public static String GetPreference(Context context,String preference_name,String defValue){
        SharedPreferences mUserPrefs = context.getSharedPreferences(preference_name,Context.MODE_PRIVATE);
        return mUserPrefs.getString(preference_name,defValue);
    }

    public static String getBaseUrl(String url){
        return SuperConstance.BASE_URL+url;
    }

    public static String getBaseUrl(){
        return SuperConstance.BASE_URL+"/";
    }

    public static String getProfilePicture(String image,String size){
        return SuperConstance.BASE_URL+SuperConstance.PROFILE_PATH+"/"+size+"/"+image;
    }

    public static String getProfilePicture(String image){
        return SuperConstance.BASE_URL+SuperConstance.PROFILE_PATH+"/"+image;
    }

    public static String getCover(String image){
        return SuperConstance.BASE_URL+SuperConstance.COVER_PATH+"/"+image;
    }

    public static String getAlbumPicture(String image,String size){
        return SuperConstance.BASE_URL+SuperConstance.ALBUM_PATH+"/"+size+"/"+image;
    }

    public static String getAlbumPicture(String image){
        return SuperConstance.BASE_URL+SuperConstance.ALBUM_PATH+"/"+image;
    }

    public static String getCommentPicture(String image,String size){
        return SuperConstance.BASE_URL+SuperConstance.COMMENT_PATH+"/"+size+"/"+image;
    }

    public static String getCommentPicture(String image){
        return SuperConstance.BASE_URL+SuperConstance.COMMENT_PATH+"/"+image;
    }

    public static String getMessageResource(String file,int type){
        String sub_folder[] = {"site/message/image","site/message/audio","site/message/file","site/message/video","site/message/text","site/message/other"};
        if (type==Conversation.IMAGE){
            return SuperConstance.BASE_URL+sub_folder[type]+"/original/"+file;
        }else {
            return SuperConstance.BASE_URL+sub_folder[type]+"/"+file;
        }
    }

    public static String getMessageResource(String file,int type,String size){
        String sub_folder[] = {"site/message/image","site/message/audio","site/message/file","site/message/video","site/message/text","site/message/other"};
        if (type==Conversation.IMAGE){
            return SuperConstance.BASE_URL+sub_folder[type]+"/"+size+"/"+file;
        }else {
            return SuperConstance.BASE_URL+sub_folder[type]+"/"+file;
        }
    }

    public static String getCategoryPath(String image){
        return SuperConstance.BASE_URL+SuperConstance.CATEGORY_ICON+"/"+image;
    }

    public static void errorTrancking(String tracking_action){
        Sentry.getContext().recordBreadcrumb(
                new BreadcrumbBuilder().setMessage(tracking_action).build()
        );
        Sentry.getContext().setUser(
                new UserBuilder().setEmail("nounvannakrootid@gmail.com").build()
        );
    }

    public static void setImageResource(ImageView imageView, String src){
        try {
            if (src!=null){
                /*init policy accession*/
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                URL myFileUrl = new URL(src);
                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                imageView.setImageBitmap(BitmapFactory.decodeStream(is));
            }else{
                imageView.setImageResource(R.drawable.user_not_found);
            }
        } catch (IOException e) {
            imageView.setImageResource(R.drawable.user_not_found);
        }
    }

    public static Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = "Shutta_"+ timeStamp +".jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static Bitmap getBitmap(String src){
        Bitmap bitmap = null;
        try {
            if (src!=null){
                /*init policy accession*/
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                URL myFileUrl = new URL(src);
                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                bitmap = BitmapFactory.decodeStream(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }
}

package com.tagusnow.tagmejob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.feed.Comment;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class UpdateCommentActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = UpdateCommentActivity.class.getSimpleName();
    private static final String PROGRESS_DIALOG = "ProgressDialog";
    private final String OPEN_MEDIA_TITLE = "title";
    private final String OPEN_MEDIA_MODE = "mode";
    private final String OPEN_MEDIA_MAX = "maxSelection";
    private final int OPEN_MEDIA_PICKER = 8202;
    private CircleImageView edit_comment_profile;
    private EmojiconEditText edit_comment_box;
    private RelativeLayout edit_comment_image_layout;
    private ImageView edit_comment_image;
    private ImageButton edit_comment_remove_image,edit_comment_camera;
    private Button btnUpdate,btnCancel;
    private View edit_comment_border;
    private Comment comment;
    private File file;
    private boolean isDeleteFile = false;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_comment);
        setupWindowAnimations();
        gotIntent();
        initView();
        setTemp();
        conditionView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        conditionView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        conditionView();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        conditionView();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,UpdateCommentActivity.class);
    }

    private void initView(){
        edit_comment_profile = (CircleImageView)findViewById(R.id.edit_comment_profile);
        edit_comment_box = (EmojiconEditText)findViewById(R.id.edit_comment_box);
        edit_comment_image_layout = (RelativeLayout)findViewById(R.id.edit_comment_image_layout);
        edit_comment_image = (ImageView)findViewById(R.id.edit_comment_image);
        edit_comment_remove_image = (ImageButton)findViewById(R.id.edit_comment_remove_image);
        edit_comment_camera = (ImageButton)findViewById(R.id.edit_comment_camera);
        edit_comment_border = (View)findViewById(R.id.edit_comment_border);
        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        btnCancel = (Button)findViewById(R.id.btnCancel);

        btnUpdate.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        edit_comment_remove_image.setOnClickListener(this);
        edit_comment_camera.setOnClickListener(this);
        edit_comment_box.setUseSystemDefault(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.windowBackground));
            }
        }
        getWindow().setBackgroundDrawableResource(R.color.windowBackground);

        textChange();
    }

    private void textChange(){
        edit_comment_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                conditionView();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void conditionView(){

//        if (comment.getDes()!=null){
//            edit_comment_box.setVisibility(View.VISIBLE);
//        }else{
//            edit_comment_box.setVisibility(View.GONE);
//        }

        if (comment.getImage()!=null){
            edit_comment_image_layout.setVisibility(View.VISIBLE);
            edit_comment_border.setVisibility(View.VISIBLE);
        }else{
            edit_comment_image_layout.setVisibility(View.GONE);
            edit_comment_border.setVisibility(View.GONE);
        }

        if (comment.getDes()!=null || comment.getImage()!=null){
            if (comment.getDes()!=null && !comment.getDes().equals(edit_comment_box.getText().toString())){
                btnUpdate.setEnabled(true);
                btnUpdate.setBackgroundColor(getColor(R.color.colorAccent));
            }else if(comment.getImage()!=null && file!=null && !comment.getImage().equals(file.getAbsolutePath())){
                btnUpdate.setEnabled(true);
                btnUpdate.setBackgroundColor(getColor(R.color.colorAccent));
            }else{
                btnUpdate.setEnabled(false);
                btnUpdate.setBackgroundColor(getColor(R.color.colorBorder));
            }
        }else{
            btnUpdate.setEnabled(false);
            btnUpdate.setBackgroundColor(getColor(R.color.colorBorder));
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupWindowAnimations() {
        Slide slide = new Slide();
        slide.setDuration(1000);
        getWindow().setExitTransition(slide);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setTemp(){
        if (comment.getComment_user().getImage().contains("http")){
            Glide.with(this)
                    .load(comment.getComment_user().getImage())
                    .fitCenter()
                    .error(getDrawable(R.drawable.user_not_found))
                    .into(edit_comment_profile);
        }else{
            Glide.with(this)
                    .load(SuperUtil.getProfilePicture(comment.getComment_user().getImage(),"100"))
                    .centerCrop()
                    .error(getDrawable(R.drawable.user_not_found))
                    .into(edit_comment_profile);
        }

        if (comment.getDes()!=null){
            edit_comment_box.setText(comment.getDes());
        }

        if (comment.getImage()!=null){
            if (comment.getImage().contains("http")){
                Glide.with(this)
                        .load(comment.getImage())
                        .centerCrop()
                        .error(getDrawable(android.R.drawable.ic_menu_report_image))
                        .into(edit_comment_image);
            }else{
                Glide.with(this)
                        .load(SuperUtil.getCommentPicture(comment.getImage(),"100"))
                        .centerCrop()
                        .error(getDrawable(R.drawable.user_not_found))
                        .into(edit_comment_image);
            }
        }

    }

    private void removeImage(){
        if (file!=null){
            file = null;
        }

        if (comment.getImage()!=null){
            comment.setImage(null);
        }

        isDeleteFile = true;
        conditionView();
    }

    private void gotIntent(){
        Intent intent = getIntent();
        comment = new Gson().fromJson(intent.getStringExtra("Comment"),Comment.class);
        position = intent.getIntExtra("position",-1);
    }

    private void cancel(){
        onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void openCamera(){
//        Intent mCameraIntent = new Intent(getApplicationContext(),Gallery.class);
//        mCameraIntent.putExtra(OPEN_MEDIA_TITLE,"Gallery");
//        mCameraIntent.putExtra(OPEN_MEDIA_MODE,2);
//        mCameraIntent.putExtra(OPEN_MEDIA_MAX,1);
//        startActivityForResult(mCameraIntent,OPEN_MEDIA_PICKER);
        AndroidImagePicker.getInstance().pickSingle(this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                selectImage(new File(items.get(0).path));
            }
        });
    }

    public void showProgress() {
        ProgressDialogFragment f = ProgressDialogFragment.getInstance();
        getSupportFragmentManager().beginTransaction().add(f,PROGRESS_DIALOG).commitAllowingStateLoss();
    }

    public void dismissProgress() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager == null) return;
        ProgressDialogFragment f = (ProgressDialogFragment) manager.findFragmentByTag(PROGRESS_DIALOG);
        if (f != null) {
            getSupportFragmentManager().beginTransaction().remove(f).commitAllowingStateLoss();
        }
    }

    private void update(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("comment_id",comment.getId());
        if (edit_comment_box.getText()!=null){
            params.put("comment",edit_comment_box.getText());
            comment.setDes(edit_comment_box.getText().toString());
        }

        if (file!=null){
            try {
                params.put("image",file,"multipart/form-data","default.jpeg");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (isDeleteFile){
            params.put("delete_image",1);
        }

        client.post(SuperUtil.getBaseUrl(SuperConstance.EDIT_COMMENT), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                Log.w(TAG,rawJsonResponse);
                dismissProgress();
                finishRequest();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
                dismissProgress();
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void finishRequest(){
        Intent intent = getIntent();
        intent.putExtra("Comment",new Gson().toJson(comment));
        intent.putExtra("position",position);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==OPEN_MEDIA_PICKER && resultCode==RESULT_OK && data!=null){
            ArrayList<String> selectionResult=data.getStringArrayListExtra("result");
            file = new File(selectionResult.get(0));
            selectImage(file);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void selectImage(File file){
        if (file!=null){
            comment.setImage(file.getName());
            Glide.with(this)
                    .load(file)
                    .centerCrop()
                    .error(getDrawable(android.R.drawable.ic_menu_report_image))
                    .into(edit_comment_image);
            conditionView();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnUpdate:
                showProgress();
                update();
                break;
            case R.id.btnCancel:
                cancel();
                break;
            case R.id.edit_comment_remove_image:
                removeImage();
                break;
            case R.id.edit_comment_camera:
                openCamera();
                break;
        }
    }
}

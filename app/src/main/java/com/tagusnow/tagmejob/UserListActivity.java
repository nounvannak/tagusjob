package com.tagusnow.tagmejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.ConnectSuggestFaceAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.v3.user.UserCardTwo;
import com.tagusnow.tagmejob.v3.user.UserCardTwoListener;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserListActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, UserCardTwoListener {

    private static final String TAG = UserListActivity.class.getSimpleName();
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private UserPaginate userPaginate;
    private List<SmUser> users;
    private ConnectSuggestFaceAdapter adapter;
    private SmUser Auth;
    /*Service*/
    private FollowService service = ServiceGenerator.createService(FollowService.class);

    private Callback<UserPaginate> mNewCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewConnectSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> mNextCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextConnectSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    /* Socket.io */
    Socket socket;
    private Emitter.Listener FOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("auth_user");
                            JSONObject json = obj.getJSONObject("user");
                            if (Auth.getId() == userID){
                                if (json != null){
                                    SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                                    SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                                    if (follower != null && following != null){
                                        int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                            List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                            if (lists.size() > 0){
                                                SmUser user = lists.get(0);
                                                int index = users.indexOf(user);
                                                follower.setIs_follow(true);
                                                if (following.getId() == userID){
                                                    users.set(index,follower);
                                                    adapter.notifyDataSetChanged();
                                                }
                                            }
                                        }else {
                                            for (SmUser user: users){
                                                if (user.getId() == id){
                                                    int index = users.indexOf(user);
                                                    follower.setIs_follow(true);
                                                    if (following.getId() == userID){
                                                        users.set(index,follower);
                                                        adapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener UNFOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("auth_user");
                            JSONObject json = obj.getJSONObject("user");
                            if (Auth.getId() == userID){
                                if (json != null){
                                    SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                                    SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                                    if (follower != null && following != null){
                                        int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                            List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                            if (lists.size() > 0){
                                                SmUser user = lists.get(0);
                                                int index = users.indexOf(user);
                                                follower.setIs_follow(false);
                                                if (following.getId() == userID){
                                                    users.set(index,follower);
                                                    adapter.notifyDataSetChanged();
                                                }
                                            }
                                        }else {
                                            for (SmUser user: users){
                                                if (user.getId() == id){
                                                    int index = users.indexOf(user);
                                                    follower.setIs_follow(false);
                                                    if (following.getId() == userID){
                                                        users.set(index,follower);
                                                        adapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Follow User",FOLLOW_USER);
        socket.on("Unfollow User",UNFOLLOW_USER);
        socket.connect();
    }
    /* End Socket.io */

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    nextConnectSuggest();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initTemp(){
        this.Auth = new Auth(this).checkAuth().token();
    }

    private void initNewConnectSuggest(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users = userPaginate.getData();
        this.checkNext();
        this.adapter = new ConnectSuggestFaceAdapter(this,users,this);
        recyclerView.setAdapter(this.adapter);
    }

    private void initNextConnectSuggest(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users.addAll(userPaginate.getData());
        this.checkNext();
        this.adapter.NextData(userPaginate.getData());
        this.adapter.notifyDataSetChanged();
    }

    private void newConnectSuggest(){
        int user = this.Auth!=null ? this.Auth.getId() : 0;
        int limit = 10;
        service.callSuggest(user,limit).enqueue(mNewCallback);
    }

    private void nextConnectSuggest(){
        int user = this.Auth!=null ? this.Auth.getId() : 0;
        int limit = 10;
        int page = this.userPaginate.getCurrent_page() + 1;
        service.callSuggest(user,limit,page).enqueue(mNextCallback);
    }

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            newConnectSuggest();
            refreshLayout.setRefreshing(false);
        }
    };

    private void initView(){
        initToolbar();
        initRefresh();
        initRecycler();
    }

    private void initToolbar() {
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initRefresh(){
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            refreshLayout.setColorSchemeColors(Objects.requireNonNull(getColor(R.color.colorAccent)));
        }
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRun,2000);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,UserListActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_post,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.btnSearchViewPost){
            this.viewSearchSuggest();
        }
        return super.onOptionsItemSelected(item);
    }

    private void viewSearchSuggest(){
        startActivity(SearchSuggestFollowActivity.createIntent(this).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        initSocket();
        initTemp();
        initView();
        refreshLayout.setRefreshing(true);
        onRefresh();
    }

    @Override
    public void connect(UserCardTwo view, SmUser user) {
        view.DoFollow(Auth,user);
    }

    @Override
    public void profile(UserCardTwo view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void remove(UserCardTwo view, SmUser user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Close User").setMessage("Are you sure to close this user?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                users.remove(user);
                adapter.notifyDataSetChanged();
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }
}

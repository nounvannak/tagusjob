package com.tagusnow.tagmejob.V2.Company;

import android.content.Context;
import android.util.AttributeSet;

import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public abstract class BaseCompanyRecentPost<T> extends TagUsJobRelativeLayout {

    public BaseCompanyRecentPost(Context context) {
        super(context);
    }

    public BaseCompanyRecentPost(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseCompanyRecentPost(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public BaseCompanyRecentPost(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public abstract T getAdapter();
}

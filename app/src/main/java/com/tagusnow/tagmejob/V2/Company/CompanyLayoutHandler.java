package com.tagusnow.tagmejob.V2.Company;

import com.tagusnow.tagmejob.V2.Model.Company;

public interface CompanyLayoutHandler{
    void onDetail(CompanyLayout view, Company company);
}

package com.tagusnow.tagmejob.V2.Company;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.V2.Model.CompanyPaginate;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.company.CompanyCard;
import com.tagusnow.tagmejob.v3.company.CompanyCardHolder;
import com.tagusnow.tagmejob.v3.company.CompanyListener;
import com.tagusnow.tagmejob.v3.company.FullCompanyCard;
import com.tagusnow.tagmejob.v3.company.FullCompanyCardHolder;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyListActivity extends TagUsJobActivity implements CompanyListener<FullCompanyCard> {

    private static final String TAG = CompanyListActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private CompanyPaginate companyPaginate;
    private SmUser user;
    private Adapter adapter;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<CompanyPaginate> mFirst = new Callback<CompanyPaginate>() {
        @Override
        public void onResponse(Call<CompanyPaginate> call, Response<CompanyPaginate> response) {
            Log.e(TAG, String.valueOf(response.isSuccessful()));
            if (response.isSuccessful()){
                setFrist(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CompanyPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public static Intent createIntent(Activity activity){
        return new Intent(activity,CompanyListActivity.class);
    }

    private void InitTemp(){
        this.user = new Auth(this).checkAuth().token();
    }

    private void onFirst(){
        service.getCompany(user.getId(),10).enqueue(mFirst);
    }

    private void onNext(){
        int page = this.companyPaginate.getCurrent_page() + 1;
        service.getCompany(user.getId(),10,page).enqueue(new Callback<CompanyPaginate>() {
            @Override
            public void onResponse(Call<CompanyPaginate> call, Response<CompanyPaginate> response) {
                if (response.isSuccessful()){
                    setNext(response.body());
                }else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CompanyPaginate> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (companyPaginate.getTo() - 1)) && (companyPaginate.getTo() < companyPaginate.getTotal())) {
                        if (companyPaginate.getNext_page_url()!=null || !companyPaginate.getNext_page_url().equals("")){
                            if (lastPage!=companyPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = companyPaginate.getCurrent_page();
                                    onNext();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void setFrist(CompanyPaginate companyPaginate){
        Log.e(TAG, String.valueOf(companyPaginate.getTotal()));
        this.companyPaginate = companyPaginate;
        this.checkNext();
        this.adapter = new Adapter(this,companyPaginate.getData(),this);
        recyclerView.setAdapter(this.adapter);
    }

    private void setNext(CompanyPaginate companyPaginate){
        this.companyPaginate = companyPaginate;
        this.checkNext();
        this.adapter.NextData(companyPaginate.getData());
    }

    private void InitUI(){
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_list);
        InitTemp();
        InitUI();
        onFirst();
    }

    @Override
    public void onSubcribe(FullCompanyCard view, Company company) {

    }

    @Override
    public void onDetail(FullCompanyCard view, Company company) {
        view.OpenCompany(company);
    }
}

class Adapter extends RecyclerView.Adapter<FullCompanyCardHolder>{

    private Activity activity;
    private List<Company> companies;
    private CompanyListener<FullCompanyCard> companyListener;

    Adapter(Activity activity, List<Company> companies, CompanyListener<FullCompanyCard> companyListener) {
        this.activity = activity;
        this.companies = companies;
        this.companyListener = companyListener;
    }

    void NextData(List<Company> companies){
        this.companies.addAll(companies);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FullCompanyCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FullCompanyCardHolder(new FullCompanyCard(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull FullCompanyCardHolder holder, int position) {
        holder.BindView(activity,companies.get(position),companyListener);
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }
}

class CompanyHolder extends RecyclerView.ViewHolder {

    private CompanyLayout companyLayout;

    public CompanyHolder(View itemView) {
        super(itemView);
        companyLayout = (CompanyLayout)itemView;
    }

    public void bindView(Activity activity,Company company){
        companyLayout.setActivity(activity);
        companyLayout.setCompany(company);
        companyLayout.setPosition(getAdapterPosition());
    }
}

class CompanyLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private Company company;
    private int position;
    private ImageView logo;

    private void InitUI(Context context){
        inflate(context,R.layout.company_layout,this);
        this.context = context;

        logo = (ImageView) findViewById(R.id.logo);
        logo.setOnClickListener(this);
    }

    public CompanyLayout(Context context) {
        super(context);
        InitUI(context);
    }

    public CompanyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public CompanyLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public CompanyLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setCompany(Company company) {
        this.company = company;
        if (company!=null){
            if (company.getImage()!=null){
                if (company.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(company.getImage())
                            .fitCenter()
                            .error(R.drawable.not_available_)
                            .into(logo);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getAlbumPicture(company.getImage(),"500"))
                            .fitCenter()
                            .error(R.drawable.not_available_)
                            .into(logo);
                }
            }else {
                Glide.with(this.context)
                        .load(R.drawable.not_available_)
                        .fitCenter()
                        .error(R.drawable.not_available_)
                        .into(logo);
            }
        }
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        if (v==logo){
            this.activity.startActivity(CompanyProfileActivity.createIntent(this.activity)
                    .putExtra(CompanyProfileActivity.COMPANY,new Gson().toJson(company))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}


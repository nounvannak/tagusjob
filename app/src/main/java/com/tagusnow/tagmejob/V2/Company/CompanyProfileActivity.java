package com.tagusnow.tagmejob.V2.Company;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.v3.company.CompanyProfileHeader;
import com.tagusnow.tagmejob.v3.company.CompanyProfileHeaderHolder;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedListener;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJob;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobListener;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyProfileActivity extends TagUsJobActivity implements LoadingListener<LoadingView>, SmallFeedJobListener, FeedListener {

    private static final String TAG = CompanyProfileActivity.class.getSimpleName();
    private SmUser Auth;
    private RecyclerView recyclerView;
    private Company company;
    private FeedResponse feedResponse;
    public static final String COMPANY = "company";
    private CompanyAdapter adapter;
    private List<Feed> feeds;
    private LoadingView loadingView;
    private FeedService service = ServiceGenerator.createService(FeedService.class);

    /* Socket.io */
    Socket socket;
    private Emitter.Listener NEW_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
        }
    };
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalLikes = obj.getInt("count_likes");
                            int userID = obj.getInt("user_id");
                            String channelID = obj.getString("channel_id");
                            String likesResult = obj.getString("like_result");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);

                                    feedDetail.setCount_likes(totalLikes);
                                    feedDetail.setLike_result(likesResult);
                                    feedDetail.setIs_like(Auth.getId() == userID);

                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);

                                        feedDetail.setCount_likes(totalLikes);
                                        feedDetail.setLike_result(likesResult);
                                        feedDetail.setIs_like(Auth.getId() == userID);

                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalComments = obj.getInt("count_comments");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);
                                    feedDetail.setCount_comments(totalComments);
                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);
                                        feedDetail.setCount_comments(totalComments);
                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        int index = feeds.indexOf(feed);
                                        feeds.set(index,feedJson);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            int index = feeds.indexOf(feed);
                                            feeds.set(index,feedJson);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        feeds.remove(feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            int userID = obj.getInt("user_id");
                            if (Auth.getId() == userID){
                                Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                                if (feedJson != null){
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                        List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                        if (lists.size() > 0){
                                            Feed feed = lists.get(0);
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }else {
                                        for (Feed feed : feeds){
                                            if (feed.getId() == feedJson.getId()){
                                                feeds.remove(feed);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                }
            });
        }
    };
    private void InitSocket(){
        socket = new App().getSocket();
        socket.on("New Post",NEW_POST);
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.connect();
    }
    /* End Socket.io */

    public static Intent createIntent(Activity activity){
        return new Intent(activity,CompanyProfileActivity.class);
    }

    private void onFirst(){
        service.Search(this.Auth.getId(),1,company.getTitle_name(),0,0,10).enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()){
                    setFirst(response.body());
                }else {
                    try {
                        Log.e("onFirst",response.errorBody().string());
                        loadingView.setError(true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
                t.printStackTrace();
                loadingView.setError(true);
            }
        });
    }

    private void setFirst(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds = feedResponse.getData();
        this.checkNext();
        adapter.NewData(feeds);
        recyclerView.setAdapter(adapter);
        loadingView.setLoading(feedResponse.getTotal() > feedResponse.getTo());
    }

    private void onNext(){
        int page = feedResponse.getCurrent_page() + 1;
        service.Search(this.Auth.getId(),1,company.getTitle_name(),0,0,10,page).enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()){
                    setNext(response.body());
                }else {
                    try {
                        Log.e("onNext",response.errorBody().string());
                        loadingView.setError(true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
                t.printStackTrace();
                loadingView.setError(true);
            }
        });
    }

    private void setNext(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds.addAll(feedResponse.getData());
        this.checkNext();
        adapter.NextData(feedResponse.getData());
        loadingView.setLoading(feedResponse.getTotal() > feedResponse.getTo());
    }

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    onNext();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void InitUI(){
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

    }

    private void InitTemp(){
        this.Auth = new Auth(this).checkAuth().token();
        this.company = new Gson().fromJson(getIntent().getStringExtra(COMPANY),Company.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_profile);
        InitTemp();
        InitUI();
        InitSocket();
        adapter = new CompanyAdapter(this,company,this,this);
        loadingView = adapter.getLoadingView();
        onFirst();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    @Override
    public void reload(LoadingView view) {
        onNext();
    }

    @Override
    public void onMore(SmallFeedJob view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void viewDetail(SmallFeedJob view, Feed feed) {
        view.DetailFeedJob(Auth,feed);
    }

    @Override
    public void saveFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Save Post").setMessage("Are you sure to save this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                service.SaveFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            int index = feeds.indexOf(feed);
                            FeedDetail temp = feed.getFeed();
                            feed.setIs_save(1);
                            temp.setIs_save(true);
                            feed.setFeed(temp);
                            feeds.add(index,feed);
                            adapter.notifyDataSetChanged();
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void editFeed(SmUser user, Feed feed) {
        EditPostNormal(feed);
    }

    @Override
    public void hideFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Hide Post").setMessage("Are you sure to hide this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                service.HideFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            feeds.remove(feed);
                            adapter.notifyDataSetChanged();
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void removeFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Post").setMessage("Are you sure to remove this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            feeds.remove(feed);
                            adapter.notifyDataSetChanged();
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }
}

class CompanyAdapter extends RecyclerView.Adapter{

    private static final int HEADER = 0;
    private static final int POST = 1;
    private static final int LOADING = 2;
    private Activity activity;
    private List<Feed> feeds;
    private Company company;
    private SmallFeedJobListener feedJobListener;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    private int other = 2;

    CompanyAdapter(Activity activity, Company company, SmallFeedJobListener feedJobListener, LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.company = company;
        this.feedJobListener = feedJobListener;
        this.loadingListener = loadingListener;
        this.loadingView = new LoadingView(activity);
    }

    public void NewData(List<Feed> feeds){
        this.feeds = feeds;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return HEADER;
        }else if (position == (getItemCount() - 1)){
            return LOADING;
        }else {
            return POST;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == HEADER){
            return new CompanyProfileHeaderHolder(new CompanyProfileHeader(activity));
        }else if (viewType == POST){
            return new SmallFeedJobHolder(new SmallFeedJob(activity));
        }else {
            return new LoadingHolder(loadingView);
        }
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == HEADER){
            ((CompanyProfileHeaderHolder) holder).BindView(activity,company);
        }else if (getItemViewType(position) == POST){
            ((SmallFeedJobHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedJobListener);
        }else {
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }
    }

    @Override
    public int getItemCount() {
        return feeds.size() + other;
    }
}

package com.tagusnow.tagmejob.V2.Company;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.util.List;

public class CompanyRecentPost extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private TextView txtCompanyName,title1,title2,title3;
    private ImageView logo;
    private RelativeLayout layout1,layout2,layout3;
    private int position;
    private List<Feed> feeds;
    private Company company;
    private CompanyRecentPostListener companyRecentPostListener;

    public void setCompanyRecentPostListener(CompanyRecentPostListener companyRecentPostListener) {
        this.companyRecentPostListener = companyRecentPostListener;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private void InitData(List<Feed> feeds){
        this.feeds = feeds;
        if (feeds!=null && feeds.size() > 0){
            if (feeds.size()==1){
                layout1.setOnClickListener(this);
                title1.setText(feeds.get(0).getFeed().getTitle());
                title2.setText(feeds.get(0).getFeed().getSalary());
                title3.setText(feeds.get(0).getFeed().getCloseDate());
            }else if (feeds.size()==2){
                layout1.setOnClickListener(this);
                layout2.setOnClickListener(this);
                title1.setText(feeds.get(0).getFeed().getTitle());
                title2.setText(feeds.get(1).getFeed().getTitle());
            }else {
                layout1.setOnClickListener(this);
                layout2.setOnClickListener(this);
                layout3.setOnClickListener(this);
                title1.setText(feeds.get(0).getFeed().getTitle());
                title2.setText(feeds.get(1).getFeed().getTitle());
                title3.setText(feeds.get(2).getFeed().getTitle());
            }
        }
    }

    public void setCompany(Company company) {
        this.company = company;

        if (company!=null){
            txtCompanyName.setText(company.getTitle_name());
            if (company.getImage()!=null && !company.getImage().equals("")){
                if (company.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(company.getImage())
                            .fitCenter()
                            .error(R.drawable.not_available_)
                            .into(logo);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getAlbumPicture(company.getImage(),"500"))
                            .fitCenter()
                            .error(R.drawable.not_available_)
                            .into(logo);
                }
            }else {
                Glide.with(this.context)
                        .load(R.drawable.not_available_)
                        .fitCenter()
                        .error(R.drawable.not_available_)
                        .into(logo);
            }

            this.InitData(company.getFeeds().getData());
        }
    }

    private void InitUI(Context context){
        inflate(context, R.layout.company_recent_job,this);
        this.context = context;

        txtCompanyName = (TextView)findViewById(R.id.txtCompanyName);
        title1 = (TextView)findViewById(R.id.title1);
        title2 = (TextView)findViewById(R.id.title2);
        title3 = (TextView)findViewById(R.id.title3);

        logo = (ImageView)findViewById(R.id.logo);

        layout1 = (RelativeLayout)findViewById(R.id.layout1);
        layout2 = (RelativeLayout)findViewById(R.id.layout2);
        layout3 = (RelativeLayout)findViewById(R.id.layout3);

        this.setOnClickListener(this);
        logo.setOnClickListener(this);
    }

    public CompanyRecentPost(Context context) {
        super(context);
        this.InitUI(context);
    }

    public CompanyRecentPost(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public CompanyRecentPost(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public CompanyRecentPost(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (this == v) {
            this.companyRecentPostListener.onCompanyDetail(this, this.company, this.position);
        }else if (logo==v){
            this.companyRecentPostListener.onCompanyDetail(this, this.company, this.position);
        }else if (layout1 == v){
            if (this.feeds!=null && this.feeds.size() > 0){
                this.companyRecentPostListener.onJobDetail(this,this.feeds.get(0),this.position);
            }
        }else if (layout2 == v){
            if (this.feeds!=null && this.feeds.size() > 1){
                this.companyRecentPostListener.onJobDetail(this,this.feeds.get(1),this.position);
            }
        }else if (layout3 == v){
            if (this.feeds!=null && this.feeds.size() > 2){
                this.companyRecentPostListener.onJobDetail(this,this.feeds.get(2),this.position);
            }
        }
    }
}

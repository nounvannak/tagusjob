package com.tagusnow.tagmejob.V2.Company;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.V2.Model.Company;

import java.util.ArrayList;
import java.util.List;

public class CompanyRecentPostAdapter extends RecyclerView.Adapter<CompanyRecentPostHolder>{

    private Activity activity;
    private Context context;
    private CompanyRecentPostListener companyRecentPostListener;

    public void setCompanies(List<Company> companies) {
        Log.e("setCompanies ", String.valueOf(companies.size()));
        this.companies.addAll(companies);
    }

    private List<Company> companies = new ArrayList<>();

    public CompanyRecentPostAdapter(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    @NonNull
    @Override
    public CompanyRecentPostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CompanyRecentPostHolder(new CompanyRecentPost(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyRecentPostHolder holder, int position) {
        holder.bindView(this.activity,this.companies.get(position),companyRecentPostListener);
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    public void setCompanyRecentPostListener(CompanyRecentPostListener companyRecentPostListener) {
        this.companyRecentPostListener = companyRecentPostListener;
    }
}

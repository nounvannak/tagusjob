package com.tagusnow.tagmejob.V2.Company;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.V2.Model.Company;

public class CompanyRecentPostHolder extends RecyclerView.ViewHolder {

    private CompanyRecentPost companyRecentPost;

    public CompanyRecentPostHolder(View itemView) {
        super(itemView);
        companyRecentPost = (CompanyRecentPost)itemView;
    }

    public void bindView(Activity activity, Company company,CompanyRecentPostListener listener){
        companyRecentPost.setActivity(activity);
        companyRecentPost.setCompany(company);
        companyRecentPost.setCompanyRecentPostListener(listener);
        companyRecentPost.setPosition(getAdapterPosition());
    }
}

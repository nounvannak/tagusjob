package com.tagusnow.tagmejob.V2.Company;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.V2.Model.CompanyPaginate;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyRecentPostLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = CompanyRecentPostLayout.class.getSimpleName();
    private Context context;
    private SmUser user;
    private TextView btnMore;
    private RecyclerView recyclerView;
    private CompanyPaginate companyPaginate;
    CompanyRecentPostAdapter adapter;
    private FeedService service = ServiceGenerator.createService(FeedService.class);

    public void setPosition(int position) {
        this.position = position;
    }

    private int position;

    public void setCompanyRecentPostListener(CompanyRecentPostListener companyRecentPostListener) {
        this.companyRecentPostListener = companyRecentPostListener;
    }

    private CompanyRecentPostListener companyRecentPostListener;

    private void InitUI(Context context){
        inflate(context, R.layout.company_recent_post_layout,this);
        this.context = context;

        btnMore = (TextView)findViewById(R.id.btnMore);
        btnMore.setOnClickListener(this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(layoutManager);

        this.user = new Auth(context).checkAuth().token();
    }

    public CompanyRecentPostLayout(Context context) {
        super(context);
        this.InitUI(context);
    }

    public CompanyRecentPostLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public CompanyRecentPostLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public CompanyRecentPostLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
        service.getCompanyRecentPost(user.getId(),10).enqueue(new Callback<CompanyPaginate>() {
            @Override
            public void onResponse(Call<CompanyPaginate> call, Response<CompanyPaginate> response) {
                if (response.isSuccessful()){
                    setCompanyPaginate(response.body());
                }else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CompanyPaginate> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (btnMore == v){
            this.companyRecentPostListener.onSeeAll(this);
        }
    }

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (companyPaginate.getTo() - 1)) && (companyPaginate.getTo() < companyPaginate.getTotal())) {
                        if (companyPaginate.getNext_page_url()!=null || !companyPaginate.getNext_page_url().equals("")){
                            if (lastPage!=companyPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = companyPaginate.getCurrent_page();
                                    nextCompanyPaginate();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void setCompanyPaginate(CompanyPaginate companyPaginate) {
        this.companyPaginate = companyPaginate;
        this.adapter = new CompanyRecentPostAdapter(activity);
        this.checkNext();
        adapter.setCompanies(companyPaginate.getData());
        adapter.setCompanyRecentPostListener(companyRecentPostListener);
        recyclerView.setAdapter(adapter);
    }

    private void nextCompanyPaginate(){
        service.getCompanyRecentPost(user.getId(),10,companyPaginate.getCurrent_page() + 1).enqueue(new Callback<CompanyPaginate>() {
            @Override
            public void onResponse(Call<CompanyPaginate> call, Response<CompanyPaginate> response) {
                if (response.isSuccessful()){
                    adapter.setCompanies(response.body().getData());
                    adapter.notifyDataSetChanged();
                    checkNext();
                }else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CompanyPaginate> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}


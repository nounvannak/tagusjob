package com.tagusnow.tagmejob.V2.Company;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class CompanyRecentPostLayoutHolder extends RecyclerView.ViewHolder {

    private CompanyRecentPostLayout companyRecentPostLayout;

    public CompanyRecentPostLayoutHolder(View itemView) {
        super(itemView);
        companyRecentPostLayout = (CompanyRecentPostLayout)itemView;
    }

    public void bindView(Activity activity,CompanyRecentPostListener listener){
        companyRecentPostLayout.setActivity(activity);
        companyRecentPostLayout.setPosition(getAdapterPosition());
        companyRecentPostLayout.setCompanyRecentPostListener(listener);
    }
}

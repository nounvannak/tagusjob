package com.tagusnow.tagmejob.V2.Company;

import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.feed.Feed;

public interface CompanyRecentPostListener {
    void onJobDetail(CompanyRecentPost view, Feed feed, int position);
    void onCompanyDetail(CompanyRecentPost view, Company company, int position);
    void onSeeAll(CompanyRecentPostLayout view);
}

package com.tagusnow.tagmejob.V2.Message.Adapter;

import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;

public class MessageListAdapter extends DialogsListAdapter<Dialog> {

    public MessageListAdapter(ImageLoader imageLoader) {
        super(imageLoader);
    }

    public MessageListAdapter(int itemLayoutId, ImageLoader imageLoader) {
        super(itemLayoutId, imageLoader);
    }

    public MessageListAdapter(int itemLayoutId, Class<? extends BaseDialogViewHolder> holderClass, ImageLoader imageLoader) {
        super(itemLayoutId, holderClass, imageLoader);
    }
}

package com.tagusnow.tagmejob.V2.Message.Fixtures;

import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Message.Model.Message;
import com.tagusnow.tagmejob.V2.Message.Model.User;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.model.v2.feed.MessageFile;

import java.util.ArrayList;
import java.util.List;

public class MessageListFixtures extends FixturesData {

    public MessageListFixtures() {
        super();
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setUser1(SmUser user1) {
        this.user1 = user1;
    }

    private SmUser user1;

    private ArrayList<Message> messages = new ArrayList<>();

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;

        for (Conversation conversation : this.conversations){

            String image = null;

            if (this.user1.getId()==conversation.getUser_id()){
                if (conversation.getUserOne().getImage()!=null && !conversation.getUserOne().getImage().equals("")){
                    if (conversation.getUserOne().getImage().contains("http")){
                        image = conversation.getUserOne().getImage();
                    }else {
                        image = SuperUtil.getProfilePicture(conversation.getUserOne().getImage(),"200");
                    }
                }

                User user = new User(conversation.getUserOne().getProfile_id(),conversation.getUserOne().getUser_name(),image,true,conversation.getUserOne());

                if (conversation.getMessage_file()!=null && conversation.getMessage_file().size() > 0){
                    for (MessageFile messageFile: conversation.getMessage_file()){
                        Message message = new Message(String.valueOf(conversation.getId()),user,null,conversation.getDateCreated());
                        message.setImage(new Message.Image(SuperUtil.getMessageResource(messageFile.getNew_name(),messageFile.getFile_type(),"500")));
                        messages.add(message);
                    }
                }else {
                    Message message = new Message(String.valueOf(conversation.getId()),user,conversation.getMessage(),conversation.getDateCreated());
                    messages.add(message);
                }
            }else {
                if (conversation.getUserTwo().getImage()!=null && !conversation.getUserTwo().getImage().equals("")){
                    if (conversation.getUserTwo().getImage().contains("http")){
                        image = conversation.getUserTwo().getImage();
                    }else {
                        image = SuperUtil.getProfilePicture(conversation.getUserTwo().getImage(),"200");
                    }
                }

                User user = new User(conversation.getUserTwo().getProfile_id(),conversation.getUserTwo().getUser_name(),image,true,conversation.getUserTwo());

                if (conversation.getMessage_file()!=null && conversation.getMessage_file().size() > 0){
                    for (MessageFile messageFile: conversation.getMessage_file()){
                        Message message = new Message(String.valueOf(conversation.getId()),user,null,conversation.getDateCreated());
                        message.setImage(new Message.Image(SuperUtil.getMessageResource(messageFile.getNew_name(),messageFile.getFile_type(),"500")));
                        messages.add(message);
                    }
                }else {
                    Message message = new Message(String.valueOf(conversation.getId()),user,conversation.getMessage(),conversation.getDateCreated());
                    messages.add(message);
                }
            }
        }
    }

    private List<Conversation> conversations;

}

package com.tagusnow.tagmejob.V2.Message.Fixtures;

import android.util.Log;

import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;
import com.tagusnow.tagmejob.V2.Message.Model.Message;
import com.tagusnow.tagmejob.V2.Message.Model.User;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserListFixture extends FixturesData{

    private UserPaginate userPaginate;
    private List<SmUser> users = new ArrayList<>();
    private List<Dialog> dialogs = new ArrayList<>();

    public UserListFixture(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users.addAll(userPaginate.getData());
        this.InitDialogs();
    }

    private void InitDialogs(){
        if (this.users!=null){
            if (this.users.size() > 0){
                for (SmUser user : this.users){
                    String image = "";
                    String text = user.getConversation().getMessage()!=null ? user.getConversation().getMessage() : "Start message with " + user.getFirs_name();
                    Date date = user.getConversation().getTime()!=null ? user.getConversation().getDateCreated() : null;
                    int unreadCount = user.getConversation().getCounter();
                    User user1 = new User(user.getProfile_id(),user.getUser_name(),user.getImage(),true,user);
                    ArrayList<User> users_ = new ArrayList<>();
                    users_.add(user1);
                    Message message = new Message(String.valueOf(user.getConversation().getId()),user1,text,date);
                    if (user.getImage()!=null && !user.getImage().equals("")){
                        if (user.getImage().contains("http")){
                            image = user.getImage();
                        }else {
                            image = SuperUtil.getProfilePicture(user.getImage(),"200");
                        }
                    }
                    Log.e("InitDialogs: ", image);
                    Dialog dialog = new Dialog(user.getProfile_id(),user.getUser_name(),image,users_,message,unreadCount);
                    this.dialogs.add(dialog);
                }
            }
        }
    }

    public List<Dialog> getDialogs() {
        return dialogs;
    }
}

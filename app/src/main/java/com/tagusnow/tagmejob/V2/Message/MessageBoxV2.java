package com.tagusnow.tagmejob.V2.Message;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Message.Fixtures.MessageListFixtures;
import com.tagusnow.tagmejob.V2.Message.Incoming.CustomIncomingImageMessageViewHolder;
import com.tagusnow.tagmejob.V2.Message.Incoming.CustomIncomingTextMessageViewHolder;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;
import com.tagusnow.tagmejob.V2.Message.Model.Message;
import com.tagusnow.tagmejob.V2.Message.Model.User;
import com.tagusnow.tagmejob.V2.Message.Outcoming.CustomOutcomingImageMessageViewHolder;
import com.tagusnow.tagmejob.V2.Message.Outcoming.CustomOutcomingTextMessageViewHolder;
import com.tagusnow.tagmejob.V2.Message.media.holders.IncomingVoiceMessageViewHolder;
import com.tagusnow.tagmejob.V2.Message.media.holders.OutcomingVoiceMessageViewHolder;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.model.v2.feed.ConversationPagination;
import com.tagusnow.tagmejob.model.v2.feed.MessageFile;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageBoxV2 extends TagUsJobActivity implements MessagesListAdapter.OnMessageLongClickListener<Message>,
        MessageInput.InputListener,
        MessagesListAdapter.OnLoadMoreListener,
        MessagesListAdapter.SelectionListener,
        MessageInput.AttachmentsListener,
        MessageHolders.ContentChecker<Message>,
        DialogInterface.OnClickListener{

    private static final byte CONTENT_TYPE_VOICE = 1;

    private static final int TOTAL_MESSAGES_COUNT = 100;

    protected final String senderId = "0";
    protected ImageLoader imageLoader;
    protected MessagesListAdapter<Message> messagesAdapter;
    protected ConversationPagination pagination;

    private Menu menu;
    private int selectionCount;
    private Date lastLoadedDate;

    private static final String TAG = MessageBoxV2.class.getSimpleName();
    public static final String USER_TWO = "user_2";
    public static final String CHAT_ID = "chat_id";
    public static final String DIALOG = "dialog";
    public static final int FILE_PICKER = 849;
    private static final int PERMISSIONS_REQUEST_CODE = 543;
    private MessagesList messagesList;
    private Toolbar toolbar;
    private AvatarView profile;
    private AvatarPlaceholder refreshableAvatarPlaceholder;
    private IImageLoader iImageLoader;
    private TextView user_name,time;
    private SmUser user1,user2;
    private Dialog dialog;
    private int chat_id;
    private Socket socket;
    private List<File> images = new ArrayList<>();
    private File my_file;
    private UserService service = ServiceGenerator.createService(UserService.class);

    private Callback<Conversation> ChatCallback = new Callback<Conversation>() {
        @Override
        public void onResponse(Call<Conversation> call, Response<Conversation> response) {
            if (response.isSuccessful()){
                clearForm();
                AddMessage(response.body());
            }else {
                clearForm();
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Conversation> call, Throwable t) {
            clearForm();
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private void clearForm(){
        this.my_file = null;
        this.images = new ArrayList<>();
    }

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("error", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, Arrays.toString(args));
            }
        });
        socket.on("packet", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, Arrays.toString(args));
            }
        });
        socket.on("New Message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG,new Gson().toJson(args));
                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e(TAG,obj.toString());
                            if (obj.has("message")){
                                Conversation conversation = new Gson().fromJson(new String(Base64.decode(obj.getString("message"),Base64.DEFAULT)),Conversation.class);
                                if (conversation.getConversation_id()==chat_id && conversation.getUser_one()==user1.getId()){
                                    Log.e(TAG, "run: 1");
                                    AddMessage(conversation);
                                    PlayMessageSound();
                                }else {
                                    if (conversation.getUser_one()==user1.getId() && conversation.getUser_two()==user2.getId()){
                                        Log.e(TAG, "run: 2");
                                        AddMessage(conversation);
                                        PlayMessageSound();
                                    }
//                                    else if (conversation.getUser_one()==user2.getId() && conversation.getUser_two()==user1.getId()){
//                                        Log.e(TAG, "run: 3");
//                                        AddMessage(conversation);
//                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.on("Count Notification Message" + this.user2.getId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("Count Not Message",new Gson().toJson(obj));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.on("Typing Message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("Typing",new Gson().toJson(obj));
                            String typing = obj.getString("user_name")+" is typing...";
                            if (obj.getInt("conversation_id")==chat_id && obj.getInt("user_id")==user2.getId()){
                                time.setText(typing);
                            }else {
                                if (obj.getInt("user_id")==user2.getId()){
                                    time.setText(typing);
                                }else {
                                    time.setText("last seen recently");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.on("Stop Typing Message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("Stop Typing",new Gson().toJson(obj));
                            if (obj.getInt("conversation_id")==chat_id && obj.getInt("user_id")==user2.getId()){
                                time.setText("last seen recently");
                            }else {
                                if (obj.getInt("user_id")==user2.getId()){
                                    time.setText("last seen recently");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.connect();
    }

    private void AddMessage(Conversation conversation) {
        if (conversation!=null){
            this.chat_id = conversation.getConversation_id();

            String image = null;

            if (this.user1.getId()==conversation.getUserOne().getId()){
                if (conversation.getUserOne().getImage()!=null && !conversation.getUserOne().getImage().equals("")){
                    if (conversation.getUserOne().getImage().contains("http")){
                        image = conversation.getUserOne().getImage();
                    }else {
                        image = SuperUtil.getProfilePicture(conversation.getUserOne().getImage(),"200");
                    }
                }

                User user = new User(conversation.getUserOne().getProfile_id(),conversation.getUserOne().getUser_name(),image,true,conversation.getUserOne());

                if (conversation.getMessage_file()!=null && conversation.getMessage_file().size() > 0){
                    for (MessageFile messageFile: conversation.getMessage_file()){
                        Message message = new Message(String.valueOf(conversation.getId()),user,null,conversation.getDateCreated());
                        message.setImage(new Message.Image(SuperUtil.getMessageResource(messageFile.getNew_name(),messageFile.getFile_type())));
                        messagesAdapter.addToStart(message,true);
                    }
                }else {
                    Message message = new Message(String.valueOf(conversation.getId()),user,conversation.getMessage(),conversation.getDateCreated());
                    messagesAdapter.addToStart(message,true);
                }
            }else {
                if (conversation.getUserTwo().getImage()!=null && !conversation.getUserTwo().getImage().equals("")){
                    if (conversation.getUserTwo().getImage().contains("http")){
                        image = conversation.getUserTwo().getImage();
                    }else {
                        image = SuperUtil.getProfilePicture(conversation.getUserTwo().getImage(),"200");
                    }
                }

                User user = new User(conversation.getUserTwo().getProfile_id(),conversation.getUserTwo().getUser_name(),image,true,conversation.getUserTwo());

                if (conversation.getMessage_file()!=null && conversation.getMessage_file().size() > 0){
                    for (MessageFile messageFile: conversation.getMessage_file()){
                        Message message = new Message(String.valueOf(conversation.getId()),user,null,conversation.getDateCreated());
                        message.setImage(new Message.Image(SuperUtil.getMessageResource(messageFile.getNew_name(),messageFile.getFile_type())));
                        messagesAdapter.addToStart(message,true);
                    }
                }else {
                    Message message = new Message(String.valueOf(conversation.getId()),user,conversation.getMessage(),conversation.getDateCreated());
                    messagesAdapter.addToStart(message,true);
                }
            }

        }
    }

    @Override
    protected void onDestroy() {
        if (socket.connected()){
            socket.disconnect();
        }
        Log.e(TAG,"destroy");
        super.onDestroy();
    }

    private void First(){

        Log.e(TAG, "On First");

        service.ChatResource(this.user1.getId(),this.chat_id,10).enqueue(new Callback<ConversationPagination>() {
            @Override
            public void onResponse(Call<ConversationPagination> call, Response<ConversationPagination> response) {
                if (response.isSuccessful()){
                    First(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ConversationPagination> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void First(ConversationPagination pagination){
        this.pagination = pagination;
        if (pagination.getData()!=null && pagination.getData().size() > 0){
            this.chat_id = pagination.getData().get(0).getConversation_id();
        }

        loadMessages();
    }

    protected void loadMessages() {

        new Handler().postDelayed(new Runnable() { //imitation of internet connection
            @Override
            public void run() {
                MessageListFixtures messageListFixtures = new MessageListFixtures();
                messageListFixtures.setUser1(user1);
                messageListFixtures.setConversations(pagination.getData());
                ArrayList<Message> messages = messageListFixtures.getMessages();
                messagesAdapter.addToEnd(messages, false);
            }
        }, 1000);
    }

    private void Next(){
        int page = this.pagination.getCurrent_page() + 1;
        service.ChatResource(this.user1.getId(),this.chat_id,10,page).enqueue(new Callback<ConversationPagination>() {
            @Override
            public void onResponse(Call<ConversationPagination> call, Response<ConversationPagination> response) {
                if (response.isSuccessful()){
                    Next(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ConversationPagination> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void Next(ConversationPagination pagination){
        this.pagination = pagination;
        loadMessages();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity, MessageBoxV2.class);
    }

    private void InitTemp(){
        this.user1 = new Auth(this).checkAuth().token();
//        this.user2 = (SmUser) getIntent().getSerializableExtra(USER_TWO);
        this.dialog = new Gson().fromJson(getIntent().getStringExtra(USER_TWO),Dialog.class);
        this.user2 = this.dialog.getUsers().get(0).getUser();
        this.chat_id = getIntent().getIntExtra(CHAT_ID,0);
    }

    private void checkPermissionsAndOpenFilePicker() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            openFilePicker();
        }
    }

    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode==PERMISSIONS_REQUEST_CODE){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openFilePicker();
            } else {
                showError();
            }
        }
    }

    private void openFilePicker() {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(FILE_PICKER)
                .withHiddenFiles(true)
                .withTitle("File Picker")
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_PICKER && resultCode == RESULT_OK) {
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            if (path != null) {
                this.my_file = new File(path);
                Chat(null);
            }
        }
    }

    public static void open(Context context) {
        context.startActivity(new Intent(context, MessageBoxV2.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_box_v2);

        InitTemp();
        initSocket();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        profile = (AvatarView)findViewById(R.id.profile);
        user_name = (TextView)findViewById(R.id.user_name);
        time = (TextView)findViewById(R.id.time);

        if (dialog!=null){

            user_name.setText(dialog.getDialogName());

            refreshableAvatarPlaceholder = new AvatarPlaceholder(dialog.getDialogName(), 50);
            iImageLoader = new GlideLoader();
            if (this.dialog.getDialogPhoto()!=null){
                iImageLoader.loadImage(profile, refreshableAvatarPlaceholder,dialog.getDialogPhoto());

            }else {
                iImageLoader.loadImage(profile, refreshableAvatarPlaceholder,null);
            }
//            Glide.with(this).load(dialog.getDialogPhoto()).centerCrop().error(R.drawable.not_available_).into(profile);
        }

        imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url, Object payload) {
                Picasso.with(MessageBoxV2.this).load(url).into(imageView);
            }
        };

        messagesList = (MessagesList) findViewById(R.id.messagesList);
        initAdapter();

        MessageInput input = (MessageInput) findViewById(R.id.input);
        input.setInputListener(this);
        input.setAttachmentsListener(this);
    }

    private void initAdapter() {

        //We can pass any data to ViewHolder with payload
        CustomIncomingTextMessageViewHolder.Payload payload = new CustomIncomingTextMessageViewHolder.Payload();
        //For example click listener
        payload.avatarClickListener = new CustomIncomingTextMessageViewHolder.OnAvatarClickListener() {
            @Override
            public void onAvatarClick() {
                Toast.makeText(MessageBoxV2.this, "Text message avatar clicked", Toast.LENGTH_SHORT).show();
            }
        };

        MessageHolders holdersConfig = new MessageHolders()
                .registerContentType(
                        CONTENT_TYPE_VOICE,
                        IncomingVoiceMessageViewHolder.class,
                        R.layout.item_custom_incoming_voice_message,
                        OutcomingVoiceMessageViewHolder.class,
                        R.layout.item_custom_outcoming_voice_message,
                        this)
                .setIncomingTextConfig(
                        CustomIncomingTextMessageViewHolder.class,
                        R.layout.item_custom_incoming_text_message,
                        payload)
                .setOutcomingTextConfig(
                        CustomOutcomingTextMessageViewHolder.class,
                        R.layout.item_custom_outcoming_text_message)
                .setIncomingImageConfig(
                        CustomIncomingImageMessageViewHolder.class,
                        R.layout.item_custom_incoming_image_message)
                .setOutcomingImageConfig(
                        CustomOutcomingImageMessageViewHolder.class,
                        R.layout.item_custom_outcoming_image_message);

        this.messagesAdapter = new MessagesListAdapter<>(this.user1.getProfile_id(), holdersConfig, this.imageLoader);
        this.messagesAdapter.setOnMessageLongClickListener(this);
        this.messagesAdapter.setLoadMoreListener(this);
        messagesList.setAdapter(this.messagesAdapter);
    }

    @Override
    public void onAddAttachments() {
        new AlertDialog.Builder(this)
                .setItems(R.array.view_types_dialog, this)
                .show();
    }

    @Override
    public boolean onSubmit(CharSequence input) {
        Chat(input);
        return true;
    }

    private void Chat(CharSequence input){
        if (my_file!=null && input!=null){
            MultipartBody.Part file = prepareFilePart("file",my_file);
            RequestBody message = createPartFromString(input.toString());
            service.Chat(chat_id,Conversation.FILE,this.user1.getId(),this.user2.getId(),message,file).enqueue(ChatCallback);
        }else {
            if (my_file!=null) {
                MultipartBody.Part file = prepareFilePart("file", my_file);
                service.Chat(chat_id, Conversation.FILE, this.user1.getId(), this.user2.getId(), file).enqueue(ChatCallback);
            }else if (images.size() > 0  && input!=null) {
                List<MultipartBody.Part> imageParts = new ArrayList<>();
                RequestBody message = createPartFromString(input.toString());
                for (File file : this.images) {
                    imageParts.add(prepareFilePart("images[]", file));
                }
                service.Chat(chat_id, Conversation.IMAGE, this.user1.getId(), this.user2.getId(), message, imageParts).enqueue(ChatCallback);
            }else if (images.size() > 0){
                List<MultipartBody.Part> imageParts = new ArrayList<>();
                for (File file : this.images) {
                    imageParts.add(prepareFilePart("images[]", file));
                }
                service.Chat(chat_id, Conversation.IMAGE, this.user1.getId(), this.user2.getId(),imageParts).enqueue(ChatCallback);
            }else {
                RequestBody message = createPartFromString(input.toString());
                service.Chat(chat_id,Conversation.TEXT,this.user1.getId(),this.user2.getId(),message).enqueue(ChatCallback);
            }
        }
    }

    @Override
    public void onMessageLongClick(Message message) {
        Log.e(TAG,new Gson().toJson(message));
    }

    private void OpenImagePicker(){
        AndroidImagePicker.getInstance().pickMulti(this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                for (ImageItem image : items){
                    images.add(new File(image.path));
                }

                Chat(null);
            }
        });
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case 0:
                // Select Image
                OpenImagePicker();
//                messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
                break;
            case 1:
                // Select Attachment
                checkPermissionsAndOpenFilePicker();
//                messagesAdapter.addToStart(MessagesFixtures.getVoiceMessage(), true);
                break;
        }
    }

    @Override
    public boolean hasContentFor(Message message, byte type) {
        switch (type) {
            case CONTENT_TYPE_VOICE:
                return message.getVoice() != null
                        && message.getVoice().getUrl() != null
                        && !message.getVoice().getUrl().isEmpty();
        }
        return false;
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        if (this.pagination!=null){
            Next();
        }else {
            First();
        }
    }

    @Override
    public void onSelectionChanged(int count) {
        this.selectionCount = count;
        menu.findItem(R.id.action_delete).setVisible(count > 0);
        menu.findItem(R.id.action_copy).setVisible(count > 0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        First();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.chat_actions_menu, menu);
        onSelectionChanged(0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                messagesAdapter.deleteSelectedMessages();
                break;
            case R.id.action_copy:
                messagesAdapter.copySelectedMessagesText(this, getMessageStringFormatter(), true);
                SuperUtil.showToast(this, R.string.copied_message, true);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (selectionCount == 0) {
            super.onBackPressed();
        } else {
            messagesAdapter.unselectAllItems();
        }
    }

    private MessagesListAdapter.Formatter<Message> getMessageStringFormatter() {
        return new MessagesListAdapter.Formatter<Message>() {
            @Override
            public String format(Message message) {
                String createdAt = new SimpleDateFormat("MMM d, EEE 'at' h:mm a", Locale.getDefault())
                        .format(message.getCreatedAt());

                String text = message.getText();
                if (text == null) text = "[attachment]";

                return String.format(Locale.getDefault(), "%s: %s (%s)",
                        message.getUser().getName(), text, createdAt);
            }
        };
    }


}

package com.tagusnow.tagmejob.V2.Message;

import android.view.View;

import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;

public class MessageDialogViewHolder extends DialogsListAdapter.DialogViewHolder<Dialog> {

    private View onlineIndicator;

    public MessageDialogViewHolder(View itemView) {
        super(itemView);
        onlineIndicator = itemView.findViewById(R.id.onlineIndicator);
    }

    @Override
    public void onBind(Dialog dialog) {
        super.onBind(dialog);

        if (dialog.getUsers().size() > 1) {
            onlineIndicator.setVisibility(View.GONE);
        } else {
            boolean isOnline = dialog.getUsers().get(0).isOnline();
            onlineIndicator.setVisibility(View.VISIBLE);
            if (isOnline) {
                onlineIndicator.setBackgroundResource(R.drawable.shape_bubble_online);
            } else {
                onlineIndicator.setBackgroundResource(R.drawable.shape_bubble_offline);
            }
        }
    }
}

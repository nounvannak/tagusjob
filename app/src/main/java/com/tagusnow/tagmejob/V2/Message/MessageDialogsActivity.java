package com.tagusnow.tagmejob.V2.Message;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;
import com.tagusnow.tagmejob.fragment.TagUsJobFragment;

public abstract class MessageDialogsActivity extends TagUsJobFragment implements DialogsListAdapter.OnDialogClickListener<Dialog>,
        MessagesListAdapter.OnLoadMoreListener,
        DialogsListAdapter.OnDialogLongClickListener<Dialog> {

    protected ImageLoader imageLoader;
    protected DialogsListAdapter<Dialog> dialogsAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url, Object payload) {
                Log.e("payload",new Gson().toJson(payload));
                Glide.with(getContext())
                        .load(url)
                        .centerCrop()
                        .error(R.drawable.not_available_)
                        .into(imageView);
            }
        };
    }

    @Override
    public void onDialogLongClick(Dialog dialog) {
        SuperUtil.showToast(getContext(),dialog.getDialogName(), false);
    }

    @Override
    public void onDialogClick(Dialog dialog) {
        SuperUtil.showToast(getContext(),dialog.getDialogName(), false);
    }
}

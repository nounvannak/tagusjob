package com.tagusnow.tagmejob.V2.Message;

import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.V2.Message.Adapter.MessageListAdapter;
import com.tagusnow.tagmejob.V2.Message.Fixtures.UserListFixture;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageV2 extends MessageDialogsActivity {

    private static final String TAG = MessageV2.class.getSimpleName();
    private SmUser user;
    private UserPaginate paginate;
    private UserService userService = ServiceGenerator.createService(UserService.class);
    private Socket socket;
    private Emitter.Listener NewMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e(TAG,new Gson().toJson(obj));
                        if (obj.getInt("user_id")==user.getId()){
                            if (obj.has("chat_list")){
                                Log.e(TAG,new String(Base64.decode(obj.getString("chat_list"),Base64.DEFAULT)));
                                UserPaginate paginate = new Gson().fromJson(new String(Base64.decode(obj.getString("chat_list"),Base64.DEFAULT)),UserPaginate.class);
                                First(paginate);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private void InitSocket(){
        socket = new App().getSocket();
        socket.on("error", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, Arrays.toString(args));
            }
        });
        socket.on("New Message",NewMessage);
        socket.connect();
    }
    private void initTemp(){
        this.user = new Auth(getContext()).checkAuth().token();
    }

    @Override
    public void onDestroy() {
        if (socket.connected()){
            socket.disconnect();
        }
        Log.e(TAG,"destroy");
        super.onDestroy();
    }

    private void First(){
        userService.ChatList(this.user.getId(),0,10).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    First(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void First(UserPaginate paginate){
        this.paginate = paginate;
        UserListFixture userListFixture = new UserListFixture(paginate);
        super.dialogsAdapter.setItems(userListFixture.getDialogs());
        dialogsList.setAdapter(super.dialogsAdapter);
    }

    private void Next(){
        int page = this.paginate.getCurrent_page() + 1;
        userService.ChatList(this.user.getId(),0,10,page).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    Next(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void Next(UserPaginate paginate){
        this.paginate = paginate;
        UserListFixture userListFixture = new UserListFixture(paginate);
        super.dialogsAdapter.setItems(userListFixture.getDialogs());
    }

    public MessageV2() {}
    public static MessageV2 newInstance() {
        return new MessageV2();
    }

    private DialogsList dialogsList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivity(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_v2, container, false);
        dialogsList = (DialogsList) view.findViewById(R.id.dialogsList);
        initTemp();
        InitSocket();
        initAdapter();
        First();
        return view;
    }

    @Override
    public void onDialogClick(Dialog dialog) {
        Log.e(TAG,new Gson().toJson(dialog.getUsers().get(0).getUser()));
//        MessageBoxV2.open(getContext());
        startActivity(MessageBoxV2.createIntent(getActivity())
                .putExtra(MessageBoxV2.CHAT_ID,dialog.getUsers().get(0).getUser().getConversation().getConversation_id())
                .putExtra(MessageBoxV2.USER_TWO,new Gson().toJson(dialog)));
    }

    private void initAdapter() {
        super.dialogsAdapter = new MessageListAdapter(
                R.layout.item_custom_dialog_view_holder,
                MessageDialogViewHolder.class,
                super.imageLoader);

        super.dialogsAdapter.setOnDialogClickListener(this);
        super.dialogsAdapter.setOnDialogLongClickListener(this);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        if (this.paginate!=null){
            Next();
        }else {
            First();
        }
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }
}

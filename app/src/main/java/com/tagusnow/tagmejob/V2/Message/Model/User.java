package com.tagusnow.tagmejob.V2.Message.Model;

import com.stfalcon.chatkit.commons.models.IUser;
import com.tagusnow.tagmejob.auth.SmUser;

public class User implements IUser {
    private String id;
    private String name;
    private String avatar;
    private boolean online;

    public User(String id, String name, String avatar, boolean online, SmUser user) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.online = online;
        this.user = user;
    }

    private SmUser user;

    public User(String id, String name, String avatar, boolean online) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.online = online;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatar() {
        return avatar;
    }

    public boolean isOnline() {
        return online;
    }

    public SmUser getUser() {
        return user;
    }
}

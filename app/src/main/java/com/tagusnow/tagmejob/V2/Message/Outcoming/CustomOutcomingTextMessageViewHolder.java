package com.tagusnow.tagmejob.V2.Message.Outcoming;

import android.view.View;

import com.stfalcon.chatkit.messages.MessageHolders;
import com.tagusnow.tagmejob.V2.Message.Model.Message;

public class CustomOutcomingTextMessageViewHolder
        extends MessageHolders.OutcomingTextMessageViewHolder<Message> {

    public CustomOutcomingTextMessageViewHolder(View itemView, Object payload) {
        super(itemView, payload);
    }

    @Override
    public void onBind(Message message) {
        super.onBind(message);

        time.setText(message.getStatus() + " " + time.getText());
    }
}

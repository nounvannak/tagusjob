package com.tagusnow.tagmejob.V2.Model;

import com.tagusnow.tagmejob.feed.FeedResponse;

import java.io.Serializable;

public class Company implements Serializable {
    public String getTitle_name() {
        return title_name;
    }

    public void setTitle_name(String title_name) {
        this.title_name = title_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public Company() {
        super();
    }

    private String title_name;
    private String address;
    private String image;
    private double lat;
    private double lng;
    private int zoom;

    public Company(String title_name, String address, String image, double lat, double lng, int zoom, FeedResponse feeds) {
        this.title_name = title_name;
        this.address = address;
        this.image = image;
        this.lat = lat;
        this.lng = lng;
        this.zoom = zoom;
        this.feeds = feeds;
    }

    private FeedResponse feeds;

    public FeedResponse getFeeds() {
        return feeds;
    }

    public void setFeeds(FeedResponse feeds) {
        this.feeds = feeds;
    }
}

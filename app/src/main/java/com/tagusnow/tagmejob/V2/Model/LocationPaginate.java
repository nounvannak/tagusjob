package com.tagusnow.tagmejob.V2.Model;

import com.tagusnow.tagmejob.V2.Paginate.BasePaginate;

import java.util.List;

public class LocationPaginate extends BasePaginate<Location> {
    
    public LocationPaginate(int current_page, int last_page, int from, int to, int total, String per_page, String next_page_url, String path, String prev_page_url, List<Location> data) {
        super(current_page, last_page, from, to, total, per_page, next_page_url, path, prev_page_url, data);
    }

    public LocationPaginate() {
        super();
    }

    @Override
    public int getCurrent_page() {
        return super.getCurrent_page();
    }

    @Override
    public void setCurrent_page(int current_page) {
        super.setCurrent_page(current_page);
    }

    @Override
    public int getLast_page() {
        return super.getLast_page();
    }

    @Override
    public void setLast_page(int last_page) {
        super.setLast_page(last_page);
    }

    @Override
    public int getFrom() {
        return super.getFrom();
    }

    @Override
    public void setFrom(int from) {
        super.setFrom(from);
    }

    @Override
    public int getTo() {
        return super.getTo();
    }

    @Override
    public void setTo(int to) {
        super.setTo(to);
    }

    @Override
    public int getTotal() {
        return super.getTotal();
    }

    @Override
    public void setTotal(int total) {
        super.setTotal(total);
    }

    @Override
    public String getPer_page() {
        return super.getPer_page();
    }

    @Override
    public void setPer_page(String per_page) {
        super.setPer_page(per_page);
    }

    @Override
    public String getNext_page_url() {
        return super.getNext_page_url();
    }

    @Override
    public void setNext_page_url(String next_page_url) {
        super.setNext_page_url(next_page_url);
    }

    @Override
    public String getPath() {
        return super.getPath();
    }

    @Override
    public void setPath(String path) {
        super.setPath(path);
    }

    @Override
    public String getPrev_page_url() {
        return super.getPrev_page_url();
    }

    @Override
    public void setPrev_page_url(String prev_page_url) {
        super.setPrev_page_url(prev_page_url);
    }

    @Override
    public List<Location> getData() {
        return super.getData();
    }

    @Override
    public void setData(List<Location> data) {
        super.setData(data);
    }
}

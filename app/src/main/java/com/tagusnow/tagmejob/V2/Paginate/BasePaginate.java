package com.tagusnow.tagmejob.V2.Paginate;

import java.io.Serializable;
import java.util.List;

public abstract class BasePaginate<U> implements Serializable {

    public BasePaginate(int current_page, int last_page, int from, int to, int total, String per_page, String next_page_url, String path, String prev_page_url, List<U> data) {
        this.current_page = current_page;
        this.last_page = last_page;
        this.from = from;
        this.to = to;
        this.total = total;
        this.per_page = per_page;
        this.next_page_url = next_page_url;
        this.path = path;
        this.prev_page_url = prev_page_url;
        this.data = data;
    }

    public BasePaginate() {
        super();
    }

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public int getLast_page() {
        return last_page;
    }

    public void setLast_page(int last_page) {
        this.last_page = last_page;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getPer_page() {
        return per_page;
    }

    public void setPer_page(String per_page) {
        this.per_page = per_page;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public List<U> getData() {
        return data;
    }

    public void setData(List<U> data) {
        this.data = data;
    }

    private int current_page;
    private int last_page;
    private int from;
    private int to;
    private int total;
    private String per_page;
    private String next_page_url;
    private String path;
    private String prev_page_url;
    private List<U> data;
}

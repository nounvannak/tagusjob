package com.tagusnow.tagmejob.V2.Suggest.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.V2.Suggest.UserSuggest;
import com.tagusnow.tagmejob.V2.Suggest.UserSuggestHolder;
import com.tagusnow.tagmejob.V2.Suggest.UserSuggestListener;
import com.tagusnow.tagmejob.auth.SmUser;

import java.util.List;

public class UserSuggestAdapter extends RecyclerView.Adapter<UserSuggestHolder> {

    public UserSuggestAdapter(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    private Activity activity;
    private Context context;

    public void setUserSuggestListener(UserSuggestListener userSuggestListener) {
        this.userSuggestListener = userSuggestListener;
    }

    private UserSuggestListener userSuggestListener;
    private SmUser auth;

    public void setUsers(List<SmUser> users) {
        this.users = users;
    }

    private List<SmUser> users;

    @NonNull
    @Override
    public UserSuggestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserSuggestHolder(new UserSuggest(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull UserSuggestHolder holder, int position) {
        holder.bindView(userSuggestListener,activity,auth,users.get(position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}

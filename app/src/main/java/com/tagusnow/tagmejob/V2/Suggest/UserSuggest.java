package com.tagusnow.tagmejob.V2.Suggest;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;

public class UserSuggest extends TagUsJobRelativeLayout implements View.OnClickListener {

    public static final String TAG = UserSuggest.class.getSimpleName();
    private Context context;
    private AvatarView profile_picture;
    private TextView profile_name,follower_counter;
    private ImageButton btnClose;
    private Button btnFollow;
    private AvatarPlaceholder refreshableAvatarPlaceholder;
    private IImageLoader imageLoader;
    private SmUser auth;

    public void setButtonTitle(String buttonTitle) {
        this.btnFollow.setText(buttonTitle);
    }

    public void setAuth(SmUser auth) {
        this.auth = auth;
    }

    public void setUser(SmUser user) {
        this.user = user;
        if (user!=null){
            this.setVisibility(VISIBLE);
            refreshableAvatarPlaceholder = new AvatarPlaceholder(this.user.getUser_name(), 50);
            imageLoader = new GlideLoader();
            if (user.getImage()!=null){
                if (user.getImage().contains("http")){
                    imageLoader.loadImage(profile_picture, refreshableAvatarPlaceholder,user.getImage());
                }else {
                    imageLoader.loadImage(profile_picture, refreshableAvatarPlaceholder, SuperUtil.getProfilePicture(user.getImage()));
                }
            }else {
                imageLoader.loadImage(profile_picture, refreshableAvatarPlaceholder,null);
            }
            profile_name.setText(user.getUser_name());

            if (user.getFollower_counter() > 0){
                follower_counter.setVisibility(VISIBLE);
                follower_counter.setText(this.activity.getString(R.string.follower_counter,user.getFollower_counter()));
            }else {
                follower_counter.setVisibility(GONE);
                if (user.getCounter_post_jobs() > 0){
                    follower_counter.setVisibility(VISIBLE);
                    follower_counter.setText(this.activity.getString(R.string.counter_post_jobs,user.getCounter_post_jobs()));
                }else {
                    follower_counter.setVisibility(GONE);
                }
            }

        }else {
            this.setVisibility(GONE);
            Toast.makeText(this.context,"No SmUser",Toast.LENGTH_SHORT).show();
        }
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private SmUser user;
    private int position;

    public void setUserSuggestListener(UserSuggestListener userSuggestListener) {
        this.userSuggestListener = userSuggestListener;
    }

    private UserSuggestListener userSuggestListener;

    @Override
    public void showProgress() {
        super.showProgress();
    }

    @Override
    public void dismissProgress() {
        super.dismissProgress();
    }

    @Override
    public void ShowAlert(String title, String msg) {
        super.ShowAlert(title, msg);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.follow_suggest_face,this);
        this.context = context;
        profile_picture = (AvatarView)findViewById(R.id.profile_picture);
        profile_name = (TextView)findViewById(R.id.profile_name);
        follower_counter = (TextView)findViewById(R.id.follower_counter);
        btnClose = (ImageButton)findViewById(R.id.btnClose);
        btnFollow = (Button)findViewById(R.id.btnFollow);
        btnClose.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public UserSuggest(Context context) {
        super(context);
        this.InitUI(context);
    }

    public UserSuggest(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public UserSuggest(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public UserSuggest(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v==this){
            this.userSuggestListener.viewProfile(this,this.auth,this.user,this.position);
        }else if (v==btnClose){
            this.userSuggestListener.close(this,this.user,this.position);
        }else if (v==btnFollow){
            this.userSuggestListener.following(this,this.user,this.position);
        }
    }
}

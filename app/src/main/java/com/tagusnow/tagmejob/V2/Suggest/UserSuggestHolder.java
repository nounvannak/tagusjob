package com.tagusnow.tagmejob.V2.Suggest;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class UserSuggestHolder extends RecyclerView.ViewHolder {

    private UserSuggest suggestUser;

    public UserSuggestHolder(View itemView) {
        super(itemView);
        suggestUser = (UserSuggest) itemView;
    }

    public void bindView(UserSuggestListener listener,Activity activity, SmUser auth,SmUser user){
        suggestUser.setActivity(activity);
        suggestUser.setAuth(auth);
        suggestUser.setUser(user);
        suggestUser.setUserSuggestListener(listener);
        suggestUser.setPosition(getAdapterPosition());
    }
}

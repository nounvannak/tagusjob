package com.tagusnow.tagmejob.V2.Suggest;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class UserSuggestLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    public static final String TAG = UserSuggestLayout.class.getSimpleName();
    private Context context;
    private Button btnSeeMore;
    private RecyclerView recyclerView;

    public void setAuth(SmUser auth) {
        this.auth = auth;
    }

    private SmUser auth;
//    private UserPaginate userPaginate;r
//    private List<SmUser> users;

    public void setUserSuggestListener(UserSuggestListener userSuggestListener) {
        this.userSuggestListener = userSuggestListener;
    }

    private UserSuggestListener userSuggestListener;

    private void InitUI(Context context){
        inflate(context, R.layout.follow_suggest_horizontal_layout,this);
        this.context = context;
        this.btnSeeMore = (Button)findViewById(R.id.btnSeeMore);
        this.btnSeeMore.setOnClickListener(this);
        this.recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(layoutManager);
    }

    public UserSuggestLayout(Context context) {
        super(context);
        this.InitUI(context);
    }

    public UserSuggestLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public UserSuggestLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public UserSuggestLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
        this.userSuggestListener.onScroll(this,this.recyclerView);
    }

    @Override
    public void onClick(View v) {
        if (v==btnSeeMore){
            this.userSuggestListener.seeMore(this,this.auth);
        }
    }
}

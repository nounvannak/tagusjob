package com.tagusnow.tagmejob.V2.Suggest;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class UserSuggestLayoutHolder extends RecyclerView.ViewHolder {

    private UserSuggestLayout userSuggestLayout;

    public UserSuggestLayoutHolder(View itemView) {
        super(itemView);
        userSuggestLayout = (UserSuggestLayout) itemView;
    }

    public void bindView(Activity activity, SmUser user,UserSuggestListener listener){
        userSuggestLayout.setActivity(activity);
        userSuggestLayout.setAuth(user);
        userSuggestLayout.setUserSuggestListener(listener);
    }
}

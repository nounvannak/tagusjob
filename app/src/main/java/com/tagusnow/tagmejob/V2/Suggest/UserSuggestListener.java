package com.tagusnow.tagmejob.V2.Suggest;

import android.support.v7.widget.RecyclerView;

import com.tagusnow.tagmejob.auth.SmUser;

public interface UserSuggestListener {
    void following(UserSuggest view, SmUser user, int position);
    void close(UserSuggest view,SmUser user,int position);
    void viewProfile(UserSuggest view,SmUser authUser,SmUser user,int position);
    void seeMore(UserSuggestLayout view,SmUser user);
    void onScroll(UserSuggestLayout view, RecyclerView recyclerView);
}

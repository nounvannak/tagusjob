package com.tagusnow.tagmejob.V2.View;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.tagusnow.tagmejob.V2.Paginate.BasePaginate;

public abstract class BaseRecycler<T extends BasePaginate<U>,U> extends RecyclerView {

    T data;
    int lastPage;
    boolean isRequest;

    public BaseRecycler(Context context) {
        super(context);
    }

    public BaseRecycler(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseRecycler(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setListener(BaseRecyclerListener listener) {
        recyclerListener = listener;
    }

    public void onScroll(){
        addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (data.getTo() - 1)) && (data.getTo() < data.getTotal())) {
                        if (data.getNext_page_url()!=null || !data.getNext_page_url().equals("")){
                            if (lastPage!=data.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = data.getCurrent_page();
                                    recyclerListener.loadMore(data);
                                }
                            }else{
                                isRequest = false;
                                recyclerListener.noLoadMore(data,false);
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private BaseRecyclerListener<T> recyclerListener;
}

package com.tagusnow.tagmejob.V2.View;

public interface BaseRecyclerListener<T> {
    void loadMore(T data);
    void noLoadMore(T data,boolean isLoadMore);
}

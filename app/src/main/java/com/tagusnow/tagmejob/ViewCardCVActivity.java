package com.tagusnow.tagmejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.artjimlop.altex.AltexImageDownloader;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.CV;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.Skill;
import com.tagusnow.tagmejob.model.v2.feed.Education;
import com.tagusnow.tagmejob.model.v2.feed.Experience;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCardCVActivity extends TagUsJobActivity implements DocumentListener<DocumentCell> {

    private static final String TAG = ViewCardCVActivity.class.getSimpleName();
    public static final String FEED = "feed";
    private Toolbar toolbar;
    private SmUser Auth;
    private Feed feed;
    private RecyclerView education,workExperience,skill,document;
    private TextView name,gender,phone,email,position,dob,address;
    private RoundedImageView profile;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<List<Education>> FetchEducationCallback = new Callback<List<Education>>() {
        @Override
        public void onResponse(Call<List<Education>> call, Response<List<Education>> response) {
            if (response.isSuccessful()){
                Log.e(TAG,new Gson().toJson(response.body()));
                fetchEducation(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<List<Education>> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<List<Experience>> FetchExperienceCallback = new Callback<List<Experience>>() {
        @Override
        public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
            if (response.isSuccessful()){
                fetchExperience(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<List<Experience>> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<List<Skill>> FetchSkillCallback = new Callback<List<Skill>>() {
        @Override
        public void onResponse(Call<List<Skill>> call, Response<List<Skill>> response) {
            if (response.isSuccessful()){
                fetchSkill(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<List<Skill>> call, Throwable t) {
            t.printStackTrace();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_card_cv);

        initTemp();
        initUI();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ViewCardCVActivity.class);
    }

    private void initTemp(){
        this.Auth = new Auth(this).checkAuth().token();
        this.feed = new Gson().fromJson(getIntent().getStringExtra(FEED),Feed.class);
    }

    private void initUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        name = (TextView)findViewById(R.id.name);
        gender = (TextView)findViewById(R.id.gender);
        phone = (TextView)findViewById(R.id.phone);
        email = (TextView)findViewById(R.id.email);
        position = (TextView)findViewById(R.id.position);
        dob = (TextView)findViewById(R.id.dob);
        address = (TextView)findViewById(R.id.address);

        profile = (RoundedImageView)findViewById(R.id.profile);

        education = (RecyclerView)findViewById(R.id.education);
        workExperience = (RecyclerView)findViewById(R.id.workExperience);
        skill = (RecyclerView)findViewById(R.id.skill);
        document = (RecyclerView)findViewById(R.id.document);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this),
                manager1 = new LinearLayoutManager(this),
                manager2 = new LinearLayoutManager(this),
                manager3 = new LinearLayoutManager(this);

        education.setHasFixedSize(true);
        workExperience.setHasFixedSize(true);
        skill.setHasFixedSize(true);
        document.setHasFixedSize(true);

        education.setLayoutManager(manager);
        workExperience.setLayoutManager(manager1);
        skill.setLayoutManager(manager2);
        document.setLayoutManager(manager3);

        if (feed != null){
            setAlbumPhoto(profile,(feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0 ? feed.getFeed().getAlbum().get(0) : null));
            setName(name,feed.getUser_post());
            position.setText(feed.getFeed().getCategory());
            gender.setText(feed.getUser_post().getGender());
            email.setText(feed.getFeed().getEmail() != null && !feed.getFeed().getEmail().equals("") ? feed.getFeed().getEmail() : "N/A");
            phone.setText(feed.getFeed().getPhone() != null && !feed.getFeed().getPhone().equals("") ? feed.getFeed().getPhone() : "N/A");
            dob.setText(feed.getUser_post().getDateOfBirth());
            address.setText(feed.getUser_post().getLocation() != null && !feed.getUser_post().getLocation().equals("") ? feed.getUser_post().getLocation() : "N/A");

            if (feed.getFeed().getStaff_file() != null && feed.getFeed().getStaff_file().size() > 0){
                DocumentAdapter documentAdapter = new DocumentAdapter(this,feed.getFeed().getStaff_file(),this);
                document.setAdapter(documentAdapter);
            }

            fetchEducation(feed.getUser_post());
            fetchExperience(feed.getUser_post());
            fetchSkill(feed.getUser_post());

        }
    }

    private void fetchEducation(SmUser user){
        service.GetEducation(user.getId()).enqueue(FetchEducationCallback);
    }

    private void fetchEducation(List<Education> educations){
        EducationAdapter educationAdapter = new EducationAdapter(this,educations);
        education.setAdapter(educationAdapter);
    }

    private void fetchExperience(SmUser user){
        service.GetExperience(user.getId()).enqueue(FetchExperienceCallback);
    }

    private void fetchExperience(List<Experience> experiences){
        ExperienceAdapter experienceAdapter = new ExperienceAdapter(this,experiences);
        workExperience.setAdapter(experienceAdapter);
    }

    private void fetchSkill(SmUser user){
        service.GetSkill(user.getId()).enqueue(FetchSkillCallback);
    }

    private void fetchSkill(List<Skill> skills){
        SkillAdapter skillAdapter = new SkillAdapter(this,skills);
        skill.setAdapter(skillAdapter);
    }

    @Override
    public void downloadFile(DocumentCell view, CV file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Download").setMessage("Are you sure to download this file?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(ViewCardCVActivity.this,"Downloading...",Toast.LENGTH_SHORT).show();
                AltexImageDownloader.writeToDisk(ViewCardCVActivity.this,SuperUtil.getBaseUrl(file.getFile()), Environment.DIRECTORY_DOWNLOADS);
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }
}

// Education
class EducationCell extends TagUsJobRelativeLayout {

    private Context context;
    private TextView year,text;
    private Education education;

    private void InitUI(Context context){
        inflate(context,R.layout.education_cell,this);
        this.context = context;

        year = (TextView)findViewById(R.id.year);
        text = (TextView)findViewById(R.id.text);
    }

    public EducationCell(Context context) {
        super(context);
        InitUI(context);
    }

    public EducationCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public EducationCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public EducationCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setEducation(Education education) {
        this.education = education;
        String from = "",to = "",desc = "";
        from = (education.getFrom() != null && !education.getFrom().equals("") ? GetDate(education.getFrom(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss,DATE_FORMAT_MMM_yyyy) : "");
        to = (education.getTo() != null && !education.getTo().equals("") ? GetDate(education.getTo(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss,DATE_FORMAT_MMM_yyyy) : "");
        year.setText((from + " - " + to));

        desc = education.getDegree() + ", " + education.getField_study() + " at " + education.getSchool();
        if (education.getLocation() != null && !education.getLocation().equals("")){
            desc += "\n" + education.getLocation();
        }

        text.setText(desc);
    }
}

class EducationCellHolder extends RecyclerView.ViewHolder {

    private EducationCell educationCell;

    public EducationCellHolder(View itemView) {
        super(itemView);
        educationCell = (EducationCell)itemView;
    }

    public void BindView(Activity activity,Education education){
        educationCell.setActivity(activity);
        educationCell.setEducation(education);
    }
}

class EducationAdapter extends RecyclerView.Adapter<EducationCellHolder>{

    public EducationAdapter(Activity activity, List<Education> educations) {
        this.activity = activity;
        this.educations = educations;
    }

    public void NextData(List<Education> educations){
        this.educations.addAll(educations);
    }

    private Activity activity;
    private List<Education> educations;

    @NonNull
    @Override
    public EducationCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EducationCellHolder(new EducationCell(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull EducationCellHolder holder, int position) {
        holder.BindView(activity,educations.get(position));
    }

    @Override
    public int getItemCount() {
        return educations.size();
    }
}

// Experience
class ExperienceCell extends TagUsJobRelativeLayout {

    private Context context;
    private TextView year,text;
    private Experience experience;

    private void InitUI(Context context){
        inflate(context,R.layout.education_cell,this);
        this.context = context;

        year = (TextView)findViewById(R.id.year);
        text = (TextView)findViewById(R.id.text);
    }

    public ExperienceCell(Context context) {
        super(context);
        InitUI(context);
    }

    public ExperienceCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public ExperienceCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public ExperienceCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
        String from = "",to = "",desc = "";
        from = (experience.getFrom() != null && !experience.getFrom().equals("") ? GetDate(experience.getFrom(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss,DATE_FORMAT_MMM_yyyy) : "");
        to = (experience.getTo() != null && !experience.getTo().equals("") ? GetDate(experience.getTo(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss,DATE_FORMAT_MMM_yyyy) : "");
        year.setText((from + " - " + to));

        desc = experience.getName() + " at " + experience.getCompany();
        if (experience.getLocation() != null && !experience.getLocation().equals("")){
            desc += "\n" + experience.getHeadline() + "\n" + experience.getLocation();
        }

        text.setText(desc);
    }
}

class ExperienceCellHolder extends RecyclerView.ViewHolder {

    private ExperienceCell experienceCell;

    public ExperienceCellHolder(View itemView) {
        super(itemView);
        experienceCell = (ExperienceCell)itemView;
    }

    public void BindView(Activity activity,Experience experience){
        experienceCell.setActivity(activity);
        experienceCell.setExperience(experience);
    }
}

class ExperienceAdapter extends RecyclerView.Adapter<ExperienceCellHolder>{

    private Activity activity;

    public ExperienceAdapter(Activity activity, List<Experience> experiences) {
        this.activity = activity;
        this.experiences = experiences;
    }

    public void NextData(List<Experience> experiences){
        this.experiences.addAll(experiences);
    }

    private List<Experience> experiences;

    @NonNull
    @Override
    public ExperienceCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ExperienceCellHolder(new ExperienceCell(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull ExperienceCellHolder holder, int position) {
        holder.BindView(activity,experiences.get(position));
    }

    @Override
    public int getItemCount() {
        return experiences.size();
    }
}

// Skill
class SkillCell extends TagUsJobRelativeLayout {

    private Context context;
    private TextView year,text;
    private Skill skill;

    private void InitUI(Context context){
        inflate(context,R.layout.education_cell,this);
        this.context = context;

        year = (TextView)findViewById(R.id.year);
        text = (TextView)findViewById(R.id.text);
    }

    public SkillCell(Context context) {
        super(context);
        InitUI(context);
    }

    public SkillCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public SkillCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public SkillCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
        String from = "",to = "",desc = "";
        from = (skill.getFrom() != null && !skill.getFrom().equals("") ? GetDate(skill.getFrom(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss,DATE_FORMAT_MMM_yyyy) : "");
        to = (skill.getTo() != null && !skill.getTo().equals("") ? GetDate(skill.getTo(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss,DATE_FORMAT_MMM_yyyy) : "");
        year.setText((from + " - " + to));

        desc = skill.getName();

        text.setText(desc);
    }
}

class SkillCellHolder extends RecyclerView.ViewHolder {

    private SkillCell skillCell;

    public SkillCellHolder(View itemView) {
        super(itemView);
        skillCell = (SkillCell)itemView;
    }

    public void BindView(Activity activity,Skill skill){
        skillCell.setActivity(activity);
        skillCell.setSkill(skill);
    }
}

class SkillAdapter extends RecyclerView.Adapter<SkillCellHolder>{

    private Activity activity;

    public SkillAdapter(Activity activity, List<Skill> skills) {
        this.activity = activity;
        this.skills = skills;
    }

    public void NextData(List<Skill> skills){
        this.skills.addAll(skills);
    }

    private List<Skill> skills;

    @NonNull
    @Override
    public SkillCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SkillCellHolder(new SkillCell(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull SkillCellHolder holder, int position) {
        holder.BindView(activity,skills.get(position));
    }

    @Override
    public int getItemCount() {
        return skills.size();
    }
}

// Document
class DocumentCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private TextView text;
    private ImageView imageView;
    private CV cv;

    public void setDocumentListener(DocumentListener<DocumentCell> documentListener) {
        this.documentListener = documentListener;
    }

    private DocumentListener<DocumentCell> documentListener;

    private void InitUI(Context context){
        inflate(context,R.layout.document_cell,this);
        this.context = context;

        imageView = (ImageView) findViewById(R.id.icon);
        text = (TextView)findViewById(R.id.text);

        this.setOnClickListener(this);
    }

    public DocumentCell(Context context) {
        super(context);
        InitUI(context);
    }

    public DocumentCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public DocumentCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public DocumentCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setCv(CV cv) {
        this.cv = cv;

        if (cv.getFile() != null && !cv.getFile().equals("")){
            if (cv.getType().equals(CV.COVER)){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imageView.setImageDrawable(context.getDrawable(R.drawable.doc));
                }
                text.setText(context.getString(R.string.cover_letter));
            }else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imageView.setImageDrawable(context.getDrawable(R.drawable.icon_resume));
                }
                text.setText(context.getString(R.string.resume));
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == this){
            documentListener.downloadFile(this,cv);
        }
    }
}

class DocumentCellHolder extends RecyclerView.ViewHolder {

    private DocumentCell documentCell;

    public DocumentCellHolder(View itemView) {
        super(itemView);
        documentCell = (DocumentCell)itemView;
    }

    public void BindView(Activity activity,CV cv,DocumentListener<DocumentCell> documentListener){
        documentCell.setActivity(activity);
        documentCell.setCv(cv);
        documentCell.setDocumentListener(documentListener);
    }
}

class DocumentAdapter extends RecyclerView.Adapter<DocumentCellHolder> {

    private Activity activity;
    private List<CV> cvs;

    public DocumentAdapter(Activity activity, List<CV> cvs, DocumentListener<DocumentCell> documentListener) {
        this.activity = activity;
        this.cvs = cvs;
        this.documentListener = documentListener;
    }

    public void NextData(List<CV> cvs){
        this.cvs.addAll(cvs);
    }

    private DocumentListener<DocumentCell> documentListener;

    @NonNull
    @Override
    public DocumentCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DocumentCellHolder(new DocumentCell(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentCellHolder holder, int position) {
        holder.BindView(activity,cvs.get(position),documentListener);
    }

    @Override
    public int getItemCount() {
        return cvs.size();
    }
}

interface DocumentListener<V extends TagUsJobRelativeLayout> {
    void downloadFile(V view,CV file);
}
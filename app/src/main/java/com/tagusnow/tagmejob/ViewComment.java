package com.tagusnow.tagmejob;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ahmedadeltito.photoeditorsdk.BrushDrawingView;
import com.ahmedadeltito.photoeditorsdk.PhotoEditorSDK;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.SmComment;

public class ViewComment extends AppCompatActivity implements View.OnClickListener{

    private RelativeLayout commentImageViewParent,commentImageViewDelete;
    private ImageView commentImageView;
    private ImageButton closeViewComment;
    private TextView txtCommentStory;
    private BrushDrawingView commentImageViewBrush;
    private SmComment.Comment.Data mSmComment;
    private SmUser commentUser;
    private PhotoEditorSDK photoEditorSDK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_comment);

        Intent mCommentIntent = getIntent();
        mSmComment = new Gson().fromJson(mCommentIntent.getStringExtra("smComment"), SmComment.Comment.Data.class);
        commentUser = new Gson().fromJson(mCommentIntent.getStringExtra("commentUser"),SmUser.class);

        /*init view*/
        commentImageViewParent = (RelativeLayout)findViewById(R.id.commentImageViewParent);
        commentImageViewDelete = new RelativeLayout(this);
        commentImageView = (ImageView)findViewById(R.id.commentImageView);
        closeViewComment = (ImageButton) findViewById(R.id.closeViewComment);
        txtCommentStory = (TextView) findViewById(R.id.txtCommentStory);

        txtCommentStory.setText(getString(R.string.comment_story,commentUser.getUser_name()+"",mSmComment.getTimeline()));
        closeViewComment.setOnClickListener(this);
        Glide.with(this)
                .load(SuperUtil.getCommentPicture(mSmComment.getImage(),"500"))
                .fitCenter()
                .into(commentImageView);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id==R.id.closeViewComment){
            onBackPressed();
        }
    }
}

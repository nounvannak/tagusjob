package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.ViewDetailPostAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.model.v2.feed.Socket.SocketTyping;
import com.tagusnow.tagmejob.view.CommentBox;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewDetailPostActivity extends TagUsJobActivity {

    private static final String TAG = ViewDetailPostActivity.class.getSimpleName();
    public static final String FEED = "Feed";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ViewDetailPostAdapter adapter;
    private Feed feed;
    private SmUser authUser;
    private CommentBox commentBox;
    private CommentResponse commentResponse;
    private CommentService service = ServiceGenerator.createService(CommentService.class);

    /*Socket.IO*/
    Socket socket;
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("New Comment "+feed.getId(),NEW_COMMENT);
        socket.on("Typing",TYPING);
        socket.on("Stop Typing",STOP_TYPING);
        socket.connect();
    }

    private Emitter.Listener NEW_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj!=null){
                            Comment comment = new Gson().fromJson(obj.toString(),Comment.class);
                            initNewComment(comment);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener TYPING = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,String.valueOf(obj.getInt("feed_id")));
                        if (feed.getId()==obj.getInt("feed_id")){
                            if (obj.has("user_id") && (obj.getInt("user_id")!=authUser.getId())){
                                String user_name = obj.getString("user_name");
                                String image = "";
                                displayUserComment(image,user_name);
                            }else {
                                hideUserComment();
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener STOP_TYPING = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,String.valueOf(obj.getInt("feed_id")));
                        if (feed.getId()==obj.getInt("feed_id")){
                            hideUserComment();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void initNewComment(Comment comment){
        int to = commentResponse.getTo() + 1;
        int from = commentResponse.getFrom() + 1;
        int total = commentResponse.getTotal() + 1;
        ArrayList<Comment> mComments = new ArrayList<>();
        commentResponse.setTo(to);
        commentResponse.setFrom(from);
        commentResponse.setTotal(total);
        mComments.add(comment);
        commentResponse.setData(mComments);
        this.CheckNext();
        this.adapter.update(this,this.commentResponse).notifyDataSetChanged();
        recyclerView.scrollToPosition(commentResponse.getTo() -1);
    }

    private void displayUserComment(String image,String name){
        this.adapter.Commenting(image,name).notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(commentResponse.getTo() - 1);
    }

    private void hideUserComment(){
        this.adapter.isComment(false).notifyDataSetChanged();
        recyclerView.scrollToPosition(commentResponse.getTo() -1);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ViewDetailPostActivity.class);
    }

    private void First(){
        service.getList(this.feed.getId(),10).enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                if (response.isSuccessful()){
                    First(response.body());
                }else {
                    try {
                        call.execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void Next(){
        int page = commentResponse.getCurrent_page() + 1;
        service.getList(this.feed.getId(),10,page).enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                if (response.isSuccessful()){
                    Next(response.body());
                }else {
                    try {
                        call.execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                ShowAlert("Connction Error!",t.getMessage());
            }
        });
    }

    private void Next(CommentResponse commentResponse){
        this.commentResponse = commentResponse;
        this.CheckNext();
        this.adapter.update(this,commentResponse).notifyDataSetChanged();
    }

    private void First(CommentResponse commentResponse){
//        this.commentResponse = commentResponse;
//        this.CheckNext();
//        this.adapter = new ViewDetailPostAdapter(this,commentResponse,authUser,feed);
//        recyclerView.setAdapter(this.adapter);
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (commentResponse.getTo() - 1)) && (commentResponse.getTo() < commentResponse.getTotal())) {
                        if (commentResponse.getNext_page_url()!=null || !commentResponse.getNext_page_url().equals("")){
                            if (lastPage!=commentResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = commentResponse.getCurrent_page();
                                    Next();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initTemp(){
        this.authUser = new Auth(this).checkAuth().token();
        feed = new Gson().fromJson(getIntent().getStringExtra(FEED),Feed.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_detail_post);

        /*get intent extra*/
        initTemp();
        initSocket();
        /*init toolbar*/
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        toolbar.setTitle(getString(R.string.view_post_detail_title,feed.getUser_post().getUser_name()));
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        commentBox = (CommentBox)findViewById(R.id.comment_box);
        commentBox.setAuthUser(authUser).setFeed(feed).setActivity(this);
        /*init recycler view*/
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        this.First();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}

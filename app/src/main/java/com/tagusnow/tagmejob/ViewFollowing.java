package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.adapter.ViewFollowerAdapter;
import com.tagusnow.tagmejob.adapter.ViewFollowingAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewFollowing extends AppCompatActivity {

    private static final String TAG = ViewFollowing.class.getSimpleName();
    private SmUser authUser;
    private UserPaginate userPaginate;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private SpinKitView spinKitView;
    private ViewFollowingAdapter adapter;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService service = retrofit.create(FollowService.class);
    private Callback<UserPaginate> mFollowerCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initFollower(response.body());
                spinKitView.setVisibility(View.GONE);
            }else {
                Log.e(TAG,response.errorBody().toString());
                spinKitView.setVisibility(View.GONE);
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<UserPaginate> mNextFollowerCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextFollower(response.body());
                spinKitView.setVisibility(View.GONE);
            }else {
                Log.e(TAG,response.errorBody().toString());
                spinKitView.setVisibility(View.GONE);
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void callFollower(){
        spinKitView.setVisibility(View.VISIBLE);
        int user = authUser!=null ? authUser.getId() : 0;
        int limit = 20;
        service.callFollowing(user,limit).enqueue(mFollowerCallback);
    }

    private void callNextFollower(){
        spinKitView.setVisibility(View.VISIBLE);
        int user = authUser!=null ? authUser.getId() : 0;
        int limit = 20;
        int page = userPaginate.getCurrent_page() + 1;
        service.callFollowing(user,limit,page).enqueue(mNextFollowerCallback);
    }

    private void initFollower(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.checkNext();
        adapter = new ViewFollowingAdapter(this,userPaginate);
        recyclerView.setAdapter(adapter);
    }

    private void initNextFollower(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.checkNext();
        adapter.update(this,userPaginate).notifyDataSetChanged();
    }

    static int lastPage;
    static boolean isRequest = false;

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    callNextFollower();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_following);
        initTemp();
        initView();
        callFollower();
    }

    private void initView(){
        initToolbar();
        initRecycler();
        spinKitView = (SpinKitView)findViewById(R.id.loading);
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.following_,this.authUser.getFollowing_counter()));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        initTemp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTemp();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initTemp();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ViewFollowing.class);
    }

    private void initTemp(){
        authUser = new Auth(this).checkAuth().token();
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import com.artjimlop.altex.AltexImageDownloader;
import com.ortiz.touchview.TouchImageView;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Photo;
import com.tagusnow.tagmejob.model.v2.feed.PhotoPaginate;
import com.tagusnow.tagmejob.view.BaseCard.Card;
import com.tagusnow.tagmejob.view.ExtendedViewPager;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.Widget.WidgetFeedFooter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import me.relex.photodraweeview.OnPhotoTapListener;
import me.relex.photodraweeview.OnViewTapListener;

public class ViewImageActivity extends TagUsJobActivity{

    private static final String TAG = ViewImageActivity.class.getSimpleName();
    private static final String DOWNLOAD_FOLDER = "";
    private int position = 0;
    public static final String POSITION = "position";
    public static final String PHOTOS = "photos";
    public static final String PARCEL = PhotoPaginate.class.getCanonicalName();
    private String[] photos;
    private Toolbar toolbar;
    private List<Photo> photoList;
    private PhotoPaginate photoPaginate;
    private WidgetFeedFooter footer;
    private ExtendedViewPager pager;
    private SmUser user;

    @NonNull
    public static Intent createIntent(Activity context) {
       return new Intent(context, ViewImageActivity.class);
    }

    private void initTemp(){
        this.position = getIntent().getIntExtra(POSITION,-1);
        this.photos = getIntent().getStringArrayExtra(PHOTOS);
        Log.e(TAG,String.valueOf(getIntent().getSerializableExtra(PARCEL)!=null));
        if (getIntent().getSerializableExtra(PARCEL)!=null){
            this.photoPaginate = (PhotoPaginate) getIntent().getSerializableExtra(PARCEL);
            this.photoList = this.photoPaginate.getData();
        }
        this.user = new Auth(this).checkAuth().token();
    }

    private void initView(){
        initToolbar();
        initPager();
        initFooter();
    }

    private void initPager(){
        pager =  (ExtendedViewPager)findViewById(R.id.pager);
        pager.setAdapter(new TouchImageAdapter(this.photos,this.position));
    }

    private void initFooter(){
        footer = (WidgetFeedFooter)findViewById(R.id.footer);
        if (this.photoPaginate!=null){
            footer.setVisibility(View.VISIBLE);
            footer.Activity(this).User(this.user).Feed(this.photoList.get(this.position).getFeed_id());
        }else {
            footer.setVisibility(View.GONE);
        }
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void download(){
        Toast.makeText(this,"Downloading...",Toast.LENGTH_SHORT).show();
        AltexImageDownloader.writeToDisk(this,this.photos[this.position],DOWNLOAD_FOLDER);
    }

    private void DisplayFeed(){
        if (this.photoPaginate!=null){
            ScrollView view = new ScrollView(this);
            view.addView(new Card(this).Auth(user).Activity(this).Feed(this.photoList.get(position).getFeed_id()));
            new AlertDialog.Builder(this).setView(view).create().show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_image,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.photoPaginate!=null){
            if (item.getItemId()==R.id.home){
                item.setVisible(false);
            }else {
                item.setVisible(true);
            }
        }else {
            item.setVisible(true);
        }

        switch (item.getItemId()){
            case R.id.download:
                download();
                break;
            case R.id.copy:
                break;
            case R.id.home:
                DisplayFeed();
                break;
                default:
                    break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        initTemp();
        initView();
    }

    static class TouchImageAdapter extends PagerAdapter {

        private static String[] photos;
        private int pos;

        TouchImageAdapter(String[] photos,int pos){
            TouchImageAdapter.photos = photos;
            this.pos = pos;
        }

        @Override
        public int getCount() {
            return TouchImageAdapter.photos.length;
        }

        @NonNull
        @Override
        public View instantiateItem(@NonNull ViewGroup container, int position) {
            TouchImageView img = new TouchImageView(container.getContext());
            if (this.pos > -1){
                img.setImageBitmap(SuperUtil.getBitmap(TouchImageAdapter.photos[this.pos]));
                this.pos = -1;
            }else {
                img.setImageBitmap(SuperUtil.getBitmap(TouchImageAdapter.photos[position]));;
            }
            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return img;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

    }
}

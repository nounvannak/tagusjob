package com.tagusnow.tagmejob;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.adapter.PostMoreImageSelectAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FacebookFeed;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.feed.TagusnowFeed;

import java.util.ArrayList;

public class ViewMoreImageSelectActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView rvMoreViewImageSelect;
    RecyclerView.LayoutManager mManagerMoreViewImageSelect;
    RecyclerView.Adapter mAdapterMoreViewImageSelect;
    ArrayList<String> mFileSelectStr = new ArrayList<>();
    SmTagSubCategory smTagSubCategory;
    Feed mSmTagFeed;
    SmUser authUser;
    Intent mIntent;
    private final String OPEN_MEDIA_TITLE = "title";
    private final String OPEN_MEDIA_MODE = "mode";
    private final String OPEN_MEDIA_MAX = "maxSelection";
    private final int REQUEST_GALLERY = 8930;
    private final int REQUEST_VIEW_IMAGE = 8931;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_more_image_select);

        Intent mEditPostIntent = getIntent();
        authUser = new Gson().fromJson(mEditPostIntent.getStringExtra(SuperConstance.USER_SESSION),SmUser.class);
        smTagSubCategory = new  Gson().fromJson(mEditPostIntent.getStringExtra("SmTagSubCategory"),SmTagSubCategory.class);

        /*Request Edit Post*/
        mSmTagFeed = new Gson().fromJson(mEditPostIntent.getStringExtra("Feed"),Feed.class);

        /*init toolbar*/
        toolbar = (Toolbar)findViewById(R.id.toolbarViewMoreImageSelect);
        toolbar.setTitle("Edit");
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        /*get request intent*/
        mIntent = getIntent();
        mFileSelectStr.addAll(mIntent.getStringArrayListExtra("mFileSelectStr"));

        /*init recycler view*/
        rvMoreViewImageSelect = (RecyclerView)findViewById(R.id.rvMoreViewImageSelect);
        rvMoreViewImageSelect.setHasFixedSize(true);
        mManagerMoreViewImageSelect = new LinearLayoutManager(this);
        rvMoreViewImageSelect.setLayoutManager(mManagerMoreViewImageSelect);

        refreshAdapter();

    }

    private void refreshAdapter(){
        PostMoreImageSelectAdapter adapter = new PostMoreImageSelectAdapter(this,mFileSelectStr);
        rvMoreViewImageSelect.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_more_image_select,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode==REQUEST_GALLERY && resultCode==RESULT_OK && data!=null){
            ArrayList<String> results = data.getStringArrayListExtra("result");
            if (results.size() > 0){
                mFileSelectStr.addAll(results);
                refreshAdapter();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_more_select_ok:
                Intent mPostIntent = new Intent(getApplicationContext(),CreatePostActivity.class);
                mPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
                mPostIntent.putExtra("Feed",new Gson().toJson(mSmTagFeed));
                mPostIntent.putExtra("SmTagSubCategory",new Gson().toJson(smTagSubCategory));
                mPostIntent.putExtra("more_image_result",mFileSelectStr);
                setResult(RESULT_OK,mPostIntent);
                finish();
                break;
            case R.id.menu_item_more_add_more:
                Intent mGalleryIntent = new Intent(getApplicationContext(),Gallery.class);
                mGalleryIntent.putExtra(OPEN_MEDIA_TITLE,"Gallery");
                mGalleryIntent.putExtra(OPEN_MEDIA_MODE,1);
                mGalleryIntent.putExtra(OPEN_MEDIA_MAX,0);
                mGalleryIntent.putExtra("file_selected",mFileSelectStr);
                startActivityForResult(mGalleryIntent,REQUEST_GALLERY);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tagusnow.tagmejob.adapter.ViewNoticeMainSearchResultAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class ViewNoticeMainSearchResultActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ViewNoticeMainSearchResultActivity.class.getSimpleName();
    public static final String TITLE = "title";
    public static final String TYPE_TITLE = "type";
    public static final int SEE_ALL = 1;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private ViewNoticeMainSearchResultAdapter adapter;
    private Toolbar toolbar;
    private SmUser user;
    private String query;
    private int type = 0;

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            initResult();
            refreshLayout.setRefreshing(false);
        }
    };

    private void initResult(){
        this.adapter = new ViewNoticeMainSearchResultAdapter(this,this.user,this.query);
        recyclerView.setAdapter(this.adapter);
    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.type = getIntent().getIntExtra(TYPE_TITLE,0);
        this.query = getIntent().getStringExtra(TITLE);
    }

    private void initView(){
        initToolbar();
        initSwipe();
        initRecycler();
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        if (this.type==SEE_ALL){
            toolbar.setTitle("See all similar searches");
        }else {
            toolbar.setTitle(getString(R.string.see_all_results_for_s, this.query));
        }
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initSwipe(){
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            refreshLayout.setColorSchemeColors(getColor(R.color.colorAccent));
        }
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ViewNoticeMainSearchResultActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notice_main_search_result);
        initTemp();
        initView();
        refreshLayout.setRefreshing(true);
        onRefresh();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRun,2000);
    }
}

package com.tagusnow.tagmejob;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.SocketService;
import com.tagusnow.tagmejob.adapter.CommentAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.OkHttpClient;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewNotificationComment extends TagUsJobActivity implements View.OnClickListener,SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = ViewNotificationComment.class.getSimpleName();
    private final static int IMAGE_SELECT = 200;
    private Toolbar toolbar;
    private SwipeRefreshLayout refresh;
    private RelativeLayout edit_comment_image_layout;
    private ImageView edit_comment_image;
    private ImageButton edit_comment_remove_image,camera,send;
    private EmojiconEditText edit_comment_box;
    private View edit_comment_border;
    private RelativeLayout rlTriggerComment;
    private TextView trigger_msg;
    private SmUser authUser;
    private SmTagSubCategory category;
    private RecyclerView recycler_comment_body;
    private CommentAdapter mCommentAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private CommentResponse commentResponse;
    int lastPage = 0;
    boolean isRequest = true;
    int feed_id = 0;
    File mFileSelected;
    Socket socket;
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private Retrofit socketRetrofit = new Retrofit.Builder().baseUrl(SuperConstance.SOCKET_IO_).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private CommentService service = retrofit.create(CommentService.class);
    private CommentService socketService = socketRetrofit.create(CommentService.class);
    private FeedService feedService = retrofit.create(FeedService.class);
    private SocketService _SocketService = socketRetrofit.create(SocketService.class);

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("New Comment "+feed_id,mNewComment);
        socket.on("Count Comment "+feed_id,mCountComment);
        socket.on("User Comment "+feed_id,mUserComment);
        socket.on("Check Commenting "+feed_id,mKeyType);
        socket.connect();
    }

    private void initNewComment(Comment comment){
//        int to = commentResponse.getTo() + 1;
//        int from = commentResponse.getFrom() + 1;
//        int total = commentResponse.getTotal() + 1;
//        ArrayList<Comment> mComments = commentResponse.getData();
//        commentResponse.setTo(to);
//        commentResponse.setFrom(from);
//        commentResponse.setTotal(total);
//        mComments.add(comment);
//        commentResponse.setData(mComments);
//        mCommentAdapter.update(this,this.commentResponse,this.authUser).notifyDataSetChanged();
//        recycler_comment_body.scrollToPosition(to);
//        onScroll();
    }

    private Emitter.Listener mKeyType = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,obj.toString());
                        if (obj.has("isType") && !obj.getBoolean("isType")){
                            Log.w(TAG,"isType");
                            hideUserComment();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mNewComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj!=null){
                            Comment comment = new Gson().fromJson(obj.toString(),Comment.class);
                            initNewComment(comment);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,obj.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mUserComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj.has("message") && obj.getInt("feed_id")==feed_id && obj.getInt("user_id")!=authUser.getId()){
                            displayUserComment(obj.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void onReqCommenting(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(SuperConstance.SOCKET_IO + "/CheckCommenting/" + feed_id + "?isType=false", new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                Log.w(TAG,rawJsonResponse);
                hideUserComment();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
                hideUserComment();
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void displayUserComment(String message){
        rlTriggerComment.setVisibility(View.VISIBLE);
        trigger_msg.setText(message);
    }

    private void hideUserComment(){
        rlTriggerComment.setVisibility(View.GONE);
    }

    private void requestTriggerComment(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        if (authUser!=null){
            params.put("auth_user",authUser.getId());
        }
        params.put("feed_id",feed_id);
        client.post(SuperUtil.getBaseUrl("api/v1/trigger/comment"), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                Log.w(TAG,rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notification_comment);
        initView();
        initTemp();
        initSocket();
        responseComment();
        onScroll();
        viewCondition();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    private void initTemp(){
        authUser = new Auth(this).checkAuth().token();
        category = new Repository(this).restore().getCategory();
        feed_id = getIntent().getIntExtra("feed_id",0);
    }

    private void initRecycler(){
        recycler_comment_body = (RecyclerView) findViewById(R.id.recycler_comment_body);
        recycler_comment_body.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recycler_comment_body.setLayoutManager(mLayoutManager);
    }

    private void initView(){
        initToolbar();
        initRecycler();
        rlTriggerComment = (RelativeLayout)findViewById(R.id.rlTriggerComment);
        trigger_msg = (TextView)findViewById(R.id.trigger_msg);
        rlTriggerComment.setVisibility(View.GONE);
        edit_comment_image_layout = (RelativeLayout)findViewById(R.id.edit_comment_image_layout);
        edit_comment_image = (ImageView)findViewById(R.id.edit_comment_image);
        edit_comment_remove_image = (ImageButton)findViewById(R.id.edit_comment_remove_image);
        camera = (ImageButton)findViewById(R.id.camera);
        send = (ImageButton)findViewById(R.id.send);
        edit_comment_box = (EmojiconEditText)findViewById(R.id.edit_comment_box);
        edit_comment_border = (View)findViewById(R.id.edit_comment_border);
        refresh = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refresh.setOnRefreshListener(this);
        refresh.setColorScheme(android.R.color.holo_blue_bright,android.R.color.holo_green_light,android.R.color.holo_orange_light,android.R.color.holo_red_light);
        edit_comment_remove_image.setOnClickListener(this);
        camera.setOnClickListener(this);
        send.setOnClickListener(this);
        edit_comment_box.setUseSystemDefault(true);
        edit_comment_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                conditionCommentBox();
                hideUserComment();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                conditionCommentBox();
                requestTriggerComment();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                conditionCommentBox();
                onReqCommenting();
                hideUserComment();
            }
        });
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void onScroll(){
        /*recycler view event*/
        recycler_comment_body.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (commentResponse.getTo() - 1)) && (commentResponse.getTo() < commentResponse.getTotal())) {
                        if (commentResponse.getNext_page_url()!=null || !commentResponse.getNext_page_url().equals("")){
                            if (lastPage!=commentResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = commentResponse.getCurrent_page();
                                    nextComment(commentResponse.getNext_page_url());
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void nextComment(String next_page_url) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("feed_id",feed_id);
        client.post(next_page_url, params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                commentResponse = new Gson().fromJson(rawJsonResponse,CommentResponse.class);
                next(commentResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void next(CommentResponse commentResponse){
//        mCommentAdapter.update(this,this.commentResponse,this.authUser).notifyDataSetChanged();
    }

    private void responseComment(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("feed_id",feed_id);
        client.post(SuperUtil.getBaseUrl(SuperConstance.GET_COMMENT), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                commentResponse = new Gson().fromJson(rawJsonResponse,CommentResponse.class);
                initComment();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void initComment() {
//        mCommentAdapter = new CommentAdapter(this,this.commentResponse,this.authUser);
//        recycler_comment_body.setAdapter(mCommentAdapter);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initTemp();
        viewCondition();
        onScroll();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTemp();
        viewCondition();
        onScroll();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initTemp();
        viewCondition();
        onScroll();
    }

    private void viewCondition(){
        edit_comment_image_layout.setVisibility(View.GONE);
        edit_comment_border.setVisibility(View.GONE);
        send.setVisibility(View.GONE);
        conditionCommentBox();
    }

    private void clearForm(){
        mFileSelected = null;
        edit_comment_box.getText().clear();
        viewCondition();
    }

    private void conditionCommentBox(){
        if (!edit_comment_box.getText().toString().equals("")){
            send.setVisibility(View.VISIBLE);
            if (mFileSelected!=null){
                edit_comment_image_layout.setVisibility(View.VISIBLE);
                edit_comment_border.setVisibility(View.VISIBLE);
            }else{
                edit_comment_image_layout.setVisibility(View.GONE);
                edit_comment_border.setVisibility(View.GONE);
            }
            if (edit_comment_box.getText().length() > 30){
                edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.MORE_TEXT);
            }else{
                edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
            }
        }else {
            if (mFileSelected!=null){
                edit_comment_image_layout.setVisibility(View.VISIBLE);
                edit_comment_border.setVisibility(View.VISIBLE);
                send.setVisibility(View.VISIBLE);
            }else {
                edit_comment_image_layout.setVisibility(View.GONE);
                edit_comment_border.setVisibility(View.GONE);
                send.setVisibility(View.GONE);
            }
            edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
        }
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ViewNotificationComment.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_post,menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void search(){
        startActivity(SearchActivity.createIntent(this).putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser)).putExtra("SmTagSubCategory",new Gson().toJson(category)));
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void camera(){
//        Intent mCameraIntent = new Intent(this,Gallery.class);
//        mCameraIntent.putExtra(OPEN_MEDIA_TITLE,"Gallery");
//        mCameraIntent.putExtra(OPEN_MEDIA_MODE,IMAGE_ONLY);
//        mCameraIntent.putExtra(OPEN_MEDIA_MAX,1);
//        startActivityForResult(mCameraIntent,IMAGE_SELECT);
        AndroidImagePicker.getInstance().pickSingle(this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                mFileSelected = new File(items.get(0).path);
                selectImage();
            }
        });
    }

    private void send(){
        showProgress();
        comment();
    }

    private void comment(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("feed_id",feed_id);
        params.put("auth_user",authUser.getId());
        params.put("des",edit_comment_box.getText());
        if (mFileSelected!=null){
            try {
                params.put("image",mFileSelected,"multipart/form-data","default.png");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        client.post(SuperUtil.getBaseUrl(SuperConstance.SAVE_COMMENT), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                Log.w(TAG,rawJsonResponse);
                clearForm();
                onRefresh();
                dismissProgress();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
                dismissProgress();
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void removeImage(){
        mFileSelected = null;
        viewCondition();
    }

    private void selectImage(){
        Glide.with(this)
                .load(mFileSelected)
                .error(R.color.colorBorder)
                .into(edit_comment_image);
        viewCondition();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==IMAGE_SELECT && resultCode==RESULT_OK && data!=null){
            ArrayList<String> selected = data.getStringArrayListExtra("result");
            mFileSelected = new File(selected.get(0));
            selectImage();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.btnSearchViewPost:
                search();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                responseComment();
                onScroll();
                new Task().execute();
            }
        }, 2000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_comment_remove_image:
                removeImage();
                break;
            case R.id.camera:
                camera();
                break;
            case R.id.send:
                send();
                break;
        }
    }

    private class Task extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            refresh.setRefreshing(false);
            super.onPostExecute(aVoid);
        }
    }
}

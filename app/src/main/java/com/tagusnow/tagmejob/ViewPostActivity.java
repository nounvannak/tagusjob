package com.tagusnow.tagmejob;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.adapter.ViewPostAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import cz.msebera.android.httpclient.Header;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewPostActivity extends TagUsJobActivity implements View.OnClickListener,SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = ViewPostActivity.class.getSimpleName();
    public static final String FEED = "Feed";
    private static final int REQ_PRIVACY = 8472;
    RecyclerView recyclerView;
    Feed feed;
    SmUser authUser;
    CommentResponse commentResponse;
    SmTagSubCategory smTagSubCategory;
    ViewPostAdapter adapter;
    Toolbar toolbar;
    ImageView icon_saved;
    SwipeRefreshLayout refreshViewPost;
    RelativeLayout ViewPostProfileBar;
    private RelativeLayout rlTriggerComment;
    private TextView trigger_msg;
    /*Block Comment View*/
    private RelativeLayout edit_comment_image_layout;
    private ImageView edit_comment_image;
    private ImageButton edit_comment_remove_image,camera,send;
    private EmojiconEditText edit_comment_box;
    private View edit_comment_border;
    private File mFileSelected;
    static final int OPEN_MEDIA_PICKER = 1;
    private final String OPEN_MEDIA_TITLE = "title";
    private final String OPEN_MEDIA_MODE = "mode";
    private final String OPEN_MEDIA_MAX = "maxSelection";
    private final static int IMAGE_ONLY = 2;
    private String title;
    private Uri mUri;
    int feed_id = 0;
    int limit = 10;
    Intent intent;
    int lastPage = 0;
    boolean isRequest = true;
    int position = -1;
    Socket socket;
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private Retrofit socketRetrofit = new Retrofit.Builder().baseUrl(SuperConstance.SOCKET_IO_).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private CommentService socketService = socketRetrofit.create(CommentService.class);
    private CommentService service = retrofit.create(CommentService.class);
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            refreshViewPost.setRefreshing(false);
            newData();
        }
    };
    private Callback<CommentResponse> mNewBack = new Callback<CommentResponse>() {
        @Override
        public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNew(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CommentResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<CommentResponse> mNextBack = new Callback<CommentResponse>() {
        @Override
        public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNext(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CommentResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Comment> mSaveBack = new Callback<Comment>() {
        @Override
        public void onResponse(Call<Comment> call, Response<Comment> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                clearForm();
                dismissProgress();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    dismissProgress();
                    clearForm();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Comment> call, Throwable t) {
            dismissProgress();
            clearForm();
            t.printStackTrace();
        }
    };

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("New Comment "+feed_id,mNewComment);
        socket.on("Count Comment "+feed_id,mCountComment);
        socket.on("User Comment "+feed_id,mUserComment);
        socket.on("Check Commenting "+feed_id,mKeyType);
        socket.connect();
    }

    private void initNewComment(Comment comment){
        int to = commentResponse.getTo() + 1;
        int from = commentResponse.getFrom() + 1;
        int total = commentResponse.getTotal() + 1;
        ArrayList<Comment> mComments = new ArrayList<>();
        commentResponse.setTo(to);
        commentResponse.setFrom(from);
        commentResponse.setTotal(total);
        mComments.add(comment);
        commentResponse.setData(mComments);
        this.onScroll();
        this.adapter.update(this,this.commentResponse,this.feed,this.authUser).notifyDataSetChanged();
        recyclerView.scrollToPosition(commentResponse.getTo() -1);
    }

    private Emitter.Listener mKeyType = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,obj.toString());
                        if (obj.has("isType") && !obj.getBoolean("isType")){
                            Log.w(TAG,"isType");
                            hideUserComment();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mNewComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj!=null){
                            Comment comment = new Gson().fromJson(obj.toString(),Comment.class);
                            initNewComment(comment);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,obj.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mUserComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj.has("message") && obj.getInt("feed_id")==feed_id && obj.getInt("user_id")!=authUser.getId()){
                            displayUserComment(obj.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Callback<Object> mCheckCommentingCallback = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                hideUserComment();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    hideUserComment();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Object> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void onReqCommenting(){
        socketService.checkCommenting(this.feed_id,false).enqueue(mCheckCommentingCallback);
    }

    private void requestTriggerComment(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        if (authUser!=null){
            params.put("auth_user",authUser.getId());
        }
        params.put("feed_id",feed_id);
        client.post(SuperUtil.getBaseUrl("api/v1/trigger/comment"), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                Log.w(TAG,rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void displayUserComment(String message){
        rlTriggerComment.setVisibility(View.VISIBLE);
        trigger_msg.setText(message);
    }

    private void hideUserComment(){
        rlTriggerComment.setVisibility(View.GONE);
    }

    private void newData(){
        service.getList(this.feed.getId(),this.limit).enqueue(mNewBack);
    }

    private void nextData(){
        int page = this.commentResponse.getCurrent_page() + 1;
        service.getList(this.feed.getId(),this.limit,page).enqueue(mNextBack);
    }

    private void initNew(CommentResponse commentResponse){
        commentResponse.setTo(commentResponse.getTo() + ViewPostAdapter.MORE_VIEW);
        this.commentResponse = commentResponse;
        this.onScroll();
        this.adapter = new ViewPostAdapter(this,this.commentResponse,this.feed,this.authUser);
        recyclerView.setAdapter(this.adapter);
    }

    private void initNext(CommentResponse commentResponse){
        this.commentResponse = commentResponse;
        this.onScroll();
        this.adapter.update(this,this.commentResponse,this.feed,this.authUser).notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);
        initTemp();
        initView();
        initSocket();
        refreshViewPost.setRefreshing(true);
        onRefresh();
    }

    private void initTemp(){
        /*get intent extra*/
        this.authUser = new Auth(this).checkAuth().token();
        this.feed = new Gson().fromJson(getIntent().getStringExtra(FEED),Feed.class);
        ShowAlert("Test Feed",getIntent().getStringExtra(FEED));
        this.smTagSubCategory = new Repository(this).restore().getCategory();
        if (getIntent().getStringExtra("searchTitle")!=null){
            this.title = getIntent().getStringExtra("searchTitle");
        }else {
            this.title = "Title";
        }
        if (getIntent().getIntExtra("position",-1) > -1){
            this.position = getIntent().getIntExtra("position",-1);
        }else {
            this.position = -1;
        }
        this.feed_id = this.feed.getId();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ViewPostActivity.class);
    }

    private void initToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
        if (this.title==null || this.title.equals("")){
            this.title = getString(R.string.view_post_detail_title,feed.getUser_post().getUser_name());
        }else {
            if (this.title.length() > 35){
                this.title = this.title.substring(0,35)+"...";
            }
        }
        toolbar.setTitle(this.title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initView(){
        initToolbar();
        rlTriggerComment = (RelativeLayout)findViewById(R.id.rlTriggerComment);
        trigger_msg = (TextView)findViewById(R.id.trigger_msg);
        rlTriggerComment.setVisibility(View.GONE);
        initSwipe();
        initRecycler();
        initCommentBox();
        viewCondition();
    }

    private void initRecycler(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initSwipe(){
        refreshViewPost = (SwipeRefreshLayout)findViewById(R.id.refresh);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            refreshViewPost.setColorSchemeColors(getColor(R.color.colorAccent));
        }
        refreshViewPost.setOnRefreshListener(this);
    }

    private void initCommentBox(){
        edit_comment_image_layout = (RelativeLayout)findViewById(R.id.edit_comment_image_layout);
        edit_comment_image = (ImageView)findViewById(R.id.edit_comment_image);
        edit_comment_remove_image = (ImageButton)findViewById(R.id.edit_comment_remove_image);
        camera = (ImageButton)findViewById(R.id.camera);
        send = (ImageButton)findViewById(R.id.send);
        edit_comment_box = (EmojiconEditText)findViewById(R.id.edit_comment_box);
        edit_comment_border = (View)findViewById(R.id.edit_comment_border);
        edit_comment_remove_image.setOnClickListener(this);
        camera.setOnClickListener(this);
        send.setOnClickListener(this);
        edit_comment_box.setUseSystemDefault(true);
        edit_comment_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                conditionCommentBox();
                onReqCommenting();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                conditionCommentBox();
                requestTriggerComment();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                conditionCommentBox();
                onReqCommenting();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_post,menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void search(){
        startActivity(SearchActivity.createIntent(this).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.btnSearchViewPost:
                search();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==OPEN_MEDIA_PICKER && resultCode==RESULT_OK && data!=null){
            ArrayList<String> selectionResult=data.getStringArrayListExtra("result");
            mUri = Uri.parse(selectionResult.get(0));
            mFileSelected = new File(selectionResult.get(0));
            selectImage();
        }else if (requestCode==REQ_PRIVACY && resultCode==RESULT_OK && data!=null){
            int privacy = data.getIntExtra(SuperConstance.PRIVACY,-1);
            feed.setPrivacy(privacy);
            adapter.notifyItemChanged(position,feed);
        }
    }

    private void onScroll(){
        /*recycler view event*/
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (commentResponse.getTo() - 1)) && (commentResponse.getTo() < commentResponse.getTotal())) {
                        if (commentResponse.getNext_page_url()!=null || !commentResponse.getNext_page_url().equals("")){
                            if (lastPage!=commentResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = commentResponse.getCurrent_page();
                                    nextData();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void send(){
        showProgress();
        comment();
    }

    private void comment(){
        RequestBody feed_id   = createPartFromString(String.valueOf(this.feed.getId()));
        RequestBody auth_user = createPartFromString(String.valueOf(this.authUser!=null ? this.authUser.getId() : 0));
        RequestBody text      = createPartFromString(edit_comment_box.getText().toString());
        if (this.mFileSelected!=null){
            MultipartBody.Part part = prepareFilePart("image",this.mFileSelected);
            service.save(auth_user,feed_id,text,part).enqueue(mSaveBack);
        }else {
            service.save(auth_user,feed_id,text).enqueue(mSaveBack);
        }

    }

    private void removeImage(){
        mFileSelected = null;
        mUri = null;
        viewCondition();
    }

    private void selectImage(){
        Glide.with(this)
                .load(mFileSelected)
                .error(R.color.colorBorder)
                .into(edit_comment_image);
        viewCondition();
    }

    private void viewCondition(){
        edit_comment_image_layout.setVisibility(View.GONE);
        edit_comment_border.setVisibility(View.GONE);
        send.setVisibility(View.GONE);
        conditionCommentBox();
    }

    private void clearForm(){
        mFileSelected = null;
        mUri = null;
        edit_comment_box.getText().clear();
        viewCondition();
    }

    private void conditionCommentBox(){
        if (!edit_comment_box.getText().toString().equals("")){
            send.setVisibility(View.VISIBLE);
            if (mFileSelected!=null){
                edit_comment_image_layout.setVisibility(View.VISIBLE);
                edit_comment_border.setVisibility(View.VISIBLE);
            }else{
                edit_comment_image_layout.setVisibility(View.GONE);
                edit_comment_border.setVisibility(View.GONE);
            }
            if (edit_comment_box.getText().length() > 30){
                edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.MORE_TEXT);
            }else{
                edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
            }
        }else {
            if (mFileSelected!=null){
                edit_comment_image_layout.setVisibility(View.VISIBLE);
                edit_comment_border.setVisibility(View.VISIBLE);
                send.setVisibility(View.VISIBLE);
            }else {
                edit_comment_image_layout.setVisibility(View.GONE);
                edit_comment_border.setVisibility(View.GONE);
                send.setVisibility(View.GONE);
            }
            edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void openGallery(){
//        Intent mCameraIntent = new Intent(getApplicationContext(),Gallery.class);
//        mCameraIntent.putExtra(OPEN_MEDIA_TITLE,"Gallery");
//        mCameraIntent.putExtra(OPEN_MEDIA_MODE,IMAGE_ONLY);
//        mCameraIntent.putExtra(OPEN_MEDIA_MAX,1);
//        startActivityForResult(mCameraIntent,OPEN_MEDIA_PICKER);
        AndroidImagePicker.getInstance().pickSingle(this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                mUri = Uri.parse(items.get(0).path);
                mFileSelected = new File(items.get(0).path);
                selectImage();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ViewPostCommentButton:
                edit_comment_box.setFocusable(true);
                break;
            case R.id.send:
                send();
                break;
            case R.id.camera:
                openGallery();
                break;
            case R.id.edit_comment_remove_image:
                removeImage();
                break;
        }
    }

    @Override
    public void onRefresh() {
       new Handler().postDelayed(mRun,2000);
    }
}

package com.tagusnow.tagmejob;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nineoldandroids.view.ViewHelper;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.CommentAdapter;
import com.tagusnow.tagmejob.adapter.FeedAdapter;
import com.tagusnow.tagmejob.adapter.ViewProfileUserAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.feed.Target;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.Profile.User.HeaderLayout;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewProfileActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private final static String TAG = ViewProfileActivity.class.getSimpleName();
    public static final String PROFILE_USER = "profileUser";
    private static final int MORE_VIEW = 1;
    private SmUser authUser,profileUser;
    private FeedResponse feedResponse;
    private ViewProfileUserAdapter feedAdapter;
    int lastPage = 0;
    boolean isRequest = true;
    /*RecyclerView*/
    private RecyclerView recyclerProfile;
    /*view*/
    private SwipeRefreshLayout refresh;
    private Toolbar profile_toolbar;
    private EditText profile_toolbar_search_box;
    private FloatingActionButton fabTop;
    /*Service*/
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);
    private Callback<FeedResponse> mNewCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> mNextCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        initTemp();
        initView();
        refresh.setRefreshing(true);
        onRefresh();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ViewProfileActivity.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initTemp();
        initViewData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initTemp();
        initViewData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTemp();
        initViewData();
    }

    private void onScroll(){
        /*recycler view event*/
        recyclerProfile.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    getNextFeed();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void getNewFeed(){
        int auth_user = this.authUser!=null ? this.authUser.getId() : 0;
        int profile_user = this.profileUser!=null ? this.profileUser.getId() : 0;
        int limit = 10;
        feedService.Profile(auth_user,profile_user,limit).enqueue(mNewCallback);
    }

    private void getNextFeed(){
        int auth_user = this.authUser!=null ? this.authUser.getId() : 0;
        int profile_user = this.profileUser!=null ? this.profileUser.getId() : 0;
        int limit = 10;
        int page = this.feedResponse.getCurrent_page() + 1;
        feedService.Profile(auth_user,profile_user,limit,page).enqueue(mNextCallback);
    }

    private void initNewFeed(FeedResponse feedResponse){
//        feedResponse.setTo(feedResponse.getTo() + MORE_VIEW);
//        this.feedResponse = feedResponse;
//        this.onScroll();
//        this.feedAdapter = new ViewProfileUserAdapter(this.feedResponse,this).user(profileUser);
//        recyclerProfile.setAdapter(feedAdapter);
    }

    private void initNextFeed(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.onScroll();
        this.feedAdapter.next(this.feedResponse).user(profileUser).notifyDataSetChanged();
    }

    private void initView(){
        initToolbar();
        initRecycler();
        refresh = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refresh.setOnRefreshListener(this);
        fabTop = (FloatingActionButton)findViewById(R.id.fabTop);
        fabTop.setOnClickListener(this);
        fabTop.hide();
    }

    private void initRecycler(){
        recyclerProfile = (RecyclerView) findViewById(R.id.recyclerProfile);
        recyclerProfile.setHasFixedSize(true);
        recyclerProfile.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager  mLayoutManager = new LinearLayoutManager(this);
        recyclerProfile.setLayoutManager(mLayoutManager);
        recyclerProfile.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy < 0 && fabTop.getVisibility() == View.VISIBLE) {
                    fabTop.hide();
                } else if (dy > 0 && fabTop.getVisibility() != View.VISIBLE) {
                    fabTop.show();
                }
            }
        });
    }

    private void initToolbar(){
        profile_toolbar = (Toolbar)findViewById(R.id.profile_toolbar);
        profile_toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back_white);
//        profile_toolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.colorPrimary)));
        setSupportActionBar(profile_toolbar);
        profile_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        profile_toolbar_search_box = (EditText)findViewById(R.id.profile_toolbar_search_box);
    }

    private void initTemp(){
        this.authUser = new Auth(this).checkAuth().token();
        this.profileUser = new Gson().fromJson(getIntent().getStringExtra(PROFILE_USER),SmUser.class);
    }

    private void initViewData(){
        profile_toolbar_search_box.setHint(getString(R.string.search_in_profile,profileUser.getFirs_name()));
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRun,2000);
    }

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            refresh.setRefreshing(false);
            getNewFeed();
        }
    };

    @Override
    public void onClick(View view) {
        if (view==fabTop){
            recyclerProfile.smoothScrollToPosition(0);
        }
    }
}

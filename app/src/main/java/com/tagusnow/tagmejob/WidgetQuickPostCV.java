package com.tagusnow.tagmejob;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.ImageSelectFaceAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedFindJob;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.fragment.TagUsJobFragment;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.ImageSelectFace;
import com.tagusnow.tagmejob.view.Post.LayoutCamera;
import com.tagusnow.tagmejob.view.Post.LayoutCameraHandler;
import com.tagusnow.tagmejob.view.Post.LayoutSelectImageHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class WidgetQuickPostCV extends TagUsJobFragment implements View.OnClickListener, LayoutCameraHandler, LayoutSelectImageHandler {

    private static final String TAG = WidgetQuickPostCV.class.getSimpleName();
    public static final int PERMISSIONS_REQUEST_CODE_CV = 451;
    public static final int PERMISSIONS_REQUEST_CODE_COVER_LETTER = 452;
    public static final int CV_PICKER_REQUEST_CODE = 453;
    public static final int COVER_LETTER_PICKER_REQUEST_CODE = 454;
    private Spinner spnCategory,spnCity;
    private EditText title,phone,email,salary,description,file_cv,file_cover_letter;
    private View error_position,error_title,error_phone,error_email,error_city,error_cv,error_cover;
    private SmUser user;
    private SmTagSubCategory category;
    private SmLocation location;
    private RecyclerView recyclerImg;
    private ImageSelectFaceAdapter adapter;
    private List<String> categoryList = new ArrayList<>();
    private List<String> cityList = new ArrayList<>();
    private List<String> images;
    private int category_id;
    private int city;
    private File cv,cover_letter;
    private List<File> photo = new ArrayList<>();
    private FeedFindJob feed;
    /*Service*/
    private JobsService service = ServiceGenerator.createService(JobsService.class);
    private Callback<FeedFindJob> mSave = new Callback<FeedFindJob>() {
        @Override
        public void onResponse(Call<FeedFindJob> call, Response<FeedFindJob> response) {
            if (response.isSuccessful()){
                feed = response.body();
            }else {
                try {
                    call.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedFindJob> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public WidgetQuickPostCV() {
        // Required empty public constructor
    }

    public static WidgetQuickPostCV newInstance(){
        return new WidgetQuickPostCV();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_widget_quick_post_cv, container, false);
        initTemp();
        initView(view);
        setupView();
        initUserInfo();
        return view;
    }

    private void initView(View view){
        initRecycler(view);
        initErrorView(view);
        spnCategory = (Spinner)view.findViewById(R.id.spnCategory);
        spnCity = (Spinner)view.findViewById(R.id.spnCity);
        title = (EditText)view.findViewById(R.id.title);
        phone = (EditText)view.findViewById(R.id.phone);
        email = (EditText)view.findViewById(R.id.email);
        salary = (EditText)view.findViewById(R.id.salary);
        file_cv = (EditText)view.findViewById(R.id.file_cv);
        file_cover_letter = (EditText)view.findViewById(R.id.file_cover_letter);
        description = (EditText)view.findViewById(R.id.description);
        file_cv.setOnClickListener(this);
        file_cover_letter.setOnClickListener(this);
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0){
                    category_id = category.getData().get(i - 1).getId();
                }else {
                    category_id = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                category_id = 0;
            }
        });
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0){
                    city = location.getData().get(i - 1).getId();
                }else {
                    city = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                city = 0;
            }
        });

        this.initImageSelect(new ArrayList<String>());
    }

    private void initTemp(){
        this.user = new Auth(getContext()).checkAuth().token();
        this.category = new Repository(getContext()).restore().getCategory();
        this.location = new Repository(getContext()).restore().getLocation();
        this.categoryList.add(0,"Choose Position");
        if (this.category.getData()!=null){
            for (int i=1;i <= this.category.getData().size();i++){
                this.categoryList.add(this.category.getData().get(i-1).getTitle());
            }
        }

        this.cityList.add(0,"Choose City/Province");
        if (this.location.getData()!=null){
            for (int i=1;i <= this.location.getData().size();i++){
                this.cityList.add(this.location.getData().get(i-1).getName());
            }
        }
    }

    private void initUserInfo(){
        if (this.user!=null){

            if (this.user.getEmail()!=null){
                email.setText(this.user.getEmail());
            }

            if (this.user.getPhone()!=null){
                phone.setText(this.user.getPhone());
            }
        }
    }

    private void initErrorView(View view){
        error_position = (View)view.findViewById(R.id.error_position);
        error_title = (View)view.findViewById(R.id.error_title);
        error_phone = (View)view.findViewById(R.id.error_phone);
        error_email = (View)view.findViewById(R.id.error_email);
        error_city = (View)view.findViewById(R.id.error_city);
        error_cv = (View)view.findViewById(R.id.error_cv);
        error_cover = (View)view.findViewById(R.id.error_cover);
    }

    private void initRecycler(View view){
        recyclerImg = (RecyclerView)view.findViewById(R.id.recyclerImg);
        recyclerImg.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false);
        recyclerImg.setLayoutManager(layoutManager);
    }

    private void setupView(){
        if (this.categoryList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),android.R.layout.simple_spinner_dropdown_item,this.categoryList);
            spnCategory.setAdapter(adapter);
        }

        if (this.cityList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),android.R.layout.simple_spinner_dropdown_item,this.cityList);
            spnCity.setAdapter(adapter);
        }
    }

    public boolean isValid(){
        boolean valid = true;
        if (this.category_id > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_position.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_position.setBackgroundColor(Color.RED);
            valid = false;
        }
        if (this.city > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_city.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_city.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*Title*/
        if (this.title.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_title.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_title.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*phone*/
        if (this.phone.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_phone.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_phone.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*email*/
        if (this.email.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_email.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_email.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (this.cv==null){
            ShowAlert("Form Invalid","Please select your CV.");
            valid = false;
        }

        return valid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CV_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            if (path != null) {
                cv = new File(path);
                file_cv.setText(cv.getName());
            }
        }else if (requestCode == COVER_LETTER_PICKER_REQUEST_CODE && resultCode == RESULT_OK){
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            if (path != null) {
                cover_letter = new File(path);
                file_cover_letter.setText(cover_letter.getName());
            }
        }
        else if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK){
            ArrayList<String> results = data.getStringArrayListExtra(Gallery.RESULT);
            if (results!=null){
                for(String file : results){
                    this.photo.add(new File(file));
                }
                initImageSelect(results);
            }
        }
    }

    public void initImageSelect(List<String> fileLists){
        this.images = fileLists;
        this.adapter = new ImageSelectFaceAdapter(getContext());
        this.adapter.setHandler(this);
        this.adapter.setLayoutSelectImageHandler(this);
        this.adapter.setFileLists(fileLists);
        recyclerImg.setAdapter(this.adapter);
    }

    private void checkPermissionsAndOpenFilePicker(int requestCode) {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        switch (requestCode){
            case CV_PICKER_REQUEST_CODE:
                if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                        showError();
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, PERMISSIONS_REQUEST_CODE_CV);
                    }
                } else {
                    openFilePicker(requestCode);
                }
                break;
            case COVER_LETTER_PICKER_REQUEST_CODE:
                if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                        showError();
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, PERMISSIONS_REQUEST_CODE_COVER_LETTER);
                    }
                } else {
                    openFilePicker(requestCode);
                }
                break;
        }
    }

    private void showError() {
        Toast.makeText(getContext(), "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE_CV:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker(CV_PICKER_REQUEST_CODE);
                } else {
                    showError();
                }
                break;
            case PERMISSIONS_REQUEST_CODE_COVER_LETTER:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker(COVER_LETTER_PICKER_REQUEST_CODE);
                } else {
                    showError();
                }
                break;
        }
    }

    private void openFilePicker(int requestCode) {
        new MaterialFilePicker()
                .withSupportFragment(this)
                .withRequestCode(requestCode)
                .withHiddenFiles(true)
                .withTitle("File Picker")
                .start();
    }

    public boolean isPost() {
        return this.feed!=null;
    }

    public WidgetQuickPostCV save() throws IOException {

        RequestBody user_id = createPartFromString(String.valueOf(user.getId()));
        RequestBody cat_id = createPartFromString(String.valueOf(category_id));
        RequestBody title = createPartFromString(this.title.getText().toString());
        RequestBody phone = createPartFromString(this.phone.getText().toString());
        RequestBody email = createPartFromString(this.email.getText().toString());
        RequestBody salary = null;
        RequestBody city_id = createPartFromString(String.valueOf(city));
        RequestBody desc = null;
        MultipartBody.Part cv = null,cover_letter = null;
        List<MultipartBody.Part> photos = new ArrayList<>();

        if (this.salary.getText().toString().length() > 0){
            salary = createPartFromString(this.salary.getText().toString());
        }else {
            salary = createPartFromString("");
        }

        if (this.description.getText().toString().length() > 0){
            desc = createPartFromString(this.description.getText().toString());
        }else {
            desc = createPartFromString("");
        }

        if (this.photo!=null){
            for (File file : this.photo){
                photos.add(prepareFilePart("photo[]",file));
            }
        }

        if (this.cv!=null){
            cv = prepareFilePart("cv",this.cv);
        }

        if (this.cover_letter!=null){
            cover_letter = prepareFilePart("cover_letter",this.cover_letter);
        }

        if (this.photo!=null && this.cv!=null && this.cover_letter!=null){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            service.PostCV(user_id,cat_id,title,phone,email,salary,city_id,desc,photos,cv,cover_letter).execute().body();
        }else {
            if (this.photo!=null){
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                service.PostCV(user_id,cat_id,title,phone,email,salary,city_id,desc,photos,cv).execute().body();
            }else if (this.cover_letter!=null){
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                service.PostCV(user_id,cat_id,title,phone,email,salary,city_id,desc,cv,cover_letter).execute().body();
            }
        }

//        service.PostCV(user_id,cat_id,title,phone,email,salary,city_id,desc,photos,cv,cover_letter).enqueue(mSave);
        return this;
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void openGallery(){
        AndroidImagePicker.getInstance().pickMulti(getActivity(), true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                if (items!=null){
                    ArrayList<String> list = new ArrayList<>();
                    for(ImageItem file : items){
                        photo.add(new File(file.path));
                        list.add(file.path);
                    }
                    initImageSelect(list);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view==file_cv){
            checkPermissionsAndOpenFilePicker(CV_PICKER_REQUEST_CODE);
        }else if (view==file_cover_letter){
            checkPermissionsAndOpenFilePicker(COVER_LETTER_PICKER_REQUEST_CODE);
        }
    }

    @Override
    public void openCamera(LayoutCamera camera, int requestCode) {
        openGallery();
    }

    @Override
    public void remove(ImageSelectFace imageSelectFace, RecyclerView.Adapter adapter, int position) {
        this.photo.remove(position);
        this.images.remove(position);
        ((ImageSelectFaceAdapter) adapter).setFileLists(this.images);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void selected(List<ImageItem> items) {

    }
}

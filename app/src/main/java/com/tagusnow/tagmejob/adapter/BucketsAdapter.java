package com.tagusnow.tagmejob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import java.util.List;

public class BucketsAdapter extends RecyclerView.Adapter<BucketsAdapter.MyViewHolder>{
    private List<String> bucketNames,bitmapList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;
        public RelativeLayout btnCaptureComment;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail=(ImageView) view.findViewById(R.id.image);
            btnCaptureComment=(RelativeLayout)view.findViewById(R.id.btnCaptureComment);
        }
    }

    public BucketsAdapter(List<String> bucketNames,List<String> bitmapList,Context context) {
        this.bucketNames=bucketNames;
        this.bitmapList = bitmapList;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        bucketNames.get(position);
        holder.title.setText(bucketNames.get(position));
        Glide.with(this.context).load("file://"+bitmapList.get(position)).override(300,300).centerCrop().into(holder.thumbnail);
        holder.btnCaptureComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("btnCaptureComment","clicked");
            }
        });
    }

    @Override
    public int getItemCount() {
        return bucketNames.size();
    }
}

package com.tagusnow.tagmejob.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.view.CategoryLayout;

import java.util.ArrayList;

public class CategoryAdapter extends BaseAdapter {

    ArrayList<Category> category;
    Context context;

    public CategoryAdapter(Context context,ArrayList<Category> category){
        this.context = context;
        this.category = category;
    }

    @Override
    public int getCount() {
        return category.size();
    }

    @Override
    public Object getItem(int i) {
        return category.get(i);
    }

    @Override
    public long getItemId(int i) {
        return category.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null)
        {
            convertView = new CategoryLayout(context);
        }
        CategoryLayout categoryLayout = (CategoryLayout) convertView;
        Category mCategory = (Category) getItem(i);
        categoryLayout.setImage(mCategory.getImage());
        categoryLayout.setTitle(mCategory.getTitle());
        categoryLayout.setCounter(mCategory.getJob_counter());
        return categoryLayout;
    }
}

package com.tagusnow.tagmejob.adapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.feed.Category;
import java.util.ArrayList;
import java.util.List;

public class CategoryLoader extends ArrayAdapter<Category> implements Filterable{
    private Context context;
    private List<Category> data,filterList;
    private CategoryFilter filter;

    public CategoryLoader(Context context,List<Category> data){
        super(context,-1,data);
        this.context = context;
        this.data = data;
        filterList = new ArrayList<>();
        this.filterList.addAll(data);
    }
    public CategoryLoader(@NonNull Context context, int resource, @NonNull List<Category> objects) {
        super(context, resource, objects);
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Nullable
    @Override
    public Category getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.data.get(position).getId();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View rowView = LayoutInflater.from(context).inflate(R.layout.category_list_view,parent,false);
        ImageView category_check = (ImageView)rowView.findViewById(R.id.category_check);
        ImageView category_icon = (ImageView)rowView.findViewById(R.id.category_icon);
        TextView category_title = (TextView)rowView.findViewById(R.id.category_title);

        category_title.setText(this.data.get(position).getTitle());
        category_check.setTag(this.data.get(position).getId());
        if (this.data.get(position).getImage()==null || this.data.get(position).getImage().equals("")){
            category_icon.setImageResource(R.drawable.ic_tag_white);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                category_icon.setImageTintList(ColorStateList.valueOf(context.getColor(R.color.colorAccent)));
            }
        }else{
            Glide.with(this.context).load(SuperUtil.getCategoryPath(this.data.get(position).getImage())).fitCenter().override(100,100).into(category_icon);
        }

        return rowView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new CategoryFilter();
        return filter;
    }

    private class CategoryFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String data = charSequence.toString().toLowerCase();
            FilterResults results = new FilterResults();
            if (data.length() > 0){
                ArrayList<Category> filtered = new ArrayList<>(filterList);
                ArrayList<Category> nList = new ArrayList<>();
                int count = filtered.size();
                for (int i=0;i < count;i++){
                    Category item = filtered.get(i);
                    String title = item.getTitle().toLowerCase();
                    if (title.startsWith(data)){
                        nList.add(item);
                    }
                }
                results.count = nList.size();
                results.values = nList;
            }else {
                ArrayList<Category> list = new ArrayList<>(filterList);
                results.count = list.size();
                results.values = list;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            data = (ArrayList<Category>)filterResults.values;
            clear();
            for (int i=0;i <data.size();i++){
                Category data_ = (Category)data.get(i);
                add(data_);
                notifyDataSetChanged();
            }
        }
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.Chat.Holder.ChatUserUIHolder;
import com.tagusnow.tagmejob.view.Chat.UI.ChatUserUI;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Space;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter {

    private static final int CHAT = 1;
    private static final int SPACE = 2;
    private static final int LOADING = 3;
    private Activity activity;
    private Context context;
    private UserPaginate paginate;
    private List<SmUser> userList;
    private SmUser user;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public ChatAdapter(Activity activity,UserPaginate paginate,SmUser user,LoadingListener<LoadingView> loadingListener){
        this.activity = activity;
        this.context = activity;
        this.paginate = paginate;
        this.userList = paginate.getData();
        this.user = user;
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public ChatAdapter Update(UserPaginate paginate){
        this.paginate = paginate;
        this.userList.addAll(paginate.getData());
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if (position==(this.getItemCount() - 1)){
            if (this.paginate!=null){
                if (this.paginate.getTo()==this.paginate.getTotal()){
                    type = SPACE;
                }else {
                    type = LOADING;
                }
            }
        }else {
            type = CHAT;
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==CHAT){
            return new ChatUserUIHolder(new ChatUserUI(this.context));
        }else if (viewType==LOADING){
            return new LoadingHolder(loadingView);
        }else {
            return new SpaceHolder(new Space(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==CHAT){
            ((ChatUserUIHolder) holder).bindView(this.activity,this.user,this.userList.get(position),this);
        }else if (getItemViewType(position)==LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else {
            ((SpaceHolder) holder).bindView();
        }
    }

    @Override
    public int getItemCount() {
        return this.paginate.getTo() + 1;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.view.Chat.Holder.ChatFileHolder;
import com.tagusnow.tagmejob.view.Chat.UI.ChatFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ChatFileAdapter extends RecyclerView.Adapter {

    public static final int FILE = 1;
    public static final int IMAGE = 2;
    private Activity activity;
    private Context context;
    private int Res[] = {};
    private List<File> Res_ = new ArrayList<>();
    private int type;

    public ChatFileAdapter(Activity activity,int Res[]){
        this.activity = activity;
        this.context = activity;
        this.Res = Res;
        this.type = FILE;
    }

    public ChatFileAdapter(Activity activity,List<File> Res_){
        this.activity = activity;
        this.context = activity;
        this.Res_.addAll(Res_);
        this.type = IMAGE;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (this.type==FILE){
            viewType = FILE;
        }else {
            viewType = IMAGE;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChatFileHolder(new ChatFile(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==IMAGE){
            ((ChatFileHolder) holder).bindView(this.activity,this.Res_.get(position),this);
        }else {
            ((ChatFileHolder) holder).bindView(this.activity,this.Res[position],this);
        }
    }

    @Override
    public int getItemCount() {
        return this.Res.length + this.Res_.size();
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.model.v2.feed.ConversationPagination;
import com.tagusnow.tagmejob.view.Chat.Holder.MyMessageHolder;
import com.tagusnow.tagmejob.view.Chat.Holder.TheirMessageHolder;
import com.tagusnow.tagmejob.view.Chat.UI.MyMessage;
import com.tagusnow.tagmejob.view.Chat.UI.TheirMessage;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.Space;

import java.util.Collections;
import java.util.List;

public class ChatRoomAdapter extends RecyclerView.Adapter {

    private static final int MY_MESSAGE = 1;
    private static final int THEIR_MESSAGE = 2;
    private static final int SPACE = 3;
    private Activity activity;
    private Context context;
    private ConversationPagination pagination;
    private List<Conversation> conversations;
    private SmUser user1,user2;

    public ChatRoomAdapter(Activity activity,ConversationPagination pagination,SmUser user1,SmUser user2){
        this.activity = activity;
        this.context = activity;
        this.pagination = pagination;
        this.conversations = pagination.getData();
        Collections.reverse(this.conversations);
        this.user1 = user1;
        this.user2 = user2;
    }

    public ChatRoomAdapter Update(ConversationPagination pagination){
        this.pagination = pagination;
        List<Conversation> tempCon = this.conversations;
        List<Conversation> newCon = pagination.getData();
        Collections.reverse(tempCon);
        Collections.reverse(newCon);
        tempCon.addAll(newCon);
        this.conversations = tempCon;
        Collections.reverse(this.conversations);
        return this;
    }

    public void AppendItem(Conversation conversation){
        this.conversations.add(conversation);
        notifyItemRangeInserted(getItemCount(),this.conversations.size());
        notifyDataSetChanged();
    }

    public void RemoveItem(int position){
        this.conversations.remove(position);
        notifyDataSetChanged();
    }

    public void UpdateItem(Conversation conversation,int position){
        this.conversations.add(position,conversation);
        notifyItemChanged(position,conversation);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if (this.pagination!=null){
            if (position==(getItemCount() - 1)){
                type = SPACE;
            }else {
                if (this.conversations.get(position).getUser_id()==user1.getId()){
                    type = MY_MESSAGE;
                }else {
                    type = THEIR_MESSAGE;
                }
            }
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==THEIR_MESSAGE){
            return new TheirMessageHolder(new TheirMessage(this.context));
        }else if (viewType==SPACE){
            return new SpaceHolder(new Space(this.context));
        }else {
            return new MyMessageHolder(new MyMessage(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==THEIR_MESSAGE){
            ((TheirMessageHolder) holder).bindView(this.activity,this.user1,this.conversations.get(position));
        }else if (getItemViewType(position)==SPACE){
            ((SpaceHolder) holder).bindView();
        }else {
            ((MyMessageHolder) holder).bindView(this.activity,this.user1,this.conversations.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return this.conversations.size() + 1;
    }
}

package com.tagusnow.tagmejob.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;

import java.util.ArrayList;
import java.util.List;

public class CityAdapter extends ArrayAdapter<SmLocation.Data> implements Filterable {

    Context context;
    SmLocation smLocation;
    ArrayList<SmLocation.Data> list,filterList;
    CityFilter cityFilter;

    public CityAdapter(Context context,ArrayList<SmLocation.Data> list){
        super(context,-1,list);
        this.context = context;
        this.list = list;
        this.filterList = new ArrayList<>();
        this.filterList.addAll(this.list);
    }

    public CityAdapter(@NonNull Context context, int resource, @NonNull ArrayList<SmLocation.Data> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.category_list_view,parent,false);
        ImageView category_check = (ImageView)rowView.findViewById(R.id.category_check);
        ImageView category_icon = (ImageView)rowView.findViewById(R.id.category_icon);
        TextView category_title = (TextView)rowView.findViewById(R.id.category_title);

        category_title.setText(this.list.get(position).getName());
        category_check.setTag(this.list.get(position).getId());
        category_icon.setVisibility(View.GONE);

        return rowView;
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Nullable
    @Override
    public SmLocation.Data getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.list.get(position).getId();
    }

    @NonNull
    @Override
    public Filter getFilter() {
        if (cityFilter == null)
            cityFilter = new CityFilter();
        return cityFilter;
    }

    private class CityFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String data = charSequence.toString().toLowerCase();
            FilterResults results = new FilterResults();
            if (data.length() > 0){
                ArrayList<SmLocation.Data> filtered = new ArrayList<>(filterList);
                ArrayList<SmLocation.Data> nList = new ArrayList<>();
                int count = filtered.size();
                for (int i=0;i < count;i++){
                    SmLocation.Data item = filtered.get(i);
                    String title = item.getName().toLowerCase();
                    if (title.startsWith(data)){
                        nList.add(item);
                    }
                }
                results.count = nList.size();
                results.values = nList;
            }else {
                ArrayList<SmLocation.Data> list = new ArrayList<>(filterList);
                results.count = list.size();
                results.values = list;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            list = (ArrayList<SmLocation.Data>)filterResults.values;
            clear();
            for (int i=0;i <list.size();i++){
                SmLocation.Data data_ = (SmLocation.Data)list.get(i);
                add(data_);
                notifyDataSetChanged();
            }
        }
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}

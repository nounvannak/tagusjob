package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.v3.comment.CommentCell;
import com.tagusnow.tagmejob.v3.comment.CommentCellHolder;
import com.tagusnow.tagmejob.v3.comment.CommentListener;
import com.tagusnow.tagmejob.v3.empty.EmptyHolder;
import com.tagusnow.tagmejob.v3.empty.EmptyView;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.detail.DetailFeedCV;
import com.tagusnow.tagmejob.v3.feed.detail.DetailFeedCVHolder;
import com.tagusnow.tagmejob.v3.feed.detail.DetailFeedJob;
import com.tagusnow.tagmejob.v3.feed.detail.DetailFeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.detail.DetailFeedNormal;
import com.tagusnow.tagmejob.v3.feed.detail.DetailFeedNormalHolder;
import com.tagusnow.tagmejob.v3.feed.detail.FeedListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.view.CommentFace;
import com.tagusnow.tagmejob.view.Empty.EmptyComment;
import com.tagusnow.tagmejob.view.Holder.CommentFaceHolder;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptyCommentHolder;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Space;

import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter{

    private static final int COMMENT = 1;
    private static final int LOADING = 2;
    private static final int EMPTY = 3;
    private Activity activity;
    private SmUser user;
    private List<Comment> comments;
    private CommentListener<CommentCell> commentListener;
    private CommentListener.OnOptionSelected<CommentCell> onOptionSelected;
    private int other = 1;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public CommentAdapter(Activity activity, SmUser user, CommentListener<CommentCell> commentListener, CommentListener.OnOptionSelected<CommentCell> onOptionSelected,LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.user = user;
        this.commentListener = commentListener;
        this.onOptionSelected = onOptionSelected;
        this.loadingListener = loadingListener;
        this.loadingView = new LoadingView(activity);
    }

    public void NewData(List<Comment> comments){
        this.comments = comments;
    }

    public void NextData(List<Comment> comments){
        this.comments.addAll(comments);
        notifyDataSetChanged();
    }

    public void Insert(Comment comment){
        this.comments.add(comment);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (comments.size() > 0){
            if (position == (getItemCount() - 1)){
                return LOADING;
            }else{
                return COMMENT;
            }
        }else {
            return EMPTY;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == COMMENT){
            return new CommentCellHolder(new CommentCell(activity));
        }else if (viewType == LOADING) {
            return new LoadingHolder(loadingView);
        }else {
            return new EmptyHolder(new EmptyView(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == COMMENT){
            ((CommentCellHolder) holder).BindView(activity,user,comments.get(position - (other - 1)),commentListener,onOptionSelected);
        }else if (getItemViewType(position) == LOADING) {
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else {
            ((EmptyHolder) holder).BindView(activity);
        }
    }

    @Override
    public int getItemCount() {
        return comments.size() > 0 ? (comments.size() + other) : other;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.empty.EmptyHolder;
import com.tagusnow.tagmejob.v3.empty.EmptyView;
import com.tagusnow.tagmejob.v3.user.UserCardTwo;
import com.tagusnow.tagmejob.v3.user.UserCardTwoHolder;
import com.tagusnow.tagmejob.v3.user.UserCardTwoListener;

import java.util.List;

public class ConnectSuggestFaceAdapter extends RecyclerView.Adapter{

    public ConnectSuggestFaceAdapter(Activity activity, List<SmUser> users, UserCardTwoListener userCardTwoListener) {
        this.activity = activity;
        this.users = users;
        this.userCardTwoListener = userCardTwoListener;
    }

    public void NextData(List<SmUser> users){
        this.users.addAll(users);
    }

    private static final int USER = 0;
    private static final int LOADING = 2;
    private Activity activity;
    private List<SmUser> users;
    private UserCardTwoListener userCardTwoListener;
    private int other = 1;

    @Override
    public int getItemViewType(int position) {
        if (position == (getItemCount() - 1)){
            return LOADING;
        }else {
            return USER;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == USER){
            return new UserCardTwoHolder(new UserCardTwo(activity));
        }else{
            return new EmptyHolder(new EmptyView(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == USER){
            ((UserCardTwoHolder) holder).BindView(activity,users.get(position),userCardTwoListener);
        }else {
            ((EmptyHolder) holder).BindView(activity);
        }
    }

    @Override
    public int getItemCount() {
        return users.size() + other;
    }
}

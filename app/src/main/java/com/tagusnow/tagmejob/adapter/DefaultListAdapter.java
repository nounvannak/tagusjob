package com.tagusnow.tagmejob.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.model.v2.feed.ImageItem;

import java.util.List;

public class DefaultListAdapter extends ArrayAdapter<ImageItem> implements ImageFeedAdapter {

    private Context context;

    private final LayoutInflater layoutInflater;

    public DefaultListAdapter(Context context, List<ImageItem> items) {
        super(context, 0, items);
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public DefaultListAdapter(Context context) {
        super(context, 0);
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override public int getViewTypeCount() {
        return 2;
    }

    @Override public int getItemViewType(int position) {
        return position % 2 == 0 ? 1 : 0;
    }

    @Override
    public void appendItems(List<ImageItem> newItems) {
        addAll(newItems);
        notifyDataSetChanged();
    }

    @Override
    public void setItems(List<ImageItem> moreItems) {
        clear();
        appendItems(moreItems);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v;
        ImageItem item = getItem(position);
        if (convertView == null) {
            v = layoutInflater.inflate(R.layout.layout_image_adapter, parent, false);
        } else {
            v = convertView;
        }
        Glide.with(this.context).load(item.getPath()).centerCrop().error(R.drawable.ic_error).into((ImageView) v.findViewById(R.id.image));

        return v;
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.tagusnow.tagmejob.model.v2.feed.Education;
import com.tagusnow.tagmejob.view.List.EducationFace;

import java.util.List;

public class EducationList extends ArrayAdapter<Education> {

    private static final String TAG = EducationList.class.getSimpleName();
    private Activity activity;
    private Context context;
    private List<Education> educations;
    private int resource;
    private int textViewResourceId;

    @Nullable
    @Override
    public Education getItem(int position) {
        return this.educations.get(position);
    }

    @Override
    public int getCount() {
        return this.educations.size();
    }

    public EducationList(@NonNull Context context, int resource) {
        super(context, resource);
        this.resource = resource;
        this.context = context;
    }

    public EducationList(@NonNull Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
    }

    public EducationList(@NonNull Context context, int resource, @NonNull Education[] objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    public EducationList(@NonNull Context context, int resource, int textViewResourceId, @NonNull Education[] objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
    }

    public EducationList(@NonNull Context context, int resource, @NonNull List<Education> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.context = context;
        this.educations = objects;
    }

    public EducationList(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<Education> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.educations = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return new EducationFace(this.context).setEducation(this.educations.get(position));
    }
}

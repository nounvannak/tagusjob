package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.tagusnow.tagmejob.model.v2.feed.Experience;
import com.tagusnow.tagmejob.view.List.ExperienceFace;

import java.util.List;

public class ExperienceList extends ArrayAdapter<Experience> {

    private static final String TAG = ExperienceList.class.getSimpleName();
    private Activity activity;
    private Context context;
    private List<Experience> experiences;
    private int resource;
    private int textViewResourceId;

    @Nullable
    @Override
    public Experience getItem(int position) {
        return this.experiences.get(position);
    }

    @Override
    public int getCount() {
        return this.experiences.size();
    }

    public ExperienceList(@NonNull Context context, int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    public ExperienceList(@NonNull Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
        this.context = context;
        this.resource = resource;
    }

    public ExperienceList(@NonNull Context context, int resource, @NonNull Experience[] objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    public ExperienceList(@NonNull Context context, int resource, int textViewResourceId, @NonNull Experience[] objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
    }

    public ExperienceList(@NonNull Context context, int resource, @NonNull List<Experience> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.experiences = objects;
    }

    public ExperienceList(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<Experience> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.experiences = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return new ExperienceFace(this.context).setExperience(this.experiences.get(position));
    }
}

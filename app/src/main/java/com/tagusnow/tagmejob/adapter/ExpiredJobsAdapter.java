package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.view.BaseCard.CardSmall;
import com.tagusnow.tagmejob.view.BaseCard.ExpiredJobsFace;
import com.tagusnow.tagmejob.view.Empty.EmptyJob;
import com.tagusnow.tagmejob.view.Holder.CardSmallHolder;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptyJobHolder;
import com.tagusnow.tagmejob.view.Holder.ExpiredJobsHolder;

import java.util.List;

public class ExpiredJobsAdapter extends RecyclerView.Adapter{

    public static final int MORE_VIEW = 0;
    private static final int CARD = 0;
    private static final int EMPTY = 1;
    private Activity activity;
    private Context context;
    private FeedResponse feedResponse;
    private List<Feed> feedList;
    private SmUser user;

    public ExpiredJobsAdapter(Activity activity,FeedResponse feedResponse,SmUser user){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feedList = feedResponse.getData();
        this.user = user;
    }

    public ExpiredJobsAdapter update(Activity activity,FeedResponse feedResponse,SmUser user){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feedList.addAll(feedResponse.getData());
        this.user = user;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (this.feedResponse.getTotal() > 0){
            viewType = CARD;
        }else{
            viewType = EMPTY;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==CARD) {
            return new CardSmallHolder(new CardSmall(this.context));
        }else {
            return new EmptyJobHolder(new EmptyJob(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==CARD) {
            ((CardSmallHolder) holder).bindView(this.activity, this,this.feedList.get(position),this.user);
        }else {
            ((EmptyJobHolder) holder).bindView("No expire job");
        }
    }

    @Override
    public int getItemCount() {
        return this.feedResponse.getTotal() > 0 ? this.feedResponse.getTo() : 1;
    }
}

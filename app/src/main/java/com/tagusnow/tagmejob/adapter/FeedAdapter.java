package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.BaseCard.Card;
import com.tagusnow.tagmejob.view.BaseCard.CardCV;
import com.tagusnow.tagmejob.view.BaseCard.CardJob;
import com.tagusnow.tagmejob.view.Holder.CardCVHolder;
import com.tagusnow.tagmejob.view.Holder.CardHolder;
import com.tagusnow.tagmejob.view.Holder.CardJobHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.FollowSuggestHorizontalHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.WidgetThreePostHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestHorizontalLayout;
import com.tagusnow.tagmejob.view.Widget.WidgetThreePost;
import java.util.ArrayList;
import java.util.List;

public class FeedAdapter extends RecyclerView.Adapter{

    private Context context;
    private Activity activity;
    private SmUser authUser;
    private FeedResponse feedResponse;
    private SmTagSubCategory category;
    private ArrayList<Feed> feeds;
    private List<SmUser> listFollowSuggest = new ArrayList<SmUser>();
    private final static int REQ_PRIVACY = 3729;
    private static final int SUGGEST = 1;
    private static final int CARD = 2;
    private static final int JOB = 4;
    private static final int CV = 3;
    private static final int LOADING = 10;
    private static final int POST_WIDGET = 0;
    private static final int MORE_VIEW = 2;
    private final static String TAG = FeedAdapter.class.getSimpleName();
    private static int random_index = 0;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public FeedAdapter(Activity activity,FeedResponse feedResponse,SmUser authUser,LoadingListener<LoadingView> loadingListener){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feeds = feedResponse.getData();
        this.authUser = authUser;
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public FeedAdapter update(Activity activity,FeedResponse feedResponse,SmUser authUser){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feeds.addAll(feedResponse.getData());
        this.authUser = authUser;
        return this;
    }

    public FeedAdapter listFollowSuggest(List<SmUser> listFollowSuggest){
        this.listFollowSuggest.addAll(listFollowSuggest);
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = CARD;
        if (position==1){
            viewType = SUGGEST;
        }else if (position==0) {
            viewType = POST_WIDGET;
        }else if (position==(this.getItemCount() - 1)){
            viewType = LOADING;
        }else {
            if (this.feeds.get(position - MORE_VIEW).getFeed().getFeed_type().equals(FeedDetail.JOB_POST)){
                viewType = JOB;
            }else if (this.feeds.get(position - MORE_VIEW).getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)){
                viewType = CARD;
            }else if (this.feeds.get(position - MORE_VIEW).getFeed().getFeed_type().equals(FeedDetail.STAFF_POST)){
                viewType = CV;
            }
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==CARD) {
            return new CardHolder(new Card(this.context));
        }else if (viewType==JOB){
            return new CardJobHolder(new CardJob(this.context));
        }else if (viewType==CV) {
            return new CardCVHolder(new CardCV(this.context));
        }else if (viewType==LOADING){
            return new LoadingHolder(loadingView);
        }else if (viewType==POST_WIDGET){
            return new WidgetThreePostHolder(new WidgetThreePost(this.context));
        }else{
            return new FollowSuggestHorizontalHolder(new FollowSuggestHorizontalLayout(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==CARD) {
            ((CardHolder) holder).Adapter(this).bindView(this.activity, this.authUser, this.feeds.get(position - MORE_VIEW));
        }else if (getItemViewType(position)==JOB){
            if (this.random_index == 3) {
                this.random_index = 0;
            } else {
                this.random_index++;
            }
            ((CardJobHolder) holder).bindView(this.activity,this.feeds.get(position - MORE_VIEW),this,this.authUser);
        }else if(getItemViewType(position)==CV) {
            ((CardCVHolder) holder).bindView(this.activity, this.feeds.get(position - MORE_VIEW), this, this.authUser);
        }else if (getItemViewType(position)==LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else if(getItemViewType(position)==POST_WIDGET){
//            ((PostWidgetHolder) holder).setActivity(activity).bindView(authUser);
            ((WidgetThreePostHolder) holder).bindView(this.activity,this.authUser);
        }else{
            ((FollowSuggestHorizontalHolder) holder).bindView(this.activity,this.authUser);
        }
    }

    @Override
    public int getItemCount() {
        return this.feedResponse.getTo() + 1;
    }

    public int getRandom_index() {
        return random_index;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

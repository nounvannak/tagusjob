package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.Holder.Widget.FollowSuggestHolder;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestFace;

import java.util.List;

public class FollowSuggestHorizontalAdapter extends RecyclerView.Adapter{

    private Activity activity;
    private Context context;
    private UserPaginate userPaginate;

    public void setUserList(List<SmUser> userList) {
        this.userList = userList;
    }

    private List<SmUser> userList;
    private SmUser auth;

    public FollowSuggestHorizontalAdapter(Activity activity,UserPaginate userPaginate,SmUser auth){
        this.activity = activity;
        this.context = activity;
        this.userPaginate = userPaginate;
        this.userList = userPaginate.getData();
        this.auth = auth;
    }

    public FollowSuggestHorizontalAdapter update(Activity activity,UserPaginate userPaginate,SmUser auth){
        this.activity = activity;
        this.context = activity;
        this.userPaginate = userPaginate;
        this.userList.addAll(userPaginate.getData());
        this.auth = auth;
        return this;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FollowSuggestHolder(new FollowSuggestFace(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((FollowSuggestHolder) holder).bindView(this.activity,this.auth,this.userList.get(position),this);
    }

    @Override
    public int getItemCount() {
        return this.userPaginate.getTo();
    }
}

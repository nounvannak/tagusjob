package com.tagusnow.tagmejob.adapter;

import android.widget.ListAdapter;

import com.tagusnow.tagmejob.model.v2.feed.Photo;

import java.util.List;

public interface IUserPostImageAdapter extends ListAdapter{

    void appendItems(List<Photo> newPhotos);

    void setItems(List<Photo> morePhotos);
}

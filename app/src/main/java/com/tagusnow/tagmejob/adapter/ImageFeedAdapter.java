package com.tagusnow.tagmejob.adapter;

import android.widget.ListAdapter;

import com.tagusnow.tagmejob.model.v2.feed.ImageItem;

import java.util.List;

public interface ImageFeedAdapter extends ListAdapter {
    void appendItems(List<ImageItem> newItems);

    void setItems(List<ImageItem> moreItems);
}

package com.tagusnow.tagmejob.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.view.Holder.ImageSelectFaceHolder;
import com.tagusnow.tagmejob.view.ImageSelectFace;
import com.tagusnow.tagmejob.view.Post.LayoutCamera;
import com.tagusnow.tagmejob.view.Post.LayoutCameraHandler;
import com.tagusnow.tagmejob.view.Post.LayoutCameraHolder;
import com.tagusnow.tagmejob.view.Post.LayoutSelectImageHandler;

import java.util.List;

public class ImageSelectFaceAdapter extends RecyclerView.Adapter{

    public void setFileLists(List<String> fileLists) {
        this.fileLists = fileLists;
    }

    private List<String> fileLists;
    private Context context;

    public void setHandler(LayoutCameraHandler handler) {
        this.handler = handler;
    }

    private LayoutCameraHandler handler;

    public void setLayoutSelectImageHandler(LayoutSelectImageHandler layoutSelectImageHandler) {
        this.layoutSelectImageHandler = layoutSelectImageHandler;
    }

    private LayoutSelectImageHandler layoutSelectImageHandler;

    public ImageSelectFaceAdapter(Context context){
        this.context = context;
    }
    @Override
    public int getItemViewType(int position) {
        int type_ = 0;
        Log.e("compare", String.valueOf(position == getItemCount() - 1));
        if (position == getItemCount() - 1){
            type_ = 1;
        }
        return type_;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==1){
            return new LayoutCameraHolder(new LayoutCamera(this.context));
        }else {
            return new ImageSelectFaceHolder(new ImageSelectFace(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==1){
            ((LayoutCameraHolder) holder).bindView(this.handler);
        }else {
            ((ImageSelectFaceHolder) holder).bindView(this.layoutSelectImageHandler,this,this.fileLists.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return this.fileLists.size() + 1;
    }
}

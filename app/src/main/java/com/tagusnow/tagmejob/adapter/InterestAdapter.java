package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserInterest;
import com.tagusnow.tagmejob.view.Setting.ui.InterestJobUI;
import java.util.List;

public class InterestAdapter extends RecyclerView.Adapter<InterestAdapter.InterestHolder> {

    private List<UserInterest> interests;
    private SmUser user;
    private Activity activity;
    private Context context;

    public InterestAdapter(Activity activity,List<UserInterest> interests,SmUser user){
        this.activity = activity;
        this.context = activity;
        this.interests = interests;
        this.user = user;
    }

    @NonNull
    @Override
    public InterestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new InterestHolder(new InterestJobUI(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull InterestHolder holder, int position) {
        holder.bindView(this.activity,this.interests.get(position),this.user);
    }

    @Override
    public long getItemId(int position) {
        return (long)this.interests.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return this.interests.size();
    }

    class InterestHolder extends RecyclerView.ViewHolder{

        private InterestJobUI interestJobUI;

        InterestHolder(View itemView) {
            super(itemView);
            this.interestJobUI = (InterestJobUI)itemView;
        }

        public void bindView(Activity activity,UserInterest interest,SmUser user){
            this.interestJobUI.setActivity(activity);
            this.interestJobUI.setUser(user);
            this.interestJobUI.setInterest(interest);
        }
    }
}

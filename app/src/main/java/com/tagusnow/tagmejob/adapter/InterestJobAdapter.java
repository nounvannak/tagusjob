package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.Holder.InterestJobHolder;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.Widget.InterestJobFace;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.Space;

import java.util.ArrayList;

public class InterestJobAdapter extends RecyclerView.Adapter {

    private static final String TAG = InterestJobAdapter.class.getSimpleName();
    private static final int OTHER = 0;
    private static final int JOB = 1;
    private Activity activity;
    private Context context;
    public ArrayList<Boolean> listChecked = new ArrayList<>();
    private ArrayList<Category> InterestJob = new ArrayList<>();
    private ArrayList<Category> categories;

    public InterestJobAdapter(Activity activity,ArrayList<Category> categories){
        this.activity = activity;
        this.context = activity;
        this.categories = categories;
        for (int i=0;i <40;i++){
            this.listChecked.add(false);
        }
    }

    public InterestJobAdapter update(ArrayList<Category> categories){
        this.categories = categories;
        for (int i=0;i <40;i++){
            this.listChecked.add(false);
        }
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        if (position==(this.categories.size())){
            return OTHER;
        }else {
            return JOB;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==JOB){
            return new InterestJobHolder(new InterestJobFace(this.context));
        }else {
            return new SpaceHolder(new Space(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (getItemViewType(position)==JOB){
                ((InterestJobHolder) holder).bindView(this.activity,this.categories.get(position)).Adapter(this,position);
            }else {
//            ((InterestJobHolder) holder).Adapter(this).onOther(this.activity);
                ((SpaceHolder) holder).bindView();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.categories.size() + 1;
    }

    public ArrayList<Category> getInterestJob() {
        return InterestJob;
    }

    public void setInterestJob(ArrayList<Category> interestJob) {
        InterestJob = interestJob;
    }
}

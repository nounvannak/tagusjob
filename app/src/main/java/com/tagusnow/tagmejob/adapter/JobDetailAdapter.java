package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.Empty.EmptyJob;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptyJobHolder;
import com.tagusnow.tagmejob.view.Holder.JobDetailHolder;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.JobDetailView;
import com.tagusnow.tagmejob.view.Space;

public class JobDetailAdapter extends RecyclerView.Adapter{

    private static final String TAG = JobDetailAdapter.class.getSimpleName();
    private static final int EMPTY = 0;
    private static final int JOB_DETAIL = 1;
    private Context context;
    private Activity activity;
    private Feed feed;

    public JobDetailAdapter(Activity activity,Feed feed){
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.feed = feed;
    }

    @Override
    public int getItemViewType(int position) {
        int type;
        if (position==0){
            type = JOB_DETAIL;
        }else {
            type = EMPTY;
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==JOB_DETAIL){
            return new JobDetailHolder(new JobDetailView(this.context));
        }else {
            return new SpaceHolder(new Space(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==JOB_DETAIL){
            ((JobDetailHolder) holder).bindView(this.activity,this.feed);
        }else {
            ((SpaceHolder) holder).bindView();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}

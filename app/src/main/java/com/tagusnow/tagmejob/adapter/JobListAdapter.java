package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.v3.category.SubcribeListener;
import com.tagusnow.tagmejob.view.Subcribe.SubcribeLayout;
import com.tagusnow.tagmejob.view.Subcribe.SubscribeHolder;
import java.util.List;

public class JobListAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<Category> categories;
    private SubcribeListener<SubcribeLayout> subcribeListener;

    public JobListAdapter(Activity activity, List<Category> categories, SubcribeListener<SubcribeLayout> subcribeListener) {
        this.activity = activity;
        this.categories = categories;
        this.subcribeListener = subcribeListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubscribeHolder(new SubcribeLayout(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((SubscribeHolder) holder).BindView(activity,categories.get(position),subcribeListener);
    }

    @Override
    public int getItemCount() {
        return this.categories.size();
    }
}

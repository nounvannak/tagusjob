package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.V2.Company.CompanyRecentPostLayout;
import com.tagusnow.tagmejob.V2.Company.CompanyRecentPostLayoutHolder;
import com.tagusnow.tagmejob.V2.Company.CompanyRecentPostListener;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedJob;
import com.tagusnow.tagmejob.v3.feed.FeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.FeedJobListener;
import com.tagusnow.tagmejob.v3.page.job.PostFunctionTwoCell;
import com.tagusnow.tagmejob.v3.page.job.PostFunctionTwoCellHolder;
import com.tagusnow.tagmejob.v3.page.job.PostFunctionTwoCellListener;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;

import java.util.List;

public class JobsAdapter extends RecyclerView.Adapter{

    private Activity activity;
    private Context context;

    private static final int POST_CELL = 0;
    private static final int COMPANY_SUGGEST = 1;
    private static final int JOB = 2;
    private static final int LOADING = 3;
    private int other = 3;

    private List<Feed> feeds;
    private CompanyRecentPostListener companyRecentPostListener;
    private PostFunctionTwoCellListener postFunctionTwoCellListener;

    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public void setCompanyRecentPostListener(CompanyRecentPostListener companyRecentPostListener) {
        this.companyRecentPostListener = companyRecentPostListener;
    }

    public void setPostFunctionTwoCellListener(PostFunctionTwoCellListener postFunctionTwoCellListener) {
        this.postFunctionTwoCellListener = postFunctionTwoCellListener;
    }

    public void setFeedJobListener(FeedJobListener feedJobListener) {
        this.feedJobListener = feedJobListener;
    }

    private FeedJobListener feedJobListener;

    public JobsAdapter(Activity activity, LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.context = activity;
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public void NewData(List<Feed> feeds){
        this.feeds = feeds;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (position == 0){
            viewType = POST_CELL;
        }else if (position == 1){
            viewType = COMPANY_SUGGEST;
        }else if (position == (getItemCount() - 1)){
            viewType = LOADING;
        }else {
            viewType = JOB;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == POST_CELL){
            return new PostFunctionTwoCellHolder(new PostFunctionTwoCell(context));
        }else if (viewType == COMPANY_SUGGEST){
            return new CompanyRecentPostLayoutHolder(new CompanyRecentPostLayout(context));
        }else if (viewType == LOADING){
            return new LoadingHolder(loadingView);
        }else{
            return new FeedJobHolder(new FeedJob(context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == POST_CELL){
            ((PostFunctionTwoCellHolder) holder).BindView(activity,postFunctionTwoCellListener);
        }else if (getItemViewType(position) == COMPANY_SUGGEST){
            ((CompanyRecentPostLayoutHolder) holder).bindView(activity,companyRecentPostListener);
        }else if (getItemViewType(position) == LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else {
            ((FeedJobHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedJobListener);
        }
    }

    @Override
    public int getItemCount() {
        return feeds.size() + other;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

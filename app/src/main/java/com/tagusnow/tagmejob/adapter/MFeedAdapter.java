package com.tagusnow.tagmejob.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.Controller.FeedController;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewDetailPostActivity;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FacebookFeed;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.feed.TagusnowFeed;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import io.rmiri.skeleton.SkeletonGroup;
import ru.whalemare.sheetmenu.SheetMenu;

public  class MFeedAdapter extends RecyclerView.Adapter<MFeedAdapter.ViewHolder> {
    private ArrayList<ViewGroup> mDataset;
    private ArrayList<SmTagFeed.Feed.Data> mFeed;
    private int rowLayout;
    private Context context;
    private SmUser authUser;
    private ArrayList<SmUser> feedUser;
    private ArrayList<SmTagFeed.Comment> comment;
    private ArrayList<SmTagFeed.Like> like;
    private FeedController feedController;
    private List<FacebookFeed> facebookFeeds;
    private List<TagusnowFeed> tagusnowFeeds;
    private ArrayList<FeedHolder> feedHolder;
    private Intent intent;

    public MFeedAdapter(FeedController feedController, int rowLayout, Context context){
        this.feedController = feedController;
        this.rowLayout = rowLayout;
        this.context = context;
        this.feedHolder = new ArrayList<>();
        this.feedUser =  feedController.getTag_feed().getUser();
        this.authUser = feedController.getTag_feed().getAuth_user();
        this.facebookFeeds = feedController.getTag_feed().getFacebook_feed();
        this.tagusnowFeeds = feedController.getTag_feed().getTagusnow_feed();
        this.mFeed = feedController.getTag_feed().getFeed().getData();
        this.comment = feedController.getTag_feed().getComments();
        this.like = feedController.getTag_feed().getLikes();
        try {
            deploy(this.mFeed);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public RecyclerView.Adapter setIntent(Intent intent){
        this.intent = intent;
        return this;
    }

    private void deploy(ArrayList<SmTagFeed.Feed.Data> mFeed){
        for (int i=0;i<mFeed.size();i++){
            if (mFeed.get(i).getType().equals(SuperConstance.PLATFORM_FACEBOOK)){
                SmUser feedUser = this.feedUser.get(i);
                SmUser authUser = this.authUser;
                SmTagFeed.Comment comment = this.comment.get(i);
                SmTagFeed.Like like = this.like.get(i);
                TagusnowFeed tagusnowFeed = new TagusnowFeed();
                FacebookFeed facebookFeed = this.facebookFeeds.get(i);
                FeedHolder feedHolder = new FeedHolder(feedUser,authUser,mFeed.get(i),comment,like,facebookFeed,tagusnowFeed);
                this.feedHolder.add(feedHolder);
            }else if (mFeed.get(i).getType().equals(SuperConstance.TAGUSNOW_POST)){
                SmUser feedUser = this.feedUser.get(i);
                SmUser authUser = this.authUser;
                SmTagFeed.Comment comment = this.comment.get(i);
                SmTagFeed.Like like = this.like.get(i);
                TagusnowFeed tagusnowFeed = this.tagusnowFeeds.get(i);
                FacebookFeed facebookFeed = new FacebookFeed();
                FeedHolder feedHolder = new FeedHolder(feedUser,authUser,mFeed.get(i),comment,like,facebookFeed,tagusnowFeed);
                this.feedHolder.add(feedHolder);
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(v);

    }

    public MFeedAdapter(ArrayList<ViewGroup> mDataset){
        this.mDataset = mDataset;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            /*holder.skeletonGroup.setAutoPlay(true);
            holder.skeletonFeedProfile.setAutoPlay(true);
            holder.skeletonFeedText.setAutoPlay(true);
            holder.skeletonGroupProfileName.setAutoPlay(true);*/
            holder.setBaseIntent(intent);
//            holder.setContext(context);
            holder.bindFeed(this.feedHolder.get(position),this.mFeed.get(position).getType());
            /*holder.skeletonGroup.finishAnimation();
            holder.skeletonFeedProfile.finishAnimation();
            holder.skeletonFeedText.finishAnimation();
            holder.skeletonGroupProfileName.finishAnimation();*/
            /*SkeletonConfig skeletonConfig = new SkeletonConfig();
            holder.feedCard.setPreventCornerOverlap(false);
            skeletonConfig.setSkeletonIsOn(true);
            holder.skeletonGroup.setPosition(position);
            if (skeletonConfig.isSkeletonIsOn()) {
                holder.skeletonGroup.setAutoPlay(true);
                holder.skeletonFeedProfile.setAutoPlay(true);
                holder.skeletonFeedText.setAutoPlay(true);
                holder.skeletonGroupProfileName.setAutoPlay(true);
                return;
            } else {
                holder.skeletonGroup.setAutoPlay(false);
                holder.skeletonFeedProfile.setAutoPlay(false);
                holder.skeletonFeedText.setAutoPlay(false);
                holder.skeletonGroupProfileName.setAutoPlay(false);
                holder.skeletonGroup.finishAnimation();
                holder.skeletonFeedProfile.finishAnimation();
                holder.skeletonFeedText.finishAnimation();
                holder.skeletonGroupProfileName.finishAnimation();
            }*/
//            holder.bindFeed(this.feedHolder,this.mFeed,position);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mFeed.size();
    }

    public FeedController getFeedController() {
        return feedController;
    }

    public void setFeedController(FeedController feedController) {
        this.feedController = feedController;
    }

    public List<FacebookFeed> getFacebookFeeds() {
        return facebookFeeds;
    }

    public void setFacebookFeeds(ArrayList<FacebookFeed> facebookFeeds) {
        this.facebookFeeds = facebookFeeds;
    }

    public List<TagusnowFeed> getTagusnowFeeds() {
        return tagusnowFeeds;
    }

    public void setTagusnowFeeds(ArrayList<TagusnowFeed> tagusnowFeeds) {
        this.tagusnowFeeds = tagusnowFeeds;
    }

    public SmUser getSmUser() {
        return authUser;
    }

    public void setSmUser(SmUser smUser) {
        this.authUser = smUser;
    }

    public void setFeedHolder(ArrayList<FeedHolder> feedHolder) {
        this.feedHolder = feedHolder;
    }

    public ArrayList<FeedHolder> getFeedHolder(){
        return feedHolder;
    }

    class FeedHolder{
        private SmUser smUser,authUser;
        private SmTagFeed.Comment comment;
        private SmTagFeed.Like like;
        private FacebookFeed facebookFeed;
        private TagusnowFeed tagusnowFeed;
        private SmTagFeed.Feed.Data mFeed;

        public SmUser getSmUser() {
            return smUser;
        }

        public void setSmUser(SmUser smUser) {
            this.smUser = smUser;
        }

        public SmTagFeed.Comment getComment() {
            return comment;
        }

        public void setComment(SmTagFeed.Comment comment) {
            this.comment = comment;
        }

        public SmTagFeed.Like getLike() {
            return like;
        }

        public void setLike(SmTagFeed.Like like) {
            this.like = like;
        }

        FeedHolder(SmUser smUser, SmUser authUser, SmTagFeed.Feed.Data mFeed, SmTagFeed.Comment comment, SmTagFeed.Like like, FacebookFeed facebookFeed, TagusnowFeed tagusnowFeed){
            this.smUser = smUser;
            this.comment = comment;
            this.authUser = authUser;
            this.like = like;
            this.mFeed = mFeed;
            this.facebookFeed = facebookFeed;
            this.tagusnowFeed = tagusnowFeed;
        }

        public FeedHolder(){
            super();
        }

         public FacebookFeed getFacebookFeed() {
             return facebookFeed;
         }

         public void setFacebookFeed(FacebookFeed facebookFeed) {
             this.facebookFeed = facebookFeed;
         }

         public TagusnowFeed getTagusnowFeed() {
             return tagusnowFeed;
         }

         public void setTagusnowFeed(TagusnowFeed tagusnowFeed) {
             this.tagusnowFeed = tagusnowFeed;
         }

        public void setAuthUser(SmUser authUser) {
            this.authUser = authUser;
        }

        public SmUser getAuthUser() {
            return this.authUser;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ViewGroup mView;
        CardView feedCard;
        CircleImageView mProfilePicture;
        TextView mFeedStoryTitle,mFeedStoryTime,mLikeText,mLikeCounter,mCommentText,mCommentCounter,albumMoreThan4_text;
        ImageView mFeedPicture,mLikeIcon,mCommentIcon,album2_1,album2_2,album3_1,album3_2,album3_3,album4_1,album4_2,album4_3,album4_4,albumMoreThan4_1,albumMoreThan4_2,albumMoreThan4_3,albumMoreThan4_4;
        LinearLayout mFeedLikeButton,mFeedCommentButton,album2,album3,album4,albumMoreThan4;
        LinearLayout feed_text_box;
        RelativeLayout profileBar;
        Button mButtonMoreFeed;
        HtmlTextView mFeedTitle;
        SkeletonGroup skeletonGroup,skeletonFeedProfile,skeletonGroupProfileName,skeletonFeedText;
        private Context context;
        private Intent intent;

        public void setBaseIntent(Intent intent){
            this.intent = intent;
        }

        public void setContext(Context context){this.context = context;}

        ViewHolder(View view) {
            super(view);
            feedCard = (CardView)view.findViewById(R.id.feedCard);
            mProfilePicture = (CircleImageView)view.findViewById(R.id.mProfilePicture);
            mFeedStoryTitle = (TextView) view.findViewById(R.id.mFeedStoryTitle);
            mFeedStoryTime = (TextView) view.findViewById(R.id.mFeedStoryTime);
            mButtonMoreFeed = (Button) view.findViewById(R.id.mButtonMoreFeed);
            mFeedPicture = (ImageView) view.findViewById(R.id.mFeedPicture);
            mFeedTitle = (HtmlTextView) view.findViewById(R.id.mFeedTitle);
            feed_text_box = (LinearLayout)view.findViewById(R.id.feed_text_box);
            profileBar = (RelativeLayout) view.findViewById(R.id.profileBar);
            /*Skeleton View*/
//            skeletonGroup = (SkeletonGroup) view.findViewById(R.id.skeletonGroup);
//            skeletonFeedProfile = (SkeletonGroup) view.findViewById(R.id.skeletonFeedProfile);
//            skeletonGroupProfileName = (SkeletonGroup) view.findViewById(R.id.skeletonGroupProfileName);
//            skeletonFeedText = (SkeletonGroup) view.findViewById(R.id.skeletonFeedText);

            /*Album More 4*/
            albumMoreThan4_text = (TextView)view.findViewById(R.id.albumMoreThan4_text);
            albumMoreThan4 = (LinearLayout)view.findViewById(R.id.albumMoreThan4);
            albumMoreThan4_1 = (ImageView)view.findViewById(R.id.albumMoreThan4_1);
            albumMoreThan4_2 = (ImageView)view.findViewById(R.id.albumMoreThan4_2);
            albumMoreThan4_3 = (ImageView)view.findViewById(R.id.albumMoreThan4_3);
            albumMoreThan4_4 = (ImageView)view.findViewById(R.id.albumMoreThan4_4);
            /*Album 4*/
            album4 = (LinearLayout)view.findViewById(R.id.album4);
            album4_1 = (ImageView)view.findViewById(R.id.album4_1);
            album4_2 = (ImageView)view.findViewById(R.id.album4_2);
            album4_3 = (ImageView)view.findViewById(R.id.album4_3);
            album4_4 = (ImageView)view.findViewById(R.id.album4_4);
            /*Album 3*/
            album3 = (LinearLayout)view.findViewById(R.id.album3);
            album3_1 = (ImageView)view.findViewById(R.id.album3_1);
            album3_2 = (ImageView)view.findViewById(R.id.album3_2);
            album3_3 = (ImageView)view.findViewById(R.id.album3_3);
            /*Album 2*/
            album2 = (LinearLayout)view.findViewById(R.id.album2);
            album2_1 = (ImageView)view.findViewById(R.id.album2_1);
            album2_2 = (ImageView)view.findViewById(R.id.album2_2);

            //*Like Button*//
            mFeedLikeButton = (LinearLayout) view.findViewById(R.id.mFeedLikeButton);
            mLikeIcon = (ImageView)view.findViewById(R.id.likeIcon);
            mLikeText = (TextView)view.findViewById(R.id.likeText);
            mLikeCounter = (TextView)view.findViewById(R.id.likeCounter);
            //*Like Button*//
            mFeedCommentButton = (LinearLayout) view.findViewById(R.id.mFeedCommentButton);
            mCommentIcon = (ImageView)view.findViewById(R.id.commentIcon);
            mCommentText = (TextView)view.findViewById(R.id.commentText);
            mCommentCounter = (TextView)view.findViewById(R.id.commentCounter);

            mView = (ViewGroup) view;
            this.context = view.getContext();
        }

        private void hidePost(){
            getFeedHolder().remove(getAdapterPosition());
            notifyItemRemoved(getAdapterPosition());
        }

        private void removePost(){
            getFeedHolder().remove(getAdapterPosition());
            notifyItemRemoved(getAdapterPosition());
        }

        @TargetApi(Build.VERSION_CODES.M)
        private void bindFeed(ArrayList<FeedHolder> feedHolder, ArrayList<SmTagFeed.Feed.Data> mFeed, int position){
            for (int i=0;i < feedHolder.size();i++){
                if (position==(i-1)){
                    Log.d("next page","ok");
                }

                FacebookFeed facebookFeed = feedHolder.get(i).getFacebookFeed();
                TagusnowFeed tagusnowFeed = feedHolder.get(i).getTagusnowFeed();
                SmUser feedUser = feedHolder.get(i).getSmUser();
                SmTagFeed.Comment comments = feedHolder.get(i).getComment();
                SmTagFeed.Like likes = feedHolder.get(i).getLike();
                if (mFeed.get(i).getType().equals(SuperConstance.PLATFORM_FACEBOOK)){
                    if (facebookFeed.getCount_likes() > 0){
                        mLikeCounter.setVisibility(View.VISIBLE);
                        mLikeCounter.setText(facebookFeed.getCount_likes());
                        if (facebookFeed.getUser_id().equals(feedUser.getProfile_id())){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                                mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                                mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                            }
                        }else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                                mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                                mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                            }
                        }
                    }else{
                        mLikeCounter.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                            mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                            mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                        }
                    }

                    if (facebookFeed.getCount_comments() > 0){
                        mCommentCounter.setVisibility(View.VISIBLE);
                        mCommentCounter.setText(facebookFeed.getCount_comments());
                        if (facebookFeed.getUser_id().equals(feedUser.getProfile_id())){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mCommentCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                                mCommentText.setTextColor(context.getColor(R.color.colorIconSelect));
                                mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                            }
                        }else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                                mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                                mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                            }
                        }
                    }else{
                        mCommentCounter.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                            mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                            mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                        }
                    }

                    //profile image
                    SuperUtil.setImageResource(mProfilePicture,facebookFeed.getProfile_image());
                    //feed story title
                    mFeedStoryTitle.setText(facebookFeed.getStory());
                    //feed story time
                    mFeedStoryTime.setText(facebookFeed.getTimeline());
                    //feed picture
                    if (facebookFeed.getHashtag_image()!=null){
                        mFeedPicture.setVisibility(View.VISIBLE);
                        SuperUtil.setImageResource(mFeedPicture,facebookFeed.getHashtag_image());
                    }else{
                        mFeedPicture.setVisibility(View.GONE);
                    }
                    //feed title
                    mFeedTitle.setHtml(facebookFeed.getHashtag_text());
                }else{
                    if (likes.getCount_likes() > 0){
                        mLikeCounter.setVisibility(View.VISIBLE);
                        mLikeCounter.setText(String.valueOf(likes.getCount_likes()));
                        if (likes.isIs_like()){
                            mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                            mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                            mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                        }else {
                            mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                            mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                            mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                        }
                    }else{
                        mLikeCounter.setVisibility(View.GONE);
                        mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                        mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                        mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                    }

                    if (comments.getCount_comments() > 0){
                        mCommentCounter.setVisibility(View.VISIBLE);
                        mCommentCounter.setText(String.valueOf(comments.getCount_comments()));
                        mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                        mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                        mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                    }else{
                        mCommentCounter.setVisibility(View.GONE);
                        mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                        mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                        mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                    }

                    //profile image
                    SuperUtil.setImageResource(mProfilePicture,feedUser.getImage());
                    //feed story title
                    mFeedStoryTitle.setText(feedUser.getUser_name());
                    //feed story time
                    mFeedStoryTime.setText(tagusnowFeed.getTimeline());
                    //feed picture
                    Toast.makeText(context,tagusnowFeed.getAlbum().size()+"",Toast.LENGTH_LONG).show();
                    if (tagusnowFeed.getAlbum().size() > 0 && !tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc().equals("")){
                        mFeedPicture.setVisibility(View.VISIBLE);
                        SuperUtil.setImageResource(mFeedPicture,SuperConstance.TAGUSNOW_ALBUM_PATH+tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc());
                    }else{
                        mFeedPicture.setVisibility(View.GONE);
                    }
                    //feed title
                    mFeedTitle.setHtml(tagusnowFeed.getHashtag_text());
                }
            }
        }

        @TargetApi(Build.VERSION_CODES.M)
        private void bindFeed(final FeedHolder feedHolder, String type){
            FacebookFeed facebookFeed = feedHolder.getFacebookFeed();
            final TagusnowFeed tagusnowFeed = feedHolder.getTagusnowFeed();
            final SmUser feedUser = feedHolder.getSmUser();
            final SmTagFeed.Feed.Data mFeed = feedHolder.mFeed;
            final SmTagFeed.Comment comments = feedHolder.getComment();
            final SmTagFeed.Like likes = feedHolder.getLike();

//            SkeletonConfig skeletonConfig = new SkeletonConfig();
//            feedCard.setPreventCornerOverlap(false);
//            skeletonConfig.setSkeletonIsOn(true);
//            skeletonGroup.setPosition(position);
//            if (skeletonConfig.isSkeletonIsOn()) {
//                skeletonGroup.setAutoPlay(true);
//                skeletonFeedProfile.setAutoPlay(true);
//                skeletonFeedText.setAutoPlay(true);
//                skeletonGroupProfileName.setAutoPlay(true);
//                return;
//            } else {
//                skeletonGroup.setAutoPlay(false);
//                skeletonFeedProfile.setAutoPlay(false);
//                skeletonFeedText.setAutoPlay(false);
//                skeletonGroupProfileName.setAutoPlay(false);
//                skeletonGroup.finishAnimation();
//                skeletonFeedProfile.finishAnimation();
//                skeletonFeedText.finishAnimation();
//                skeletonGroupProfileName.finishAnimation();
//            }
            /*disabled when not auth*/
            if (feedHolder.getAuthUser()!=null){
                mButtonMoreFeed.setVisibility(View.VISIBLE);
            }else{
                mButtonMoreFeed.setVisibility(View.GONE);
            }
            /*more option*/
            mButtonMoreFeed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*check auth to change menu*/
                    int mOptionPostId = 0;
                    if (feedHolder.getAuthUser()!=null){
                        if (feedHolder.getAuthUser().getId()==feedUser.getId())
                            mOptionPostId = R.menu.menu_post_option_auth;
                        else{
                            mOptionPostId = R.menu.menu_post_option_not_auth;
                        }
                    }
                    SheetMenu.with(context)
                             .setMenu(mOptionPostId)
                             .setAutoCancel(false)
                             .showIcons(true)
                             .setAutoCancel(true)
                             .setClick(new MenuItem.OnMenuItemClickListener() {
                                 @Override
                                 public boolean onMenuItemClick(MenuItem menuItem) {
                                     switch (menuItem.getItemId()){
                                         case R.id.optionSavePost :
                                             /*code*/
                                             break;
                                         case R.id.optionEditPost :
                                             Intent mPostIntent = new Intent(context, CreatePostActivity.class);
                                             mPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                                             mPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                                             mPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                                             mPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                                             mPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                                             mPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                                             mPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                                             mPostIntent.putExtra("SmTagSubCategory",intent.getStringExtra("SmTagSubCategory"));
                                             context.startActivity(mPostIntent);
                                             break;
                                         case R.id.optionHide :
                                             hidePost();
                                             break;
                                         case R.id.optionDelete :
                                             removePost();
                                             break;
                                     }
                                     return false;
                                 }
                             }).show();
                }
            });

            /*Init event of button click*/
            mFeedLikeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (feedHolder.getAuthUser()==null){
                        Intent loginIntent = new Intent(context, LoginActivity.class);
                        context.startActivity(loginIntent);
                    }
                    AsyncHttpClient mLikeFeedClient = new AsyncHttpClient();
                    RequestParams mLikeFeedParam = new RequestParams();
                    mLikeFeedParam.put("feed_id",mFeed.getId());
                    mLikeFeedParam.put("user_id",feedHolder.getAuthUser().getId());
                    String url = "";
                    if (likes.isIs_like()){
                        url = SuperUtil.getBaseUrl(SuperConstance.UNLIKE_FEED);
                    }else {
                        url = SuperUtil.getBaseUrl(SuperConstance.LIKE_FEED);
                    }
                    mLikeFeedClient.post(url, mLikeFeedParam, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                            Log.w("like response",rawJsonResponse);
                            SmTagFeed.Like mLikeClick = new Gson().fromJson(rawJsonResponse, SmTagFeed.Like.class);
                            Log.w("like",mLikeClick.getCount_likes()+"");
                            likes.setIs_like(mLikeClick.isIs_like());
                            likes.setCount_likes(mLikeClick.getCount_likes());
                            mLikeCounter.setVisibility(View.VISIBLE);
                            mLikeCounter.setText(String.valueOf(mLikeClick.getCount_likes()));
                            if (mLikeClick.isIs_like()){
                                mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                                mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                                mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                            }else {
                                mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                                mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                                mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                            Log.w("like response",rawJsonData);
                        }

                        @Override
                        protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                            return null;
                        }
                    });
                }
            });

            mFeedCommentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent commentIntent = new Intent(context, CommentActivity.class);
                    commentIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                    commentIntent.putExtra("count_likes",likes.getCount_likes());
                    commentIntent.putExtra("is_like",likes.isIs_like());
                    commentIntent.putExtra("count_comments",comments.getCount_comments());
                    commentIntent.putExtra("feed_id",feedHolder.mFeed.getId());
                    context.startActivity(commentIntent);
                }
            });

            if (type.equals(SuperConstance.PLATFORM_FACEBOOK)){
                if (facebookFeed.getCount_likes() > 0){
                    mLikeCounter.setVisibility(View.VISIBLE);
                    mLikeCounter.setText(facebookFeed.getCount_likes());
                    if (facebookFeed.getUser_id().equals(feedUser.getProfile_id())){
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                        mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                        mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                    }else {
                        mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                        mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                        mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                    }
                }else{
                    mLikeCounter.setVisibility(View.GONE);
                    mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                    mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                    mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                }

                if (facebookFeed.getCount_comments() > 0){
                    mCommentCounter.setVisibility(View.VISIBLE);
                    mCommentCounter.setText(facebookFeed.getCount_comments());
                    if (facebookFeed.getUser_id().equals(feedUser.getProfile_id())){
                        mCommentCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                        mCommentText.setTextColor(context.getColor(R.color.colorIconSelect));
                        mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                    }else {
                        mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                        mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                        mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                    }
                }else{
                    mCommentCounter.setVisibility(View.GONE);
                    mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                    mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                    mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                }

                //profile image
                if (feedUser.getStatus()==2){
                    SuperUtil.setImageResource(mProfilePicture,feedUser.getImage());
                }else{
                    if (feedUser.getImage().contains("http")){
                        SuperUtil.setImageResource(mProfilePicture,feedUser.getImage());
                    }else {
                        SuperUtil.setImageResource(mProfilePicture,SuperUtil.getProfilePicture(feedUser.getImage(),"100"));
                    }
                }
                //feed story title
                mFeedStoryTitle.setText(facebookFeed.getStory());
                //feed story time
                mFeedStoryTime.setText(facebookFeed.getTimeline());
                //feed picture
                if (facebookFeed.getHashtag_image()!=null){
                    mFeedPicture.setVisibility(View.VISIBLE);
                    SuperUtil.setImageResource(mFeedPicture,facebookFeed.getHashtag_image());
                }else{
                    mFeedPicture.setVisibility(View.GONE);
                }
                //feed title
                mFeedTitle.setHtml(facebookFeed.getHashtag_text());
            }else{
                if (likes.getCount_likes() > 0){
                    mLikeCounter.setVisibility(View.VISIBLE);
                    mLikeCounter.setText(String.valueOf(likes.getCount_likes()));
                    if (likes.isIs_like()){
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                        mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_blue);
                    }else {
                        mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                        mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_black);
                    }
                }else{
                    mLikeCounter.setVisibility(View.GONE);
                    mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                    mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                    mLikeIcon.setImageResource(R.drawable.ic_action_like_black);
                }

                if (comments.getCount_comments() > 0){
                    mCommentCounter.setVisibility(View.VISIBLE);
                    mCommentCounter.setText(String.valueOf(comments.getCount_comments()));
                    mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                    mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                    mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                }else{
                    mCommentCounter.setVisibility(View.GONE);
                    mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                    mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                    mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                }

                //profile image
                if (feedUser.getImage().contains("http")){
                    SuperUtil.setImageResource(mProfilePicture,feedUser.getImage());
                }else {
                    SuperUtil.setImageResource(mProfilePicture,SuperUtil.getProfilePicture(feedUser.getImage(),"200"));
                }
                //feed story title
                mFeedStoryTitle.setText(feedUser.getUser_name());
                //feed story time
                mFeedStoryTime.setText(tagusnowFeed.getTimeline());
                //feed picture
//                Toast.makeText(context,tagusnowFeed.getAlbum().size()+"",Toast.LENGTH_LONG).show();
                if (tagusnowFeed.getAlbum().size()==1 && !tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc().equals("")) {
                    mFeedPicture.setVisibility(View.VISIBLE);
                    albumMoreThan4.setVisibility(View.GONE);
                    album4.setVisibility(View.GONE);
                    album3.setVisibility(View.GONE);
                    album2.setVisibility(View.GONE);
                    SuperUtil.setImageResource(mFeedPicture, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"400"));
                    mFeedPicture.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                }else if(tagusnowFeed.getAlbum().size()==2){
                    mFeedPicture.setVisibility(View.GONE);
                    albumMoreThan4.setVisibility(View.GONE);
                    album4.setVisibility(View.GONE);
                    album3.setVisibility(View.GONE);
                    album2.setVisibility(View.VISIBLE);
                    SuperUtil.setImageResource(album2_1, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"200"));
                    SuperUtil.setImageResource(album2_2, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"200"));
                    album2_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    album2_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });

                }else if(tagusnowFeed.getAlbum().size()==3){
                    mFeedPicture.setVisibility(View.GONE);
                    albumMoreThan4.setVisibility(View.GONE);
                    album4.setVisibility(View.GONE);
                    album3.setVisibility(View.VISIBLE);
                    album2.setVisibility(View.GONE);
                    SuperUtil.setImageResource(album3_1, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"200"));
                    SuperUtil.setImageResource(album3_2, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"100"));
                    SuperUtil.setImageResource(album3_3, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"100"));
                    album3_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    album3_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    album3_3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                }else if(tagusnowFeed.getAlbum().size()==4){
                    mFeedPicture.setVisibility(View.GONE);
                    albumMoreThan4.setVisibility(View.GONE);
                    album4.setVisibility(View.VISIBLE);
                    album3.setVisibility(View.GONE);
                    album2.setVisibility(View.GONE);
                    SuperUtil.setImageResource(album4_1, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"100"));
                    SuperUtil.setImageResource(album4_2, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"100"));
                    SuperUtil.setImageResource(album4_3, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"100"));
                    SuperUtil.setImageResource(album4_4, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(3).getMedia().getImage().getSrc(),"100"));
                    album4_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    album4_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    album4_3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    album4_4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                }else if(tagusnowFeed.getAlbum().size()> 4){
                    mFeedPicture.setVisibility(View.GONE);
                    albumMoreThan4.setVisibility(View.VISIBLE);
                    album4.setVisibility(View.GONE);
                    album3.setVisibility(View.GONE);
                    album2.setVisibility(View.GONE);
                    SuperUtil.setImageResource(albumMoreThan4_1, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"100"));
                    SuperUtil.setImageResource(albumMoreThan4_2, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"100"));
                    SuperUtil.setImageResource(albumMoreThan4_3, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"100"));
                    SuperUtil.setImageResource(albumMoreThan4_4, SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(3).getMedia().getImage().getSrc(),"100"));
                    albumMoreThan4_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    albumMoreThan4_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    albumMoreThan4_3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    albumMoreThan4_4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                    albumMoreThan4_text.setText(tagusnowFeed.getAlbumText());
                }else{
                    mFeedPicture.setVisibility(View.GONE);
                    albumMoreThan4.setVisibility(View.GONE);
                    album4.setVisibility(View.GONE);
                    album3.setVisibility(View.GONE);
                    album2.setVisibility(View.GONE);
                }
                //feed title
                if (tagusnowFeed.getHashtag_text()==null || tagusnowFeed.getHashtag_text().equals("")){
                    feed_text_box.setVisibility(View.GONE);
                }else {
                    feed_text_box.setVisibility(View.VISIBLE);
                    mFeedTitle.setHtml(tagusnowFeed.getHashtag_text());
                    mFeedTitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
                            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                            mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                            mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                            mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                            mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                            context.startActivity(mViewPostIntent);
                        }
                    });
                }
                profileBar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String searchTitle = "";
                        if (tagusnowFeed.getOriginalHastTagText()==null || tagusnowFeed.getOriginalHastTagText().equals("")){
                            searchTitle = "";
                        }else{
                            searchTitle = mFeedTitle.getText().toString();
                        }
                        Intent mViewPostIntent = new Intent(context, ViewPostActivity.class);
                        mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(feedHolder.getAuthUser()));
                        mViewPostIntent.putExtra("postUser",new Gson().toJson(feedHolder.getSmUser()));
                        mViewPostIntent.putExtra("postComment",new Gson().toJson(feedHolder.getComment()));
                        mViewPostIntent.putExtra("postLike",new Gson().toJson(feedHolder.getLike()));
                        mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(feedHolder.getFacebookFeed()));
                        mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(feedHolder.getTagusnowFeed()));
                        mViewPostIntent.putExtra("SmTagSubCategory",intent.getStringExtra("SmTagSubCategory"));
                        mViewPostIntent.putExtra("Feed",new Gson().toJson(mFeed));
                        mViewPostIntent.putExtra("searchTitle",searchTitle);
                        context.startActivity(mViewPostIntent);
                    }
                });

            }
        }

        public void bindTagusjobFeed(SmTagFeed.Feed.Data mFeed, final SmUser feedUser, final SmTagFeed.Comment comments, final SmTagFeed.Like likes, final int position){
            try {
                Log.i("feed type",mFeed.getType());
                if (mFeed.getType().equals(SuperConstance.PLATFORM_FACEBOOK)){
                    AsyncHttpClient mFacebookFeedClient = new AsyncHttpClient();
                    RequestParams mFacebookFeedParam = new RequestParams();
                    mFacebookFeedParam.put("feed",mFeed.getFeed());
                    mFacebookFeedClient.post(SuperConstance.BASE_URL+"/deployFeed", mFacebookFeedParam, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                            Log.w("facebookFeed",rawJsonResponse);
                            try {
                                FacebookFeed facebookFeed = new Gson().fromJson(rawJsonResponse,FacebookFeed.class);
                                mFeedLikeButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                    }
                                });

                                mFeedCommentButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                    }
                                });

                                if (facebookFeed.getCount_likes() > 0){
                                    mLikeCounter.setVisibility(View.VISIBLE);
                                    mLikeCounter.setText(facebookFeed.getCount_likes());
                                    if (facebookFeed.getUser_id().equals(feedUser.getProfile_id())){
                                        mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                                        mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                                        mLikeIcon.setBackgroundColor(context.getColor(R.color.colorIconSelect));
                                    }else {
                                        mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                                        mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                                        mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                                    }
                                }else{
                                    mLikeCounter.setVisibility(View.GONE);
                                    mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                                    mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                                    mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                                }

                                if (facebookFeed.getCount_comments() > 0){
                                    mCommentCounter.setVisibility(View.VISIBLE);
                                    mCommentCounter.setText(facebookFeed.getCount_comments());
                                    if (facebookFeed.getUser_id().equals(feedUser.getProfile_id())){
                                        mCommentCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                                        mCommentText.setTextColor(context.getColor(R.color.colorIconSelect));
                                        mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                                    }else {
                                        mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                                        mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                                        mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                                    }
                                }else{
                                    mCommentCounter.setVisibility(View.GONE);
                                    mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                                    mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                                    mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                                }

                                //profile image
                                SuperUtil.setImageResource(mProfilePicture,facebookFeed.getProfile_image());
                                //feed story title
                                mFeedStoryTitle.setText(facebookFeed.getStory());
                                //feed story time
                                mFeedStoryTime.setText(facebookFeed.getTimeline());
                                //feed picture
                                if (facebookFeed.getHashtag_image()!=null){
                                    mFeedPicture.setVisibility(View.VISIBLE);
                                    SuperUtil.setImageResource(mFeedPicture,facebookFeed.getHashtag_image());
                                }else{
                                    mFeedPicture.setVisibility(View.GONE);
                                }
                                //feed title
                                mFeedTitle.setHtml(facebookFeed.getHashtag_text());
                            }catch (Exception e){
                                Log.e("facebookFeed Exception",e.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                            Log.w("onFailure facebookFeed",rawJsonData);
                        }

                        @Override
                        protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                            return null;
                        }
                    });
                }else if(mFeed.getType().equals(SuperConstance.TAGUSNOW_POST)){
                    final AsyncHttpClient mTagusnowFeedClient = new AsyncHttpClient();
                    final RequestParams mTagusnowFeedParam = new RequestParams();
                    mTagusnowFeedParam.put("feed",mFeed.getFeed());
                    mTagusnowFeedClient.post(SuperConstance.BASE_URL+"/deployFeed", mTagusnowFeedParam, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                            Log.w("tagusnowFeed",rawJsonResponse);
                            try {
                                TagusnowFeed tagusnowFeed = new Gson().fromJson(rawJsonResponse,TagusnowFeed.class);

                                mFeedLikeButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                    }
                                });

                                mFeedCommentButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                    }
                                });

                                if (likes.getCount_likes() > 0){
                                    mLikeCounter.setVisibility(View.VISIBLE);
                                    mLikeCounter.setText(String.valueOf(likes.getCount_likes()));
                                    if (likes.isIs_like()){
                                        mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                                        mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                                        mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorIconSelect)));
                                    }else {
                                        mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                                        mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                                        mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                                    }
                                }else{
                                    mLikeCounter.setVisibility(View.GONE);
                                    mLikeCounter.setTextColor(context.getColor(R.color.colorBorder));
                                    mLikeText.setTextColor(context.getColor(R.color.colorBorder));
                                    mLikeIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                                }

                                if (comments.getCount_comments() > 0){
                                    mCommentCounter.setVisibility(View.VISIBLE);
                                    mCommentCounter.setText(String.valueOf(comments.getCount_comments()));
                                    mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                                    mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                                    mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                                }else{
                                    mCommentCounter.setVisibility(View.GONE);
                                    mCommentCounter.setTextColor(context.getColor(R.color.colorBorder));
                                    mCommentText.setTextColor(context.getColor(R.color.colorBorder));
                                    mCommentIcon.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.colorBorder)));
                                }

                                //profile image
                                SuperUtil.setImageResource(mProfilePicture,feedUser.getImage());
                                //feed story title
                                mFeedStoryTitle.setText(feedUser.getUser_name());
                                //feed story time
                                mFeedStoryTime.setText(tagusnowFeed.getTimeline());
                                //feed picture
                                SuperUtil.setImageResource(mFeedPicture,SuperConstance.TAGUSNOW_ALBUM_PATH+tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc());
                                //feed title
                                mFeedTitle.setHtml(tagusnowFeed.getHashtag_text());
                            }catch (Exception ex){
//                            String err = (ex.getMessage()==null)?"SD Card failed":ex.getMessage();
//                            Log.e("Exception tagusnowFeed",err);
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                            Log.w("onFailure tagusnowFeed",rawJsonData);
                        }

                        @Override
                        protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                            return null;
                        }
                    });
                }
            }catch (Exception e){
                Log.e("onBindViewHolder",e.getMessage());
            }
        }
    }
}

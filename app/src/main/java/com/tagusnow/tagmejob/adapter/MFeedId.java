package com.tagusnow.tagmejob.adapter;

public class MFeedId {
    private int mProfilePicture;
    private int mFeedStoryTitle;
    private int mFeedStoryTime;
    private int mButtonMoreFeed;
    private int mFeedPicture;
    private int mFeedTitle;
    private int mFeedLikeButton;
    private int mLikeIcon;
    private int mLikeText;
    private int mLikeCounter;
    private int mFeedCommentButton;
    private int mCommentIcon;
    private int mCommentText;
    private int mCommentCounter;

    public MFeedId(int mProfilePicture, int mFeedStoryTitle, int mFeedStoryTime, int mButtonMoreFeed, int mFeedPicture, int mFeedTitle, int mFeedLikeButton, int mLikeIcon, int mLikeText, int mLikeCounter, int mFeedCommentButton, int mCommentIcon, int mCommentText, int mCommentCounter) {
        this.mProfilePicture = mProfilePicture;
        this.mFeedStoryTitle = mFeedStoryTitle;
        this.mFeedStoryTime = mFeedStoryTime;
        this.mButtonMoreFeed = mButtonMoreFeed;
        this.mFeedPicture = mFeedPicture;
        this.mFeedTitle = mFeedTitle;
        this.mFeedLikeButton = mFeedLikeButton;
        this.mLikeIcon = mLikeIcon;
        this.mLikeText = mLikeText;
        this.mLikeCounter = mLikeCounter;
        this.mFeedCommentButton = mFeedCommentButton;
        this.mCommentIcon = mCommentIcon;
        this.mCommentText = mCommentText;
        this.mCommentCounter = mCommentCounter;
    }

    public MFeedId(){
        super();
    }

    public int getmProfilePicture() {
        return mProfilePicture;
    }

    public void setmProfilePicture(int mProfilePicture) {
        this.mProfilePicture = mProfilePicture;
    }

    public int getmFeedStoryTitle() {
        return mFeedStoryTitle;
    }

    public void setmFeedStoryTitle(int mFeedStoryTitle) {
        this.mFeedStoryTitle = mFeedStoryTitle;
    }

    public int getmFeedStoryTime() {
        return mFeedStoryTime;
    }

    public void setmFeedStoryTime(int mFeedStoryTime) {
        this.mFeedStoryTime = mFeedStoryTime;
    }

    public int getmButtonMoreFeed() {
        return mButtonMoreFeed;
    }

    public void setmButtonMoreFeed(int mButtonMoreFeed) {
        this.mButtonMoreFeed = mButtonMoreFeed;
    }

    public int getmFeedPicture() {
        return mFeedPicture;
    }

    public void setmFeedPicture(int mFeedPicture) {
        this.mFeedPicture = mFeedPicture;
    }

    public int getmFeedTitle() {
        return mFeedTitle;
    }

    public void setmFeedTitle(int mFeedTitle) {
        this.mFeedTitle = mFeedTitle;
    }

    public int getmFeedLikeButton() {
        return mFeedLikeButton;
    }

    public void setmFeedLikeButton(int mFeedLikeButton) {
        this.mFeedLikeButton = mFeedLikeButton;
    }

    public int getmLikeIcon() {
        return mLikeIcon;
    }

    public void setmLikeIcon(int mLikeIcon) {
        this.mLikeIcon = mLikeIcon;
    }

    public int getmLikeText() {
        return mLikeText;
    }

    public void setmLikeText(int mLikeText) {
        this.mLikeText = mLikeText;
    }

    public int getmLikeCounter() {
        return mLikeCounter;
    }

    public void setmLikeCounter(int mLikeCounter) {
        this.mLikeCounter = mLikeCounter;
    }

    public int getmFeedCommentButton() {
        return mFeedCommentButton;
    }

    public void setmFeedCommentButton(int mFeedCommentButton) {
        this.mFeedCommentButton = mFeedCommentButton;
    }

    public int getmCommentIcon() {
        return mCommentIcon;
    }

    public void setmCommentIcon(int mCommentIcon) {
        this.mCommentIcon = mCommentIcon;
    }

    public int getmCommentText() {
        return mCommentText;
    }

    public void setmCommentText(int mCommentText) {
        this.mCommentText = mCommentText;
    }

    public int getmCommentCounter() {
        return mCommentCounter;
    }

    public void setmCommentCounter(int mCommentCounter) {
        this.mCommentCounter = mCommentCounter;
    }
}

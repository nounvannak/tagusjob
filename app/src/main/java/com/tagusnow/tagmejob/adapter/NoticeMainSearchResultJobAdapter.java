package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.view.BaseCard.CardSmall;
import com.tagusnow.tagmejob.view.Empty.EmptySearch;
import com.tagusnow.tagmejob.view.Holder.CardSmallHolder;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptySearchHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Main.ResultSearchJobFaceHolder;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultHeader;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultHeaderHolder;
import com.tagusnow.tagmejob.view.Search.Main.ResultSearchJobFace;
import java.util.List;

public class NoticeMainSearchResultJobAdapter extends RecyclerView.Adapter{

    private static final String TAG = NoticeMainSearchResultJobAdapter.class.getSimpleName();
    public static final int MORE_VIEW = 1;
    private static final int HEADER = 0;
    private static final int BODY = 1;
    private static final int EMPTY = 2;
    private Activity activity;
    private Context context;
    private FeedResponse feedResponse;
    private List<Feed> feedList;
    private SmUser user;
    private String query;
    private int type;
    public static final int TYPE = 12;

    public NoticeMainSearchResultJobAdapter(Activity activity,FeedResponse feedResponse,SmUser user,String query){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.user = user;
        this.query = query;
        this.feedList = feedResponse.getData();
    }

    public NoticeMainSearchResultJobAdapter(Activity activity,FeedResponse feedResponse,SmUser user,String query,int type){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.user = user;
        this.query = query;
        this.feedList = feedResponse.getData();
        this.type = type;
    }

    public NoticeMainSearchResultJobAdapter update(Activity activity,FeedResponse feedResponse,SmUser user,String query){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.user = user;
        this.query = query;
        this.feedList.addAll(feedResponse.getData());
        return this;
    }

    public NoticeMainSearchResultJobAdapter update(Activity activity,FeedResponse feedResponse,SmUser user,String query,int type){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.user = user;
        this.query = query;
        this.feedList.addAll(feedResponse.getData());
        this.type = type;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;

        if (this.feedResponse.getTotal() > 0){
            if (position==0){
                if (this.type==TYPE){
                    viewType = BODY;
                }else {
                    viewType = HEADER;
                }
            }else {
                viewType = BODY;
            }
        }else {
            viewType = EMPTY;
        }

        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==HEADER) {
            return new NoticeMainSearchResultHeaderHolder(new NoticeMainSearchResultHeader(this.context));
        }else if (viewType==EMPTY){
            return new EmptySearchHolder(new EmptySearch(this.context));
        }else {
//            return new ResultSearchJobFaceHolder(new ResultSearchJobFace(this.context));
            return new CardSmallHolder(new CardSmall(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==HEADER) {
            ((NoticeMainSearchResultHeaderHolder) holder).bindView(this.activity, this.user, NoticeMainSearchResultHeader.JOB, this.query);
        }else if (getItemViewType(position)==EMPTY){
            ((EmptySearchHolder) holder).bindView(this.activity,this.query);
        }else {
            if (this.type==TYPE) {
                ((CardSmallHolder) holder).bindView(this.activity,this,this.feedList.get(position),this.user);
            }else {
//                ((CardSmallHolder) holder).bindView(this.activity, this.feedList.get(position - MORE_VIEW), this.user, false);
                ((CardSmallHolder) holder).bindView(this.activity,this,this.feedList.get(position - MORE_VIEW),this.user);
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.feedResponse.getTotal() > 0 ? this.feedResponse.getTo() : 1;
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.Empty.EmptySearch;
import com.tagusnow.tagmejob.view.FollowerFace;
import com.tagusnow.tagmejob.view.Holder.ConnectSuggestFaceHolder;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptySearchHolder;
import com.tagusnow.tagmejob.view.Holder.Profile.User.TitleHolder;
import com.tagusnow.tagmejob.view.Holder.Search.RecentSearchCardHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Suggest.ResultHolder;
import com.tagusnow.tagmejob.view.Holder.ViewFollowerHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.FollowSuggestHolder;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultHeader;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultHeaderHolder;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestResultFace;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestFace;

import java.util.List;

public class NoticeMainSearchResultPeopleAdapter extends RecyclerView.Adapter{

    private static final int HEADER = 0;
    private static final int BODY = 1;
    public static final int MORE_VIEW = 1;
    private static final int EMPTY = 2;
    public static final int TYPE = 9;
    private Activity activity;
    private Context context;
    private UserPaginate userPaginate;
    private List<SmUser> userList;
    private SmUser user;
    private String query;
    private int type;

    public NoticeMainSearchResultPeopleAdapter(Activity activity,UserPaginate userPaginate,SmUser user,String query){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.userPaginate = userPaginate;
        this.userList = userPaginate.getData();
        this.query = query;
    }

    public NoticeMainSearchResultPeopleAdapter(Activity activity,UserPaginate userPaginate,SmUser user,String query,int type){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.userPaginate = userPaginate;
        this.userList = userPaginate.getData();
        this.query = query;
        this.type = type;
    }

    public NoticeMainSearchResultPeopleAdapter update(Activity activity,UserPaginate userPaginate,SmUser user,String query){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.userPaginate = userPaginate;
        this.userList.addAll(userPaginate.getData());
        this.query = query;
        return this;
    }

    public NoticeMainSearchResultPeopleAdapter update(Activity activity,UserPaginate userPaginate,SmUser user,String query,int type){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.userPaginate = userPaginate;
        this.userList.addAll(userPaginate.getData());
        this.query = query;
        this.type = type;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (this.userPaginate.getTotal() > 0){
            if (position==0){
                if (this.type==TYPE){
                    viewType = BODY;
                }else {
                    viewType = HEADER;
                }
            }else {
                viewType = BODY;
            }
        }else {
            viewType = EMPTY;
        }

        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==HEADER) {
            return new NoticeMainSearchResultHeaderHolder(new NoticeMainSearchResultHeader(this.context));
        }else if (viewType==EMPTY){
            return new EmptySearchHolder(new EmptySearch(this.context));
        }else {
            if (this.type==TYPE){
                return new ViewFollowerHolder(new FollowerFace(this.context));
            }else {
                return new ResultHolder(new SearchSuggestResultFace(this.context));
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==HEADER) {
            ((NoticeMainSearchResultHeaderHolder) holder).bindView(this.activity, this.user, NoticeMainSearchResultHeader.PEOPLE, this.query);
        }else if (getItemViewType(position)==EMPTY){
            ((EmptySearchHolder) holder).bindView(this.activity,this.query);
        }else {
            if (this.type==TYPE){
                ((ViewFollowerHolder) holder).bindView(this.activity,this.user,this.userList.get(position));
            }else {
                ((ResultHolder) holder).bindView(this.activity,this.userList.get(position - MORE_VIEW));
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.userPaginate.getTotal() > 0 ? this.userPaginate.getTo() : 1;
    }
}

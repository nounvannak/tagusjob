package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.view.Empty.EmptySearch;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptySearchHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Main.ResultSearchPostFaceHolder;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultHeader;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultHeaderHolder;
import com.tagusnow.tagmejob.view.Search.Main.ResultSearchPostFace;
import com.tagusnow.tagmejob.view.Search.UI.Face.Normal.SearchUIFaceNormal;
import com.tagusnow.tagmejob.view.Search.UI.Holder.Normal.SearchUIFaceNormalHolder;

import java.util.List;

public class NoticeMainSearchResultPostAdapter extends RecyclerView.Adapter{

    private static final String TAG = NoticeMainSearchResultPostAdapter.class.getSimpleName();
    public static final int TYPE = 21;
    public static final int MORE_VIEW = 1;
    private static final int HEADER = 0;
    private static final int BODY = 1;
    private static final int EMPTY = 2;
    private Activity activity;
    private Context context;
    private SmUser user;
    private FeedResponse feedResponse;
    private List<Feed> feedList;
    private String query;
    private int type;

    public NoticeMainSearchResultPostAdapter(Activity activity,FeedResponse feedResponse,SmUser user,String query){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.query = query;
        this.feedResponse = feedResponse;
        this.feedList = feedResponse.getData();
    }

    public NoticeMainSearchResultPostAdapter update(Activity activity,FeedResponse feedResponse,SmUser user,String query){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.query = query;
        this.feedResponse = feedResponse;
        this.feedList.addAll(feedResponse.getData());
        return this;
    }


    public NoticeMainSearchResultPostAdapter(Activity activity,FeedResponse feedResponse,SmUser user,String query,int type){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.query = query;
        this.feedResponse = feedResponse;
        this.feedList = feedResponse.getData();
        this.type = type;
    }

    public NoticeMainSearchResultPostAdapter update(Activity activity,FeedResponse feedResponse,SmUser user,String query,int type){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.query = query;
        this.feedResponse = feedResponse;
        this.feedList.addAll(feedResponse.getData());
        this.type = type;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (this.feedResponse.getTotal() > 0){
            if (position==0){
                if (this.type==TYPE){
                    viewType = BODY;
                }else {
                    viewType = HEADER;
                }
            }else {
                viewType = BODY;
            }
        }else {
            viewType = EMPTY;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==HEADER) {
            return new NoticeMainSearchResultHeaderHolder(new NoticeMainSearchResultHeader(this.context));
        }else if (viewType==EMPTY){
            return new EmptySearchHolder(new EmptySearch(this.context));
        }else {
//            return new ResultSearchPostFaceHolder(new ResultSearchPostFace(this.context));
            return new SearchUIFaceNormalHolder(new SearchUIFaceNormal(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==HEADER) {
            ((NoticeMainSearchResultHeaderHolder) holder).bindView(this.activity, this.user, NoticeMainSearchResultHeader.POST, this.query);
        }else if (getItemViewType(position)==EMPTY){
            ((EmptySearchHolder) holder).bindView(this.activity,this.query);
        }else {

            return;
//            if (this.type==TYPE){
////                ((ResultSearchPostFaceHolder) holder).bindView(this.activity,this.user,this.feedList.get(position),false);
//                ((SearchUIFaceNormalHolder) holder).bindView(this.activity,this.feedList.get(position),this.user,this,false);
//            }else {
////                ((ResultSearchPostFaceHolder) holder).bindView(this.activity,this.user,this.feedList.get(position - MORE_VIEW),false);
//                ((SearchUIFaceNormalHolder) holder).bindView(this.activity,this.feedList.get(position - MORE_VIEW),this.user,this,false);
//            }
        }
    }

    @Override
    public int getItemCount() {
        return this.feedResponse.getTotal() > 0 ? this.feedResponse.getTo() : 1;
    }
}

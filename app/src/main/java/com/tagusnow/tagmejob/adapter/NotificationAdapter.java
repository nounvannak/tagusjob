package com.tagusnow.tagmejob.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewNotificationComment;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.feed.NotificationResponse;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private Context context;
    private Activity activity;
    private SmUser authUser;
    private NotificationResponse notificationResponse;
    private ArrayList<Notification> notifications = new ArrayList<Notification>();


    public NotificationAdapter(Context context){
        this.context = context;
        this.authUser = new Auth(context).checkAuth().token();
    }

    public NotificationAdapter with(NotificationResponse notificationResponse){
        this.notificationResponse = notificationResponse;
        this.notifications.addAll(notificationResponse.getData());
        return this;
    }

    public NotificationAdapter setActivity(Activity activity){
        this.activity = activity;
        return this;
    }

    @NonNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.new_notification_layout,parent,false);
        return new NotificationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.ViewHolder holder, int position) {
        holder.with(notifications.get(position)).setActivity(activity).setAuth(authUser).bind();
    }

    @Override
    public int getItemCount() {
        return notificationResponse.getTo();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final String TAG = ViewHolder.class.getSimpleName();
        private static final String LIKE_USER = "like_user";
        private static final String FOLLOW_USER = "follow_user";
        private static final String LIKE_POST = "like_post";
        private static final String COMMENT_POST = "comment_post";
        private static final String SEE_FIRST = "see_first";
        private Context context;
        private Activity activity;
        private SmUser authUser;
        private Notification notification;
        private SmTagSubCategory category;
        /*View*/
        private LinearLayout notificationBackground;
        private CircleImageView user_profile;
        private TextView notification_body,timeline;

        private ViewHolder setActivity(Activity activity){
            this.activity = activity;
            return this;
        }

        private ViewHolder setAuth(SmUser authUser){
            this.authUser = authUser;
            return this;
        }

        private ViewHolder with(Notification notification){
            this.notification = notification;
            return this;
        }

        private void initView(View view){
            notificationBackground = (LinearLayout)view.findViewById(R.id.notificationBackground);
            user_profile = (CircleImageView)view.findViewById(R.id.user_profile);
            notification_body = (TextView)view.findViewById(R.id.notification_body);
            timeline = (TextView)view.findViewById(R.id.timeline);

            notificationBackground.setOnClickListener(this);
        }

        private void viewNotification(){
            switch (notification.getType()) {
                case LIKE_POST:
                    if (notification.getFeed() != null) {
                        viewLike();
                    } else {
                        Log.w(TAG, "feed null");
                    }
                    break;
                case COMMENT_POST:
                    viewComment();
                    break;
                case LIKE_USER:
                    viewProfile();
                    break;
                case FOLLOW_USER:
                    viewProfile();
                    break;
                default:
                    viewSystemNotification();
                    break;
            }
        }

        private void viewSystemNotification(){}

        private void viewProfile(){
            context.startActivity(ViewProfileActivity
                    .createIntent(activity)
                    .putExtra("SmTagSubCategory",new Gson().toJson(category))
                    .putExtra("authUser",new Gson().toJson(authUser))
                    .putExtra("profileUser",new Gson().toJson(notification.getRequest_user())));
        }

        private void viewComment(){
            context.startActivity(ViewNotificationComment.createIntent(activity).putExtra("feed_id",notification.getFeed().getId()));
        }

        private void viewLike(){
            String searchTitle = "Posts";
            Intent mViewPostIntent = new Intent(context,ViewPostActivity.class);
            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
            mViewPostIntent.putExtra("SmTagSubCategory",new Gson().toJson(category));
            mViewPostIntent.putExtra("Feed",new Gson().toJson(notification.getFeed()));
            mViewPostIntent.putExtra("searchTitle",searchTitle);
            context.startActivity(mViewPostIntent);
        }

        @TargetApi(Build.VERSION_CODES.M)
        private void bind(){

            notification_body.setText(this.notification.getBody());
            timeline.setText(this.notification.getTimeline());
            if (!this.notification.isSeen()){
                notificationBackground.setBackgroundColor(Color.WHITE);
                timeline.setTextColor(context.getColor(R.color.colorIcon));
//                notificationBackground.setBackgroundColor(context.getColor(R.color.colorNotRead));
//                timeline.setTextColor(context.getColor(R.color.colorNotificationHours));
            }else {
                notificationBackground.setBackgroundColor(Color.WHITE);
                timeline.setTextColor(context.getColor(R.color.colorIcon));
            }

            if (this.notification.getType().equals("system")){
                Glide.with(context).load(R.mipmap.ic_launcher_circle).into(user_profile);
            }else{
                if (this.notification.getRequest_user_profile().contains("http")){
                    Glide.with(context)
                            .load(this.notification.getRequest_user_profile())
                            .error(R.mipmap.ic_launcher_circle)
                            .into(user_profile);
                }else{
                    Glide.with(context)
                            .load(SuperUtil.getProfilePicture(this.notification.getRequest_user_profile(),"100"))
                            .error(R.mipmap.ic_launcher_circle)
                            .into(user_profile);
                }
            }
        }

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
            this.context = itemView.getContext();
            this.category = new Repository(this.context).restore().getCategory();
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.notificationBackground:
                    viewNotification();
                    break;
            }
        }
    }
}

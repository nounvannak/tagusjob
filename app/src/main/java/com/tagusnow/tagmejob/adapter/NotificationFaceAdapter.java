package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.v3.empty.EmptyHolder;
import com.tagusnow.tagmejob.v3.empty.EmptyView;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.notification.NCommentCell;
import com.tagusnow.tagmejob.v3.notification.NCommentHolder;
import com.tagusnow.tagmejob.v3.notification.NFollowCell;
import com.tagusnow.tagmejob.v3.notification.NFollowHolder;
import com.tagusnow.tagmejob.v3.notification.NJobCell;
import com.tagusnow.tagmejob.v3.notification.NJobHolder;
import com.tagusnow.tagmejob.v3.notification.NListener;
import com.tagusnow.tagmejob.v3.notification.NMessageCell;
import com.tagusnow.tagmejob.v3.notification.NMessageHolder;
import com.tagusnow.tagmejob.v3.notification.NStaffCell;
import com.tagusnow.tagmejob.v3.notification.NStaffHolder;
import com.tagusnow.tagmejob.v3.notification.NSystemCell;
import com.tagusnow.tagmejob.v3.notification.NSystemHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;

import java.util.List;

public class NotificationFaceAdapter extends RecyclerView.Adapter{

    private static final int COMMENT = 0;
    private static final int FOLLOW = 1;
    private static final int LIKE = 8;
    private static final int JOB = 2;
    private static final int STAFF = 3;
    private static final int MESSAGE = 4;
    private static final int SYSTEM = 5;
    private static final int LOADING = 6;
    private static final int EMPTY = 7;

    private Activity activity;
    private List<Notification> notifications;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public NotificationFaceAdapter(Activity activity, NListener listener,LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;

        this.listener = listener;
        this.loadingListener = loadingListener;
        this.loadingView = new LoadingView(activity);
    }

    private NListener listener;

    public void NextData(List<Notification> notifications){
        this.notifications.addAll(notifications);
        notifyDataSetChanged();
    }

    public void NewData(List<Notification> notifications){
        this.notifications = notifications;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (notifications.size() > 0){
            if (position == (getItemCount() - 1)){
                viewType = LOADING;
            }else {
                if (notifications.get(position).getType().equals(Notification.COMMENT_POST)){
                    viewType = COMMENT;
                }else if (notifications.get(position).getType().equals(Notification.FOLLOW_USER)){
                    viewType = FOLLOW;
                }else if (notifications.get(position).getType().equals(Notification.JOB_SEND)){
                    viewType = JOB;
                }else if (notifications.get(position).getType().equals(Notification.SUGGEST_STAFF)){
                    viewType = STAFF;
                }else if (notifications.get(position).getType().equals(Notification.SEND_MESSAGE)){
                    viewType = MESSAGE;
                }else if (notifications.get(position).getType().equals(Notification.LIKE_POST)){
                    viewType = LIKE;
                }else {
                    viewType = SYSTEM;
                }
            }
        }else {
            viewType = EMPTY;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == COMMENT){
            return new NCommentHolder(new NCommentCell(activity));
        }else if (viewType == FOLLOW){
            return new NFollowHolder(new NFollowCell(activity));
        }else if (viewType == JOB){
            return new NJobHolder(new NJobCell(activity));
        }else if (viewType == STAFF){
            return new NStaffHolder(new NStaffCell(activity));
        }else if (viewType == MESSAGE){
            return new NMessageHolder(new NMessageCell(activity));
        }else if (viewType == LIKE){
            return new NCommentHolder(new NCommentCell(activity));
        }else if (viewType == SYSTEM){
            return new NSystemHolder(new NSystemCell(activity));
        }else if (viewType == LOADING){
            return new LoadingHolder(loadingView);
        }else {
            return new EmptyHolder(new EmptyView(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == COMMENT || getItemViewType(position) == LIKE){
            ((NCommentHolder) holder).BindView(activity,notifications.get(position),listener);
        }else if (getItemViewType(position) == FOLLOW){
            ((NFollowHolder) holder).BindView(activity,notifications.get(position),listener);
        }else if (getItemViewType(position) == JOB){
            ((NJobHolder) holder).BindView(activity,notifications.get(position),listener);
        }else if (getItemViewType(position) == STAFF){
            ((NStaffHolder) holder).BindView(activity,notifications.get(position),listener);
        }else if (getItemViewType(position) == MESSAGE){
            ((NMessageHolder) holder).BindView(activity,notifications.get(position),listener);
        }else if (getItemViewType(position) == SYSTEM){
            ((NSystemHolder) holder).BindView(activity,notifications.get(position),listener);
        }else if (getItemViewType(position) == LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else {
            ((EmptyHolder) holder).BindView(activity);
        }
    }

    @Override
    public int getItemCount() {
        return notifications.size() > 0 ? (notifications.size() + 1) : 1;
    }
}

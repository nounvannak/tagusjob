package com.tagusnow.tagmejob.adapter;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem;

public class PostItem implements AsymmetricItem {
    private int coumnSpan = 0;
    private int rowSpan = 0;
    private int position = 0;

    public PostItem(Parcel in){
        readFromParcel(in);
    }

    public PostItem(){
        this(1,1,0);
    }

    public PostItem(int coumnSpan, int rowSpan, int position) {
        this.coumnSpan = coumnSpan;
        this.rowSpan = rowSpan;
        this.position = position;
    }


    @Override public String toString() {
        return String.format("%s: %sx%s", position, rowSpan, coumnSpan);
    }


    @Override
    public int getColumnSpan() {
        return this.coumnSpan;
    }

    @Override
    public int getRowSpan() {
        return this.rowSpan;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public int getPosition() {
        return position;
    }

    private void readFromParcel(Parcel in) {
        coumnSpan = in.readInt();
        rowSpan = in.readInt();
        position = in.readInt();
    }

    @Override public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(coumnSpan);
        dest.writeInt(rowSpan);
        dest.writeInt(position);
    }

    /* Parcelable interface implementation */
    public static final Parcelable.Creator<PostItem> CREATOR = new Parcelable.Creator<PostItem>() {
        @Override public PostItem createFromParcel(@NonNull Parcel in) {
            return new PostItem(in);
        }

        @Override @NonNull public PostItem[] newArray(int size) {
            return new PostItem[size];
        }
    };
}

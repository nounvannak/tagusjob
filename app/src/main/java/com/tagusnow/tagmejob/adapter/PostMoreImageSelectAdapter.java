package com.tagusnow.tagmejob.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;

import java.util.ArrayList;

public class PostMoreImageSelectAdapter extends RecyclerView.Adapter<PostMoreImageSelectAdapter.ViewHolder>{

    private Context context;
    static ArrayList<String> mFileSelectStr = new ArrayList<>();

    @NonNull
    @Override
    public PostMoreImageSelectAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewGroup group = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.layout_more_image_select,parent,false);
        return new ViewHolder(group);
    }

    public PostMoreImageSelectAdapter(Context context, ArrayList<String> mFileSelectStr){
        this.context = context;
        this.mFileSelectStr = mFileSelectStr;
    }

    @Override
    public void onBindViewHolder(@NonNull PostMoreImageSelectAdapter.ViewHolder holder, int position) {
        holder.bindView(context,mFileSelectStr.get(position));
    }

    @Override
    public int getItemCount() {
        return mFileSelectStr.size();
    }

    public static ArrayList<String> getmFileSelectStr() {
        return mFileSelectStr;
    }

    public static void setmFileSelectStr(ArrayList<String> mFileSelectStr) {
        PostMoreImageSelectAdapter.mFileSelectStr = mFileSelectStr;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private Context context;
        private ImageView imageOfMoreSelectImage;
        private ImageButton buttonOfMoreSelectImage;
        private ArrayList<String> mFileSelectStrs = new ArrayList<>();

        public ViewHolder(View itemView) {
            super(itemView);
            imageOfMoreSelectImage = (ImageView)itemView.findViewById(R.id.imageOfMoreImageSelect);
            buttonOfMoreSelectImage = (ImageButton)itemView.findViewById(R.id.buttonOfMoreImageSelect);
            this.mFileSelectStrs = getmFileSelectStr();
        }

        private void bindView(Context context, final String mFileSelectStr){
            this.context = context;
            Glide.with(this.context).load(mFileSelectStr)
                    .centerCrop()
                    .into(imageOfMoreSelectImage);
            buttonOfMoreSelectImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemRemoved(getAdapterPosition());
                    mFileSelectStrs.remove(mFileSelectStr);
                    setmFileSelectStr(mFileSelectStrs);
                }
            });
        }
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.airbnb.lottie.L;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.BaseCard.Card;
import com.tagusnow.tagmejob.view.BaseCard.CardCV;
import com.tagusnow.tagmejob.view.BaseCard.CardJob;
import com.tagusnow.tagmejob.view.Holder.CardCVHolder;
import com.tagusnow.tagmejob.view.Holder.CardHolder;
import com.tagusnow.tagmejob.view.Holder.CardJobHolder;
import com.tagusnow.tagmejob.view.Holder.Profile.User.TitleHolder;
import com.tagusnow.tagmejob.view.Holder.ProfileHeaderHolder;
import com.tagusnow.tagmejob.view.Holder.ProfileInfoHolder;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.FollowSuggestHorizontalHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.WidgetImagePostHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.WidgetThreePostHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Profile.User.BodyTitleLayout;
import com.tagusnow.tagmejob.view.ProfileHeader;
import com.tagusnow.tagmejob.view.ProfileInfo;
import com.tagusnow.tagmejob.view.Space;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestHorizontalLayout;
import com.tagusnow.tagmejob.view.Widget.WidgetImagePost;
import com.tagusnow.tagmejob.view.Widget.WidgetThreePost;

import java.util.List;

public class ProfileAdapter extends RecyclerView.Adapter{

    private static final String TAG = ProfileAdapter.class.getSimpleName();
    public static final int MORE_VIEW = 5;
    private static final int PROFILE = 0;
    private static final int WIDGET_POST = 1;
    private static final int WIDGET_FOLLOWING = 2;
    private static final int POST_TITLE = 4;
    private static final int INFO_BODY = 5;
    private static final int USER_IMAGE_POST = 6;
    private static final int JOB = 9;
    private static final int NORMAL = 8;
    private static final int CV = 7;
    private static final int FEED = 3;
    private static final int LOADING = 10;
    private static final int SPACE = 11;
    public static final int HOME = 0;
    public static final int INFO = 1;
    private Activity activity;
    private Context context;
    private FeedResponse feedResponse;
    private List<Feed> feedList;
    private SmUser user;
    private ProfileHeader profileHeader;
    private int type;

    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;


    public ProfileAdapter(Activity activity,FeedResponse feedResponse,SmUser user,int type,LoadingListener<LoadingView> loadingListener){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feedList = feedResponse.getData();
        if (this.profileHeader==null){
            this.profileHeader = new ProfileHeader(this.context);
        }
        this.type = type;
        this.user = user;
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public ProfileAdapter(Activity activity,SmUser user,int type){
        this.activity = activity;
        this.context = activity;
        if (this.profileHeader==null){
            this.profileHeader = new ProfileHeader(this.context);
        }
        this.type = type;
        this.user = user;
    }

    public ProfileAdapter update(Activity activity,FeedResponse feedResponse,SmUser user,int type){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feedList.addAll(feedResponse.getData());
        this.user = user;
        this.type = type;
        return this;
    }

    public ProfileHeader getProfileHeader() {
        return profileHeader;
    }

    public ProfileAdapter update(Activity activity, SmUser user, int type){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.type = type;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;

        if (this.type==HOME){
            if (position==0){
                viewType = PROFILE;
            }else if (position==1){
                viewType = WIDGET_POST;
            }else if (position==2) {
                viewType = WIDGET_FOLLOWING;
            }else if (position==4) {
                viewType = POST_TITLE;
            }else if (position==3) {
                viewType = USER_IMAGE_POST;
            }else {
                if (this.feedResponse.getTotal() > 0){
                    if ((this.feedResponse.getTo() - MORE_VIEW) > (position - MORE_VIEW)){
                        if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.NORMAL_POST)){
                            viewType = NORMAL;
                        }else if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.JOB_POST)){
                            viewType = JOB;
                        }else if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.STAFF_POST)) {
                            viewType = CV;
                        }else if (position==(getItemCount() - 1)){
                            viewType = LOADING;
                        }
                    }else if ((this.feedResponse.getTo() - MORE_VIEW) == (position - MORE_VIEW)){
                        viewType = SPACE;
                    }
                }else{
                   viewType = SPACE;
                }
            }
        }else {
            if (position==0){
                viewType = PROFILE;
            }else {
                viewType = INFO_BODY;
            }
        }

        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (this.type==HOME){
            if (viewType==PROFILE){
                return new ProfileHeaderHolder(this.profileHeader);
            }else if (viewType==WIDGET_POST){
                return new WidgetThreePostHolder(new WidgetThreePost(this.context));
            }else if (viewType==WIDGET_FOLLOWING){
                return new FollowSuggestHorizontalHolder(new FollowSuggestHorizontalLayout(this.context));
            }else if (viewType==POST_TITLE) {
                return new TitleHolder(new BodyTitleLayout(this.context));
            }else if (viewType==USER_IMAGE_POST) {
                return new WidgetImagePostHolder(new WidgetImagePost(this.context));
            }else if (viewType==LOADING){
                return new LoadingHolder(loadingView);
            }else if (viewType==NORMAL){
                return new CardHolder(new Card(this.context));
            }else if (viewType==JOB){
                return new CardJobHolder(new CardJob(this.context));
            }else if (viewType==CV){
                return new CardCVHolder(new CardCV(this.context));
            }else {
                return new SpaceHolder(new Space(this.context));
            }
        }else {
            if (viewType==PROFILE){
                return new ProfileHeaderHolder(this.profileHeader);
            }else {
                return new ProfileInfoHolder(new ProfileInfo(this.context));
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (this.type==HOME){
            if (getItemViewType(position)==PROFILE){
                ((ProfileHeaderHolder) holder).bindView(this.activity,this.user);
            }else if (getItemViewType(position)==WIDGET_POST){
                ((WidgetThreePostHolder) holder).bindView(this.activity,this.user);
            }else if (getItemViewType(position)==WIDGET_FOLLOWING){
                ((FollowSuggestHorizontalHolder) holder).bindView(this.activity,this.user);
            }else if (getItemViewType(position)==POST_TITLE) {
                ((TitleHolder) holder).bindView();
            }else if (getItemViewType(position)==USER_IMAGE_POST) {
                ((WidgetImagePostHolder) holder).bindView(this.activity, this.user);
            }else if (getItemViewType(position)==LOADING){
                ((LoadingHolder) holder).BindView(activity,loadingListener);
            }else if (getItemViewType(position)==NORMAL){
                ((CardHolder) holder).Adapter(this).bindView(this.activity,this.user,this.feedList.get(position - MORE_VIEW));
            }else if (getItemViewType(position)==JOB){
                ((CardJobHolder) holder).bindView(this.activity,this.feedList.get(position - MORE_VIEW),this,this.user);
            }else if (getItemViewType(position)==CV){
                ((CardCVHolder) holder).bindView(this.activity,this.feedList.get(position -  MORE_VIEW),this,this.user);
            }else {
                ((SpaceHolder) holder).bindView();
            }
        }else {
            if (getItemViewType(position)==PROFILE){
                ((ProfileHeaderHolder) holder).bindView(this.activity,this.user);
            }else {
                ((ProfileInfoHolder) holder).bindView(this.activity,this.user);
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.type==HOME ? this.feedResponse.getTo() + 1: 2;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

package com.tagusnow.tagmejob.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewDetailPostActivity;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FacebookFeed;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.feed.TagusnowFeed;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.whalemare.sheetmenu.SheetMenu;

public class ProfileHomeAdapter extends RecyclerView.Adapter<ProfileHomeAdapter.ViewHolder> {

    private Context context;
    private Intent intent;
    private SmTagFeed smTagFeed;
    private SmTagSubCategory category;
    private ArrayList<SmTagFeed.Feed.Data> feed = new ArrayList<SmTagFeed.Feed.Data>();
    private ArrayList<FacebookFeed> facebookFeed = new ArrayList<FacebookFeed>();
    private ArrayList<TagusnowFeed> tagusnowFeed = new ArrayList<TagusnowFeed>();
    private ArrayList<SmUser> postUser = new ArrayList<SmUser>();
    private ArrayList<SmTagFeed.Comment> comment = new ArrayList<SmTagFeed.Comment>();
    private ArrayList<SmTagFeed.Like> like = new ArrayList<SmTagFeed.Like>();

    public ProfileHomeAdapter(Context context){
        this.context = context;
    }

    public ProfileHomeAdapter setIntent(Intent intent) {
        this.intent = intent;
        return this;
    }

    public ProfileHomeAdapter setCategory(SmTagSubCategory category){
        this.category = category;
        return this;
    }

    public ProfileHomeAdapter with(SmTagFeed smTagFeed){
        this.smTagFeed = smTagFeed;
        this.feed.addAll(smTagFeed.getFeed().getData());
        this.facebookFeed.addAll(smTagFeed.getFacebook_feed());
        this.tagusnowFeed.addAll(smTagFeed.getTagusnow_feed());
        this.postUser.addAll(smTagFeed.getUser());
        this.comment.addAll(smTagFeed.getComments());
        this.like.addAll(smTagFeed.getLikes());
        return this;
    }

    @NonNull
    @Override
    public ProfileHomeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card,parent,false);
        return new ProfileHomeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileHomeAdapter.ViewHolder holder, int position) {
        try {
            holder.setIntent(intent)
                    .setFeed(feed.get(position))
                    .setAuthUser(smTagFeed.getAuth_user())
                    .setPostUser(postUser.get(position))
                    .setComment(comment.get(position))
                    .setLike(like.get(position))
                    .setTagusnowFeed(tagusnowFeed.get(position))
                    .setCategory(category)
                    .bindView();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.smTagFeed.getFeed().getTo();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private Intent intent;
        private Context context;
        private SmTagFeed.Feed.Data feed;
        private SmUser authUser,postUser;
        private SmTagFeed.Comment comment;
        private SmTagFeed.Like like;
        private TagusnowFeed tagusnowFeed;
        private SmTagSubCategory category;
        /*view card*/
        CardView feedCard;
        CircleImageView mProfilePicture;
        TextView mFeedStoryTitle,mFeedStoryTime,mLikeText,mLikeCounter,mCommentText,mCommentCounter,albumMoreThan4_text;
        ImageView mFeedPicture,mLikeIcon,mCommentIcon,album2_1,album2_2,album3_1,album3_2,album3_3,album4_1,album4_2,album4_3,album4_4,albumMoreThan4_1,albumMoreThan4_2,albumMoreThan4_3,albumMoreThan4_4;
        LinearLayout mFeedLikeButton,mFeedCommentButton,album2,album3,album4,albumMoreThan4;
        LinearLayout feed_text_box,layout_job_description;
        RelativeLayout profileBar;
        Button mButtonMoreFeed;
        HtmlTextView mFeedTitle,job_description;

        private void init(View view){
            feedCard = (CardView)view.findViewById(R.id.feedCard);
            mProfilePicture = (CircleImageView)view.findViewById(R.id.mProfilePicture);
            mFeedStoryTitle = (TextView) view.findViewById(R.id.mFeedStoryTitle);
            mFeedStoryTime = (TextView) view.findViewById(R.id.mFeedStoryTime);
            mButtonMoreFeed = (Button) view.findViewById(R.id.mButtonMoreFeed);
            mFeedPicture = (ImageView) view.findViewById(R.id.mFeedPicture);
            mFeedTitle = (HtmlTextView) view.findViewById(R.id.mFeedTitle);
            job_description = (HtmlTextView) view.findViewById(R.id.job_description);
            feed_text_box = (LinearLayout)view.findViewById(R.id.feed_text_box);
            layout_job_description = (LinearLayout)view.findViewById(R.id.layout_job_description);
            profileBar = (RelativeLayout) view.findViewById(R.id.profileBar);
            /*Album More 4*/
            albumMoreThan4_text = (TextView)view.findViewById(R.id.albumMoreThan4_text);
            albumMoreThan4 = (LinearLayout)view.findViewById(R.id.albumMoreThan4);
            albumMoreThan4_1 = (ImageView)view.findViewById(R.id.albumMoreThan4_1);
            albumMoreThan4_2 = (ImageView)view.findViewById(R.id.albumMoreThan4_2);
            albumMoreThan4_3 = (ImageView)view.findViewById(R.id.albumMoreThan4_3);
            albumMoreThan4_4 = (ImageView)view.findViewById(R.id.albumMoreThan4_4);
            /*Album 4*/
            album4 = (LinearLayout)view.findViewById(R.id.album4);
            album4_1 = (ImageView)view.findViewById(R.id.album4_1);
            album4_2 = (ImageView)view.findViewById(R.id.album4_2);
            album4_3 = (ImageView)view.findViewById(R.id.album4_3);
            album4_4 = (ImageView)view.findViewById(R.id.album4_4);
            /*Album 3*/
            album3 = (LinearLayout)view.findViewById(R.id.album3);
            album3_1 = (ImageView)view.findViewById(R.id.album3_1);
            album3_2 = (ImageView)view.findViewById(R.id.album3_2);
            album3_3 = (ImageView)view.findViewById(R.id.album3_3);
            /*Album 2*/
            album2 = (LinearLayout)view.findViewById(R.id.album2);
            album2_1 = (ImageView)view.findViewById(R.id.album2_1);
            album2_2 = (ImageView)view.findViewById(R.id.album2_2);

            //*Like Button*//
            mFeedLikeButton = (LinearLayout) view.findViewById(R.id.mFeedLikeButton);
            mLikeIcon = (ImageView)view.findViewById(R.id.likeIcon);
            mLikeText = (TextView)view.findViewById(R.id.likeText);
            mLikeCounter = (TextView)view.findViewById(R.id.likeCounter);
            //*Like Button*//
            mFeedCommentButton = (LinearLayout) view.findViewById(R.id.mFeedCommentButton);
            mCommentIcon = (ImageView)view.findViewById(R.id.commentIcon);
            mCommentText = (TextView)view.findViewById(R.id.commentText);
            mCommentCounter = (TextView)view.findViewById(R.id.commentCounter);
        }

        private void likePost(){
            if (authUser==null){
                Intent loginIntent = new Intent(context, LoginActivity.class);
                context.startActivity(loginIntent);
            }
            AsyncHttpClient mLikeFeedClient = new AsyncHttpClient();
            RequestParams mLikeFeedParam = new RequestParams();
            mLikeFeedParam.put("feed_id",feed.getId());
            mLikeFeedParam.put("user_id",authUser.getId());
            String url = "";
            if (like.isIs_like()){
                url = SuperUtil.getBaseUrl(SuperConstance.UNLIKE_FEED);
            }else {
                url = SuperUtil.getBaseUrl(SuperConstance.LIKE_FEED);
            }
            mLikeFeedClient.post(url, mLikeFeedParam, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                    Log.w("like response",rawJsonResponse);
                    SmTagFeed.Like mLikeClick = new Gson().fromJson(rawJsonResponse, SmTagFeed.Like.class);
                    Log.w("like",mLikeClick.getCount_likes()+"");
                    like.setIs_like(mLikeClick.isIs_like());
                    like.setCount_likes(mLikeClick.getCount_likes());
                    mLikeCounter.setVisibility(View.VISIBLE);
                    mLikeCounter.setText(String.valueOf(like.getCount_likes()));
                    if (mLikeClick.isIs_like()){
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                        mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_blue);
                    }else {
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIcon));
                        mLikeText.setTextColor(context.getColor(R.color.colorIcon));
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_black);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                    Log.w("like response",rawJsonData);
                }

                @Override
                protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                    return null;
                }
            });
        }

        private void viewComment(){
            Intent commentIntent = new Intent(context, CommentActivity.class);
            commentIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
            commentIntent.putExtra("count_likes",like.getCount_likes());
            commentIntent.putExtra("is_like",like.isIs_like());
            commentIntent.putExtra("count_comments",comment.getCount_comments());
            commentIntent.putExtra("feed_id",feed.getId());
            context.startActivity(commentIntent);
        }

        private void viewPost(){
            String searchTitle = "";
            if (tagusnowFeed.getOriginalHastTagText()==null || tagusnowFeed.getOriginalHastTagText().equals("")){
                searchTitle = "";
            }else{
                searchTitle = mFeedTitle.getText().toString();
            }
            Intent mViewPostIntent = new Intent(context, ViewPostActivity.class);
            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
            mViewPostIntent.putExtra("postUser",new Gson().toJson(postUser));
            mViewPostIntent.putExtra("postComment",new Gson().toJson(comment));
            mViewPostIntent.putExtra("postLike",new Gson().toJson(like));
            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(facebookFeed));
            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(tagusnowFeed));
            mViewPostIntent.putExtra("SmTagSubCategory",new Gson().toJson(category));
            mViewPostIntent.putExtra("Feed",new Gson().toJson(feed));
            mViewPostIntent.putExtra("searchTitle",searchTitle);
            context.startActivity(mViewPostIntent);
        }

        private void viewPostDetail(){
            Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class);
            mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
            mViewPostIntent.putExtra("postUser",new Gson().toJson(postUser));
            mViewPostIntent.putExtra("postComment",new Gson().toJson(comment));
            mViewPostIntent.putExtra("postLike",new Gson().toJson(like));
            mViewPostIntent.putExtra("FacebookFeed",new Gson().toJson(facebookFeed));
            mViewPostIntent.putExtra("TagusnowFeed",new Gson().toJson(tagusnowFeed));
            mViewPostIntent.putExtra("Feed",new Gson().toJson(feed));
            context.startActivity(mViewPostIntent);
        }

        private void showMore(){
            int menu = 0;
            if (authUser.getId()==postUser.getId()){
                menu = R.menu.menu_post_option_auth;
            }else{
                menu = R.menu.menu_post_option_not_auth;
            }
            SheetMenu.with(context)
                    .setAutoCancel(true)
                    .setMenu(menu)
                    .setClick(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            switch (menuItem.getItemId()){
                                case R.id.optionSavePost :
                                    /*code*/
                                    savePost();
                                    break;
                                case R.id.optionEditPost :
                                    editPost();
                                    break;
                                case R.id.optionHide :
                                    hidePost();
                                    break;
                                case R.id.optionDelete :
                                    removePost();
                                    break;
                            }
                            return false;
                        }
                    }).showIcons(true).show();
        }

        public ViewHolder(View itemView) {
            super(itemView);
            this.init(itemView);
            this.context = itemView.getContext();
        }

        public ViewHolder setIntent(Intent intent) {
            this.intent = intent;
            return this;
        }

        public ViewHolder setFeed(SmTagFeed.Feed.Data feed) {
            this.feed = feed;
            return this;
        }

        public ViewHolder setAuthUser(SmUser authUser) {
            this.authUser = authUser;
            return this;
        }

        public ViewHolder setPostUser(SmUser postUser) {
            this.postUser = postUser;
            return this;
        }

        public ViewHolder setComment(SmTagFeed.Comment comment) {
            this.comment = comment;
            return this;
        }

        public ViewHolder setLike(SmTagFeed.Like like) {
            this.like = like;
            return this;
        }

        public ViewHolder setTagusnowFeed(TagusnowFeed tagusnowFeed) {
            this.tagusnowFeed = tagusnowFeed;
            return this;
        }

        public ViewHolder setCategory(SmTagSubCategory category) {
            this.category = category;
            return this;
        }

        private void removePost(){}

        private void hidePost(){}

        private void editPrivacy(){}

        private void editPost(){
            Intent mPostIntent = new Intent(context, CreatePostActivity.class);
            mPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
            mPostIntent.putExtra("postUser",new Gson().toJson(postUser));
            mPostIntent.putExtra("postComment",new Gson().toJson(comment));
            mPostIntent.putExtra("postLike",new Gson().toJson(like));
            mPostIntent.putExtra("FacebookFeed",new Gson().toJson(facebookFeed));
            mPostIntent.putExtra("TagusnowFeed",new Gson().toJson(tagusnowFeed));
            mPostIntent.putExtra("Feed",new Gson().toJson(feed));
            mPostIntent.putExtra("SmTagSubCategory",new Gson().toJson(category));
            context.startActivity(mPostIntent);
        }

        private void savePost(){}

        @TargetApi(Build.VERSION_CODES.M)
        private void bindView(){

            mFeedPicture.setVisibility(View.GONE);
            album2.setVisibility(View.GONE);
            album3.setVisibility(View.GONE);
            album4.setVisibility(View.GONE);
            albumMoreThan4.setVisibility(View.GONE);
            layout_job_description.setVisibility(View.GONE);

            mFeedStoryTitle.setText(postUser.getUser_name());
            if (postUser.getImage().contains("http")){
                Glide.with(context)
                        .load(postUser.getImage())
                        .error(android.R.drawable.ic_menu_report_image)
                        .centerCrop()
                        .override(100,100)
                        .into(mProfilePicture);
            }else{
                Glide.with(context)
                        .load(SuperUtil.getProfilePicture(postUser.getImage(),"200"))
                        .error(android.R.drawable.ic_menu_report_image)
                        .centerCrop()
                        .override(100,100)
                        .into(mProfilePicture);
            }

            if (feed.getType().equals(SuperConstance.TAGUSNOW_POST)){
                if (tagusnowFeed.getType().equals(SuperConstance.NORMAL_POST)){
                    layout_job_description.setVisibility(View.GONE);
                    mFeedStoryTime.setText(tagusnowFeed.getTimeline());
                    if (tagusnowFeed.getOriginalHastTagText()==null || tagusnowFeed.getOriginalHastTagText().equals("")){
                        feed_text_box.setVisibility(View.GONE);
                    }else{
                        feed_text_box.setVisibility(View.VISIBLE);
                        mFeedTitle.setHtml(tagusnowFeed.getHashtag_text());
                        mFeedTitle.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                viewPostDetail();
                            }
                        });
                    }

                    if (like.isIs_like()){
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_blue);
                        mLikeText.setTextColor(context.getColor(R.color.colorAccent));
                        mLikeCounter.setTextColor(context.getColor(R.color.colorAccent));
                    }else{
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_black);
                        mLikeText.setTextColor(context.getColor(R.color.colorIcon));
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIcon));
                    }

                    if (like.getCount_likes() > 0){
                        mLikeCounter.setVisibility(View.VISIBLE);
                        mLikeCounter.setText(like.getCount_likes()+"");
                    }else {
                        mLikeCounter.setVisibility(View.GONE);
                    }

                    if (comment.getCount_comments() > 0){
                        mCommentCounter.setVisibility(View.VISIBLE);
                        mCommentCounter.setText(comment.getCount_comments()+"");
                    }else{
                        mCommentCounter.setVisibility(View.GONE);
                    }

                    if (tagusnowFeed.getAlbum().size()==1 && (tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc()==null || tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc().equals(""))){
                        mFeedPicture.setVisibility(View.GONE);
                    }else{
                        if (tagusnowFeed.getAlbum().size()==1){
                            mFeedPicture.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(mFeedPicture);
                        }else if(tagusnowFeed.getAlbum().size()==2){
                            album2.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album2_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album2_2);
                        }else if(tagusnowFeed.getAlbum().size()==3){
                            album3.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album3_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album3_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album3_3);
                        }else if(tagusnowFeed.getAlbum().size()==4){
                            album4.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album4_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album4_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album4_3);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(3).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album4_4);
                        }else if(tagusnowFeed.getAlbum().size() > 4){
                            albumMoreThan4.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(albumMoreThan4_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(albumMoreThan4_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(albumMoreThan4_3);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(3).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(albumMoreThan4_4);
                            albumMoreThan4_text.setText(context.getString(R.string.view_all_image,tagusnowFeed.getAlbum().size()));
                        }
                    }
                }else {
                    layout_job_description.setVisibility(View.VISIBLE);
                    /*Job Description*/
                    job_description.setHtml(tagusnowFeed.getJobDescription());

                    mFeedStoryTime.setText(tagusnowFeed.getTimeline());
                    if (tagusnowFeed.getOriginalHastTagText()==null || tagusnowFeed.getOriginalHastTagText().equals("")){
                        feed_text_box.setVisibility(View.GONE);
                    }else{
                        feed_text_box.setVisibility(View.VISIBLE);
                        mFeedTitle.setHtml(tagusnowFeed.getHashtag_text());
                        mFeedTitle.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                viewPostDetail();
                            }
                        });
                    }

                    if (like.isIs_like()){
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_blue);
                        mLikeText.setTextColor(context.getColor(R.color.colorAccent));
                        mLikeCounter.setTextColor(context.getColor(R.color.colorAccent));
                    }else{
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_black);
                        mLikeText.setTextColor(context.getColor(R.color.colorIcon));
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIcon));
                    }

                    if (like.getCount_likes() > 0){
                        mLikeCounter.setVisibility(View.VISIBLE);
                        mLikeCounter.setText(like.getCount_likes()+"");
                    }else {
                        mLikeCounter.setVisibility(View.GONE);
                    }

                    if (comment.getCount_comments() > 0){
                        mCommentCounter.setVisibility(View.VISIBLE);
                        mCommentCounter.setText(comment.getCount_comments()+"");
                    }else{
                        mCommentCounter.setVisibility(View.GONE);
                    }

                    if (tagusnowFeed.getAlbum().size()==1 && (tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc()==null || tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc().equals(""))){
                        mFeedPicture.setVisibility(View.GONE);
                    }else{
                        if (tagusnowFeed.getAlbum().size()==1){
                            mFeedPicture.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(mFeedPicture);
                        }else if(tagusnowFeed.getAlbum().size()==2){
                            album2.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album2_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album2_2);
                        }else if(tagusnowFeed.getAlbum().size()==3){
                            album3.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album3_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album3_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album3_3);
                        }else if(tagusnowFeed.getAlbum().size()==4){
                            album4.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album4_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album4_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album4_3);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(3).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(album4_4);
                        }else if(tagusnowFeed.getAlbum().size() > 4){
                            albumMoreThan4.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(albumMoreThan4_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(1).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(albumMoreThan4_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(2).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(albumMoreThan4_3);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(tagusnowFeed.getAlbum().get(3).getMedia().getImage().getSrc(),"500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(albumMoreThan4_4);
                            albumMoreThan4_text.setText(context.getString(R.string.view_all_image,tagusnowFeed.getAlbum().size()));
                        }
                    }
                }
            }else if(feed.getType().equals(SuperConstance.POST_JOB)){

            }

            mFeedCommentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewComment();
                }
            });

            mFeedLikeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    likePost();
                }
            });

            mButtonMoreFeed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showMore();
                }
            });

            mFeedPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPostDetail();
                }
            });
            album2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPostDetail();
                }
            });
            album3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPostDetail();
                }
            });
            album4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPostDetail();
                }
            });
            albumMoreThan4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPostDetail();
                }
            });

            profileBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        viewPost();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            job_description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPostDetail();
                }
            });
        }
    }
}

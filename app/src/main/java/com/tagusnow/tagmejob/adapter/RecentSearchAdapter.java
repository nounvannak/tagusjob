package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearchHistory;
import com.tagusnow.tagmejob.feed.RecentSearchHistoryPaginate;
import com.tagusnow.tagmejob.view.Holder.Search.RecentSearchHolder;
import com.tagusnow.tagmejob.view.Search.RecentSearchCard;

import java.util.List;

public class RecentSearchAdapter extends RecyclerView.Adapter{

    private Activity activity;
    private Context context;
    private SmUser auth;
    private RecentSearchHistoryPaginate recentSearchHistoryPaginate;
    private List<RecentSearchHistory> recentSearchHistories;

    public RecentSearchAdapter(Activity activity,RecentSearchHistoryPaginate recentSearchHistoryPaginate,SmUser auth){
        this.activity = activity;
        this.context = activity;
        this.auth = auth;
        this.recentSearchHistoryPaginate = recentSearchHistoryPaginate;
        this.recentSearchHistories = recentSearchHistoryPaginate.getData();
    }

    public RecentSearchAdapter update(Activity activity,RecentSearchHistoryPaginate recentSearchHistoryPaginate,SmUser auth){
        this.activity = activity;
        this.context = activity;
        this.auth = auth;
        this.recentSearchHistoryPaginate = recentSearchHistoryPaginate;
        this.recentSearchHistories.addAll(recentSearchHistoryPaginate.getData());
        return this;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecentSearchHolder(new RecentSearchCard(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((RecentSearchHolder) holder).bindView(this.activity,this.auth,this.recentSearchHistories.get(position).getTimeline(),this.recentSearchHistories.get(position).getHistory());
    }

    @Override
    public int getItemCount() {
        return this.recentSearchHistoryPaginate.getTo();
    }
}

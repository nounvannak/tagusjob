package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.view.Holder.Search.RecentSearchCardHolder;
import com.tagusnow.tagmejob.view.Holder.Search.RecentSearchCardLoadMoreHolder;
import com.tagusnow.tagmejob.view.Search.RecentSearchLoadMore;
import com.tagusnow.tagmejob.view.Search.RecentSearchesFace;
import java.util.List;

public class RecentSearchCardAdapter extends RecyclerView.Adapter{

    private Activity activity;
    private Context context;
    private RecentSearchPaginate recentSearchPaginate;
    private List<RecentSearch> recentSearches;
    private SmUser auth;
    public static final int FOOTER = 1;
    private static final int LOAD_MORE = 0;
    private static final int BODY = 1;

    public RecentSearchCardAdapter(Activity activity,RecentSearchPaginate recentSearchPaginate,SmUser auth){
        this.activity = activity;
        this.recentSearchPaginate = recentSearchPaginate;
        this.recentSearches = recentSearchPaginate.getData();
        this.context = activity.getApplicationContext();
        this.auth = auth;
    }

    public RecentSearchCardAdapter update(Activity activity,RecentSearchPaginate recentSearchPaginate){
        this.activity = activity;
        this.recentSearchPaginate = recentSearchPaginate;
        this.recentSearches.addAll(recentSearchPaginate.getData());
        this.context = activity.getApplicationContext();
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = -1;
        if (this.recentSearchPaginate.getTotal() > this.recentSearchPaginate.getTo()){
            if ((this.recentSearchPaginate.getTo() - 1)==position){
                viewType = LOAD_MORE;
            }else {
                viewType = BODY;
            }
        }else{
            viewType = LOAD_MORE;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==BODY){
            return new RecentSearchCardHolder(new RecentSearchesFace(this.context));
        }else{
            return new RecentSearchCardLoadMoreHolder(new RecentSearchLoadMore(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==BODY){
            ((RecentSearchCardHolder) holder).bindView(this.activity,this.auth,this.recentSearches.get(position));
        }else{
            ((RecentSearchCardLoadMoreHolder) holder).bindView(this.activity,this.recentSearchPaginate,this,this.auth);
        }
    }

    @Override
    public int getItemCount() {
        return this.recentSearchPaginate.getTo();
    }
}

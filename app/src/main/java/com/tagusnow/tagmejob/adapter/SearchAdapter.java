package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJob;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.v3.search.OnItemListener;
import com.tagusnow.tagmejob.v3.search.RecentResume;
import com.tagusnow.tagmejob.v3.search.RecentResumeHolder;
import com.tagusnow.tagmejob.v3.search.RecentText;
import com.tagusnow.tagmejob.v3.search.RecentTextHolder;
import com.tagusnow.tagmejob.v3.search.RecentUser;
import com.tagusnow.tagmejob.v3.search.RecentUserHolder;
import com.tagusnow.tagmejob.v3.search.SearchForListener;
import com.tagusnow.tagmejob.v3.search.SearchSuggestRecentHeaderListener;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.v3.search.result.SearchForCompany;
import com.tagusnow.tagmejob.v3.search.result.SearchForCompanyHolder;
import com.tagusnow.tagmejob.v3.search.result.SearchForContent;
import com.tagusnow.tagmejob.v3.search.result.SearchForContentHolder;
import com.tagusnow.tagmejob.v3.search.result.SearchForJob;
import com.tagusnow.tagmejob.v3.search.result.SearchForJobHolder;
import com.tagusnow.tagmejob.v3.search.result.SearchForPeople;
import com.tagusnow.tagmejob.v3.search.result.SearchForPeopleHolder;
import com.tagusnow.tagmejob.v3.search.result.SearchForStaff;
import com.tagusnow.tagmejob.v3.search.result.SearchForStaffHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Suggest.RecentHeaderHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Search.Main.Holder.JobsSuggestionLayoutHolder;
import com.tagusnow.tagmejob.view.Search.Main.Holder.SearchForHolder;
import com.tagusnow.tagmejob.view.Search.Main.Holder.StaffSuggestionLayoutHolder;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.JobsSuggestionLayout;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.SearchForLayout;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.StaffSuggestionLayout;
import com.tagusnow.tagmejob.view.Search.UI.Face.Normal.SearchUIFaceNormal;
import com.tagusnow.tagmejob.view.Search.UI.Face.Normal.SearchUIFeedEmbedURL;
import com.tagusnow.tagmejob.view.Search.UI.Holder.Normal.SearchUIFaceNormalHolder;
import com.tagusnow.tagmejob.view.Search.UI.Holder.Normal.SearchUIFeedEmbedURLHolder;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestRecentHeader;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter {

    private static final int SEARCH_FOR = 0;
    private static final int JOB_SUGGEST = 1;
    private static final int STAFF_SUGGEST = 2;
    private static final int HEADER_RECENT = 3;
    private static final int RECENT_NORMAL = 4;
    private static final int RECENT_EMBED = 15;
    private static final int RECENT_JOB = 5;
    private static final int RECENT_STAFF = 6;
    private static final int RECENT_USER = 7;
    private static final int RECENT_TEXT = 8;
    private static final int RESULT_PEOPLE = 9;
    private static final int RESULT_COMPANY = 10;
    private static final int RESULT_JOB = 11;
    private static final int RESULT_STAFF = 12;
    private static final int RESULT_CONTENT = 13;
    private static final int LOADING = 14;

    private Activity activity;
    private SmUser Auth;
    private List<RecentSearch> recentSearches;
    private boolean isRecent;
    private int recent = 5;
    private int search = 5;

    private SearchForLayout searchForLayout;
    private JobsSuggestionLayout jobsSuggestionLayout;
    private StaffSuggestionLayout staffSuggestionLayout;
    private SuggestionListener suggestionListener;
    private SearchSuggestRecentHeaderListener searchSuggestRecentHeaderListener;

    private SearchForPeople searchForPeople;
    private SearchForCompany searchForCompany;
    private SearchForJob searchForJob;
    private SearchForStaff searchForStaff;
    private SearchForListener searchForListener;
    private FeedNormalListener feedNormalListener;
    private GridViewListener gridViewListener;
    private SmallFeedJobListener smallFeedJobListener;
    private OnItemListener onItemListener;
    private SearchForContent searchForContent;

    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public SearchAdapter(Activity activity, SmUser auth,SuggestionListener suggestionListener, SearchSuggestRecentHeaderListener searchSuggestRecentHeaderListener, SearchForListener searchForListener, FeedNormalListener feedNormalListener, GridViewListener gridViewListener, SmallFeedJobListener smallFeedJobListener, OnItemListener onItemListener,LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        Auth = auth;

        this.suggestionListener = suggestionListener;
        this.searchSuggestRecentHeaderListener = searchSuggestRecentHeaderListener;
        this.searchForListener = searchForListener;
        this.feedNormalListener = feedNormalListener;
        this.gridViewListener = gridViewListener;
        this.smallFeedJobListener = smallFeedJobListener;
        this.onItemListener = onItemListener;
        this.loadingListener = loadingListener;

        searchForLayout = new SearchForLayout(activity);
        jobsSuggestionLayout = new JobsSuggestionLayout(activity);
        staffSuggestionLayout = new StaffSuggestionLayout(activity);

        searchForPeople = new SearchForPeople(activity);
        searchForJob = new SearchForJob(activity);
        searchForStaff = new SearchForStaff(activity);
        searchForCompany = new SearchForCompany(activity);
        searchForContent = new SearchForContent(activity);

        loadingView = new LoadingView(activity);
    }

    public void isRecent(boolean isRecent){
        this.isRecent = isRecent;
    }

    public void NewData(List<RecentSearch> recentSearches){
        this.recentSearches = recentSearches;
    }

    public void NextData(List<RecentSearch> recentSearches){
        this.recentSearches.addAll(recentSearches);
        notifyDataSetChanged();
    }

    public SearchForPeople getSearchForPeople() {
        return searchForPeople;
    }

    public SearchForCompany getSearchForCompany() {
        return searchForCompany;
    }

    public SearchForJob getSearchForJob() {
        return searchForJob;
    }

    public SearchForStaff getSearchForStaff() {
        return searchForStaff;
    }

    public SearchForContent getSearchForContent() {
        return searchForContent;
    }

    public JobsSuggestionLayout getJobsSuggestionLayout() {
        return jobsSuggestionLayout;
    }

    public SearchForLayout getSearchForLayout() {
        return searchForLayout;
    }

    public StaffSuggestionLayout getStaffSuggestionLayout() {
        return staffSuggestionLayout;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (isRecent){
            if (position == 0){
                viewType = SEARCH_FOR;
            }else if (position == 1){
                viewType = JOB_SUGGEST;
            }else if (position == 2){
                viewType = STAFF_SUGGEST;
            }else if (position == 3){
                viewType = HEADER_RECENT;
            }else if (position == (getItemCount() - 1)){
                viewType = LOADING;
            }else {
                if (recentSearches.get(position - (recent - 1)).getSearch_type() == RecentSearch.USER){
                    viewType = RECENT_USER;
                }else if (recentSearches.get(position - (recent - 1)).getSearch_type() == RecentSearch.WORD){
                    viewType = RECENT_TEXT;
                }else {
                    if (recentSearches.get(position - (recent - 1)).getFeed() != null){
                        if (recentSearches.get(position - (recent - 1)).getFeed().getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)){
                            if (recentSearches.get(position - (recent - 1)).getFeed().getFeed().getType_link() != null && !recentSearches.get(position - (recent - 1)).getFeed().getFeed().getType_link().equals("")){
                                viewType = RECENT_EMBED;
                            }else {
                                viewType = RECENT_NORMAL;
                            }
                        }else if (recentSearches.get(position - (recent - 1)).getFeed().getFeed().getFeed_type().equals(FeedDetail.JOB_POST)){
                            viewType = RECENT_JOB;
                        }else {
                            viewType = RECENT_STAFF;
                        }
                    }else {
                        viewType = RECENT_TEXT;
                    }
                }
            }
        }else {
            if (position == 0){
                viewType = RESULT_PEOPLE;
            }else if (position == 1){
                viewType = RESULT_COMPANY;
            }else if (position == 2){
                viewType = RESULT_JOB;
            }else if (position == 3){
                viewType = RESULT_STAFF;
            }else {
                viewType = RESULT_CONTENT;
            }
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == SEARCH_FOR){
            return new SearchForHolder(searchForLayout);
        }else if (viewType == JOB_SUGGEST){
            return new JobsSuggestionLayoutHolder(jobsSuggestionLayout);
        }else if (viewType == STAFF_SUGGEST){
            return new StaffSuggestionLayoutHolder(staffSuggestionLayout);
        }else if (viewType == HEADER_RECENT){
            return new RecentHeaderHolder(new SearchSuggestRecentHeader(activity));
        }else if (viewType == RECENT_NORMAL){
            return new SearchUIFaceNormalHolder(new SearchUIFaceNormal(activity));
        }else if (viewType == RECENT_EMBED){
            return new SearchUIFeedEmbedURLHolder(new SearchUIFeedEmbedURL(activity));
        }else if (viewType == RECENT_JOB){
            return new SmallFeedJobHolder(new SmallFeedJob(activity));
        }else if (viewType == RECENT_STAFF){
            return new RecentResumeHolder(new RecentResume(activity));
        }else if (viewType == RECENT_USER){
            return new RecentUserHolder(new RecentUser(activity));
        }else if (viewType == RECENT_TEXT) {
            return new RecentTextHolder(new RecentText(activity));
        }else if (viewType == RESULT_PEOPLE){
            return new SearchForPeopleHolder(searchForPeople);
        }else if (viewType == RESULT_COMPANY){
            return  new SearchForCompanyHolder(searchForCompany);
        }else if (viewType == RESULT_JOB){
            return new SearchForJobHolder(searchForJob);
        }else if (viewType == RESULT_STAFF){
            return new SearchForStaffHolder(searchForStaff);
        }else if (viewType == RESULT_CONTENT){
            return new SearchForContentHolder(searchForContent);
        }else {
            return new LoadingHolder(loadingView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == SEARCH_FOR){
            ((SearchForHolder) holder).BindView(activity,Auth,searchForListener);
        }else if (getItemViewType(position) == JOB_SUGGEST){
            ((JobsSuggestionLayoutHolder) holder).BindView(activity,suggestionListener);
        }else if (getItemViewType(position) == STAFF_SUGGEST){
            ((StaffSuggestionLayoutHolder) holder).BindView(activity,suggestionListener);
        }else if (getItemViewType(position) == HEADER_RECENT){
            ((RecentHeaderHolder) holder).bindView(activity,searchSuggestRecentHeaderListener);
        }else if (getItemViewType(position) == RECENT_NORMAL){
            ((SearchUIFaceNormalHolder) holder).bindView(activity,recentSearches.get(position - (recent - 1)).getFeed(),feedNormalListener,gridViewListener);
        }else if (getItemViewType(position) == RECENT_EMBED){
            ((SearchUIFeedEmbedURLHolder) holder).BindView(activity,recentSearches.get(position - (recent - 1)).getFeed(),feedNormalListener);
        }else if (getItemViewType(position) == RECENT_JOB){
            ((SmallFeedJobHolder) holder).BindView(activity,recentSearches.get(position - (recent - 1)).getFeed(),smallFeedJobListener);
        }else if (getItemViewType(position) == RECENT_STAFF) {
            ((RecentResumeHolder) holder).BindView(activity,recentSearches.get(position - (recent - 1)),onItemListener);
        }else if (getItemViewType(position) == RECENT_USER){
            ((RecentUserHolder) holder).BindView(activity,recentSearches.get(position - (recent - 1)),onItemListener);
        }else if (getItemViewType(position) == RECENT_TEXT) {
            ((RecentTextHolder) holder).BindView(activity, recentSearches.get(position - (recent - 1)), onItemListener);
        }else if (getItemViewType(position) == RESULT_PEOPLE){
            ((SearchForPeopleHolder) holder).BindView(activity,searchForListener);
        }else if (getItemViewType(position) == RESULT_COMPANY){
            ((SearchForCompanyHolder) holder).BindView(activity,searchForListener);
        }else if (getItemViewType(position) == RESULT_JOB){
            ((SearchForJobHolder) holder).BindView(activity,searchForListener);
        }else if (getItemViewType(position) == RESULT_STAFF){
            ((SearchForStaffHolder) holder).BindView(activity,searchForListener);
        }else if (getItemViewType(position) == RESULT_CONTENT){
            ((SearchForContentHolder) holder).BindView(activity,searchForListener);
        }else {
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }
    }

    @Override
    public int getItemCount() {
        return isRecent ? (recentSearches.size() + recent) : search;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.Chat.Holder.ChatUserUIHolder;
import com.tagusnow.tagmejob.view.Chat.Holder.UserUIHolder;
import com.tagusnow.tagmejob.view.Chat.UI.ChatUserUI;
import com.tagusnow.tagmejob.view.Chat.UI.UserUI;

import java.util.List;

public class SearchChatAdatper extends RecyclerView.Adapter {

    public static final int RECENT_CHAT = 0;
    public static final int RESULT = 1;
    private static final int RECENT = 1;
    private static final int CHAT = 2;
    private Activity activity;
    private Context context;
    private UserPaginate paginate;
    private List<SmUser> users;
    private SmUser user;
    private int type;

    public SearchChatAdatper(Activity activity,UserPaginate paginate,SmUser user,int type){
        this.activity = activity;
        this.context = activity;
        this.paginate = paginate;
        this.users = paginate.getData();
        this.user = user;
        this.type = type;
    }

    public SearchChatAdatper Update(UserPaginate paginate,int type){
        this.paginate = paginate;
        this.users.addAll(paginate.getData());
        this.type = type;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (this.type==RECENT_CHAT){
            viewType = RECENT;
        }else {
            viewType = CHAT;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==RECENT){
            return new UserUIHolder(new UserUI(this.context));
        }else {
            return new ChatUserUIHolder(new ChatUserUI(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==RECENT){
            ((UserUIHolder) holder).bindView(this.activity,this.users.get(position),this);
        }else {
            ((ChatUserUIHolder) holder).bindView(this.activity,this.user,this.users.get(position),this);
        }
    }

    @Override
    public int getItemCount() {
        return paginate.getTo();
    }
}

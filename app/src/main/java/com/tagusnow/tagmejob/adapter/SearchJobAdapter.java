package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.view.Holder.Search.Main.SearchFaceHolder;
import com.tagusnow.tagmejob.view.Search.Main.SearchFace;
import com.tagusnow.tagmejob.view.SearchJobsFace;
import java.util.List;

public class SearchJobAdapter extends RecyclerView.Adapter{

    public static final int MORE_VIEW = 0;
    public static final int RESULT = 0;
    public static final int RECENT = 1;
    private static final int RESULT_SEARCH = 2;
    private int type;
    private Activity activity;
    private Context context;
    private SmUser user;
    private FeedResponse feedResponse;
    private List<Feed> feedList;
    private RecentSearchPaginate recentSearchPaginate;
    private List<RecentSearch> recentSearchList;
    private SearchJobsFace jobsFace;

    public SearchJobAdapter(Activity activity,FeedResponse feedResponse,SmUser user,int type){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feedList = feedResponse.getData();
        this.user = user;
        this.jobsFace = new SearchJobsFace(this.context);
        this.type = type;
    }

    public SearchJobAdapter update(Activity activity,FeedResponse feedResponse,SmUser user,int type){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feedList.addAll(feedResponse.getData());
        this.user = user;
        this.type = type;
        return this;
    }

    public SearchJobAdapter(Activity activity,RecentSearchPaginate recentSearchPaginate,SmUser user,int type){
        this.activity = activity;
        this.context = activity;
        this.recentSearchPaginate = recentSearchPaginate;
        this.recentSearchList = recentSearchPaginate.getData();
        this.jobsFace = new SearchJobsFace(this.context);
        this.user = user;
        this.type = type;
    }

    public SearchJobAdapter update(Activity activity,RecentSearchPaginate recentSearchPaginate,SmUser user,int type){
        this.activity = activity;
        this.context = activity;
        this.recentSearchPaginate = recentSearchPaginate;
        this.recentSearchList.addAll(recentSearchPaginate.getData());
        this.user = user;
        this.type = type;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 2;
        if (this.type==RECENT){
            type = RECENT;
        }else {
            type = RESULT;
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SearchFaceHolder(new SearchFace(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==RECENT){
            ((SearchFaceHolder) holder).bindView(this.activity,this.recentSearchList.get(position),this.user);
        }else {
            ((SearchFaceHolder) holder).bindView(this.activity,this.feedList.get(position),this.user);
        }
    }

    @Override
    public int getItemCount() {
        return this.type==RESULT ? this.feedResponse.getTo() : this.recentSearchPaginate.getTo();
    }

    public SearchJobsFace getJobsFace() {
        return jobsFace;
    }
}

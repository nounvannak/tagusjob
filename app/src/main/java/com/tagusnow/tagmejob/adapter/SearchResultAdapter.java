package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.view.BaseCard.Card;
import com.tagusnow.tagmejob.view.Holder.CardHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Main.ResultSearchPostFaceHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Similar.SimilarSearchFace;
import com.tagusnow.tagmejob.view.Holder.Search.Similar.SimilarSearchFaceHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Similar.SimilarSearchFooter;
import com.tagusnow.tagmejob.view.Holder.Search.Similar.SimilarSearchFooterHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Similar.SimilarSearchHeader;
import com.tagusnow.tagmejob.view.Holder.Search.Similar.SimilarSearchHeaderHolder;
import com.tagusnow.tagmejob.view.Search.Main.ResultSearchPostFace;
import java.util.List;

public class SearchResultAdapter extends RecyclerView.Adapter{

    public static final int MORE_VIEW = 3;
    private static final int MAIN_CARD = 0;
    private static final int SIMILAR_HERDER = 1;
    private static final int SIMILAR_NORMAL = 2;
    private static final int SIMILAR_JOB = 5;
    private static final int SIMILAR_FOOTER = 3;
    private static final int USER_JOB_SUGGEST = 4;
    private Activity activity;
    private Context context;
    private FeedResponse feedResponse;
    private List<Feed> feedList;
    private Feed feed;
    private SmUser user;

    public SearchResultAdapter(Activity activity,FeedResponse feedResponse,Feed feed,SmUser user){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feed = feed;
        this.feedList = feedResponse.getData();
        this.user = user;
    }

    public SearchResultAdapter update(Activity activity,FeedResponse feedResponse,Feed feed,SmUser user){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feed = feed;
        this.feedList.addAll(feedResponse.getData());
        this.user = user;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (position==0){
            viewType = MAIN_CARD;
        }else if (position==1){
            viewType = SIMILAR_HERDER;
        }else if (position==(this.feedResponse.getTo() - 1)){
            viewType = SIMILAR_FOOTER;
        }else {
//            viewType = SIMILAR_JOB;
            if (this.feedResponse.getTotal() > 0){
                if (this.feedList.get(position - (MORE_VIEW - 1)).getType().equals(Feed.TAGUSJOB)){
                    if (this.feedList.get(position - (MORE_VIEW - 1)).getFeed().getType().equals(FeedDetail.JOB_POST)){
                        viewType = SIMILAR_JOB;
                    }else {
                        viewType = SIMILAR_NORMAL;
                    }
                }
            }
        }

        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==MAIN_CARD){
            return new CardHolder(new Card(this.context));
        }else if (viewType==SIMILAR_HERDER){
            return new SimilarSearchHeaderHolder(new SimilarSearchHeader(this.context));
        }else if (viewType==SIMILAR_FOOTER){
            return new SimilarSearchFooterHolder(new SimilarSearchFooter(this.context));
        }else if (viewType==SIMILAR_NORMAL){
            return new ResultSearchPostFaceHolder(new ResultSearchPostFace(this.context));
        }else {
//            return new ResultSearchJobFaceHolder(new ResultSearchJobFace(this.context));
//            return new CardSmallHolder(new CardSmall(this.context));
            return new SimilarSearchFaceHolder(new SimilarSearchFace(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==MAIN_CARD){
            ((CardHolder) holder).Adapter(this).bindView(this.activity,this.user,this.feed.getId());
        }else if (getItemViewType(position)==SIMILAR_HERDER){
            ((SimilarSearchHeaderHolder) holder).bindView();
        }else if (getItemViewType(position)==SIMILAR_JOB){
//            ((ResultSearchJobFaceHolder) holder).bindView(this.activity,this.feedList.get(position - (MORE_VIEW - 1)),this.user,false);
//            ((CardSmallHolder) holder).bindView(this.activity,this.user,this.feedList.get(position - (MORE_VIEW - 1)).getId(),false);
            ((SimilarSearchFaceHolder) holder).bindView(this.activity,this.user,this.feedList.get(position - (MORE_VIEW - 1)).getId(),true);
        }else if (getItemViewType(position)==SIMILAR_NORMAL){
            ((ResultSearchPostFaceHolder) holder).bindView(this.activity,this.user,this.feedList.get(position - (MORE_VIEW - 1)),false);
        }else if (getItemViewType(position)==SIMILAR_FOOTER){
            ((SimilarSearchFooterHolder) holder).bindView(this.activity,this.user,this.feed);
        }
    }

    @Override
    public int getItemCount() {
        return this.feedResponse.getTo();
    }
}

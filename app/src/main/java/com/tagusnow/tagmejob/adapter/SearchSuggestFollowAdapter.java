package com.tagusnow.tagmejob.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.view.Holder.Search.Suggest.NoticeWordHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Suggest.RecentBodyHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Suggest.RecentHeaderHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Suggest.ResultHolder;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestRecentFace;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestRecentHeader;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestResultFace;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestWordsNotice;

import java.util.List;

public class SearchSuggestFollowAdapter extends RecyclerView.Adapter{

    private Activity activity;
    private SmUser authUser;
    private List<SmUser> userList;
    private UserPaginate userPaginate;
    private List<RecentSearch> recentSearches;
    private RecentSearchPaginate recentSearchPaginate;
    private static final int NOTICE_ONLY = 0;
    private static final int NOTICE_WITH_RESULT = -1;
    private static final int RECENT_SEARCH_HEADER = 0;
    private static final int RECENT_SEARCH_BODY = 1;
    public static final int RESULT = 1;
    public static final int RECENT = 2;
    public static final int MORE_VIEW = 1;
    private int type = 0;
    private String search_text;
    private static int superPosition = 0;

    public SearchSuggestFollowAdapter(Activity activity,UserPaginate userPaginate,String search_text){
        this.activity = activity;
        this.search_text = search_text;
        this.userPaginate = userPaginate;
        this.userList = userPaginate.getData();
        this.authUser = new Auth(activity).checkAuth().token();
    }

    public SearchSuggestFollowAdapter(Activity activity,RecentSearchPaginate recentSearchPaginate){
        this.activity = activity;
        this.recentSearchPaginate = recentSearchPaginate;
        this.recentSearches = recentSearchPaginate.getData();
        this.authUser = new Auth(activity).checkAuth().token();
    }

    public SearchSuggestFollowAdapter update(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.userList.addAll(userPaginate.getData());
        return this;
    }

    public SearchSuggestFollowAdapter update(RecentSearchPaginate recentSearchPaginate){
        this.recentSearchPaginate = recentSearchPaginate;
        this.recentSearches.addAll(recentSearchPaginate.getData());
        return this;
    }

    public SearchSuggestFollowAdapter type(int type){
        this.type = type;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        superPosition = position;
        if (this.type==RESULT){
            if (this.userPaginate.getTotal()==0){
                viewType = NOTICE_ONLY;
            }else{
                viewType = NOTICE_WITH_RESULT;
            }
        }else if(this.type==RECENT){
            if (position==RECENT_SEARCH_HEADER){
                viewType = RECENT_SEARCH_HEADER;
            }else{
                viewType = RECENT_SEARCH_BODY;
            }
        }
        return viewType;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder view = null;
        if (this.type==RESULT){
            if (viewType==NOTICE_ONLY){
                view = new NoticeWordHolder(new SearchSuggestWordsNotice(this.activity.getApplicationContext()));
            }else if(viewType==NOTICE_WITH_RESULT){
                if ((getItemCount() - 1)==superPosition){
                    view = new NoticeWordHolder(new SearchSuggestWordsNotice(this.activity.getApplicationContext()));
                }else {
                    if (this.userPaginate.getTotal() > 0) {
                        view = new ResultHolder(new SearchSuggestResultFace(this.activity.getApplicationContext()));
                    }else{
                        view = new NoticeWordHolder(new SearchSuggestWordsNotice(this.activity.getApplicationContext()));
                    }
                }
            }
        }else if(this.type==RECENT){
            if (viewType==RECENT_SEARCH_HEADER){
                view = new RecentHeaderHolder(new SearchSuggestRecentHeader(this.activity.getApplicationContext()));
            }else if(viewType==RECENT_SEARCH_BODY){
                view = new RecentBodyHolder(new SearchSuggestRecentFace(this.activity.getApplicationContext()));
            }
        }
        return view;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (this.type==RESULT){
            if (getItemViewType(position)==NOTICE_ONLY){
                ((NoticeWordHolder) holder).bindView(this.activity,this.search_text);
            }else if (getItemViewType(position)==NOTICE_WITH_RESULT){
                if ((getItemCount() - 1)==position){
                    ((NoticeWordHolder) holder).bindView(this.activity,this.search_text);
                }else {
                    if (this.userPaginate.getTotal() > 0) {
                        ((ResultHolder) holder).bindView(this.activity,this.userList.get(position));
                    }else {
                        ((NoticeWordHolder) holder).bindView(this.activity,this.search_text);
                    }
                }
            }
        }else if (this.type==RECENT){
            if (getItemViewType(position)==RECENT_SEARCH_HEADER){
                return;
//                ((RecentHeaderHolder) holder).bindView(this.activity);
            }else if(getItemViewType(position)==RECENT_SEARCH_BODY){
                ((RecentBodyHolder) holder).bindView(this.activity,this.recentSearches.get(position - MORE_VIEW));
            }
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (this.type==RESULT){
            if (this.userPaginate.getTotal()==0){
                count = 1;
            }else{
                count = this.userPaginate.getTo();
            }
        }else{
            count = this.recentSearchPaginate.getTo();
        }
        return count;
    }
}

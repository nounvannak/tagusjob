package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.Holder.NotFoundHolder;
import com.tagusnow.tagmejob.view.Holder.Search.Suggest.SearchViewNoticeHolder;
import com.tagusnow.tagmejob.view.Holder.Search.TotalResultHolder;
import com.tagusnow.tagmejob.view.Search.NotFound;
import com.tagusnow.tagmejob.view.Search.TotalResult;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchViewNoticeFace;

import java.util.List;


public class SearchViewNoticeAdapter extends RecyclerView.Adapter{

    private static final String TAG = SearchViewNoticeAdapter.class.getSimpleName();
    private Activity activity;
    private Context context;
    private SmUser auth;
    private UserPaginate userPaginate;
    private List<SmUser> userList;
    public static final int MORE_VIEW = 1;
    private static final int NO_RESULT = -1;
    private static final int RESULT = 1;
    private static final int HEADER = 0;
    private static int POSITION = -1;

    public SearchViewNoticeAdapter(Activity activity,UserPaginate userPaginate){
        this.activity = activity;
        this.context = activity;
        this.userPaginate = userPaginate;
        this.userList = userPaginate.getData();
        this.auth = new Auth(activity).checkAuth().token();
    }

    public SearchViewNoticeAdapter update(Activity activity,UserPaginate userPaginate){
        this.activity = activity;
        this.context = activity;
        this.userPaginate = userPaginate;
        this.userList.addAll(userPaginate.getData());
        this.auth = new Auth(activity).checkAuth().token();
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        POSITION = position;
        if (this.userPaginate.getTotal()==0){
            viewType = NO_RESULT;
        }else{
            if (position==0){
                viewType = HEADER;
            }else{
                viewType = RESULT;
            }
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==NO_RESULT){
            return new NotFoundHolder(new NotFound(this.context));
        }else {
            if (viewType==HEADER){
                return new TotalResultHolder(new TotalResult(this.context));
            }else {
                return new SearchViewNoticeHolder(new SearchViewNoticeFace(this.context));
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position)==NO_RESULT){
            ((NotFoundHolder) holder).bindView();
        }else {
            if (getItemViewType(position)==HEADER){
                ((TotalResultHolder) holder).bindView(this.userPaginate.getTotal());
            }else {
                ((SearchViewNoticeHolder) holder).bindView(this.activity,this.auth,this.userList.get(position - MORE_VIEW));
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.userPaginate.getTotal()==0 ? 1 : this.userPaginate.getTo();
    }
}

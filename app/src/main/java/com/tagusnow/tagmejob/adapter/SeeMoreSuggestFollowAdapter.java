package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.SeeMoreSuggestFollowActivity;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.Holder.CounterHolder;
import com.tagusnow.tagmejob.view.Holder.FollowHeaderHolder;
import com.tagusnow.tagmejob.view.Holder.FollowHolder;
import com.tagusnow.tagmejob.view.UserFollowSuggestLayout;
import com.tagusnow.tagmejob.view.Widget.CounterFollowerAndFollowingWidget;
import com.tagusnow.tagmejob.view.Widget.FollowListHeaderLayout;

import java.util.ArrayList;
import java.util.List;

public class SeeMoreSuggestFollowAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<SmUser> userList = new ArrayList<>();
    private UserPaginate userPaginate;
    private static final int COUNTER = 0;
    private static final int FOLLOW_HEADER = 1;
    private static final int FOLLOW_LIST = 2;
    private SmUser authUser;
    private static final int MORE_VIEW = 2;

    public SeeMoreSuggestFollowAdapter(Activity activity){
        this.activity = activity;
        this.authUser = new Auth(activity.getApplicationContext()).checkAuth().token();
    }

    public SeeMoreSuggestFollowAdapter with(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.userList.addAll(userPaginate.getData());
        return this;
    }

    public SeeMoreSuggestFollowAdapter setUserList(List<SmUser> userList){
        this.userList.addAll(userList);
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int type = COUNTER;
        if (position==FOLLOW_HEADER){
            type = FOLLOW_HEADER;
        }else if(position >=FOLLOW_LIST){
            type = FOLLOW_LIST;
        }else{
            type = COUNTER;
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==COUNTER){
            return new CounterHolder(new CounterFollowerAndFollowingWidget(this.activity.getApplicationContext()).with(this.activity));
        }else if(viewType==FOLLOW_HEADER){
            return new FollowHeaderHolder(new FollowListHeaderLayout(this.activity.getApplicationContext()));
        }else{
            return new FollowHolder(new UserFollowSuggestLayout(this.activity.getApplicationContext()).with(this.activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==COUNTER){
            ((CounterHolder) holder).activity(this.activity).user(this.authUser).bindView();
        }else if(getItemViewType(position)==FOLLOW_HEADER){
            ((FollowHeaderHolder) holder).bindView();
        }else{
            ((FollowHolder) holder).with(this.activity).user(this.userList.get(position - MORE_VIEW)).bindView();
        }
    }

    @Override
    public int getItemCount() {
        return this.userPaginate.getTo();
    }
}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.BaseCard.CardCV;
import com.tagusnow.tagmejob.view.BaseCard.CardJob;
import com.tagusnow.tagmejob.view.BaseCard.CardSmall;
import com.tagusnow.tagmejob.view.CV.RelatedCVFace;
import com.tagusnow.tagmejob.view.CV.RelatedCVHolder;
import com.tagusnow.tagmejob.view.Empty.EmptyJob;
import com.tagusnow.tagmejob.view.Holder.CardCVHolder;
import com.tagusnow.tagmejob.view.Holder.CardJobHolder;
import com.tagusnow.tagmejob.view.Holder.CardSmallHolder;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptyJobHolder;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.SearchByLocationHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.WidgetDisplayFeedAndPostHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Space;
import com.tagusnow.tagmejob.view.Widget.SearchByLocation;
import com.tagusnow.tagmejob.view.Widget.WidgetDisplayFeedAndPost;

import java.util.List;

public class SeeSingleTopJobsAdapter extends RecyclerView.Adapter{

    public static final int BIG = 0;
    public static final int SMALL = 1;
    public static final int MORE_VIEW = 2;
    private static final int HEADER = 0;
    private static final int BODY_SMALL = 1;
    private static final int BODY_BIG = 2;
    private static final int CV_BIG = 7;
    private static final int CV_SMALL = 8;
    private static final int LOADING = 9;
    private static final int EMPTY = 3;
    private static final int SPACE = 30;
    private static final int SEARCH_LOCATION = 4;
    private Activity activity;
    private Context context;
    private FeedResponse feedResponse;
    private List<Feed> feedList;
    private SmUser user;
    private Category category;
    private int display_type = 0;

    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public SeeSingleTopJobsAdapter(Activity activity, FeedResponse feedResponse, Category category, SmUser user,LoadingListener<LoadingView> loadingListener){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feedList = feedResponse.getData();
        this.category = category;
        this.user = user;
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public SeeSingleTopJobsAdapter update(Activity activity,FeedResponse feedResponse,Category category,SmUser user){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feedList.addAll(feedResponse.getData());
        this.category = category;
        this.user = user;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (this.feedResponse.getTotal() > 0){
            if (position==1) {
                viewType = HEADER;
            }else if (position==0) {
                viewType = SEARCH_LOCATION;
            }else {
                if (getDisplay_type()==SMALL){
                    if ((this.feedResponse.getTo() - MORE_VIEW) > (position - MORE_VIEW)) {
                        if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.JOB_POST)){
                            viewType = BODY_SMALL;
                        }else if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.STAFF_POST)){
                            viewType = CV_SMALL;
                        }else if (position == (getItemCount() - 1)){
                            viewType = LOADING;
                        }
                    }else{
                        if ((this.feedResponse.getTo())==this.feedResponse.getTotal()){
                            viewType = SPACE;
                        }else {
                            viewType = LOADING;
                        }
                    }
                }else {
                    if ((this.feedResponse.getTo() - MORE_VIEW) > (position - MORE_VIEW)){
                        if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.JOB_POST)){
                            viewType = BODY_BIG;
                        }else if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.STAFF_POST)){
                            viewType = CV_BIG;
                        }else if (position == (getItemCount() - 1)){
                            viewType = LOADING;
                        }
                    }else{
                        if ((this.feedResponse.getTo())==this.feedResponse.getTotal()){
                            viewType = SPACE;
                        }else {
                            viewType = LOADING;
                        }
                    }
                }
            }
        }else {
            if (position==1) {
                viewType = HEADER;
            }else if (position==0){
                viewType = SEARCH_LOCATION;
            }else {
                viewType = EMPTY;
            }
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==HEADER) {
            return new WidgetDisplayFeedAndPostHolder(new WidgetDisplayFeedAndPost(this.context));
        }else if (viewType==SEARCH_LOCATION){
            return new SearchByLocationHolder(new SearchByLocation(this.context));
        }else if (viewType==BODY_SMALL) {
            return new CardSmallHolder(new CardSmall(this.context));
        }else if (viewType==CV_SMALL) {
            return new RelatedCVHolder(new RelatedCVFace(this.context));
        }else if (viewType==CV_BIG) {
            return new CardCVHolder(new CardCV(this.context));
        }else if (viewType==LOADING){
            return new LoadingHolder(loadingView);
        }else if(viewType==EMPTY) {
            return new EmptyJobHolder(new EmptyJob(this.context));
        }else if (viewType==SPACE){
            return new SpaceHolder(new Space(this.context));
        }else {
            return new CardJobHolder(new CardJob(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==HEADER) {
            ((WidgetDisplayFeedAndPostHolder) holder).bindView(this.activity, this.user, this, this.category.getJob_counter());
        }else if (getItemViewType(position)==SEARCH_LOCATION){
            ((SearchByLocationHolder) holder).bindView(this.activity,this.user,this.category.getId());
        }else if (getItemViewType(position)==BODY_SMALL) {
            ((CardSmallHolder) holder).bindView(this.activity,this, this.feedList.get(position - MORE_VIEW),this.user);
        }else if (getItemViewType(position)==CV_SMALL) {
            ((RelatedCVHolder) holder).bindView(this.activity, this.feedList.get(position - MORE_VIEW));
        }else if (getItemViewType(position)==CV_BIG) {
            ((CardCVHolder) holder).bindView(this.activity,this.feedList.get(position - MORE_VIEW),this,this.user);
        }else if (getItemViewType(position)==LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else if (getItemViewType(position)==EMPTY) {
            ((EmptyJobHolder) holder).bindView();
        }else if (getItemViewType(position)==SPACE){
            ((SpaceHolder) holder).bindView();
        }else {
            ((CardJobHolder) holder).bindView(this.activity,this.feedList.get(position - MORE_VIEW),this,this.user);
        }
    }

    @Override
    public int getItemCount() {
        return this.feedResponse.getTotal() > 0 ? this.feedResponse.getTo() + 1 : 3;
    }

    private int getDisplay_type() {
        return display_type;
    }

    public void setDisplay_type(int display_type) {
        this.display_type = display_type;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

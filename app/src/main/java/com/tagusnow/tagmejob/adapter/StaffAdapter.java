package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVHolder;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.page.job.PostFunctionTwoCell;
import com.tagusnow.tagmejob.v3.page.job.PostFunctionTwoCellHolder;
import com.tagusnow.tagmejob.v3.page.job.PostFunctionTwoCellListener;
import com.tagusnow.tagmejob.v3.page.resume.CompanyFaceListener;
import com.tagusnow.tagmejob.view.Holder.Widget.WidgetTopCompanyHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Widget.WidgetTopCompany;

import java.util.List;

public class StaffAdapter extends RecyclerView.Adapter {

    private static final String TAG = StaffAdapter.class.getSimpleName();
    private static final int STAFF = 2;
    private static final int POST_WIDGET = 1;
    private static final int TOP_COMPANY = 0;
    private static final int LOADING = 3;
    private int other = 3;
    private Activity activity;
    private Context context;
    private List<Feed> feeds;
    private CompanyFaceListener companyFaceListener;
    private PostFunctionTwoCellListener postFunctionTwoCellListener;
    private FeedCVListener feedCVListener;
    private WidgetTopCompany widgetTopCompany;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public StaffAdapter(Activity activity,LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.context = activity;
        this.loadingListener = loadingListener;
        this.loadingView = new LoadingView(activity);
    }

    public void NewData(List<Feed> feeds){
        this.feeds = feeds;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
    }

    public void setPostFunctionTwoCellListener(PostFunctionTwoCellListener postFunctionTwoCellListener) {
        this.postFunctionTwoCellListener = postFunctionTwoCellListener;
    }

    public void setCompanyFaceListener(CompanyFaceListener companyFaceListener) {
        this.companyFaceListener = companyFaceListener;
    }

    public void setFeedCVListener(FeedCVListener feedCVListener) {
        this.feedCVListener = feedCVListener;
    }

    public WidgetTopCompany getWidgetTopCompany() {
        return widgetTopCompany;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if (position == 0){
            type = TOP_COMPANY;
        }else if (position == 1){
            type = POST_WIDGET;
        }else if (position == (getItemCount() - 1)){
            type = LOADING;
        }else {
            type = STAFF;
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==POST_WIDGET){
            return new PostFunctionTwoCellHolder(new PostFunctionTwoCell(context));
        }else if (viewType==TOP_COMPANY){
            widgetTopCompany = new WidgetTopCompany(context);
            return new WidgetTopCompanyHolder(widgetTopCompany);
        }else if (viewType==LOADING){
            return new LoadingHolder(loadingView);
        }else {
            return new FeedCVHolder(new FeedCV(context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==POST_WIDGET){
            ((PostFunctionTwoCellHolder) holder).BindView(activity,postFunctionTwoCellListener);
        }else if (getItemViewType(position)==TOP_COMPANY){
            ((WidgetTopCompanyHolder) holder).BindView(activity,companyFaceListener);
        }else if (getItemViewType(position)==LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else {
            ((FeedCVHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedCVListener);
        }
    }

    @Override
    public int getItemCount() {
        return feeds.size() + other;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

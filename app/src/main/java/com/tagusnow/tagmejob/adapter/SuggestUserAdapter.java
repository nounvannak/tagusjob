package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.Holder.SuggestUserHolder;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.SuggestUserActivity;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.Widget.SuggestUser;
import com.tagusnow.tagmejob.view.Holder.ConnectSuggestFaceHolder;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.Space;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestFace;

import java.util.ArrayList;
import java.util.List;

public class SuggestUserAdapter extends RecyclerView.Adapter  {

    private static final String TAG = SuggestUserAdapter.class.getSimpleName();
    public static final int MORE_VIEW = 1;
    private static final int DISPLAY_HORIZONTAL = 3;
    private static final int USER = 0;
    private static final int SPACE = 1;
    private Activity activity;
    private Context context;
    private List<SmUser> userList;
    private UserPaginate userPaginate;
    private SmUser auth;
    private SmUser[] UserList = {null,null,null};
    private ArrayList<SmUser> FollowList = new ArrayList<>();
    private int total;

    public SuggestUserAdapter(Activity activity,UserPaginate userPaginate){
        try {
            this.activity = activity;
            this.context = activity;
            this.userPaginate = userPaginate;
            this.userList = userPaginate.getData();
            this.auth = new Auth(activity).checkAuth().token();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public SuggestUserAdapter Next(Activity activity,UserPaginate userPaginate){
        this.activity = activity;
        this.context = activity;
        this.userPaginate = userPaginate;
        this.userList.addAll(userPaginate.getData());
        this.auth = new Auth(activity).checkAuth().token();
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        if (this.userPaginate.getTotal() > 0){
            if (position==(this.total() - 1)){
                return SPACE;
            }else {
                return USER;
            }
        }else {
            return SPACE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==USER){
            return new SuggestUserHolder(new SuggestUser(this.context));
        }else {
            return new SpaceHolder(new Space(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (getItemViewType(position)==USER){
                if (this.userPaginate.getTotal() > (position * DISPLAY_HORIZONTAL) && this.userPaginate.getTotal() > ((position * DISPLAY_HORIZONTAL) + 1) && this.userPaginate.getTotal() > ((position * DISPLAY_HORIZONTAL) + 2)){
                    this.UserList[SuggestUser.ONE] = this.userList.get(position * DISPLAY_HORIZONTAL);
                    this.UserList[SuggestUser.TWO] = this.userList.get((position * DISPLAY_HORIZONTAL)+1);
                    this.UserList[SuggestUser.THREE] = this.userList.get((position * DISPLAY_HORIZONTAL) + 2);
                    ((SuggestUserHolder) holder).bindView(this.activity,this,this.auth,this.UserList,position);
                }else{
                    if (this.userPaginate.getTotal() > (position * DISPLAY_HORIZONTAL) && this.userPaginate.getTotal() > ((position * DISPLAY_HORIZONTAL) + 1)){
                        this.UserList[SuggestUser.ONE] = this.userList.get(position * DISPLAY_HORIZONTAL);
                        this.UserList[SuggestUser.TWO] = this.userList.get((position * DISPLAY_HORIZONTAL)+1);
                        this.UserList[SuggestUser.THREE] = null;
                        ((SuggestUserHolder) holder).bindView(this.activity,this,this.auth,this.UserList,position);
                    }else {
                        this.UserList[SuggestUser.ONE] = this.userList.get(position * DISPLAY_HORIZONTAL);
                        this.UserList[SuggestUser.TWO] = null;
                        this.UserList[SuggestUser.THREE] = null;
                        ((SuggestUserHolder) holder).bindView(this.activity,this,this.auth,this.UserList,position);
                    }
                }
            }else {
                ((SpaceHolder) holder).bindView();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.total();
    }

    private int total(){
        int total = 0;
        try {
            if (this.userPaginate.getTotal() > 0){
                int mul = this.userPaginate.getTo() % DISPLAY_HORIZONTAL;
                int mulVal = Math.round(this.userPaginate.getTo() / DISPLAY_HORIZONTAL);
                if (mul > 0){
                    total = mulVal + 2;
                }else {
                    total = mulVal + 1;
                }
            }else {
                total = 1;
            }
            this.total = total;

        }catch (Exception e){
            e.printStackTrace();
        }
        return total;
    }

    public List<SmUser> getUserList() {
        return userList;
    }

    public void setUserList(List<SmUser> userList) {
        this.userList = userList;
    }

    public UserPaginate getUserPaginate() {
        return userPaginate;
    }

    public void setUserPaginate(UserPaginate userPaginate) {
        this.userPaginate = userPaginate;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<SmUser> getFollowList() {
        return FollowList;
    }

    public void setFollowList(ArrayList<SmUser> followList) {
        FollowList = followList;
    }
}

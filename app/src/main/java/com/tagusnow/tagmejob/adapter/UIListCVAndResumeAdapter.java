package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.CV;
import com.tagusnow.tagmejob.view.Holder.Widget.UIListCVAndResumeHolder;
import com.tagusnow.tagmejob.view.Widget.UIListCVAndResume;

import java.util.ArrayList;

public class UIListCVAndResumeAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private Context context;
    private ArrayList<CV> cvs;

    public UIListCVAndResumeAdapter(Activity activity,ArrayList<CV> cvs){
        this.activity = activity;
        this.context = activity;
        this.cvs = cvs;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UIListCVAndResumeHolder(new UIListCVAndResume(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((UIListCVAndResumeHolder) holder).bindView(this.activity,this.cvs.get(position));
    }

    @Override
    public int getItemCount() {
        return this.cvs.size();
    }
}

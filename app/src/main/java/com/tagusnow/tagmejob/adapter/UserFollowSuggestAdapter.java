package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.User;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.FollowSuggestFaceLayout;
import com.tagusnow.tagmejob.view.UserFollowSuggestLayout;

import java.util.ArrayList;
import java.util.List;

public class UserFollowSuggestAdapter extends RecyclerView.Adapter<UserFollowSuggestAdapter.ViewHolder> {

    private static final String TAG = UserFollowSuggestAdapter.class.getSimpleName();
    private Context context;
    private Activity activity;
    private List<SmUser> userList = new ArrayList<>();

    public UserFollowSuggestAdapter(Context context){
        this.context = context;
    }

    public UserFollowSuggestAdapter suggestUser(List<SmUser> userList){
        this.userList.addAll(userList);
        return this;
    }

    public UserFollowSuggestAdapter with(Activity activity){
        this.activity = activity;
        return this;
    }

    @NonNull
    @Override
    public UserFollowSuggestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(new UserFollowSuggestLayout(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull UserFollowSuggestAdapter.ViewHolder holder, int position) {
        holder.setList(this.userList.get(position)).with(activity).onBind();
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private SmUser user;
        private Activity activity;
        private UserFollowSuggestLayout userFollowSuggestLayout;

        private ViewHolder setList(SmUser user){
            this.user = user;
            return this;
        }

        private ViewHolder with(Activity activity){
            this.activity = activity;
            return this;
        }

        private void onBind(){
            this.userFollowSuggestLayout.with(activity).setUserFollowSuggest(this.user);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            this.userFollowSuggestLayout = (UserFollowSuggestLayout)itemView;
        }
    }
}

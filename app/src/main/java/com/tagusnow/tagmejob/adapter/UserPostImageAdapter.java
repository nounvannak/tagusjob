package com.tagusnow.tagmejob.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.model.v2.feed.Photo;
import java.util.List;

public class UserPostImageAdapter extends ArrayAdapter<Photo> implements IUserPostImageAdapter{

    private final LayoutInflater layoutInflater;
    private Context context;

    public UserPostImageAdapter(Context context, List<Photo> items) {
        super(context, 0, items);
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ImageView v = new ImageView(this.context);
        Photo item = getItem(position);
        if (item!=null){
            if (item.getImage()!=null){
                if (item.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(item.getImage())
                            .centerCrop()
                            .error(R.drawable.ic_file_picture_icon)
                            .into(v);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getAlbumPicture(item.getImage(),"300"))
                            .centerCrop()
                            .error(R.drawable.ic_file_picture_icon)
                            .into(v);
                }
            }
        }
        return v;
    }

    @Override public int getViewTypeCount() {
        return 2;
    }

    @Override public int getItemViewType(int position) {
        return position % 2 == 0 ? 1 : 0;
    }

    public UserPostImageAdapter(Context context) {
        super(context, 0);
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public void appendItems(List<Photo> newPhotos) {
        addAll(newPhotos);
        notifyDataSetChanged();
    }

    @Override
    public void setItems(List<Photo> morePhotos) {
        clear();
        appendItems(morePhotos);
    }
}

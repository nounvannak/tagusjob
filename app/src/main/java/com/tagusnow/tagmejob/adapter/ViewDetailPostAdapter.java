package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.BaseCard.Card;
import com.tagusnow.tagmejob.view.CommentFace;
import com.tagusnow.tagmejob.view.CommentingHolder;
import com.tagusnow.tagmejob.view.CommentingLayout;
import com.tagusnow.tagmejob.view.Holder.CardHolder;
import com.tagusnow.tagmejob.view.Holder.CommentFaceHolder;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Space;

import java.util.ArrayList;

public class ViewDetailPostAdapter extends RecyclerView.Adapter{

    Context context;
    private Activity activity;
    private ArrayList<Comment> comments;
    private CommentResponse commentResponse;
    private SmUser user;
    private Feed feed;
    private String image,name;
    public static final int MORE_VIEW = 1;
    private static final int CARD = 1;
    private static final int COMMENT = 2;
    private static final int LOADING = 3;
    private static final int COMMENTING = 4;
    private static final int SPACE = 5;
    private boolean is_commenting = false;

    private LoadingListener<LoadingView> loadingListener;
    private LoadingView loadingView;


    public ViewDetailPostAdapter(Activity activity, CommentResponse commentResponse, SmUser user,Feed feed,LoadingListener<LoadingView> loadingListener){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.feed = feed;
        this.is_commenting = false;
        this.commentResponse = commentResponse;
        this.comments = commentResponse.getData();
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public ViewDetailPostAdapter update(Activity activity,CommentResponse commentResponse){
        this.activity = activity;
        this.context = activity;
        this.commentResponse = commentResponse;
        this.comments.addAll(commentResponse.getData());
        this.is_commenting = false;
        return this;
    }

    public ViewDetailPostAdapter Commenting(String image,String name){
        this.image = image;
        this.name = name;
        this.is_commenting = true;
        return this;
    }

    public ViewDetailPostAdapter isComment(boolean is_commenting){
        this.is_commenting = is_commenting;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if (position==0){
            type = CARD;
        }else if (position==(getItemCount() - 1)){
            if (this.is_commenting){
                type = COMMENTING;
            }else {
                if (this.commentResponse.getTotal() > this.commentResponse.getTo()){
                    type = LOADING;
                }else {
                    type = SPACE;
                }
            }
        }else {
            type = COMMENT;
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==CARD){
            return new CardHolder(new Card(this.context));
        }else if (viewType==COMMENT) {
            return new CommentFaceHolder(new CommentFace(this.context));
        }else if (viewType==SPACE) {
            return new SpaceHolder(new Space(this.context));
        }else if (viewType==COMMENTING){
            return new CommentingHolder(new CommentingLayout(this.context));
        }else {
            return new LoadingHolder(loadingView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==CARD){
            ((CardHolder) holder).Adapter(this).bindView(this.activity,user,feed);
        }else if (getItemViewType(position)==COMMENT){
            ((CommentFaceHolder) holder).bindView(this.activity,user,comments.get(position - MORE_VIEW),this);
        }else if (getItemViewType(position)==LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else if (getItemViewType(position)==SPACE){
            ((SpaceHolder)holder).bindView();
        }else if (getItemViewType(position)==COMMENTING){
            ((CommentingHolder)holder).bindView(this.activity,this.image,this.name);
        }
    }

    @Override
    public int getItemCount() {
        return this.commentResponse.getTo() + 1 + MORE_VIEW;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

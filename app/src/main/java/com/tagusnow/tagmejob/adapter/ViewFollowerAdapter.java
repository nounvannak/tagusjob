package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.Empty.EmptyFollower;
import com.tagusnow.tagmejob.view.FollowerFace;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptyFollowerHolder;
import com.tagusnow.tagmejob.view.Holder.ViewFollowerHolder;

import java.util.List;

public class ViewFollowerAdapter extends RecyclerView.Adapter{

    private static final int FACE = 0;
    private static final int EMPTY = 1;
    private Activity activity;
    private Context context;
    private SmUser authUser;
    private UserPaginate userPaginate;
    private List<SmUser> userList;

    public ViewFollowerAdapter(Activity activity,UserPaginate userPaginate){
        this.activity = activity;
        this.context = activity;
        this.userPaginate = userPaginate;
        this.userList = userPaginate.getData();
        this.authUser = new Auth(activity).checkAuth().token();
    }

    public ViewFollowerAdapter update(Activity activity,UserPaginate userPaginate){
        this.activity = activity;
        this.context = activity;
        this.userPaginate = userPaginate;
        this.userList.addAll(userPaginate.getData());
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (this.userPaginate.getTotal() > 0){
            viewType = FACE;
        }else {
            viewType = EMPTY;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==FACE) {
            return new ViewFollowerHolder(new FollowerFace(this.context));
        }else {
            return new EmptyFollowerHolder(new EmptyFollower(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == FACE) {
            ((ViewFollowerHolder) holder).bindView(this.activity, this.authUser, this.userList.get(position));
        }else {
            ((EmptyFollowerHolder) holder).bindView();
        }
    }

    @Override
    public int getItemCount() {
        return this.userPaginate.getTotal() > 0 ? this.userPaginate.getTo() : 1;
    }
}

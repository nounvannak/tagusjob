package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Empty.EmptyJob;
import com.tagusnow.tagmejob.view.Empty.EmptySearch;
import com.tagusnow.tagmejob.view.Holder.Empty.EmptySearchHolder;
import com.tagusnow.tagmejob.view.Holder.NoticeMainSearchResultJobHolder;
import com.tagusnow.tagmejob.view.Holder.NoticeMainSearchResultPeopleHolder;
import com.tagusnow.tagmejob.view.Holder.NoticeMainSearchResultPostHolder;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultJob;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultPeople;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultPost;

public class ViewNoticeMainSearchResultAdapter extends RecyclerView.Adapter{

    private static final String TAG = ViewNoticeMainSearchResultAdapter.class.getSimpleName();
    private static final int TOTAL_RESULT = 0;
    private static final int HEADER_PEOPLE = 1;
    private static final int HEADER_JOB = 2;
    private static final int HEADER_POST = 3;
    private static final int RESULT_PEOPLE = 4;
    private static final int RESULT_JOB = 5;
    private static final int RESULT_POST = 6;
    private static final int EMPTY = 7;
    private NoticeMainSearchResultPeople people;
    private NoticeMainSearchResultJob job;
    private NoticeMainSearchResultPost post;
    private Activity activity;
    private Context context;
    private SmUser user;
    private String query;
    private static int total = 1;

    public ViewNoticeMainSearchResultAdapter(Activity activity,SmUser user,String query){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.query = query;
        this.people = new NoticeMainSearchResultPeople(this.context);
        this.job = new NoticeMainSearchResultJob(this.context);
        this.post = new NoticeMainSearchResultPost(this.context);
//        total = this.people.getTotal() + this.post.getTotal() + this.job.getTotal();
    }

    public ViewNoticeMainSearchResultAdapter update(Activity activity,SmUser user,String query){
        this.activity = activity;
        this.context = activity;
        this.user = user;
        this.query = query;
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (total > 0){
            if (position==0){
                viewType = RESULT_PEOPLE;
            }else if (position==1){
                viewType = RESULT_JOB;
            }else {
                viewType = RESULT_POST;
            }
        }else {
            if (position==0){
                viewType = EMPTY;
            }
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==RESULT_PEOPLE){
            return new NoticeMainSearchResultPeopleHolder(this.people);
        }else if (viewType==RESULT_JOB) {
            return new NoticeMainSearchResultJobHolder(this.job);
        }else if (viewType==EMPTY){
            return new EmptySearchHolder(new EmptySearch(this.context));
        }else {
            return new NoticeMainSearchResultPostHolder(this.post);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==RESULT_PEOPLE){
            ((NoticeMainSearchResultPeopleHolder) holder).bindView(this.activity,this.user,this.query);
        }else if (getItemViewType(position)==RESULT_JOB) {
            ((NoticeMainSearchResultJobHolder) holder).bindView(this.activity, this.user, this.query);
        }else if (getItemViewType(position)==EMPTY){
            ((EmptySearchHolder) holder).bindView(this.activity,this.query);
        }else {
            ((NoticeMainSearchResultPostHolder) holder).bindView(this.activity,this.user,this.query);
        }
    }

    @Override
    public int getItemCount() {
        return total > 0 ? 3 : 1;
    }

}

package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.CardNoPadding;
import com.tagusnow.tagmejob.view.CommentFace;
import com.tagusnow.tagmejob.view.Holder.CardNoPaddingHolder;
import com.tagusnow.tagmejob.view.Holder.CommentFaceHolder;
import com.tagusnow.tagmejob.view.Holder.SpaceHolder;
import com.tagusnow.tagmejob.view.Space;

import java.util.List;

public class ViewPostAdapter extends RecyclerView.Adapter{

    private static final String TAG = ViewPostAdapter.class.getSimpleName();
    public static final int MORE_VIEW = 2;
    private static final int FEED = 0;
    private static final int SPACE = 2;
    private static final int EMPTY = 3;
    private static final int COMMENT = 1;
    private Activity activity;
    private Context context;
    private SmUser user;
    private CommentResponse commentResponse;
    private List<Comment> commentList;
    private Feed feed;

    public ViewPostAdapter(Activity activity,CommentResponse commentResponse,Feed feed,SmUser user){
        this.activity = activity;
        this.context = activity;
        this.commentResponse = commentResponse;
        this.feed = feed;
        this.user = user;
        this.commentList = commentResponse.getData();
    }

    public ViewPostAdapter update(Activity activity,CommentResponse commentResponse,Feed feed,SmUser user){
        this.activity = activity;
        this.context = activity;
        this.commentResponse = commentResponse;
        this.feed = feed;
        this.user = user;
        this.commentList.addAll(commentResponse.getData());
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;

        if (this.commentResponse.getTotal() > 0){
            if (position==0){
                viewType = FEED;
            }else if (position==(this.commentResponse.getTo() - 1)){
                viewType = SPACE;
            }else {
                viewType = COMMENT;
            }
        }else {
            if (position==0){
                viewType = FEED;
            }else {
                viewType = SPACE;
            }
        }

        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==FEED){
            return new CardNoPaddingHolder(new CardNoPadding(this.context));
        }else if (viewType==COMMENT){
            return new CommentFaceHolder(new CommentFace(this.context));
        }else if (viewType==SPACE){
            return new SpaceHolder(new Space(this.context));
        }else {
            return new SpaceHolder(new Space(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==FEED){
            ((CardNoPaddingHolder) holder).Adapter(this).bindView(this.activity,this.user,this.feed.getId());
        }else if (getItemViewType(position)==COMMENT){
            ((CommentFaceHolder) holder).bindView(this.activity,this.user,this.commentList.get(position - (MORE_VIEW - 1)),this);
        }else if (getItemViewType(position)==SPACE){
            ((SpaceHolder) holder).bindView();
        }else {
            ((SpaceHolder) holder).bindView();
        }
    }

    @Override
    public int getItemCount() {
        return this.commentResponse.getTotal() > 0 ? this.commentResponse.getTo() : 2;
    }
}

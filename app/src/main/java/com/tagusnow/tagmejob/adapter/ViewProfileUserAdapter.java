package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.BaseCard.Card;
import com.tagusnow.tagmejob.view.BaseCard.CardCV;
import com.tagusnow.tagmejob.view.BaseCard.CardJob;
import com.tagusnow.tagmejob.view.Holder.CardCVHolder;
import com.tagusnow.tagmejob.view.Holder.CardHolder;
import com.tagusnow.tagmejob.view.Holder.CardJobHolder;
import com.tagusnow.tagmejob.view.Holder.Profile.User.BodyHolder;
import com.tagusnow.tagmejob.view.Holder.Profile.User.HeaderHolder;
import com.tagusnow.tagmejob.view.Holder.Profile.User.TitleHolder;
import com.tagusnow.tagmejob.view.Holder.Widget.WidgetImagePostHolder;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.Profile.User.BodyTitleLayout;
import com.tagusnow.tagmejob.view.Profile.User.HeaderLayout;
import com.tagusnow.tagmejob.view.Widget.WidgetImagePost;

import java.util.List;

public class ViewProfileUserAdapter extends RecyclerView.Adapter{

    private Context context;
    private Activity activity;
    private SmUser AuthUser,ProfileUser;
    private SmTagSubCategory category;
    private List<Feed> feedList;
    private FeedResponse feedResponse;
    private static final int MORE_VIEW = 3;
    private static final int HEADER = 0;
    private static final int TITLE = -1;
    private static final int BODY = 1;
    private static final int USER_IMAGE_POST = 2;
    private static final int NORMAL = 7;
    private static final int JOB = 8;
    private static final int CV = 9;
    private static final int LOADING = 10;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public ViewProfileUserAdapter(FeedResponse feedResponse,Activity activity,LoadingListener<LoadingView> loadingListener){
        this.feedResponse = feedResponse;
        this.feedList = feedResponse.getData();
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.AuthUser = new Auth(this.context).checkAuth().token();
        this.category = new Repository(this.context).restore().getCategory();
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public ViewProfileUserAdapter user(SmUser ProfileUser){
        this.ProfileUser = ProfileUser;
        return this;
    }

    public ViewProfileUserAdapter next(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feedList.addAll(feedResponse.getData());
        return this;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==HEADER){
            return new HeaderHolder(new HeaderLayout(this.context));
        }else if(viewType==TITLE) {
            return new TitleHolder(new BodyTitleLayout(this.context));
        }else if (viewType==USER_IMAGE_POST) {
            return new WidgetImagePostHolder(new WidgetImagePost(this.context));
        }else if (viewType==LOADING){
            return new LoadingHolder(loadingView);
        }else if (viewType==NORMAL){
            return new CardHolder(new Card(this.context));
        }else if (viewType==JOB){
            return new CardJobHolder(new CardJob(this.context));
        }else {
            return new CardCVHolder(new CardCV(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==HEADER) {
            ((HeaderHolder) holder).BindView(this.activity,this.ProfileUser,this.AuthUser);
        }else if (getItemViewType(position)==USER_IMAGE_POST){
            ((WidgetImagePostHolder) holder).bindView(this.activity,this.ProfileUser);
        }else if (getItemViewType(position)==TITLE) {
            ((TitleHolder) holder).bindView();
        }else if (getItemViewType(position)==LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else if (getItemViewType(position)==NORMAL){
            ((CardHolder) holder).Adapter(this).bindView(this.activity,this.AuthUser,this.feedList.get(position - MORE_VIEW));
        }else if (getItemViewType(position)==JOB){
            ((CardJobHolder) holder).bindView(this.activity,this.feedList.get(position - MORE_VIEW),this,this.AuthUser);
        }else {
            ((CardCVHolder) holder).bindView(this.activity,this.feedList.get(position - MORE_VIEW),this,this.AuthUser);
        }
    }

    @Override
    public int getItemCount() {
        return this.feedResponse.getTo() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (position==0) {
            viewType = HEADER;
        }else if (position==1){
            viewType = USER_IMAGE_POST;
        }else if(position==2){
            viewType = TITLE;
        }else {
            if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.NORMAL_POST)){
                viewType = NORMAL;
            }else if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.JOB_POST)){
                viewType = JOB;
            }else if (this.feedList.get(position - MORE_VIEW).getFeed().getType().equals(FeedDetail.STAFF_POST)){
                viewType = CV;
            }else if (position==(getItemCount() - 1)){
                viewType = LOADING;
            }
        }
        return viewType;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

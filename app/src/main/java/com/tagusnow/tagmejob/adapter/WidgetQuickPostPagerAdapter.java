package com.tagusnow.tagmejob.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tagusnow.tagmejob.WidgetQuickPostCV;
import com.tagusnow.tagmejob.fragment.WidgetQuickPostJobs;
import com.tagusnow.tagmejob.fragment.WidgetQuickPostNormal;
import com.tagusnow.tagmejob.view.Widget.WidgetQuickPost;

public class WidgetQuickPostPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public WidgetQuickPostPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position==WidgetQuickPost.NORMAL_POST){
            return WidgetQuickPostNormal.newInstance();
        }else if (position==WidgetQuickPost.JOBS_POST) {
            return WidgetQuickPostJobs.newInstance();
        }else{
            return WidgetQuickPostCV.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}

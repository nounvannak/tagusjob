package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.page.resume.CompanyFaceListener;
import com.tagusnow.tagmejob.view.Holder.Widget.CompanyFaceHolder;
import com.tagusnow.tagmejob.view.Widget.CompanyFace;

import java.util.List;

public class WidgetTopCompanyAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private Context context;
    private List<Feed> feeds;
    private CompanyFaceListener companyFaceListener;

    public WidgetTopCompanyAdapter(Activity activity, List<Feed> feeds) {
        this.activity = activity;
        this.context = activity;
        this.feeds = feeds;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
    }

    public void setCompanyFaceListener(CompanyFaceListener companyFaceListener) {
        this.companyFaceListener = companyFaceListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CompanyFaceHolder(new CompanyFace(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((CompanyFaceHolder) holder).bindView(this.activity,this.feeds.get(position),companyFaceListener);
    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }
}

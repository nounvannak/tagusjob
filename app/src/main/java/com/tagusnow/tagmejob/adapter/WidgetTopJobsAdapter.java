package com.tagusnow.tagmejob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.view.Holder.Widget.WidgetTopJobsHolder;
import com.tagusnow.tagmejob.view.Widget.WidgetTopJobsFace;

import java.util.List;

public class WidgetTopJobsAdapter extends RecyclerView.Adapter{

    private SmTagSubCategory category;
    private List<Category> dataList;
    private Activity activity;
    private Context context;
    private SmUser auth;

    public WidgetTopJobsAdapter(Activity activity,SmTagSubCategory category,SmUser auth){
        this.activity = activity;
        this.context = activity;
        this.auth = auth;
        this.category = category;
        this.dataList = category.getData();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WidgetTopJobsHolder(new WidgetTopJobsFace(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((WidgetTopJobsHolder) holder).bindView(this.activity,this.dataList.get(position),this.auth);
    }

    @Override
    public int getItemCount() {
        return this.dataList.size();
    }
}

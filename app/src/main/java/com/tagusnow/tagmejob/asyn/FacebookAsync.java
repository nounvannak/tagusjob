package com.tagusnow.tagmejob.asyn;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.tagusnow.tagmejob.feed.FacebookFeed;

import cz.msebera.android.httpclient.Header;

public class FacebookAsync extends BaseJsonHttpResponseHandler<AsyncHttpResponseHandler> {
    private FacebookFeed facebookFeed;

    public FacebookAsync(){
        super();
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
        this.facebookFeed = new Gson().fromJson(rawJsonResponse,FacebookFeed.class);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
        this.facebookFeed = null;
    }

    @Override
    protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
        return null;
    }

    public FacebookFeed getFacebookFeed() {
        return facebookFeed;
    }

    public void setFacebookFeed(FacebookFeed facebookFeed) {
        this.facebookFeed = facebookFeed;
    }
}

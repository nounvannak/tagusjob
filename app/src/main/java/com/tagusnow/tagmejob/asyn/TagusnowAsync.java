package com.tagusnow.tagmejob.asyn;

import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.tagusnow.tagmejob.adapter.MFeedAdapter;
import com.tagusnow.tagmejob.feed.TagusnowFeed;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class TagusnowAsync extends BaseJsonHttpResponseHandler<AsyncHttpResponseHandler> implements OnLoopjCompleted{

    private TagusnowFeed tagusnowFeed;

    public TagusnowAsync(){
        this.tagusnowFeed = new TagusnowFeed();
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
        Log.i("tagusnowFeed",rawJsonResponse);
        tagusnowFeed = new Gson().fromJson(rawJsonResponse,TagusnowFeed.class);
        taskCompleted(tagusnowFeed);
        setTagusnowFeed(tagusnowFeed);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
        tagusnowFeed = null;
    }

    @Override
    protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
        return null;
    }

    public TagusnowFeed getTagusnowFeed() {
        return tagusnowFeed;
    }

    public void setTagusnowFeed(TagusnowFeed tagusnowFeed) {
        this.tagusnowFeed = tagusnowFeed;
    }

    @Override
    public void taskCompleted(Object object) {
        this.tagusnowFeed = (TagusnowFeed) object;
    }
}

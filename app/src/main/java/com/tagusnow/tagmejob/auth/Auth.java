package com.tagusnow.tagmejob.auth;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Handler.AuthHandler;
import com.tagusnow.tagmejob.social.MFacebook;
import com.tagusnow.tagmejob.social.MGoogle;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.sentry.Sentry;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import studios.codelight.smartloginlibrary.UserSessionManager;
import studios.codelight.smartloginlibrary.users.SmartUser;

public class Auth extends SmUser{

    private Context context;
    private EditText txtUsername,txtEmail,txtPhone,txtPassword,txtRepassword;
    private TextView flash;
    private String username,password,email,phone,repassword,platform;
    private String authJson;
    private boolean isAuth;
    private static boolean isValidUser = false;
    private boolean isLogin;
    private boolean isRegister;
    private MFacebook mFacebook;
    private MGoogle mGoogle;
    private SmUser authUser;
    private SmartUser user;
    private final static String TAG = Auth.class.getSimpleName();
    public static final int LOGIN_WEB = 0;
    public static final int LOGIN_GOOGLE = 2;
    public static final int LOGIN_FACEBOOK = 1;
    private RequestBody user_name,firs_name,last_name,_email,image,profile_id,birthday,gender,location,_password,confirm_password,_phone;

    /*Service*/
    private AuthService service = ServiceGenerator.createService(AuthService.class);

    @NonNull
    private RequestBody createPartFromString(String name) {
        return RequestBody.create(okhttp3.MultipartBody.FORM, name);
    }

    private Callback<Token> mLoginWebCallback = new Callback<Token>() {
        @Override
        public void onResponse(Call<Token> call, Response<Token> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                postToken(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Token> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<SmUser> mAuthenticateCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                Log.w(TAG,new Gson().toJson(response.body()));
                isLogin = true;
                User(response.body()).save();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<SmUser> mRegisterCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            isValidUser = response.isSuccessful();
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                isRegister = true;
                putAuth(response.body()).save();
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<ValidUser> mValidUser = new Callback<ValidUser>() {
        @Override
        public void onResponse(Call<ValidUser> call, Response<ValidUser> response) {
            Log.e("mValidUser isSuccessful",String.valueOf(response.isSuccessful()));
            if (response.isSuccessful()){
                Log.w("mValidUser",String.valueOf(response.body().isValid()));
                isValidUser = response.body().isValid();
                setValidUser(response.body().isValid());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<ValidUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public boolean isValidUser() {
        return isValidUser;
    }

    public Auth(Context context){
        this.context = context;
        this.user = UserSessionManager.getCurrentUser(context);
    }

    public Auth with(Context context){this.context = context;return this;}

    public boolean isAuth(){
        return isAuth;
    }

    public Auth setFacebook(MFacebook facebook){
        this.mFacebook = facebook;
        this.authUser = new SmUser(facebook);
        this.authJson = new Gson().toJson(this.authUser);
        return this;
    }

    public Auth setGoogle(MGoogle google){
        this.mGoogle = google;
        this.authUser = new SmUser(google);
        this.authJson = new Gson().toJson(this.authUser);
        return this;
    }

    public Auth User(SmUser authUser){
        this.authUser = authUser;
        this.authJson = new Gson().toJson(authUser);
        return this;
    }

    public SmUser logout(){ LoginManager.getInstance().logOut();
      return new SmUser();
    }

    public Auth setPlatform(String platform){
        this.platform = platform;
        return this;
    }

    public Auth login(){
        Log.e(TAG,String.valueOf(isValidUser));
        if (isValidUser){
            if (this.platform.equals(SuperConstance.PLATFORM_WEB)){
                LoginWeb();
            }else{
                LoginSocial();
            }
        }else{
            if (this.platform.equals(SuperConstance.PLATFORM_WEB)){
                Register().LoginWeb();
            }else{
                Register().LoginSocial();
            }
        }
        return this;
    }

    public Auth restore(){
        this.email = this.authUser.getEmail();
        return this;
    }

    public Auth response(){
        this.authUser = checkAuth().token();
        if (this.authUser!=null){
            this.email = this.authUser.getEmail();
            if (this.authUser.getStatus()==LOGIN_WEB){
                LoginWeb();
            }else{
                LoginSocial();
            }
        }
        return this;
    }

    private void LoginWeb(){
        if (this.password!=null){
            service.loginWeb(this.email,this.password,SuperConstance.PLATFORM_WEB).enqueue(mLoginWebCallback);
        }else {
            service.loginWeb(this.email,SuperConstance.PLATFORM_WEB,1).enqueue(mLoginWebCallback);
        }
    }

    private void LoginSocial(){
        service.loginWeb(this.authUser.getEmail(),this.authUser.getPassword(),this.authUser.getProfile_id(),SuperConstance.PLATFORM_FACEBOOK).enqueue(mLoginWebCallback);
    }

    public Auth param(String email,String password){
        this.email = email;
        this.password = password;
        this.authUser = new SmUser();
        this.authUser.setPassword(this.password);
        this.authUser.setRepassword(this.password);
        this.authUser.setEmail(this.email);
        return this;
    }

    public Auth param(String email){
        this.email = email;
        return this;
    }

    public Auth param(String username,String email,String phone,String password,String repassword){
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.repassword = repassword;
        return this;
    }

    public Auth putAuth(SmUser authUser){
        this.authUser = authUser;
        return this;
    }

    private void setValidUser(boolean isValidUser){
        Auth.isValidUser = isValidUser;
    }

    private void postToken(Token token){
        if (token!=null){
            this.Authenticate(token.getToken());
        }else {
            Log.e(TAG,"Token null (postToken(Token token))");
        }
    }

    protected void Authenticate(String token){
        Log.w(TAG,token);
        service.Authenticate(token).enqueue(mAuthenticateCallback);
    }

    public Auth save(){
        try {
            SharedPreferences mAuthPrefs = context.getSharedPreferences(SuperConstance.USER_SESSION,Context.MODE_PRIVATE);
            SharedPreferences.Editor mAuthEditor = mAuthPrefs.edit();
            mAuthEditor.putString(SuperConstance.USER_SESSION,this.authJson);
            mAuthEditor.apply();
            mAuthEditor.commit();
        }catch (Exception e){
            Sentry.capture(e);
            e.printStackTrace();
        }
        return this;
    }

    public Auth save(SmUser user){
        try {
            SharedPreferences mAuthPrefs = context.getSharedPreferences(SuperConstance.USER_SESSION,Context.MODE_PRIVATE);
            SharedPreferences.Editor mAuthEditor = mAuthPrefs.edit();
            mAuthEditor.putString(SuperConstance.USER_SESSION,new Gson().toJson(user));
            mAuthEditor.apply();
            mAuthEditor.commit();
        }catch (Exception e){
            Sentry.capture(e);
            e.printStackTrace();
        }
        return this;
    }


    public Auth destroy(){
        try{
            SuperUtil.errorTrancking(TAG+"::destroy");
            if (this.isAuth){
                SharedPreferences mAuthPrefs = context.getSharedPreferences(SuperConstance.USER_SESSION,Context.MODE_PRIVATE);
                SharedPreferences.Editor mAuthEditor = mAuthPrefs.edit();
                mAuthEditor.putString(SuperConstance.USER_SESSION,null);
                mAuthEditor.apply();
                mAuthEditor.commit();
                this.isAuth = false;
            }
        }catch (Exception e){
            Sentry.capture(e);
            e.printStackTrace();
        }
        return this;
    }

    protected void Register(MFacebook facebook){
        this.authUser = new SmUser(facebook);
        this.user_name = createPartFromString(this.authUser.getUser_name());
        this.firs_name = createPartFromString(this.authUser.getFirs_name());
        this.last_name = createPartFromString(this.authUser.getLast_name());
        this.birthday = createPartFromString(this.authUser.getDob());
//        Log.w("facebook",facebook.getEmail());
        this._email    = createPartFromString(this.authUser.getEmail());
        this.image     = createPartFromString(this.authUser.getImage());
        this._password = createPartFromString(this.authUser.getPassword());
        this.confirm_password = createPartFromString(this.authUser.getPassword());
        this.location  = createPartFromString(this.authUser.getLocation());
        this.gender    = createPartFromString(this.authUser.getGender());
        this.profile_id = createPartFromString(this.authUser.getProfile_id());
        service.saveFacebook(SuperConstance.PLATFORM_FACEBOOK,user_name,firs_name,last_name,_email,image,_password,location,gender,birthday,profile_id).enqueue(mRegisterCallback);
    }

    protected void Register(MGoogle google) {
        this.authUser = new SmUser(google);
        this.user_name = createPartFromString(this.authUser.getUser_name());
        this.firs_name = createPartFromString(this.authUser.getFirs_name());
        this.last_name = createPartFromString(this.authUser.getLast_name());
        Log.w("google",this.authUser.getEmail());
        this._email = createPartFromString(this.authUser.getEmail());
        this.image = createPartFromString(this.authUser.getImage());
        this._password = createPartFromString(this.authUser.getPassword());
        this.profile_id = createPartFromString(this.authUser.getProfile_id());
        service.saveGoogle(SuperConstance.PLATFORM_GOOGLE,user_name,firs_name,last_name,_email,image,_password,profile_id).enqueue(mRegisterCallback);
    }

    protected void Register(SmUser user){
        this.authUser = user;
        this.user_name = createPartFromString(this.authUser.getUser_name());
        this._email    = createPartFromString(this.authUser.getEmail());
        this._phone = createPartFromString(this.authUser.getPhone());
        this._password = createPartFromString(this.authUser.getPassword());
        this.confirm_password = createPartFromString(this.authUser.getRepassword());
        service.saveWeb(SuperConstance.PLATFORM_WEB,user_name,_email,_phone,_password,confirm_password).enqueue(mRegisterCallback);
    }


    private Auth Register(){
        switch (this.platform) {
            case SuperConstance.PLATFORM_WEB:
                this.Register(this.authUser);
                break;
            case SuperConstance.PLATFORM_FACEBOOK:
                this.Register(this.mFacebook);
                break;
            case SuperConstance.PLATFORM_GOOGLE:
                this.Register(this.mGoogle);
                break;
            default:
                Log.e(TAG, "No Platform");
                break;
        }
        return this;
    }

    public Auth isValid(){
        Log.e(TAG,"Do isValid");
        if (this.platform.equals(SuperConstance.PLATFORM_WEB)){
            Log.e(TAG,"Do isValid WEB");
            Log.e(TAG,this.authUser.getEmail());
            service.validUser(SuperConstance.PLATFORM_WEB,this.authUser.getEmail()).enqueue(mValidUser);
        }else {
            Log.e(TAG,"Do isValid Social");
            Log.e(TAG,this.authUser.getProfile_id());
            service.validUser(SuperConstance.PLATFORM_FACEBOOK,this.authUser.getEmail(),this.authUser.getProfile_id()).enqueue(mValidUser);
        }
        return this;
    }

    public SmUser token(){
        return this.authUser;
    }

    public Auth checkAuth(){
        try {
            SuperUtil.errorTrancking(TAG+"::checkAuth");
            SharedPreferences mUserPrefs = context.getSharedPreferences(SuperConstance.USER_SESSION,Context.MODE_PRIVATE);
            this.authJson = mUserPrefs.getString(SuperConstance.USER_SESSION,null);
            if (this.authJson!=null){
                this.authUser = new Gson().fromJson(this.authJson,SmUser.class);
            }
            this.isAuth = this.authJson != null;
            save();
        }catch (Exception e){
            Sentry.capture(e);
            e.printStackTrace();
        }
        return this;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public boolean isRegister() {
        return isRegister;
    }
}

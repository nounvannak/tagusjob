package com.tagusnow.tagmejob.auth;

public class Error {
    private String error;

    public Error() {
        super();
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

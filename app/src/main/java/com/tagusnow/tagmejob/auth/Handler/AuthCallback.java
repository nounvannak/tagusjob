package com.tagusnow.tagmejob.auth.Handler;

import android.content.Context;

import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.social.MFacebook;
import com.tagusnow.tagmejob.social.MGoogle;
import retrofit2.Call;
import retrofit2.Response;

public class AuthCallback extends Auth implements AuthHandler{

    public AuthCallback(Context context) {
        super(context);
    }

    @Override
    public void Login(String token) {
        Authenticate(token);
    }

    @Override
    public void Facebook(MFacebook facebook) {
        Register(facebook);
    }

    @Override
    public void GooglePlus(MGoogle google) {
        Register(google);
    }

    @Override
    public void Register(SmUser user) {
        Register(user);
    }

    @Override
    public void Authenicate(SmUser user) {
        User(user);
    }

    @Override
    public void UserValid(boolean isValid) {

    }

    @Override
    public void onResponse(Call call, Response response) {

    }

    @Override
    public void onFailure(Call call, Throwable t) {

    }
}

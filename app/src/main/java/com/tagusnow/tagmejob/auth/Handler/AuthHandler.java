package com.tagusnow.tagmejob.auth.Handler;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.social.MFacebook;
import com.tagusnow.tagmejob.social.MGoogle;
import retrofit2.Callback;

public interface AuthHandler extends Callback {

    void Login(String token);

    void Facebook(MFacebook facebook);

    void GooglePlus(MGoogle google);

    void Register(SmUser user);

    void Authenicate(SmUser user);

    void UserValid(boolean isValid);

}

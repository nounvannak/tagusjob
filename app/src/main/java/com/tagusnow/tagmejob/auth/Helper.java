package com.tagusnow.tagmejob.auth;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.github.scribejava.apis.FacebookApi;
import com.github.scribejava.apis.GitHubApi;
import com.github.scribejava.apis.GoogleApi20;
import com.github.scribejava.apis.LinkedInApi20;
import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.apis.YahooApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.BaseApi;
import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;

import org.fuckboilerplate.rx_social_connect.RxSocialConnect;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Helper {
    private final Object targetUI;
    private Callback<Object> AUTH = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.isSuccessful()){
                Log.e("Status",String.valueOf(response.code()));
                Log.e("Auth",String.valueOf(response.body()));
                GoogleResponse user = new Gson().fromJson(new Gson().toJson(response.body()),GoogleResponse.class);
                Log.e("user",user.getId());
            }else {
                try {
                    Log.e("Status",String.valueOf(response.code()));
                    Log.e("Auth",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Object> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public Helper(Object targetUI) {
        this.targetUI = targetUI;
    }

    OAuth10aService twitterService() {
        String consumerKey = "3wEAwhjvJmBpJpUfIXiY9PQOg";
        String consumerSecret = "8N47XdpE4GSlswMcBTOZFc3fDcil7WnK4RINfUyKf2d0rFrF2I";
        String callbackUrl = "http://victoralbertos.com";

        return new ServiceBuilder()
                .apiKey(consumerKey)
                .apiSecret(consumerSecret)
                .callback(callbackUrl)
                .build(TwitterApi.instance());
    }

    OAuth20Service facebookService() {
        String appId = "274892416189311";
        String appSecret = "eeb0e79e4f5f0a1d45535476e4b29d43";
        String callbackUrl = "https://tagusjob.com/api/v1/auth/callback";
        String permissions = "public_profile, email";

        return new ServiceBuilder()
                .apiKey(appId)
                .apiSecret(appSecret)
                .callback(callbackUrl)
                .scope(permissions)
                .build(FacebookApi.instance());
    }

    public OAuth20Service googleService() {
        String clientId = getContext().getResources().getString(R.string.google_client_id);
        String clientSecret = getContext().getResources().getString(R.string.google_client_secret);
        String callbackUrl = SuperUtil.getBaseUrl("api/v1/auth/callback");
        String permissions = "profile";
        return new ServiceBuilder()
                .apiKey(clientId)
                .apiSecret(clientSecret)
                .callback(callbackUrl)
                .scope(permissions)
                .build(GoogleApi20.instance());
    }

    OAuth20Service linkedinService() {
        String clientId = "77u9plrpoq0g6t";
        String clientSecret = "VlH229TNkzJysxbq";
        String callbackUrl = "http://victoralbertos.com";
        String permissions = "r_basicprofile";

        return new ServiceBuilder()
                .apiKey(clientId)
                .apiSecret(clientSecret)
                .callback(callbackUrl)
                .scope(permissions)
                .build(LinkedInApi20.instance());
    }

    OAuth10aService yahooService() {
        String clientId = "dj0yJmk9Sk9NZUlPc0RaODVDJmQ9WVdrOVRubHlWMWRuTTJVbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0wNw--";
        String clientSecret = "449b2d2f06d986297d65df7fc020544bf71eb10c";
        String callbackUrl = "http://victoralbertos.com";

        return new ServiceBuilder()
                .apiKey(clientId)
                .apiSecret(clientSecret)
                .callback(callbackUrl)
                .build(YahooApi.instance());
    }

    OAuth20Service githubService() {
        String appId = "725a59980165855cc986";
        String appSecret = "38733b4bf1c8646af86e38797f345847a0b61388";
        String callbackUrl = "http://victoralbertos.com/";

        return new ServiceBuilder()
            .apiKey(appId)
            .apiSecret(appSecret)
            .callback(callbackUrl)
            .build(GitHubApi.instance());
    }

    void showTokenOAuth1(Class<? extends DefaultApi10a> clazz) {
        RxSocialConnect.getTokenOAuth1(clazz)
                .subscribe(token -> showResponse(token),
                        error -> showError(error));
    }

    void showTokenOAuth2(Class<? extends DefaultApi20> clazz) {
        RxSocialConnect.getTokenOAuth2(clazz)
                .subscribe(token -> showResponse(token),
                        error -> showError(error));
    }

    void closeConnection(Class<? extends BaseApi> clazz) {
        RxSocialConnect
                .closeConnection(clazz)
                .subscribe(_I -> {
                    showToast(clazz.getName() + " disconnected");
                }, error -> showError(error));
    }

    void closeAllConnection() {
        RxSocialConnect
                .closeConnections()
                .subscribe(_I -> {
                    showToast("All disconnected");
                }, error -> showError(error));
    }

    void showResponse(OAuth1AccessToken token) {
        Log.e("Res",new Gson().toJson(token));
    }

    public void showResponse(OAuth2AccessToken token) {
        AuthService service = ServiceGenerator.createServices(AuthService.class);
        Log.e("token",String.valueOf(token));
        service.AuthGoogle(token.getTokenType() + " " + token.getAccessToken(),"application/json").enqueue(AUTH);
    }

    public void showError(Throwable throwable) {
        showToast(throwable.getMessage());
    }

    private View findViewById(int resId) {
        if (targetUI instanceof Activity) return ((Activity) targetUI).findViewById(resId);
        else return ((Fragment) targetUI).getView().findViewById(resId);
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private Context getContext() {
        if (targetUI instanceof Activity) return  ((Activity) targetUI);
        else return((Fragment) targetUI).getActivity();
    }
}

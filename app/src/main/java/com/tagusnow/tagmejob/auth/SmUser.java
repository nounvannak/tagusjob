package com.tagusnow.tagmejob.auth;

import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.feed.CV;
import com.tagusnow.tagmejob.feed.Skill;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.social.MFacebook;
import com.tagusnow.tagmejob.social.MGoogle;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SmUser implements Serializable {
    public static final int NO_TYPE = 0;
    public static final int FIND_JOB = 2;
    public static final int FIND_STAFF = 1;
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String FACEBOOK = "facebook";
    public static final String GOOGLE = "google";
    public static final String ANONYMOUS = "anonymous";
    private int id;
    private int auth_user;
    private String login_method;
    private String password;
    private String repassword;
    private String firs_name;
    private String last_name;
    private String user_name;
    private String email;
    private String phone;
    private String phone1;
    private String phone2;
    private String image;
    private String cover;
    private String dob;
    private int role;
    private int group;
    private String re_token;
    private String location;
    private String address;
    private String gender;
    private String profile_id;
    private String profile_link;
    private int status;
    private int trash;
    private String created_at;
    private String updated_at;
    private int user_type;
    private int active;
    private String slug;
    private String biography;
    private Object langauge;
    private int is_admin;
    private int is_looking_job;
    private boolean is_like;
    private boolean is_follow;
    private boolean is_see_first;
    private int following_counter;
    private int follower_counter;
    private int counter_post_jobs;
    private int counter_saved_jobs;
    private int counter_expired_jobs;
    private ArrayList<CV> staff_file;
    private ArrayList<UserInterest> interests;
    private ArrayList<Skill> skills;
    private String skill_name;
    private String interest_name;
    private String skill_text;
    private String interest_text;
    private Conversation conversation;

    public SmUser() {
        super();
    }

    public SmUser(MFacebook facebook){
        this.setFirs_name(facebook.getFirst_name());
        this.setLast_name(facebook.getLast_name());
        this.setUser_name(facebook.getName());
        this.setPassword(facebook.getId());
        if (facebook.getLocation()!=null){
            this.setLocation(facebook.getLocation().getName());
            this.setAddress(facebook.getLocation().getName());
        }else {
            this.setLocation("");
            this.setAddress("");
        }
        this.setProfile_id(facebook.getId());
        if (facebook.getBirthday()!=null){
            this.setDob(facebook.getBirthday());
        }else {
            this.setDob("");
        }

        if (facebook.getGender()!=null){
            this.setGender(facebook.getGender());
        }else {
            this.setGender("male");
        }
        if (facebook.getCover()!=null){
            if (facebook.getCover().getSource()!=null){
                this.setCover(facebook.getCover().getSource());
            }else {
                this.setCover("");
            }
        }else {
            this.setCover("");
        }

        if (facebook.getEmail()!=null){
            this.setEmail(facebook.getEmail());
        }else {
            this.setEmail("");
        }
        this.setImage(SuperConstance.FACEBOOK_GRAPE+"/"+facebook.getId()+"/picture?width=200");
    }

    public SmUser(MGoogle google){
        this.setFirs_name(google.getGivenName());
        this.setLast_name(google.getFamilyName());
        this.setUser_name(google.getDisplayName());
        if (google.getPhotoUrl()!=null){
            this.setImage("https://"+google.getPhotoUrl().getHost()+google.getPhotoUrl().getPath()+"?sz=200");
        }else {
            this.setImage("");
        }
        this.setEmail(google.getEmail());
        this.setProfile_id(google.getId());
        this.setPassword(google.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirs_name() {
        return firs_name;
    }

    public void setFirs_name(String firs_name) {
        this.firs_name = firs_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getRe_token() {
        return re_token;
    }

    public void setRe_token(String re_token) {
        this.re_token = re_token;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        if(this.gender!=null){
            if (this.gender.equals("1")){
                return "Male";
            }else {
                return "Female";
            }
        }else {
            return "Male";
        }
    }

    public int getGenderId(){
        return this.gender!=null ? Integer.parseInt(this.gender) : 0;
    }

    public String getDateOfBirth(){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat fa = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
        String date = "";
        try {
            if(this.dob!=null) {
                date = fa.format(new Date(f.parse(this.dob).getTime()));
            }else {
                date = null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getProfile_link() {
        return profile_link;
    }

    public void setProfile_link(String profile_link) {
        this.profile_link = profile_link;
    }

    public boolean isIs_see_first() {
        return is_see_first;
    }

    public void setIs_see_first(boolean is_see_first) {
        this.is_see_first = is_see_first;
    }

    public boolean isIs_follow() {
        return is_follow;
    }

    public void setIs_follow(boolean is_follow) {
        this.is_follow = is_follow;
    }

    public boolean isIs_like() {
        return is_like;
    }

    public void setIs_like(boolean is_like) {
        this.is_like = is_like;
    }

    public int getFollowing_counter() {
        return following_counter;
    }

    public void setFollowing_counter(int following_counter) {
        this.following_counter = following_counter;
    }

    public int getFollower_counter() {
        return follower_counter;
    }

    public void setFollower_counter(int follower_counter) {
        this.follower_counter = follower_counter;
    }

    public int getCounter_post_jobs() {
        return counter_post_jobs;
    }

    public void setCounter_post_jobs(int counter_post_jobs) {
        this.counter_post_jobs = counter_post_jobs;
    }

    public int getCounter_saved_jobs() {
        return counter_saved_jobs;
    }

    public void setCounter_saved_jobs(int counter_saved_jobs) {
        this.counter_saved_jobs = counter_saved_jobs;
    }

    public int getCounter_expired_jobs() {
        return counter_expired_jobs;
    }

    public void setCounter_expired_jobs(int counter_expired_jobs) {
        this.counter_expired_jobs = counter_expired_jobs;
    }

    public int getAuth_user() {
        return auth_user;
    }

    public void setAuth_user(int auth_user) {
        this.auth_user = auth_user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public int getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(int is_admin) {
        this.is_admin = is_admin;
    }

    public ArrayList<CV> getStaff_file() {
        return staff_file;
    }

    public void setStaff_file(ArrayList<CV> staff_file) {
        this.staff_file = staff_file;
    }

    public int getIs_looking_job() {
        return is_looking_job;
    }

    public void setIs_looking_job(int is_looking_job) {
        this.is_looking_job = is_looking_job;
    }

    public ArrayList<UserInterest> getInterest() {
        return interests;
    }

    public void setInterest(ArrayList<UserInterest> interest) {
        this.interests = interest;
    }

    public ArrayList<Skill> getSkill() {
        return skills;
    }

    public void setSkill(ArrayList<Skill> skill) {
        this.skills = skill;
    }

    public String getInterest_text() {
        String interest_text = null;
        if (interests!=null){
            for (UserInterest s : interests){
                if (interest_text==null){
                    interest_text = s.getName();
                }else {
                    interest_text +=", "+s.getName();
                }
            }
        }
        return interest_text;
    }

    public void setInterest_text(String interest_text) {
        this.interest_text = interest_text;
    }

    public String getSkill_text() {
        String skill_text = null;
        if (skills!=null){
            for (Skill s : skills){
                if (skill_text==null){
                    skill_text = s.getName();
                }else {
                    skill_text +=", "+s.getName();
                }
            }
        }
        return skill_text;
    }

    public void setSkill_text(String skill_text) {
        this.skill_text = skill_text;
    }

    public String getInterest_name() {
        return interest_name;
    }

    public void setInterest_name(String interest_name) {
        this.interest_name = interest_name;
    }

    public String getSkill_name() {
        return skill_name;
    }

    public void setSkill_name(String skill_name) {
        this.skill_name = skill_name;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public Object getLanguage() {
        return langauge;
    }

    public void setLanguage(Object language) {
        this.langauge = language;
    }

    public String getLogin_method() {
        return login_method;
    }

    public void setLogin_method(String login_method) {
        this.login_method = login_method;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }
}

package com.tagusnow.tagmejob.auth;

public class Token {

    public Token(){
        super();
    }

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

package com.tagusnow.tagmejob.auth;

public class ValidUser {
    private boolean valid;
    private String message;

    public ValidUser() {
        super();
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

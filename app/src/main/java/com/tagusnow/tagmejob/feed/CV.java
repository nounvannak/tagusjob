package com.tagusnow.tagmejob.feed;

import java.io.Serializable;

public class CV implements Serializable {

    public static final String CV = "file_cv";
    public static final String COVER = "cover_letter";

    private String type;
    private int id;
    private String file;
    public CV(){
        super();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}

package com.tagusnow.tagmejob.feed;

import java.io.Serializable;

public class Category implements Serializable {
    private int id;
    private String title;
    private String description;
    private String icon;
    private String image;
    private int job_counter;
    private int subscribe;

    public Category(int id,String title,String description,String icon,String image,int job_counter){
        this.id = id;
        this.title = title;
        this.description = description;
        this.icon = icon;
        this.image = image;
        this.job_counter = job_counter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getJob_counter() {
        return job_counter;
    }

    public void setJob_counter(int job_counter) {
        this.job_counter = job_counter;
    }

    public boolean isSubscribe() {
        return subscribe==1;
    }

    public void setSubscribe(int subscribe) {
        this.subscribe = subscribe;
    }
}

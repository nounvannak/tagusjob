package com.tagusnow.tagmejob.feed;

import android.text.format.DateUtils;

import com.tagusnow.tagmejob.auth.SmUser;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Comment implements Serializable {
    private int id;
    private int feed_id;
    private int user_id;
    private String channel_id;
    private String image;
    private String des;
    private String c_date;
    private int status;
    private int trash;
    private SmUser comment_user;

    public Comment() {
        super();
    }

    public boolean isNullImage(){
        return this.image==null || this.image.equals("");
    }

    public boolean isNullDescription(){
        return this.des==null;
    }

    public String getTimeline(){
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date d = f.parse(this.c_date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            milliseconds = new Date().getTime();
        }
        return DateUtils.getRelativeTimeSpanString(milliseconds,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(int feed_id) {
        this.feed_id = feed_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getC_date() {
        return c_date;
    }

    public void setC_date(String c_date) {
        this.c_date = c_date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }

    public SmUser getComment_user() {
        return comment_user;
    }

    public void setComment_user(SmUser comment_user) {
        this.comment_user = comment_user;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }
}

package com.tagusnow.tagmejob.feed;

import android.text.format.DateFormat;
import android.text.format.DateUtils;

import java.util.Date;

public class FacebookFeed {
    private String profile_name;
    private String profile_username;
    private String profile_image;
    private String profile_url;
    private int count_likes;
    private int count_comments;
    private String hashtag_text;
    private String hashtag_url;
    private String hashtag_image;
    private String album;
    private String icon;
    private String type;
    private String feed_type;
    private String story;
    private String user_id;
    private String video_url;
    private String post_link;
    private String key;
    private int is_accept;
    private long created_date;

    public FacebookFeed() {
        this.profile_name = "";
        this.profile_username = "";
        this.profile_image = "";
        this.profile_url = "";
        this.count_likes = 0;
        this.count_comments = 0;
        this.hashtag_text = "";
        this.hashtag_url = "";
        this.hashtag_image = "";
        this.album = "";
        this.icon = "";
        this.type = "";
        this.feed_type = "";
        this.story = "";
        this.user_id = "";
        this.video_url = "";
        this.post_link = "";
        this.key = "";
        this.is_accept = 0;
        this.created_date = 0;
    }

    public String getTimeline(){
        return DateUtils.getRelativeTimeSpanString(this.created_date,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getProfile_username() {
        return profile_username;
    }

    public void setProfile_username(String profile_username) {
        this.profile_username = profile_username;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public int getCount_likes() {
        return count_likes;
    }

    public void setCount_likes(int count_likes) {
        this.count_likes = count_likes;
    }

    public int getCount_comments() {
        return count_comments;
    }

    public void setCount_comments(int count_comments) {
        this.count_comments = count_comments;
    }

    public String getHashtag_text() {
        return hashtag_text;
    }

    public void setHashtag_text(String hashtag_text) {
        this.hashtag_text = hashtag_text;
    }

    public String getHashtag_url() {
        return hashtag_url;
    }

    public void setHashtag_url(String hashtag_url) {
        this.hashtag_url = hashtag_url;
    }

    public String getHashtag_image() {
        return hashtag_image;
    }

    public void setHashtag_image(String hashtag_image) {
        this.hashtag_image = hashtag_image;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFeed_type() {
        return feed_type;
    }

    public void setFeed_type(String feed_type) {
        this.feed_type = feed_type;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getPost_link() {
        return post_link;
    }

    public void setPost_link(String post_link) {
        this.post_link = post_link;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getIs_accept() {
        return is_accept;
    }

    public void setIs_accept(int is_accept) {
        this.is_accept = is_accept;
    }

    public long getCreated_date() {
        return created_date;
    }

    public void setCreated_date(long created_date) {
        this.created_date = created_date;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }
}

package com.tagusnow.tagmejob.feed;

import com.tagusnow.tagmejob.auth.SmUser;
import java.io.Serializable;

public class Feed implements Serializable {
    public static final String FACEBOOK = "facebook";
    public static final String TAGUSJOB = "tagusnow_post";
    private int id;
    private int sm_tag_id;
    private int user_id;
    private int is_approved;
    private int sub_tag_id;
    private int delete;
    private int privacy;
    private int is_hide;
    private int is_save;
    private int is_follow;
    private String description;
    private String type;
    private String created_at;
    private String profile_name;
    private String story;
    private String search_query;
    private String detail;
    private SmUser user_post;
    private FeedDetail feed;

    public Feed(){
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSm_tag_id() {
        return sm_tag_id;
    }

    public void setSm_tag_id(int sm_tag_id) {
        this.sm_tag_id = sm_tag_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getIs_approved() {
        return is_approved;
    }

    public void setIs_approved(int is_approved) {
        this.is_approved = is_approved;
    }

    public int getSub_tag_id() {
        return sub_tag_id;
    }

    public void setSub_tag_id(int sub_tag_id) {
        this.sub_tag_id = sub_tag_id;
    }

    public int getDelete() {
        return delete;
    }

    public void setDelete(int delete) {
        this.delete = delete;
    }

    public int getPrivacy() {
        return privacy;
    }

    public void setPrivacy(int privacy) {
        this.privacy = privacy;
    }

    public int getIs_hide() {
        return is_hide;
    }

    public void setIs_hide(int is_hide) {
        this.is_hide = is_hide;
    }

    public int getIs_save() {
        return is_save;
    }

    public void setIs_save(int is_save) {
        this.is_save = is_save;
    }

    public int getIs_follow() {
        return is_follow;
    }

    public void setIs_follow(int is_follow) {
        this.is_follow = is_follow;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public SmUser getUser_post() {
        return user_post;
    }

    public void setUser_post(SmUser user_post) {
        this.user_post = user_post;
    }

    public FeedDetail getFeed() {
        return feed;
    }

    public void setFeed(FeedDetail feed) {
        this.feed = feed;
    }

    public String getSearch_query() {
        return search_query;
    }

    public void setSearch_query(String search_query) {
        this.search_query = search_query;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}

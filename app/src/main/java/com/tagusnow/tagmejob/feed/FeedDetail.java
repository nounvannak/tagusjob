package com.tagusnow.tagmejob.feed;

import android.text.format.DateUtils;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class FeedDetail implements Serializable {
    public static final String NORMAL_POST = "post_normal";
    public static final String JOB_POST = "jobs_post";
    public static final String STAFF_POST = "staff_post";
    public static final int JOB = 1;
    public static final int POST = 2;
    public static final int ALL = 0;
    private String profile_name;
    private String profile_username;
    private String profile_image;
    private String profile_url;
    private int count_likes;
    private int count_comments;
    private String like_result;
    private String hashtag_text;
    private String hashtag_url;
    private Album hastage_image;
    private ArrayList<Album> album;
    private ArrayList<CV> staff_file;
    private String icon;
    private String type;
    private String feed_type;
    private String story;
    private int user_id;
    private String video_url;
    private String post_link;
    private String key;
    private int is_accept;
    private String title;
    private String title_name;
    private String salary;
    private String close_date;
    private String phone;
    private String phone2;
    private String email;
    private int city;
    private String location_des;
    private String created_date;
    private String city_name;
    private String num_employee;
    private String website;
    private String category;
    private boolean is_like;
    private boolean is_save;
    private boolean is_hide;
    private boolean is_follow;
    private boolean is_see_first;
    private boolean liked;
    private int category_id;
    private String description;
    private String requirement;
    private String level;
    private String working_time;
    //////// Embed URL /////////
    private String type_link;
    private String source_url;
    private String source_host;
    private String source_title;

    private String source_text;

    public String getType_link() {
        return type_link;
    }

    public void setType_link(String type_link) {
        this.type_link = type_link;
    }

    public String getSource_host() {
        return source_host;
    }

    public void setSource_host(String source_host) {
        this.source_host = source_host;
    }

    public String getSource_title() {
        return source_title;
    }

    public void setSource_title(String source_title) {
        this.source_title = source_title;
    }

    public String getSource_text() {
        return source_text;
    }

    public void setSource_text(String source_text) {
        this.source_text = source_text;
    }

    public String getSource_thumbnail() {
        return source_thumbnail;
    }

    public void setSource_thumbnail(String source_thumbnail) {
        this.source_thumbnail = source_thumbnail;
    }

    private String source_thumbnail;

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    private int zoom;
    private Double lat;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    private Double lng;

    public FeedDetail(){
        super();
    }

    public String getAlbumText(){
        String albumText = "";
        if (this.album.size() > 4){
            albumText = "View all "+(this.album.size())+" images";
        }
        return albumText;
    }

    public String getTimeline(){
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date d = f.parse(this.created_date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            milliseconds = new Date().getTime();
        }
        return DateUtils.getRelativeTimeSpanString(milliseconds,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public String getCloseDate(){
        String newDate = "";
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
        try {
            Date d = f.parse(this.close_date);
            newDate = newFormat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        return newDate;
    }

    public String getStoryView(){
        String str = "";
        if (hashtag_text==null || hashtag_text.equals("")){
            str = profile_username+"'s post";
        }else{
            if (hashtag_text.length() > 17){
                str = hashtag_text.substring(0,17)+"...";
            }else{
                str = hashtag_text;
            }
        }
        return str;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getProfile_username() {
        return profile_username;
    }

    public void setProfile_username(String profile_username) {
        this.profile_username = profile_username;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public int getCount_likes() {
        return count_likes;
    }

    public void setCount_likes(int count_likes) {
        this.count_likes = count_likes;
    }

    public int getCount_comments() {
        return count_comments;
    }

    public void setCount_comments(int count_comments) {
        this.count_comments = count_comments;
    }

    public void setHashtag_text(String hashtag_text) {
        this.hashtag_text = hashtag_text;
    }

    public String getHashtag_text() {
        String text = "Read more";
        if (hashtag_text!=null && hashtag_text.length() > 150){
            text = hashtag_text.substring(0,150)+"...";
        }else{
            text = hashtag_text;
        }
        return text;
    }

    public String getJobDescription(){
        String html = "";
        /*Category*/
        if (this.category!=null){
            html += "<div style='font-size:10px;'>Job Type : "+this.category+"</div></br>";
        }
        /*Job*/
        if (this.title!=null) {
            html += "<div style='font-size:10px;'>Title : "+this.title+"</div></br>";
        }
        /*Salary*/
        if (this.salary!=null) {
            html += "<div style='font-size:10px;'>Salary : "+this.salary+"</div></br>";
        }
        /*Close date*/
        if (this.close_date!=null) {
            html += "<div style='font-size:10px;'>Close Date : "+this.getCloseDate()+"</div></br>";
        }
        /*Phone*/
        if (this.phone!=null) {
            html += "<div style='font-size:10px;'>Phone : "+this.phone;
            if (this.phone2!=null){
                html += "<span> , "+this.phone2+"</span>";
            }
            html += "</div></br>";
        }
        /*Email*/
        if (this.email!=null) {
            html += "<div style='font-size:10px;'>Email : "+this.email+"</div></br>";
        }
        /*City*/
        if (this.city_name!=null) {
            html += "<div style='font-size:10px;'>City : "+this.city_name+"</div></br>";
        }
        /*Address*/
        if (this.location_des!=null) {
            html += "<div style='font-size:10px;'>Address : "+this.location_des+"</div></br>";
        }
        html += "";
        return html;
    }

    public String getOriginalHastTagText(){
        return hashtag_text;
    }

    public String getHashtag_url() {
        return hashtag_url;
    }

    public void setHashtag_url(String hashtag_url) {
        this.hashtag_url = hashtag_url;
    }

    public Album getHastage_image() {
        return hastage_image;
    }

    public void setHastage_image(Album hastage_image) {
        this.hastage_image = hastage_image;
    }

    public ArrayList<Album> getAlbum() {
        return album;
    }

    public void setAlbum(ArrayList<Album> album) {
        this.album = album;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFeed_type() {
        return feed_type;
    }

    public void setFeed_type(String feed_type) {
        this.feed_type = feed_type;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getPost_link() {
        return post_link;
    }

    public void setPost_link(String post_link) {
        this.post_link = post_link;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getIs_accept() {
        return is_accept;
    }

    public void setIs_accept(int is_accept) {
        this.is_accept = is_accept;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getClose_date() {
        return close_date;
    }

    public void setClose_date(String close_date) {
        this.close_date = close_date;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public String getLocation_des() {
        return location_des;
    }

    public void setLocation_des(String location_des) {
        this.location_des = location_des;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isIs_like() {
        return is_like;
    }

    public void setIs_like(boolean is_like) {
        this.is_like = is_like;
    }

    public String getNum_employee() {
        return num_employee;
    }

    public void setNum_employee(String num_employee) {
        this.num_employee = num_employee;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTitle_name() {
        return title_name;
    }

    public void setTitle_name(String title_name) {
        this.title_name = title_name;
    }

    public ArrayList<CV> getStaff_file() {
        return staff_file;
    }

    public void setStaff_file(ArrayList<CV> staff_file) {
        this.staff_file = staff_file;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public boolean isIs_save() {
        return is_save;
    }

    public void setIs_save(boolean is_save) {
        this.is_save = is_save;
    }

    public boolean isIs_hide() {
        return is_hide;
    }

    public void setIs_hide(boolean is_hide) {
        this.is_hide = is_hide;
    }

    public boolean isIs_follow() {
        return is_follow;
    }

    public void setIs_follow(boolean is_follow) {
        this.is_follow = is_follow;
    }

    public boolean isIs_see_first() {
        return is_see_first;
    }

    public void setIs_see_first(boolean is_see_first) {
        this.is_see_first = is_see_first;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public String getLike_result() {
        return like_result;
    }

    public void setLike_result(String like_result) {
        this.like_result = like_result;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getWorking_time() {
        return working_time;
    }

    public void setWorking_time(String working_time) {
        this.working_time = working_time;
    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }
}

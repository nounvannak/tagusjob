package com.tagusnow.tagmejob.feed;

import java.io.Serializable;

public class Image implements Serializable {
    private int height;
    private int width;
    private String src;

    public Image(){
        super();
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}

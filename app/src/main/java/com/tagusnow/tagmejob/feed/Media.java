package com.tagusnow.tagmejob.feed;

import java.io.Serializable;

public class Media implements Serializable {
    private Image image;

    public Media(){
        super();
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}

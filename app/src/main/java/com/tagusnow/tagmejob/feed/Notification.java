package com.tagusnow.tagmejob.feed;

import android.text.format.DateUtils;

import com.tagusnow.tagmejob.auth.SmUser;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Notification implements Serializable {

    public static final String LIKE_USER = "like_user";
    public static final String FOLLOW_USER = "follow_user";
    public static final String LIKE_POST = "like_post";
    public static final String COMMENT_POST = "comment_post";
    public static final String SEE_FIRST = "see_first";
    public static final String SEND_MESSAGE = "send_message";
    public static final String NEW_USER = "new_user";
    public static final String JOB_SEND = "job_sent_notification";
    public static final String SUGGEST_STAFF = "suggest_staff_notification";

    private String id;
    private String notification_id;
    private String channel_id;
    private String group_name;
    private String topic;
    private String target_user_id;
    private String request_user_id;
    private String target_user_profile;
    private String request_user_profile;
    private String type;
    private String body;
    private String title;
    private String seen;
    private String created_time;
    private String created_at;
    private String seen_time;
    private Feed feed;

    public Notification(String id, String notification_id, String channel_id, String group_name, String topic, String target_user_id, String request_user_id, String type, String body, String title, String seen, String created_time, String seen_time) {
        this.id = id;
        this.notification_id = notification_id;
        this.channel_id = channel_id;
        this.group_name = group_name;
        this.topic = topic;
        this.target_user_id = target_user_id;
        this.request_user_id = request_user_id;
        this.type = type;
        this.body = body;
        this.title = title;
        this.seen = seen;
        this.created_time = created_time;
        this.seen_time = seen_time;
    }

    public FeedResponse getFeedResponse() {
        return feedResponse;
    }

    public void setFeedResponse(FeedResponse feedResponse) {
        this.feedResponse = feedResponse;
    }

    public Notification(String id, String notification_id, String channel_id, String group_name, String topic, String target_user_id, String request_user_id, String target_user_profile, String request_user_profile, String type, String body, String title, String seen, String created_time, String created_at, String seen_time, Feed feed, FeedResponse feedResponse, SmUser target_user, SmUser request_user) {
        this.id = id;
        this.notification_id = notification_id;
        this.channel_id = channel_id;
        this.group_name = group_name;
        this.topic = topic;
        this.target_user_id = target_user_id;
        this.request_user_id = request_user_id;
        this.target_user_profile = target_user_profile;
        this.request_user_profile = request_user_profile;
        this.type = type;
        this.body = body;
        this.title = title;
        this.seen = seen;
        this.created_time = created_time;
        this.created_at = created_at;
        this.seen_time = seen_time;
        this.feed = feed;
        this.feedResponse = feedResponse;
        this.target_user = target_user;
        this.request_user = request_user;
    }

    private FeedResponse feedResponse;
    private SmUser target_user;
    private SmUser request_user;

    public Notification(){
        super();
    }

    public Notification(String id, String notification_id, String channel_id, String group_name, String topic, String target_user_id, String request_user_id, String type, String body, String title, String seen, String created_time, String seen_time, FeedResponse feedResponse){
        this.id = id;
        this.notification_id = notification_id;
        this.channel_id = channel_id;
        this.group_name = group_name;
        this.topic = topic;
        this.target_user_id = target_user_id;
        this.request_user_id = request_user_id;
        this.type = type;
        this.body = body;
        this.title = title;
        this.seen = seen;
        this.created_time = created_time;
        this.seen_time = seen_time;
    }

    public Notification(String id, String notification_id, String channel_id, String group_name, String topic, String target_user_id, String request_user_id,String target_user_profile,String request_user_profile, String type, String body, String title, String seen, String created_time, String seen_time){
        this.id = id;
        this.notification_id = notification_id;
        this.channel_id = channel_id;
        this.group_name = group_name;
        this.topic = topic;
        this.target_user_id = target_user_id;
        this.request_user_id = request_user_id;
        this.target_user_profile = target_user_profile;
        this.request_user_profile = request_user_profile;
        this.type = type;
        this.body = body;
        this.title = title;
        this.seen = seen;
        this.created_time = created_time;
        this.seen_time = seen_time;
    }

    public String getTimeline(){
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date d = f.parse(this.created_at);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            milliseconds = new Date().getTime();
        }
        return DateUtils.getRelativeTimeSpanString(milliseconds,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTarget_user_id() {
        return target_user_id;
    }

    public void setTarget_user_id(String target_user_id) {
        this.target_user_id = target_user_id;
    }

    public String getRequest_user_id() {
        return request_user_id;
    }

    public void setRequest_user_id(String request_user_id) {
        this.request_user_id = request_user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSeen() {
        return seen.equals("1");
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getSeen_time() {
        return seen_time;
    }

    public void setSeen_time(String seen_time) {
        this.seen_time = seen_time;
    }

    public String getTarget_user_profile() {
        return target_user_profile;
    }

    public void setTarget_user_profile(String target_user_profile) {
        this.target_user_profile = target_user_profile;
    }

    public String getRequest_user_profile() {
        return request_user_profile;
    }

    public void setRequest_user_profile(String request_user_profile) {
        this.request_user_profile = request_user_profile;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public SmUser getTarget_user() {
        return target_user;
    }

    public void setTarget_user(SmUser target_user) {
        this.target_user = target_user;
    }

    public SmUser getRequest_user() {
        return request_user;
    }

    public void setRequest_user(SmUser request_user) {
        this.request_user = request_user;
    }
}

package com.tagusnow.tagmejob.feed;

import com.tagusnow.tagmejob.auth.SmUser;

import java.io.Serializable;

public class RecentSearch implements Serializable {
    public static final int WORD = 0;
    public static final int USER = 1;
    public static final int JOBS = 2;
    public static final int OTHER = 3;
    public static final int RESUME = 4;
    public static final int ALL = -1;
    private int id;
    private String search_text;
    private String search_image;
    private int search_type;
    private int master_id;
    private int trash;
    private SmUser user;
    private Feed feed;
    private int search_user_id;
    private String created_at;

    public RecentSearch(){
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSearch_text() {
        return search_text;
    }

    public void setSearch_text(String search_text) {
        this.search_text = search_text;
    }

    public String getSearch_image() {
        return search_image;
    }

    public void setSearch_image(String search_image) {
        this.search_image = search_image;
    }

    public int getSearch_type() {
        return search_type;
    }

    public void setSearch_type(int search_type) {
        this.search_type = search_type;
    }

    public int getMaster_id() {
        return master_id;
    }

    public void setMaster_id(int master_id) {
        this.master_id = master_id;
    }

    public SmUser getUser() {
        return user;
    }

    public void setUser(SmUser user) {
        this.user = user;
    }

    public int getSearch_user_id() {
        return search_user_id;
    }

    public void setSearch_user_id(int search_user_id) {
        this.search_user_id = search_user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }
}

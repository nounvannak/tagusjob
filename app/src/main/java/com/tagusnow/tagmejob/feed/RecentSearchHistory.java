package com.tagusnow.tagmejob.feed;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RecentSearchHistory {
    private String group_date;
    private RecentSearchPaginate history;

    public String getTimeline(){
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            Date d = f.parse(this.group_date);
            milliseconds = d.getTime();
            if (DateUtils.isToday(milliseconds)){
                return "Today";
            }
        } catch (ParseException e) {
            e.printStackTrace();
            milliseconds = new Date().getTime();
        }
        return DateUtils.getRelativeTimeSpanString(milliseconds,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public RecentSearchHistory(){
        super();
    }

    public String getGroup_date() {
        return group_date;
    }

    public void setGroup_date(String group_date) {
        this.group_date = group_date;
    }

    public RecentSearchPaginate getHistory() {
        return history;
    }

    public void setHistory(RecentSearchPaginate history) {
        this.history = history;
    }
}

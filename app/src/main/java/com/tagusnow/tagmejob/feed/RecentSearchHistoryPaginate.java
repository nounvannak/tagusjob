package com.tagusnow.tagmejob.feed;

import java.util.List;

public class RecentSearchHistoryPaginate {
    private int current_page;
    private int last_page;
    private int from;
    private int to;
    private int total;
    private String per_page;
    private String next_page_url;
    private String path;
    private String prev_page_url;
    private List<RecentSearchHistory> data;

    public RecentSearchHistoryPaginate(){
        super();
    }

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public int getLast_page() {
        return last_page;
    }

    public void setLast_page(int last_page) {
        this.last_page = last_page;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getPer_page() {
        return per_page;
    }

    public void setPer_page(String per_page) {
        this.per_page = per_page;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public List<RecentSearchHistory> getData() {
        return data;
    }

    public void setData(List<RecentSearchHistory> data) {
        this.data = data;
    }
}

package com.tagusnow.tagmejob.feed;

import java.io.Serializable;

public class SavePost implements Serializable {
    private int id;
    private int user_post_id;
    private int post_id;
    private int user_save_id;
    private int trash;
    private String created_at;
    private String updated_at;

    public SavePost(){
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_post_id() {
        return user_post_id;
    }

    public void setUser_post_id(int user_post_id) {
        this.user_post_id = user_post_id;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public int getUser_save_id() {
        return user_save_id;
    }

    public void setUser_save_id(int user_save_id) {
        this.user_save_id = user_save_id;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

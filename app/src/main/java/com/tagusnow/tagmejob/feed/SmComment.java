package com.tagusnow.tagmejob.feed;

import android.text.format.DateUtils;

import com.tagusnow.tagmejob.auth.SmUser;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SmComment implements Serializable {
    private Comment comment;
    private ArrayList<SmUser> comment_user;
    private SmUser auth_user;
    private int count_comments;

    public SmComment() {
        super();
    }

    public boolean isAuth(){
        return this.auth_user!=null;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public SmUser getAuthUser() {
        return auth_user;
    }

    public void setAuthUser(SmUser auth_user) {
        this.auth_user = auth_user;
    }

    public int getCountComments() {
        return count_comments;
    }

    public void setCountComments(int count_comments) {
        this.count_comments = count_comments;
    }

    public ArrayList<SmUser> getComment_user() {
        return comment_user;
    }

    public void setComment_user(ArrayList<SmUser> comment_user) {
        this.comment_user = comment_user;
    }

    public class Comment implements Serializable{
        private int current_page;
        private int from;
        private int last_page;
        private String next_page_url;
        private String path;
        private int per_page;
        private String prev_page_url;
        private int to;
        private int total;
        private ArrayList<Data> data;

        public Comment() {
            super();
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public String getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(String prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        public class Data{
            private int id;
            private int feed_id;
            private int user_id;
            private String image;
            private String des;
            private String c_date;
            private String updated_at;

            public Data() {
                super();
            }

            public boolean isNullImage(){
                return this.image==null || this.image.equals("");
            }

            public boolean isNullDescription(){
                return this.des==null;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getDes() {
                return des;
            }

            public void setDes(String des) {
                this.des = des;
            }

            public String getC_date() {
                return c_date;
            }

            public String getTimeline(){
                long milliseconds = 0;
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                try {
                    Date d = f.parse(this.c_date);
                    milliseconds = d.getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                    milliseconds = new Date().getTime();
                }
                return DateUtils.getRelativeTimeSpanString(milliseconds,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
            }

            public void setC_date(String c_date) {
                this.c_date = c_date;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getFeed_id() {
                return feed_id;
            }

            public void setFeed_id(int feed_id) {
                this.feed_id = feed_id;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}

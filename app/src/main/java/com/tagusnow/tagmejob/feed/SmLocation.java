package com.tagusnow.tagmejob.feed;

import java.io.Serializable;
import java.util.ArrayList;

public class SmLocation implements Serializable {

    private ArrayList<Data> data;

    public SmLocation() {
        super();
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data implements Serializable{
        private int id;
        private String name;
        private String description;

        public Data() {
            super();
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}

package com.tagusnow.tagmejob.feed;
import com.tagusnow.tagmejob.auth.SmUser;

import java.util.ArrayList;

public class SmTagFeed {
    private Feed feed;
    private ArrayList<SmUser> user;
    private SmUser auth_user;
    private ArrayList<Like> likes;
    private ArrayList<Comment> comments;
    private ArrayList<FacebookFeed> facebook_feed;
    private ArrayList<TagusnowFeed> tagusnow_feed;
    public SmTagFeed(){
        super();
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public ArrayList<SmUser> getUser() {
        return user;
    }

    public void setUser(ArrayList<SmUser> user) {
        this.user = user;
    }

    public ArrayList<Like> getLikes() {
        return likes;
    }

    public void setLikes(ArrayList<Like> likes) {
        this.likes = likes;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public ArrayList<FacebookFeed> getFacebook_feed() {
        return facebook_feed;
    }

    public void setFacebook_feed(ArrayList<FacebookFeed> facebook_feed) {
        this.facebook_feed = facebook_feed;
    }

    public ArrayList<TagusnowFeed> getTagusnow_feed() {
        return tagusnow_feed;
    }

    public void setTagusnow_feed(ArrayList<TagusnowFeed> tagusnow_feed) {
        this.tagusnow_feed = tagusnow_feed;
    }

    public SmUser getAuth_user() {
        return auth_user;
    }

    public void setAuth_user(SmUser auth_user) {
        this.auth_user = auth_user;
    }

    public class Like{
        private int count_likes;
        private boolean is_like;

        public Like() {
            super();
        }

        public int getCount_likes() {
            return count_likes;
        }

        public void setCount_likes(int count_likes) {
            this.count_likes = count_likes;
        }

        public boolean isIs_like() {
            return is_like;
        }

        public void setIs_like(boolean is_like) {
            this.is_like = is_like;
        }
    }

    public class Comment{
        private int count_comments;

        public Comment() {
            this.count_comments = 0;
        }

        public int getCount_comments() {
            return count_comments;
        }

        public void setCount_comments(int count_comments) {
            this.count_comments = count_comments;
        }
    }

    public class Feed{
        private int current_page;
        private int from;
        private int last_page;
        private String next_page_url;
        private String path;
        private int per_page;
        private String prev_page_url;
        private int to;
        private int total;
        private ArrayList<Data> data;

        public Feed() {
            super();
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public String getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(String prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        public class Data{

            private int id;
            private String post_id;
            private int sm_tag_id;
            private String description;
            private String type;
            private String user_id;
            private String profile_name;
            private String story;
            private String feed;
            private int is_hide;
            private int is_save;
            private int is_follow;

            public Data() {
                super();
            }

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public int getSm_tag_id() {
                return sm_tag_id;
            }

            public void setSm_tag_id(int sm_tag_id) {
                this.sm_tag_id = sm_tag_id;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getProfile_name() {
                return profile_name;
            }

            public void setProfile_name(String profile_name) {
                this.profile_name = profile_name;
            }

            public String getStory() {
                return story;
            }

            public void setStory(String story) {
                this.story = story;
            }

            public String getFeed() {
                return feed;
            }

            public void setFeed(String feed) {
                this.feed = feed;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getIs_hide() {
                return is_hide;
            }

            public void setIs_hide(int is_hide) {
                this.is_hide = is_hide;
            }

            public int getIs_save() {
                return is_save;
            }

            public void setIs_save(int is_save) {
                this.is_save = is_save;
            }

            public int getIs_follow() {
                return is_follow;
            }

            public void setIs_follow(int is_follow) {
                this.is_follow = is_follow;
            }
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

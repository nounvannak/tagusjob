package com.tagusnow.tagmejob.feed;

import java.util.ArrayList;

public class SmTagSubCategory {

    private ArrayList<Category> data;

    public SmTagSubCategory(){
        super();
    }

    public ArrayList<Category> getData() {
        return data;
    }

    public void setData(ArrayList<Category> data) {
        this.data = data;
    }
}

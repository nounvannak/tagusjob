package com.tagusnow.tagmejob.feed;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class TagusnowFeed {
    private String profile_name;
    private String profile_username;
    private String profile_image;
    private String profile_url;
    private String category;
    private int count_likes;
    private int count_comments;
    private String hashtag_text;
    private String hashtag_url;
    private String icon;
    private String type;
    private String feed_type;
    private String story;
    private long user_id;
    private String video_url;
    private String post_link;
    private String key;
    private int is_accept;
    private String title;
    private String salary;
    private String close_date;
    private String phone;
    private String phone2;
    private String email;
    private String city;
    private String city_name;
    private String location_des;
    private String created_date;
    /*private HashTagImage hashtag_image;*/
    private ArrayList<Album> album;

    public TagusnowFeed() {
        super();
    }

    public String getAlbumText(){
        String albumText = "";
        if (this.album.size() > 4){
            albumText = "View all "+(this.album.size())+" images";
        }
        return albumText;
    }

    public String getTimeline(){
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date d = f.parse(this.created_date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            milliseconds = new Date().getTime();
        }
        return DateUtils.getRelativeTimeSpanString(milliseconds,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public String getStoryView(){
        String str = "";
        if (hashtag_text==null || hashtag_text.equals("")){
            str = profile_username+"'s post";
        }else{
            if (hashtag_text.length() > 17){
                str = hashtag_text.substring(0,17)+"...";
            }else{
                str = hashtag_text;
            }
        }
        return str;
    }

    /*public HashTagImage getHashtag_image() {
        return hashtag_image;
    }

    public void setHashtag_image(HashTagImage hashtag_image) {
        this.hashtag_image = hashtag_image;
    }*/

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getLocation_des() {
        return location_des;
    }

    public void setLocation_des(String location_des) {
        this.location_des = location_des;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getClose_date() {
        return close_date;
    }

    public void setClose_date(String close_date) {
        this.close_date = close_date;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIs_accept() {
        return is_accept;
    }

    public void setIs_accept(int is_accept) {
        this.is_accept = is_accept;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPost_link() {
        return post_link;
    }

    public void setPost_link(String post_link) {
        this.post_link = post_link;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getFeed_type() {
        return feed_type;
    }

    public void setFeed_type(String feed_type) {
        this.feed_type = feed_type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getHashtag_url() {
        return hashtag_url;
    }

    public void setHashtag_url(String hashtag_url) {
        this.hashtag_url = hashtag_url;
    }

    public String getHashtag_text() {
        String text = "";
        if (hashtag_text!=null && hashtag_text.length() > 150){
            text = hashtag_text.substring(0,150)+"...";
            text += "<p style='text-color:#19539a;text-align:center;'>Read more</p>";
        }else{
            text = hashtag_text;
        }
        return text;
    }

    public String getJobDescription(){
        String html = "";
                /*Category*/
                if (this.category!=null){
                    html += "<p>Job Type : "+this.category+"</p>";
                }
                /*Job*/
                if (this.title!=null) {
                    html += "<p>Title : "+this.title+"</p>";
                }
                /*Salary*/
                if (this.salary!=null) {
                    html += "<p>Salary : "+this.salary+"</p>";
                }
                /*Close date*/
                if (this.close_date!=null) {
                    html += "<p>Close Date : "+this.close_date+"</p>";
                }
                /*Phone*/
                if (this.phone!=null) {
                    html += "<p>Phone : "+this.phone;
                    if (this.phone2!=null){
                        html += "<span> , "+this.phone2+"</span>";
                    }
                    html += "</p>";
                }
                /*Email*/
                if (this.email!=null) {
                    html += "<p>Email : "+this.email+"</p>";
                }
                /*City*/
                if (this.city_name!=null) {
                    html += "<p>City : "+this.city_name+"</p>";
                }
                /*Address*/
                if (this.location_des!=null) {
                    html += "<p>Address : "+this.location_des+"</p>";
                }
                html += "";
        return html;
    }

    public String getOriginalHastTagText(){
        return hashtag_text;
    }

    public void setHashtag_text(String hashtag_text) {
        this.hashtag_text = hashtag_text;
    }

    public int getCount_comments() {
        return count_comments;
    }

    public void setCount_comments(int count_comments) {
        this.count_comments = count_comments;
    }

    public int getCount_likes() {
        return count_likes;
    }

    public void setCount_likes(int count_likes) {
        this.count_likes = count_likes;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getProfile_username() {
        return profile_username;
    }

    public void setProfile_username(String profile_username) {
        this.profile_username = profile_username;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public ArrayList<Album> getAlbum() {
        return album;
    }

    public void setAlbum(ArrayList<Album> album) {
        this.album = album;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    /*public class HashTagImage{
        private String type;
        private String url;
        private Media media;
        private Target target;

        public HashTagImage() {
            super();
        }

        public Target getTarget() {
            return target;
        }

        public void setTarget(Target target) {
            this.target = target;
        }

        public Media getMedia() {
            return media;
        }

        public void setMedia(Media media) {
            this.media = media;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public class Media{
            private Image image;

            public Media() {
                super();
            }

            public Image getImage() {
                return image;
            }

            public void setImage(Image image) {
                this.image = image;
            }

            public class Image{
                private String src;

                public Image() {
                    super();
                }

                public String getSrc() {
                    return src;
                }

                public void setSrc(String src) {
                    this.src = src;
                }
            }
        }

        public class Target{
            private String url;

            public Target() {
                super();
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }*/

    public class Album{
        private String type;
        private String url;
        private Media media;
        private Target target;

        public Album() {
            super();
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Media getMedia() {
            return media;
        }

        public void setMedia(Media media) {
            this.media = media;
        }

        public Target getTarget() {
            return target;
        }

        public void setTarget(Target target) {
            this.target = target;
        }

        public class Media{
            private Image image;

            public Media() {
                super();
            }

            public Image getImage() {
                return image;
            }

            public void setImage(Image image) {
                this.image = image;
            }

            public class Image{
                private String src;
                private int height;
                private int width;

                public Image() {
                    super();
                }

                public String getSrc() {
                    return src;
                }

                public void setSrc(String src) {
                    this.src = src;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }
            }
        }

        public class Target{
            private String url;
            private String id;

            public Target() {
                super();
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }
}

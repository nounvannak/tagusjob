package com.tagusnow.tagmejob.feed;

public class Target {
    private String id;
    private String url;

    public Target(){
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

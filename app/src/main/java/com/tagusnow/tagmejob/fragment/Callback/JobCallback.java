package com.tagusnow.tagmejob.fragment.Callback;

import android.support.v7.widget.RecyclerView;

import com.tagusnow.tagmejob.V2.Company.CompanyRecentPostListener;
import com.tagusnow.tagmejob.V2.Model.CompanyPaginate;

public interface JobCallback {
    void setCompanyPagite(CompanyPaginate companyPagite);
    void setCompanyListener(CompanyRecentPostListener listener);
    void setAdapter(RecyclerView.Adapter adapter);
}

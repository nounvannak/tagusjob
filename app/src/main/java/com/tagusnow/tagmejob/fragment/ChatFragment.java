package com.tagusnow.tagmejob.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.adapter.ChatAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.LoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatFragment extends TagUsJobFragment implements SwipeRefreshLayout.OnRefreshListener, LoadingListener<LoadingView> {

    private static final String TAG = ChatFragment.class.getSimpleName();
    private SmUser user;
    private UserPaginate paginate;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private ChatAdapter adapter;
    private UserService userService = ServiceGenerator.createService(UserService.class);
    private Socket socket;
    private LoadingView loadingView;
    private Emitter.Listener NewMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj.getInt("user_id")==user.getId()){
                            if (obj.has("chat_list")){
                                UserPaginate paginate = new Gson().fromJson(new String(Base64.decode(obj.getString("chat_list"),Base64.DEFAULT)),UserPaginate.class);
                                First(paginate);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private void InitSocket(){
        socket = new App().getSocket();
        socket.on("error", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, Arrays.toString(args));
            }
        });
        socket.on("New Message",NewMessage);
        socket.connect();
    }

    public ChatFragment() {
    }

    public static ChatFragment newInstance() {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        if (socket.connected()){
            socket.disconnect();
        }
        Log.e(TAG,"destroy");
        super.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.InitSocket();
        Log.e(TAG,"create");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setActivity(getActivity());
        View view =  inflater.inflate(R.layout.fragment_chat, container, false);
        this.initTemp();
        this.InitUI(view);
        refreshLayout.setRefreshing(true);
        onRefresh();
        return view;
    }

    private void First(){
        userService.ChatList(this.user.getId(),0,10).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    First(response.body());
                }else {
                    try {
                        Log.e("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void First(UserPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adapter = new ChatAdapter(this.activity,paginate,this.user,this);
        recyclerView.setAdapter(this.adapter);
    }

    private void Next(){
        int page = this.paginate.getCurrent_page() + 1;
        userService.ChatList(this.user.getId(),0,10,page).enqueue(new Callback<UserPaginate>() {
            @Override
            public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
                if (response.isSuccessful()){
                    Next(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserPaginate> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void Next(UserPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        loadingView.setLoading(paginate.getTotal() > paginate.getTo());
        this.adapter.Update(paginate).notifyDataSetChanged();
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (paginate.getTo() - 1)) && (paginate.getTo() < paginate.getTotal())) {
                        if (paginate.getNext_page_url()!=null || !paginate.getNext_page_url().equals("")){
                            if (lastPage!=paginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = paginate.getCurrent_page();
                                    Next();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void initTemp(){
        this.user = new Auth(getContext()).checkAuth().token();
    }

    private void InitUI(View view){
        refreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.refresh);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(false);
                First();
            }
        }, 2000);
    }

    @Override
    public void reload(LoadingView view) {

    }
}

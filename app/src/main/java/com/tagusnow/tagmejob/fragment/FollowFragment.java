package com.tagusnow.tagmejob.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.ConnectSuggestFaceAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.v3.user.UserCardTwo;
import com.tagusnow.tagmejob.v3.user.UserCardTwoListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowFragment extends TagUsJobFragment implements SwipeRefreshLayout.OnRefreshListener, UserCardTwoListener {

    private static final String TAG = FollowFragment.class.getSimpleName();
    public static final String FRAGMENT = FollowFragment.class.getSimpleName();
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private UserPaginate userPaginate;
    private List<SmUser> users;
    private ConnectSuggestFaceAdapter adapter;
    private SmUser Auth;
    /*Service*/
    private FollowService service = ServiceGenerator.createService(FollowService.class);
    private Callback<UserPaginate> mNewCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewConnectSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> mNextCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextConnectSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    /* Socket.io */
    Socket socket;
    private Emitter.Listener FOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int userID = obj.getInt("auth_user");
                    JSONObject json = obj.getJSONObject("user");
                    if (Auth.getId() == userID){
                        if (json != null){
                            SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                            SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                            if (follower != null && following != null){
                                int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        SmUser user = lists.get(0);
                                        int index = users.indexOf(user);
                                        follower.setIs_follow(true);
                                        if (following.getId() == userID){
                                            users.set(index,follower);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    for (SmUser user: users){
                                        if (user.getId() == id){
                                            int index = users.indexOf(user);
                                            follower.setIs_follow(true);
                                            if (following.getId() == userID){
                                                users.set(index,follower);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener UNFOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int userID = obj.getInt("auth_user");
                    JSONObject json = obj.getJSONObject("user");
                    if (Auth.getId() == userID){
                        if (json != null){
                            SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                            SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                            if (follower != null && following != null){
                                int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        SmUser user = lists.get(0);
                                        int index = users.indexOf(user);
                                        follower.setIs_follow(false);
                                        if (following.getId() == userID){
                                            users.set(index,follower);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    for (SmUser user: users){
                                        if (user.getId() == id){
                                            int index = users.indexOf(user);
                                            follower.setIs_follow(false);
                                            if (following.getId() == userID){
                                                users.set(index,follower);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    };
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Follow User",FOLLOW_USER);
        socket.on("Unfollow User",UNFOLLOW_USER);
        socket.connect();
    }
    /* End Socket.io */

    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    nextConnectSuggest();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initTemp(){
        Auth = new Auth(getContext()).checkAuth().token();
    }

    private void initNewConnectSuggest(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users = userPaginate.getData();
        this.checkNext();
        this.adapter = new ConnectSuggestFaceAdapter(activity,users,this);
        if (userPaginate.getTotal() > 0){
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),2);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(layoutManager);
        }else {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(this.adapter);
    }

    private void initNextConnectSuggest(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users.addAll(userPaginate.getData());
        this.checkNext();
        this.adapter.NextData(userPaginate.getData());
        this.adapter.notifyDataSetChanged();
    }

    private void newConnectSuggest(){
        int user = this.Auth!=null ? this.Auth.getId() : 0;
        int limit = 10;
        service.callSuggest(user,limit).enqueue(mNewCallback);
    }

    private void nextConnectSuggest(){
        int user = this.Auth!=null ? this.Auth.getId() : 0;
        int limit = 10;
        int page = this.userPaginate.getCurrent_page() + 1;
        service.callSuggest(user,limit,page).enqueue(mNextCallback);
    }

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            newConnectSuggest();
            refreshLayout.setRefreshing(false);
        }
    };

    public FollowFragment() {
        // Required empty public constructor
    }

    public static FollowFragment newInstance(){
        FollowFragment fragment = new FollowFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_follow, container, false);
        setActivity(getActivity());
        initTemp();
        initView(view);
        initSocket();
        newConnectSuggest();
        return view;
    }

    private void initView(View view){
        initRefresh(view);
        initRecycler(view);
    }

    private void initRefresh(View view){
        refreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecycler(View view){
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRun,2000);
    }

    @Override
    public void connect(UserCardTwo view, SmUser user) {
        view.DoFollow(Auth,user);
    }

    @Override
    public void profile(UserCardTwo view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void remove(UserCardTwo view, SmUser user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Close User").setMessage("Are you sure to close this user?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                users.remove(user);
                adapter.notifyDataSetChanged();
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }
}

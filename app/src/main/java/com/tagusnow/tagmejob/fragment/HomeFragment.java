package com.tagusnow.tagmejob.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.HomeService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.FeedAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.LoadingView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.rmiri.skeleton.SkeletonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends TagUsJobFragment implements SwipeRefreshLayout.OnRefreshListener, LoadingListener<LoadingView> {

    private static final String TAG = HomeFragment.class.getSimpleName();
    private static final int LIMIT = 10;
    private SmUser authUser;
    private List<SmUser> listFollowSuggests = new ArrayList<>();
    private SmTagSubCategory category;
    private FeedResponse feedResponse;
    private FeedAdapter feedAdapter;
    private SwipeRefreshLayout rllFeedBody;
    private RecyclerView mRecyclerView;
    private SkeletonGroup skeletonGroup;
    private HomeService homeService = ServiceGenerator.createService(HomeService.class);
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            rllFeedBody.setRefreshing(false);
            callFeed();
        }
    };

    private Callback<FeedResponse> FirstPage = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                initFeed(response.body());
            }else {
                try {
                    ShowAlert(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            ShowAlert("Connection Error!",t.getMessage());
        }
    };
    private Callback<FeedResponse> NextPage = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                next(response.body());
            }else {
                try {
                    ShowAlert(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private void initTemp(){
        authUser = new Auth(getContext()).checkAuth().token();
        category = new Repository(getContext()).restore().getCategory();
    }

    private void initView(View view){
        initSwipeRefresh(view);
        initRecyclerView(view);
    }

    private void initFeed(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.checkNextFeed();
        this.listFollowSuggest();
        feedAdapter = new FeedAdapter(this.activity,this.feedResponse,this.authUser,this)
                            .listFollowSuggest(this.listFollowSuggests);
        mRecyclerView.setAdapter(feedAdapter);
    }

    private void listFollowSuggest(){
        int limit = 3;
        int user = authUser!=null ? authUser.getId() : 0;
        homeService.listFollowSuggest(user,limit).enqueue(new Callback<List<SmUser>>() {
            @Override
            public void onResponse(Call<List<SmUser>> call, Response<List<SmUser>> response) {
                if (response.isSuccessful()){
                    Log.w(TAG,"listFollowSuggest::"+response.message());
                    listFollowSuggests = response.body();
                }else {
                    Log.w(TAG,response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<List<SmUser>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void callFeed(){
        int auth_user = authUser!=null ? authUser.getId() : 0;
        feedService.Home(auth_user,LIMIT).enqueue(FirstPage);
    }

    private void nextFeed(){
        int auth_user = authUser!=null ? authUser.getId() : 0;
        int page = this.feedResponse.getCurrent_page() + 1;
        feedService.Home(auth_user,LIMIT,page).enqueue(NextPage);
    }

    private void next(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.checkNextFeed();
        this.feedAdapter.update(this.activity,this.feedResponse,this.authUser).notifyDataSetChanged();
    }

    private void checkNextFeed(){
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    nextFeed();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSwipeRefresh(View view){
        rllFeedBody = (SwipeRefreshLayout)view.findViewById(R.id.rllFeedBody);
        rllFeedBody.setOnRefreshListener(this);
    }

    private void initRecyclerView(View view){
        mRecyclerView = (RecyclerView)view.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.activity);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    public void scrollToTop(){
        if (mRecyclerView!=null)
        mRecyclerView.smoothScrollToPosition(0);
    }

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivity(getActivity());
        this.initTemp();
        this.listFollowSuggest();
        this.callFeed();
    }

    private void showSkeleton(LayoutInflater inflater){
        View view = inflater.inflate(R.layout.skeleton_home,null);
        skeletonGroup = view.findViewById(R.id.skeletonGroup);
        skeletonGroup.setAutoPlay(true);
        skeletonGroup.setShowSkeleton(true);
        skeletonGroup.startAnimation();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onRefresh() {
        rllFeedBody.setRefreshing(true);
        new Handler().postDelayed(mRun, 2000);
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void reload(LoadingView view) {

    }
}

package com.tagusnow.tagmejob.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.NotificationService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.NotificationFaceAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.feed.NotificationResponse;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.like.LikeResultCell;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.v3.notification.NCommentCell;
import com.tagusnow.tagmejob.v3.notification.NFollowCell;
import com.tagusnow.tagmejob.v3.notification.NJobCell;
import com.tagusnow.tagmejob.v3.notification.NListener;
import com.tagusnow.tagmejob.v3.notification.NMessageCell;
import com.tagusnow.tagmejob.v3.notification.NStaffCell;
import com.tagusnow.tagmejob.v3.notification.NSystemCell;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NofiticationFragment extends TagUsJobFragment implements SwipeRefreshLayout.OnRefreshListener, NListener, LikeResultListener, LoadingListener<LoadingView> {

    private static final String TAG = NofiticationFragment.class.getSimpleName();
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private NotificationResponse notificationResponse;
    private List<Notification> notifications = new ArrayList<>();
    private NotificationFaceAdapter adapter;
    private SmUser Auth;
    private LoadingView loadingView;
    /*Service*/
    private NotificationService service = ServiceGenerator.createService(NotificationService.class);
    private Callback<NotificationResponse> mNewCallback = new Callback<NotificationResponse>() {
        @Override
        public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewNotification(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<NotificationResponse> call, Throwable t) {
            loadingView.setError(true);
            t.printStackTrace();
        }
    };
    private Callback<NotificationResponse> mNextCallback = new Callback<NotificationResponse>() {
        @Override
        public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextNotification(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<NotificationResponse> call, Throwable t) {
            loadingView.setError(true);
            t.printStackTrace();
        }
    };

    static int lastPage;
    static boolean isRequest = false;
    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (notificationResponse.getTo() - 1)) && (notificationResponse.getTo() < notificationResponse.getTotal())) {
                        if (notificationResponse.getNext_page_url()!=null || !notificationResponse.getNext_page_url().equals("")){
                            if (lastPage!=notificationResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = notificationResponse.getCurrent_page();
                                    nextNotification();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initTemp(){
        Auth = new Auth(getContext()).checkAuth().token();
    }

    private void initNewNotification(NotificationResponse notificationResponse){
        this.notificationResponse = notificationResponse;
        this.notifications = notificationResponse.getData();
        checkNext();
        adapter.NewData(notifications);
        recyclerView.setAdapter(this.adapter);
        loadingView.setLoading(notificationResponse.getTotal() > notificationResponse.getTo());
    }

    private void initNextNotification(NotificationResponse notificationResponse){
        this.notificationResponse = notificationResponse;
        this.notifications.addAll(notificationResponse.getData());
        this.checkNext();
        loadingView.setLoading(notificationResponse.getTotal() > notificationResponse.getTo());
        this.adapter.NextData(notificationResponse.getData());
    }

    private void newNotification(){
        service.getNotificationList(Auth.getId(),10).enqueue(mNewCallback);
    }

    private void nextNotification(){
        boolean isNext = notificationResponse.getCurrent_page() != notificationResponse.getLast_page();
        if (isNext){
            int page = this.notificationResponse.getCurrent_page() + 1;
            service.getNotificationList(Auth.getId(),10,page).enqueue(mNextCallback);
        }
    }

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            newNotification();
            refreshLayout.setRefreshing(false);
        }
    };


    public NofiticationFragment() {
        // Required empty public constructor
    }

    public static NofiticationFragment newInstance(){
        NofiticationFragment fragment = new NofiticationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nofitication, container, false);
        initTemp();
        initView(view);
        adapter = new NotificationFaceAdapter(getActivity(),this,this);
        loadingView = adapter.getLoadingView();
        newNotification();
        return view;
    }

    private void initView(View view){
        initRefresh(view);
        initRecycler(view);
    }

    private void initRefresh(View view){
        refreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecycler(View view){
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(mRun,2000);
    }

    @Override
    public void connect(TagUsJobRelativeLayout view, Object user) {
        Notification obj = (Notification)user;
        view.DoFollow(Auth,obj.getTarget_user());
    }

    @Override
    public void reply(TagUsJobRelativeLayout view, Object conversation) {
        Notification obj = (Notification)conversation;
    }

    @Override
    public void select(TagUsJobRelativeLayout view, Object notification) {
        Notification obj = (Notification)notification;
        if (view.getClass() == NCommentCell.class){
            if (obj.getType().equals(Notification.COMMENT_POST)){
                view.DetailFeedNormal(obj.getFeed());
            }else{
                view.ShowAllUserLiked(Auth,obj.getFeed(),this);
            }
        }else if (view.getClass() == NFollowCell.class){
            view.OpenProfile(Auth,obj.getTarget_user());
        }else if (view.getClass() == NJobCell.class){
            view.DetailFeedJob(Auth,obj.getFeed());
        }else if (view.getClass() == NMessageCell.class){

        }else if (view.getClass() == NStaffCell.class){

        }else if (view.getClass() == NSystemCell.class){

        }
    }

    @Override
    public void delete(TagUsJobRelativeLayout view, Object notification) {
        Notification obj = (Notification)notification;
        notifications.remove(obj);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void select(LikeResultCell cell, SmUser user) {
        cell.OpenProfile(Auth,user);
    }

    @Override
    public void connect(LikeResultCell cell, SmUser user) {
        cell.DoFollow(Auth,user);
    }

    @Override
    public void reload(LoadingView view) {
        nextNotification();
    }
}

package com.tagusnow.tagmejob.fragment.Search.Suggest.User;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.R;

public class NoResult extends Fragment {

    public NoResult() {
        // Required empty public constructor
    }

    public static NoResult newInstance(){
        NoResult fragment = new NoResult();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_no_result, container, false);
    }
}

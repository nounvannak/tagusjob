package com.tagusnow.tagmejob.fragment.Search.Suggest.User;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.R;

public class ResultFragment extends Fragment{

    private static final String TAG = ResultFragment.class.getSimpleName();
    private RecyclerView recycler;

    public ResultFragment() {
        // Required empty public constructor
    }

    public static ResultFragment newInstance(){
        ResultFragment fragment = new ResultFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public RecyclerView getRecycler() {
        return recycler;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        initRecycler(view);
    }

    private void initRecycler(View view){
        recycler = (RecyclerView)view.findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(layoutManager);
    }
}

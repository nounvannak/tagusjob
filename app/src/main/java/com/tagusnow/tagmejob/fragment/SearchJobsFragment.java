package com.tagusnow.tagmejob.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.SearchJobAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import java.io.IOException;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchJobsFragment extends TagUsJobFragment implements View.OnClickListener {


    private static final String TAG = SearchJobsFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private FeedResponse feedResponse;
    private RecentSearchPaginate recentSearchPaginate;
    private SearchJobAdapter adapter;
    private Button btnSearch;
    private SmUser user;
    /*Service*/
    private RecentSearchService recentSearchService = ServiceGenerator.createService(RecentSearchService.class);
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<FeedResponse> mNewSearchCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewSearch(response.body());
                dismissProgress();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    dismissProgress();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            dismissProgress();
            t.printStackTrace();
        }
    };

    private Callback<FeedResponse> mNextSearchCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextSearch(response.body());
                dismissProgress();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    dismissProgress();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            dismissProgress();
            t.printStackTrace();
        }
    };

    private Callback<RecentSearchPaginate> mNewRecentCallback = new Callback<RecentSearchPaginate>() {
        @Override
        public void onResponse(Call<RecentSearchPaginate> call, Response<RecentSearchPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewRecent(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearchPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<RecentSearchPaginate> mNextRecentCallback = new Callback<RecentSearchPaginate>() {
        @Override
        public void onResponse(Call<RecentSearchPaginate> call, Response<RecentSearchPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextRecent(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearchPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void newSearchJob(){
        int limit = 10;
        int user = this.user!=null ? this.user.getId() : 0;
        service.Search(user,1,this.adapter.getJobsFace().getSearchText(),0,this.adapter.getJobsFace().getLocation(),limit).enqueue(mNewSearchCallback);
    }

    private void nextSearchJob(){
        int limit = 10;
        int user = this.user!=null ? this.user.getId() : 0;
        int page = this.feedResponse.getCurrent_page() + 1;
        service.Search(user,1,this.adapter.getJobsFace().getSearchText(),0,this.adapter.getJobsFace().getLocation(),limit,page).enqueue(mNextSearchCallback);
    }

    private void initNewSearch(FeedResponse feedResponse){
        feedResponse.setTo(feedResponse.getTo() + SearchJobAdapter.MORE_VIEW);
        this.feedResponse = feedResponse;
        this.checkSearch();
        this.adapter = new SearchJobAdapter(getActivity(),feedResponse,this.user,SearchJobAdapter.RESULT);
        btnSearch = (Button)this.adapter.getJobsFace().getBtnSearch();
        btnSearch.setOnClickListener(this);
        recyclerView.setAdapter(this.adapter);
    }

    private void initNextSearch(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.checkSearch();
        this.adapter.update(getActivity(),feedResponse,this.user,SearchJobAdapter.RESULT).notifyDataSetChanged();
        btnSearch = (Button)this.adapter.getJobsFace().getBtnSearch();
        btnSearch.setOnClickListener(this);
    }

    private void newRecent(){
        int user = this.user!=null ? this.user.getId() : 0;
        int limit = 10;
        recentSearchService.hasRecentSearch_(user, RecentSearch.JOBS,limit).enqueue(mNewRecentCallback);
    }

    private void nextRecent(){
        int user = this.user!=null ? this.user.getId() : 0;
        int limit = 10;
        int page = this.recentSearchPaginate.getCurrent_page() + 1;
        recentSearchService.hasRecentSearch_(user,RecentSearch.JOBS,limit,page).enqueue(mNextRecentCallback);
    }

    private void initNewRecent(RecentSearchPaginate recentSearchPaginate){
        if (recentSearchPaginate.getTotal() > 0){
            recentSearchPaginate.setTo(recentSearchPaginate.getTo() + SearchJobAdapter.MORE_VIEW);
        }else {
            recentSearchPaginate.setTo(recentSearchPaginate.getTo() + 1);
        }
        this.recentSearchPaginate = recentSearchPaginate;
        this.checkRecent();
        this.adapter = new SearchJobAdapter(getActivity(),recentSearchPaginate,this.user,SearchJobAdapter.RECENT);
        btnSearch = (Button)this.adapter.getJobsFace().getBtnSearch();
        btnSearch.setOnClickListener(this);
        recyclerView.setAdapter(this.adapter);
    }

    private void initNextRecent(RecentSearchPaginate recentSearchPaginate){
        this.recentSearchPaginate = recentSearchPaginate;
        this.checkRecent();
        this.adapter.update(getActivity(),recentSearchPaginate,this.user,SearchJobAdapter.RECENT).notifyDataSetChanged();
        btnSearch = (Button)this.adapter.getJobsFace().getBtnSearch();
        btnSearch.setOnClickListener(this);
    }

    static int lastPage;
    static boolean isRequest = false;

    private void checkSearch(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    nextSearchJob();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void checkRecent(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (recentSearchPaginate.getTo() - 1)) && (recentSearchPaginate.getTo() < recentSearchPaginate.getTotal())) {
                        if (recentSearchPaginate.getNext_page_url()!=null || !recentSearchPaginate.getNext_page_url().equals("")){
                            if (lastPage!=recentSearchPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = recentSearchPaginate.getCurrent_page();
                                    nextRecent();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onResume() {
        this.restoreTemp();
        super.onResume();
    }

    private void restoreTemp(){
        if (this.adapter!=null){
            btnSearch = (Button)this.adapter.getJobsFace().getBtnSearch();
            btnSearch.setOnClickListener(this);
        }
    }

    public SearchJobsFragment() {
        // Required empty public constructor
    }

    public static SearchJobsFragment newInstance(){
        return new SearchJobsFragment();
    }

    private void initTemp(){
        this.user = new Auth(getContext()).checkAuth().token();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setActivity(getActivity());
        View view = inflater.inflate(R.layout.fragment_search_jobs, container, false);
        initTemp();
        initView(view);
        newRecent();
        return view;
    }

    private void initView(View view){
        initRecycler(view);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void initRecycler(View view){
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public void onClick(View view) {
        if (view==btnSearch){
            if (!Objects.requireNonNull(getActivity()).isFinishing()){
                showProgress();
                newSearchJob();
            }
        }
    }
}

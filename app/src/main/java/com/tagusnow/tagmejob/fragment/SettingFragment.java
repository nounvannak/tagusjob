package com.tagusnow.tagmejob.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.JobListActivity;
import com.tagusnow.tagmejob.PolicyActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.TermsActivity;
import com.tagusnow.tagmejob.adapter.InterestAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserInterest;
import com.tagusnow.tagmejob.v3.login.auth.AuthCallback;
import com.tagusnow.tagmejob.v3.login.auth.AuthReponse;
import com.tagusnow.tagmejob.v3.login.auth.BaseLogin;
import com.tagusnow.tagmejob.v3.setting.ContactUsActivity;
import com.tagusnow.tagmejob.view.Setting.activity.AccountMoreSetting;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingFragment extends TagUsJobFragment implements View.OnClickListener, View.OnLongClickListener, AuthCallback<SmUser, AuthReponse> {

    private static final String PROFILE_LINK = "tagusjob_profile_link";
    private CircleImageView profile;
    private ImageView cover;
    private TextView name,phone,email,user_type,slug,more_account,more_interest,version;
    private RelativeLayout btnLogout,layout_title_interest,layout_body_interest,layout_looking_for_job,layout_policy,layout_terms,layout_slug,layout_contact_us;
    private RecyclerView list_interest;
    private Switch sw_looking_for_job,sw_notification_alert,sw_message_sound;
    private SmUser user;
    private List<UserInterest> interests;
    private JSONObject settings;
    private BaseLogin baseLogin;
    private UserService service = ServiceGenerator.createService(UserService.class);

    private void UserInterest(){
        service.UserInterest(this.user.getId()).enqueue(new Callback<List<UserInterest>>() {
            @Override
            public void onResponse(Call<List<UserInterest>> call, Response<List<UserInterest>> response) {
                if (response.isSuccessful()){
                    UserInterest(response.body());
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<UserInterest>> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void UserInterest(List<UserInterest> interests){
        this.interests = interests;
        if (interests!=null){
            layout_title_interest.setVisibility(View.VISIBLE);
            layout_body_interest.setVisibility(View.VISIBLE);
            list_interest.setAdapter(new InterestAdapter(getActivity(),interests,this.user));
        }else {
            layout_title_interest.setVisibility(View.GONE);
            layout_body_interest.setVisibility(View.GONE);
        }
    }

    private void InitTemp(){
        this.user = new Auth(getContext()).checkAuth().token();
        this.settings = SuperUtil.GetPreference(this.activity,SuperConstance.SETTING_PREF);
    }

    private void Setup(){
        if (this.user!=null){

            ((TagUsJobActivity) activity).setUserProfile(profile,user);
            ((TagUsJobActivity) activity).setUserCover(cover,user);

            if (this.user.getSlug()!=null){
                slug.setText(this.user.getSlug());
            }else {
                slug.setText("Unknow");
            }

            ((TagUsJobActivity) activity).setName(name,user);
            phone.setText(this.user.getPhone() != null ? this.user.getPhone() : "N/A");
            email.setText(this.user.getEmail() != null ? this.user.getEmail() : "N/A");

            if (this.user.getUser_type()==SmUser.FIND_JOB){
                user_type.setText("Find Job");
            }else {
                user_type.setText("Find Staff");
            }

            if (this.user.getUser_type()==SmUser.FIND_JOB){
                layout_looking_for_job.setVisibility(View.VISIBLE);
            }else {
                layout_looking_for_job.setVisibility(View.GONE);
            }

            if (this.user.getIs_looking_job()==1){
                sw_looking_for_job.setChecked(true);
            }else {
                sw_looking_for_job.setChecked(false);
            }

            if (this.settings!=null){
                try {
                    sw_message_sound.setChecked(this.settings.getBoolean("isMessageSound"));
                    sw_notification_alert.setChecked(this.settings.getBoolean("isNotificationAlert"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            UserInterest();
        }
    }

    private void InitUI(View view){
        profile = (CircleImageView)view.findViewById(R.id.profile);
        cover = (ImageView)view.findViewById(R.id.cover);
        name = (TextView)view.findViewById(R.id.name);
        phone = (TextView)view.findViewById(R.id.phone);
        email = (TextView)view.findViewById(R.id.email);
        user_type = (TextView)view.findViewById(R.id.user_type);
        slug = (TextView)view.findViewById(R.id.slug);
        version = (TextView)view.findViewById(R.id.version);

        more_account = (TextView)view.findViewById(R.id.more_account);
        more_interest = (TextView)view.findViewById(R.id.more_interest);

        layout_title_interest = (RelativeLayout)view.findViewById(R.id.layout_title_interest);
        layout_body_interest = (RelativeLayout)view.findViewById(R.id.layout_body_interest);
        layout_looking_for_job  = (RelativeLayout)view.findViewById(R.id.layout_looking_for_job);
        layout_policy = (RelativeLayout)view.findViewById(R.id.layout_policy);
        layout_terms  = (RelativeLayout)view.findViewById(R.id.layout_terms);
        layout_contact_us  = (RelativeLayout)view.findViewById(R.id.layout_contact_us);
        layout_slug = (RelativeLayout)view.findViewById(R.id.layout_slug);
        btnLogout = (RelativeLayout)view.findViewById(R.id.btnLogout);

        list_interest = (RecyclerView)view.findViewById(R.id.list_interest);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        list_interest.setHasFixedSize(true);
        list_interest.setLayoutManager(manager);

        sw_looking_for_job  = (Switch)view.findViewById(R.id.sw_looking_for_job);
        sw_notification_alert  = (Switch)view.findViewById(R.id.sw_notification_alert);
        sw_message_sound  = (Switch)view.findViewById(R.id.sw_message_sound);

        layout_slug.setOnLongClickListener(this);
        btnLogout.setOnClickListener(this);

        layout_policy.setOnClickListener(this);
        layout_terms.setOnClickListener(this);
        layout_contact_us.setOnClickListener(this);

        more_interest.setOnClickListener(this);
        more_account.setOnClickListener(this);

        PackageInfo pinfo = null;
        try {
            pinfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            int versionNumber = pinfo.versionCode;
            String versionName = pinfo.versionName;
            version.setText(versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public SettingFragment() {
        // Required empty public constructor
    }

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivity(getActivity());
        InitTemp();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        baseLogin = new BaseLogin(activity);
        baseLogin.addCallback(this);
        InitUI(view);
        Setup();
        return view;
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view==layout_terms){
            this.activity.startActivity(TermsActivity.createIntent(this.activity));
        }else if (view==layout_policy){
            this.activity.startActivity(PolicyActivity.createIntent(this.activity));
        }else if (view==layout_contact_us){
            this.activity.startActivity(ContactUsActivity.createIntent(this.activity));
        }else if (view==more_account){
            this.activity.startActivity(AccountMoreSetting.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (view==more_interest){
            this.activity.startActivity(JobListActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (view==btnLogout){
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle("Logout").setMessage("Are you sure to logout?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    showProgress();
                    String access_token = SuperUtil.GetPreference(activity,SuperUtil.ACCESS_TOKEN,null);
                    String token_type = SuperUtil.GetPreference(activity,SuperUtil.TOKEN_TYPE,null);
                    String token = token_type + " " + access_token;
                    baseLogin.LogOut(token);
                }
            }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create().show();
        }
    }

    private void copyLink(){
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(PROFILE_LINK,this.user.getProfile_link());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getContext(),"Copied",Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onLongClick(View view) {
        copyLink();
        return false;
    }

    private void SaveSetting(){
        boolean isLookingForJob = sw_looking_for_job.isChecked();
        boolean isNotificationAlert = sw_notification_alert.isChecked();
        boolean isMessageSound = sw_message_sound.isChecked();
        JSONObject obj = new JSONObject();
        try {
            obj.put("isNotificationAlert",isNotificationAlert);
            obj.put("isMessageSound",isMessageSound);
            if (isLookingForJob){
                this.user.setIs_looking_job(1);
            }else {
                this.user.setIs_looking_job(0);
            }
            new Auth(getContext()).save(this.user);
            SuperUtil.SavePreference(this.activity,SuperConstance.SETTING_PREF,new Gson().toJson(obj));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SaveSetting();
        Log.e("Setting","destroy");
    }

    @Override
    public void onSuccess(SmUser user) {

    }

    @Override
    public void onSent(AuthReponse response) {

    }

    @Override
    public void onLogOut(Response response) {
        dismissProgress();
        new Auth(activity).save(null);
        ((TagUsJobActivity) activity).AccessApp(null);
        onDestroy();
    }

    @Override
    public void onWarning(Response response) {
        Toast.makeText(activity,response.message(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    public void onAccessToken(String access_token, String token_type) {
        SuperUtil.SavePreference(activity,SuperUtil.ACCESS_TOKEN,access_token);
        SuperUtil.SavePreference(activity,SuperUtil.TOKEN_TYPE,token_type);
    }
}

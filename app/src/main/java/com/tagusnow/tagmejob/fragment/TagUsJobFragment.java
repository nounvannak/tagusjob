package com.tagusnow.tagmejob.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.webkit.MimeTypeMap;

import com.tagusnow.tagmejob.ProgressDialogFragment;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.File;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TagUsJobFragment extends Fragment {
    private final static String PROGRESS_DIALOG = "ProgressDialog";
    protected final String OPEN_MEDIA_TITLE = "title";
    protected final String OPEN_MEDIA_MODE = "mode";
    protected final String OPEN_MEDIA_MAX = "maxSelection";
    protected static final int OPEN_MEDIA_PICKER = 1;
    protected final static int IMAGE_ONLY = 2;
    protected final static int REQUEST_EDIT = 8203;
    protected int lastPage = 0;
    protected boolean isRequest = false;
    protected Activity activity;

    public void PlayMessageSound(){
        ((TagUsJobActivity) activity).PlayMessageSound();
    }

    public boolean IsMessageSound(){
        return ((TagUsJobActivity) activity).IsMessageSound();
    }

    public boolean IsNotificationAlert(){
        return ((TagUsJobActivity) activity).IsNotificationAlert();
    }

    public void showProgress() {
        com.tagusnow.tagmejob.ProgressDialogFragment f = com.tagusnow.tagmejob.ProgressDialogFragment.getInstance();
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().add(f,PROGRESS_DIALOG).commitAllowingStateLoss();
    }

    public void dismissProgress() {
        FragmentManager manager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        if (manager == null) return;
        com.tagusnow.tagmejob.ProgressDialogFragment f = (ProgressDialogFragment) manager.findFragmentByTag(PROGRESS_DIALOG);
        if (f != null) {
            ((TagUsJobActivity)activity).getSupportFragmentManager().beginTransaction().remove(f).commitAllowingStateLoss();
        }
    }

    public String getMimeTypeFromMediaContentUri(Uri uri) {
        String mimeType;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = ((TagUsJobActivity)activity).getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    public void ShowAlert(String title,String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(((TagUsJobActivity)activity)).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    protected void setActivity(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    protected RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    protected MultipartBody.Part prepareFilePart(String partName, File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse(getMimeTypeFromMediaContentUri(Uri.fromFile(file))),file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }
}

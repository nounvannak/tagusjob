package com.tagusnow.tagmejob.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.AllPostActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.ImageSelectFaceAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.model.v2.feed.PostJobRequest;
import com.tagusnow.tagmejob.model.v2.feed.Setting;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.ImageSelectFace;
import com.tagusnow.tagmejob.view.Post.LayoutCamera;
import com.tagusnow.tagmejob.view.Post.LayoutCameraHandler;
import com.tagusnow.tagmejob.view.Post.LayoutSelectImageHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WidgetQuickPostJobs extends TagUsJobFragment implements View.OnClickListener, LayoutCameraHandler, LayoutSelectImageHandler {

    private static final String TAG = WidgetQuickPostJobs.class.getSimpleName();
    public static final int OPEN_CAMERA = 6379;
    private Spinner spnCategory,spnCity,spnLevel,spnWorkingTime;
    private EditText title,phone,email,salary,close_date,website,numberOfEmployee,description,txtRequirement;
    private View error_position,error_title,error_salary,error_close_date,error_phone,error_email,error_numberOfEmployee,error_city,error_level,error_workingTime;
    private SmUser user;
    private SmTagSubCategory category;
    private SmLocation location;
    private RecyclerView recyclerImg;
    private ImageSelectFaceAdapter adapter;
    private List<String> categoryList = new ArrayList<String>();
    private List<String> cityList = new ArrayList<String>();
    private List<String> levelList = new ArrayList<String>();
    private List<String> workTimeList = new ArrayList<String>();
    private List<String> images;
    private int category_id;
    private int city;
    private String level,workingTime;
    private String close_dateStr;
    private Calendar closeDateCal = Calendar.getInstance();
    private Feed feed;
    private PostJobRequest request;
    private static boolean isPost;
    /*Service*/
    private JobsService service = ServiceGenerator.createService(JobsService.class);

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            closeDateCal.set(Calendar.YEAR,i);
            closeDateCal.set(Calendar.MONTH,i1);
            closeDateCal.set(Calendar.DAY_OF_MONTH,i2);
            setupDate();
        }
    };

    private Callback<Feed> SAVE = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                setFeed(response.body());
                isPost = response.isSuccessful();
                ((AllPostActivity) activity).onBackPressed();
                dismissProgress();
            }else {
                dismissProgress();
                try {
                    ShowAlert(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            ShowAlert("Connection Error",t.getMessage());
        }
    };

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public WidgetQuickPostJobs() {
        // Required empty public constructor
    }

    public static WidgetQuickPostJobs newInstance(){
        return new WidgetQuickPostJobs();
    }

    public WidgetQuickPostJobs Activity(Activity activity){
        this.activity = (AllPostActivity) activity;
        return this;
    }

    private void initErrorView(View view){
        error_position = (View)view.findViewById(R.id.error_position);
        error_title = (View)view.findViewById(R.id.error_title);
        error_salary = (View)view.findViewById(R.id.error_salary);
        error_close_date = (View)view.findViewById(R.id.error_close_date);
        error_phone = (View)view.findViewById(R.id.error_phone);
        error_email = (View)view.findViewById(R.id.error_email);
        error_numberOfEmployee = (View)view.findViewById(R.id.error_numberOfEmployee);
        error_city = (View)view.findViewById(R.id.error_city);
        error_level = (View)view.findViewById(R.id.error_level);
        error_workingTime = (View)view.findViewById(R.id.error_workingTime);
    }

    private void initRecycler(View view){
        recyclerImg = (RecyclerView)view.findViewById(R.id.recyclerImg);
        recyclerImg.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        recyclerImg.setLayoutManager(layoutManager);
    }

    private void initView(View view){
        initRecycler(view);
        initErrorView(view);
        spnCategory = (Spinner)view.findViewById(R.id.spnCategory);
        spnCity = (Spinner)view.findViewById(R.id.spnCity);
        spnLevel = (Spinner)view.findViewById(R.id.spnLevel);
        spnWorkingTime = (Spinner)view.findViewById(R.id.spnWorkingTime);
        title = (EditText)view.findViewById(R.id.title);
        phone = (EditText)view.findViewById(R.id.phone);
        email = (EditText)view.findViewById(R.id.email);
        salary = (EditText)view.findViewById(R.id.salary);
        close_date = (EditText)view.findViewById(R.id.close_date);
        website = (EditText)view.findViewById(R.id.website);
        numberOfEmployee = (EditText)view.findViewById(R.id.numberOfEmployee);
        description = (EditText)view.findViewById(R.id.description);
        txtRequirement = (EditText)view.findViewById(R.id.txtRequirement);
        close_date.setOnClickListener(this);
        spnLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                level = levelList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                level = null;
            }
        });
        spnWorkingTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                workingTime = workTimeList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                workingTime = null;
            }
        });
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0){
                    category_id = category.getData().get(i - 1).getId();
                }else {
                    category_id = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                category_id = 0;
            }
        });
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0){
                    city = location.getData().get(i - 1).getId();
                }else {
                    city = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                city = 0;
            }
        });

        this.initImageSelect(new ArrayList<String>());
    }

    private void initTemp(){
        this.user = new Auth(getContext()).checkAuth().token();
        this.category = new Repository(getContext()).restore().getCategory();
        this.location = new Repository(getContext()).restore().getLocation();
        this.categoryList.add(0,"Choose Position");
        if (this.category.getData()!=null){
            for (int i=1;i <= this.category.getData().size();i++){
                this.categoryList.add(this.category.getData().get(i-1).getTitle());
            }
        }

        this.cityList.add(0,"Choose City/Province");
        if (this.location.getData()!=null){
            for (int i=1;i <= this.location.getData().size();i++){
                this.cityList.add(this.location.getData().get(i-1).getName());
            }
        }

        Setting levelObj,workTimeObj;
        levelObj = new Repository(getContext()).restore().getLevel();
        workTimeObj = new Repository(getContext()).restore().getWorkingTime();
        if (levelObj != null){
            Log.e(TAG, "initTemp: " + levelObj.getValue());
            String[] levelArr = levelObj.getValue().split(",");
            levelList.addAll(Arrays.asList(levelArr));
            Log.e(TAG, levelList.toString());
        }

        if (workTimeObj != null){
            String[] levelArr = workTimeObj.getValue().split(",");
            workTimeList.addAll(Arrays.asList(levelArr));
        }
    }

    private void initUserInfo(){
        if (this.user!=null){

            if (this.user.getEmail()!=null){
                email.setText(this.user.getEmail());
            }

            if (this.user.getPhone()!=null){
                phone.setText(this.user.getPhone());
            }
        }
    }

    private void initDatePicker(){
        if (this.closeDateCal==null){
            this.closeDateCal = Calendar.getInstance();
        }
        new DatePickerDialog(Objects.requireNonNull(getActivity()), dateSetListener, this.closeDateCal.get(Calendar.YEAR), this.closeDateCal.get(Calendar.MONTH), this.closeDateCal.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setupView(){
        if (this.categoryList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),android.R.layout.simple_spinner_dropdown_item,this.categoryList);
            spnCategory.setAdapter(adapter);
        }

        if (this.cityList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),android.R.layout.simple_spinner_dropdown_item,this.cityList);
            spnCity.setAdapter(adapter);
        }

        if (this.levelList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),android.R.layout.simple_spinner_dropdown_item,this.levelList);
            spnLevel.setAdapter(adapter);
        }else {
            Log.e(TAG,"Level list is null");
        }

        if (this.workTimeList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),android.R.layout.simple_spinner_dropdown_item,this.workTimeList);
            spnWorkingTime.setAdapter(adapter);
        }else {
            Log.e(TAG, "Work time list is null");
        }

        setupDate();
    }

    private void setupDate(){
        if (this.closeDateCal!=null){
            String date = this.closeDateCal.get(Calendar.DAY_OF_MONTH)+"/"+((this.closeDateCal.get(Calendar.MONTH) + 1) > 9 ? (this.closeDateCal.get(Calendar.MONTH) + 1) : "0"+(this.closeDateCal.get(Calendar.MONTH) + 1))+"/"+this.closeDateCal.get(Calendar.YEAR);
            this.close_dateStr = this.closeDateCal.get(Calendar.YEAR)+"-"+((this.closeDateCal.get(Calendar.MONTH) + 1) > 9 ? (this.closeDateCal.get(Calendar.MONTH) + 1) : "0"+(this.closeDateCal.get(Calendar.MONTH) + 1))+"-"+this.closeDateCal.get(Calendar.DAY_OF_MONTH);
            close_date.setText(date);
        }
    }

    public boolean isValid(){
        boolean valid = true;
        this.request = new PostJobRequest();
        this.request.setJob_type(category_id);
        this.request.setJob_city(city);
        this.request.setAuth_user(this.user.getId());

        if (this.category_id > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_position.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_position.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (this.city > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_city.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_city.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (this.level != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_level.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_level.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (this.workingTime != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_workingTime.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
        }else {
            error_workingTime.setBackgroundColor(Color.RED);
            valid = false;
        }

        /*Title*/
        if (this.title.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_title.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
            this.request.setJob_title(title.getText().toString());
        }else {
            error_title.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*Salary*/
        if (this.salary.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_salary.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
            this.request.setJob_salary(salary.getText().toString());
        }else {
            error_salary.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*close_date*/
        if (this.close_date.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_close_date.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
            this.request.setJob_close_date(close_dateStr);
        }else {
            error_close_date.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*phone*/
        if (this.phone.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_phone.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
            this.request.setJob_phone(phone.getText().toString());
        }else {
            error_phone.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*email*/
        if (this.email.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_email.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
            this.request.setJob_email(email.getText().toString());
        }else {
            error_email.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*numberOfEmployee*/
        if (this.numberOfEmployee.getText().length() > 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                error_numberOfEmployee.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.dotted));
            }
            this.request.setNum_employee(this.numberOfEmployee.getText().toString());
        }else {
            error_numberOfEmployee.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (description.getText().length() > 0){
            this.request.setJob_desc(description.getText().toString());
            description.setHintTextColor(getResources().getColor(R.color.light_gray));
        }else {
            description.setHintTextColor(getResources().getColor(R.color.pdlg_color_red));
            valid = false;
        }

        if (txtRequirement.getText().length() > 0){
            this.request.setRequirement(txtRequirement.getText().toString());
            txtRequirement.setHintTextColor(getResources().getColor(R.color.light_gray));

        }else {
            txtRequirement.setHintTextColor(getResources().getColor(R.color.pdlg_color_red));
            valid = false;
        }

        return valid;
    }

    private void openGallery(){
        AndroidImagePicker.getInstance().pickSingle(this.activity, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                if (items!=null){
                    List<String> list = new ArrayList<>();
                    for (ImageItem item : items){
                        list.add(item.path);
                    }
                    initImageSelect(list);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==OPEN_CAMERA && resultCode==Activity.RESULT_OK && data!=null){
            List<String> uriList = data.getStringArrayListExtra("result");
            if (uriList!=null){
                initImageSelect(uriList);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_widget_quick_post_jobs, container, false);
        initTemp();
        initView(view);
        setupView();
        initUserInfo();
        return view;
    }

    private List<MultipartBody.Part> parts = new ArrayList<>();

    public void initImageSelect(List<String> fileLists){
        this.images = fileLists;
        this.initParts(fileLists);
        this.adapter = new ImageSelectFaceAdapter(activity);
        this.adapter.setHandler(this);
        this.adapter.setLayoutSelectImageHandler(this);
        this.adapter.setFileLists(fileLists);
        recyclerImg.setAdapter(this.adapter);
    }

    private void initParts(List<String> list){
        if (list!=null){
            if (list.size() > 0){
                for (int i=0;i < list.size();i++){
                    if (list.get(i)!=null){
                        File photoUri = new File(list.get(i));
                        this.parts.add(prepareFilePart("job_upload_file[]",photoUri));
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view==close_date){
            initDatePicker();
        }
    }

    public boolean isPost() {
        return this.feed!=null;
    }

    public WidgetQuickPostJobs save() throws IOException {
        RequestBody job_title,job_close_date,job_phone,job_city,job_email,job_salary,job_desc,job_type,num_employee,job_website,auth_user,requirement,level,workTime;
        auth_user = createPartFromString(String.valueOf(this.user.getId()));
        job_city = createPartFromString(String.valueOf(city));
        job_type = createPartFromString(String.valueOf(category_id));
        job_title = createPartFromString(this.title.getText().toString());
        job_salary = createPartFromString(this.salary.getText().toString());
        job_close_date = createPartFromString(this.close_dateStr);
        job_phone = createPartFromString(this.phone.getText().toString());
        job_email = createPartFromString(this.email.getText().toString());
        num_employee = createPartFromString(this.numberOfEmployee.getText().toString());
        job_desc = createPartFromString(this.description.getText().toString());
        job_website = createPartFromString(this.website.getText().toString());
        level = createPartFromString(this.level);
        requirement = createPartFromString(this.txtRequirement.getText().toString());
        workTime = createPartFromString(this.workingTime);
        service.saveJobs(job_title,auth_user,job_close_date,job_phone,job_city,job_email,job_salary,job_desc,job_type,num_employee,job_website,level,workTime,requirement,this.parts).enqueue(SAVE);
        return this;
    }

    @Override
    public void openCamera(LayoutCamera camera, int requestCode) {
        openGallery();
    }

    @Override
    public void remove(ImageSelectFace imageSelectFace, RecyclerView.Adapter adapter, int position) {
        this.images.remove(position);
        this.initParts(this.images);
        ((ImageSelectFaceAdapter) adapter).setFileLists(this.images);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void selected(List<ImageItem> items) {

    }
}

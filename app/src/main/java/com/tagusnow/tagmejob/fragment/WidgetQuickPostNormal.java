package com.tagusnow.tagmejob.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.tagusnow.tagmejob.R;

public class WidgetQuickPostNormal extends Fragment {

    private static final String TAG = WidgetQuickPostNormal.class.getSimpleName();
    private EditText normal_post_text;
    private ImageButton btnCamera;
    private RecyclerView recyclerImage;

    public WidgetQuickPostNormal() {
        // Required empty public constructor
    }

    public static WidgetQuickPostNormal newInstance(){
        return new WidgetQuickPostNormal();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_widget_quick_post_normal, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        normal_post_text = (EditText)view.findViewById(R.id.normal_post_text);
        btnCamera = (ImageButton)view.findViewById(R.id.btnCamera);
        initRecycler(view);
    }

    private void initRecycler(View view){
        recyclerImage = (RecyclerView)view.findViewById(R.id.recyclerImage);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerImage.setHasFixedSize(true);
        recyclerImage.setLayoutManager(layoutManager);
    }

    public boolean isValid() {
        return false;
    }

    public boolean isPost() {
        return false;
    }

    public WidgetQuickPostNormal save() {
        return this;
    }
}

package com.tagusnow.tagmejob.model.v2.feed;

import java.io.Serializable;

public class ApplyJob implements Serializable {
    private int id;
    private int apply_to;
    private int auth_user;
    private String auth_name;
    private String apply_for;
    private String auth_email;
    private String apply_to_email;
    private String subject;
    private String cv_file;
    private String cover_letter_file;
    private int feed_id;
    private int apply_type;

    public ApplyJob(){
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApply_to() {
        return apply_to;
    }

    public void setApply_to(int apply_to) {
        this.apply_to = apply_to;
    }

    public int getAuth_user() {
        return auth_user;
    }

    public void setAuth_user(int auth_user) {
        this.auth_user = auth_user;
    }

    public String getAuth_name() {
        return auth_name;
    }

    public void setAuth_name(String auth_name) {
        this.auth_name = auth_name;
    }

    public String getApply_for() {
        return apply_for;
    }

    public void setApply_for(String apply_for) {
        this.apply_for = apply_for;
    }

    public String getAuth_email() {
        return auth_email;
    }

    public void setAuth_email(String auth_email) {
        this.auth_email = auth_email;
    }

    public String getApply_to_email() {
        return apply_to_email;
    }

    public void setApply_to_email(String apply_to_email) {
        this.apply_to_email = apply_to_email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCv_file() {
        return cv_file;
    }

    public void setCv_file(String cv_file) {
        this.cv_file = cv_file;
    }

    public String getCover_letter_file() {
        return cover_letter_file;
    }

    public void setCover_letter_file(String cover_letter_file) {
        this.cover_letter_file = cover_letter_file;
    }

    public int getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(int feed_id) {
        this.feed_id = feed_id;
    }

    public int getApply_type() {
        return apply_type;
    }

    public void setApply_type(int apply_type) {
        this.apply_type = apply_type;
    }
}

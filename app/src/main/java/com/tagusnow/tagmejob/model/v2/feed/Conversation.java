package com.tagusnow.tagmejob.model.v2.feed;

import android.text.format.DateUtils;

import com.tagusnow.tagmejob.auth.SmUser;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Conversation implements Serializable{

    public static final int TEXT = 4;
    public static final int IMAGE = 0;
    public static final int AUDIO = 1;
    public static final int FILE = 2;
    public static final int VIDEO = 3;

    public Conversation(int id, int conversation_id, int user_one, int user_two, int message_id, int file_type, int counter, String story, String message, String image, int seen, String time, String seen_time, String created_at, String updated_at, SmUser userOne, SmUser userTwo, int is_message, List<MessageFile> message_file) {
        this.id = id;
        this.conversation_id = conversation_id;
        this.user_one = user_one;
        this.user_two = user_two;
        this.message_id = message_id;
        this.file_type = file_type;
        this.counter = counter;
        this.story = story;
        this.message = message;
        this.image = image;
        this.seen = seen;
        this.time = time;
        this.seen_time = seen_time;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.userOne = userOne;
        this.userTwo = userTwo;
        this.is_message = is_message;
        this.message_file = message_file;
    }

    private int id;
    private int conversation_id;
    private int user_id;
    private int last_message_id;
    private String users;
    private String msg_file_id;
    private int user_seen;
    private int user_one;
    private int user_two;
    private int message_id;
    private int file_type;
    private int counter;
    private String story;
    private String message;
    private String image;
    private int seen;
    private String time;
    private String seen_time;
    private String created_at;
    private String updated_at;
    private SmUser userOne;
    private SmUser userTwo;
    private int is_message;
    private List<MessageFile> message_file;

    public Conversation() {
        super();
    }

    public Conversation(int id, int conversation_id, int user_one, int user_two, int message_id, int file_type, String message, String image, int seen, String time, String seen_time, String created_at, String updated_at) {
        this.id = id;
        this.conversation_id = conversation_id;
        this.user_one = user_one;
        this.user_two = user_two;
        this.message_id = message_id;
        this.file_type = file_type;
        this.message = message;
        this.image = image;
        this.seen = seen;
        this.time = time;
        this.seen_time = seen_time;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getTimeStory(){
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date d = f.parse(this.time);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            milliseconds = new Date().getTime();
        }
        return DateUtils.getRelativeTimeSpanString(milliseconds,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public Date getDateCreated(){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date d = new Date();
        try {
            d = f.parse(this.time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public String getSeenStoryTime(){
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date d = f.parse(this.seen_time);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            milliseconds = new Date().getTime();
        }
        return DateUtils.getRelativeTimeSpanString(milliseconds,new Date().getTime(),DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConversation_id() {
        return conversation_id;
    }

    public void setConversation_id(int conversation_id) {
        this.conversation_id = conversation_id;
    }

    public int getUser_one() {
        return user_one;
    }

    public void setUser_one(int user_one) {
        this.user_one = user_one;
    }

    public int getUser_two() {
        return user_two;
    }

    public void setUser_two(int user_two) {
        this.user_two = user_two;
    }

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public int getFile_type() {
        return file_type;
    }

    public void setFile_type(int file_type) {
        this.file_type = file_type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isSeen() {
        return this.seen != 0;
    }

    public void setSeen(int seen) {
        this.seen = seen;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSeen_time() {
        return seen_time;
    }

    public void setSeen_time(String seen_time) {
        this.seen_time = seen_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public SmUser getUserOne() {
        return userOne;
    }

    public void setUserOne(SmUser userOne) {
        this.userOne = userOne;
    }

    public SmUser getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(SmUser userTwo) {
        this.userTwo = userTwo;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public List<MessageFile> getMessage_file() {
        return message_file;
    }

    public void setMessage_file(List<MessageFile> message_file) {
        this.message_file = message_file;
    }

    public boolean isIs_message() {
        return this.is_message!=0;
    }

    public void setIs_message(int is_message) {
        this.is_message = is_message;
    }

    public boolean getUser_seen() {
        return this.user_seen!=0;
    }

    public void setUser_seen(int user_seen) {
        this.user_seen = user_seen;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

}

package com.tagusnow.tagmejob.model.v2.feed;

import java.io.Serializable;

public class Experience implements Serializable {
    private int id;
    private int user_id;
    private String name;
    private String company;
    private String location;
    private String headline;
    private String img;
    private String img_des;
    private String link;
    private String description;
    private String from;
    private String to;
    private String current_work_here;
    private int status;
    private int trash;

    public Experience(){
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_des() {
        return img_des;
    }

    public void setImg_des(String img_des) {
        this.img_des = img_des;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCurrent_work_here() {
        return current_work_here;
    }

    public void setCurrent_work_here(String current_work_here) {
        this.current_work_here = current_work_here;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }
}

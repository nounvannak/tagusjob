package com.tagusnow.tagmejob.model.v2.feed;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem;

public class ImageItem implements AsymmetricItem {

    private int columnSpan;
    private int rowSpan;
    private int position;
    private String path;

    public ImageItem() {
        this(1, 1, 0,"");
    }

    public ImageItem(int columnSpan, int rowSpan, int position,String path) {
        this.columnSpan = columnSpan;
        this.rowSpan = rowSpan;
        this.position = position;
        this.path = path;
    }

    public ImageItem(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        columnSpan = in.readInt();
        rowSpan = in.readInt();
        position = in.readInt();
        path = in.readString();
    }

    @Override
    public int getColumnSpan() {
        return this.columnSpan;
    }

    @Override
    public int getRowSpan() {
        return this.rowSpan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(columnSpan);
        parcel.writeInt(rowSpan);
        parcel.writeInt(position);
        parcel.writeString(path);
    }

    @Override public String toString() {
        return String.format("%s: %sx%s", position, rowSpan, columnSpan);
    }

    public int getPosition() {
        return position;
    }

    public static final Parcelable.Creator<ImageItem> CREATOR = new Parcelable.Creator<ImageItem>() {
        @Override public ImageItem createFromParcel(@NonNull Parcel in) {
            return new ImageItem(in);
        }

        @Override @NonNull public ImageItem[] newArray(int size) {
            return new ImageItem[size];
        }
    };

    public String getPath() {
        return path;
    }
}

package com.tagusnow.tagmejob.model.v2.feed;

import java.io.Serializable;

public class Language implements Serializable {

    public Language() {
        super();
    }

    public Language(int id, String name, int status, int trash) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.trash = trash;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }

    private int id;
    private String name;
    private int status;
    private int trash;
}

package com.tagusnow.tagmejob.model.v2.feed;

import java.io.Serializable;

public class MessageFile implements Serializable {
    private int id;
    private int message_id;
    private int file_type;
    private String original_name;
    private String new_name;
    private int trash;
    private String created_at;
    private String updated_at;

    public MessageFile() {
        super();
    }

    public MessageFile(int id, int message_id, int file_type, String original_name, String new_name, int trash, String created_at, String updated_at) {
        this.id = id;
        this.message_id = message_id;
        this.file_type = file_type;
        this.original_name = original_name;
        this.new_name = new_name;
        this.trash = trash;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public int getFile_type() {
        return file_type;
    }

    public void setFile_type(int file_type) {
        this.file_type = file_type;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public String getNew_name() {
        return new_name;
    }

    public void setNew_name(String new_name) {
        this.new_name = new_name;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

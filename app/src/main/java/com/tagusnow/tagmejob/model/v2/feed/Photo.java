package com.tagusnow.tagmejob.model.v2.feed;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem;
import java.io.Serializable;

public class Photo implements AsymmetricItem,Serializable{
    private int columnSpan;
    private int rowSpan;
    private int position;
    private int id;
    private int feed_id;
    private int user_id;
    private String image;
    private String created_at;
    private String updated_at;

    public Photo() {
        this(1, 1, 0,0,0,0,"");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(int feed_id) {
        this.feed_id = feed_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Photo(int columnSpan, int rowSpan, int position,int id,int feed_id,int user_id,String image) {
        this.columnSpan = columnSpan;
        this.rowSpan = rowSpan;
        this.position = position;
        this.id = id;
        this.feed_id = feed_id;
        this.user_id = user_id;
        this.image = image;
    }

    public Photo(Parcel in) {
        readFromParcel(in);
    }

    @Override public int getColumnSpan() {
        return columnSpan;
    }

    @Override public int getRowSpan() {
        return rowSpan;
    }

    public int getPosition() {
        return position;
    }

    @Override public String toString() {
        return String.format("%s: %sx%s", position, rowSpan, columnSpan);
    }

    @Override public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        columnSpan = in.readInt();
        rowSpan = in.readInt();
        position = in.readInt();
        id = in.readInt();
        feed_id = in.readInt();
        user_id = in.readInt();
        image = in.readString();
    }

    @Override public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(columnSpan);
        dest.writeInt(rowSpan);
        dest.writeInt(position);
        dest.writeInt(id);
        dest.writeInt(feed_id);
        dest.writeInt(user_id);
        dest.writeString(image);
    }

    /* Parcelable interface implementation */
    public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
        @Override public Photo createFromParcel(@NonNull Parcel in) {
            return new Photo(in);
        }

        @Override @NonNull public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}

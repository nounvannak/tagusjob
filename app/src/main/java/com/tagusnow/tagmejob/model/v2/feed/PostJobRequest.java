package com.tagusnow.tagmejob.model.v2.feed;

import java.io.Serializable;

public class PostJobRequest implements Serializable {
    private String job_title;
    private String job_close_date;
    private String job_phone;
    private String job_phone2;
    private int job_city;
    private String job_email;
    private String job_salary;
    private String job_location;
    private String job_desc;
    private String job_name;
    private int job_type;
    private String num_employee;
    private String work_time;

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    private String requirement;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    private String level;
    private String website;
    private int auth_user;

    public PostJobRequest() {
        super();
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getJob_close_date() {
        return job_close_date;
    }

    public void setJob_close_date(String job_close_date) {
        this.job_close_date = job_close_date;
    }

    public String getJob_phone() {
        return job_phone;
    }

    public void setJob_phone(String job_phone) {
        this.job_phone = job_phone;
    }

    public String getJob_phone2() {
        return job_phone2;
    }

    public void setJob_phone2(String job_phone2) {
        this.job_phone2 = job_phone2;
    }

    public int getJob_city() {
        return job_city;
    }

    public void setJob_city(int job_city) {
        this.job_city = job_city;
    }

    public String getJob_email() {
        return job_email;
    }

    public void setJob_email(String job_email) {
        this.job_email = job_email;
    }

    public String getJob_salary() {
        return job_salary;
    }

    public void setJob_salary(String job_salary) {
        this.job_salary = job_salary;
    }

    public String getJob_desc() {
        return job_desc;
    }

    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public int getJob_type() {
        return job_type;
    }

    public void setJob_type(int job_type) {
        this.job_type = job_type;
    }

    public String getWork_time() {
        return work_time;
    }

    public void setWork_time(String work_time) {
        this.work_time = work_time;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getAuth_user() {
        return auth_user;
    }

    public void setAuth_user(int auth_user) {
        this.auth_user = auth_user;
    }

    public String getNum_employee() {
        return num_employee;
    }

    public void setNum_employee(String num_employee) {
        this.num_employee = num_employee;
    }

    public String getJob_location() {
        return job_location;
    }

    public void setJob_location(String job_location) {
        this.job_location = job_location;
    }
}

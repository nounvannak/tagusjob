package com.tagusnow.tagmejob.model.v2.feed;

import java.io.Serializable;

public class Setting  implements Serializable {

    public Setting(int id, String key, String value, String type, String des) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.type = type;
        this.des = des;
    }

    public Setting() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    private int id;
    private String key;
    private String value;
    private String type;
    private String des;

}

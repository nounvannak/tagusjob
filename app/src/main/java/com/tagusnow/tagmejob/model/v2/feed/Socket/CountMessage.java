package com.tagusnow.tagmejob.model.v2.feed.Socket;

import com.tagusnow.tagmejob.feed.Notification;
import java.io.Serializable;

public class CountMessage implements Serializable {
    private int counter;
    private Notification notification;

    public CountMessage(int counter, Notification notification) {
        this.counter = counter;
        this.notification = notification;
    }

    public CountMessage(){
        super();
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }
}

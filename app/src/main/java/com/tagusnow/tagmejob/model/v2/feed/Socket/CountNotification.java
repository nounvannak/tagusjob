package com.tagusnow.tagmejob.model.v2.feed.Socket;

import com.tagusnow.tagmejob.feed.Notification;

public class CountNotification {
    private int counter;
    private Notification notification;

    public CountNotification(int counter, Notification notification) {
        this.counter = counter;
        this.notification = notification;
    }

    public CountNotification(){
        super();
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }
}

package com.tagusnow.tagmejob.model.v2.feed.Socket;

import java.io.Serializable;

public class MessageTyping implements Serializable {
    private int user_id;
    private String user_name;
    private int conversation_id;

    public MessageTyping() {
        super();
    }

    public MessageTyping(int user_id, String user_name, int conversation_id) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.conversation_id = conversation_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getConversation_id() {
        return conversation_id;
    }

    public void setConversation_id(int conversation_id) {
        this.conversation_id = conversation_id;
    }
}

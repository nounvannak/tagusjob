package com.tagusnow.tagmejob.model.v2.feed.Socket;

public class SocketTyping {
    private int feed_id;
    private int user_id;
    private String user_name;

    public SocketTyping(int feed_id, int user_id, String user_name) {
        this.feed_id = feed_id;
        this.user_id = user_id;
        this.user_name = user_name;
    }

    public SocketTyping(){
        super();
    }

    public int getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(int feed_id) {
        this.feed_id = feed_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}

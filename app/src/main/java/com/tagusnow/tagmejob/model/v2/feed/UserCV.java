package com.tagusnow.tagmejob.model.v2.feed;

import java.io.Serializable;

public class UserCV implements Serializable {
    private int id;
    private int feed_id;
    private int find_job_id;
    private String file;
    private String photo;
    private int user_id;
    private String created_at;
    private String updated_at;

    public UserCV(){
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(int feed_id) {
        this.feed_id = feed_id;
    }

    public int getFind_job_id() {
        return find_job_id;
    }

    public void setFind_job_id(int find_job_id) {
        this.find_job_id = find_job_id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getType() {
        return photo;
    }

    public void setType(String photo) {
        this.photo = photo;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

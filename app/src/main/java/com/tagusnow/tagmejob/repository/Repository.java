package com.tagusnow.tagmejob.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.REST.Service.RepositoryService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.model.v2.feed.Setting;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    private static final String TAG = Repository.class.getSimpleName();
    private static final String CATEGORY = "tagusjob_category";
    private static final String LOCATION = "tagusjob_location";
    static SmLocation location;
    static SmTagSubCategory category;

    public Setting getLevel() {
        return level;
    }

    public Setting getWorkingTime() {
        return workingTime;
    }

    static Setting level,workingTime;
    static String category_json;
    static String location_json;
    static String level_json;
    static String workingTime_json;
    Context context;
    private RepositoryService repositoryService = ServiceGenerator.createService(RepositoryService.class);

    public Repository(Context context){
        this.context = context;
    }


    private void callCategory(){
        repositoryService.callCategory().enqueue(new Callback<SmTagSubCategory>() {
            @Override
            public void onResponse(Call<SmTagSubCategory> call, Response<SmTagSubCategory> response) {
                if (response.isSuccessful()){
                    if (response.body()!=null){
                        category_json = new Gson().toJson(response.body());
                        category = response.body();
                        save();
                    }
                }else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SmTagSubCategory> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void callLocation(){
        repositoryService.callLocation().enqueue(new Callback<SmLocation>() {
            @Override
            public void onResponse(Call<SmLocation> call, Response<SmLocation> response) {
                if (response.isSuccessful()){
                    if (response.body()!=null){
                        location_json = new Gson().toJson(response.body());
                        location = response.body();
                        save();
                    }
                }else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SmLocation> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void callLevel(){
        repositoryService.GetSetting("level_job").enqueue(new Callback<Setting>() {
            @Override
            public void onResponse(Call<Setting> call, Response<Setting> response) {
                if (response.isSuccessful()){
                    level_json = new Gson().toJson(response.body());
                    level = response.body();
                    save();
                }else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Setting> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void callWorkingTime(){
        repositoryService.GetSetting("work_time").enqueue(new Callback<Setting>() {
            @Override
            public void onResponse(Call<Setting> call, Response<Setting> response) {
                if (response.isSuccessful()){
                    workingTime_json = new Gson().toJson(response.body());
                    workingTime = response.body();
                    save();
                }else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Setting> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void save(){
        SharedPreferences mAuthPrefs = context.getSharedPreferences(CATEGORY,Context.MODE_PRIVATE);
        SharedPreferences.Editor mAuthEditor = mAuthPrefs.edit();
        mAuthEditor.putString(CATEGORY,category_json);
        mAuthEditor.putString(LOCATION,location_json);
        mAuthEditor.putString("level",level_json);
        mAuthEditor.putString("working_time",workingTime_json);
        mAuthEditor.apply();
        mAuthEditor.commit();
        Log.w(TAG,"save");
    }

    private void check(){
        SharedPreferences mUserPrefs = context.getSharedPreferences(CATEGORY,Context.MODE_PRIVATE);
        category_json = mUserPrefs.getString(CATEGORY,null);
        location_json = mUserPrefs.getString(LOCATION,null);
        level_json = mUserPrefs.getString("level",null);
        workingTime_json = mUserPrefs.getString("working_time",null);
    }

    public Repository response(){
        callCategory();
        callLocation();
        callLevel();
        callWorkingTime();
        return this;
    }

    public Repository restore(){
        check();
        if (category_json!=null){
            category = new Gson().fromJson(category_json,SmTagSubCategory.class);
        }else{
            callCategory();
        }

        if (location_json!=null){
            location = new Gson().fromJson(location_json,SmLocation.class);
        }else{
            callLocation();
        }

        if (level_json!=null){
            level = new Gson().fromJson(level_json,Setting.class);
        }else{
            callLevel();
        }

        if (workingTime_json!=null){
            workingTime = new Gson().fromJson(workingTime_json,Setting.class);
        }else{
            callWorkingTime();
        }

        save();
        return this;
    }

    public SmLocation getLocation() {
        return location;
    }

    public SmTagSubCategory getCategory() {
        return category;
    }

}

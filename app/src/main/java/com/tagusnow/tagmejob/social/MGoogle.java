package com.tagusnow.tagmejob.social;

import android.content.Intent;
import android.net.Uri;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;

public class MGoogle {
    protected static final String TAG = "LoginActivity";
    protected static final int RC_SIGN_IN = 9001;

    private String display_name;
    private String email;
    private String family_name;
    private String given_name;
    private String id;
    private String id_token;
    private Uri photo_url;
    private String server_auth_code;

    public MGoogle() {
        super();
    }

    public MGoogle(String display_name,String email,String family_name,String given_name,String id,String id_token,Uri photo_url,String server_auth_code) {
        this.display_name=display_name;
        this.email=email;
        this.family_name=family_name;
        this.given_name=given_name;
        this.id=id;
        this.id_token=id_token;
        this.photo_url=photo_url;
        this.server_auth_code=server_auth_code;
    }

    public Intent signIn(GoogleSignInClient googleSignInClient){
        Intent signInIntent = googleSignInClient.getSignInIntent();
        return signInIntent;
    }

    public String getDisplayName() {
        return display_name;
    }

    public void setDisplayName(String display_name) {
        this.display_name = display_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFamilyName() {
        return family_name;
    }

    public void setFamilyName(String family_name) {
        this.family_name = family_name;
    }

    public String getGivenName() {
        return given_name;
    }

    public void setGivenName(String given_name) {
        this.given_name = given_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdToken() {
        return id_token;
    }

    public void setIdToken(String id_token) {
        this.id_token = id_token;
    }

    public Uri getPhotoUrl() {
        return photo_url;
    }

    public void setPhotoUrl(Uri photo_url) {
        this.photo_url = photo_url;
    }

    public String getServerAuthCode() {
        return server_auth_code;
    }

    public void setServerAuthCode(String server_auth_code) {
        this.server_auth_code = server_auth_code;
    }
}

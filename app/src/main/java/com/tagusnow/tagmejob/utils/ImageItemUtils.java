package com.tagusnow.tagmejob.utils;

import com.tagusnow.tagmejob.model.v2.feed.ImageItem;

import java.util.ArrayList;
import java.util.List;

public class ImageItemUtils {
    int currentOffset;

    public ImageItemUtils() {}

    public List<ImageItem> moarItems(int qty,String path) {
        List<ImageItem> items = new ArrayList<>();

        for (int i = 0; i < qty; i++) {
            int colSpan = Math.random() < 0.2f ? 2 : 1;
            // Swap the next 2 lines to have items with variable
            // column/row span.
            // int rowSpan = Math.random() < 0.2f ? 2 : 1;
            int rowSpan = colSpan;
            ImageItem item = new ImageItem(colSpan, rowSpan, currentOffset + i,path);
            items.add(item);
        }

        currentOffset += qty;

        return items;
    }
}

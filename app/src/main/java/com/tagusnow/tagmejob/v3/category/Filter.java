package com.tagusnow.tagmejob.v3.category;

import android.support.annotation.DrawableRes;

import java.io.Serializable;
import java.util.List;

public class Filter implements Serializable {
    public Filter() {
        super();
    }

    private @DrawableRes int resId;

    public FilterListener.FilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(FilterListener.FilterType filterType) {
        this.filterType = filterType;
    }

    private FilterListener.FilterType filterType;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    private String text;

    public Filter(int resId, FilterListener.FilterType filterType, String text, List<String> tags) {
        this.resId = resId;
        this.filterType = filterType;
        this.text = text;
        this.tags = tags;
    }

    private List<String> tags;
}

package com.tagusnow.tagmejob.v3.category;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

public class FilterAdapter extends RecyclerView.Adapter<FilterCellHolder> {

    public FilterAdapter(Activity activity, FilterListener filterListener, List<Filter> filters) {
        this.activity = activity;
        this.filterListener = filterListener;
        this.filters = filters;
    }

    private Activity activity;
    private FilterListener filterListener;
    private List<Filter> filters;

    @NonNull
    @Override
    public FilterCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterCellHolder(new FilterCell(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull FilterCellHolder holder, int position) {
        Filter filter = filters.get(position);
        holder.BindView(activity,filter.getFilterType(),filter.getResId(),filter.getText(),filterListener);
    }

    @Override
    public int getItemCount() {
        return filters.size();
    }
}

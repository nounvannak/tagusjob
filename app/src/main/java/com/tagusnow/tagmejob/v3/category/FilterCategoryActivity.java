package com.tagusnow.tagmejob.v3.category;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.V2.Model.Location;
import com.tagusnow.tagmejob.V2.Model.LocationPaginate;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.model.v2.feed.Setting;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.feed.FeedJob;
import com.tagusnow.tagmejob.v3.feed.FeedJobListener;
import com.tagusnow.tagmejob.v3.feed.FeedListener;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJob;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobListener;
import com.tagusnow.tagmejob.v3.like.LikeResultCell;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterCategoryActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, TextWatcher, FilterListener, FilterCategoryListener, FeedJobListener, SmallFeedJobListener, FeedListener, LikeResultListener, FeedCVListener, LoadingListener<LoadingView> {

    private static final String TAG = FilterCategoryActivity.class.getSimpleName();
    public static final String CATEGORY = "FilterCategoryActivity_category";
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private FloatingActionButton fabTop;
    private SmUser Auth;
    private Category category;
    private FeedResponse feedResponse;
    private List<Feed> feeds;
    private FilterCategoryAdapter adapter;
    private FilterCategoryHeader filterCategoryHeader;
    private String searchText = "";
    private List<String> categories,cities,levels,workTypes,salaries;
    private List<String> tagsOfCity = new ArrayList<>(),
            tagsOfLevels = new ArrayList<>(),
            tagsOfWorkType = new ArrayList<>(),
            tagsOfSalary = new ArrayList<>();
    private List<RequestBody> categoryBody = new ArrayList<>();
    private List<RequestBody> cityBody = new ArrayList<>();
    private List<RequestBody> levelBody = new ArrayList<>();
    private List<RequestBody> workTypeBody = new ArrayList<>();
    private List<RequestBody> salaryBody = new ArrayList<>();
    private List<Location> rawLocations;
    private Setting rawLevel,rawWorkType;
    private FilterAdapter filterAdapter;
    private List<Filter> filters = new ArrayList<>();
    private LoadingView loadingView;
    private FeedService service = ServiceGenerator.createService(FeedService.class);

    /* Socket.io */
    Socket socket;
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalLikes = obj.getInt("count_likes");
                            int userID = obj.getInt("user_id");
                            String likesResult = obj.getString("like_result");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);

                                    feedDetail.setCount_likes(totalLikes);
                                    feedDetail.setLike_result(likesResult);
                                    feedDetail.setIs_like(Auth.getId() == userID);

                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);

                                        feedDetail.setCount_likes(totalLikes);
                                        feedDetail.setLike_result(likesResult);
                                        feedDetail.setIs_like(Auth.getId() == userID);

                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });

        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalComments = obj.getInt("count_comments");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);
                                    feedDetail.setCount_comments(totalComments);
                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);
                                        feedDetail.setCount_comments(totalComments);
                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        int index = feeds.indexOf(feed);
                                        Feed updateFeed = feeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        feeds.set(index,updateFeed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            int index = feeds.indexOf(feed);
                                            Feed updateFeed = feeds.get(index);
                                            updateFeed.setFeed(feedJson.getFeed());
                                            feeds.set(index,updateFeed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        feeds.remove(feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("user_id");
                            if (Auth.getId() == userID){
                                Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                                if (feedJson != null){
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                        List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                        if (lists.size() > 0){
                                            Feed feed = lists.get(0);
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }else {
                                        for (Feed feed: feeds){
                                            if (feed.getId() == feedJson.getId()){
                                                feeds.remove(feed);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Runnable RunCallback = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(false);
            fetchFeed();
        }
    };
    private Callback<FeedResponse> FetchFeedCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.message());
                    Log.e(TAG,response.headers().toString());
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<FeedResponse> FetchNextFeedCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchNextFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };

    @NonNull
    @Override
    protected RequestBody createPartFromString(String descriptionString) {
        return super.createPartFromString(descriptionString);
    }

    private void InitSocket(){
        socket = new App().getSocket();
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.connect();
    }
    /* End Socket.io */

    private void fetchFeed(){
        preparedRequestBody();
        service.Search(Auth.getId(),1,searchText,categoryBody,cityBody,levelBody,workTypeBody,salaryBody,10).enqueue(FetchFeedCallback);
    }

    private void preparedRequestBody(){
        categoryBody.add(createPartFromString(String.valueOf(category.getId())));

        if (this.cities!=null && this.cities.size() > 0){
            for (String value : this.cities){
                int id = this.cities.indexOf(value);
                cityBody.add(createPartFromString(String.valueOf(id)));
            }
        }else {
            cityBody.add(createPartFromString("0"));
        }

        if (this.levels!=null && this.levels.size() > 0){
            for (String value : this.levels){
                levelBody.add(createPartFromString(value));
            }
        }else {
            levelBody.add(createPartFromString("All Levels"));
        }

        if (this.workTypes!=null && this.workTypes.size() > 0){
            for (String value : this.workTypes){
                workTypeBody.add(createPartFromString(value));
            }
        }else {
            workTypeBody.add(createPartFromString("Other"));
        }

        if (this.salaries!=null && this.salaries.size() > 0){
            for (String value : this.salaries){
                salaryBody.add(createPartFromString(value));
            }
        }else {
            salaryBody.add(createPartFromString("Other"));
        }
    }

    private void fetchFeed(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds = feedResponse.getData();
        CheckNext();
        filterAdapter = new FilterAdapter(this,this,filters);
        filterCategoryHeader.setAdapter(filterAdapter);
        filterCategoryHeader.setResult(feedResponse.getTotal());
        adapter.NewData(feeds);
        recyclerView.setAdapter(adapter);
        loadingView.setLoading(feedResponse.getTotal() > feedResponse.getTo());
    }

    private void fetchNextFeed(){
        boolean isNext = feedResponse.getCurrent_page() != feedResponse.getLast_page();
        if (isNext){
            int page = feedResponse.getCurrent_page() + 1;
            preparedRequestBody();
            service.Search(Auth.getId(),1,searchText,categoryBody,cityBody,levelBody,workTypeBody,salaryBody,10,page).enqueue(FetchNextFeedCallback);
        }
    }

    private void fetchNextFeed(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds.addAll(feedResponse.getData());
        CheckNext();
        loadingView.setLoading(feedResponse.getTotal() > feedResponse.getTo());
        adapter.NextData(feedResponse.getData());
        adapter.notifyDataSetChanged();
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    fetchNextFeed();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,FilterCategoryActivity.class);
    }

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();
        category = new Gson().fromJson(getIntent().getStringExtra(CATEGORY),Category.class);
        LocationPaginate locationPaginate = new Gson().fromJson(new Gson().toJson(new Repository(this).restore().getLocation()),LocationPaginate.class);
        if (locationPaginate != null){
            rawLocations = locationPaginate.getData();
        }

        if (rawLocations != null && rawLocations.size() > 0){
            for (Location location : rawLocations){
                tagsOfCity.add(location.getName());
            }
            filters.add(new Filter(R.drawable.map_color, FilterListener.FilterType.CITY,"Location",tagsOfCity));
        }

        rawLevel = new Repository(this).restore().getLevel();
        rawWorkType = new Repository(this).restore().getWorkingTime();

        if (rawLevel != null){
            tagsOfLevels.addAll(Arrays.asList(rawLevel.getValue().split(",")));
            filters.add(new Filter(R.drawable.list_color, FilterListener.FilterType.WORKTYPE,"Level",tagsOfLevels));
        }

        if (rawWorkType != null){
            tagsOfWorkType.addAll(Arrays.asList(rawWorkType.getValue().split(",")));
            filters.add(new Filter(R.drawable.alarm_clock_color, FilterListener.FilterType.WORKTYPE,"Work Type",tagsOfWorkType));
        }

        tagsOfSalary.add("Negotiation");
        tagsOfSalary.add("Less 500$");
        tagsOfSalary.add("Less 1,000$");
        tagsOfSalary.add("Less 2,000$");
        tagsOfSalary.add("2,000$ Up");
        tagsOfSalary.add("Other");
        filters.add(new Filter(R.drawable.diamond_color, FilterListener.FilterType.SALARY,"Salary",tagsOfSalary));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_category);

        InitTemp();
        InitSocket();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(category.getTitle());
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        fabTop = (FloatingActionButton)findViewById(R.id.fabTop);
        fabTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

        adapter = new FilterCategoryAdapter(this,(Auth.getUser_type() == SmUser.FIND_JOB),this,this,this,this,this,this,this);
        loadingView = adapter.getLoadingView();
        filterCategoryHeader = adapter.getFilterCategoryHeader();

        fetchFeed();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(RunCallback,2000);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        this.searchText = charSequence.toString();
        this.fetchFeed();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onItemSelected(FilterHeader view, FilterType filterType, String selected) {

    }

    @Override
    public void onItemSelected(FilterHeader view, FilterType filterType, List<String> lists, int position) {

    }

    @Override
    public void selected(FilterHeader view, FilterType filterType, List<String> selectedLists) {
        if (filterType == FilterType.CITY){
            cities = selectedLists;
            fetchFeed();
        }else if (filterType == FilterType.LEVEL){
            levels = selectedLists;
            fetchFeed();
        }else if (filterType == FilterType.WORKTYPE){
            workTypes = selectedLists;
            fetchFeed();
        }else if (filterType == FilterType.SALARY){
            salaries = selectedLists;
            fetchFeed();
        }
    }

    @Override
    public void onClick(FilterCell view, FilterType filterType) {
        filterCategoryHeader.setFilterType(filterType);
        if (filterType == FilterType.CITY){
            filterCategoryHeader.setTags(tagsOfCity);
        }else if (filterType == FilterType.LEVEL){
            filterCategoryHeader.setTags(tagsOfLevels);
        }else if (filterType == FilterType.WORKTYPE){
            filterCategoryHeader.setTags(tagsOfWorkType);
        }else if (filterType == FilterType.SALARY){
            filterCategoryHeader.setTags(tagsOfSalary);
        }
        filterCategoryHeader.setDropdown(true);
    }

    @Override
    public void cancel(FilterHeader view, FilterType filterType) {
        filterCategoryHeader.setDropdown(false);
    }

    @Override
    public void changeFeedSize(FilterCategoryHeader view, boolean isBig) {
        this.adapter.setBig(isBig);
    }

    @Override
    public void onLike(FeedJob view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    private void LikeFeed(SmUser Auth, Feed feed){
        if (!feed.getFeed().isIs_like()){
            int index = feeds.indexOf(feed);
            FeedDetail temp = feed.getFeed();
            temp.setIs_like(true);
            temp.setLike_result("You liked this post.");
            temp.setCount_likes(1);
            feed.setFeed(temp);
            feeds.set(index,feed);
            adapter.notifyDataSetChanged();
            service.LikeFeed(feed.getId(),Auth.getId()).enqueue(new Callback<SmTagFeed.Like>() {
                @Override
                public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                    if (response.isSuccessful()){
                        Log.d(TAG, response.message());
                    }else {
                        try {
                            Log.e(TAG,response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onComment(FeedJob view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onShare(FeedJob view, Feed feed) {
        view.ShareFeed(feed);
    }

    @Override
    public void onMore(FeedJob view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void onDetail(FeedJob view, Feed feed) {
        view.DetailFeedJob(Auth,feed);
    }

    @Override
    public void onApplyNow(FeedJob view, Feed feed) {
        view.ApplyJob(feed);
    }

    @Override
    public void showProfile(FeedJob view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(FeedJob view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void onMore(SmallFeedJob view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void viewDetail(SmallFeedJob view, Feed feed) {
        view.DetailFeedJob(Auth,feed);
    }

    @Override
    public void saveFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Save Post").setMessage("Are you sure to save this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!feed.getFeed().isIs_save()){
                    int index = feeds.indexOf(feed);
                    FeedDetail temp = feed.getFeed();
                    feed.setIs_save(1);
                    temp.setIs_save(true);
                    feed.setFeed(temp);
                    feeds.set(index,feed);
                    adapter.notifyDataSetChanged();
                    service.SaveFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                        @Override
                        public void onResponse(Call<Feed> call, Response<Feed> response) {
                            if (response.isSuccessful()){
                                Log.w(TAG,response.message());
                            }else {
                                try {
                                    Log.e(TAG,response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Feed> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void editFeed(SmUser user, Feed feed) {
        EditPostNormal(feed);
    }

    @Override
    public void hideFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Hide Post").setMessage("Are you sure to hide this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feeds.remove(feed);
                adapter.notifyDataSetChanged();
                service.HideFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void removeFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Post").setMessage("Are you sure to remove this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feeds.remove(feed);
                adapter.notifyDataSetChanged();
                service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void select(LikeResultCell cell, SmUser user) {
        cell.OpenProfile(Auth,user);
    }

    @Override
    public void connect(LikeResultCell cell, SmUser user) {
        cell.DoFollow(Auth,user);
    }

    @Override
    public void onLike(FeedCV view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    @Override
    public void onComment(FeedCV view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onDownload(FeedCV view, Feed feed) {
        view.DisplayDownloadBox(feed);
    }

    @Override
    public void viewDetail(FeedCV view, Feed feed) {
        view.DetailFeedCV(feed);
    }

    @Override
    public void viewProfile(FeedCV view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(FeedCV view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void onClickedMore(FeedCV view, Feed feed, boolean isShow) {

    }

    @Override
    public void reload(LoadingView view) {
        fetchNextFeed();
    }
}

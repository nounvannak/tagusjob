package com.tagusnow.tagmejob.v3.category;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVHolder;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.feed.FeedJob;
import com.tagusnow.tagmejob.v3.feed.FeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.FeedJobListener;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJob;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobListener;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;

import java.util.List;

public class FilterCategoryAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private static final int FILTER = 0;
    private static final int BIG = 1;
    private static final int SMALL = 2;
    private static final int LOADING = 3;

    public FilterCategoryHeader getFilterCategoryHeader() {
        return filterCategoryHeader;
    }

    private FilterCategoryHeader filterCategoryHeader;
    private FilterListener filterListener;
    private FilterCategoryListener filterCategoryListener;
    private FeedJobListener feedJobListener;
    private SmallFeedJobListener smallFeedJobListener;
    private FeedCVListener feedCVListener;
    private TextWatcher textWatcher;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public void setBig(boolean big) {
        isBig = big;
        notifyDataSetChanged();
    }

    private boolean isBig = false;
    private boolean isJob;
    private int other = 2;

    public FilterCategoryAdapter(Activity activity, boolean isJob, FilterListener filterListener, FilterCategoryListener filterCategoryListener, FeedCVListener feedCVListener, FeedJobListener feedJobListener, SmallFeedJobListener smallFeedJobListener, TextWatcher textWatcher, LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.filterListener = filterListener;
        this.filterCategoryListener = filterCategoryListener;
        this.feedJobListener = feedJobListener;
        this.smallFeedJobListener = smallFeedJobListener;
        this.feedCVListener = feedCVListener;
        this.textWatcher = textWatcher;
        this.isJob = isJob;
        this.filterCategoryHeader = new FilterCategoryHeader(activity);
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public void NewData(List<Feed> feeds){
        this.feeds = feeds;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
    }

    private List<Feed> feeds;


    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if (position == 0){
            type = FILTER;
        }else if (position == (getItemCount() - 1)){
            type = LOADING;
        }else {
            if (isBig){
                type = BIG;
            }else {
                type = SMALL;
            }
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == FILTER) {
            return new FilterCategoryHolder(filterCategoryHeader);
        }else if (viewType == BIG){
            if (isJob){
                return new FeedJobHolder(new FeedJob(activity));
            }else {
                return new FeedCVHolder(new FeedCV(activity));
            }
        }else if (viewType == SMALL){
            if (isJob){
                return new SmallFeedJobHolder(new SmallFeedJob(activity));
            }else {
                return new FeedCVHolder(new FeedCV(activity));
            }
        }else {
            return new LoadingHolder(loadingView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == FILTER){
            ((FilterCategoryHolder) holder).BindView(activity,filterListener,filterCategoryListener,textWatcher);
        }else if (getItemViewType(position) == BIG){
            if (isJob){
                ((FeedJobHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedJobListener);
            }else {
                ((FeedCVHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedCVListener);
            }
        }else if (getItemViewType(position) == SMALL){
            if (isJob){
                ((SmallFeedJobHolder) holder).BindView(activity,feeds.get(position - (other - 1)),smallFeedJobListener);
            }else {
                ((FeedCVHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedCVListener);
            }
        }else {
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }
    }

    @Override
    public int getItemCount() {
        return feeds.size() + other;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

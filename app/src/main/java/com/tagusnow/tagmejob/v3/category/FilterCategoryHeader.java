package com.tagusnow.tagmejob.v3.category;

import android.app.Activity;
import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.util.List;

public class FilterCategoryHeader extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private EditText searchBox;
    private FilterHeader filterHeader;
    private RelativeLayout layoutControl;
    private ImageButton btnSmall,btnBig;
    private TextView result;

    public void setFilterType(FilterListener.FilterType filterType) {
        this.filterType = filterType;
        filterHeader.setFilterType(filterType);
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
        filterHeader.setTags(tags);
    }

    private List<String> tags;

    public void setDropdown(boolean dropdown) {
        isDropdown = dropdown;
        filterHeader.setDropdown(dropdown);
    }

    private boolean isDropdown;
    private FilterListener.FilterType filterType;
    private FilterCategoryListener listener;
    private FilterListener filterListener;
    private TextWatcher textWatcher;

    public void setAdapter(FilterAdapter adapter) {
        this.adapter = adapter;
        filterHeader.setAdapter(adapter);
    }

    private FilterAdapter adapter;

    public void setTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
        searchBox.addTextChangedListener(textWatcher);
    }

    public void setListener(FilterCategoryListener listener) {
        this.listener = listener;
    }

    public void setFilterListener(FilterListener filterListener) {
        this.filterListener = filterListener;
        filterHeader.setListener(filterListener);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
        filterHeader.setActivity(activity);
    }

    public void setResult(int total){
        result.setText(context.getString(R.string.results,total));
    }

    private void InitUI(Context context){
        inflate(context, R.layout.filter_category_header,this);
        this.context = context;

        searchBox = (EditText)findViewById(R.id.searchBox);
        filterHeader = (FilterHeader)findViewById(R.id.filterHeader);
        btnBig = (ImageButton)findViewById(R.id.btnBig);
        btnSmall = (ImageButton)findViewById(R.id.btnSmall);
        result = (TextView)findViewById(R.id.result);
        layoutControl = (RelativeLayout)findViewById(R.id.layoutControl);
        filterHeader.setDropdown(false);
        btnSmall.setOnClickListener(this);
        btnBig.setOnClickListener(this);
    }

    public FilterCategoryHeader(Context context) {
        super(context);
        InitUI(context);
    }

    public FilterCategoryHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public FilterCategoryHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public FilterCategoryHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public boolean hideControl(){
        layoutControl.setVisibility(GONE);
        return false;
    }

    public boolean showControl(){
        layoutControl.setVisibility(VISIBLE);
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view == btnBig){
            listener.changeFeedSize(this,true);
        }else if (view == btnSmall){
            listener.changeFeedSize(this,false);
        }
    }
}

package com.tagusnow.tagmejob.v3.category;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.view.View;

public class FilterCategoryHolder extends RecyclerView.ViewHolder {

    private FilterCategoryHeader filterCategoryHeader;

    public FilterCategoryHolder(View itemView) {
        super(itemView);
        filterCategoryHeader = (FilterCategoryHeader)itemView;
    }

    public void BindView(Activity activity, FilterListener filterListener, FilterCategoryListener filterCategoryListener, TextWatcher textWatcher){
        filterCategoryHeader.setActivity(activity);
        filterCategoryHeader.setFilterListener(filterListener);
        filterCategoryHeader.setListener(filterCategoryListener);
        filterCategoryHeader.setTextWatcher(textWatcher);
    }
}

package com.tagusnow.tagmejob.v3.category;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class FilterCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private ImageView icon;
    private TextView text;

    public void setFilterType(FilterListener.FilterType filterType) {
        this.filterType = filterType;
    }

    private FilterListener.FilterType filterType;

    public void setListener(FilterListener listener) {
        this.listener = listener;
    }

    private FilterListener listener;

    public void setIcon(@DrawableRes int resId){
        icon.setImageResource(resId);
    }

    public void setText(String str){
        text.setText(str);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.filter_cell,this);

        icon = (ImageView)findViewById(R.id.icon);
        text = (TextView)findViewById(R.id.text);
        this.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public FilterCell(Context context) {
        super(context);
        InitUI(context);
    }

    public FilterCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public FilterCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public FilterCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == this){
            listener.onClick(this, filterType);
        }
    }
}

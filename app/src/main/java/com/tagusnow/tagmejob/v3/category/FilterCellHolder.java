package com.tagusnow.tagmejob.v3.category;

import android.app.Activity;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class FilterCellHolder extends RecyclerView.ViewHolder {

    private FilterCell filterCell;

    public FilterCellHolder(View itemView) {
        super(itemView);
        filterCell = (FilterCell)itemView;
    }

    public void BindView(Activity activity, FilterListener.FilterType filterType, @DrawableRes int resId,String text,FilterListener filterListener){
        filterCell.setActivity(activity);
        filterCell.setFilterType(filterType);
        filterCell.setIcon(resId);
        filterCell.setText(text);
        filterCell.setListener(filterListener);
    }
}

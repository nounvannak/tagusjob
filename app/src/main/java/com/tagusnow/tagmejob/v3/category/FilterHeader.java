package com.tagusnow.tagmejob.v3.category;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.util.ArrayList;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

public class FilterHeader extends TagUsJobRelativeLayout implements View.OnClickListener, TagView.OnTagClickListener {

    private Context context;
    private RecyclerView recyclerView;
    private TagContainerLayout layoutTag;
    private Button btnSelect, btnCancel;
    private FilterListener listener;
    private FilterListener.FilterType filterType;
    private RelativeLayout layoutDropdown;
    private List<String> selectedList = new ArrayList<>();
    private List<String> tags;
    private FilterAdapter adapter;
    private boolean isDropdown;

    public void setAdapter(FilterAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    public void setDropdown(boolean dropdown) {
        isDropdown = dropdown;
        if (dropdown) {
            layoutDropdown.setVisibility(VISIBLE);
        } else {
            layoutDropdown.setVisibility(GONE);
        }
    }

    public void setListener(FilterListener listener) {
        this.listener = listener;
    }

    public void setFilterType(FilterListener.FilterType filterType) {
        this.filterType = filterType;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
        List<int[]> colors = new ArrayList<int[]>();
        //int[] color = {TagBackgroundColor, TabBorderColor, TagTextColor, TagSelectedBackgroundColor}
        int[] color1 = {Color.RED, Color.BLACK, Color.WHITE, Color.YELLOW};
        int[] color2 = {Color.BLUE, Color.BLACK, Color.WHITE, Color.YELLOW};
        colors.add(color1);
        colors.add(color2);
        layoutTag.setTags(tags);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context) {

        inflate(context, R.layout.filter_header, this);
        this.context = context;
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        layoutDropdown = (RelativeLayout) findViewById(R.id.layoutDropdown);
        layoutTag = (TagContainerLayout) findViewById(R.id.layoutTag);
        btnSelect = (Button) findViewById(R.id.btnSelect);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSelect.setText(R.string.select_);
        layoutTag.setEnableCross(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layoutTag.setBackgroundColor(context.getColor(R.color.white));
        }

        btnSelect.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        layoutTag.setOnTagClickListener(this);

    }

    public FilterHeader(Context context) {
        super(context);
        InitUI(context);
    }

    public FilterHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public FilterHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public FilterHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnSelect) {
            listener.selected(this, filterType, selectedList);
        } else if (view == btnCancel) {
            listener.cancel(this, filterType);
        }
    }

    @Override
    public void onTagClick(int position, String text) {
        selectedList.add(text);
        btnSelect.setText(context.getString(R.string.selected_0, selectedList.size()));
    }

    @Override
    public void onTagLongClick(int position, String text) {
        CopyText(text);
    }

    @Override
    public void onTagCrossClick(int position) {
        selectedList.remove(tags.get(position));
        btnSelect.setText(context.getString(R.string.selected_0, selectedList.size()));
    }
}
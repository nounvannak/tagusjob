package com.tagusnow.tagmejob.v3.category;

import java.util.List;

public interface FilterListener {
    enum FilterType {POSITION,CITY,LEVEL,WORKTYPE,SALARY};
    void onItemSelected(FilterHeader view,FilterType filterType,String selected);
    void onItemSelected(FilterHeader view, FilterType filterType, List<String> lists,int position);
    void selected(FilterHeader view,FilterType filterType,List<String> selectedLists);
    void onClick(FilterCell view,FilterType filterType);
    void cancel(FilterHeader view,FilterType filterType);
}

package com.tagusnow.tagmejob.v3.category;

import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface SubcribeListener<V extends TagUsJobRelativeLayout> {
    void subcribe(V view, Category category);
    void position(V view, Category category);
}

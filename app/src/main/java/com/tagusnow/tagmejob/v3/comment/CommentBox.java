package com.tagusnow.tagmejob.v3.comment;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectAdapter;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class CommentBox extends TagUsJobRelativeLayout implements View.OnClickListener, TextWatcher {

    private Context context;
    private ImageButton camera,send;
    private EditText input;
    private View comment_border;
    private RecyclerView recyclerView;
    private boolean hasFile;
    private CommentBoxListener<CommentBox> boxListener;
    private ImageSelectAdapter adapter;

    private void InitUI(Context context){
        inflate(context, R.layout.comment_box1,this);
        this.context = context;

        camera = (ImageButton)findViewById(R.id.camera);
        send = (ImageButton)findViewById(R.id.send);
        input = (EditText)findViewById(R.id.input);

        comment_border = (View)findViewById(R.id.comment_border);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        camera.setOnClickListener(this);
        send.setOnClickListener(this);
        input.addTextChangedListener(this);

    }

    public CommentBox(Context context) {
        super(context);
        InitUI(context);
    }

    public CommentBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public CommentBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public CommentBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setFocusable(boolean focusable){
        input.setFocusable(focusable);
    }

    public void AutoResizeHeightOfInput(){
        if (!input.getText().toString().equals("")){
            send.setBackgroundResource(R.drawable.send_color);
            send.setEnabled(true);
            if (hasFile){
                recyclerView.setVisibility(View.VISIBLE);
                comment_border.setVisibility(View.VISIBLE);
            }else{
                recyclerView.setVisibility(View.GONE);
                comment_border.setVisibility(View.GONE);
            }
            if (input.getText().length() > 30){
                input.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.MORE_TEXT);
            }else{
                input.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
            }
        }else {
            if (hasFile){
                recyclerView.setVisibility(View.VISIBLE);
                comment_border.setVisibility(View.VISIBLE);
                send.setBackgroundResource(R.drawable.send_color);
                send.setEnabled(true);
            }else {
                recyclerView.setVisibility(View.GONE);
                comment_border.setVisibility(View.GONE);
                send.setBackgroundResource(R.drawable.send_gray);
                send.setEnabled(false);
            }
            input.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
        }
    }

    public void viewCondition(){
        recyclerView.setVisibility(View.GONE);
        comment_border.setVisibility(View.GONE);
        send.setEnabled(false);
        send.setBackgroundResource(R.drawable.send_gray);
        AutoResizeHeightOfInput();
    }

    public boolean isReset(){
        hasFile = false;
        input.getText().clear();
        AutoResizeHeightOfInput();
        viewCondition();
        return true;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view == camera){
            boxListener.openCamera(this);
        }else if (view == send){
            boxListener.onSend(this);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        AutoResizeHeightOfInput();
        boxListener.onTextChanged(this,charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    public boolean isHasFile() {
        return hasFile;
    }

    public void setHasFile(boolean hasFile) {
        this.hasFile = hasFile;
    }

    public void setBoxListener(CommentBoxListener<CommentBox> boxListener) {
        this.boxListener = boxListener;
    }

    public void setAdapter(ImageSelectAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }
}

package com.tagusnow.tagmejob.v3.comment;

import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface CommentBoxListener<V extends TagUsJobRelativeLayout> {
    void openCamera(V view);
    void onSend(V view);
    void onTextChanged(V view,String comment);
}

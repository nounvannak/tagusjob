package com.tagusnow.tagmejob.v3.comment;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private CircleImageView commentUserProfile;
    private TextView comment_username,comment_time;
    private HtmlTextView comment_text;
    private ImageView comment_image;
    private LinearLayout layout_comment_image;
    private ImageButton comment_btn_option;
    private Comment comment;

    public void setUser(SmUser user) {
        this.user = user;
    }

    private SmUser user;
    private CommentListener.OnOptionSelected<CommentCell> OnOptionSelected;

    public void setComment(Comment comment) {
        this.comment = comment;
        setUserProfile(commentUserProfile,comment.getComment_user());
        setName(comment_username,comment.getComment_user());
        if (!comment.isNullDescription()){
            comment_text.setVisibility(View.VISIBLE);
            comment_text.setHtml(comment.getDes());
        }else {
            comment_text.setVisibility(View.GONE);
        }
        if (!comment.isNullImage()){
            layout_comment_image.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(comment.getImage().contains("/storage/emulated/0/") ? comment.getImage() : SuperUtil.getCommentPicture(comment.getImage(),"100"))
                    .fitCenter()
                    .error(android.R.drawable.ic_menu_report_image)
                    .into(comment_image);
        }else {
            layout_comment_image.setVisibility(View.GONE);
        }

        comment_time.setText(comment.getTimeline());

        if (this.user!=null && this.user.getId()==comment.getComment_user().getId()){
            comment_btn_option.setVisibility(View.VISIBLE);
        }else{
            comment_btn_option.setVisibility(View.GONE);
        }
    }

    public void setCommentListener(CommentListener<CommentCell> commentListener) {
        this.commentListener = commentListener;
    }

    private CommentListener<CommentCell> commentListener;

    private void InitUI(Context context){
        inflate(context, R.layout.comment_component_layout,this);
        this.context = context;
        commentUserProfile = (CircleImageView)findViewById(R.id.commentUserProfile);
        comment_username = (TextView)findViewById(R.id.comment_username);
        comment_text = (HtmlTextView)findViewById(R.id.comment_text);
        comment_image = (ImageView)findViewById(R.id.comment_image);
        layout_comment_image = (LinearLayout)findViewById(R.id.layout_comment_image);
        comment_time = (TextView) findViewById(R.id.comment_time);
        comment_btn_option = (ImageButton) findViewById(R.id.comment_btn_option);
        comment_image.setOnClickListener(this);
        comment_btn_option.setOnClickListener(this);
        commentUserProfile.setOnClickListener(this);
        comment_username.setOnClickListener(this);
    }

    public CommentCell(Context context) {
        super(context);
        InitUI(context);
    }

    public CommentCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public CommentCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public CommentCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view==comment_image){
            commentListener.displayImage(this,comment);
        }else if (view==comment_btn_option){
            commentListener.onOptionSelected(this,comment,OnOptionSelected);
        }else if (view==commentUserProfile){
            commentListener.profile(this,comment.getComment_user());
        }else if (view==comment_username){
            commentListener.profile(this,comment.getComment_user());
        }
    }

    public void setOnOptionSelected(CommentListener.OnOptionSelected<CommentCell> onOptionSelected) {
        OnOptionSelected = onOptionSelected;
    }
}

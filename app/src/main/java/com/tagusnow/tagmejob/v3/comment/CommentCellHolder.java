package com.tagusnow.tagmejob.v3.comment;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;

public class CommentCellHolder extends RecyclerView.ViewHolder {

    private CommentCell commentCell;

    public CommentCellHolder(View itemView) {
        super(itemView);
        commentCell = (CommentCell)itemView;
    }

    public void BindView(Activity activity, SmUser user, Comment comment, CommentListener<CommentCell> commentListener, CommentListener.OnOptionSelected<CommentCell> onOptionSelected){
        commentCell.setActivity(activity);
        commentCell.setUser(user);
        commentCell.setComment(comment);
        commentCell.setCommentListener(commentListener);
        commentCell.setOnOptionSelected(onOptionSelected);
    }
}

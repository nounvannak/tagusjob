package com.tagusnow.tagmejob.v3.comment;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface CommentListener<V extends TagUsJobRelativeLayout> {

    void onOptionSelected(V view,Comment comment,OnOptionSelected<V> onOptionSelected);
    void select(V view, Comment comment);
    void profile(V view, SmUser user);
    void displayImage(V view,Comment comment);

    interface OnOptionSelected<V extends TagUsJobRelativeLayout> {
        void copy(V view, Comment comment);
        void edit(V view, Comment comment);
        void delete(V view, Comment comment);
    }

    interface OnUpdateListener<V extends TagUsJobRelativeLayout>{
        void camera(V view,Comment comment);
        void cancel(V view);
        void update(V view,Comment comment);
        void onUpdateTextChanged(V view,Comment comment,String text);
    }
}

package com.tagusnow.tagmejob.v3.comment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectAdapter;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditCommentCell extends TagUsJobRelativeLayout implements TextWatcher, View.OnClickListener {

    private Context context;
    private EditText input;
    private ImageButton btnCamera;
    private CircleImageView profile;
    private Button btnCancel,btnUpdate;
    private RelativeLayout layoutImage;
    private RecyclerView recyclerView;
    private ImageSelectAdapter adapter;
    private Comment comment;
    private boolean hasFile;
    private CommentListener.OnUpdateListener<EditCommentCell> onUpdateListener;

    private void InitUI(Context context){
        inflate(context, R.layout.edit_comment_cell,this);
        this.context = context;

        input = (EditText)findViewById(R.id.input);
        input.addTextChangedListener(this);

        btnCamera = (ImageButton)findViewById(R.id.btnCamera);
        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        btnCancel = (Button)findViewById(R.id.btnCancel);

        profile = (CircleImageView)findViewById(R.id.profile);

        layoutImage = (RelativeLayout)findViewById(R.id.layoutImage);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        btnCamera.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public EditCommentCell(Context context) {
        super(context);
        InitUI(context);
    }

    public EditCommentCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public EditCommentCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public EditCommentCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        onUpdateListener.onUpdateTextChanged(this,comment,s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        if (v == btnCamera){
            onUpdateListener.camera(this,comment);
        }else if (v == btnCancel){
            onUpdateListener.cancel(this);
        }else if (v == btnUpdate){
            onUpdateListener.update(this,comment);
        }
    }

    public void setOnUpdateListener(CommentListener.OnUpdateListener<EditCommentCell> onUpdateListener) {
        this.onUpdateListener = onUpdateListener;
    }

    public void setAdapter(ImageSelectAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setComment(Comment comment) {
        this.comment = comment;
        input.setCursorVisible(true);
        setUserProfile(profile,comment.getComment_user());
        if (!comment.isNullDescription()){
            input.setText(comment.getDes());
        }
    }

    public void AutoResizeHeightOfInput(){
        if (!input.getText().toString().equals("")){
            if (hasFile){
                recyclerView.setVisibility(View.VISIBLE);
                layoutImage.setVisibility(View.VISIBLE);
            }else{
                recyclerView.setVisibility(View.GONE);
                layoutImage.setVisibility(View.GONE);
            }
            if (input.getText().length() > 30){
                input.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.MORE_TEXT);
            }else{
                input.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
            }
        }else {
            if (hasFile){
                recyclerView.setVisibility(View.VISIBLE);
                layoutImage.setVisibility(View.VISIBLE);
            }else {
                recyclerView.setVisibility(View.GONE);
                layoutImage.setVisibility(View.GONE);
            }
            input.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
        }
    }

    public void viewCondition(){
        recyclerView.setVisibility(View.GONE);
        layoutImage.setVisibility(View.GONE);
        AutoResizeHeightOfInput();
    }

    public boolean isReset(){
        input.getText().clear();
        enabledUpdate(false);
        hasFile = false;
        AutoResizeHeightOfInput();
        viewCondition();
        return true;
    }

    public void enabledUpdate(boolean enabled){
        if (enabled){
            btnUpdate.setEnabled(true);
            btnUpdate.setBackgroundColor(Color.parseColor("#1a7ab9"));
        }else {
            btnUpdate.setEnabled(false);
            btnUpdate.setBackgroundColor(Color.parseColor("#dddddd"));
        }
    }

    public void setHasFile(boolean hasFile) {
        this.hasFile = hasFile;
        if (hasFile){
            btnCamera.setVisibility(INVISIBLE);
            btnCamera.setEnabled(false);
        }else {
            btnCamera.setVisibility(VISIBLE);
            btnCamera.setEnabled(true);
        }
    }
}

package com.tagusnow.tagmejob.v3.comment;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class TypingCell extends TagUsJobRelativeLayout {

    private TextView text;

    public void setText(String text) {
        this.text.setText(text);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.typing_cell,this);
        text = (TextView)findViewById(R.id.text);
    }

    public TypingCell(Context context) {
        super(context);
        InitUI(context);
    }

    public TypingCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public TypingCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public TypingCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }
}

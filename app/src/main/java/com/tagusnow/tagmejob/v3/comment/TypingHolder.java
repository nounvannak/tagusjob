package com.tagusnow.tagmejob.v3.comment;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class TypingHolder extends RecyclerView.ViewHolder {

    private TypingCell typingCell;

    public TypingHolder(View itemView) {
        super(itemView);
        typingCell = (TypingCell)itemView;
    }

    public void BindView(Activity activity){
        typingCell.setActivity(activity);
    }
}

package com.tagusnow.tagmejob.v3.company;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.V2.Model.Company;

public class CompanyCardHolder extends RecyclerView.ViewHolder {

    private CompanyCard companyCard;

    public CompanyCardHolder(View itemView) {
        super(itemView);
        companyCard = (CompanyCard)itemView;
    }

    public void BindView(Activity activity, Company company,CompanyListener<CompanyCard> companyListener){
        companyCard.setActivity(activity);
        companyCard.setCompany(company);
        companyCard.setCompanyListener(companyListener);
    }
}

package com.tagusnow.tagmejob.v3.company;

import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface CompanyListener<V extends TagUsJobRelativeLayout> {
    void onSubcribe(V view, Company company);
    void onDetail(V view, Company company);
}

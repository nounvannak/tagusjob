package com.tagusnow.tagmejob.v3.company;

import android.app.Activity;
import android.content.Context;
import android.nfc.Tag;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class CompanyProfileHeader extends TagUsJobRelativeLayout {

    private ImageView logo;
    private TextView name,address;
    private SupportMapFragment mapFragment;
    private Company company;

    private void InitUI(Context context){
        inflate(context, R.layout.company_profile_header,this);

        logo = (ImageView)findViewById(R.id.logo);
        name = (TextView)findViewById(R.id.name);
        address = (TextView)findViewById(R.id.address);
    }

    public CompanyProfileHeader(Context context) {
        super(context);
        InitUI(context);
    }

    public CompanyProfileHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public CompanyProfileHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public CompanyProfileHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
        mapFragment = (SupportMapFragment) ((TagUsJobActivity) activity).getSupportFragmentManager().findFragmentById(R.id.map);
    }

    public void setCompany(Company company) {
        this.company = company;

        name.setText(company.getTitle_name());
        setAlbumPhoto(logo,company.getImage(),"original",true);
        address.setText(company.getAddress());
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(company.getLat(), company.getLng()))
                        .title(company.getTitle_name()));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(company.getLat(), company.getLng()), company.getZoom()));
            }
        });
    }
}

package com.tagusnow.tagmejob.v3.company;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.V2.Model.Company;

public class CompanyProfileHeaderHolder extends RecyclerView.ViewHolder {

    private CompanyProfileHeader companyProfileHeader;

    public CompanyProfileHeaderHolder(View itemView) {
        super(itemView);
        companyProfileHeader = (CompanyProfileHeader)itemView;
    }

    public void BindView(Activity activity, Company company){
        companyProfileHeader.setActivity(activity);
        companyProfileHeader.setCompany(company);
    }
}

package com.tagusnow.tagmejob.v3.company;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class FullCompanyCard extends TagUsJobRelativeLayout implements View.OnClickListener {
    private ImageView logo;
    private TextView name;
    private TextView address;
    private Button btnSubcribe;
    private Company company;
    private CompanyListener<FullCompanyCard> companyListener;

    private void InitUI(Context context){
        inflate(context, R.layout.full_company_card,this);

        logo = (ImageView)findViewById(R.id.logo);
        name = (TextView)findViewById(R.id.name);
        address = (TextView)findViewById(R.id.address);
        btnSubcribe = (Button)findViewById(R.id.btnSubcribe);

        btnSubcribe.setOnClickListener(this);
        logo.setOnClickListener(this);
    }

    public FullCompanyCard(Context context) {
        super(context);
        InitUI(context);
    }

    public FullCompanyCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public FullCompanyCard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public FullCompanyCard(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View v) {
        if (v == btnSubcribe){
            companyListener.onSubcribe(this,company);
        }else if (v == logo){
            companyListener.onDetail(this,company);
        }
    }

    public void setCompanyListener(CompanyListener<FullCompanyCard> companyListener) {
        this.companyListener = companyListener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setCompany(Company company) {
        this.company = company;

        setAlbumPhoto(logo,company.getImage(),"500");
        name.setText(company.getTitle_name());
        address.setText(company.getAddress());
    }
}

package com.tagusnow.tagmejob.v3.company;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.V2.Model.Company;

public class FullCompanyCardHolder extends RecyclerView.ViewHolder {

    private FullCompanyCard fullCompanyCard;

    public FullCompanyCardHolder(View itemView) {
        super(itemView);

        fullCompanyCard = (FullCompanyCard)itemView;
    }

    public void BindView(Activity activity, Company company, CompanyListener<FullCompanyCard> companyListener){
        fullCompanyCard.setActivity(activity);
        fullCompanyCard.setCompany(company);
        fullCompanyCard.setCompanyListener(companyListener);
    }
}

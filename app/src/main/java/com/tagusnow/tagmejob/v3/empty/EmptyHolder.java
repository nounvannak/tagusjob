package com.tagusnow.tagmejob.v3.empty;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class EmptyHolder extends RecyclerView.ViewHolder {

    private EmptyView emptyView;

    public EmptyHolder(View itemView) {
        super(itemView);
        emptyView = (EmptyView)itemView;
    }

    public void BindView(Activity activity){
        emptyView.setActivity(activity);
    }
}

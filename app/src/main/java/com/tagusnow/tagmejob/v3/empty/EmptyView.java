package com.tagusnow.tagmejob.v3.empty;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class EmptyView extends TagUsJobRelativeLayout {

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.empty_view,this);
    }

    public EmptyView(Context context) {
        super(context);
        InitUI(context);
    }

    public EmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }
}

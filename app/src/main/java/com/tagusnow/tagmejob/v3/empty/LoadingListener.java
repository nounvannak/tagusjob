package com.tagusnow.tagmejob.v3.empty;

import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface LoadingListener<V extends TagUsJobRelativeLayout> {
    void reload(V view);
}

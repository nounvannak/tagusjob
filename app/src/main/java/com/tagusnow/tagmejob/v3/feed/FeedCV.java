package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class FeedCV extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = FeedCV.class.getSimpleName();
    private Context context;
    /*View*/
    public ImageView user_image;
    public RelativeLayout body,body_content,show_more,footer,description;
    public TextView name,title,tel,email,location,description_text,text_show_more,like_result,comment_result,like_text;
    public ImageView icon_show_more,like_icon;
    public LinearLayout like_button,comment_button,download_button,like_and_comment_information;
    /*Object*/
    private Feed feed;

    public void setFeed(Feed feed) {
        this.feed = feed;
        InitFeed(feed);
    }

    private void InitFeed(Feed feed){
        if (feed!=null && feed.getDelete()==0 && feed.getIs_hide()==0){
            /*Profile Information*/
            setAlbumPhoto(user_image,(feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0) ? feed.getFeed().getAlbum().get(0) : null);
            /*Body*/
            setName(name,feed.getUser_post());
            title.setText(feed.getFeed().getTitle());
            tel.setText(feed.getFeed().getPhone());
            email.setText(feed.getFeed().getEmail());
            location.setText(feed.getFeed().getCity_name());

            if (feed.getFeed().getOriginalHastTagText()!=null && !feed.getFeed().getOriginalHastTagText().equals("")){
                show_more.setVisibility(VISIBLE);
                description_text.setText(feed.getFeed().getOriginalHastTagText());
            }else {
                show_more.setVisibility(GONE);
            }

            if (feed.getFeed().getCount_comments()==0 && feed.getFeed().getCount_likes()==0){
                isShowLikeAndCommentInfo = false;
            }else {
                isShowLikeAndCommentInfo = true;
            }

            if (isShowLikeAndCommentInfo){
                like_and_comment_information.setVisibility(VISIBLE);

                if (feed.getFeed().getCount_likes()==0){
                    isShowLike = false;
                }else {
                    isShowLike = true;
                }

                if (isShowLike){
                    like_result.setVisibility(VISIBLE);
                    like_result.setText(feed.getFeed().getLike_result());
                }else {
                    like_result.setVisibility(GONE);

                }

                if (feed.getFeed().isIs_like()){
                    like_icon.setImageResource(R.drawable.ic_action_like_blue);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        like_text.setTextColor(this.context.getColor(R.color.colorPrimary));
                    }
                }else {
                    like_icon.setImageResource(R.drawable.ic_action_like_black);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        like_text.setTextColor(this.context.getColor(R.color.colorIcon));
                    }
                }

                if (feed.getFeed().getCount_comments()==0){
                    isShowComment = false;
                }else {
                    isShowComment = true;
                }

                if (isShowComment){
                    comment_result.setVisibility(VISIBLE);
                    comment_result.setText(this.activity.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                }else {
                    comment_result.setVisibility(GONE);
                }

            }else {
                like_and_comment_information.setVisibility(GONE);
            }

        }
    }

    public void setFeedCVListener(FeedCVListener feedCVListener) {
        this.feedCVListener = feedCVListener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private FeedCVListener feedCVListener;
    public boolean isShowHeader,isShowIconSave,isShowLikeAndCommentInfo,isShowComment,isShowLike,isReponse,isShowMore = true;

    private void InitUI(Context context){
        inflate(context, R.layout.feed_cv,this);
        this.context = context;
        user_image = (ImageView)findViewById(R.id.user_image);
        body = (RelativeLayout)findViewById(R.id.body);
        body_content = (RelativeLayout)findViewById(R.id.body_content);
        show_more = (RelativeLayout)findViewById(R.id.show_more);
        like_and_comment_information = (LinearLayout) findViewById(R.id.like_and_comment_information);
        footer = (RelativeLayout)findViewById(R.id.footer);
        description = (RelativeLayout)findViewById(R.id.description);
        icon_show_more = (ImageView)findViewById(R.id.icon_show_more);
        like_icon = (ImageView)findViewById(R.id.like_icon);
        like_button = (LinearLayout)findViewById(R.id.like_button);
        comment_button = (LinearLayout)findViewById(R.id.comment_button);
        download_button = (LinearLayout)findViewById(R.id.download_button);

        name = (TextView)findViewById(R.id.name);
        title = (TextView)findViewById(R.id.title);
        tel = (TextView)findViewById(R.id.tel);
        email = (TextView)findViewById(R.id.email);
        location = (TextView)findViewById(R.id.location);
        description_text = (TextView)findViewById(R.id.description_text);
        text_show_more = (TextView)findViewById(R.id.text_show_more);
        like_result = (TextView)findViewById(R.id.like_result);
        comment_result = (TextView)findViewById(R.id.comment_result);
        like_text = (TextView)findViewById(R.id.like_text);

        description.setVisibility(GONE);

        like_result.setOnClickListener(this);
        comment_result.setOnClickListener(this);
        user_image.setOnClickListener(this);
        show_more.setOnClickListener(this);
        body_content.setOnClickListener(this);
        like_button.setOnClickListener(this);
        comment_button.setOnClickListener(this);
        download_button.setOnClickListener(this);
    }

    public FeedCV(Context context) {
        super(context);
        InitUI(context);
    }

    public FeedCV(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public FeedCV(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public FeedCV(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view==user_image){
            feedCVListener.viewProfile(this,feed);
        }else if (view==show_more){
            feedCVListener.onClickedMore(this,feed,isShowMore);
        }else if (view==body_content){
            feedCVListener.viewDetail(this,feed);
        }else if (view==like_button){
            feedCVListener.onLike(this,feed);
        }else if (view==comment_button) {
            feedCVListener.onComment(this, feed);
        }else if (view==like_result) {
            feedCVListener.showAllUserLiked(this, feed);
        }else if (view==comment_result){
            feedCVListener.onComment(this,feed);
        }else if (view==download_button){
            feedCVListener.onDownload(this,feed);
        }
    }
}

package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;

public class FeedCVHolder extends RecyclerView.ViewHolder {

    private FeedCV feedCV;

    public FeedCVHolder(View itemView) {
        super(itemView);
        feedCV = (FeedCV)itemView;
    }

    public void BindView(Activity activity, Feed feed,FeedCVListener feedCVListener){
        feedCV.setActivity(activity);
        feedCV.setFeed(feed);
        feedCV.setFeedCVListener(feedCVListener);
    }
}

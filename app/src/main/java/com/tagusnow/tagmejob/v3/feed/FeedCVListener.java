package com.tagusnow.tagmejob.v3.feed;

import com.tagusnow.tagmejob.feed.Feed;

public interface FeedCVListener {
    void onLike(FeedCV view, Feed feed);
    void onComment(FeedCV view, Feed feed);
    void onDownload(FeedCV view, Feed feed);
    void viewDetail(FeedCV view, Feed feed);
    void viewProfile(FeedCV view, Feed feed);
    void showAllUserLiked(FeedCV view, Feed feed);
    void onClickedMore(FeedCV view,Feed feed,boolean isShow);
}

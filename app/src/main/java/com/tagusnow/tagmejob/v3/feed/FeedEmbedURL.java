package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.feed.embed.URLLinkView;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeedEmbedURL extends TagUsJobRelativeLayout implements View.OnClickListener {

    public void setFeedNormalListener(FeedNormalListener<FeedEmbedURL> feedNormalListener) {
        this.feedNormalListener = feedNormalListener;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        InitFeed(feed);
    }

    @Override
    public void setActivity(Activity activity) {
        embedView.setActivity(activity);
        embedView.setOnClickListener(this);
        super.setActivity(activity);
    }

    private FeedNormalListener<FeedEmbedURL> feedNormalListener;
    private Feed feed;
    private Context context;

    private CircleImageView mProfilePicture;
    private TextView mFeedStoryTitle,mFeedStoryTime,like_result,comment_result,like_text;
    private ImageView like_icon,icon_saved;
    private LinearLayout feed_text_box,like_button,comment_button,share_button,like_and_comment_information;
    private RelativeLayout profileBar;
    private Button mButtonMoreFeed;
    private TextView mFeedTitle;

    ////////Embed URL////////////
    private URLLinkView embedView;

    private void InitUI(Context context){
        inflate(context, R.layout.feed_embed_url,this);
        this.context = context;

        mProfilePicture = (CircleImageView) findViewById(R.id.mProfilePicture);
        mFeedStoryTitle = (TextView) findViewById(R.id.mFeedStoryTitle);
        mFeedStoryTime = (TextView) findViewById(R.id.mFeedStoryTime);
        mButtonMoreFeed = (Button) findViewById(R.id.mButtonMoreFeed);
        mFeedTitle = (TextView) findViewById(R.id.mFeedTitle);
        feed_text_box = (LinearLayout) findViewById(R.id.feed_text_box);
        profileBar = (RelativeLayout) findViewById(R.id.profileBar);
        icon_saved = (ImageView) findViewById(R.id.icon_saved);

        like_and_comment_information = (LinearLayout) findViewById(R.id.like_and_comment_information);
        like_result = (TextView) findViewById(R.id.like_result);
        like_text = (TextView)findViewById(R.id.like_text);
        like_icon = (ImageView)findViewById(R.id.like_icon);
        like_button = (LinearLayout)findViewById(R.id.like_button);
        comment_result = (TextView)findViewById(R.id.comment_result);
        comment_button = (LinearLayout) findViewById(R.id.comment_button);
        share_button = (LinearLayout)findViewById(R.id.share_button);

        like_result.setOnClickListener(this);
        comment_result.setOnClickListener(this);
        like_button.setOnClickListener(this);
        comment_button.setOnClickListener(this);
        share_button.setOnClickListener(this);

        mButtonMoreFeed.setOnClickListener(this);
        profileBar.setOnClickListener(this);
        mFeedStoryTitle.setOnClickListener(this);
        mProfilePicture.setOnClickListener(this);
        mFeedTitle.setOnClickListener(this);

        ////////// Embed URL ///////////
        embedView = (URLLinkView)findViewById(R.id.embedView);
    }

    private void InitFeed(Feed feed){

        setName(mFeedStoryTitle,feed.getUser_post());
        setUserProfile(mProfilePicture,feed.getUser_post());
        if (feed.getIs_save() == 0) {
            icon_saved.setVisibility(View.GONE);
        } else {
            icon_saved.setVisibility(View.VISIBLE);
        }

        mFeedStoryTime.setText(feed.getFeed().getTimeline());
        if (feed.getFeed().getOriginalHastTagText() == null || feed.getFeed().getOriginalHastTagText().equals("")) {
            feed_text_box.setVisibility(View.GONE);
        } else {
            feed_text_box.setVisibility(View.VISIBLE);

            String description = feed.getFeed().getOriginalHastTagText();

            if (description.length() > 200){
                description = description.substring(0,197) + "...";
            }
            mFeedTitle.setText(description);
        }

        if (feed.getFeed().getType_link() != null && !feed.getFeed().getType_link().equals("")){
            embedView.setVisibility(VISIBLE);
            embedView.setFeed(feed);
        }else {
            embedView.setVisibility(GONE);
        }

        if (feed.getFeed().isIs_like()) {
            like_icon.setImageResource(R.drawable.ic_action_like_blue);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                like_text.setTextColor(context.getColor(R.color.colorAccent));
            }

        } else {
            like_icon.setImageResource(R.drawable.ic_action_like_black);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                like_text.setTextColor(context.getColor(R.color.colorIcon));
            }
        }

        if (feed.getFeed().getCount_likes() > 0) {
            like_result.setVisibility(View.VISIBLE);
            like_result.setText(feed.getFeed().getLike_result());
        } else {
            like_result.setVisibility(View.GONE);
        }

        if (feed.getFeed().getCount_comments() > 0) {
            comment_result.setVisibility(View.VISIBLE);
            comment_result.setText(context.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
        } else {
            comment_result.setVisibility(View.GONE);
        }

        if (feed.getFeed().getCount_likes()==0 && feed.getFeed().getCount_comments()==0){
            like_and_comment_information.setVisibility(GONE);
        }else {
            like_and_comment_information.setVisibility(VISIBLE);
        }
    }

    public FeedEmbedURL(Context context) {
        super(context);
        InitUI(context);
    }

    public FeedEmbedURL(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public FeedEmbedURL(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public FeedEmbedURL(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view==mProfilePicture){
            feedNormalListener.viewProfile(this,feed);
        }else if (view==mFeedStoryTitle){
            feedNormalListener.viewProfile(this,feed);
        }else if (view==share_button){
            feedNormalListener.onShare(this,feed);
        }else if (view==profileBar){
            feedNormalListener.showDetail(this,feed);
        }else if (view==mButtonMoreFeed){
            feedNormalListener.onMore(this,feed);
        }else if (view==like_button){
            feedNormalListener.onLike(this,feed);
        }else if (view==comment_button){
            feedNormalListener.onComment(this,feed);
        }else if (view==mFeedTitle){
            feedNormalListener.showDetail(this,feed);
        }else if (view == comment_result){
            feedNormalListener.onComment(this,feed);
        }else if (view == like_result){
            feedNormalListener.showAllUserLiked(this,feed);
        }else if (view == embedView){
            feedNormalListener.openWebsite(this,feed);
        }
    }
}

package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;

public class FeedEmbedURLHolder extends RecyclerView.ViewHolder {

    private FeedEmbedURL feedEmbedURL;

    public FeedEmbedURLHolder(View itemView) {
        super(itemView);
        feedEmbedURL = (FeedEmbedURL)itemView;
    }

    public void BindView(Activity activity, Feed feed, FeedNormalListener feedNormalListener){
        feedEmbedURL.setActivity(activity);
        feedEmbedURL.setFeed(feed);
        feedEmbedURL.setFeedNormalListener(feedNormalListener);
    }
}

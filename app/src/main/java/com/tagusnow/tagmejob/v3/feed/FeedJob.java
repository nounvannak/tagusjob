package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import java.util.Random;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeedJob extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    /*Profile Information*/
    RelativeLayout profileBar;
    CircleImageView mProfilePicture;
    TextView mFeedStoryTitle,mFeedStoryTime;
    ImageView icon_saved;
    Button mButtonMoreFeed;
    /*Header*/
    private RelativeLayout header;
    private ImageView background,logo;
    private TextView txtTitle,txtCloseDate,txtHiring,txtQualification,txtWorkingTime,txtSalary,txtPosition;
    private Button btnShowDetail;
    /*Body*/
    private LinearLayout like_and_comment_information;
    private TextView like_result,comment_result;
    /*Footer*/
    private RelativeLayout footer;
    private LinearLayout like_button,comment_button,share_button;
    private ImageView like_icon;
    private TextView like_text;
    /*Object*/
    private Feed feed;

    public void setFeed(Feed feed) {
        this.feed = feed;
        InitFeed(feed);
    }

    public void setFeedJobListener(FeedJobListener feedJobListener) {
        this.feedJobListener = feedJobListener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private FeedJobListener feedJobListener;
    private boolean isShowHeader,isShowIconSave,isShowLikeAndCommentInfo,isShowComment,isShowLike,isReponse = true;

    private void InitUI(Context context){
        inflate(context, R.layout.feed_job,this);
        this.context = context;
        txtTitle = (TextView)findViewById(R.id.txtTitle);
        txtCloseDate = (TextView)findViewById(R.id.txtCloseDate);
        txtHiring = (TextView)findViewById(R.id.txtHiring);
        txtPosition = (TextView)findViewById(R.id.txtPosition);
        txtQualification = (TextView)findViewById(R.id.txtQualification);
        txtSalary = (TextView)findViewById(R.id.txtSalary);
        txtWorkingTime = (TextView)findViewById(R.id.txtWorkingTime);

        btnShowDetail = (Button) findViewById(R.id.btnShowDetails);
        btnShowDetail.setOnClickListener(this);

        /*Profile Information*/
        profileBar = (RelativeLayout)findViewById(R.id.profileBar);
        mProfilePicture = (CircleImageView)findViewById(R.id.mProfilePicture);
        mFeedStoryTitle = (TextView)findViewById(R.id.mFeedStoryTitle);
        mFeedStoryTime = (TextView)findViewById(R.id.mFeedStoryTime);
        icon_saved = (ImageView)findViewById(R.id.icon_saved);
        mButtonMoreFeed = (Button)findViewById(R.id.mButtonMoreFeed);
        mProfilePicture.setOnClickListener(this);
        mButtonMoreFeed.setOnClickListener(this);
        profileBar.setOnClickListener(this);
        /*Header*/
        header = (RelativeLayout)findViewById(R.id.header);
        background = (ImageView)findViewById(R.id.background);
        logo = (ImageView)findViewById(R.id.logo);
        logo.setOnClickListener(this);

        like_and_comment_information = (LinearLayout)findViewById(R.id.like_and_comment_information);

        like_result = (TextView)findViewById(R.id.like_result);
        comment_result = (TextView)findViewById(R.id.comment_result);
        /*Footer*/
        footer = (RelativeLayout)findViewById(R.id.footer);
        like_button = (LinearLayout)findViewById(R.id.like_button);
        like_result.setOnClickListener(this);
        comment_result.setOnClickListener(this);
        comment_button = (LinearLayout)findViewById(R.id.comment_button);
        share_button = (LinearLayout)findViewById(R.id.share_button);
        like_icon = (ImageView)findViewById(R.id.like_icon);
        like_text = (TextView)findViewById(R.id.like_text);
        like_button.setOnClickListener(this);
        comment_button.setOnClickListener(this);
        share_button.setOnClickListener(this);
    }

    public FeedJob(Context context) {
        super(context);
        InitUI(context);
    }

    public FeedJob(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public FeedJob(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public FeedJob(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    private void InitFeed(Feed feed){
        if (feed!=null){
            setUserProfile(mProfilePicture,feed.getUser_post());
            setName(mFeedStoryTitle,feed.getUser_post());
            mFeedStoryTime.setText(feed.getFeed().getTimeline());
            if (feed.getIs_save()==1){
                icon_saved.setVisibility(VISIBLE);
            }else {
                icon_saved.setVisibility(GONE);
            }
            setAlbumPhoto(logo,(feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0) ? feed.getFeed().getAlbum().get(0) : null,true);
            int random = new Random().nextInt(3);
            Glide.with(this.context)
                    .load(SuperUtil.getBaseUrl(SuperConstance.RANDOM_BACKGROUND[random]))
                    .centerCrop()
                    .into(background);
            txtTitle.setText(feed.getFeed().getTitle());
            txtCloseDate.setText(feed.getFeed().getCloseDate());
            txtPosition.setText(feed.getFeed().getCategory());

            if (feed.getFeed().getNum_employee()!=null && !feed.getFeed().getNum_employee().equals("")) {
                txtHiring.setText(feed.getFeed().getNum_employee());
            }else{
                txtHiring.setText("N/A");
            }

            if (feed.getFeed().getLevel()!=null && !feed.getFeed().getLevel().equals("")){
                txtQualification.setText(feed.getFeed().getLevel());
            }else {
                txtQualification.setText("N/A");
            }

            if (feed.getFeed().getWorking_time()!=null && !feed.getFeed().getWorking_time().equals("")){
                txtWorkingTime.setText(feed.getFeed().getWorking_time());
            }else {
                txtWorkingTime.setText("N/A");
            }

            if (feed.getFeed().getSalary()!=null && !feed.getFeed().getSalary().equals("")){
                txtSalary.setText(feed.getFeed().getSalary());
            }else {
                txtSalary.setText("N/A");
            }


            if (feed.getFeed().getCount_comments()==0 && feed.getFeed().getCount_likes()==0){
                isShowLikeAndCommentInfo = false;
            }else {
                isShowLikeAndCommentInfo = true;
            }

            if (isShowLikeAndCommentInfo){
                like_and_comment_information.setVisibility(VISIBLE);

                if (feed.getFeed().getCount_likes()==0){
                    isShowLike = false;
                }else {
                    isShowLike = true;
                }

                if (isShowLike){
                    like_result.setVisibility(VISIBLE);
                    like_result.setText(feed.getFeed().getLike_result());
                }else {
                    like_result.setVisibility(GONE);
                }

                if (feed.getFeed().getCount_comments()==0){
                    isShowComment = false;
                }else {
                    isShowComment = true;
                }

                if (feed.getFeed().isIs_like()){
                    like_icon.setImageResource(R.drawable.ic_action_like_blue);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        like_text.setTextColor(this.context.getColor(R.color.colorPrimary));
                    }
                }else {
                    like_icon.setImageResource(R.drawable.ic_action_like_black);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        like_text.setTextColor(this.context.getColor(R.color.colorIcon));
                    }
                }

                if (isShowComment){
                    comment_result.setVisibility(VISIBLE);
                    comment_result.setText(this.activity.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                }else {
                    comment_result.setVisibility(GONE);
                }

            }else {
                like_and_comment_information.setVisibility(GONE);
            }

        }
    }

    @Override
    public void onClick(View view) {
        if (view==like_button){
            feedJobListener.onLike(this,feed);
        }else if (view==comment_button){
            feedJobListener.onComment(this,feed);
        }else if (view==share_button){
            feedJobListener.onShare(this,feed);
        }else if (view==logo){
            feedJobListener.onDetail(this,feed);
        }else if (view==profileBar){
            feedJobListener.onDetail(this,feed);
        }else if (view==mProfilePicture){
            feedJobListener.showProfile(this,feed);
        }else if (view==mButtonMoreFeed){
            feedJobListener.onMore(this,feed);
        }else if (view==btnShowDetail){
            feedJobListener.onDetail(this,feed);
        }else if (view==like_result){
            feedJobListener.showAllUserLiked(this,feed);
        }else if (view==comment_result){
            feedJobListener.onComment(this,feed);
        }
    }
}

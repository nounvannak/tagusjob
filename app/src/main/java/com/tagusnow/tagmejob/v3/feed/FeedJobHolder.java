package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;

public class FeedJobHolder extends RecyclerView.ViewHolder {

    private FeedJob feedJob;

    public FeedJobHolder(View itemView) {
        super(itemView);
        feedJob = (FeedJob)itemView;
    }

    public void BindView(Activity activity, Feed feed,FeedJobListener feedJobListener){
        feedJob.setActivity(activity);
        feedJob.setFeed(feed);
        feedJob.setFeedJobListener(feedJobListener);
    }
}

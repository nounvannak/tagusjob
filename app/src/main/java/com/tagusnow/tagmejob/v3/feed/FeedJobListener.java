package com.tagusnow.tagmejob.v3.feed;

import com.tagusnow.tagmejob.feed.Feed;

public interface FeedJobListener {
    void onLike(FeedJob view, Feed feed);
    void onComment(FeedJob view, Feed feed);
    void onShare(FeedJob view, Feed feed);
    void onMore(FeedJob view, Feed feed);
    void onDetail(FeedJob view, Feed feed);
    void onApplyNow(FeedJob view, Feed feed);
    void showProfile(FeedJob view, Feed feed);
    void showAllUserLiked(FeedJob view, Feed feed);
}

package com.tagusnow.tagmejob.v3.feed;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;

public interface FeedListener {
    void saveFeed(SmUser user, Feed feed);
    void editFeed(SmUser user, Feed feed);
    void hideFeed(SmUser user, Feed feed);
    void removeFeed(SmUser user, Feed feed);
}

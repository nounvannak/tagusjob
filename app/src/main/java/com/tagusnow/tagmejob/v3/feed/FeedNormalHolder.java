package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;

public class FeedNormalHolder extends RecyclerView.ViewHolder {

    private FeedNormal feedNormal;

    public FeedNormalHolder(View itemView) {
        super(itemView);
        feedNormal = (FeedNormal)itemView;
    }

    public void BindView(Activity activity, Feed feed, FeedNormalListener feedNormalListener, GridViewListener gridViewListener){
        feedNormal.setActivity(activity);
        feedNormal.setFeedNormalListener(feedNormalListener);
        feedNormal.setGridViewListener(gridViewListener);
        feedNormal.setFeed(feed);
    }
}

package com.tagusnow.tagmejob.v3.feed;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface FeedNormalListener<V extends TagUsJobRelativeLayout> {
    void onLike(V view, Feed feed);
    void onComment(V view, Feed feed);
    void onShare(V view, Feed feed);
    void onMore(V view, Feed feed);
    void showDetail(V view, Feed feed);
    void viewProfile(V view, Feed feed);
    void showAllUserLiked(V view, Feed feed);
    void openWebsite(V view, Feed feed);
}

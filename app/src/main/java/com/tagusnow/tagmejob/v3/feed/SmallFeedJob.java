package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class SmallFeedJob extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private Feed feed;

    public void setFeed(Feed feed) {
        this.feed = feed;
        if (feed!=null){
            setAlbumPhoto(image,(feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0) ? feed.getFeed().getAlbum().get(0) : null,true);
            title.setText(feed.getFeed().getTitle());
            salary.setText(feed.getFeed().getSalary());
            close_date.setText(feed.getFeed().getCloseDate());
            location.setText(feed.getFeed().getCity_name());
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    public void setSmallFeedJobListener(SmallFeedJobListener smallFeedJobListener) {
        this.smallFeedJobListener = smallFeedJobListener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private SmallFeedJobListener smallFeedJobListener;
    private ImageView image;
    private ImageButton btnMore;
    private TextView title,salary,close_date,location;

    private void InitUI(Context context){
        inflate(context, R.layout.small_feed_job,this);
        this.context = context;
        image = (ImageView)findViewById(R.id.image);
        title = (TextView)findViewById(R.id.title);
        salary = (TextView)findViewById(R.id.salary);
        close_date = (TextView)findViewById(R.id.close_date);
        btnMore = (ImageButton)findViewById(R.id.btnMore);
        location = (TextView)findViewById(R.id.location);
        btnMore.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public SmallFeedJob(Context context) {
        super(context);
        InitUI(context);
    }

    public SmallFeedJob(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public SmallFeedJob(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public SmallFeedJob(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            smallFeedJobListener.viewDetail(this,feed);
        }else if (view==btnMore){
            smallFeedJobListener.onMore(this,feed);
        }
    }
}

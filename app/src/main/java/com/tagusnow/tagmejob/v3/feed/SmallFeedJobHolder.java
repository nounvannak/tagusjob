package com.tagusnow.tagmejob.v3.feed;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;

public class SmallFeedJobHolder extends RecyclerView.ViewHolder {

    private SmallFeedJob smallFeedJob;

    public SmallFeedJobHolder(View itemView) {
        super(itemView);
        smallFeedJob = (SmallFeedJob)itemView;
    }

    public void BindView(Activity activity, Feed feed,SmallFeedJobListener smallFeedJobListener){
        smallFeedJob.setActivity(activity);
        smallFeedJob.setFeed(feed);
        smallFeedJob.setSmallFeedJobListener(smallFeedJobListener);
    }
}

package com.tagusnow.tagmejob.v3.feed;

import com.tagusnow.tagmejob.feed.Feed;

public interface SmallFeedJobListener {
    void onMore(SmallFeedJob view, Feed feed);
    void viewDetail(SmallFeedJob view, Feed feed);
}

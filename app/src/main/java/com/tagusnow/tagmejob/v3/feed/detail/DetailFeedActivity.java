package com.tagusnow.tagmejob.v3.feed.detail;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.v3.comment.CommentBox;
import com.tagusnow.tagmejob.v3.comment.CommentBoxListener;
import com.tagusnow.tagmejob.v3.comment.CommentCell;
import com.tagusnow.tagmejob.v3.comment.CommentListener;
import com.tagusnow.tagmejob.v3.comment.EditCommentCell;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.v3.like.LikeResultCell;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.v3.post.photo.CameraView;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelect;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectAdapter;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectListener;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import okhttp3.MultipartBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFeedActivity extends TagUsJobActivity implements CommentBoxListener<CommentBox>, CommentListener<CommentCell>, CommentListener.OnOptionSelected<CommentCell>, FeedListener, GridViewListener, ImageSelectListener, LikeResultListener, CommentListener.OnUpdateListener<EditCommentCell>, LoadingListener<LoadingView> {

    private static final String TAG = DetailFeedActivity.class.getSimpleName();
    public static final String FEED = "DetailFeedActivity_feed";
    private static final String ChannelID = UUID.randomUUID().toString();
    private SmUser Auth;
    private Feed feed;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private CommentBox commentBox;
    private EditCommentCell editCommentCell;
    private CommentResponse commentResponse;
    private List<Comment> comments;
    private DetailFeedAdapter adapter;
    private ImageSelectAdapter imageSelectAdapter;
    private String commentText,editCommentText;
    private TextView typing;
    private List<String> images = new ArrayList<>(),updateImages = new ArrayList<>();
    private Comment TempComment,TempUpdateComment;
    private AlertDialog builder;
    private boolean isUpdate = false;
    private LoadingView loadingView;
    private CommentService service = ServiceGenerator.createService(CommentService.class);
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);

    /* Socket.io */
    Socket socket;
    private Emitter.Listener TYPING = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int userID = obj.getInt("user_id");
                            String userName = obj.getString("user_name");
                            if (feedID == feed.getId() && userID != Auth.getId()){
                                typing.setVisibility(View.VISIBLE);
                                typing.setText((userName + " is typing..."));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener STOP_TYPING = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int userID = obj.getInt("user_id");
                            if (feedID == feed.getId() && userID != Auth.getId()){
                                typing.setVisibility(View.GONE);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener NEW_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Stuff that updates the UI
                    Comment comment = new Gson().fromJson(obj.toString(),Comment.class);
                    if (Auth.getId() != comment.getUser_id()){
                        if (comment.getDes() != null && !comment.getDes().equals("")){
                            comment.setDes(SuperUtil.DecodeBase64(comment.getDes()));
                        }
                        comments.add(comment);
                        adapter.notifyDataSetChanged();
                        recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                    }else{
                        if (comment.getChannel_id() != null && comment.getChannel_id().equals(ChannelID)){
                            TempComment.setId(comment.getId());
                            adapter.notifyDataSetChanged();
                        }else if (comment.getChannel_id() != null){
                            if (comment.getDes() != null && !comment.getDes().equals("")){
                                comment.setDes(SuperUtil.DecodeBase64(comment.getDes()));
                            }
                            comments.add(comment);
                            adapter.notifyDataSetChanged();
                            recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                        }
                    }
                }
            });
        }
    };
    private Emitter.Listener UPDATE_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            Comment commentJSON = new Gson().fromJson(obj.toString(),Comment.class);
                            if (commentJSON != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Comment> lists = comments.stream().filter(t -> t.getId() == commentJSON.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Comment comment = lists.get(0);


                                        if (Auth.getId() != comment.getUser_id()){
                                            if (comment.getDes() != null && !comment.getDes().equals("")){
                                                comment.setDes(SuperUtil.DecodeBase64(comment.getDes()));
                                            }
                                            int index = comments.indexOf(comment);
                                            comments.set(index,comment);
                                            adapter.notifyDataSetChanged();
                                        }else{
                                            if (comment.getChannel_id() != null){
                                                if (comment.getDes() != null && !comment.getDes().equals("")){
                                                    comment.setDes(SuperUtil.DecodeBase64(comment.getDes()));
                                                }
                                                int index = comments.indexOf(comment);
                                                comments.set(index,comment);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }

                                    }
                                }else {
                                    for (Comment comment: comments){
                                        if (comment.getId() == commentJSON.getId()){
                                            if (Auth.getId() != comment.getUser_id()){
                                                if (comment.getDes() != null && !comment.getDes().equals("")){
                                                    comment.setDes(SuperUtil.DecodeBase64(comment.getDes()));
                                                }
                                                int index = comments.indexOf(comment);
                                                comments.set(index,comment);
                                                adapter.notifyDataSetChanged();
                                            }else{
                                                if (comment.getChannel_id() != null){
                                                    if (comment.getDes() != null && !comment.getDes().equals("")){
                                                        comment.setDes(SuperUtil.DecodeBase64(comment.getDes()));
                                                    }
                                                    int index = comments.indexOf(comment);
                                                    comments.set(index,comment);
                                                    adapter.notifyDataSetChanged();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener REMOVE_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            Comment commentJSON = new Gson().fromJson(obj.toString(),Comment.class);
                            if (commentJSON != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Comment> lists = comments.stream().filter(t -> t.getId() == commentJSON.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Comment comment = lists.get(0);
                                        comments.remove(comment);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Comment comment: comments){
                                        if (comment.getId() == commentJSON.getId()){
                                            comments.remove(comment);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalLikes = obj.getInt("count_likes");
                            int userID = obj.getInt("user_id");
                            String likesResult = obj.getString("like_result");
                            if (feedID == feed.getId()){
                                FeedDetail feedDetail = feed.getFeed();
                                feedDetail.setCount_likes(totalLikes);
                                feedDetail.setLike_result(likesResult);
                                feedDetail.setIs_like(Auth.getId() == userID);
                                feed.setFeed(feedDetail);
                                adapter.notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }
            });

        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalComments = obj.getInt("count_comments");
                            if (feedID == feed.getId()){
                                FeedDetail feedDetail = feed.getFeed();
                                feedDetail.setCount_comments(totalComments);
                                feed.setFeed(feedDetail);
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (feedJson.getId() == feed.getId()){
                                    feed.setFeed(feedJson.getFeed());
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (feedJson.getId() == feed.getId()){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailFeedActivity.this);
                                    builder.setTitle("Notice!").setMessage("Sorry! content no longer.").setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            onBackPressed();
                                        }
                                    }).create().show();
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (obj != null){
                        try {
                            int userID = obj.getInt("user_id");
                            if (Auth.getId() == userID){
                                Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                                if (feedJson != null){
                                    if (feedJson.getId() == feed.getId()){
                                        AlertDialog.Builder builder = new AlertDialog.Builder(DetailFeedActivity.this);
                                        builder.setTitle("Notice!").setMessage("Sorry! content no longer.").setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                onBackPressed();
                                            }
                                        }).create().show();
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                }
            });

        }
    };
    private Callback<CommentResponse> FetchCommentCallback = new Callback<CommentResponse>() {
        @Override
        public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
            if (response.isSuccessful()){
                fetchComment(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CommentResponse> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<CommentResponse> FetchNextCommentCallback = new Callback<CommentResponse>() {
        @Override
        public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
            if (response.isSuccessful()){
                fetchNextComment(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CommentResponse> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<Comment> SaveCallback = new Callback<Comment>() {
        @Override
        public void onResponse(Call<Comment> call, Response<Comment> response) {
            Log.e(TAG,response.message());
        }

        @Override
        public void onFailure(Call<Comment> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Comment> UpdateCallback = new Callback<Comment>() {
        @Override
        public void onResponse(Call<Comment> call, Response<Comment> response) {
            if (response.isSuccessful()){
                Log.d(TAG,response.message());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Comment> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Comment> RemoveCallback = new Callback<Comment>() {
        @Override
        public void onResponse(Call<Comment> call, Response<Comment> response) {
            if (response.isSuccessful()){
                Log.d(TAG,response.message());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Comment> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void InitSocket(){
        socket = new App().getSocket();
        socket.on("Typing",TYPING);
        socket.on("Stop Typing",STOP_TYPING);
        socket.on("New Comment " + feed.getId(),NEW_COMMENT);
        socket.on("Update Comment " + feed.getId(),UPDATE_COMMENT);
        socket.on("Remove Comment " + feed.getId(),REMOVE_COMMENT);
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.connect();
    }
    /* End Socket.io */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,DetailFeedActivity.class);
    }

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();
        feed = new Gson().fromJson(getIntent().getStringExtra(FEED),Feed.class);
    }

    private void fetchComment(){
        service.getList(feed.getId(),10).enqueue(FetchCommentCallback);
    }

    private void fetchComment(CommentResponse commentResponse){
        this.commentResponse = commentResponse;
        this.comments = commentResponse.getData();
        CheckNext();
        adapter.NewData(comments);
        recyclerView.setAdapter(adapter);
        loadingView.setLoading(commentResponse.getTotal() > commentResponse.getTo());
    }

    private void fetchNextComment(){
        boolean isNext = commentResponse.getCurrent_page() != commentResponse.getLast_page();
        if (isNext){
            int page = commentResponse.getCurrent_page() + 1;
            service.getList(feed.getId(),10,page).enqueue(FetchNextCommentCallback);
        }
    }

    private void fetchNextComment(CommentResponse commentResponse){
        this.commentResponse = commentResponse;
        this.comments.addAll(commentResponse.getData());
        CheckNext();
        loadingView.setLoading(commentResponse.getTotal() > commentResponse.getTo());
        adapter.NextData(commentResponse.getData());
    }

    private void OnTyping(String commentText){
        try {
            JSONObject com = new JSONObject();
            com.put("feed_id",feed.getId());
            com.put("user_name",getName(Auth,NameType.FIRST));
            com.put("user_id",Auth.getId());
            com.put("ChannelID",ChannelID);
            socket.emit("Typing",com);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (commentResponse.getTo() - 1)) && (commentResponse.getTo() < commentResponse.getTotal())) {
                        if (commentResponse.getNext_page_url()!=null || !commentResponse.getNext_page_url().equals("")){
                            if (lastPage!=commentResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = commentResponse.getCurrent_page();
                                    fetchNextComment();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void OpenImagePicker(){
        AndroidImagePicker.getInstance().pickSingle(this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                InitImage(items);
            }
        });
    }

    private void InitImage(List<ImageItem> items){
        if(items != null && items.size() > 0){
            for (ImageItem item : items){
                Log.e(TAG,Uri.parse(item.path).getPath());
                if (isUpdate){
                    updateImages.add(item.path);
                }else {
                    images.add(item.path);
                }
            }

            if (!isUpdate){

                commentBox.setHasFile(true);
                imageSelectAdapter = new ImageSelectAdapter(this,false,images,this);
                commentBox.setAdapter(imageSelectAdapter);
                commentBox.viewCondition();
            }else {
                editCommentCell.setHasFile(true);
                imageSelectAdapter = new ImageSelectAdapter(this,false,updateImages,this);
                editCommentCell.setAdapter(imageSelectAdapter);
                editCommentCell.viewCondition();
                TempUpdateComment.setImage(updateImages.get(0));
                editCommentCell.enabledUpdate(true);
            }
        }
    }

    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            AlertDialog.Builder builder = new AlertDialog.Builder(DetailFeedActivity.this);
            builder.setTitle("Permission Notice").setMessage("Sorry you can't use camera & gallery. Please allow permission first.").setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onBackPressed();
                }
            }).create().show();
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_feed);

        InitTemp();
        InitSocket();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.view_post_detail_title,getName(feed.getUser_post(),NameType.FIRST)));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        typing = (TextView)findViewById(R.id.typing);
        typing.setVisibility(View.GONE);

        commentBox = (CommentBox)findViewById(R.id.comment_box);
        commentBox.setActivity(this);
        commentBox.setHasFile(false);
        commentBox.viewCondition();
        commentBox.setBoxListener(this);

        adapter = new DetailFeedAdapter(this,feed,Auth,this,this,this,this,this);
        loadingView = adapter.getLoadingView();

        fetchComment();
    }

    @Override
    public void openCamera(CommentBox view) {
        OpenImagePicker();
    }

    @Override
    public void onSend(CommentBox view) {
        Save();
//        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
//        builder.setTitle("Save Comment").setMessage("Are you sure to comment this feed?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Save();
//            }
//        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        }).create().show();
    }

    private void Save(){

        TempComment = new Comment();
        List<MultipartBody.Part> request = new ArrayList<>();

        request.add(okhttp3.MultipartBody.Part.createFormData("auth_user",String.valueOf(Auth.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("feed_id",String.valueOf(feed.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("channel_id",ChannelID));

        TempComment.setId(0);
        TempComment.setUser_id(Auth.getId());
        TempComment.setComment_user(Auth);
        TempComment.setFeed_id(feed.getId());
        TempComment.setStatus(1);
        TempComment.setTrash(0);
        TempComment.setC_date(GetDate(new Date(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss));

        if (commentText != null && !commentText.equals("")){
            request.add(okhttp3.MultipartBody.Part.createFormData("des",commentText));
            TempComment.setDes(commentText);
        }

        if (images != null && images.size() > 0){
            Uri uri = Uri.parse(images.get(0));
            File file = new File(Objects.requireNonNull(uri.getPath()));
            request.add(prepareFilePart("image",file));
            TempComment.setImage(uri.getPath());
        }
        adapter.Insert(TempComment);
        recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
        commentBox.isReset();
        images.clear();
        service.save(request).enqueue(SaveCallback);
    }

    private void Update(Comment comment){
        List<MultipartBody.Part> request = new ArrayList<>();

        request.add(okhttp3.MultipartBody.Part.createFormData("auth_user",String.valueOf(Auth.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("feed_id",String.valueOf(feed.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("comment_id",String.valueOf(comment.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("channel_id",ChannelID));

        if (editCommentText != null && !editCommentText.equals("")){
            request.add(okhttp3.MultipartBody.Part.createFormData("comment",editCommentText));
            TempUpdateComment.setDes(editCommentText);
        }

        if (updateImages != null && updateImages.size() > 0 && !updateImages.get(0).contains("http")){
            Uri uri = Uri.parse(updateImages.get(0));
            File file = new File(Objects.requireNonNull(uri.getPath()));
            request.add(prepareFilePart("image",file));
            TempUpdateComment.setImage(updateImages.get(0));
        }

        int index = comments.indexOf(comment);
        Log.e(TAG,String.valueOf(index));
        Log.e(TAG,new Gson().toJson(comment));
        Log.e(TAG,new Gson().toJson(TempUpdateComment));
        comments.set(index,TempUpdateComment);
        adapter.notifyDataSetChanged();
        editCommentCell.isReset();
        updateImages.clear();
        builder.dismiss();
        service.update(request).enqueue(UpdateCallback);
        if (!TempUpdateComment.equals(comment)){

        }
    }

    @Override
    public void onTextChanged(CommentBox view, String comment) {
        commentText = comment;
        OnTyping(comment);
    }

    @Override
    public void onOptionSelected(CommentCell view,Comment comment, OnOptionSelected<CommentCell> onOptionSelected) {
        view.OnOptionComment(comment,onOptionSelected);
    }

    @Override
    public void select(CommentCell view, Comment comment) {

    }

    @Override
    public void profile(CommentCell view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void displayImage(CommentCell view, Comment comment) {
        ArrayList<String> images = new ArrayList<>();
        if (comment.getImage().contains("/storage/emulated/0/")){
            images.add(comment.getImage());
        }else {
            images.add(SuperUtil.getCommentPicture(comment.getImage(),"original"));
        }
        DisplayImage(images,0);
    }

    @Override
    public void copy(CommentCell view, Comment comment) {
        view.CopyText(comment.getDes());
    }

    @Override
    public void edit(CommentCell view, Comment comment) {
        isUpdate = true;
        TempUpdateComment = comment;
        editCommentCell = new EditCommentCell(this);

        editCommentCell.setActivity(this);
        editCommentCell.setOnUpdateListener(this);
        editCommentCell.setComment(comment);

        if (comment.getImage() != null && !comment.getImage().equals("")){
            updateImages.add(SuperUtil.getCommentPicture(comment.getImage(),"original"));
            imageSelectAdapter = new ImageSelectAdapter(this,false,updateImages,this);
            editCommentCell.setAdapter(imageSelectAdapter);

        }
        editCommentCell.setHasFile((comment.getImage() != null && !comment.getImage().equals("")));
        editCommentCell.viewCondition();
        editCommentCell.enabledUpdate(true);
        builder = new AlertDialog.Builder(this).setView(editCommentCell).create();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        builder.getWindow().setLayout((int) (displayWidth * 1f),(int) (displayHeight * 1f));
        builder.setCanceledOnTouchOutside(false);
        builder.show();
    }

    @Override
    public void delete(CommentCell view, Comment comment) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Remove Comment").setMessage("Are you sure to remove this comment?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                service.delete(Auth.getId(),feed.getId(),comment.getId()).enqueue(RemoveCallback);
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void onLike(TagUsJobRelativeLayout view, Feed feed) {
        LikeFeed(Auth,feed);
    }


    private void LikeFeed(SmUser Auth, Feed feed){
        if (!feed.getFeed().isIs_like()){
            FeedDetail temp = feed.getFeed();
            temp.setIs_like(true);
            temp.setCount_likes(1);
            temp.setLike_result("You liked this post.");
            feed.setFeed(temp);
            adapter.notifyDataSetChanged();
            feedService.LikeFeed(feed.getId(),Auth.getId()).enqueue(new Callback<SmTagFeed.Like>() {
                @Override
                public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                    if (response.isSuccessful()){
                        Log.e(TAG,response.message());
                    }else {
                        try {
                            Log.e(TAG,response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onComment(TagUsJobRelativeLayout view, Feed feed) {
        commentBox.setFocusable(true);
    }

    @Override
    public void onDownload(TagUsJobRelativeLayout view, Feed feed) {
        view.DisplayDownloadBox(feed);
    }

    @Override
    public void viewDetail(TagUsJobRelativeLayout view, Feed feed) {
        if (view.getClass() == DetailFeedJob.class){
            view.DetailFeedJob(Auth,feed);
        }else if (view.getClass() == DetailFeedCV.class){
            view.DetailFeedCV(feed);
        }else {
            if (((DetailFeedNormal) view).isShowMore()){
                ((DetailFeedNormal) view).setShowMore(false);
            }else {
                ((DetailFeedNormal) view).setShowMore(true);
            }
//            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void viewProfile(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(TagUsJobRelativeLayout view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void onClickedMore(TagUsJobRelativeLayout view, Feed feed, boolean isShow) {

    }

    @Override
    public void onShare(TagUsJobRelativeLayout view, Feed feed) {
        view.ShareFeed(feed);
    }

    @Override
    public void onMore(TagUsJobRelativeLayout view, Feed feed) {

    }

    @Override
    public void onApplyNow(TagUsJobRelativeLayout view, Feed feed) {
        view.ApplyJob(feed);
    }

    @Override
    public void openWebsite(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenWebsite(feed.getFeed().getSource_url());
    }

    @Override
    public void showGallery(List<Album> albums, int index) {
        ArrayList<String> images = new ArrayList<>();
        for (Album album: albums){
            images.add(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"original"));
        }
        DisplayImage(images,index);
    }

    @Override
    public void remove(ImageSelect view, int position) {
        if (isUpdate){
            Comment comment = TempUpdateComment;
            updateImages.remove(position);
            imageSelectAdapter.remove(updateImages);
            editCommentCell.setHasFile(updateImages.size() > 0);
            editCommentCell.viewCondition();
            TempUpdateComment.setImage("");
            editCommentCell.enabledUpdate(!TempUpdateComment.equals(comment));
        }else {
            images.remove(position);
            imageSelectAdapter.remove(images);
            commentBox.setHasFile(images.size() > 0);
            commentBox.viewCondition();
        }
    }

    @Override
    public void camera(CameraView view) {

    }

    @Override
    public void select(LikeResultCell cell, SmUser user) {
        cell.OpenProfile(Auth,user);
    }

    @Override
    public void connect(LikeResultCell cell, SmUser user) {
        cell.DoFollow(Auth,user);
    }

    @Override
    public void camera(EditCommentCell view, Comment comment) {
        OpenImagePicker();
    }

    @Override
    public void cancel(EditCommentCell view) {
        isUpdate = false;
        editCommentCell.isReset();
        updateImages.clear();
        builder.dismiss();
    }

    @Override
    public void update(EditCommentCell view, Comment comment) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Update Comment").setMessage("Are you sure to update this comment?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Update(comment);
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void onUpdateTextChanged(EditCommentCell view,Comment comment, String text) {
        editCommentText = text;
        TempUpdateComment.setDes(text);
    }

    @Override
    public void reload(LoadingView view) {
        fetchNextComment();
    }
}

package com.tagusnow.tagmejob.v3.feed.detail;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.v3.comment.CommentCell;
import com.tagusnow.tagmejob.v3.comment.CommentCellHolder;
import com.tagusnow.tagmejob.v3.comment.CommentListener;
import com.tagusnow.tagmejob.v3.empty.EmptyHolder;
import com.tagusnow.tagmejob.v3.empty.EmptyView;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;

import java.util.List;

public class DetailFeedAdapter extends RecyclerView.Adapter {

    private static final int NORMAL = 0;
    private static final int EMBED = 7;
    private static final int JOB = 1;
    private static final int RESUME = 2;
    private static final int COMMENT = 3;
    private static final int LOADING = 4;
    private static final int EMPTY = 5;
    private static final int TYPING = 6;
    private Activity activity;
    private Feed feed;
    private SmUser user;
    private List<Comment> comments;
    private FeedListener feedListener;
    private GridViewListener gridViewListener;
    private CommentListener<CommentCell> commentListener;
    private CommentListener.OnOptionSelected<CommentCell> onOptionSelected;
    private int other = 2;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public DetailFeedAdapter(Activity activity, Feed feed, SmUser user, FeedListener feedListener, GridViewListener gridViewListener, CommentListener<CommentCell> commentListener, CommentListener.OnOptionSelected<CommentCell> onOptionSelected,LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.feed = feed;
        this.user = user;
        this.feedListener = feedListener;
        this.gridViewListener = gridViewListener;
        this.commentListener = commentListener;
        this.onOptionSelected = onOptionSelected;
        this.loadingListener = loadingListener;
        this.loadingView = new LoadingView(activity);
    }

    public void NextData(List<Comment> comments){
        this.comments.addAll(comments);
        notifyDataSetChanged();
    }

    public void NewData(List<Comment> comments){
        this.comments = comments;
    }

    public void Insert(Comment comment){
        this.comments.add(comment);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (comments.size() > 0){
            if (position == 0) {
                if (feed.getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)){
                    if (feed.getFeed().getType_link() != null && !feed.getFeed().getType_link().equals("")){
                        return EMBED;
                    }else {
                        return NORMAL;
                    }
                }else if (feed.getFeed().getFeed_type().equals(FeedDetail.JOB_POST)){
                    return JOB;
                }else {
                    return RESUME;
                }
            }else if (position == (getItemCount() - 1)){
                return LOADING;
            }else{
                return COMMENT;
            }
        }else {
            if (position == 0){
                if (feed.getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)){
                    if (feed.getFeed().getType_link() != null && !feed.getFeed().getType_link().equals("")){
                        return EMBED;
                    }else {
                        return NORMAL;
                    }
                }else if (feed.getFeed().getFeed_type().equals(FeedDetail.JOB_POST)){
                    return JOB;
                }else {
                    return RESUME;
                }
            }else {
                 return EMPTY;
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == NORMAL){
            return new DetailFeedNormalHolder(new DetailFeedNormal(activity));
        }else if (viewType == EMBED){
            return new DetailFeedEmbedURLHolder(new DetailFeedEmbedURL(activity));
        }else if (viewType == JOB){
            return new DetailFeedJobHolder(new DetailFeedJob(activity));
        }else if (viewType == RESUME){
            return new DetailFeedCVHolder(new DetailFeedCV(activity));
        }else if (viewType == COMMENT){
            return new CommentCellHolder(new CommentCell(activity));
        }else if (viewType == LOADING) {
            return new LoadingHolder(loadingView);
        }else {
            return new EmptyHolder(new EmptyView(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == NORMAL){
            ((DetailFeedNormalHolder) holder).BindView(activity,feed,feedListener,gridViewListener);
        }else if (getItemViewType(position) == EMBED){
            ((DetailFeedEmbedURLHolder) holder).BindView(activity,feed,feedListener);
        }else if (getItemViewType(position) == JOB){
            ((DetailFeedJobHolder) holder).BindView(activity,feed,feedListener);
        }else if (getItemViewType(position) == RESUME){
            ((DetailFeedCVHolder) holder).BindView(activity,feed,feedListener);
        }else if (getItemViewType(position) == COMMENT){
            ((CommentCellHolder) holder).BindView(activity,user,comments.get(position - (other - 1)),commentListener,onOptionSelected);
        }else if (getItemViewType(position) == LOADING) {
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else {
            ((EmptyHolder) holder).BindView(activity);
        }
    }

    @Override
    public int getItemCount() {
        return comments.size() > 0 ? (comments.size() + other) : other;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

package com.tagusnow.tagmejob.v3.feed.detail;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;

public class DetailFeedCVHolder extends RecyclerView.ViewHolder {

    private DetailFeedCV detailFeedCV;

    public DetailFeedCVHolder(View itemView) {
        super(itemView);
        detailFeedCV = (DetailFeedCV)itemView;
    }

    public void BindView(Activity activity, Feed feed,FeedListener<DetailFeedCV> feedListener){
        detailFeedCV.setActivity(activity);
        detailFeedCV.setFeed(feed);
        detailFeedCV.setFeedListener(feedListener);
    }
}

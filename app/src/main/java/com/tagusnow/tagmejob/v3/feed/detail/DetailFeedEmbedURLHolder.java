package com.tagusnow.tagmejob.v3.feed.detail;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;

public class DetailFeedEmbedURLHolder extends RecyclerView.ViewHolder {

    private DetailFeedEmbedURL detailFeedEmbedURL;

    public DetailFeedEmbedURLHolder(View itemView) {
        super(itemView);
        detailFeedEmbedURL = (DetailFeedEmbedURL)itemView;
    }

    public void BindView(Activity activity, Feed feed, FeedListener feedNormalListener){
        detailFeedEmbedURL.setActivity(activity);
        detailFeedEmbedURL.setFeed(feed);
        detailFeedEmbedURL.setFeedListener(feedNormalListener);
    }
}

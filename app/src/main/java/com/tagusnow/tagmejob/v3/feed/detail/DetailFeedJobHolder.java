package com.tagusnow.tagmejob.v3.feed.detail;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;

public class DetailFeedJobHolder extends RecyclerView.ViewHolder {

    private DetailFeedJob detailFeedJob;

    public DetailFeedJobHolder(View itemView) {
        super(itemView);
        detailFeedJob = (DetailFeedJob)itemView;
    }

    public void BindView(Activity activity, Feed feed,FeedListener<DetailFeedJob> feedListener){
        detailFeedJob.setActivity(activity);
        detailFeedJob.setFeed(feed);
        detailFeedJob.setFeedListener(feedListener);
    }
}

package com.tagusnow.tagmejob.v3.feed.detail;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;

public class DetailFeedNormalHolder extends RecyclerView.ViewHolder {

    private DetailFeedNormal detailFeedNormal;

    public DetailFeedNormalHolder(View itemView) {
        super(itemView);
        detailFeedNormal = (DetailFeedNormal)itemView;
    }

    public void BindView(Activity activity, Feed feed, FeedListener<DetailFeedNormal> feedListener, GridViewListener gridViewListener){
        detailFeedNormal.setActivity(activity);
        detailFeedNormal.setFeedListener(feedListener);
        detailFeedNormal.setGridViewListener(gridViewListener);
        detailFeedNormal.setFeed(feed);
    }
}

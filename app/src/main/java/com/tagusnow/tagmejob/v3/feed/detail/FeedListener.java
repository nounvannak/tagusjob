package com.tagusnow.tagmejob.v3.feed.detail;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface FeedListener<V extends TagUsJobRelativeLayout> {
    void onLike(V view, Feed feed);
    void onComment(V view, Feed feed);
    void onDownload(V view, Feed feed);
    void viewDetail(V view, Feed feed);
    void viewProfile(V view, Feed feed);
    void showAllUserLiked(V view, Feed feed);
    void onClickedMore(V view,Feed feed,boolean isShow);
    void onShare(V view, Feed feed);
    void onMore(V view, Feed feed);
    void onApplyNow(V view, Feed feed);
    void openWebsite(V view, Feed feed);
}

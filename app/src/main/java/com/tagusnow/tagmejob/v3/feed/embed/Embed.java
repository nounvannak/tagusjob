package com.tagusnow.tagmejob.v3.feed.embed;

import java.io.Serializable;

public class Embed implements Serializable {

    private String thumbnail;
    private String title;
    private String website;
    private String description;
}

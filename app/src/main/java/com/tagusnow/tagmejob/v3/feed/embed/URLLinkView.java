package com.tagusnow.tagmejob.v3.feed.embed;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class URLLinkView extends TagUsJobRelativeLayout {

    private Context context;
    private ImageView thumbnail;
    private TextView website,title,description;
    private Feed feed;

    public LinkSourceContent getLinkSourceContent() {
        return linkSourceContent;
    }

    private LinkSourceContent linkSourceContent;

    private void InitUI(Context context){
        inflate(context, R.layout.url_link_view,this);
        this.context = context;
        thumbnail = (ImageView)findViewById(R.id.thumbnail);
        website = (TextView)findViewById(R.id.website);
        title = (TextView)findViewById(R.id.title);
        description = (TextView)findViewById(R.id.description);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setLinkSourceContent(LinkSourceContent linkSourceContent) {
        this.linkSourceContent = linkSourceContent;
        String image = (linkSourceContent.getImages() != null && linkSourceContent.getImages().size() > 0) ? linkSourceContent.getImages().get(0) : null;
        Glide.with(context).load(image).centerCrop().error(R.drawable.not_available_).into(thumbnail);
        title.setText(linkSourceContent.getTitle());
        website.setText(linkSourceContent.getCannonicalUrl());
        description.setText(linkSourceContent.getDescription());

    }

    public URLLinkView(Context context) {
        super(context);
        InitUI(context);
    }

    public URLLinkView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public URLLinkView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public URLLinkView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        String image = (feed.getFeed().getSource_thumbnail() != null && !feed.getFeed().getSource_thumbnail().equals("")) ? feed.getFeed().getSource_thumbnail() : null;
        Glide.with(context).load(image).centerCrop().error(R.drawable.not_available_).into(thumbnail);
        title.setText(feed.getFeed().getSource_title());
        website.setText(feed.getFeed().getSource_host());
        description.setText(feed.getFeed().getSource_text());
    }
}

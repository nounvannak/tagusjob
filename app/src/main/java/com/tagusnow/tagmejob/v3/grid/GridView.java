package com.tagusnow.tagmejob.v3.grid;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.v3.grid.view.GridFourFour;
import com.tagusnow.tagmejob.v3.grid.view.GridFourOne;
import com.tagusnow.tagmejob.v3.grid.view.GridFourThree;
import com.tagusnow.tagmejob.v3.grid.view.GridFourTwo;
import com.tagusnow.tagmejob.v3.grid.view.GridThreeFour;
import com.tagusnow.tagmejob.v3.grid.view.GridThreeOne;
import com.tagusnow.tagmejob.v3.grid.view.GridThreeThree;
import com.tagusnow.tagmejob.v3.grid.view.GridThreeTwo;
import com.tagusnow.tagmejob.v3.grid.view.GridTwoOne;
import com.tagusnow.tagmejob.v3.grid.view.GridTwoTwo;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.util.List;
import java.util.Random;

public class GridView extends TagUsJobRelativeLayout {

    private static final String TAG = GridView.class.getSimpleName();
    private Context context;
    private List<Album> albums;
    private GridViewListener gridViewListener;
    private View subView;

    public int getSubViewWidth() {
        return subViewWidth;
    }

    public int getSubViewHeight() {
        return subViewHeight;
    }

    private int subViewWidth,subViewHeight;

    void InitUI(Context context){
        this.context = context;
        this.subView = new View(context);
    }

    public GridView(Context context) {
        super(context);
        InitUI(context);
    }

    public GridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public GridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public GridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
        if (albums.size() > 0){
            subViewWidth = ViewGroup.LayoutParams.MATCH_PARENT;
            subViewHeight = ViewGroup.LayoutParams.MATCH_PARENT;
            if (albums.size() == 1){
                ImageView view = new ImageView(context);
                setAlbumPhoto(view,albums.get(0),false);
                subViewWidth = ViewGroup.LayoutParams.MATCH_PARENT;
                subViewHeight = ViewGroup.LayoutParams.MATCH_PARENT;
                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        gridViewListener.showGallery(albums,0);
                    }
                });
                subView = view;
            }else if (albums.size() == 2){
                int random = new Random().nextInt(2);
                if (random == 0){
                    GridTwoOne view = new GridTwoOne(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }else {
                    GridTwoTwo view = new GridTwoTwo(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }
            }else if (albums.size() == 3){
                int random = new Random().nextInt(4);
                if (random == 0){
                    GridThreeOne view = new GridThreeOne(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }else if (random == 1){
                    GridThreeTwo view = new GridThreeTwo(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }else if (random == 2){
                    GridThreeThree view = new GridThreeThree(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }else {
                    GridThreeFour view = new GridThreeFour(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }
            }else if (albums.size() == 4){
                int random = new Random().nextInt(3);
                if (random == 0) {
                    GridFourOne view = new GridFourOne(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }else if (random == 1) {
                    GridFourTwo view = new GridFourTwo(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }else{
                    GridFourThree view = new GridFourThree(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }
            }else{
                int random = new Random().nextInt(2);
                // more than 4
                if (random == 0){
                    // 4x3
                    GridFourThree view = new GridFourThree(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }else {
                    // 4x4
                    GridFourFour view = new GridFourFour(context);
                    view.setActivity(activity);
                    view.setGridViewListener(gridViewListener);
                    view.setAlbums(albums);
                    subView = view;
                }
            }
        }
    }

    public void setGridViewListener(GridViewListener gridViewListener) {
        this.gridViewListener = gridViewListener;
    }

    public View getSubView() {
        return subView;
    }
}

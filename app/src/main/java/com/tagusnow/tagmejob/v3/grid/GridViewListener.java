package com.tagusnow.tagmejob.v3.grid;

import com.tagusnow.tagmejob.feed.Album;
import java.util.List;

public interface GridViewListener {
    void showGallery(List<Album> albums, int index);
}

package com.tagusnow.tagmejob.v3.grid.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import java.util.List;

public class GridThreeOne extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ImageView image1,image2,image3;
    private List<Album> albums;
    private GridViewListener gridViewListener;

    private void initUI(Context context){
        inflate(context, R.layout.grid_three_one,this);
        this.context = context;
        image1 = (ImageView)findViewById(R.id.image1);
        image2 = (ImageView)findViewById(R.id.image2);
        image3 = (ImageView)findViewById(R.id.image3);

        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public GridThreeOne(Context context) {
        super(context);
        this.initUI(context);
    }

    public GridThreeOne(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public GridThreeOne(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public GridThreeOne(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
        setAlbumPhoto(image1,albums.get(0));
        setAlbumPhoto(image2,albums.get(1));
        setAlbumPhoto(image3,albums.get(2));
    }

    @Override
    public void onClick(View view) {
        if (view==image1){
            gridViewListener.showGallery(albums,0);
        }else if (view==image2){
            gridViewListener.showGallery(albums,1);
        }else if (view==image3){
            gridViewListener.showGallery(albums,2);
        }
    }

    public void setGridViewListener(GridViewListener gridViewListener) {
        this.gridViewListener = gridViewListener;
    }
}

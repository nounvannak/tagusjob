package com.tagusnow.tagmejob.v3.image;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boycy815.pinchimageview.PinchImageView;
import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ImageViewActivity extends TagUsJobActivity implements View.OnClickListener {

    private static final String TAG = ImageViewActivity.class.getSimpleName();
    public static final String INDEX = "ImageViewActivity_index";
    public static final String IMAGES = "ImageViewActivity_images";
    private RelativeLayout layoutTool;
    private ImageButton btnClose,btnShare;
    private TextView title;
    private ViewPager pager;
    private int index = 0;
    private boolean isHeader = false;
    private List<String> images = new ArrayList<>();
    private static final long ANIM_TIME = 200;
    private String myURL = "";
    private RectF mThumbMaskRect;
    private Matrix mThumbImageMatrix;
    private ObjectAnimator mBackgroundAnimator;
    private View mBackground;
    private PinchImageView mImageView;

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ImageViewActivity.class);
    }

    private void InitTemp(){
        index = getIntent().getIntExtra(INDEX,0);
        images = getIntent().getStringArrayListExtra(IMAGES);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view2);

        InitTemp();

        final LinkedList<PinchImageView> viewCache = new LinkedList<PinchImageView>();
        final DisplayImageOptions thumbOptions = new DisplayImageOptions.Builder().resetViewBeforeLoading(true).cacheInMemory(true).build();
        final DisplayImageOptions originOptions = new DisplayImageOptions.Builder().build();

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.black));
        }

        layoutTool = (RelativeLayout)findViewById(R.id.layoutToolbar);
        btnClose = (ImageButton)findViewById(R.id.btnClose);
        btnShare = (ImageButton)findViewById(R.id.btnShare);
        title = (TextView)findViewById(R.id.title);
        pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return images.size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                PinchImageView piv;
                if (viewCache.size() > 0) {
                    piv = viewCache.remove();
                    piv.reset();
                } else {
                    piv = new PinchImageView(ImageViewActivity.this);
                }
                String image = images.get(position);
                setTag((position + 1));
                myURL = image;
                Glide.with(ImageViewActivity.this).load(image).error(R.drawable.not_available).fitCenter().into(piv);
                piv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isHeader){
                            hide();
                        }else {
                            show();
                        }
                    }
                });
                container.addView(piv);
                return piv;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                PinchImageView piv = (PinchImageView) object;
                container.removeView(piv);
                viewCache.add(piv);
            }

            @Override
            public void setPrimaryItem(ViewGroup container, int position, Object object) {
                PinchImageView piv = (PinchImageView) object;
                String image = images.get(position);
                setTag((position + 1));
                myURL = image;
                Glide.with(ImageViewActivity.this).load(image).error(R.drawable.not_available).fitCenter().into(piv);
            }

        });
        pager.setCurrentItem(index);
        btnShare.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        pager.setOnClickListener(this);
    }

    private void setTag(int position){
        title.setText(getString(R.string._1_of_2,position,images.size()));
    }

    private void hide(){
        isHeader = false;
        layoutTool.setVisibility(View.GONE);
    }

    private void show(){
        isHeader = true;
        layoutTool.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        if (v == btnClose){
            finish();
        }else if (v == btnShare){
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT,myURL);
            startActivity(Intent.createChooser(intent, "Share"));
        }else if (pager == v){
            if (isHeader){
                hide();
            }else {
                show();
            }
        }
    }

//    @Override
//    public void finish() {
//        if ((mBackgroundAnimator != null && mBackgroundAnimator.isRunning())) {
//            return;
//        }
//
//        //背景动画
//        mBackgroundAnimator = ObjectAnimator.ofFloat(mBackground, "alpha", mBackground.getAlpha(), 0f);
//        mBackgroundAnimator.setDuration(ANIM_TIME);
//        mBackgroundAnimator.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                ImageViewActivity.super.finish();
//                overridePendingTransition(0, 0);
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
//        mBackgroundAnimator.start();
//
//        //mask动画
//        mImageView.zoomMaskTo(mThumbMaskRect, ANIM_TIME);
//
//        //图片缩小动画
//        mImageView.outerMatrixTo(mThumbImageMatrix, ANIM_TIME);
//    }
}

package com.tagusnow.tagmejob.v3.like;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;

import java.util.List;

public class LikeResultAdapter extends RecyclerView.Adapter {
    private Activity activity;
    private Context context;
    private LikeResultListener listener;
    private List<SmUser> results;

    public LikeResultAdapter(Activity activity, LikeResultListener listener, List<SmUser> results) {
        this.activity = activity;
        this.context = activity;
        this.listener = listener;
        this.results = results;
    }

    public void NextData(List<SmUser> results){
        this.results.addAll(results);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LikeResultHolder(new LikeResultCell(context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((LikeResultHolder) holder).BindView(activity,results.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }
}

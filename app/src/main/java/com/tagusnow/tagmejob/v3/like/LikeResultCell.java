package com.tagusnow.tagmejob.v3.like;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import de.hdodenhof.circleimageview.CircleImageView;

public class LikeResultCell extends TagUsJobRelativeLayout implements View.OnClickListener {
    // like_result_cell.xml
    private Context context;
    private SmUser User;
    private LikeResultListener listener;
    private CircleImageView profile;
    private TextView name,counter;
    private ImageButton btnConnect;


    public void setUser(SmUser user) {
        User = user;
        setName(name,user);
        counter.setText(context.getString(R.string._0_peoples_connected,user.getFollower_counter()));
        setUserProfile(profile,user);

        if (user.isIs_follow()){
            // src = check_mark_grey
            // background = button_round
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnConnect.setBackground(context.getDrawable(R.drawable.button_round));
                btnConnect.setImageDrawable(context.getDrawable(R.drawable.check_mark_grey));
            }
        }else {
            // src = user_white
            // background = button_round_background_accent_color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnConnect.setBackground(context.getDrawable(R.drawable.button_round_backgroud_accent_color));
                btnConnect.setImageDrawable(context.getDrawable(R.drawable.user_white));
            }
        }

    }

    public void setListener(LikeResultListener listener) {
        this.listener = listener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    void InitUI(Context context){
        inflate(context, R.layout.like_result_cell,this);
        this.context = context;

        profile = (CircleImageView) findViewById(R.id.profile);
        name = (TextView)findViewById(R.id.name);
        counter = (TextView)findViewById(R.id.counter);
        btnConnect = (ImageButton)findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(this);
        setOnClickListener(this);
    }

    public LikeResultCell(Context context) {
        super(context);
        InitUI(context);
    }

    public LikeResultCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public LikeResultCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public LikeResultCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnConnect) {
            listener.connect(this,User);
        }else if (view == this){
            listener.select(this,User);
        }
    }
}

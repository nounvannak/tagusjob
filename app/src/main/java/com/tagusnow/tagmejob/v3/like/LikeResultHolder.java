package com.tagusnow.tagmejob.v3.like;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class LikeResultHolder extends RecyclerView.ViewHolder {

    private LikeResultCell likeResultCell;

    public LikeResultHolder(View itemView) {
        super(itemView);
        likeResultCell = (LikeResultCell)itemView;
    }

    public void BindView(Activity activity, SmUser user, LikeResultListener listener) {
        likeResultCell.setActivity(activity);
        likeResultCell.setUser(user);
        likeResultCell.setListener(listener);
    }
}

package com.tagusnow.tagmejob.v3.like;

import com.tagusnow.tagmejob.auth.SmUser;

public interface LikeResultListener {
    void select(LikeResultCell cell,SmUser user);
    void connect(LikeResultCell cell,SmUser user);
}

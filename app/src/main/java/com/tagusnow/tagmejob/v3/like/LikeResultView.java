package com.tagusnow.tagmejob.v3.like;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LikeResultView extends TagUsJobRelativeLayout implements SwipeRefreshLayout.OnRefreshListener {
    // like_result_view.xml
    private static final String TAG = LikeResultView.class.getSimpleName();
    private Context context;
    private SmUser Auth;
    private Feed feed;
    private LikeResultListener listener;
    private LikeResultAdapter adapter;
    private UserPaginate paginate;
    private AuthService service = ServiceGenerator.createService(AuthService.class);
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private TextView result_likes;

    private Callback<UserPaginate> FetchDataCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                fetchData(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> FetchNextDataCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                fetchNextData(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    void InitUI(Context context){

        inflate(context, R.layout.like_result_view,this);
        this.context = context;

        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        result_likes = (TextView)findViewById(R.id.result_likes);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        LinearLayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        refreshLayout.setOnRefreshListener(this);

    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        result_likes.setText(feed.getFeed().getLike_result());
    }

    public void setAuth(SmUser auth) {
        Auth = auth;
    }

    public void setListener(LikeResultListener listener) {
        this.listener = listener;
    }

    public void fetchData(){
        service.UserLikedList(feed.getId(),Auth.getId(),10).enqueue(FetchDataCallback);
    }

    private void fetchData(UserPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adapter = new LikeResultAdapter(activity,listener,paginate.getData());
        recyclerView.setAdapter(adapter);
    }

    private void fetchNextData(){
        int page = paginate.getCurrent_page() + 1;
        service.UserLikedList(feed.getId(),Auth.getId(),10,page).enqueue(FetchNextDataCallback);
    }

    private void fetchNextData(UserPaginate paginate){
        this.paginate = paginate;
        this.CheckNext();
        this.adapter.NextData(paginate.getData());
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public LikeResultView(Context context) {
        super(context);
        InitUI(context);
    }

    public LikeResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public LikeResultView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public LikeResultView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy < 0){
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (paginate.getTo() - 1)) && (paginate.getTo() < paginate.getTotal())) {
                            if (paginate.getNext_page_url()!=null || !paginate.getNext_page_url().equals("")){
                                if (lastPage!=paginate.getCurrent_page()){
                                    isRequest = true;
                                    if (isRequest){
                                        lastPage = paginate.getCurrent_page();
                                        fetchNextData();
                                    }
                                }else{
                                    isRequest = false;
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private Runnable RunCallback = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(false);
            fetchData();
        }
    };

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        new Handler().postDelayed(RunCallback, 2000);
    }
}

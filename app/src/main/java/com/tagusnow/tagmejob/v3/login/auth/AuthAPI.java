package com.tagusnow.tagmejob.v3.login.auth;

import com.tagusnow.tagmejob.auth.SmUser;
import java.util.List;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface AuthAPI {

    @Multipart
    @POST("api/v1/auth/signup")
    Call<AuthReponse> SignUp(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/auth/login")
    Call<AuthReponse> LogIn(@Part List<MultipartBody.Part> request);

    @POST("api/v1/auth/logout")
    Call<AuthReponse> LogOut(@Header("Authorization") String token);

    @POST("api/v1/auth/refresh")
    Call<AuthReponse> Refresh(@Header("Authorization") String token);

    @POST("api/v1/auth/me")
    Call<SmUser> Me(@Header("Authorization") String token);

    @Multipart
    @POST("api/v1/auth/confirm_code")
    Call<AuthReponse> ConfirmCode(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/auth/send_code")
    Call<AuthReponse> SendCode(@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/auth/resend_code")
    Call<AuthReponse> ResendCode(@Header("Authorization") String token,@Part List<MultipartBody.Part> request);

    @Multipart
    @POST("api/v1/auth/update_password")
    Call<AuthReponse> UpdatePassword(@Header("Authorization") String token,@Part List<MultipartBody.Part> request);

}

package com.tagusnow.tagmejob.v3.login.auth;

import retrofit2.Response;

public interface AuthCallback<U extends Object,R extends Object> {
    void onSuccess(U user);
    void onSent(R response);
    void onLogOut(Response response);
    void onWarning(Response response);
    void onError(Throwable throwable);
    void onAccessToken(String access_token,String token_type);
}

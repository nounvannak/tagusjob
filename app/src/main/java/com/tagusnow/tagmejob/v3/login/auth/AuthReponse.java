package com.tagusnow.tagmejob.v3.login.auth;

import java.io.Serializable;

public class AuthReponse implements Serializable {
    private int line;
    private String error;
    private String message;
    private String login_method;
    private String access_token;
    private String token_type;
    private long expires_in;

    public AuthReponse(int line, String error, String message, String login_method, String access_token, String token_type, long expires_in) {
        this.line = line;
        this.error = error;
        this.message = message;
        this.login_method = login_method;
        this.access_token = access_token;
        this.token_type = token_type;
        this.expires_in = expires_in;
    }

    public AuthReponse(){
        super();
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLogin_method() {
        return login_method;
    }

    public void setLogin_method(String login_method) {
        this.login_method = login_method;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
    }
}

package com.tagusnow.tagmejob.v3.login.auth;

import android.app.Activity;
import android.util.Log;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.gson.Gson;
import com.jaychang.slm.SocialLoginManager;
import com.jaychang.slm.SocialUser;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.GoogleResponse;
import com.tagusnow.tagmejob.auth.Helper;
import com.tagusnow.tagmejob.auth.SmUser;

import org.fuckboilerplate.rx_social_connect.RxSocialConnect;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import studios.codelight.smartloginlibrary.SmartLoginCallbacks;
import studios.codelight.smartloginlibrary.SmartLoginConfig;
import studios.codelight.smartloginlibrary.users.SmartUser;
import studios.codelight.smartloginlibrary.util.SmartLoginException;

public class BaseLogin extends BaseTagUsJobAuth implements SmartLoginCallbacks {
    private static final String PHONE = "phone";
    private static final String EMAIL = "email";
    private static final String FACEBOOK = "facebook";
    private static final String GOOGLE = "google";
    private static final String ANONYMOUS = "anonymous";
    protected AuthCallback<SmUser,AuthReponse> callback;
    protected Activity activity;
    private SmartLoginConfig config;
    protected Helper helper;
    private GoogleSignInClient mGoogleSignInClient;
    protected AuthAPI api = ServiceGenerator.createService(AuthAPI.class);
    private Callback<AuthReponse> LOG_IN_WITH_PHONE = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> LOG_IN_WITH_EMAIL = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> LOG_IN_WITH_FACEBOOK = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> LOG_IN_WITH_GOOGLE = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> LOG_IN_WITH_ANONYMOUS = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<SmUser> ME_REQUEST = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                callback.onSuccess(response.body());
            }else {
                Log.e("me w",response.message());
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            Log.e("me ",t.getMessage());
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> SIGN_UP_WITH_PHONE = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> SIGN_UP_WITH_EMAIL = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> SIGN_UP_WITH_FACEBOOK = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> SIGN_UP_WITH_GOOGLE = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> SIGN_UP_WITH_ANONYMOUS = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> RESEND_CODE = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                Log.e("Resend code","Code was resend.");
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<Object> LOGIN_GOOGLE = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.isSuccessful()){
                GoogleResponse user = new Gson().fromJson(new Gson().toJson(response.body()),GoogleResponse.class);
                onLoginGoogle(user);
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<Object> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<Object> REGISTER_GOOGLE = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.isSuccessful()){
                GoogleResponse user = new Gson().fromJson(new Gson().toJson(response.body()),GoogleResponse.class);
                onRegisterGoogle(user);
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<Object> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> REFRESH_TOKEN = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                Log.e("refresh token w",response.message());
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            Log.e("refresh token",t.getMessage());
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> CONFIRM_CODE = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> LOGOUT = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                callback.onLogOut(response);
                callback.onSuccess(null);
                callback.onAccessToken(null,null);
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> SEND_CODE = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                callback.onSent(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };
    private Callback<AuthReponse> UPDATE_PASSWORD = new Callback<AuthReponse>() {
        @Override
        public void onResponse(Call<AuthReponse> call, Response<AuthReponse> response) {
            if (response.isSuccessful()){
                me(response.body());
            }else {
                callback.onWarning(response);
            }
        }

        @Override
        public void onFailure(Call<AuthReponse> call, Throwable t) {
            callback.onError(t);
        }
    };

    private void onLoginPhone(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",PHONE));
        api.LogIn(request).enqueue(LOG_IN_WITH_PHONE);
    }

    private void onLoginEmail(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",EMAIL));
        api.LogIn(request).enqueue(LOG_IN_WITH_EMAIL);
    }

    public void onLoginFacebook(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",FACEBOOK));
        api.LogIn(request).enqueue(LOG_IN_WITH_FACEBOOK);
    }

    public void onLoginGoogle(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",GOOGLE));
        api.LogIn(request).enqueue(LOG_IN_WITH_GOOGLE);
    }

    private void onLoginAnonymous(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",ANONYMOUS));
        api.LogIn(request).enqueue(LOG_IN_WITH_ANONYMOUS);
    }

    private void onRegisterPhone(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",PHONE));
        api.SignUp(request).enqueue(SIGN_UP_WITH_PHONE);
    }

    private void onRegisterEmail(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",EMAIL));
        api.SignUp(request).enqueue(SIGN_UP_WITH_EMAIL);
    }

    private void onRegisterFacebook(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",FACEBOOK));
        api.SignUp(request).enqueue(SIGN_UP_WITH_FACEBOOK);
    }

    private void onRegisterGoogle(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",GOOGLE));
        api.SignUp(request).enqueue(SIGN_UP_WITH_GOOGLE);
    }

    private void onRegisterAnonymous(List<MultipartBody.Part> request){
        request.add(okhttp3.MultipartBody.Part.createFormData("auth_type",ANONYMOUS));
        api.SignUp(request).enqueue(SIGN_UP_WITH_ANONYMOUS);
    }

    protected void me(AuthReponse response){
        String token = response.getToken_type() + " " + response.getAccess_token();
        callback.onAccessToken(response.getAccess_token(),response.getToken_type());
        api.Me(token).enqueue(ME_REQUEST);
    }

    public BaseLogin(Activity activity){
        this.activity = activity;
        helper = new Helper(activity);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.google_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(activity,gso);
        config = new SmartLoginConfig(activity,this);
        config.setFacebookAppId(activity.getString(R.string.facebook_app_id));
        config.setFacebookPermissions(SmartLoginConfig.getDefaultFacebookPermissions());
        config.setGoogleApiClient(mGoogleSignInClient.asGoogleApiClient());
    }

    @Override
    public void Login(List<MultipartBody.Part> parts, AuthType authType) {
        if (authType == AuthType.PHONE){
            onLoginPhone(parts);
        }else if (authType == AuthType.EMAIL){
            onLoginEmail(parts);
        }else if (authType == AuthType.FACEBOOK){
            onLoginFacebook();
        }else if (authType == AuthType.GOOGLE){
            onLoginGoogle();
        }else {
            onLoginAnonymous(parts);
        }
    }

    public void addCallback(AuthCallback<SmUser, AuthReponse> callback) {
        this.callback = callback;
    }

    @Override
    public void SignUp(List<MultipartBody.Part> parts, AuthType authType) {
        if (authType == AuthType.PHONE){
            onRegisterPhone(parts);
        }else if (authType == AuthType.EMAIL){
            onRegisterEmail(parts);
        }else if (authType == AuthType.FACEBOOK){
            onRegisterFacebook();
        }else if (authType == AuthType.GOOGLE){
            onRegisterGoogle();
        }else {
            onRegisterAnonymous(parts);
        }
    }

    @Override
    public void LogOut(String token) {
        api.LogOut(token).enqueue(LOGOUT);
    }

    @Override
    public void Refresh(String token) {
        api.Refresh(token).enqueue(REFRESH_TOKEN);
    }

    @Override
    public void SendCode(List<MultipartBody.Part> parts, AuthType authType) {
        if (authType == AuthType.PHONE){
            parts.add(okhttp3.MultipartBody.Part.createFormData("auth_type",PHONE));
        }else if (authType == AuthType.EMAIL){
            parts.add(okhttp3.MultipartBody.Part.createFormData("auth_type",EMAIL));
        }

        api.SendCode(parts).enqueue(SEND_CODE);
    }

    @Override
    public void ConfirmCode(List<MultipartBody.Part> parts, AuthType authType) {
        if (authType == AuthType.PHONE){
            parts.add(okhttp3.MultipartBody.Part.createFormData("auth_type",PHONE));
        }else if (authType == AuthType.EMAIL){
            parts.add(okhttp3.MultipartBody.Part.createFormData("auth_type",EMAIL));
        }

        api.ConfirmCode(parts).enqueue(CONFIRM_CODE);
    }

    @Override
    public void ResendCode(String token, List<MultipartBody.Part> parts, AuthType authType) {

        if (authType == AuthType.PHONE){
            parts.add(okhttp3.MultipartBody.Part.createFormData("auth_type",PHONE));
        }else if (authType == AuthType.EMAIL){
            parts.add(okhttp3.MultipartBody.Part.createFormData("auth_type",EMAIL));
        }

        api.ResendCode(token,parts).enqueue(RESEND_CODE);
    }

    @Override
    public void UpdatePassword(String token, List<MultipartBody.Part> parts, AuthType authType) {
        if (authType == AuthType.PHONE){
            parts.add(okhttp3.MultipartBody.Part.createFormData("auth_type",PHONE));
        }else if (authType == AuthType.EMAIL){
            parts.add(okhttp3.MultipartBody.Part.createFormData("auth_type",EMAIL));
        }

        api.UpdatePassword(token,parts).enqueue(UPDATE_PASSWORD);
    }

    // Smart Login
    private void onLoginGoogle(){
        RxSocialConnect.with(activity,helper.googleService()).subscribe(response -> onLoginGoogle(response.token()),
                        error -> callback.onError(error));
    }

    private void onRegisterGoogle(){
        RxSocialConnect.with(activity,helper.googleService()).subscribe(response -> onRegisterGoogle(response.token()),
                error -> callback.onError(error));
    }

    private void onLoginGoogle(OAuth2AccessToken token) {
        AuthService service = ServiceGenerator.createServices(AuthService.class);
        service.AuthGoogle(token.getTokenType() + " " + token.getAccessToken(),"application/json").enqueue(LOGIN_GOOGLE);
    }

    private void onRegisterGoogle(OAuth2AccessToken token) {
        AuthService service = ServiceGenerator.createServices(AuthService.class);
        service.AuthGoogle(token.getTokenType() + " " + token.getAccessToken(),"application/json").enqueue(REGISTER_GOOGLE);
    }

    private void onLoginFacebook(){
        SocialLoginManager.getInstance(activity)
                .facebook()
                .login()
                .subscribe(this::onLoginFacebook,error -> callback.onError(error));
    }

    private void onLoginFacebook(SocialUser user){
        List<MultipartBody.Part> parts = new ArrayList<>();

        if (user.userId != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("profile_id",user.userId));
        }

        if (user.photoUrl != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("image",user.photoUrl));
        }

        if (user.profile.email != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("email",user.profile.email));
        }

        if (user.profile.name != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("user_name",user.profile.name));
        }

        onLoginFacebook(parts);
    }

    private void onRegisterFacebook(){
        SocialLoginManager.getInstance(activity)
                .facebook()
                .login()
                .subscribe(this::onRegisterFacebook,error -> callback.onError(error));
    }

    private void onRegisterFacebook(SocialUser user){
        List<MultipartBody.Part> parts = new ArrayList<>();

        if (user.userId != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("profile_id",user.userId));
        }

        if (user.photoUrl != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("image",user.photoUrl));
        }

        if (user.profile.email != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("email",user.profile.email));
        }

        if (user.profile.fullName != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("user_name",user.profile.fullName));
        }

        onRegisterFacebook(parts);
    }

    @Override
    public void onLoginSuccess(SmartUser user) {

    }

    @Override
    public void onLoginFailure(SmartLoginException e) {
        callback.onError(e);
    }

    @Override
    public SmartUser doCustomLogin() {
        return null;
    }

    @Override
    public SmartUser doCustomSignup() {
        return null;
    }

    private void onLoginGoogle(GoogleResponse googleResponse) {
        List<MultipartBody.Part> parts = new ArrayList<>();

        if (googleResponse.getName() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("user_name",googleResponse.getName()));
        }

        if (googleResponse.getGiven_name() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("firs_name",googleResponse.getGiven_name()));
        }

        if (googleResponse.getFamily_name() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("last_name",googleResponse.getFamily_name()));
        }

        if (googleResponse.getEmail() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("email",googleResponse.getEmail()));
        }

        if (googleResponse.getId() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("profile_id",googleResponse.getId()));
        }

        if (googleResponse.getPicture() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("image",googleResponse.getPicture()));
        }

        onLoginGoogle(parts);
    }

    private void onRegisterGoogle(GoogleResponse googleResponse) {
        List<MultipartBody.Part> parts = new ArrayList<>();

        if (googleResponse.getName() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("user_name",googleResponse.getName()));
        }

        if (googleResponse.getGiven_name() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("firs_name",googleResponse.getGiven_name()));
        }

        if (googleResponse.getFamily_name() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("last_name",googleResponse.getFamily_name()));
        }

        if (googleResponse.getEmail() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("email",googleResponse.getEmail()));
        }

        if (googleResponse.getId() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("profile_id",googleResponse.getId()));
        }

        if (googleResponse.getPicture() != null){
            parts.add(okhttp3.MultipartBody.Part.createFormData("image",googleResponse.getPicture()));
        }

        onRegisterGoogle(parts);
    }
}

package com.tagusnow.tagmejob.v3.login.auth;

import java.util.List;

import okhttp3.MultipartBody;

public abstract class BaseTagUsJobAuth {
    public abstract void Login(List<MultipartBody.Part> parts,AuthType authType);
    public abstract void SignUp(List<MultipartBody.Part> parts,AuthType authType);
    public abstract void LogOut(String token);
    public abstract void Refresh(String token);
    public abstract void SendCode(List<MultipartBody.Part> parts,AuthType authType);
    public abstract void ConfirmCode(List<MultipartBody.Part> parts,AuthType authType);
    public abstract void ResendCode(String token,List<MultipartBody.Part> parts,AuthType authType);
    public abstract void UpdatePassword(String token,List<MultipartBody.Part> parts,AuthType authType);
}

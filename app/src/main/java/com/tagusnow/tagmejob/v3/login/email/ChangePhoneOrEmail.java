package com.tagusnow.tagmejob.v3.login.email;

import android.app.Activity;
import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.v3.login.auth.AuthType;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class ChangePhoneOrEmail extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Button btnChange,btnClose;
    private ImageView iconOfInputPhone;
    private EditText InputPhone;
    private RelativeLayout layoutError;
    private TextView errorText;
    private ImageButton btnCloseErrorMsg;
    private String changeValue;
    private AuthType authType;
    private boolean isPhone = true;
    private ChangePhoneOrEmailListener listener;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void addListener(ChangePhoneOrEmailListener listener){
        this.listener = listener;
    }

    private void InitUI(Context context){
        inflate(context, R.layout.change_phone_or_email,this);
        btnChange = (Button)findViewById(R.id.btnChange);
        btnClose = (Button)findViewById(R.id.btnClose);
        btnCloseErrorMsg = (ImageButton)findViewById(R.id.btnCloseErrorMsg);
        iconOfInputPhone = (ImageView)findViewById(R.id.iconOfInputPhone);
        InputPhone = (EditText)findViewById(R.id.InputPhone);
        layoutError = (RelativeLayout)findViewById(R.id.layoutError);
        errorText = (TextView)findViewById(R.id.errorText);

        btnChange.setOnClickListener(this);
        btnCloseErrorMsg.setOnClickListener(this);
        btnClose.setOnClickListener(this);

        InputPhone.requestFocus();
        setError(false);
    }

    public ChangePhoneOrEmail(Context context) {
        super(context);
        InitUI(context);
    }

    public ChangePhoneOrEmail(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public ChangePhoneOrEmail(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public ChangePhoneOrEmail(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setError(boolean error) {
        if (error){
            layoutError.setVisibility(View.VISIBLE);
        }else {
            layoutError.setVisibility(View.GONE);
        }
    }

    public void setErrorMessage(String errorMsg){
        setError(true);
        errorText.setText(errorMsg);
    }

    public void setPhone(boolean phone) {
        isPhone = phone;
        if (phone){
            authType = AuthType.PHONE;
            iconOfInputPhone.setImageResource(R.drawable.phone_one);
            InputPhone.setHint(activity.getString(R.string.phone_number));
            InputPhone.setInputType(InputType.TYPE_CLASS_PHONE);
        }else {
            authType = AuthType.EMAIL;
            iconOfInputPhone.setImageResource(R.drawable.message_one);
            InputPhone.setHint(activity.getString(R.string.email));
            InputPhone.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnChange){
            changeValue = InputPhone.getText().toString();
            listener.onChanged(this,changeValue,authType);
        }else if (v == btnClose){
            listener.onClosed(this);
        }else if (v == btnCloseErrorMsg){
            setError(false);
        }
    }

    public void setChangeValue(String changeValue) {
        this.changeValue = changeValue;
        InputPhone.setText(changeValue);
    }
}

package com.tagusnow.tagmejob.v3.login.email;

import com.tagusnow.tagmejob.v3.login.auth.AuthType;

public interface ChangePhoneOrEmailListener {
    void onClosed(ChangePhoneOrEmail view);
    void onChanged(ChangePhoneOrEmail view, String changedValue, AuthType authType);
}

package com.tagusnow.tagmejob.v3.login.email;

import android.content.Context;
import android.util.AttributeSet;

import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class ResendCode extends TagUsJobRelativeLayout {
    public ResendCode(Context context) {
        super(context);
    }

    public ResendCode(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ResendCode(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ResendCode(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}

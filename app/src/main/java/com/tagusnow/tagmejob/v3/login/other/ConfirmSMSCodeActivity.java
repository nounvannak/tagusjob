package com.tagusnow.tagmejob.v3.login.other;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.raywenderlich.android.validatetor.ValidateTor;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.login.auth.AuthCallback;
import com.tagusnow.tagmejob.v3.login.auth.AuthReponse;
import com.tagusnow.tagmejob.v3.login.auth.AuthType;
import com.tagusnow.tagmejob.v3.login.auth.BaseLogin;
import com.tagusnow.tagmejob.v3.login.email.ChangePhoneOrEmail;
import com.tagusnow.tagmejob.v3.login.email.ChangePhoneOrEmailListener;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Response;

public class ConfirmSMSCodeActivity extends TagUsJobActivity implements AuthCallback<SmUser, AuthReponse>, View.OnClickListener, ChangePhoneOrEmailListener {

    private static final String TAG = ConfirmSMSCodeActivity.class.getSimpleName();
    public static final String IS_FORGET = "isForget";
    private BaseLogin baseLogin;
    private Button btnConfirm;
    private TextView textResendCode;
    private EditText digit1,digit2,digit3,digit4;
    private TextView txtPhoneAndEmail;
    private ImageButton btnChange;
    private String access_token,token_type;
    private String errorMsg = "";
    private ValidateTor validateTor;
    private String phoneOrEmail = "";
    private String tempUserName,tempPassword,tempLoginMethod;
    private boolean isPhone = true;
    private boolean isForget = false;
    private SmUser Auth;
    private AlertDialog builder;

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ConfirmSMSCodeActivity.class);
    }

//    private void InitTemp(){
//        this.access_token = SuperUtil.GetPreference(this,SuperUtil.ACCESS_TOKEN,null);
//        this.token_type = SuperUtil.GetPreference(this,SuperUtil.TOKEN_TYPE,null);
//        this.Auth = new Auth(this).checkAuth().token();
//        this.isPhone = this.Auth.getLogin_method().equals(SmUser.PHONE);
//        this.phoneOrEmail = this.isPhone ? this.Auth.getPhone() : this.Auth.getEmail();
//    }

    private void InitTemp(){
        this.access_token = SuperUtil.GetPreference(this,SuperUtil.ACCESS_TOKEN,null);
        this.token_type = SuperUtil.GetPreference(this,SuperUtil.TOKEN_TYPE,null);
        this.Auth = new Auth(this).checkAuth().token();
        this.isForget = getIntent().getBooleanExtra(IS_FORGET,false);
        this.tempLoginMethod = SuperUtil.GetPreference(this,SuperUtil.LOGIN_METHOD,null);
        this.tempPassword = SuperUtil.GetPreference(this,SuperUtil.PASSWORD,null);
        this.tempUserName = SuperUtil.GetPreference(this,SuperUtil.FULL_NAME,null);
        this.phoneOrEmail = SuperUtil.GetPreference(this,SuperUtil.LOGIN_VALUE,null);
        if (this.tempLoginMethod != null){
            this.isPhone = this.tempLoginMethod.equals(SmUser.PHONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_smscode);
        ChangeStatusBar(Color.WHITE);

        InitTemp();
        validateTor = new ValidateTor();
        baseLogin = new BaseLogin(this);
        baseLogin.addCallback(this);

        btnConfirm = (Button)findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(this);

        textResendCode = (TextView)findViewById(R.id.textResend);
        textResendCode.setOnClickListener(this);

        txtPhoneAndEmail = (TextView)findViewById(R.id.txtPhoneAndEmail);
        txtPhoneAndEmail.setText(phoneOrEmail);

        btnChange = (ImageButton)findViewById(R.id.btnChange);
        btnChange.setOnClickListener(this);

        digit1 = (EditText)findViewById(R.id.digit1);
        digit2 = (EditText)findViewById(R.id.digit2);
        digit3 = (EditText)findViewById(R.id.digit3);
        digit4 = (EditText)findViewById(R.id.digit4);

        digit1.addTextChangedListener(DIGIT_1);
        digit2.addTextChangedListener(DIGIT_2);
        digit3.addTextChangedListener(DIGIT_3);
        digit4.addTextChangedListener(DIGIT_4);
    }

    private TextWatcher DIGIT_1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(digit1.getText().toString().length() == 1){
                digit2.requestFocus();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private TextWatcher DIGIT_2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(digit2.getText().toString().length() == 1){
                digit3.requestFocus();
            }else {
                digit1.requestFocus();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private TextWatcher DIGIT_3 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(digit3.getText().toString().length() == 1){
                digit4.requestFocus();
            }else {
                digit2.requestFocus();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private TextWatcher DIGIT_4 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(digit4.getText().toString().length() == 0){
                digit3.requestFocus();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onSuccess(SmUser user) {
        dismissProgress();
        if (isForget){
            // Goto Update Password
            new Auth(this).save(user);
            GoToUpdatePassword(token_type,access_token);
        }else {
            new Auth(this).save(user);
            AccessApp(user);
        }
    }

    @Override
    public void onSent(AuthReponse response) {

    }

    @Override
    public void onLogOut(Response response) {

    }

    @Override
    public void onWarning(Response response) {
        dismissProgress();
        try {
//            String errorJsonString = response.errorBody().string();
            AuthReponse authReponse = new Gson().fromJson(response.errorBody().string(),AuthReponse.class);
            String message = authReponse != null ? authReponse.getError() : new Gson().toJson(authReponse);
            if (message.equals("User not found")){
                errorMsg = "Invalid Credentials.";
            }else if (message.equals("Password not matches")){
                errorMsg = "Wrong password.";
            }else if (message.equals("User was removed")){
                errorMsg = "User was removed, please contact to admin.";
            }else if (message.equals("No permission")){
                errorMsg = "No permission, please contact to admin.";
            }else if (message.equals("No auth type")){
                errorMsg = "No auth type.";
            }else if (message.equals("No access token")){
                errorMsg = "No access token.";
            }else if (message.equals("No user")){
                errorMsg = "No user.";
            }else if (message.equals("Sorry, this code already verified.")){
                errorMsg = "Sorry, this code already verified.";
            }else if (message.equals("Sorry, we can't confirm this code.")){
                errorMsg = "Sorry, we can't confirm this code.";
            }else if (message.equals("Sorry, we can't update your account.")){
                errorMsg = "Sorry, we can't update your account.";
            }else if (message.equals("No confirm code")){
                errorMsg = "No confirm code.";
            }else if (message.equals("Pin code not sent.")){
                errorMsg = "Pin code not sent.";
            }else if (message.equals("User not save")){
                errorMsg = "User not save.";
            }else if (message.equals("Empty credentials.")){
                errorMsg = "Empty credentials.";
            }else if (message.equals("Email not available.")){
                errorMsg = "Email not available.";
            }else if (message.equals("Invalid email address.")){
                errorMsg = "Invalid email address.";
            }else if (message.equals("Phone number not available.")){
                errorMsg = "Phone number not available.";
            }else if (message.equals("Invalid phone number.")){
                errorMsg = "Invalid phone number.";
            }else {
                errorMsg = message;
            }
            ShowAlert("Confirm code",errorMsg + " Please try other code.");
            errorMsg = "";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    public void onAccessToken(String access_token, String token_type) {
        this.token_type = token_type;
        this.access_token = access_token;
        SuperUtil.SavePreference(this,SuperUtil.ACCESS_TOKEN,isForget ? null : access_token);
        SuperUtil.SavePreference(this,SuperUtil.TOKEN_TYPE,isForget ? null : token_type);
    }

    private boolean isValid(){
        boolean valid = true;
        String digit1 = this.digit1.getText().toString();
        String digit2 = this.digit2.getText().toString();
        String digit3 = this.digit3.getText().toString();
        String digit4 = this.digit4.getText().toString();

        if (validateTor.isEmpty(digit1)){
            this.digit1.setBackgroundResource(R.drawable.background_error_input_login);
            valid = false;
        }else {
            this.digit1.setBackgroundResource(R.drawable.background_input_login);
        }

        if (validateTor.isEmpty(digit2)){
            this.digit2.setBackgroundResource(R.drawable.background_error_input_login);
            valid = false;
        }else {
            this.digit2.setBackgroundResource(R.drawable.background_input_login);
        }

        if (validateTor.isEmpty(digit3)){
            this.digit3.setBackgroundResource(R.drawable.background_error_input_login);
            valid = false;
        }else {
            this.digit3.setBackgroundResource(R.drawable.background_input_login);
        }

        if (validateTor.isEmpty(digit4)){
            this.digit4.setBackgroundResource(R.drawable.background_error_input_login);
            valid = false;
        }else {
            this.digit4.setBackgroundResource(R.drawable.background_input_login);
        }

        return valid;
    }

    private void OnConfirmCode(){

        String code1 = digit1.getText().toString();
        String code2 = digit2.getText().toString();
        String code3 = digit3.getText().toString();
        String code4 = digit4.getText().toString();

        List<MultipartBody.Part> parts = new ArrayList<>();

        parts.add(okhttp3.MultipartBody.Part.createFormData("phone",phoneOrEmail));
        parts.add(okhttp3.MultipartBody.Part.createFormData("email",phoneOrEmail));
        parts.add(okhttp3.MultipartBody.Part.createFormData("confirm_code",(code1+code2+code3+code4)));

        if (isForget){
            parts.add(okhttp3.MultipartBody.Part.createFormData("user_id",String.valueOf(Auth.getId())));
        }else {
            parts.add(okhttp3.MultipartBody.Part.createFormData("password",tempPassword));
            parts.add(okhttp3.MultipartBody.Part.createFormData("user_name",tempUserName));
        }

        baseLogin.ConfirmCode(parts, isPhone ? AuthType.PHONE : AuthType.EMAIL);
    }

    private void OnChange(){
        ChangePhoneOrEmail view = new ChangePhoneOrEmail(this);
        view.setActivity(this);
        view.addListener(this);
        view.setChangeValue(phoneOrEmail);
        view.setPhone(isPhone);
        builder = new AlertDialog.Builder(this).setView(view).create();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        builder.getWindow().setLayout((int) (displayWidth * 1f),(int) 0.4f);
        builder.setCanceledOnTouchOutside(false);
        builder.show();
    }

    private void OnResendCode(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Resend Code").setMessage("Do you want to this code to "+ phoneOrEmail +"?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String token = token_type + " " + access_token;
                List<MultipartBody.Part> parts = new ArrayList<>();
                if (isPhone){
                    phoneOrEmail = phoneOrEmail.startsWith("+855") ? phoneOrEmail.replaceFirst("[+]855","0") : phoneOrEmail;
                    parts.add(okhttp3.MultipartBody.Part.createFormData("phone",phoneOrEmail));
                }else {
                    parts.add(okhttp3.MultipartBody.Part.createFormData("email",phoneOrEmail));
                }
                baseLogin.ResendCode(token,parts,isPhone ? AuthType.PHONE : AuthType.EMAIL);
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void onClick(View v) {
        if (v == btnConfirm){
            if (isValid()){
                showProgress();
                OnConfirmCode();
            }
        }else if (v == btnChange){
            OnChange();
        }else if (v == textResendCode){
            OnResendCode();
        }
    }

    @Override
    public void onClosed(ChangePhoneOrEmail view) {
        builder.dismiss();
    }

    @Override
    public void onChanged(ChangePhoneOrEmail view, String changedValue, AuthType authType) {
        if (invalidChangeValue(changedValue,authType)){
            view.setErrorMessage(errorMsg);
            errorMsg = "";
        }else {
            phoneOrEmail = changedValue;
            txtPhoneAndEmail.setText(changedValue);
            String token = token_type + " " + access_token;
            List<MultipartBody.Part> parts = new ArrayList<>();
            if (authType == AuthType.PHONE){
                phoneOrEmail = phoneOrEmail.startsWith("+855") ? phoneOrEmail.replaceFirst("[+]855","0") : phoneOrEmail;
                parts.add(okhttp3.MultipartBody.Part.createFormData("phone",phoneOrEmail));
            }else {
                parts.add(okhttp3.MultipartBody.Part.createFormData("email",phoneOrEmail));
            }
            baseLogin.ResendCode(token,parts,authType);
            builder.dismiss();
        }
    }

    private boolean invalidChangeValue(String changedValue,AuthType authType){
        boolean valid = false;
        isPhone = authType == AuthType.PHONE;

        if (isPhone){
            if (validateTor.isEmpty(changedValue)){
                errorMsg += "- Phone number is empty.\n";
                valid = true;
            }

            else if (validateTor.isPhoneNumber(changedValue)){
                errorMsg += "- Phone number not valid.\n";
                valid = true;
            }

            else if (!changedValue.startsWith("+855") && !changedValue.startsWith("0")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("00")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("0000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("00000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("0000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("00000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+8550")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+85500")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+855000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+8550000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+85500000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+855000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+8550000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+85500000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                valid = true;
            }

            else if (changedValue.startsWith("+855") && !changedValue.startsWith("+8550")){
                if (!(changedValue.length() >= 12 && changedValue.length() <= 13)){
                    errorMsg += "- Phone number should contain at least 12 or 13 numbers.\n";
                    valid = true;
                }
            }

            else if (changedValue.startsWith("0")){
                if (!(changedValue.length() >= 8 && changedValue.length() <= 9)){
                    errorMsg += "- Phone number should contain at least 8 or 9 numbers.\n";
                    valid = true;
                }
            }

        }else {
            if (validateTor.isEmpty(changedValue)){
                errorMsg += "- Email is empty.\n";
                valid = true;
            }

            else if (!validateTor.isEmail(changedValue)){
                errorMsg += "- Email not valid.\n";
                valid = true;
            }
        }

        return valid;
    }

}

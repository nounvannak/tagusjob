package com.tagusnow.tagmejob.v3.login.other;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.raywenderlich.android.validatetor.ValidateTor;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.login.auth.AuthCallback;
import com.tagusnow.tagmejob.v3.login.auth.AuthReponse;
import com.tagusnow.tagmejob.v3.login.auth.AuthType;
import com.tagusnow.tagmejob.v3.login.auth.BaseLogin;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Response;

public class ForgetPasswordActivity extends TagUsJobActivity implements View.OnClickListener, AuthCallback<SmUser, AuthReponse> {

    private static final String TAG = ForgetPasswordActivity.class.getSimpleName();
    private RelativeLayout layoutError;
    private ImageButton btnCloseError;
    private EditText InputPhone;
    private ImageView iconOfInputPhone;
    private Button btnSend;
    private ImageButton btnEmail;
    private TextView errorText,txtNotice;
    private boolean isPhone = true;
    private boolean isError;
    private String errorMsg = "";
    private ValidateTor validateTor;
    private BaseLogin baseLogin;
    private String email,phone;
    private SmUser Auth;


    public static Intent createIntent(Activity activity){
        return new Intent(activity,ForgetPasswordActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ChangeStatusBar(Color.WHITE);
        InitTemp();
        validateTor = new ValidateTor();
        baseLogin = new BaseLogin(this);
        baseLogin.addCallback(this);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        layoutError = (RelativeLayout)findViewById(R.id.layoutError);
        btnCloseError = (ImageButton)findViewById(R.id.btnCloseErrorMsg);
        btnCloseError.setOnClickListener(this);
        errorText = (TextView)findViewById(R.id.errorText);
        txtNotice = (TextView)findViewById(R.id.txtNotice);

        btnSend = (Button)findViewById(R.id.btnSendCode);
        btnSend.setOnClickListener(this);

        iconOfInputPhone = (ImageView)findViewById(R.id.iconOfInputPhone);

        InputPhone = (EditText)findViewById(R.id.InputPhone);

        btnEmail = (ImageButton)findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(this);

        setError(false);
    }

    private void InitTemp(){
        this.Auth = new Auth(this).checkAuth().token();
    }

    private void setError(boolean error) {
        isError = error;
        if (error){
            layoutError.setVisibility(View.VISIBLE);
        }else {
            layoutError.setVisibility(View.GONE);
        }
    }

    private boolean validate(){
        boolean isvalid = true;
        if (isPhone){
            String phone = InputPhone.getText().toString();
            if (validateTor.isEmpty(phone)){
                errorMsg += "- Phone number is empty.\n";
                isvalid = false;
            }

            else if (validateTor.isPhoneNumber(phone)){
                errorMsg += "- Phone number not valid.\n";
                isvalid = false;
            }

            else if (!phone.startsWith("+855") && !phone.startsWith("0")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("0000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("0000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855") && !phone.startsWith("+8550")){
                if (!(phone.length() >= 12 && phone.length() <= 13)){
                    errorMsg += "- Phone number should contain at least 12 or 13 numbers.\n";
                    isvalid = false;
                }
            }

            else if (phone.startsWith("0")){
                if (!(phone.length() >= 8 && phone.length() <= 9)){
                    errorMsg += "- Phone number should contain at least 8 or 9 numbers.\n";
                    isvalid = false;
                }
            }

        }else {
            String email = InputPhone.getText().toString();
            if (validateTor.isEmpty(email)){
                errorMsg += "- Email is empty.\n";
                isvalid = false;
            }

            else if (!validateTor.isEmail(email)){
                errorMsg += "- Email not valid.\n";
                isvalid = false;
            }
        }

        if (!isvalid){
            errorText.setText(errorMsg);
        }

        return isvalid;
    }

    private void setPhone(boolean phone) {
        isPhone = phone;
        if (phone){
            iconOfInputPhone.setImageResource(R.drawable.phone_one);
            InputPhone.setHint(getString(R.string.phone_number));
            InputPhone.setInputType(InputType.TYPE_CLASS_PHONE);
            btnEmail.setImageResource(R.drawable.message_one);
            txtNotice.setText(R.string.enter_your_phone_number_that_you_register_with_us_our_system_will_send_verify_code_to_your_phone);
        }else {
            iconOfInputPhone.setImageResource(R.drawable.message_one);
            InputPhone.setHint(getString(R.string.email));
            InputPhone.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            btnEmail.setImageResource(R.drawable.phone_one);
            txtNotice.setText(R.string.enter_your_email_that_you_register_with_us_our_system_will_send_verify_code_to_your_email);
        }
    }

    private void OnSend(){
        if (isPhone){

            phone = InputPhone.getText().toString();
            phone = phone.startsWith("+855") ? phone.replaceFirst("[+]855","0") : phone;

            List<MultipartBody.Part> parts = new ArrayList<>();

            parts.add(okhttp3.MultipartBody.Part.createFormData("user_id",String.valueOf(Auth.getId())));
            parts.add(okhttp3.MultipartBody.Part.createFormData("phone",phone));

            baseLogin.SendCode(parts, AuthType.PHONE);

        }else {
            email = InputPhone.getText().toString();

            List<MultipartBody.Part> parts = new ArrayList<>();

            parts.add(okhttp3.MultipartBody.Part.createFormData("user_id",String.valueOf(Auth.getId())));
            parts.add(okhttp3.MultipartBody.Part.createFormData("email",email));

            baseLogin.SendCode(parts,AuthType.EMAIL);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnCloseError){
            layoutError.setVisibility(View.GONE);
        }else if (view == btnEmail){
            if (isPhone){
                setPhone(false);
            }else {
                setPhone(true);
            }
        }else if (view == btnSend){
            if (validate()){
                setError(false);
                showProgress();
                OnSend();
                errorMsg = "";
            }else {
                setError(true);
                errorMsg = "";
            }
        }
    }

    @Override
    public void onSuccess(SmUser user) {
        dismissProgress();
        new Auth(this).save(user);
        AccessApp(user);
    }

    @Override
    public void onSent(AuthReponse response) {
        dismissProgress();
        SuperUtil.SavePreference(this,SuperUtil.LOGIN_METHOD,response.getLogin_method());
//        SuperUtil.SavePreference(this,SuperUtil.FULL_NAME,fullName);
//        SuperUtil.SavePreference(this,SuperUtil.PASSWORD,password);
        SuperUtil.SavePreference(this,SuperUtil.LOGIN_VALUE,phone != null ? phone : email);
        GoToConfirmSMSCode(true);
    }

    @Override
    public void onLogOut(Response response) {

    }

    @Override
    public void onWarning(Response response) {
        dismissProgress();
        if (response.code() == 401){
            try {
                String errorJsonString = response.errorBody().string();
                AuthReponse authReponse = new Gson().fromJson(errorJsonString,AuthReponse.class);
                String message = authReponse != null ? authReponse.getError() : errorJsonString;
                if (message.equals("User not found")){
                    errorMsg = "Invalid Credentials.";
                }else if (message.equals("Password not matches")){
                    errorMsg = "Wrong password.";
                }else if (message.equals("User was removed")){
                    errorMsg = "User was removed, please contact to admin.";
                }else if (message.equals("No permission")){
                    errorMsg = "No permission, please contact to admin.";
                }else if (message.equals("No auth type")){
                    errorMsg = "No auth type.";
                }else if (message.equals("No access token")){
                    errorMsg = "No access token.";
                }else if (message.equals("No user")){
                    errorMsg = "No user.";
                }else if (message.equals("Sorry, this code already verified.")){
                    errorMsg = "Sorry, this code already verified.";
                }else if (message.equals("Sorry, we can't confirm this code.")){
                    errorMsg = "Sorry, we can't confirm this code.";
                }else if (message.equals("Sorry, we can't update your account.")){
                    errorMsg = "Sorry, we can't update your account.";
                }else if (message.equals("No confirm code")){
                    errorMsg = "No confirm code.";
                }else if (message.equals("Pin code not sent.")){
                    errorMsg = "Pin code not sent.";
                }else if (message.equals("User not save")){
                    errorMsg = "User not save.";
                }else if (message.equals("Empty credentials.")){
                    errorMsg = "Empty credentials.";
                }else if (message.equals("Email not available.")){
                    errorMsg = "Email not available.";
                }else if (message.equals("Invalid email address.")){
                    errorMsg = "Invalid email address.";
                }else if (message.equals("Phone number not available.")){
                    errorMsg = "Phone number not available.";
                }else if (message.equals("Invalid phone number.")){
                    errorMsg = "Invalid phone number.";
                }else {
                    errorMsg = message;
                }
                setError(true);
                errorText.setText(errorMsg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    public void onAccessToken(String access_token, String token_type) {
        SuperUtil.SavePreference(this,SuperUtil.ACCESS_TOKEN,access_token);
        SuperUtil.SavePreference(this,SuperUtil.TOKEN_TYPE,token_type);
    }
}

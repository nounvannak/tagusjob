package com.tagusnow.tagmejob.v3.login.other;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.maksim88.passwordedittext.PasswordEditText;
import com.raywenderlich.android.validatetor.ValidateTor;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.login.auth.AuthCallback;
import com.tagusnow.tagmejob.v3.login.auth.AuthReponse;
import com.tagusnow.tagmejob.v3.login.auth.AuthType;
import com.tagusnow.tagmejob.v3.login.auth.BaseLogin;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import retrofit2.Response;

public class UpdatePasswordActivity extends TagUsJobActivity implements View.OnClickListener, AuthCallback<SmUser, AuthReponse> {

    private static final String TAG = UpdatePasswordActivity.class.getSimpleName();
    private String regexPassword = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{6,}";
    private RelativeLayout layoutError;
    private ImageButton btnCloseError;
    private EditText InputPhone;
    private EditText InputPassword,InputConfirmPassword;
    private ImageView iconOfInputPhone;
    private Button btnUpdate;
    private TextView errorText;
    private TextView name;
    private CircleImageView profile;
    private boolean isPhone = true;
    private String errorMsg = "";
    private ValidateTor validateTor;
    private BaseLogin baseLogin;
    private SmUser Auth;
    private String access_token,token_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        ChangeStatusBar(Color.WHITE);
        InitTemp();

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        validateTor = new ValidateTor();
        baseLogin = new BaseLogin(this);
        baseLogin.addCallback(this);

        name = (TextView)findViewById(R.id.name);
        profile = (CircleImageView)findViewById(R.id.profile);

        layoutError = (RelativeLayout)findViewById(R.id.layoutError);
        btnCloseError = (ImageButton)findViewById(R.id.btnCloseErrorMsg);
        btnCloseError.setOnClickListener(this);
        errorText = (TextView)findViewById(R.id.errorText);

        iconOfInputPhone = (ImageView)findViewById(R.id.iconOfInputPhone);

        InputPhone = (EditText)findViewById(R.id.InputPhone);
        InputPassword = (EditText)findViewById(R.id.InputPassword);
        InputConfirmPassword = (EditText)findViewById(R.id.InputConfirmPassword);

        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);

        name.setText(getName(Auth,NameType.FULL));
        setUserProfile(profile,Auth);
        setPhone(isPhone);
        setError(false);
    }

    private void InitTemp(){
        this.Auth = new Auth(this).checkAuth().token();
        this.access_token = getIntent().getStringExtra(SuperUtil.ACCESS_TOKEN);
        this.token_type = getIntent().getStringExtra(SuperUtil.TOKEN_TYPE);
        this.isPhone = this.Auth.getLogin_method().equals(SmUser.PHONE);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity, UpdatePasswordActivity.class);
    }

    private void setError(boolean error) {
        if (error){
            layoutError.setVisibility(View.VISIBLE);
        }else {
            layoutError.setVisibility(View.GONE);
        }
    }

    private boolean validate(){
        boolean isvalid = true;

        if (isPhone){
            String phone = InputPhone.getText().toString();
            if (validateTor.isEmpty(phone)){
                errorMsg += "- Phone number is empty.\n";
                isvalid = false;
            }

            else if (validateTor.isPhoneNumber(phone)){
                errorMsg += "- Phone number not valid.\n";
                isvalid = false;
            }

            else if (!phone.startsWith("+855") && !phone.startsWith("0")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("0000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("0000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855") && !phone.startsWith("+8550")){
                if (!(phone.length() >= 12 && phone.length() <= 13)){
                    errorMsg += "- Phone number should contain at least 12 or 13 numbers.\n";
                    isvalid = false;
                }
            }

            else if (phone.startsWith("0")){
                if (!(phone.length() >= 8 && phone.length() <= 9)){
                    errorMsg += "- Phone number should contain at least 8 or 9 numbers.\n";
                    isvalid = false;
                }
            }

        }else {
            String email = InputPhone.getText().toString();
            if (validateTor.isEmpty(email)){
                errorMsg += "- Email is empty.\n";
                isvalid = false;
            }

            else if (!validateTor.isEmail(email)){
                errorMsg += "- Email not valid.\n";
                isvalid = false;
            }
        }

        String password = InputPassword.getText().toString();
        String confirmPassword = InputConfirmPassword.getText().toString();

        if (validateTor.isEmpty(password)){
            errorMsg += "- Password is empty.\n";
            isvalid = false;
        }else {
            if (password.length() < 6){
                errorMsg += "- password should contain at least 6 characters.\n";
                isvalid = false;
            }

            else if (!password.matches(regexPassword)){
                errorMsg += getString(R.string.err_password);
                isvalid = false;
            }

        }

        if (validateTor.isEmpty(confirmPassword)){
            errorMsg += "- Confirm Password is empty.\n";
            isvalid = false;
        }else {
            if (confirmPassword.length() < 6){
                errorMsg += "- Confirm password should contain at least 6 characters.\n";
                isvalid = false;
            }

            else if (!confirmPassword.matches(regexPassword)){
                errorMsg += getString(R.string.err_password);
                isvalid = false;
            }

        }

        if (!password.contains(confirmPassword)){
            errorMsg += "- Password not matches.";
            isvalid = false;
        }

        if (!isvalid){
            errorText.setText(errorMsg);
        }

        return isvalid;
    }

    private void setPhone(boolean phone) {
        isPhone = phone;
        if (phone){
            InputPhone.setText(Auth.getPhone());
            InputPhone.setEnabled(false);
            iconOfInputPhone.setImageResource(R.drawable.phone_one);
            InputPhone.setHint(getString(R.string.phone_number));
            InputPhone.setInputType(InputType.TYPE_CLASS_PHONE);
        }else {
            InputPhone.setText(Auth.getEmail());
            InputPhone.setEnabled(false);
            iconOfInputPhone.setImageResource(R.drawable.message_one);
            InputPhone.setHint(getString(R.string.email));
            InputPhone.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        }
    }

    private void OnUpdatePassword(){
        List<MultipartBody.Part> parts = new ArrayList<>();
        String token = token_type + " " + access_token;
        String password = InputPassword.getText().toString();
        String confirmPassword = InputConfirmPassword.getText().toString();
        parts.add(okhttp3.MultipartBody.Part.createFormData("password",password));
        parts.add(okhttp3.MultipartBody.Part.createFormData("confirm_password",confirmPassword));
        baseLogin.UpdatePassword(token,parts,isPhone ? AuthType.PHONE :AuthType.EMAIL);
    }

    @Override
    public void onClick(View view) {
        if(view == btnUpdate){
            if (validate()){
                android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Update Password").setMessage("Are you sure to update your password?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setError(false);
                        showProgress();
                        OnUpdatePassword();
                        errorMsg = "";
                    }
                }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();

            }else {
                setError(true);
                errorMsg = "";
            }
        }else if (view == btnCloseError){
            layoutError.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSuccess(SmUser user) {
        dismissProgress();
        new Auth(this).save(user);
        AccessApp(user);
    }

    @Override
    public void onSent(AuthReponse response) {

    }

    @Override
    public void onLogOut(Response response) {

    }

    @Override
    public void onWarning(Response response) {
        dismissProgress();
        try {
            String errorJsonString = response.errorBody().string();
            AuthReponse authReponse = new Gson().fromJson(errorJsonString,AuthReponse.class);
            String message = authReponse != null ? authReponse.getError() : errorJsonString;
            if (message.equals("User not found")){
                errorMsg = "Invalid Credentials.";
            }else if (message.equals("Password not matches")){
                errorMsg = "Wrong password.";
            }else if (message.equals("User was removed")){
                errorMsg = "User was removed, please contact to admin.";
            }else if (message.equals("No permission")){
                errorMsg = "No permission, please contact to admin.";
            }else if (message.equals("No auth type")){
                errorMsg = "No auth type.";
            }else if (message.equals("No access token")){
                errorMsg = "No access token.";
            }else if (message.equals("No user")){
                errorMsg = "No user.";
            }else if (message.equals("Sorry, this code already verified.")){
                errorMsg = "Sorry, this code already verified.";
            }else if (message.equals("Sorry, we can't confirm this code.")){
                errorMsg = "Sorry, we can't confirm this code.";
            }else if (message.equals("Sorry, we can't update your account.")){
                errorMsg = "Sorry, we can't update your account.";
            }else if (message.equals("No confirm code")){
                errorMsg = "No confirm code.";
            }else if (message.equals("Pin code not sent.")){
                errorMsg = "Pin code not sent.";
            }else if (message.equals("User not save")){
                errorMsg = "User not save.";
            }else if (message.equals("Empty credentials.")){
                errorMsg = "Empty credentials.";
            }else if (message.equals("Email not available.")){
                errorMsg = "Email not available.";
            }else if (message.equals("Invalid email address.")){
                errorMsg = "Invalid email address.";
            }else if (message.equals("Phone number not available.")){
                errorMsg = "Phone number not available.";
            }else if (message.equals("Invalid phone number.")){
                errorMsg = "Invalid phone number.";
            }else {
                errorMsg = message;
            }
            setError(true);
            errorText.setText(errorMsg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Throwable throwable) {
        dismissProgress();
        throwable.printStackTrace();
    }

    @Override
    public void onAccessToken(String access_token, String token_type) {
        Log.e(TAG,access_token);
        SuperUtil.SavePreference(this,SuperUtil.ACCESS_TOKEN,access_token);
        SuperUtil.SavePreference(this,SuperUtil.TOKEN_TYPE,token_type);
    }
}

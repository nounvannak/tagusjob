package com.tagusnow.tagmejob.v3.login.phone;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.maksim88.passwordedittext.PasswordEditText;
import com.raywenderlich.android.validatetor.ValidateTor;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.login.auth.AuthCallback;
import com.tagusnow.tagmejob.v3.login.auth.AuthReponse;
import com.tagusnow.tagmejob.v3.login.auth.AuthType;
import com.tagusnow.tagmejob.v3.login.auth.BaseLogin;
import com.tagusnow.tagmejob.v3.login.other.ForgetPasswordActivity;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.MultipartBody;
import retrofit2.Response;

public class LoginWithPhoneActivity extends TagUsJobActivity implements View.OnClickListener, AuthCallback<SmUser, AuthReponse> {

    private static final String TAG = LoginWithPhoneActivity.class.getSimpleName();
    private String regexPassword = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{6,}";
    private RelativeLayout layoutError;
    private ImageButton btnCloseError;
    private TextView errorText;
    private RelativeLayout layoutOfInputPhone;
    private ImageView iconOfInputPhone;
    private EditText InputPhone;
    private RelativeLayout layoutOfInputPassword;
    private EditText InputPassword;
    private TextView txtForgetPassword;
    private Button btnLogin;
    private TextView textRegister;
    private ImageButton btnEmail,btnFacebook,btnGoogle;
    private boolean isPhone = true;
    private boolean isError;
    private String errorMsg = "";
    private ValidateTor validateTor;
    private BaseLogin baseLogin;

    public static Intent createIntent(Activity activity){
        return new Intent(activity,LoginWithPhoneActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_phone);
        ChangeStatusBar(Color.WHITE);

        validateTor = new ValidateTor();
        baseLogin = new BaseLogin(this);
        baseLogin.addCallback(this);

        layoutError = (RelativeLayout)findViewById(R.id.layoutError);
        btnCloseError = (ImageButton)findViewById(R.id.btnCloseErrorMsg);
        btnCloseError.setOnClickListener(this);
        errorText = (TextView)findViewById(R.id.errorText);

        layoutOfInputPhone = (RelativeLayout)findViewById(R.id.layoutOfInputPhone);
        iconOfInputPhone = (ImageView)findViewById(R.id.iconOfInputPhone);
        InputPhone = (EditText)findViewById(R.id.InputPhone);

        layoutOfInputPassword = (RelativeLayout)findViewById(R.id.layoutOfInputPassword);
        InputPassword = (EditText)findViewById(R.id.InputPassword);

        txtForgetPassword = (TextView)findViewById(R.id.txtForgetPassword);
        txtForgetPassword.setOnClickListener(this);

        textRegister = (TextView)findViewById(R.id.textRegister);
        textRegister.setOnClickListener(this);

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        btnEmail = (ImageButton)findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(this);

        btnFacebook = (ImageButton)findViewById(R.id.btnFacebook);
        btnFacebook.setOnClickListener(this);

        btnGoogle = (ImageButton)findViewById(R.id.btnGoogle);
        btnGoogle.setOnClickListener(this);

        setError(false);
    }

    private boolean validate(){
        boolean isvalid = true;
        if (isPhone){
            String phone = InputPhone.getText().toString();
            if (validateTor.isEmpty(phone)){
                errorMsg += "- Phone number is empty.\n";
                isvalid = false;
            }

            else if (validateTor.isPhoneNumber(phone)){
                errorMsg += "- Phone number not valid.\n";
                isvalid = false;
            }

            else if (!phone.startsWith("+855") && !phone.startsWith("0")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("0000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("0000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855") && !phone.startsWith("+8550")){
                if (!(phone.length() >= 12 && phone.length() <= 13)){
                    errorMsg += "- Phone number should contain at least 12 or 13 numbers.\n";
                    isvalid = false;
                }
            }

//            else if (phone.startsWith("+8550")){
//                if (!(phone.length() >= 13 && phone.length() <= 14)){
//                    errorMsg += "- Phone number should contain at least 13 or 14 numbers.\n";
//                    isvalid = false;
//                }
//            }

            else if (phone.startsWith("0")){
                if (!(phone.length() >= 8 && phone.length() <= 9)){
                    errorMsg += "- Phone number should contain at least 8 or 9 numbers.\n";
                    isvalid = false;
                }
            }

        }else {
            String email = InputPhone.getText().toString();
            if (validateTor.isEmpty(email)){
                errorMsg += "- Email is empty.\n";
                isvalid = false;
            }

            else if (!validateTor.isEmail(email)){
                errorMsg += "- Email not valid.\n";
                isvalid = false;
            }
        }

        String password = InputPassword.getText().toString();
        if (validateTor.isEmpty(password)){
            errorMsg += "- Password is empty.\n";
            isvalid = false;
        }else {
            if (password.length() < 6){
                errorMsg += "- password should contain at least 6 characters.\n";
                isvalid = false;
            }

//            else if (!password.matches(regexPassword)){
//                errorMsg += getString(R.string.err_password);
//                isvalid = false;
//            }
        }

        if (!isvalid){
            errorText.setText(errorMsg);
        }

        return isvalid;
    }

    @Override
    public void onClick(View view) {
        if (view == btnLogin){
            if (validate()){
                setError(false);
                showProgress();
                OnLogin();
                errorMsg = "";
            }else {
                setError(true);
                errorMsg = "";
            }
        }else if (view == btnEmail){
            if (isPhone){
                setPhone(false);
            }else {
                setPhone(true);
            }
        }else if (view == btnCloseError){
            layoutError.setVisibility(View.GONE);
        }else if (view == btnFacebook){
            baseLogin.Login(new ArrayList<>(), AuthType.FACEBOOK);
        }else if (view == btnGoogle){
            baseLogin.Login(new ArrayList<>(), AuthType.GOOGLE);
        }else if (view == txtForgetPassword){
            startActivity(ForgetPasswordActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (view == textRegister){
            startActivity(RegisterWithPhoneActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    private void OnLogin(){
        if (isPhone){

            String phone = InputPhone.getText().toString();
            String password = InputPassword.getText().toString();

            phone = phone.startsWith("+855") ? phone.replaceFirst("[+]855","0") : phone;

            List<MultipartBody.Part> parts = new ArrayList<>();

            parts.add(okhttp3.MultipartBody.Part.createFormData("phone",phone));
            parts.add(okhttp3.MultipartBody.Part.createFormData("password",password));

            baseLogin.Login(parts,AuthType.PHONE);

        }else {
            String email = InputPhone.getText().toString();
            String password = InputPassword.getText().toString();

            List<MultipartBody.Part> parts = new ArrayList<>();

            parts.add(okhttp3.MultipartBody.Part.createFormData("email",email));
            parts.add(okhttp3.MultipartBody.Part.createFormData("password",password));

            baseLogin.Login(parts,AuthType.EMAIL);
        }
    }

    public void setPhone(boolean phone) {
        isPhone = phone;
        if (phone){
            iconOfInputPhone.setImageResource(R.drawable.phone_one);
            InputPhone.setHint(getString(R.string.phone_number));
            InputPhone.setInputType(InputType.TYPE_CLASS_PHONE);
            btnEmail.setImageResource(R.drawable.message_one);
        }else {
            iconOfInputPhone.setImageResource(R.drawable.message_one);
            InputPhone.setHint(getString(R.string.email));
            InputPhone.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            btnEmail.setImageResource(R.drawable.phone_one);
        }
    }

    private void setError(boolean error) {
        isError = error;
        if (error){
            layoutError.setVisibility(View.VISIBLE);
        }else {
            layoutError.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSuccess(SmUser user) {
//        dismissProgress();
        Log.d(TAG,"You are logged in.");
        new Auth(this).save(user);
        AccessApp(user);
    }

    @Override
    public void onSent(AuthReponse response) {

    }

    @Override
    public void onLogOut(Response response) {

    }

    @Override
    public void onWarning(Response response) {
        dismissProgress();
        try {
            String errorJsonString = response.errorBody().string();
            AuthReponse authReponse = new Gson().fromJson(errorJsonString,AuthReponse.class);
            String message = authReponse != null ? authReponse.getError() : errorJsonString;
            if (message.equals("User not found")){
                errorMsg = "Invalid Credentials.";
            }else if (message.equals("Password not matches")){
                errorMsg = "Wrong password.";
            }else if (message.equals("User was removed")){
                errorMsg = "User was removed, please contact to admin.";
            }else if (message.equals("No permission")){
                errorMsg = "No permission, please contact to admin.";
            }else if (message.equals("No auth type")){
                errorMsg = "No auth type.";
            }else if (message.equals("No access token")){
                errorMsg = "No access token.";
            }else if (message.equals("No user")){
                errorMsg = "No user.";
            }else if (message.equals("Sorry, this code already verified.")){
                errorMsg = "Sorry, this code already verified.";
            }else if (message.equals("Sorry, we can't confirm this code.")){
                errorMsg = "Sorry, we can't confirm this code.";
            }else if (message.equals("Sorry, we can't update your account.")){
                errorMsg = "Sorry, we can't update your account.";
            }else if (message.equals("No confirm code")){
                errorMsg = "No confirm code.";
            }else if (message.equals("Pin code not sent.")){
                errorMsg = "Pin code not sent.";
            }else if (message.equals("User not save")){
                errorMsg = "User not save.";
            }else if (message.equals("Empty credentials.")){
                errorMsg = "Empty credentials.";
            }else if (message.equals("Email not available.")){
                errorMsg = "Email not available.";
            }else if (message.equals("Invalid email address.")){
                errorMsg = "Invalid email address.";
            }else if (message.equals("Phone number not available.")){
                errorMsg = "Phone number not available.";
            }else if (message.equals("Invalid phone number.")){
                errorMsg = "Invalid phone number.";
            }else {
                errorMsg = message;
            }
            setError(true);
            errorText.setText(errorMsg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Throwable throwable) {
        dismissProgress();
        throwable.printStackTrace();
    }

    @Override
    public void onAccessToken(String access_token, String token_type) {
        Log.e("access_token",access_token);
        SuperUtil.SavePreference(this,SuperUtil.ACCESS_TOKEN,access_token);
        SuperUtil.SavePreference(this,SuperUtil.TOKEN_TYPE,token_type);
    }

}

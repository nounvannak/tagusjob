package com.tagusnow.tagmejob.v3.login.phone;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.maksim88.passwordedittext.PasswordEditText;
import com.raywenderlich.android.validatetor.ValidateTor;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.login.auth.AuthCallback;
import com.tagusnow.tagmejob.v3.login.auth.AuthReponse;
import com.tagusnow.tagmejob.v3.login.auth.AuthType;
import com.tagusnow.tagmejob.v3.login.auth.BaseLogin;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Response;

public class RegisterWithPhoneActivity extends TagUsJobActivity implements AuthCallback<SmUser, AuthReponse>, View.OnClickListener {

    private static final String TAG = RegisterWithPhoneActivity.class.getSimpleName();
    private String regexPassword = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{6,}";
    private ImageButton btnBack;
    private RelativeLayout layoutError;
    private ImageButton btnCloseError;
    private EditText InputFullName,InputPhone;
    private EditText InputPassword;
    private ImageView iconOfInputPhone;
    private Button btnRegister;
    private TextView textLogin;
    private ImageButton btnEmail,btnFacebook,btnGoogle;
    private TextView errorText;
    private boolean isPhone = true;
    private boolean isError;
    private String errorMsg = "";
    private ValidateTor validateTor;
    private BaseLogin baseLogin;
    private String fullName,password,email,phone;

    public static Intent createIntent(Activity activity){
        return new Intent(activity,RegisterWithPhoneActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_with_phone);
        ChangeStatusBar(Color.WHITE);
        validateTor = new ValidateTor();
        baseLogin = new BaseLogin(this);
        baseLogin.addCallback(this);

        layoutError = (RelativeLayout)findViewById(R.id.layoutError);
        btnCloseError = (ImageButton)findViewById(R.id.btnCloseErrorMsg);
        btnCloseError.setOnClickListener(this);
        errorText = (TextView)findViewById(R.id.errorText);

        btnBack = (ImageButton)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        iconOfInputPhone = (ImageView)findViewById(R.id.iconOfInputPhone);

        InputPhone = (EditText)findViewById(R.id.InputPhone);
        InputPassword = (EditText)findViewById(R.id.InputPassword);
        InputFullName = (EditText)findViewById(R.id.InputFullName);

        textLogin = (TextView)findViewById(R.id.textLogin);
        textLogin.setOnClickListener(this);

        btnRegister = (Button)findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);

        btnEmail = (ImageButton)findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(this);

        btnFacebook = (ImageButton)findViewById(R.id.btnFacebook);
        btnFacebook.setOnClickListener(this);

        btnGoogle = (ImageButton)findViewById(R.id.btnGoogle);
        btnGoogle.setOnClickListener(this);

        setError(false);
    }

    private void setError(boolean error) {
        isError = error;
        if (error){
            layoutError.setVisibility(View.VISIBLE);
        }else {
            layoutError.setVisibility(View.GONE);
        }
    }

    private boolean validate(){
        boolean isvalid = true;

        String fullName = InputFullName.getText().toString();
        String regex = "^[\\p{L} .'-]+$";
        if (validateTor.isEmpty(fullName)){
            errorMsg += "- Name is empty.\n";
            isvalid = false;
        }else {
            if (fullName.length() < 2){
                errorMsg += "- Name should contain at least 2 characters.\n";
                isvalid = false;
            }

            else if (!fullName.matches(regex)){
                errorMsg += getString(R.string.err_password);
                isvalid = false;
            }
        }

        if (isPhone){
            String phone = InputPhone.getText().toString();
            if (validateTor.isEmpty(phone)){
                errorMsg += "- Phone number is empty.\n";
                isvalid = false;
            }

            else if (validateTor.isPhoneNumber(phone)){
                errorMsg += "- Phone number not valid.\n";
                isvalid = false;
            }

            else if (!phone.startsWith("+855") && !phone.startsWith("0")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("0000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("0000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("00000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+8550000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+85500000000")){
                errorMsg += "- Phone number not for Cambodia(+855), please use format (+855,0).\n";
                isvalid = false;
            }

            else if (phone.startsWith("+855") && !phone.startsWith("+8550")){
                if (!(phone.length() >= 12 && phone.length() <= 13)){
                    errorMsg += "- Phone number should contain at least 12 or 13 numbers.\n";
                    isvalid = false;
                }
            }

//            else if (phone.startsWith("+8550")){
//                if (!(phone.length() >= 13 && phone.length() <= 14)){
//                    errorMsg += "- Phone number should contain at least 13 or 14 numbers.\n";
//                    isvalid = false;
//                }
//            }

            else if (phone.startsWith("0")){
                if (!(phone.length() >= 8 && phone.length() <= 9)){
                    errorMsg += "- Phone number should contain at least 8 or 9 numbers.\n";
                    isvalid = false;
                }
            }

        }else {
            String email = InputPhone.getText().toString();
            if (validateTor.isEmpty(email)){
                errorMsg += "- Email is empty.\n";
                isvalid = false;
            }

            else if (!validateTor.isEmail(email)){
                errorMsg += "- Email not valid.\n";
                isvalid = false;
            }
        }

        String password = InputPassword.getText().toString();
        if (validateTor.isEmpty(password)){
            errorMsg += "- Password is empty.\n";
            isvalid = false;
        }else {
            if (password.length() < 6){
                errorMsg += "- password should contain at least 6 characters.\n";
                isvalid = false;
            }

            else if (!password.matches(regexPassword)){
                errorMsg += getString(R.string.err_password);
                isvalid = false;
            }
        }

        if (!isvalid){
            errorText.setText(errorMsg);
        }

        return isvalid;
    }

    private void setPhone(boolean phone) {
        isPhone = phone;
        if (phone){
            iconOfInputPhone.setImageResource(R.drawable.phone_one);
            InputPhone.setHint(getString(R.string.phone_number));
            InputPhone.setInputType(InputType.TYPE_CLASS_PHONE);
            btnEmail.setImageResource(R.drawable.message_one);
        }else {
            iconOfInputPhone.setImageResource(R.drawable.message_one);
            InputPhone.setHint(getString(R.string.email));
            InputPhone.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            btnEmail.setImageResource(R.drawable.phone_one);
        }
    }

    private void OnRegister(){
        if (isPhone){

            fullName = InputFullName.getText().toString();
            phone = InputPhone.getText().toString();
            password = InputPassword.getText().toString();

            phone = phone.startsWith("+855") ? phone.replaceFirst("[+]855","0") : phone;

            List<MultipartBody.Part> parts = new ArrayList<>();


//            parts.add(okhttp3.MultipartBody.Part.createFormData("user_name",fullName));
//            parts.add(okhttp3.MultipartBody.Part.createFormData("phone",phone));
//            parts.add(okhttp3.MultipartBody.Part.createFormData("password",password));

            parts.add(okhttp3.MultipartBody.Part.createFormData("user_id","0"));
            parts.add(okhttp3.MultipartBody.Part.createFormData("phone",phone));

//            baseLogin.SignUp(parts,AuthType.PHONE);
            baseLogin.SendCode(parts,AuthType.PHONE);

        }else {
            fullName = InputFullName.getText().toString();
            email = InputPhone.getText().toString();
            password = InputPassword.getText().toString();

            List<MultipartBody.Part> parts = new ArrayList<>();

//            parts.add(okhttp3.MultipartBody.Part.createFormData("user_name",fullName));
//            parts.add(okhttp3.MultipartBody.Part.createFormData("email",email));
//            parts.add(okhttp3.MultipartBody.Part.createFormData("password",password));

            parts.add(okhttp3.MultipartBody.Part.createFormData("user_id","0"));
            parts.add(okhttp3.MultipartBody.Part.createFormData("email",email));

//            baseLogin.SignUp(parts,AuthType.EMAIL);
            baseLogin.SendCode(parts,AuthType.EMAIL);
        }
    }

    @Override
    public void onSuccess(SmUser user) {
        dismissProgress();
        new Auth(this).save(user);
        AccessApp(user);
    }

    @Override
    public void onSent(AuthReponse response) {
        dismissProgress();
        SuperUtil.SavePreference(this,SuperUtil.LOGIN_METHOD,response.getLogin_method());
        SuperUtil.SavePreference(this,SuperUtil.FULL_NAME,fullName);
        SuperUtil.SavePreference(this,SuperUtil.PASSWORD,password);
        SuperUtil.SavePreference(this,SuperUtil.LOGIN_VALUE,phone != null ? phone : email);
        GoToConfirmSMSCode();
    }

    @Override
    public void onLogOut(Response response) {

    }

    @Override
    public void onWarning(Response response) {
        dismissProgress();
        if (response.code() == 401){
            try {
                String errorJsonString = response.errorBody().string();
                AuthReponse authReponse = new Gson().fromJson(errorJsonString,AuthReponse.class);
                String message = authReponse != null ? authReponse.getError() : errorJsonString;
                if (message.equals("User not found")){
                    errorMsg = "Invalid Credentials.";
                }else if (message.equals("Password not matches")){
                    errorMsg = "Wrong password.";
                }else if (message.equals("User was removed")){
                    errorMsg = "User was removed, please contact to admin.";
                }else if (message.equals("No permission")){
                    errorMsg = "No permission, please contact to admin.";
                }else if (message.equals("No auth type")){
                    errorMsg = "No auth type.";
                }else if (message.equals("No access token")){
                    errorMsg = "No access token.";
                }else if (message.equals("No user")){
                    errorMsg = "No user.";
                }else if (message.equals("Sorry, this code already verified.")){
                    errorMsg = "Sorry, this code already verified.";
                }else if (message.equals("Sorry, we can't confirm this code.")){
                    errorMsg = "Sorry, we can't confirm this code.";
                }else if (message.equals("Sorry, we can't update your account.")){
                    errorMsg = "Sorry, we can't update your account.";
                }else if (message.equals("No confirm code")){
                    errorMsg = "No confirm code.";
                }else if (message.equals("Pin code not sent.")){
                    errorMsg = "Pin code not sent.";
                }else if (message.equals("User not save")){
                    errorMsg = "User not save.";
                }else if (message.equals("Empty credentials.")){
                    errorMsg = "Empty credentials.";
                }else if (message.equals("Email not available.")){
                    errorMsg = "Email not available.";
                }else if (message.equals("Invalid email address.")){
                    errorMsg = "Invalid email address.";
                }else if (message.equals("Phone number not available.")){
                    errorMsg = "Phone number not available.";
                }else if (message.equals("Invalid phone number.")){
                    errorMsg = "Invalid phone number.";
                }else {
                    errorMsg = message;
                }
                setError(true);
                errorText.setText(errorMsg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(Throwable throwable) {
        dismissProgress();
        throwable.printStackTrace();
    }

    @Override
    public void onAccessToken(String access_token, String token_type) {
        SuperUtil.SavePreference(this,SuperUtil.ACCESS_TOKEN,access_token);
        SuperUtil.SavePreference(this,SuperUtil.TOKEN_TYPE,token_type);
    }

    @Override
    public void onClick(View view) {
        if (view == btnBack){
            onBackPressed();
        }else if(view == btnRegister){
            if (validate()){
                setError(false);
                showProgress();
                OnRegister();
                errorMsg = "";
            }else {
                setError(true);
                errorMsg = "";
            }
        }else if (view == btnEmail){
            if (isPhone){
                setPhone(false);
            }else {
                setPhone(true);
            }
        }else if (view == btnCloseError){
            layoutError.setVisibility(View.GONE);
        }else if (view == btnFacebook){
            baseLogin.SignUp(new ArrayList<>(), AuthType.FACEBOOK);
        }else if (view == btnGoogle){
            baseLogin.SignUp(new ArrayList<>(), AuthType.GOOGLE);
        }else if (view == textLogin){
            startActivity(LoginWithPhoneActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}

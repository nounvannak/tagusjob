package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class NCommentCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private CircleImageView logo;
    private TextView title,date;
    private Notification notification;

    public void setNotification(Notification notification) {
        this.notification = notification;
        if (notification.isSeen()){
//            this.setBackgroundColor(Color.parseColor("#64bec5f7"));
            this.setBackgroundColor(Color.WHITE);
        }else {
            this.setBackgroundColor(Color.WHITE);
        }
        setUserProfile(logo,notification.getTarget_user());
        title.setText(notification.getBody());
        date.setText(notification.getTimeline());
    }

    public void setListener(NListener<NCommentCell, Notification> listener) {
        this.listener = listener;
    }

    private NListener<NCommentCell,Notification> listener;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.n_comment_cell,this);

        logo = (CircleImageView)findViewById(R.id.logo);
        title = (TextView)findViewById(R.id.title);
        date = (TextView)findViewById(R.id.date);

        this.setOnClickListener(this);
    }

    public NCommentCell(Context context) {
        super(context);
        InitUI(context);
    }

    public NCommentCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public NCommentCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public NCommentCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == this){
            listener.select(this,notification);
        }
    }
}

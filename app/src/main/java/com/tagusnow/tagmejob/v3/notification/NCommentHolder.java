package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Notification;

public class NCommentHolder extends RecyclerView.ViewHolder {

    private NCommentCell nCommentCell;

    public NCommentHolder(View itemView) {
        super(itemView);
        nCommentCell = (NCommentCell)itemView;
    }

    public void BindView(Activity activity, Notification notification,NListener<NCommentCell, Notification> listener){
        nCommentCell.setActivity(activity);
        nCommentCell.setNotification(notification);
        nCommentCell.setListener(listener);
    }
}

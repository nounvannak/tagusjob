package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class NFollowCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private CircleImageView logo;
    private TextView title,date;
    private Button btnConnect,btnRemove;
    private Notification notification;

    public void setNotification(Notification notification) {
        this.notification = notification;
        if (notification.isSeen()){
            //            this.setBackgroundColor(Color.parseColor("#64bec5f7"));
            this.setBackgroundColor(Color.WHITE);
        }else {
            this.setBackgroundColor(Color.WHITE);
        }
        setUserProfile(logo,notification.getTarget_user());
        title.setText(notification.getBody());
        date.setText(notification.getTimeline());

        if (notification.getRequest_user().isIs_follow()){
            btnRemove.setVisibility(GONE);
            btnConnect.setVisibility(GONE);
        }else {
            btnConnect.setVisibility(VISIBLE);
            btnRemove.setVisibility(VISIBLE);
        }
    }

    public void setListener(NListener<NFollowCell, Notification> listener) {
        this.listener = listener;
    }

    private NListener<NFollowCell,Notification> listener;

    private void InitUI(Context context){
        inflate(context, R.layout.n_follow_cell,this);

        logo = (CircleImageView)findViewById(R.id.logo);
        title = (TextView)findViewById(R.id.title);
        date = (TextView)findViewById(R.id.date);
        btnConnect = (Button)findViewById(R.id.btnConnect);
        btnRemove = (Button)findViewById(R.id.btnRemove);

        this.setOnClickListener(this);
        btnConnect.setOnClickListener(this);
        btnRemove.setOnClickListener(this);

    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public NFollowCell(Context context) {
        super(context);
        InitUI(context);
    }

    public NFollowCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public NFollowCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public NFollowCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == this){
            listener.select(this,notification);
        }else if (view == btnConnect){
            listener.connect(this,notification);
        }else if (view == btnRemove){
            listener.delete(this,notification);
        }
    }
}

package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Notification;

public class NFollowHolder extends RecyclerView.ViewHolder {

    private NFollowCell nFollowCell;

    public NFollowHolder(View itemView) {
        super(itemView);
        nFollowCell = (NFollowCell)itemView;
    }

    public void BindView(Activity activity, Notification notification,NListener<NFollowCell,Notification> listener){
        nFollowCell.setActivity(activity);
        nFollowCell.setNotification(notification);
        nFollowCell.setListener(listener);
    }
}

package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class NJobCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private ImageView logo;
    private TextView title,position,date;
    private Notification notification;
    private Feed feed;

    public void setFeed(Feed feed) {
        this.feed = feed;

        setAlbumPhoto(logo,(feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0 ? feed.getFeed().getAlbum().get(0) : null),true);
        position.setText(feed.getFeed().getCategory());
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setListener(NListener<NJobCell, Notification> listener) {
        this.listener = listener;
    }

    private NListener<NJobCell,Notification> listener;

    private void InitUI(Context context){
        inflate(context, R.layout.n_job_cell,this);

        logo = (ImageView)findViewById(R.id.logo);
        title = (TextView)findViewById(R.id.title);
        position = (TextView)findViewById(R.id.position);
        date = (TextView)findViewById(R.id.date);

        this.setOnClickListener(this);
    }

    public NJobCell(Context context) {
        super(context);
        InitUI(context);
    }

    public NJobCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public NJobCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public NJobCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
        if (notification.isSeen()){
            //            this.setBackgroundColor(Color.parseColor("#64bec5f7"));
            this.setBackgroundColor(Color.WHITE);
        }else {
            this.setBackgroundColor(Color.WHITE);
        }
        title.setText(notification.getBody());
        date.setText(notification.getTimeline());
        setFeed(notification.getFeed());
    }

    @Override
    public void onClick(View view) {
        if (view == this){
            listener.select(this,notification);
        }
    }
}

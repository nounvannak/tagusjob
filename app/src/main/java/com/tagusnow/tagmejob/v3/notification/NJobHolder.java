package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Notification;

public class NJobHolder extends RecyclerView.ViewHolder {

    private NJobCell nJobCell;

    public NJobHolder(View itemView) {
        super(itemView);

        nJobCell = (NJobCell)itemView;
    }

    public void BindView(Activity activity, Notification notification, NListener<NJobCell,Notification> listener){
        nJobCell.setActivity(activity);
        nJobCell.setNotification(notification);
        nJobCell.setListener(listener);
    }
}

package com.tagusnow.tagmejob.v3.notification;

import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface NListener<V extends TagUsJobRelativeLayout,O extends Object> {
    void connect(V view,O user);
    void reply(V view,O conversation);
    void select(V view,O notification);
    void delete(V view,O notification);
}

package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class NMessageCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private CircleImageView logo;
    private TextView title,date;
    private Button btnReply,btnFollow,btnRemove;
    private Notification notification;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
        if (notification.isSeen()){
            //            this.setBackgroundColor(Color.parseColor("#64bec5f7"));
            this.setBackgroundColor(Color.WHITE);
        }else {
            this.setBackgroundColor(Color.WHITE);
        }
        if (notification.getRequest_user().isIs_follow()){
            btnRemove.setVisibility(GONE);
            btnFollow.setVisibility(GONE);
        }else {
            btnFollow.setVisibility(VISIBLE);
            btnRemove.setVisibility(VISIBLE);
        }
        setUserProfile(logo,notification.getTarget_user());
        title.setText(notification.getBody());
        date.setText(notification.getTimeline());
    }

    public void setListener(NListener<NMessageCell, Notification> listener) {
        this.listener = listener;
    }

    private NListener<NMessageCell,Notification> listener;

    private void InitUI(Context context){
        inflate(context, R.layout.n_message_cell,this);

        logo = (CircleImageView)findViewById(R.id.logo);
        title = (TextView)findViewById(R.id.title);
        date = (TextView)findViewById(R.id.date);
        btnFollow = (Button)findViewById(R.id.btnFollow);
        btnReply = (Button)findViewById(R.id.btnReply);
        btnRemove = (Button)findViewById(R.id.btnRemove);

        this.setOnClickListener(this);
        btnRemove.setOnClickListener(this);
        btnReply.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
    }

    public NMessageCell(Context context) {
        super(context);
        InitUI(context);
    }

    public NMessageCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public NMessageCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public NMessageCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == this){
            listener.select(this,notification);
        }else if (view == btnFollow){
            listener.connect(this,notification);
        }else if (view == btnRemove){
            listener.delete(this,notification);
        }else if (view == btnReply){
            listener.reply(this,notification);
        }
    }
}

package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Notification;

public class NMessageHolder extends RecyclerView.ViewHolder {

    private NMessageCell nMessageCell;

    public NMessageHolder(View itemView) {
        super(itemView);
        nMessageCell = (NMessageCell)itemView;
    }

    public void BindView(Activity activity, Notification notification,NListener<NMessageCell,Notification> listener) {
        nMessageCell.setActivity(activity);
        nMessageCell.setNotification(notification);
        nMessageCell.setListener(listener);
    }
}

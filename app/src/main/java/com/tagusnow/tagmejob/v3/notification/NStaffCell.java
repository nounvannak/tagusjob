package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class NStaffCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private CircleImageView logo1,logo2,logo3,logo4,logo5;
    private TextView title,counter;
    private Notification notification;

    public void setNotification(Notification notification) {
        this.notification = notification;
        if (notification.isSeen()){
            //            this.setBackgroundColor(Color.parseColor("#64bec5f7"));
            this.setBackgroundColor(Color.WHITE);
        }else {
            this.setBackgroundColor(Color.WHITE);
        }
        title.setText(notification.getBody());
        if (notification.getFeedResponse().getTotal() >= 5){
            List<Feed> feeds = notification.getFeedResponse().getData();
            setUserProfile(logo1,feeds.get(0).getUser_post());
            setUserProfile(logo2,feeds.get(1).getUser_post());
            setUserProfile(logo3,feeds.get(2).getUser_post());
            setUserProfile(logo4,feeds.get(3).getUser_post());
            setUserProfile(logo5,feeds.get(4).getUser_post());

            if (notification.getFeedResponse().getTotal() > 5){
                counter.setText("+" + (notification.getFeedResponse().getTotal() - 5));
            }else {
                counter.setVisibility(GONE);
            }
        }
    }

    public void setListener(NListener<NStaffCell, Notification> listener) {
        this.listener = listener;
    }

    private NListener<NStaffCell,Notification> listener;

    private void InitUI(Context context){
        inflate(context, R.layout.n_staff_cell,this);

        logo1 = (CircleImageView)findViewById(R.id.logo1);
        logo2 = (CircleImageView)findViewById(R.id.logo2);
        logo3 = (CircleImageView)findViewById(R.id.logo3);
        logo4 = (CircleImageView)findViewById(R.id.logo4);
        logo5 = (CircleImageView)findViewById(R.id.logo5);

        title = (TextView)findViewById(R.id.title);
        counter = (TextView)findViewById(R.id.counter);

        this.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public NStaffCell(Context context) {
        super(context);
        InitUI(context);
    }

    public NStaffCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public NStaffCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public NStaffCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == this){
            listener.select(this,notification);
        }
    }
}

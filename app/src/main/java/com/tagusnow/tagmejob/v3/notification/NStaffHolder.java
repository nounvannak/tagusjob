package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Notification;

public class NStaffHolder extends RecyclerView.ViewHolder {

    private NStaffCell nStaffCell;

    public NStaffHolder(View itemView) {
        super(itemView);
        nStaffCell = (NStaffCell)itemView;
    }

    public void BindView(Activity activity, Notification notification,NListener<NStaffCell,Notification> listener){
        nStaffCell.setActivity(activity);
        nStaffCell.setNotification(notification);
        nStaffCell.setListener(listener);
    }
}

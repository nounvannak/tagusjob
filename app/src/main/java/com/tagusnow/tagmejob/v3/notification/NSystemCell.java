package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class NSystemCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ImageView logo;
    private TextView title,date,body;
    private Notification notification;

    public void setNotification(Notification notification) {
        this.notification = notification;
        if (notification.isSeen()){
            //            this.setBackgroundColor(Color.parseColor("#64bec5f7"));
            this.setBackgroundColor(Color.WHITE);
        }else {
            this.setBackgroundColor(Color.WHITE);
        }
        Glide.with(context).load(R.drawable.logo_tagusjob).fitCenter().into(logo);
        title.setText(notification.getTitle());
        date.setText(notification.getTimeline());
        body.setText(notification.getBody());
    }

    public void setListener(NListener<NSystemCell, Notification> listener) {
        this.listener = listener;
    }

    private NListener<NSystemCell,Notification> listener;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.n_system_cell,this);
        this.context = context;
        logo = (ImageView)findViewById(R.id.logo);

        title = (TextView)findViewById(R.id.title);
        date = (TextView)findViewById(R.id.date);
        body = (TextView)findViewById(R.id.body);

        this.setOnClickListener(this);
    }

    public NSystemCell(Context context) {
        super(context);
        InitUI(context);
    }

    public NSystemCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public NSystemCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public NSystemCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == this){
            listener.select(this,notification);
        }
    }
}

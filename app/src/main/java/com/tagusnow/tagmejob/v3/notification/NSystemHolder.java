package com.tagusnow.tagmejob.v3.notification;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Notification;

public class NSystemHolder extends RecyclerView.ViewHolder {

    private NSystemCell nSystemCell;

    public NSystemHolder(View itemView) {
        super(itemView);
        nSystemCell = (NSystemCell)itemView;
    }

    public void BindView(Activity activity, Notification notification,NListener<NSystemCell,Notification> listener){
        nSystemCell.setActivity(activity);
        nSystemCell.setNotification(notification);
        nSystemCell.setListener(listener);
    }
}

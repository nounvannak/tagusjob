package com.tagusnow.tagmejob.v3.page;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.HomeService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.fragment.TagUsJobFragment;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.feed.FeedJob;
import com.tagusnow.tagmejob.v3.feed.FeedJobListener;
import com.tagusnow.tagmejob.v3.feed.FeedListener;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.v3.like.LikeResultCell;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.v3.page.adapter.HomeAdapter;
import com.tagusnow.tagmejob.v3.page.home.PostFunctionCell;
import com.tagusnow.tagmejob.v3.page.home.PostFunctionCellListener;
import com.tagusnow.tagmejob.v3.page.home.UserSuggestView;
import com.tagusnow.tagmejob.v3.page.home.UserSuggestViewAdapter;
import com.tagusnow.tagmejob.v3.page.home.UserSuggestViewListener;
import com.tagusnow.tagmejob.v3.user.UserCard;
import com.tagusnow.tagmejob.v3.user.UserCardListener;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends TagUsJobFragment implements SwipeRefreshLayout.OnRefreshListener, UserSuggestViewListener, PostFunctionCellListener, GridViewListener, FeedNormalListener, FeedJobListener, FeedCVListener, UserCardListener, FeedListener, LikeResultListener, LoadingListener<LoadingView> {

    private static final String TAG = HomeFragment.class.getSimpleName();
    private static final String ChannelID = UUID.randomUUID().toString();
    private static final int LIMIT = 10;
    private SmUser Auth;
    private FeedResponse feedResponse;
    private HomeAdapter homeAdapter;
    private UserSuggestViewAdapter userSuggestViewAdapter;
    private List<Feed> feeds;
    private List<SmUser> users;
    private SwipeRefreshLayout refresh;
    private UserSuggestView userSuggestView;
    private RecyclerView mRecyclerView;
    private LoadingView loadingView;
    private HomeService homeService = ServiceGenerator.createService(HomeService.class);
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);

    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        initSocket();
    }

    // Callback method
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            refresh.setRefreshing(false);
            fetchFeedData();
        }
    };
    private Callback<List<SmUser>> FetchUserSuggestCallback = new Callback<List<SmUser>>() {
        @Override
        public void onResponse(Call<List<SmUser>> call, Response<List<SmUser>> response) {
            if (response.isSuccessful()){
                fetchUserSuggestData(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<List<SmUser>> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchFeedCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchFeedData(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<FeedResponse> FetchFeedNextCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchFeedNextData(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };

    /* Socket.io */
    Socket socket;
    private Emitter.Listener NEW_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
        }
    };
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalLikes = obj.getInt("count_likes");
                            int userID = obj.getInt("user_id");
                            String channelID = obj.getString("channel_id");
                            String likesResult = obj.getString("like_result");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);

                                    feedDetail.setCount_likes(totalLikes);
                                    feedDetail.setLike_result(likesResult);
                                    feedDetail.setIs_like(Auth.getId() == userID);

                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    homeAdapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);

                                        feedDetail.setCount_likes(totalLikes);
                                        feedDetail.setLike_result(likesResult);
                                        feedDetail.setIs_like(Auth.getId() == userID);

                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        homeAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });

        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalComments = obj.getInt("count_comments");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);
                                    feedDetail.setCount_comments(totalComments);
                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    homeAdapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);
                                        feedDetail.setCount_comments(totalComments);
                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        homeAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                Log.e("update feed",obj.toString());
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        int index = feeds.indexOf(feed);
                                        Feed updateFeed = feeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        feeds.set(index,updateFeed);
                                        homeAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            int index = feeds.indexOf(feed);
                                            Feed updateFeed = feeds.get(index);
                                            updateFeed.setFeed(feedJson.getFeed());
                                            feeds.set(index,updateFeed);
                                            homeAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        feeds.remove(feed);
                                        homeAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            feeds.remove(feed);
                                            homeAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("user_id");
                            if (Auth.getId() == userID){
                                Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                                if (feedJson != null){
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                        List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                        if (lists.size() > 0){
                                            Feed feed = lists.get(0);
                                            feeds.remove(feed);
                                            homeAdapter.notifyDataSetChanged();
                                        }
                                    }else {
                                        for (Feed feed: feeds){
                                            if (feed.getId() == feedJson.getId()){
                                                feeds.remove(feed);
                                                homeAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener FOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("auth_user");
                            JSONObject json = obj.getJSONObject("user");
                            if (Auth.getId() == userID){
                                if (json != null){
                                    SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                                    SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                                    if (follower != null && following != null){
                                        int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                            List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                            if (lists.size() > 0){
                                                SmUser user = lists.get(0);
                                                int index = users.indexOf(user);
                                                follower.setIs_follow(true);
                                                if (following.getId() == userID){
                                                    users.set(index,follower);
                                                    userSuggestViewAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        }else {
                                            for (SmUser user: users){
                                                if (user.getId() == id){
                                                    int index = users.indexOf(user);
                                                    follower.setIs_follow(true);
                                                    if (following.getId() == userID){
                                                        users.set(index,follower);
                                                        userSuggestViewAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener UNFOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("auth_user");
                            JSONObject json = obj.getJSONObject("user");
                            if (Auth.getId() == userID){
                                if (json != null){
                                    SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                                    SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                                    if (follower != null && following != null){
                                        int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                            List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                            if (lists.size() > 0){
                                                SmUser user = lists.get(0);
                                                int index = users.indexOf(user);
                                                follower.setIs_follow(false);
                                                if (following.getId() == userID){
                                                    users.set(index,follower);
                                                    userSuggestViewAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        }else {
                                            for (SmUser user : users){
                                                if (user.getId() == id){
                                                    int index = users.indexOf(user);
                                                    follower.setIs_follow(false);
                                                    if (following.getId() == userID){
                                                        users.set(index,follower);
                                                        userSuggestViewAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("New Post",NEW_POST);
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.on("Follow User",FOLLOW_USER);
        socket.on("Unfollow User",UNFOLLOW_USER);
        socket.connect();
    }
    /* End Socket.io */

    private void initTemp(){
        Auth = new Auth(getContext()).checkAuth().token();
    }

    private void initView(View view){
        initSwipeRefresh(view);
        initRecyclerView(view);
    }

    private void fetchFeedData(){
        feedService.Home(Auth.getId(),LIMIT).enqueue(FetchFeedCallback);
    }

    private void fetchFeedData(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        feeds = feedResponse.getData();

        checkNextFeed();

        homeAdapter.NewData(feeds);
        mRecyclerView.setAdapter(homeAdapter);
        loadingView.setLoading(feedResponse.getTotal() > feedResponse.getTo());

        fetchUserSuggestData();
    }

    private void fetchFeedNextData(){
        boolean isNext = feedResponse.getCurrent_page() != feedResponse.getLast_page();
        if (isNext){
            int page = feedResponse.getCurrent_page() + 1;
            feedService.Home(Auth.getId(),LIMIT,page).enqueue(FetchFeedNextCallback);
        }

    }

    private void fetchFeedNextData(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        feeds.addAll(feedResponse.getData());
        checkNextFeed();
        loadingView.setLoading(feedResponse.getTotal() > feedResponse.getTo());
        homeAdapter.NextData(feedResponse.getData());
        homeAdapter.notifyDataSetChanged();

    }

    private void fetchUserSuggestData(){
        homeService.listFollowSuggest(Auth.getId(),20).enqueue(FetchUserSuggestCallback);
    }

    private void fetchUserSuggestData(List<SmUser> users){
        this.users = users;
        userSuggestViewAdapter = new UserSuggestViewAdapter(activity,this,users);
        if (userSuggestView != null){
            userSuggestView.setAdapter(userSuggestViewAdapter);
        }else {
            userSuggestView = homeAdapter.getUserSuggestView();
            userSuggestView.setAdapter(userSuggestViewAdapter);
        }
    }

    private void checkNextFeed(){
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    fetchFeedNextData();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSwipeRefresh(View view){
        refresh = (SwipeRefreshLayout)view.findViewById(R.id.rllFeedBody);
        refresh.setOnRefreshListener(this);
    }

    private void initRecyclerView(View view){
        mRecyclerView = (RecyclerView)view.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.activity);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    public void scrollToTop(){
        if (mRecyclerView!=null)
            mRecyclerView.smoothScrollToPosition(0);
    }

    public HomeFragment() {}

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivity(getActivity());
        initSocket();
        this.initTemp();
        homeAdapter = new HomeAdapter(activity,Auth,this);
        homeAdapter.setFeedCVListener(this);
        homeAdapter.setFeedJobListener(this);
        homeAdapter.setFeedNormalListener(this);
        homeAdapter.setGridViewListener(this);
        homeAdapter.setPostFunctionCellListener(this);
        homeAdapter.setUserSuggestViewListener(this);
        userSuggestView = homeAdapter.getUserSuggestView();
        loadingView = homeAdapter.getLoadingView();
        fetchFeedData();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home2, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onRefresh() {
        refresh.setRefreshing(true);
        new Handler().postDelayed(mRun, 2000);
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void viewMore(UserSuggestView view) {
        view.OpenFriend(Auth);
    }

    @Override
    public void normal(PostFunctionCell view) {
        view.PostNormal();
    }

    @Override
    public void job(PostFunctionCell view) {
        view.PostJob(Auth);
    }

    @Override
    public void resume(PostFunctionCell view) {
        view.PostResume(Auth);
    }

    @Override
    public void viewProfile(PostFunctionCell view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void showGallery(List<Album> albums, int index) {
        ArrayList<String> images = new ArrayList<>();
        for (Album album: albums){
            images.add(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"original"));
        }
        ((TagUsJobActivity) activity).DisplayImage(images,index);
    }

    @Override
    public void onLike(TagUsJobRelativeLayout view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    @Override
    public void onComment(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onShare(TagUsJobRelativeLayout view, Feed feed) {
        view.ShareFeed(feed);
    }

    @Override
    public void onMore(TagUsJobRelativeLayout view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void showDetail(TagUsJobRelativeLayout view, Feed feed) {
        view.DetailFeedNormal(feed);
    }

    @Override
    public void viewProfile(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(TagUsJobRelativeLayout view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void openWebsite(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenWebsite(feed.getFeed().getSource_url());
    }

    @Override
    public void onLike(FeedJob view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    @Override
    public void onComment(FeedJob view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onShare(FeedJob view, Feed feed) {
        view.ShareFeed(feed);
    }

    @Override
    public void onMore(FeedJob view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void onDetail(FeedJob view, Feed feed) {
        view.DetailFeedJob(Auth,feed);
    }

    @Override
    public void onApplyNow(FeedJob view, Feed feed) {
        view.ApplyJob(feed);
    }

    @Override
    public void showProfile(FeedJob view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(FeedJob view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void onLike(FeedCV view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    private void LikeFeed(SmUser Auth, Feed feed){
        if (!feed.getFeed().isIs_like()){
            int index = feeds.indexOf(feed);
            if (index >= 0){
                Feed feed1 = feed;
                feed1.getFeed().setIs_like(true);
                feed1.getFeed().setCount_likes(1);
                feed1.getFeed().setLike_result("You liked this post.");
                feeds.set(index,feed1);
                homeAdapter.notifyDataSetChanged();
            }
            feedService.LikeFeed(feed.getId(),Auth.getId(),ChannelID).enqueue(new Callback<SmTagFeed.Like>() {
                @Override
                public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                    if (response.isSuccessful()){
                        Log.d(TAG,response.message());
                    }else {
                        try {
                            Log.e(TAG,response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onComment(FeedCV view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onDownload(FeedCV view, Feed feed) {
        view.DisplayDownloadBox(feed);
    }

    @Override
    public void viewDetail(FeedCV view, Feed feed) {
        view.DetailFeedCV(feed);
    }

    @Override
    public void viewProfile(FeedCV view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(FeedCV view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void onClickedMore(FeedCV view, Feed feed, boolean isShow) {

    }

    @Override
    public void connect(UserCard view, SmUser user) {
        view.DoFollow(Auth,user);
    }

    @Override
    public void viewProfile(UserCard view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void remove(UserCard view, SmUser user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Close User").setMessage("Are you sure to close this user?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                users.remove(user);
                userSuggestViewAdapter.notifyDataSetChanged();
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void saveFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Save Post").setMessage("Are you sure to save this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feedService.SaveFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            int index = feeds.indexOf(feed);
                            FeedDetail temp = feed.getFeed();
                            feed.setIs_save(1);
                            temp.setIs_save(true);
                            feed.setFeed(temp);
                            feeds.set(index,feed);
                            homeAdapter.notifyDataSetChanged();
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void editFeed(SmUser user, Feed feed) {
        ((TagUsJobActivity) activity).EditPostNormal(feed);
    }

    @Override
    public void hideFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Hide Post").setMessage("Are you sure to hide this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feedService.HideFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            feeds.remove(feed);
                            homeAdapter.notifyDataSetChanged();
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void removeFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Remove Post").setMessage("Are you sure to remove this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feedService.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            feeds.remove(feed);
                            homeAdapter.notifyDataSetChanged();
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void select(LikeResultCell cell, SmUser user) {
        cell.OpenProfile(Auth,user);
    }

    @Override
    public void connect(LikeResultCell cell, SmUser user) {
        cell.DoFollow(Auth,user);
    }

    @Override
    public void reload(LoadingView view) {
        fetchFeedNextData();
    }
}

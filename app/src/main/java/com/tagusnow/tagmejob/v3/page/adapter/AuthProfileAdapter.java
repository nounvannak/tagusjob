package com.tagusnow.tagmejob.v3.page.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVHolder;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.feed.FeedEmbedURL;
import com.tagusnow.tagmejob.v3.feed.FeedEmbedURLHolder;
import com.tagusnow.tagmejob.v3.feed.FeedJob;
import com.tagusnow.tagmejob.v3.feed.FeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.FeedJobListener;
import com.tagusnow.tagmejob.v3.feed.FeedNormal;
import com.tagusnow.tagmejob.v3.feed.FeedNormalHolder;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.v3.profile.AuthHeader;
import com.tagusnow.tagmejob.v3.profile.AuthHeaderHolder;
import com.tagusnow.tagmejob.v3.profile.AuthHeaderListener;
import com.tagusnow.tagmejob.v3.profile.UserFriendCell;
import com.tagusnow.tagmejob.v3.profile.UserFriendCellHolder;
import com.tagusnow.tagmejob.v3.profile.UserFriendListener;
import com.tagusnow.tagmejob.v3.profile.UserInterestCell;
import com.tagusnow.tagmejob.v3.profile.UserInterestCellHolder;
import com.tagusnow.tagmejob.v3.profile.UserInterestListener;
import com.tagusnow.tagmejob.v3.profile.UserPhotoCell;
import com.tagusnow.tagmejob.v3.profile.UserPhotoCellHolder;
import com.tagusnow.tagmejob.v3.profile.UserPhotoListener;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;

import java.util.List;

public class AuthProfileAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<Feed> feeds;
    private SmUser Auth;
    private AuthHeaderListener authHeaderListener;
    private UserInterestListener userInterestListener;
    private UserPhotoListener userPhotoListener;
    private UserFriendListener userFriendListener;
    private FeedNormalListener feedNormalListener;
    private FeedJobListener feedJobListener;
    private FeedCVListener feedCVListener;
    private int other = 5;

    private static final int AUTH_PROFILE_HEADER = 0;
    private static final int USER_PHOTO_VIEW = 1;
    private static final int USER_FRIEND_VIEW = 2;
    private static final int USER_INTEREST_VIEW = 3;
    private static final int NORMAL = 4;
    private static final int EMBED = 8;
    private static final int JOB = 5;
    private static final int RESUME = 6;
    private static final int LOADING = 7;

    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public AuthProfileAdapter(Activity activity,SmUser Auth, AuthHeaderListener authHeaderListener, UserInterestListener userInterestListener, UserPhotoListener userPhotoListener, UserFriendListener userFriendListener, FeedNormalListener feedNormalListener, FeedJobListener feedJobListener, FeedCVListener feedCVListener, GridViewListener gridViewListener,LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.Auth = Auth;

        this.userFriendCell = new UserFriendCell(activity);
        this.userPhotoCell = new UserPhotoCell(activity);
        this.userInterestCell = new UserInterestCell(activity);
        this.loadingView = new LoadingView(activity);

        this.authHeaderListener = authHeaderListener;
        this.userInterestListener = userInterestListener;
        this.userPhotoListener = userPhotoListener;
        this.userFriendListener = userFriendListener;
        this.feedNormalListener = feedNormalListener;
        this.feedJobListener = feedJobListener;
        this.feedCVListener = feedCVListener;
        this.gridViewListener = gridViewListener;
        this.loadingListener = loadingListener;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
    }

    public void NewData(List<Feed> feeds){
        this.feeds = feeds;
    }

    private GridViewListener gridViewListener;
    private UserPhotoCell userPhotoCell;
    private UserFriendCell userFriendCell;

    public UserPhotoCell getUserPhotoCell() {
        return userPhotoCell;
    }

    public UserFriendCell getUserFriendCell() {
        return userFriendCell;
    }

    public UserInterestCell getUserInterestCell() {
        return userInterestCell;
    }

    private UserInterestCell userInterestCell;

    @Override
    public int getItemViewType(int position) {
        int type = 0;

        if (position == 0){
            type = AUTH_PROFILE_HEADER;
        }else if (position == 1){
            type = USER_PHOTO_VIEW;
        }else if (position == 2){
            type = USER_FRIEND_VIEW;
        }else if (position == 3){
            type = USER_INTEREST_VIEW;
        }else if (position == (getItemCount() - 1)){
            type = LOADING;
        }else{
            if (feeds.get(position - (other - 1)).getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)){
                if (feeds.get(position - (other - 1)).getFeed().getType_link() != null && !feeds.get(position - (other - 1)).getFeed().getType_link().equals("")){
                    type = EMBED;
                }else {
                    type = NORMAL;
                }
            }else if (feeds.get(position - (other - 1)).getFeed().getFeed_type().equals(FeedDetail.JOB_POST)){
                type = JOB;
            }else {
                type = RESUME;
            }
        }

        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == AUTH_PROFILE_HEADER){
            return new AuthHeaderHolder(new AuthHeader(activity));
        }else if (viewType == USER_PHOTO_VIEW){
            return new UserPhotoCellHolder(userPhotoCell);
        }else if (viewType == USER_FRIEND_VIEW){
            return new UserFriendCellHolder(userFriendCell);
        }else if (viewType == USER_INTEREST_VIEW){
            return new UserInterestCellHolder(userInterestCell);
        }else if (viewType == NORMAL){
            return new FeedNormalHolder(new FeedNormal(activity));
        }else if (viewType == EMBED){
            return new FeedEmbedURLHolder(new FeedEmbedURL(activity));
        }else if (viewType == JOB){
            return new FeedJobHolder(new FeedJob(activity));
        }else if (viewType == RESUME){
            return new FeedCVHolder(new FeedCV(activity));
        }else {
            return new LoadingHolder(loadingView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == AUTH_PROFILE_HEADER){
            ((AuthHeaderHolder) holder).BindView(activity,Auth,authHeaderListener);
        }else if (getItemViewType(position) == USER_PHOTO_VIEW){
            ((UserPhotoCellHolder) holder).BindView(activity,userPhotoListener);
        }else if (getItemViewType(position) == USER_FRIEND_VIEW){
            ((UserFriendCellHolder) holder).BindView(activity,userFriendListener);
        }else if (getItemViewType(position) == USER_INTEREST_VIEW){
            ((UserInterestCellHolder) holder).BindView(activity);
        }else if (getItemViewType(position) == NORMAL){
            ((FeedNormalHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedNormalListener,gridViewListener);
        }else if (getItemViewType(position) == EMBED){
            ((FeedEmbedURLHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedNormalListener);
        }else if (getItemViewType(position) == JOB){
            ((FeedJobHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedJobListener);
        }else if (getItemViewType(position) == RESUME){
            ((FeedCVHolder) holder).BindView(activity,feeds.get(position - (other - 1)),feedCVListener);
        }else {
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }
    }

    @Override
    public int getItemCount() {
        return feeds.size() + other;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

package com.tagusnow.tagmejob.v3.page.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVHolder;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.feed.FeedEmbedURL;
import com.tagusnow.tagmejob.v3.feed.FeedEmbedURLHolder;
import com.tagusnow.tagmejob.v3.feed.FeedJob;
import com.tagusnow.tagmejob.v3.feed.FeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.FeedJobListener;
import com.tagusnow.tagmejob.v3.feed.FeedNormal;
import com.tagusnow.tagmejob.v3.feed.FeedNormalHolder;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.v3.page.home.PostFunctionCell;
import com.tagusnow.tagmejob.v3.page.home.PostFunctionCellHolder;
import com.tagusnow.tagmejob.v3.page.home.PostFunctionCellListener;
import com.tagusnow.tagmejob.v3.page.home.UserSuggestView;
import com.tagusnow.tagmejob.v3.page.home.UserSuggestViewHolder;
import com.tagusnow.tagmejob.v3.page.home.UserSuggestViewListener;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter {

    private static final int POST_WIDGET = 0;
    private static final int SUGGEST = 1;
    private static final int NORMAL = 2;
    private static final int EMBED = 6;
    private static final int JOB = 3;
    private static final int CV = 4;
    private static final int LOADING = 5;

    private Activity activity;
    private Context context;
    private List<Feed> results;
    private SmUser Auth;
    private int other = 3;

    private FeedNormalListener feedNormalListener;
    private FeedJobListener feedJobListener;
    private FeedCVListener feedCVListener;
    private UserSuggestViewListener userSuggestViewListener;
    private GridViewListener gridViewListener;
    private PostFunctionCellListener postFunctionCellListener;
    private UserSuggestView userSuggestView;
    private LoadingView loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public void setFeedNormalListener(FeedNormalListener feedNormalListener) {
        this.feedNormalListener = feedNormalListener;
    }

    public void setFeedJobListener(FeedJobListener feedJobListener) {
        this.feedJobListener = feedJobListener;
    }

    public void setFeedCVListener(FeedCVListener feedCVListener) {
        this.feedCVListener = feedCVListener;
    }

    public void setUserSuggestViewListener(UserSuggestViewListener userSuggestViewListener) {
        this.userSuggestViewListener = userSuggestViewListener;
    }

    public void setGridViewListener(GridViewListener gridViewListener) {
        this.gridViewListener = gridViewListener;
    }

    public void setPostFunctionCellListener(PostFunctionCellListener postFunctionCellListener) {
        this.postFunctionCellListener = postFunctionCellListener;
    }

    public UserSuggestView getUserSuggestView() {
        return userSuggestView;
    }

    public HomeAdapter(Activity activity,SmUser Auth,LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.context = activity;
        this.userSuggestView = new UserSuggestView(activity);
        this.Auth = Auth;
        this.loadingView = new LoadingView(activity);
        this.loadingListener = loadingListener;
    }

    public void NewData(List<Feed> results){
        this.results = results;
    }

    public void NextData(List<Feed> results){
        this.results.addAll(results);
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if (position==0){
            type = POST_WIDGET;
        }else if (position==1){
            type = SUGGEST;
        }else if (position==(getItemCount() - 1)){
            type = LOADING;
        }else {
            if (results.get(position - (other - 1)).getFeed().getFeed_type().equals(FeedDetail.NORMAL_POST)){
                if (results.get(position - (other - 1)).getFeed().getType_link() != null && !results.get(position - (other - 1)).getFeed().getType_link().equals("")){
                    type = EMBED;
                }else {
                    type = NORMAL;
                }
            }else if (results.get(position - (other - 1)).getFeed().getFeed_type().equals(FeedDetail.JOB_POST)){
                type = JOB;
            }else {
                type = CV;
            }
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == POST_WIDGET){
            return new PostFunctionCellHolder(new PostFunctionCell(context));
        }else if (viewType == SUGGEST){
            return new UserSuggestViewHolder(userSuggestView);
        }else if (viewType == LOADING){
            return new LoadingHolder(loadingView);
        }else if (viewType == NORMAL){
            return new FeedNormalHolder(new FeedNormal(context));
        }else if (viewType == EMBED){
            return new FeedEmbedURLHolder(new FeedEmbedURL(context));
        }else if (viewType == JOB){
            return new FeedJobHolder(new FeedJob(context));
        }else {
            return new FeedCVHolder(new FeedCV(context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == POST_WIDGET){
            ((PostFunctionCellHolder) holder).BindView(activity,Auth,postFunctionCellListener);
        }else if (getItemViewType(position) == SUGGEST){
            ((UserSuggestViewHolder) holder).BindView(activity,userSuggestViewListener);
        }else if (getItemViewType(position) == LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else if (getItemViewType(position) == NORMAL){
            ((FeedNormalHolder) holder).BindView(activity,results.get(position - (other - 1)),feedNormalListener,gridViewListener);
        }else if (getItemViewType(position) == EMBED){
            ((FeedEmbedURLHolder) holder).BindView(activity,results.get(position - (other - 1)),feedNormalListener);
        }else if (getItemViewType(position) == JOB){
            ((FeedJobHolder) holder).BindView(activity,results.get(position - (other - 1)),feedJobListener);
        }else {
            ((FeedCVHolder) holder).BindView(activity,results.get(position - (other - 1)),feedCVListener);
        }
    }

    @Override
    public int getItemCount() {
        return results.size() + other;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

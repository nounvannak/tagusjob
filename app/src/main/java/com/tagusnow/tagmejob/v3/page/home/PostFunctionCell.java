package com.tagusnow.tagmejob.v3.page.home;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostFunctionCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private SmUser user;

    public void setUser(SmUser user) {
        this.user = user;
        setUserProfile(user_profile,user);
    }

    public void setPostFunctionCellListener(PostFunctionCellListener postFunctionCellListener) {
        this.postFunctionCellListener = postFunctionCellListener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private PostFunctionCellListener postFunctionCellListener;
    private CircleImageView user_profile;
    private ImageButton btnPost,btnPostJobs,btnPostCV;

    private void InitUI(Context context){
        inflate(context, R.layout.widget_three_post,this);
        this.context = context;
        user_profile = (CircleImageView)findViewById(R.id.user_profile);
        btnPost = (ImageButton)findViewById(R.id.btnPost);
        btnPostJobs = (ImageButton)findViewById(R.id.btnPostJobs);
        btnPostCV = (ImageButton)findViewById(R.id.btnPostCV);
        user_profile.setOnClickListener(this);
        btnPost.setOnClickListener(this);
        btnPostJobs.setOnClickListener(this);
        btnPostCV.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public PostFunctionCell(Context context) {
        super(context);
        InitUI(context);
    }

    public PostFunctionCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public PostFunctionCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public PostFunctionCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == user_profile){
            postFunctionCellListener.viewProfile(this,user);
        }else if (view == btnPost){
            postFunctionCellListener.normal(this);
        }else if (view == btnPostCV){
            postFunctionCellListener.resume(this);
        }else if (view == btnPostJobs){
            postFunctionCellListener.job(this);
        }else if (this == view){
            postFunctionCellListener.normal(this);
        }
    }
}

package com.tagusnow.tagmejob.v3.page.home;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class PostFunctionCellHolder extends RecyclerView.ViewHolder {

    private PostFunctionCell postFunctionCell;

    public PostFunctionCellHolder(View itemView) {
        super(itemView);
        postFunctionCell = (PostFunctionCell)itemView;
    }

    public void BindView(Activity activity, SmUser user,PostFunctionCellListener postFunctionCellListener){
        postFunctionCell.setActivity(activity);
        postFunctionCell.setUser(user);
        postFunctionCell.setPostFunctionCellListener(postFunctionCellListener);
    }
}

package com.tagusnow.tagmejob.v3.page.home;

import com.tagusnow.tagmejob.auth.SmUser;

public interface PostFunctionCellListener {
    void normal(PostFunctionCell view);
    void job(PostFunctionCell view);
    void resume(PostFunctionCell view);
    void viewProfile(PostFunctionCell view, SmUser user);
}

package com.tagusnow.tagmejob.v3.page.home;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class UserSuggestView extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private Button btnSeeMore;
    private RecyclerView recyclerView;

    public void setAdapter(UserSuggestViewAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    private UserSuggestViewAdapter adapter;

    public void setUserSuggestViewListener(UserSuggestViewListener userSuggestViewListener) {
        this.userSuggestViewListener = userSuggestViewListener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private UserSuggestViewListener userSuggestViewListener;

    private void InitUI(Context context){
        inflate(context, R.layout.follow_suggest_horizontal_layout,this);
        this.context = context;
        btnSeeMore = (Button)findViewById(R.id.btnSeeMore);
        btnSeeMore.setOnClickListener(this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public UserSuggestView(Context context) {
        super(context);
        InitUI(context);
    }

    public UserSuggestView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserSuggestView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserSuggestView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnSeeMore){
            userSuggestViewListener.viewMore(this);
        }
    }
}

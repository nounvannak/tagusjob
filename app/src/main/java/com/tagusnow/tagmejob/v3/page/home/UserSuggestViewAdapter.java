package com.tagusnow.tagmejob.v3.page.home;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.user.UserCard;
import com.tagusnow.tagmejob.v3.user.UserCardHolder;
import com.tagusnow.tagmejob.v3.user.UserCardListener;
import java.util.List;

public class UserSuggestViewAdapter extends RecyclerView.Adapter<UserCardHolder> {

    private Activity activity;
    private UserCardListener userCardListener;
    private List<SmUser> users;

    public UserSuggestViewAdapter(Activity activity, UserCardListener userCardListener, List<SmUser> users) {
        this.activity = activity;
        this.userCardListener = userCardListener;
        this.users = users;
    }

    public void NextData(List<SmUser> users){
        this.users.addAll(users);
    }

    @NonNull
    @Override
    public UserCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserCardHolder(new UserCard(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull UserCardHolder holder, int position) {
        holder.BindView(activity,users.get(position),userCardListener);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}

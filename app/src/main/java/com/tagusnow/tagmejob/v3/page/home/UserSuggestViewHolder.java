package com.tagusnow.tagmejob.v3.page.home;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class UserSuggestViewHolder extends RecyclerView.ViewHolder {

    private UserSuggestView view;

    public UserSuggestViewHolder(View itemView) {
        super(itemView);
        view = (UserSuggestView)itemView;
    }

    public void BindView(Activity activity,UserSuggestViewListener userSuggestViewListener){
        view.setActivity(activity);
        view.setUserSuggestViewListener(userSuggestViewListener);
    }
}

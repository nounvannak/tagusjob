package com.tagusnow.tagmejob.v3.page.home;

public interface UserSuggestViewListener {
    void viewMore(UserSuggestView view);
}

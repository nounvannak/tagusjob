package com.tagusnow.tagmejob.v3.page.job;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class PostFunctionTwoCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private RelativeLayout btnArticle,btnJobs,btnResume;
    private TextView btnPost;

    public void setPostFunctionTwoCellListener(PostFunctionTwoCellListener postFunctionTwoCellListener) {
        this.postFunctionTwoCellListener = postFunctionTwoCellListener;
    }

    private PostFunctionTwoCellListener postFunctionTwoCellListener;

    private void InitUI(Context context){
        this.context = context;
        inflate(context, R.layout.widget_post,this);
        btnArticle = (RelativeLayout)findViewById(R.id.btnArticle);
        btnJobs = (RelativeLayout)findViewById(R.id.btnPostJobs);
        btnResume = (RelativeLayout)findViewById(R.id.btnPostCV);
        btnPost = (TextView)findViewById(R.id.btnPost);
        btnArticle.setOnClickListener(this);
        btnJobs.setOnClickListener(this);
        btnResume.setOnClickListener(this);
        btnPost.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public PostFunctionTwoCell(Context context) {
        super(context);
        InitUI(context);
    }

    public PostFunctionTwoCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public PostFunctionTwoCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public PostFunctionTwoCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnArticle ){
            postFunctionTwoCellListener.normal(this);
        }else if (view == btnPost){
            postFunctionTwoCellListener.normal(this);
        }else if (view == btnJobs){
            postFunctionTwoCellListener.job(this);
        }else if (view == btnResume){
            postFunctionTwoCellListener.resume(this);
        }
    }
}

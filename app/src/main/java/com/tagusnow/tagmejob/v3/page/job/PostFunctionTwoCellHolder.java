package com.tagusnow.tagmejob.v3.page.job;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class PostFunctionTwoCellHolder extends RecyclerView.ViewHolder {

    private PostFunctionTwoCell view;

    public PostFunctionTwoCellHolder(View itemView) {
        super(itemView);
        view = (PostFunctionTwoCell)itemView;
    }

    public void BindView(Activity activity,PostFunctionTwoCellListener postFunctionTwoCellListener){
        view.setActivity(activity);
        view.setPostFunctionTwoCellListener(postFunctionTwoCellListener);
    }
}

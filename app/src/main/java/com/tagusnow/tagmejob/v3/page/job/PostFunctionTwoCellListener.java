package com.tagusnow.tagmejob.v3.page.job;

public interface PostFunctionTwoCellListener {
    void normal(PostFunctionTwoCell view);
    void job(PostFunctionTwoCell view);
    void resume(PostFunctionTwoCell view);
}

package com.tagusnow.tagmejob.v3.page.resume;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.Widget.CompanyFace;
import com.tagusnow.tagmejob.view.Widget.WidgetTopCompany;

public interface CompanyFaceListener {
    void viewStaff(CompanyFace view, Feed feed);
    void viewMore(WidgetTopCompany view);
}

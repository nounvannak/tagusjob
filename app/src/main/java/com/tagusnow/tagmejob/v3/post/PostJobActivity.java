package com.tagusnow.tagmejob.v3.post;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.nbsp.materialfilepicker.utils.FileUtils;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.squareup.picasso.Picasso;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.model.v2.feed.PostJobRequest;
import com.tagusnow.tagmejob.model.v2.feed.Setting;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.v3.post.photo.CameraView;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelect;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectAdapter;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectListener;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostJobActivity extends TagUsJobActivity implements View.OnClickListener, ImageSelectListener, OnMapReadyCallback {

    private static final String TAG = PostJobActivity.class.getSimpleName();
    public static final String IS_EDIT = "PostJobActivity_is_edit";
    public static final String FEED = "PostJobActivity_feed";
    private SmUser Auth;
    private Feed feed;
    private boolean isEdit;
    private Spinner spnCategory, spnCity, spnLevel, spnWorkingTime;
    private EditText title, phone, email, salary, close_date, website, numberOfEmployee, description, txtRequirement, txtLocation,companyName;
    private View error_company,error_position, error_title, error_salary, error_close_date, error_phone, error_email, error_numberOfEmployee, error_city, error_level, error_workingTime;
    private RecyclerView recyclerImg;
    private ImageButton btnCurrentLocation,btnPinLocation,btnShareLocation;
    private Toolbar toolbar;
    private ImageSelectAdapter adapter;
    private List<String> categoryList = new ArrayList<String>();
    private List<String> cityList = new ArrayList<String>();
    private List<String> levelList = new ArrayList<String>();
    private List<String> workTimeList = new ArrayList<String>();
    private List<String> images = new ArrayList<>();
    private SmTagSubCategory category;
    private SmLocation location;
    private int category_id;
    private int city;
    private PostJobRequest request;
    private SupportMapFragment maps;
    private String level, workingTime;
    private String close_dateStr;
    private LocationManager mLocationManager;
    private double lat = 0,lng = 0;
    private Calendar closeDateCal = Calendar.getInstance();
    /*Service*/
    private FeedService service = ServiceGenerator.createService(FeedService.class);

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            closeDateCal.set(Calendar.YEAR, i);
            closeDateCal.set(Calendar.MONTH, i1);
            closeDateCal.set(Calendar.DAY_OF_MONTH, i2);
            setupDate();
        }
    };
    private static final float LOCATION_REFRESH_DISTANCE = 1f;
    private static final long LOCATION_REFRESH_TIME = 2000;
    private Callback<Feed> SaveCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            dismissProgress();
            if (response.isSuccessful()){
                onBackPressed();
            }else {
                try {
                    ShowAlert(response.message(),GotErrorMsg(response.errorBody().string()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            dismissProgress();
            t.printStackTrace();
        }
    };
    private Callback<Feed> UpdateCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            dismissProgress();
            if (response.isSuccessful()){
                onBackPressed();
            }else {
                try {
                    ShowAlert(response.message(),GotErrorMsg(response.errorBody().string()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            dismissProgress();
            t.printStackTrace();
        }
    };

    public static Intent createIntent(Activity activity) {
        return new Intent(activity, PostJobActivity.class);
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            lat = location.getLatitude();
            lng = location.getLongitude();
            maps.getMapAsync(PostJobActivity.this);
            Log.e("lat",String.valueOf(location.getLatitude()));
            Log.e("lng",String.valueOf(location.getLongitude()));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_job2);

        initTemp();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Current Location
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, mLocationListener);
        // Google Maps
        maps = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        spnCategory = (Spinner)findViewById(R.id.spnCategory);
        spnCity = (Spinner)findViewById(R.id.spnCity);
        spnLevel = (Spinner)findViewById(R.id.spnLevel);
        spnWorkingTime = (Spinner)findViewById(R.id.spnWorkingTime);
        companyName = (EditText)findViewById(R.id.companyName);
        title = (EditText)findViewById(R.id.title);
        phone = (EditText)findViewById(R.id.phone);
        email = (EditText)findViewById(R.id.email);
        salary = (EditText)findViewById(R.id.salary);
        close_date = (EditText)findViewById(R.id.close_date);
        website = (EditText)findViewById(R.id.website);
        txtLocation = (EditText)findViewById(R.id.location);
        numberOfEmployee = (EditText)findViewById(R.id.numberOfEmployee);
        description = (EditText)findViewById(R.id.description);
        txtRequirement = (EditText)findViewById(R.id.txtRequirement);
        btnCurrentLocation = (ImageButton)findViewById(R.id.btnCurrentLocation);
        btnPinLocation = (ImageButton)findViewById(R.id.btnPinLocation);
        btnShareLocation = (ImageButton)findViewById(R.id.btnShareLocation);
        btnCurrentLocation.setOnClickListener(this);
        btnPinLocation.setOnClickListener(this);
        btnShareLocation.setOnClickListener(this);
        close_date.setOnClickListener(this);
        txtLocation.setOnClickListener(this);
        spnLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                level = levelList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                level = null;
            }
        });
        spnWorkingTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                workingTime = workTimeList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                workingTime = null;
            }
        });
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("select category",category.getData().get(i).getTitle());
                category_id = category.getData().get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                category_id = 0;
            }
        });
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                city = location.getData().get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                city = 0;
            }
        });

        error_company = (View)findViewById(R.id.error_company);
        error_position = (View)findViewById(R.id.error_position);
        error_title = (View)findViewById(R.id.error_title);
        error_salary = (View)findViewById(R.id.error_salary);
        error_close_date = (View)findViewById(R.id.error_close_date);
        error_phone = (View)findViewById(R.id.error_phone);
        error_email = (View)findViewById(R.id.error_email);
        error_numberOfEmployee = (View)findViewById(R.id.error_numberOfEmployee);
        error_city = (View)findViewById(R.id.error_city);
        error_level = (View)findViewById(R.id.error_level);
        error_workingTime = (View)findViewById(R.id.error_workingTime);

        recyclerImg = (RecyclerView)findViewById(R.id.recyclerImg);
        recyclerImg.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerImg.setLayoutManager(layoutManager);
        adapter = new ImageSelectAdapter(this,true,images,this);
        recyclerImg.setAdapter(adapter);
        initUserInfo();
        setupDate();
        setupView();
        if (isEdit){InitEdit();}
    }

    private void InitEdit(){

        companyName.setText(feed.getFeed().getTitle_name());
        title.setText(feed.getFeed().getTitle());
        salary.setText(feed.getFeed().getSalary());
        phone.setText(feed.getFeed().getPhone());
        email.setText(feed.getFeed().getEmail());
        numberOfEmployee.setText(feed.getFeed().getNum_employee());
        description.setText(feed.getFeed().getOriginalHastTagText() != null ? feed.getFeed().getOriginalHastTagText() : "");
        txtRequirement.setText(feed.getFeed().getRequirement() != null ? feed.getFeed().getRequirement() : "");

        category_id = feed.getFeed().getCategory_id();
        city = feed.getFeed().getCity();
        level = feed.getFeed().getLevel();
        workingTime = feed.getFeed().getWorking_time();
        lat = feed.getFeed().getLat() != null ? feed.getFeed().getLat() : 0;
        lng = feed.getFeed().getLng() != null ? feed.getFeed().getLng() : 0;

        spnCategory.setSelection(categoryList.indexOf(feed.getFeed().getCategory()));
        spnCity.setSelection(cityList.indexOf(feed.getFeed().getCity_name()));
        spnLevel.setSelection(levelList.indexOf(feed.getFeed().getLevel()));
        spnWorkingTime.setSelection(workTimeList.indexOf(feed.getFeed().getWorking_time()));

        if (feed.getFeed().getWebsite() != null && !feed.getFeed().getWebsite().equals("")){
            website.setText(feed.getFeed().getWebsite());
        }

        if (feed.getFeed().getLocation_des() != null && !feed.getFeed().getLocation_des().equals("")){
            txtLocation.setText(feed.getFeed().getLocation_des());
        }

        if (feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0){
            for (Album album: feed.getFeed().getAlbum()){
                images.add(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"original"));
            }

            adapter.append(images);
        }
    }

    private void initTemp(){
        this.Auth = new Auth(this).checkAuth().token();
        this.category = new Repository(this).restore().getCategory();
        this.location = new Repository(this).restore().getLocation();
        if (this.category.getData()!=null){
            for (int i=1;i <= this.category.getData().size();i++){
                this.categoryList.add(this.category.getData().get(i-1).getTitle());
            }
        }

        if (this.location.getData()!=null){
            for (int i=1;i <= this.location.getData().size();i++){
                this.cityList.add(this.location.getData().get(i-1).getName());
            }
        }

        Setting levelObj = new Repository(this).restore().getLevel();
        Setting workTimeObj = new Repository(this).restore().getWorkingTime();
        if (levelObj != null){
            levelList.addAll(Arrays.asList(levelObj.getValue().split(",")));
            Log.e(TAG, levelList.toString());
        }

        if (workTimeObj != null){
            workTimeList.addAll(Arrays.asList(workTimeObj.getValue().split(",")));
        }

        isEdit = getIntent().getBooleanExtra(IS_EDIT,false);

        if (isEdit){
            feed = new Gson().fromJson(getIntent().getStringExtra(FEED),Feed.class);
        }
    }

    private void initUserInfo(){
        if (Auth.getEmail()!=null){
            email.setText(Auth.getEmail());
        }

        setName(companyName,Auth);

        if (Auth.getPhone()!=null){
            phone.setText(Auth.getPhone());
        }
    }

    private void initDatePicker(){
        if (this.closeDateCal==null){
            this.closeDateCal = Calendar.getInstance();
        }
        new DatePickerDialog(this, dateSetListener, this.closeDateCal.get(Calendar.YEAR), this.closeDateCal.get(Calendar.MONTH), this.closeDateCal.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setupView(){
        if (this.categoryList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,this.categoryList);
            spnCategory.setAdapter(adapter);
        }

        if (this.cityList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,this.cityList);
            spnCity.setAdapter(adapter);
        }

        if (this.levelList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,this.levelList);
            spnLevel.setAdapter(adapter);
        }else {
            Log.e(TAG,"Level list is null");
        }

        if (this.workTimeList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,this.workTimeList);
            spnWorkingTime.setAdapter(adapter);
        }else {
            Log.e(TAG, "Work time list is null");
        }

        setupDate();
    }

    private void setupDate(){
        if (this.closeDateCal!=null){
            String date = this.closeDateCal.get(Calendar.DAY_OF_MONTH)+"/"+((this.closeDateCal.get(Calendar.MONTH) + 1) > 9 ? (this.closeDateCal.get(Calendar.MONTH) + 1) : "0"+(this.closeDateCal.get(Calendar.MONTH) + 1))+"/"+this.closeDateCal.get(Calendar.YEAR);
            this.close_dateStr = this.closeDateCal.get(Calendar.YEAR)+"-"+((this.closeDateCal.get(Calendar.MONTH) + 1) > 9 ? (this.closeDateCal.get(Calendar.MONTH) + 1) : "0"+(this.closeDateCal.get(Calendar.MONTH) + 1))+"-"+this.closeDateCal.get(Calendar.DAY_OF_MONTH);
            close_date.setText(date);
        }
    }

    public boolean isValid(){
        boolean valid = true;
        this.request = new PostJobRequest();
        this.request.setJob_type(category_id);
        this.request.setJob_city(city);
        this.request.setAuth_user(Auth.getId());

        if (this.category_id > 0){
            error_position.setBackgroundColor(Color.GRAY);
        }else {
            error_position.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (this.city > 0){
            error_city.setBackgroundColor(Color.GRAY);
        }else {
            error_city.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (this.level != null){
            error_level.setBackgroundColor(Color.GRAY);
        }else {
            error_level.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (this.workingTime != null){
            error_workingTime.setBackgroundColor(Color.GRAY);
        }else {
            error_workingTime.setBackgroundColor(Color.RED);
            valid = false;
        }

        /*Title*/
        if (this.companyName.getText().length() > 0){
            error_company.setBackgroundColor(Color.GRAY);
        }else {
            error_company.setBackgroundColor(Color.RED);
            valid = false;
        }

        /*Title*/
        if (this.title.getText().length() > 0){
            error_title.setBackgroundColor(Color.GRAY);
            this.request.setJob_title(title.getText().toString());
        }else {
            error_title.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*Salary*/
        if (this.salary.getText().length() > 0){
            error_salary.setBackgroundColor(Color.GRAY);
            this.request.setJob_salary(salary.getText().toString());
        }else {
            error_salary.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*close_date*/
        if (this.close_date.getText().length() > 0){
            error_close_date.setBackgroundColor(Color.GRAY);
            this.request.setJob_close_date(close_dateStr);
        }else {
            error_close_date.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*phone*/
        if (this.phone.getText().length() > 0){
            error_phone.setBackgroundColor(Color.GRAY);
            this.request.setJob_phone(phone.getText().toString());
        }else {
            error_phone.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*email*/
        if (this.email.getText().length() > 0){
            error_email.setBackgroundColor(Color.GRAY);
            this.request.setJob_email(email.getText().toString());
        }else {
            error_email.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*numberOfEmployee*/
        if (this.numberOfEmployee.getText().length() > 0){
            error_numberOfEmployee.setBackgroundColor(Color.GRAY);
            this.request.setNum_employee(this.numberOfEmployee.getText().toString());
        }else {
            error_numberOfEmployee.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (description.getText().length() > 0){
            this.request.setJob_desc(description.getText().toString());
            description.setHintTextColor(getResources().getColor(R.color.light_gray));
        }else {
            description.setHintTextColor(getResources().getColor(R.color.pdlg_color_red));
            valid = false;
        }

        if (txtRequirement.getText().length() > 0){
            this.request.setRequirement(txtRequirement.getText().toString());
            txtRequirement.setHintTextColor(getResources().getColor(R.color.light_gray));
        }else {
            txtRequirement.setHintTextColor(getResources().getColor(R.color.pdlg_color_red));
            valid = false;
        }

        return valid;
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void OpenImagePicker(){
        AndroidImagePicker.getInstance().pickMulti(this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                InitImage(items);
            }
        });
    }

    private void InitImage(List<ImageItem> items){
        if(items != null && items.size() > 0){
            for (ImageItem item : items){
                images.add(item.path);
            }

            adapter.append(images);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            AlertDialog.Builder builder = new AlertDialog.Builder(PostJobActivity.this);
            builder.setTitle("Permission Notice").setMessage("Sorry you can't use camera & gallery. Please allow permission first.").setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onBackPressed();
                }
            }).create().show();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.btnPost){
            OnPost();
        }
        return super.onOptionsItemSelected(item);
    }

    private void Save(){
        List<MultipartBody.Part> requestBodies = new ArrayList<>();
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("type",String.valueOf(category_id)));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("city",String.valueOf(city)));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("auth_user",String.valueOf(Auth.getId())));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("title",title.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("company",companyName.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("email",email.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("salary",salary.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("close_date",close_dateStr));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("phone",phone.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("num_employee",numberOfEmployee.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("desc",description.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("requirement",txtRequirement.getText().toString()));

        if (txtLocation.getText().toString().length() > 0){
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("location",txtLocation.getText().toString()));
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("lat",String.valueOf(lat)));
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("lng",String.valueOf(lng)));
        }

        if (website.getText().toString().length() > 0){
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("website",website.getText().toString()));
        }

        if (level != null){
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("level",level));
        }

        if (workingTime != null){
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("work_time",workingTime));
        }

        if (images != null && images.size() > 0){
            for (String value: images){
                Uri uri = Uri.parse(value);
                requestBodies.add(prepareFilePart("job_upload_file[]",new File(uri.getPath())));
            }
        }

        service.PostJob(requestBodies).enqueue(SaveCallback);

    }

    private void Update(){
        List<MultipartBody.Part> requestBodies = new ArrayList<>();
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("type",String.valueOf(category_id)));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("city",String.valueOf(city)));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("auth_user",String.valueOf(Auth.getId())));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("title",title.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("company",companyName.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("email",email.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("salary",salary.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("close_date",close_dateStr));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("phone",phone.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("num_employee",numberOfEmployee.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("desc",description.getText().toString()));
        requestBodies.add(okhttp3.MultipartBody.Part.createFormData("requirement",txtRequirement.getText().toString()));

        if (txtLocation.getText().toString().length() > 0){
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("location",txtLocation.getText().toString()));
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("lat",String.valueOf(lat)));
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("lng",String.valueOf(lng)));
        }

        if (website.getText().toString().length() > 0){
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("website",website.getText().toString()));
        }

        if (level != null){
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("level",level));
        }

        if (workingTime != null){
            requestBodies.add(okhttp3.MultipartBody.Part.createFormData("work_time",workingTime));
        }

        if (images != null && images.size() > 0){
            for (String value: images){
                if (value.contains("http")){
                    Uri uri = SuperUtil.getImageUri(this,SuperUtil.getBitmap(value));
                    File file = new File(uri.getPath());
                    try {
                        requestBodies.add(prepareFilePart("job_upload_file[]",File.createTempFile(file.getAbsolutePath(),".png")));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    Uri uri = Uri.parse(value);
                    File file = new File(uri.getPath());
                    requestBodies.add(prepareFilePart("job_upload_file[]",file));
                }
            }
        }

        service.UpdateJob(feed.getId(),requestBodies).enqueue(UpdateCallback);
    }

    private void OnPost(){

        if (isValid()){
            if (isEdit){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Update").setMessage("Do you want to update this job?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showProgress();
                        Update();
                    }
                }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Save").setMessage("Do you want to create this job?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showProgress();
                        Save();
                    }
                }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v==close_date){
            initDatePicker();
        }else if (v==btnPinLocation){

        }else if (v==btnCurrentLocation){

        }else if (v==btnShareLocation){

        }
    }

    @Override
    public void remove(ImageSelect view, int position) {
        images.remove(position);
        adapter.remove(images);
    }

    @Override
    public void camera(CameraView view) {
        OpenImagePicker();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title("Your place"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12));
    }
}

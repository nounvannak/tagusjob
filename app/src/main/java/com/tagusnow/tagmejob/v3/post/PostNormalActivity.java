package com.tagusnow.tagmejob.v3.post;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.feed.embed.LinkSourceContent;
import com.tagusnow.tagmejob.v3.feed.embed.LinkViewCallback;
import com.tagusnow.tagmejob.v3.feed.embed.TextCrawler;
import com.tagusnow.tagmejob.v3.feed.embed.URLLinkView;
import com.tagusnow.tagmejob.v3.post.EditText.GoEditText;
import com.tagusnow.tagmejob.v3.post.EditText.GoEditTextListenter;
import com.tagusnow.tagmejob.v3.post.photo.CameraView;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelect;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectAdapter;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectListener;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.whalemare.sheetmenu.SheetMenu;

public class PostNormalActivity extends TagUsJobActivity implements View.OnClickListener, ImageSelectListener, View.OnKeyListener, GoEditTextListenter {

    private static final String TAG = PostNormalActivity.class.getSimpleName();
    private static final String HTTPS = "https://";
    private static final String HTTP = "http://";
    private static final String WWW = "www.";
    public static final String FEED = "PostNormalActivity_feed";
    public static final String IS_EDIT = "PostNormalActivity_is_edit";
    private Toolbar toolbar;
    private GoEditText txtContent;
    private RecyclerView recyclerView;
    private RelativeLayout layoutAction,layoutPrivacy;
    private CircleImageView profile;
    private TextView name;
    private ImageView icon;
    private TextView privacyText;
    private boolean isEdit = false;
    private int privacy = 0;
    private ImageSelectAdapter adapter;
    private List<String> images = new ArrayList<>();
    private Feed feed;
    private SmUser Auth;
    private String firstURL;
    ///////// Embed URL //////////
    private TextCrawler textCrawler;
    private URLLinkView embedView;
    private RelativeLayout layoutEmbed;
    private ImageButton btnCloseEmbed;
    private boolean isEmbed = false;
    private LinkSourceContent content;

    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<Feed> SaveCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            dismissProgress();
            if (response.isSuccessful()){
                onBackPressed();
            }else {
                try {
                    ShowAlert(response.message(),GotErrorMsg(response.errorBody().string()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            dismissProgress();
            t.printStackTrace();
        }
    };
    private Callback<Feed> UpdateCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            dismissProgress();
            if (response.isSuccessful()){
                Log.e(TAG,new Gson().toJson(response.body()));
                onBackPressed();
            }else {
                try {
                    ShowAlert(response.message(),GotErrorMsg(response.errorBody().string()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            dismissProgress();
            t.printStackTrace();
        }
    };
    private LinkViewCallback callback = new LinkViewCallback() {
        @Override
        public void onBeforeLoading() {
            isEmbed(false);
        }

        @Override
        public void onAfterLoading(LinkSourceContent linkSourceContent, boolean isNull) {
            Log.e(TAG,String.valueOf(isNull));
            Log.e(TAG,new Gson().toJson(linkSourceContent));
            if (!isNull){
                isEmbed(true);
                content = linkSourceContent;
                embedView.setLinkSourceContent(linkSourceContent);
            }
        }
    };

    public static Intent createIntent(Activity activity){
        return new Intent(activity,PostNormalActivity.class);
    }

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();
        isEdit = getIntent().getBooleanExtra(IS_EDIT,false);
        if (isEdit){
            feed = new Gson().fromJson(getIntent().getStringExtra(FEED),Feed.class);
        }
    }

    private void InitEdit(){
        txtContent.setText(feed.getFeed().getOriginalHastTagText());
        ChangePrivacy(feed.getPrivacy());
        if (feed.getFeed().getType_link() != null && !feed.getFeed().getType_link().equals("")){
            makeEmbed(feed.getFeed().getSource_url());
        }
        if (feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0){
            for (Album album : feed.getFeed().getAlbum()){
                images.add(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"original"));
            }

            adapter = new ImageSelectAdapter(this,false,images,this);
            recyclerView.setAdapter(adapter);
        }
    }

    private void ChangePrivacy(int privacy){
        this.privacy = privacy;
        if (privacy == 0){
            icon.setBackgroundResource(R.drawable.world_black);
            privacyText.setText(R.string.publics);
        }else {
            icon.setBackgroundResource(R.drawable.ic_lock_outline_light_blue_24dp);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    icon.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.black)));
                }
            }
            privacyText.setText(R.string.privates);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_normal);

        InitTemp();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtContent = (GoEditText)findViewById(R.id.txtContent);
        txtContent.setOnKeyListener(this);
        txtContent.addListener(this);

        name = (TextView)findViewById(R.id.name);
        layoutAction = (RelativeLayout)findViewById(R.id.layoutAction);
        layoutPrivacy = (RelativeLayout)findViewById(R.id.layoutPrivacy);
        profile = (CircleImageView)findViewById(R.id.profile);
        icon = (ImageView)findViewById(R.id.icon);
        privacyText = (TextView)findViewById(R.id.privacy);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        layoutAction.setOnClickListener(this);
        layoutPrivacy.setOnClickListener(this);

        //////// Embed URL ///////
        textCrawler = new TextCrawler();
        layoutEmbed = (RelativeLayout)findViewById(R.id.layoutEmbed);
        btnCloseEmbed = (ImageButton)findViewById(R.id.btnCloseEmbed);
        btnCloseEmbed.setOnClickListener(this);
        embedView = (URLLinkView)findViewById(R.id.embedView);
        embedView.setActivity(this);
        isEmbed(false);

        if (isEdit){InitEdit();}
        setUserProfile(profile,Auth);
        setName(name,Auth);
        ChangePrivacy(privacy);

    }

    private void isEmbed(boolean embed){
        this.isEmbed = embed;
        if (embed){
            layoutEmbed.setVisibility(View.VISIBLE);
        }else {
            layoutEmbed.setVisibility(View.GONE);
        }
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void OpenImagePicker(){
        AndroidImagePicker.getInstance().pickMulti(this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                InitImage(items);
            }
        });
    }

    private void InitImage(List<ImageItem> items){
        if(items != null && items.size() > 0){
            for (ImageItem item : items){
                images.add(item.path);
            }

            adapter = new ImageSelectAdapter(this,false,images,this);
            recyclerView.setAdapter(adapter);
        }
    }

    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            AlertDialog.Builder builder = new AlertDialog.Builder(PostNormalActivity.this);
            builder.setTitle("Permission Notice").setMessage("Sorry you can't use camera & gallery. Please allow permission first.").setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onBackPressed();
                }
            }).create().show();
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.btnPost){
            OnPost();
        }
        return super.onOptionsItemSelected(item);
    }

    private void Save(){
        List<MultipartBody.Part> request = new ArrayList<>();

        request.add(okhttp3.MultipartBody.Part.createFormData("auth_user",String.valueOf(Auth.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("job_category","0"));
        request.add(okhttp3.MultipartBody.Part.createFormData("privacy",String.valueOf(privacy)));

        if (txtContent.getText().toString().length() > 0){
            request.add(okhttp3.MultipartBody.Part.createFormData("feed_text",txtContent.getText().toString()));
        }

        if (isEmbed){
            if (content != null){

                request.add(okhttp3.MultipartBody.Part.createFormData("type_link","link"));

                if (content.getImages() != null && content.getImages().size() > 0){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_thumbnail",content.getImages().get(0)));
                }

                if (content.getTitle() != null && !content.getTitle().equals("")){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_title",content.getTitle()));
                }

                if (content.getFinalUrl() != null && !content.getFinalUrl().equals("")){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_url",content.getFinalUrl()));
                }

                if (content.getCannonicalUrl() != null && !content.getCannonicalUrl().equals("")){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_host",content.getCannonicalUrl()));
                }

                if (content.getDescription() != null && !content.getDescription().equals("")){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_text",content.getDescription()));
                }

            }
        }

        if (images!=null && images.size() > 0){
            for (String value : images){
                Uri uri = Uri.parse(value);
                request.add(prepareFilePart("feed_image[]",new File(Objects.requireNonNull(uri.getPath()))));
            }
        }

        service.PostNormal(request).enqueue(SaveCallback);
    }

    private void Update(){
        List<MultipartBody.Part> request = new ArrayList<>();

        request.add(okhttp3.MultipartBody.Part.createFormData("auth_user",String.valueOf(Auth.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("job_category","0"));
        request.add(okhttp3.MultipartBody.Part.createFormData("privacy",String.valueOf(privacy)));
        request.add(okhttp3.MultipartBody.Part.createFormData("post_id",String.valueOf(feed.getId())));

        if (txtContent.getText().toString().length() > 0){
            request.add(okhttp3.MultipartBody.Part.createFormData("feed_text",txtContent.getText().toString()));
        }

        if (isEmbed){

            if (content != null){

                request.add(okhttp3.MultipartBody.Part.createFormData("type_link","link"));

                if (content.getImages() != null && content.getImages().size() > 0){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_thumbnail",content.getImages().get(0)));
                }

                if (content.getTitle() != null && !content.getTitle().equals("")){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_title",content.getTitle()));
                }

                if (content.getFinalUrl() != null && !content.getFinalUrl().equals("")){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_url",content.getFinalUrl()));
                }

                if (content.getCannonicalUrl() != null && !content.getCannonicalUrl().equals("")){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_host",content.getCannonicalUrl()));
                }

                if (content.getDescription() != null && !content.getDescription().equals("")){
                    request.add(okhttp3.MultipartBody.Part.createFormData("source_text",content.getDescription()));
                }

            }
        }

        if (images!=null && images.size() > 0){
            for (String value : images){
                if (value.contains("http")){
                    Uri uri = SuperUtil.getImageUri(this,SuperUtil.getBitmap(value));
                    File file = new File(uri.getPath());
                    try {
                        request.add(prepareFilePart("feed_image[]",File.createTempFile(file.getAbsolutePath(),".png")));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    Uri uri = Uri.parse(value);
                    File file = new File(uri.getPath());
                    request.add(prepareFilePart("feed_image[]",file));
                }
            }
        }

        service.UpdateNormal(request).enqueue(UpdateCallback);
    }

    private void OnPost(){

        boolean isSubmit = ((txtContent.getText()!= null && !txtContent.getText().toString().equals("")) || (images != null && images.size() > 0));
        if (isSubmit || isEmbed){
            if (isEdit){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Update").setMessage("Do you want to update this feed?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showProgress();
                        Update();
                    }
                }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Save").setMessage("Do you want to create this feed?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showProgress();
                        Save();
                    }
                }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Notice").setMessage("You have empty article or photo, please complete this first.").setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create().show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == layoutAction){
            OpenImagePicker();
        }else if (v == layoutPrivacy){
            OnChangePrivacy();
        }else if (v == btnCloseEmbed){
            isEmbed(false);
        }
    }

    private void OnChangePrivacy(){
        SheetMenu.with(this)
                .setAutoCancel(true)
                .setMenu(R.menu.menu_privacy)
                .setClick(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.btnPublic :
                                /*code*/
                                ChangePrivacy(0);
                                break;
                            case R.id.btnPrivate :
                                ChangePrivacy(1);
                                break;
                        }
                        return false;
                    }
                })
                .showIcons(true).show();
    }

    @Override
    public void remove(ImageSelect view, int position) {
        images.remove(position);
        adapter.remove(images);
    }

    @Override
    public void camera(CameraView view) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.e("onKeyDown keycode",String.valueOf(keyCode));
        if (keyCode == KeyEvent.KEYCODE_ENTER){
            String content = txtContent.getText().toString();
            makeEmbed(content);
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.e("onKeyUp keycode",String.valueOf(keyCode));
        if (keyCode == KeyEvent.KEYCODE_ENTER){
            String content = txtContent.getText().toString();
            makeEmbed(content);
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {
        EditText editText = (EditText)view;
        if (keyCode == EditorInfo.IME_ACTION_SEARCH || keyCode == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            if (!event.isShiftPressed()) {
                String content = editText.getText().toString();
                makeEmbed(content);
            }
        }
        return false;
    }

    private void makeEmbed(String content){
        firstURL = SuperUtil.getFirstLineString(content);
        boolean isPorn = isPornSite(firstURL);
        if (!isEmbed && !isPorn){
            textCrawler.makePreview(callback,firstURL,50);
        }else {
            if (isPorn){
                Toast.makeText(PostNormalActivity.this,"It's porn site.",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void contentFromLink(String content){
        if (content.length() > 0){
            if (content.contains(HTTPS)){
                int lastIndex = content.lastIndexOf(HTTPS);
                if (lastIndex > 0){
                    String textBeforeURL = content.substring(0,lastIndex);
                    String urlText = content.substring(lastIndex);
                    txtContent.setText(textBeforeURL);
                    makeEmbed(urlText);
                }else {
                    txtContent.setText(content);
                    makeEmbed(content);
                }
            }else if (content.contains(HTTP)){
                int lastIndex = content.lastIndexOf(HTTP);
                if (lastIndex > 0){
                    String textBeforeURL = content.substring(0,lastIndex);
                    String urlText = content.substring(lastIndex);
                    txtContent.setText(textBeforeURL);
                    makeEmbed(urlText);
                }else {
                    txtContent.setText(content);
                    makeEmbed(content);
                }
            }else if (content.contains(WWW)){
                int lastIndex = content.lastIndexOf(WWW);
                if (lastIndex > 0){
                    String textBeforeURL = content.substring(0,lastIndex);
                    String urlText = content.substring(lastIndex);
                    txtContent.setText(textBeforeURL);
                    makeEmbed(urlText);
                }else {
                    txtContent.setText(content);
                    makeEmbed(content);
                }
            }
        }
    }

    @Override
    public void onUpdate() {
        String content = txtContent.getText().toString();
        contentFromLink(content);
    }
}

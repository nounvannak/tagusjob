package com.tagusnow.tagmejob.v3.post;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.pedro.library.AutoPermissions;
import com.pedro.library.AutoPermissionsListener;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.Gallery;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Model.Location;
import com.tagusnow.tagmejob.V2.Model.LocationPaginate;
import com.tagusnow.tagmejob.WidgetQuickPostCV;
import com.tagusnow.tagmejob.adapter.ImageSelectFaceAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.v3.post.photo.CameraView;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelect;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectAdapter;
import com.tagusnow.tagmejob.v3.post.photo.ImageSelectListener;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;

public class PostStaffActivity extends TagUsJobActivity implements ImageSelectListener, View.OnClickListener {

    private static final String TAG = PostStaffActivity.class.getSimpleName();
    public static final String IS_EDIT = "PostStaffActivity_is_edit";
    public static final String FEED = "PostStaffActivity_feed";
    private static final int PERMISSIONS_REQUEST_CODE_CV = 451;
    private static final int PERMISSIONS_REQUEST_CODE_COVER_LETTER = 452;
    private static final int CV_PICKER_REQUEST_CODE = 453;
    private static final int COVER_LETTER_PICKER_REQUEST_CODE = 454;
    private Spinner spnCategory,spnCity;
    private EditText title,phone,email,salary,description,file_cv,file_cover_letter;
    private View error_position,error_title,error_phone,error_email,error_city,error_cv,error_cover;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private ImageSelectAdapter adapter;
    private List<String> categoryList = new ArrayList<>();
    private List<String> cityList = new ArrayList<>();
    private List<String> images = new ArrayList<>();
    private File fileCV,fileCoverLetter;
    private List<Category> categories;
    private List<Location> locations;
    private int categoryID = 0,cityID = 0;
    private SmUser Auth;
    private Feed feed;
    private boolean isEdit;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<Feed> SaveCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            dismissProgress();
            if (response.isSuccessful()){
                onBackPressed();
            }else {
                try {
                    ShowAlert(response.message(),GotErrorMsg(response.errorBody().string()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            dismissProgress();
            t.printStackTrace();
        }
    };
    private Callback<Feed> UpdateCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            dismissProgress();
            if (response.isSuccessful()){
                onBackPressed();
            }else {
                try {
                    ShowAlert(response.message(),GotErrorMsg(response.errorBody().string()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            dismissProgress();
            t.printStackTrace();
        }
    };

    public static Intent createIntent(Activity activity){
        return new Intent(activity,PostStaffActivity.class);
    }

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();

        SmTagSubCategory smTagSubCategory = new Repository(this).restore().getCategory();

        if (smTagSubCategory != null){
            categories = smTagSubCategory.getData();
            categoryList.add("Choose Position");
            if (categories != null){
                for (Category category: categories){
                    categoryList.add(category.getTitle());
                }
            }
        }

        LocationPaginate smLocation = new Gson().fromJson(new Gson().toJson(new Repository(this).restore().getLocation()),LocationPaginate.class);

        if (smLocation != null){
            locations = smLocation.getData();
            if (locations != null){
                cityList.add("Choose City/Province");
                for (Location location: locations){
                    cityList.add(location.getName());
                }
            }
        }

        isEdit = getIntent().getBooleanExtra(IS_EDIT,false);

        if (isEdit){
            feed = new Gson().fromJson(getIntent().getStringExtra(FEED),Feed.class);
        }
    }

    private void InitEdit(){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_staff);

        InitTemp();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        error_position = (View)findViewById(R.id.error_position);
        error_title = (View)findViewById(R.id.error_title);
        error_phone = (View)findViewById(R.id.error_phone);
        error_email = (View)findViewById(R.id.error_email);
        error_city = (View)findViewById(R.id.error_city);
        error_cv = (View)findViewById(R.id.error_cv);
        error_cover = (View)findViewById(R.id.error_cover);

        spnCategory = (Spinner)findViewById(R.id.spnCategory);
        spnCity = (Spinner)findViewById(R.id.spnCity);
        title = (EditText)findViewById(R.id.title);
        phone = (EditText)findViewById(R.id.phone);
        email = (EditText)findViewById(R.id.email);
        salary = (EditText)findViewById(R.id.salary);
        file_cv = (EditText)findViewById(R.id.file_cv);
        file_cover_letter = (EditText)findViewById(R.id.file_cover_letter);
        description = (EditText)findViewById(R.id.description);
        file_cv.setOnClickListener(this);
        file_cover_letter.setOnClickListener(this);
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0){
                    categoryID = categories.get(i - 1).getId();
                }else {
                    categoryID = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                categoryID = 0;
            }
        });
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0){
                    cityID = locations.get(i - 1).getId();
                }else {
                    cityID = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                cityID = 0;
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        adapter = new ImageSelectAdapter(this,true,images,this);
        recyclerView.setAdapter(adapter);

        setupView();
        initUserInfo();
        if (isEdit){InitEdit();}
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void OpenImagePicker(){
        AndroidImagePicker.getInstance().pickMulti(this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                InitImage(items);
            }
        });
    }

    private void InitImage(List<ImageItem> items){
        if(items != null && items.size() > 0){
            for (ImageItem item : items){
                images.add(item.path);
            }

            adapter.append(images);
        }
    }

    private void initUserInfo(){
        if (Auth.getEmail()!=null){
            email.setText(Auth.getEmail());
        }
        if (Auth.getPhone()!=null){
            phone.setText(Auth.getPhone());
        }
    }

    private void setupView(){
        if (this.categoryList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,this.categoryList);
            spnCategory.setAdapter(adapter);
        }

        if (this.cityList!=null){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,this.cityList);
            spnCity.setAdapter(adapter);
        }
    }

    public boolean isValid(){
        boolean valid = true;
        if (categoryID > 0){
            error_position.setBackgroundColor(Color.GRAY);
        }else {
            error_position.setBackgroundColor(Color.RED);
            valid = false;
        }
        if (cityID > 0){
            error_city.setBackgroundColor(Color.GRAY);
        }else {
            error_city.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*Title*/
        if (this.title.getText().length() > 0){
            error_title.setBackgroundColor(Color.GRAY);
        }else {
            error_title.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*phone*/
        if (this.phone.getText().length() > 0){
            error_phone.setBackgroundColor(Color.GRAY);
        }else {
            error_phone.setBackgroundColor(Color.RED);
            valid = false;
        }
        /*email*/
        if (this.email.getText().length() > 0){
            error_phone.setBackgroundColor(Color.GRAY);
        }else {
            error_email.setBackgroundColor(Color.RED);
            valid = false;
        }

        if (fileCV==null){
//            ShowAlert("Form Invalid","Please select your CV.");
            error_cv.setBackgroundColor(Color.RED);
            valid = false;
        }else {
            error_cv.setBackgroundColor(Color.GRAY);
        }

        return valid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CV_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            if (path != null) {
                fileCV = new File(path);
                file_cv.setText(fileCV.getName());
            }
        }else if (requestCode == COVER_LETTER_PICKER_REQUEST_CODE && resultCode == RESULT_OK){
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            if (path != null) {
                fileCoverLetter = new File(path);
                file_cover_letter.setText(fileCoverLetter.getName());
            }
        }
    }

    private void checkPermissionsAndOpenFilePicker(int requestCode) {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        switch (requestCode){
            case CV_PICKER_REQUEST_CODE:
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        showError();
                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE_CV);
                    }
                } else {
                    openFilePicker(requestCode);
                }
                break;
            case COVER_LETTER_PICKER_REQUEST_CODE:
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        showError();
                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE_COVER_LETTER);
                    }
                } else {
                    openFilePicker(requestCode);
                }
                break;
        }
    }

    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE_CV:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker(CV_PICKER_REQUEST_CODE);
                } else {
                    showError();
                }
                break;
            case PERMISSIONS_REQUEST_CODE_COVER_LETTER:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker(COVER_LETTER_PICKER_REQUEST_CODE);
                } else {
                    showError();
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AutoPermissions.Companion.parsePermissions(this, requestCode, permissions, listener);
    }

    private void openFilePicker(int requestCode) {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(requestCode)
                .withHiddenFiles(true)
                .withTitle("File Picker")
                .start();
    }

    private AutoPermissionsListener listener = new AutoPermissionsListener() {
        @Override
        public void onGranted(int i, String[] strings) {
            Log.w(TAG,"onGranted");
        }

        @Override
        public void onDenied(int i, String[] strings) {
            AlertDialog.Builder builder = new AlertDialog.Builder(PostStaffActivity.this);
            builder.setTitle("Permission Notice").setMessage("Sorry you can't use camera & gallery. Please allow permission first.").setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onBackPressed();
                }
            }).create().show();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.btnPost){
            OnPost();
        }
        return super.onOptionsItemSelected(item);
    }

    private void Save(){
        List<MultipartBody.Part> request = new ArrayList<>();
        request.add(okhttp3.MultipartBody.Part.createFormData("user_id",String.valueOf(Auth.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("category_id",String.valueOf(categoryID)));
        request.add(okhttp3.MultipartBody.Part.createFormData("title",this.title.getText().toString()));
        request.add(okhttp3.MultipartBody.Part.createFormData("phone",this.phone.getText().toString()));
        request.add(okhttp3.MultipartBody.Part.createFormData("email",this.email.getText().toString()));
        request.add(okhttp3.MultipartBody.Part.createFormData("city_id",String.valueOf(cityID)));

        if (this.salary.getText().toString().length() > 0){
            request.add(okhttp3.MultipartBody.Part.createFormData("salary",this.salary.getText().toString()));
        }

        if (this.description.getText().toString().length() > 0){
            request.add(okhttp3.MultipartBody.Part.createFormData("description",this.description.getText().toString()));
        }

        if (this.images!=null){
            for (String value : this.images){
                Uri uri = Uri.parse(value);
                request.add(prepareFilePart("photo[]",new File(uri.getPath())));
            }
        }

        if (this.fileCV!=null){
            request.add(prepareFilePart("cv",fileCV));
        }

        if (this.fileCoverLetter!=null){
            request.add(prepareFilePart("cover_letter",fileCoverLetter));
        }

        service.PostCV(request).enqueue(SaveCallback);
    }

    private void Update(){
        List<MultipartBody.Part> request = new ArrayList<>();
        request.add(okhttp3.MultipartBody.Part.createFormData("user_id",String.valueOf(Auth.getId())));
        request.add(okhttp3.MultipartBody.Part.createFormData("category_id",String.valueOf(categoryID)));
        request.add(okhttp3.MultipartBody.Part.createFormData("title",this.title.getText().toString()));
        request.add(okhttp3.MultipartBody.Part.createFormData("phone",this.phone.getText().toString()));
        request.add(okhttp3.MultipartBody.Part.createFormData("email",this.email.getText().toString()));
        request.add(okhttp3.MultipartBody.Part.createFormData("city_id",String.valueOf(cityID)));

        if (this.salary.getText().toString().length() > 0){
            request.add(okhttp3.MultipartBody.Part.createFormData("salary",this.salary.getText().toString()));
        }

        if (this.description.getText().toString().length() > 0){
            request.add(okhttp3.MultipartBody.Part.createFormData("description",this.description.getText().toString()));
        }

        if (this.images!=null){
            for (String value : this.images){
                if (value.contains("http")){
                    Uri uri = SuperUtil.getImageUri(this,SuperUtil.getBitmap(value));
                    File file = new File(uri.getPath());
                    try {
                        request.add(prepareFilePart("photo[]",File.createTempFile(file.getAbsolutePath(),".png")));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    Uri uri = Uri.parse(value);
                    File file = new File(uri.getPath());
                    request.add(prepareFilePart("photo[]",file));
                }
            }
        }

        if (this.fileCV!=null){
            request.add(prepareFilePart("cv",fileCV));
        }

        if (this.fileCoverLetter!=null){
            request.add(prepareFilePart("cover_letter",fileCoverLetter));
        }

        service.UpdateCV(feed.getId(),request).enqueue(UpdateCallback);
    }

    private void OnPost(){

        if (isValid()){
            if (isEdit){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Update").setMessage("Do you want to update this resume?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showProgress();
                        Update();
                    }
                }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Save").setMessage("Do you want to create this resume?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showProgress();
                        Save();
                    }
                }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        }
    }

    @Override
    public void remove(ImageSelect view, int position) {
        images.remove(position);
        adapter.remove(images);
    }

    @Override
    public void camera(CameraView view) {
        OpenImagePicker();
    }

    @Override
    public void onClick(View v) {
        if (v==file_cv){
            checkPermissionsAndOpenFilePicker(CV_PICKER_REQUEST_CODE);
        }else if (v==file_cover_letter){
            checkPermissionsAndOpenFilePicker(COVER_LETTER_PICKER_REQUEST_CODE);
        }
    }
}

package com.tagusnow.tagmejob.v3.post.photo;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class CameraView extends TagUsJobRelativeLayout implements View.OnClickListener {

    private ImageSelectListener listener;

    private void InitUI(Context context){
        inflate(context, R.layout.camera_view,this);

        this.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public CameraView(Context context) {
        super(context);
        InitUI(context);
    }

    public CameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public CameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CameraView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View v) {
        if (v == this){
            listener.camera(this);
        }
    }

    public void setListener(ImageSelectListener listener) {
        this.listener = listener;
    }
}

package com.tagusnow.tagmejob.v3.post.photo;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class CameraViewHolder extends RecyclerView.ViewHolder {

    private CameraView cameraView;

    public CameraViewHolder(View itemView) {
        super(itemView);
        cameraView = (CameraView)itemView;
    }

    public void BindView(Activity activity,ImageSelectListener imageSelectListener){
        cameraView.setActivity(activity);
        cameraView.setListener(imageSelectListener);
    }
}

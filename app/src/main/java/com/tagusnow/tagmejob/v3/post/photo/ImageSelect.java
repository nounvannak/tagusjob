package com.tagusnow.tagmejob.v3.post.photo;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class ImageSelect extends TagUsJobRelativeLayout implements View.OnClickListener {

    private ImageView imageView,btnClose;
    private ImageSelectListener listener;
    private Context context;

    public void setImage(String image){
        Glide.with(context).load(image).error(R.drawable.not_available_).fitCenter().into(imageView);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        this.context = context;
        inflate(context, R.layout.image_select,this);

        imageView = (ImageView)findViewById(R.id.imageView);
        btnClose = (ImageView)findViewById(R.id.btnClose);

        btnClose.setOnClickListener(this);
    }

    public ImageSelect(Context context) {
        super(context);
        InitUI(context);
    }

    public ImageSelect(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public ImageSelect(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public ImageSelect(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setListener(ImageSelectListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (v == btnClose){
            listener.remove(this,(int) this.getTag());
        }
    }
}

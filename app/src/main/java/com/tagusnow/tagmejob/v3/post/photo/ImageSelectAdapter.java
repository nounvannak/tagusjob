package com.tagusnow.tagmejob.v3.post.photo;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import java.util.List;

public class ImageSelectAdapter extends RecyclerView.Adapter {

    private static final int CAMERA = 0;
    private static final int PHOTO = 1;
    private Activity activity;
    private List<String> images;
    private ImageSelectListener listener;
    private int other = 1;
    private boolean isCamera;

    public ImageSelectAdapter(Activity activity,boolean isCamera, List<String> images, ImageSelectListener listener) {
        this.activity = activity;
        this.images = images;
        this.isCamera = isCamera;
        this.listener = listener;
    }

    public void remove(List<String> images){
        this.images = images;
        notifyDataSetChanged();
    }

    public void append(List<String> images){
        this.images = images;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return isCamera ? (position == (getItemCount() - 1) ? CAMERA : PHOTO) : PHOTO;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == CAMERA){
            return new CameraViewHolder(new CameraView(activity));
        }else {
            return new ImageSelectHolder(new ImageSelect(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == CAMERA){
            ((CameraViewHolder) holder).BindView(activity,listener);
        }else {
            ((ImageSelectHolder) holder).BindView(activity,images.get(position - (isCamera ? (other - 1) : 0)),listener);
        }
    }

    @Override
    public int getItemCount() {
        return isCamera ? images.size() + other : images.size();
    }
}

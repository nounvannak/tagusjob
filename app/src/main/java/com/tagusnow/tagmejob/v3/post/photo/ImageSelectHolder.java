package com.tagusnow.tagmejob.v3.post.photo;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ImageSelectHolder extends RecyclerView.ViewHolder {

    private ImageSelect imageSelect;

    public ImageSelectHolder(View itemView) {
        super(itemView);
        imageSelect = (ImageSelect)itemView;
    }

    public void BindView(Activity activity,String image,ImageSelectListener imageSelectListener){
        imageSelect.setActivity(activity);
        imageSelect.setImage(image);
        imageSelect.setTag(getAdapterPosition());
        imageSelect.setListener(imageSelectListener);
    }
}

package com.tagusnow.tagmejob.v3.post.photo;

public interface ImageSelectListener {
    void remove(ImageSelect view, int position);
    void camera(CameraView view);
}

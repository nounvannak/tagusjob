package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class AuthHeader extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private SmUser user;

    public void setUser(SmUser user) {
        this.user = user;

        setUserCover(cover,user);
        setUserProfile(profile,user);
        setName(name,user);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setAuthHeaderListener(AuthHeaderListener authHeaderListener) {
        this.authHeaderListener = authHeaderListener;
    }

    private AuthHeaderListener authHeaderListener;
    private TextView name;
    private Button btnInfo,btnMore,btnChangeCover;
    private RelativeLayout btnChangeProfile,layoutBlurOfCover,layoutBlurOfProfile;
    private ImageView cover;
    private CircleImageView profile;

    private void InitUI(Context context){
        inflate(context, R.layout.auth_header,this);
        this.context = context;

        name = (TextView)findViewById(R.id.name);
        btnInfo = (Button)findViewById(R.id.btnInfo);
        btnMore = (Button)findViewById(R.id.btnMore);
        btnChangeCover = (Button) findViewById(R.id.btnChangeCover);
        btnChangeProfile = (RelativeLayout)findViewById(R.id.btnChangeProfile);
        layoutBlurOfCover = (RelativeLayout)findViewById(R.id.layoutBlurOfCover);
        layoutBlurOfProfile = (RelativeLayout)findViewById(R.id.layoutBlurOfProfile);
        cover = (ImageView)findViewById(R.id.cover);
        profile = (CircleImageView)findViewById(R.id.profile);

        hideBlurOfCover();
        hideBlurOfProfile();

        btnChangeProfile.setOnClickListener(this);
        btnChangeCover.setOnClickListener(this);
        btnInfo.setOnClickListener(this);
        btnMore.setOnClickListener(this);
        cover.setOnClickListener(this);
        profile.setOnClickListener(this);
    }

    public void hideBlurOfCover(){
        layoutBlurOfCover.setVisibility(GONE);
    }

    public void showBlurOfCover(){
        layoutBlurOfCover.setVisibility(VISIBLE);
    }

    public void hideBlurOfProfile(){
        layoutBlurOfProfile.setVisibility(GONE);
    }

    public void showBlurOfProfile(){
        layoutBlurOfProfile.setVisibility(VISIBLE);
    }

    public AuthHeader(Context context) {
        super(context);
        InitUI(context);
    }

    public AuthHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public AuthHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public AuthHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnChangeCover){
            authHeaderListener.changeCover(this,user);
        }else if (view == btnChangeProfile){
            authHeaderListener.changeProfile(this,user);
        }else if (view == btnInfo){
            authHeaderListener.info(this,user);
        }else if (view == btnMore){
            authHeaderListener.more(this,user);
        }else if (view == cover){
            authHeaderListener.displayCover(this,user);
        }else if (view == profile){
            authHeaderListener.displayProfile(this,user);
        }
    }
}

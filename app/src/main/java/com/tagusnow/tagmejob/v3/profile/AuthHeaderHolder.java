package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class AuthHeaderHolder extends RecyclerView.ViewHolder {

    private AuthHeader authHeader;

    public AuthHeaderHolder(View itemView) {
        super(itemView);
        authHeader = (AuthHeader)itemView;
    }

    public void BindView(Activity activity, SmUser user,AuthHeaderListener authHeaderListener){
        authHeader.setActivity(activity);
        authHeader.setUser(user);
        authHeader.setAuthHeaderListener(authHeaderListener);
    }
}

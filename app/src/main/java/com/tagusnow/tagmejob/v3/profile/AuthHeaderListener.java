package com.tagusnow.tagmejob.v3.profile;

import com.tagusnow.tagmejob.auth.SmUser;

public interface AuthHeaderListener {
    void displayCover(AuthHeader view, SmUser user);
    void displayProfile(AuthHeader view, SmUser user);
    void changeCover(AuthHeader view, SmUser user);
    void changeProfile(AuthHeader view, SmUser user);
    void info(AuthHeader view, SmUser user);
    void more(AuthHeader view, SmUser user);
}

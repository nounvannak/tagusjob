package com.tagusnow.tagmejob.v3.profile;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;

public class AuthMoreFragment extends BaseDialogFragment<AuthHeader, SmUser ,MoreAuthListener> implements View.OnClickListener {
    private LinearLayout btnShowAllPhotos,btnShowAllFriends,btnCopyLink,btnMore,btnLogout;

    public AuthMoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void setListener(MoreAuthListener listener) {
        super.setListener(listener);
    }

    @Override
    public void setUser(SmUser user) {
        super.setUser(user);
    }

    @Override
    public void setV(AuthHeader authHeader) {
        super.setV(authHeader);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.auth_more_fragement, null);
        builder.setView(dialogView);

        btnShowAllPhotos = (LinearLayout)dialogView.findViewById(R.id.btnShowAllPhotos);
        btnShowAllFriends = (LinearLayout)dialogView.findViewById(R.id.btnShowAllFriends);
        btnCopyLink = (LinearLayout)dialogView.findViewById(R.id.btnCopyLink);
        btnMore = (LinearLayout)dialogView.findViewById(R.id.btnMore);
        btnLogout = (LinearLayout)dialogView.findViewById(R.id.btnLogout);

        btnShowAllPhotos.setOnClickListener(this);
        btnShowAllFriends.setOnClickListener(this);
        btnCopyLink.setOnClickListener(this);
        btnMore.setOnClickListener(this);
        btnLogout.setOnClickListener(this);

        return builder.create();
    }

    @Override
    public void onClick(View view) {
        if (view == btnShowAllPhotos){
            listener.displayAllPhotos(v,this,user);
        }else if (view == btnShowAllFriends){
            listener.displayAllFriends(v,this,user);
        }else if (view == btnCopyLink){
            listener.copyPrifleLink(v,this,user);
        }else if (view == btnLogout){
            listener.logout(v,this);
        }else if (view == btnMore){
            listener.moreOptions(v,this);
        }
    }
}

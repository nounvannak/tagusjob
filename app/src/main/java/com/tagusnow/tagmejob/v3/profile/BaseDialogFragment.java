package com.tagusnow.tagmejob.v3.profile;

import android.support.v4.app.DialogFragment;

public abstract class BaseDialogFragment<V,U,L> extends DialogFragment {
    protected V v;
    protected U user;
    protected L listener;

    public void setV(V v){
        this.v = v;
    }

    public void setUser(U user){
        this.user = user;
    }

    public void setListener(L listener){
        this.listener = listener;
    }
}

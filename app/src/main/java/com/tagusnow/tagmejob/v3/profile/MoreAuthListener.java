package com.tagusnow.tagmejob.v3.profile;

import android.support.v4.app.DialogFragment;

import com.tagusnow.tagmejob.auth.SmUser;

public interface MoreAuthListener {
    void displayAllPhotos(AuthHeader view, DialogFragment fragment, SmUser user);
    void displayAllFriends(AuthHeader view, DialogFragment fragment,SmUser user);
    void copyPrifleLink(AuthHeader view, DialogFragment fragment,SmUser user);
    void moreOptions(AuthHeader view, DialogFragment fragment);
    void logout(AuthHeader view, DialogFragment fragment);
}

package com.tagusnow.tagmejob.v3.profile;

import android.support.v4.app.DialogFragment;

import com.tagusnow.tagmejob.auth.SmUser;

public interface MoreUserListener {
    void displayAllPhotos(UserHeader view, DialogFragment fragment, SmUser user);
    void displayAllFriends(UserHeader view,DialogFragment fragment,SmUser user);
    void copyProfileLink(UserHeader view,DialogFragment fragment,SmUser user);
}

package com.tagusnow.tagmejob.v3.profile;

import java.io.Serializable;

public class Photo implements Serializable {

    public Photo(int id, int feed_id, int user_id, String image, int status, int trash, String created_at, String updated_at) {
        this.id = id;
        this.feed_id = feed_id;
        this.user_id = user_id;
        this.image = image;
        this.status = status;
        this.trash = trash;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    private int id;
    private int feed_id;
    private int user_id;
    private String image;
    private int status;
    private int trash;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(int feed_id) {
        this.feed_id = feed_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    private String created_at;

    public Photo() {
        super();
    }

    private String updated_at;
}

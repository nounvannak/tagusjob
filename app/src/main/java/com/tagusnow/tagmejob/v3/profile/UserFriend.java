package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class UserFriend extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;

    public void setUser(SmUser user) {
        this.user = user;

        setUserProfile(profile,user);
        setName(name,user);
    }

    public void setUserFriendListener(UserFriendListener userFriendListener) {
        this.userFriendListener = userFriendListener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private SmUser user;
    private UserFriendListener userFriendListener;
    private ImageView profile;
    private TextView name;

    private void InitUI(Context context){
        inflate(context, R.layout.user_friend,this);
        this.context = context;

        profile = (ImageView)findViewById(R.id.profile);
        name = (TextView)findViewById(R.id.name);

        this.setOnClickListener(this);
    }

    public UserFriend(Context context) {
        super(context);
        InitUI(context);
    }

    public UserFriend(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserFriend(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserFriend(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (this == view){
            userFriendListener.onItemSelected(this,user);
        }
    }
}

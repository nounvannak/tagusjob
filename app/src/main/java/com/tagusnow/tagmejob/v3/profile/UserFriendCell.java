package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class UserFriendCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setUserFriendListener(UserFriendListener userFriendListener) {
        this.userFriendListener = userFriendListener;
    }

    public void setAdapter(UserFriendCellAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    private UserFriendListener userFriendListener;
    private RecyclerView recyclerView;
    private Button btnSeeAll;
    private UserFriendCellAdapter adapter;

    private void InitUI(Context context){
        inflate(context, R.layout.user_friend_cell,this);
        this.context = context;

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new GridLayoutManager(context,3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        btnSeeAll = (Button)findViewById(R.id.btnSeeAll);
        btnSeeAll.setOnClickListener(this);
    }

    public boolean hideButtonSeeAll(){
        btnSeeAll.setVisibility(GONE);
        return false;
    }

    public boolean showButtonSeeAll(){
        btnSeeAll.setVisibility(VISIBLE);
        return true;
    }

    public UserFriendCell(Context context) {
        super(context);
        InitUI(context);
    }

    public UserFriendCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserFriendCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserFriendCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnSeeAll){
            userFriendListener.onSeeAll(this);
        }
    }
}

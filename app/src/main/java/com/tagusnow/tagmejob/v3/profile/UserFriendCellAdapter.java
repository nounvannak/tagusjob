package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;

import java.util.List;

public class UserFriendCellAdapter extends RecyclerView.Adapter<UserFriendHolder> {

    private Activity activity;
    private List<SmUser> users;

    public UserFriendCellAdapter(Activity activity, List<SmUser> users, UserFriendListener userFriendListener) {
        this.activity = activity;
        this.users = users;
        this.userFriendListener = userFriendListener;
    }

    public void NextData(List<SmUser> users){
        this.users.addAll(users);
    }

    private UserFriendListener userFriendListener;

    @NonNull
    @Override
    public UserFriendHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserFriendHolder(new UserFriend(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull UserFriendHolder holder, int position) {
        holder.BindView(activity,users.get(position),userFriendListener);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}

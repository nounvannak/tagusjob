package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class UserFriendCellHolder extends RecyclerView.ViewHolder {

    private UserFriendCell userFriendCell;

    public UserFriendCellHolder(View itemView) {
        super(itemView);
        userFriendCell = (UserFriendCell)itemView;
    }

    public void BindView(Activity activity,UserFriendListener userFriendListener){
        userFriendCell.setActivity(activity);
        userFriendCell.setUserFriendListener(userFriendListener);
    }
}

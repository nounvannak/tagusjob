package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class UserFriendHolder extends RecyclerView.ViewHolder {

    private UserFriend userFriend;

    public UserFriendHolder(View itemView) {
        super(itemView);
        userFriend = (UserFriend)itemView;
    }

    public void BindView(Activity activity, SmUser user,UserFriendListener userFriendListener){
        userFriend.setActivity(activity);
        userFriend.setUser(user);
        userFriend.setUserFriendListener(userFriendListener);
    }
}

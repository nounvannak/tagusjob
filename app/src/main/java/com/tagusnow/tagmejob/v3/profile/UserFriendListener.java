package com.tagusnow.tagmejob.v3.profile;

import com.tagusnow.tagmejob.auth.SmUser;

public interface UserFriendListener {
    void onItemSelected(UserFriend view, SmUser user);
    void onSeeAll(UserFriendCell view);
}

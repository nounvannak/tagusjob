package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserHeader extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private SmUser user;
    private ImageView cover;
    private TextView name;
    private Button btnConnect;
    private ImageButton btnChat,btnMore;
    private CircleImageView profile;

    public void setUser(SmUser user) {
        this.user = user;

        setName(name,user);
        setUserProfile(profile,user);
        setUserCover(cover,user);

        if (user.isIs_follow()){
            btnConnect.setText(R.string.following);
        }else {
            btnConnect.setText(R.string.follow);
        }
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setUserHeaderListener(UserHeaderListener userHeaderListener) {
        this.userHeaderListener = userHeaderListener;
    }

    private UserHeaderListener userHeaderListener;

    private void InitUI(Context context){
        inflate(context, R.layout.user_header,this);
        this.context = context;

        cover = (ImageView)findViewById(R.id.cover);
        profile = (CircleImageView)findViewById(R.id.profile);
        name = (TextView)findViewById(R.id.name);
        btnConnect = (Button)findViewById(R.id.btnConnect);
        btnChat = (ImageButton)findViewById(R.id.btnChat);
        btnMore = (ImageButton)findViewById(R.id.btnMore);

        cover.setOnClickListener(this);
        profile.setOnClickListener(this);
        btnConnect.setOnClickListener(this);
        btnChat.setOnClickListener(this);
        btnMore.setOnClickListener(this);
    }

    public UserHeader(Context context) {
        super(context);
        InitUI(context);
    }

    public UserHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == cover){
            userHeaderListener.displayCover(this,user);
        }else if (view == profile){
            userHeaderListener.displayProfile(this,user);
        }else if (view == btnConnect){
            userHeaderListener.connect(this,user);
        }else if (view == btnChat){
            userHeaderListener.about(this,user);
        }else if (view == btnMore){
            userHeaderListener.more(this,user);
        }
    }
}

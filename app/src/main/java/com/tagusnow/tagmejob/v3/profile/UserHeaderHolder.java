package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class UserHeaderHolder extends RecyclerView.ViewHolder {

    private UserHeader userHeader;

    public UserHeaderHolder(View itemView) {
        super(itemView);
        userHeader = (UserHeader)itemView;
    }

    public void BindView(Activity activity, SmUser user,UserHeaderListener userHeaderListener){
        userHeader.setActivity(activity);
        userHeader.setUser(user);
        userHeader.setUserHeaderListener(userHeaderListener);
    }
}

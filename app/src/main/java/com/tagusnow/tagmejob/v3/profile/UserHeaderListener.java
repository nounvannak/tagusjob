package com.tagusnow.tagmejob.v3.profile;

import com.tagusnow.tagmejob.auth.SmUser;

public interface UserHeaderListener {
    void connect(UserHeader view, SmUser user);
    void displayProfile(UserHeader view,SmUser user);
    void displayCover(UserHeader view,SmUser user);
    void chat(UserHeader view,SmUser user);
    void more(UserHeader view,SmUser user);
    void about(UserHeader view,SmUser user);
}

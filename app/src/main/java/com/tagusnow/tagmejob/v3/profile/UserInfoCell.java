package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class UserInfoCell extends TagUsJobRelativeLayout {

    private Context context;
    private TextView joinDate,slug,email,userType,address,phone;

    public void setUser(SmUser user) {
        this.user = user;

        joinDate.setText(GetDate(user.getCreated_at(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss,DATE_FORMAT_E_MMM_dd_yyyy));
        slug.setText(user.getSlug());
        email.setText(user.getEmail() != null ? user.getEmail() : "N/A");
        userType.setText(user.getUser_type() == SmUser.FIND_STAFF ? "Find Staff" : "Find Job");
        address.setText(user.getLocation() != null && !user.getLocation().equals("") ? user.getLocation() : (user.getAddress() != null && !user.getAddress().equals("") ? user.getAddress() : "Cambodia"));
        phone.setText(user.getPhone()!=null && !user.getPhone().equals("") ? user.getPhone() : "N/A");
    }

    private SmUser user;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.user_info_cell,this);
        this.context = context;

        joinDate = (TextView)findViewById(R.id.joinDate);
        slug = (TextView)findViewById(R.id.slug);
        email = (TextView)findViewById(R.id.email);
        userType = (TextView)findViewById(R.id.userType);
        address = (TextView)findViewById(R.id.address);
        phone = (TextView)findViewById(R.id.phone);
    }

    public UserInfoCell(Context context) {
        super(context);
        InitUI(context);
    }

    public UserInfoCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserInfoCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserInfoCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }
}

package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class UserInfoCellHolder extends RecyclerView.ViewHolder {

    private UserInfoCell userInfoCell;

    public UserInfoCellHolder(View itemView) {
        super(itemView);

        userInfoCell = (UserInfoCell)itemView;
    }

    public void BindView(Activity activity, SmUser user){
        userInfoCell.setActivity(activity);
        userInfoCell.setUser(user);
    }
}

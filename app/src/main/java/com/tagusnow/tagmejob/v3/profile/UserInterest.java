package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.util.Random;

public class UserInterest extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private Category category;
    private TextView counter;
    private ImageView logo;
    private TextView name;
    private int[] colors = {
            R.color.interest_1,R.color.interest_2,R.color.interest_3,R.color.interest_4,
            R.color.interest_5,R.color.interest_6,R.color.interest_7,R.color.interest_8,
            R.color.interest_9,R.color.interest_10,R.color.interest_11,R.color.interest_12};

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setCategory(Category category) {
        this.category = category;

        int random = new Random().nextInt(12);
        name.setText(category.getTitle());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            name.setBackgroundColor(context.getColor(colors[random]));
        }
        counter.setText(String.valueOf(category.getJob_counter()));
        setCategoryPhoto(logo,category.getImage());

    }

    public void setUserInterestListener(UserInterestListener userInterestListener) {
        this.userInterestListener = userInterestListener;
    }

    private UserInterestListener userInterestListener;

    private void InitUI(Context context){
        inflate(context,R.layout.user_interest,this);

        this.context = context;

        counter = (TextView)findViewById(R.id.counter);
        name = (TextView)findViewById(R.id.name);
        logo = (ImageView)findViewById(R.id.logo);

        this.setOnClickListener(this);
    }

    public UserInterest(Context context) {
        super(context);
        InitUI(context);
    }

    public UserInterest(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserInterest(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserInterest(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (this == view){
            userInterestListener.onItemSelected(this,category);
        }
    }
}

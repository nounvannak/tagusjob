package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class UserInterestCell extends TagUsJobRelativeLayout {

    private Context context;
    private RecyclerView recyclerView;

    public void setAdapter(UserInterestCellAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private UserInterestCellAdapter adapter;

    private void InitUI(Context context){
        inflate(context, R.layout.user_interest_cell,this);

        this.context = context;

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
    }

    public UserInterestCell(Context context) {
        super(context);
        InitUI(context);
    }

    public UserInterestCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserInterestCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserInterestCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }
}

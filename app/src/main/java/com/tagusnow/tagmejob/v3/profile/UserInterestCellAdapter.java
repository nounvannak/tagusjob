package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.feed.Category;
import java.util.List;

public class UserInterestCellAdapter extends RecyclerView.Adapter<UserInterestHolder> {

    private Activity activity;
    private List<Category> categories;

    public UserInterestCellAdapter(Activity activity, List<Category> categories, UserInterestListener userInterestListener) {
        this.activity = activity;
        this.categories = categories;
        this.userInterestListener = userInterestListener;
    }

    public void NextData(List<Category> categories){
        this.categories.addAll(categories);
    }

    private UserInterestListener userInterestListener;

    @NonNull
    @Override
    public UserInterestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserInterestHolder(new UserInterest(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull UserInterestHolder holder, int position) {
        holder.BindView(activity,categories.get(position),userInterestListener);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}

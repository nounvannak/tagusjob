package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class UserInterestCellHolder extends RecyclerView.ViewHolder {

    private UserInterestCell userInterestCell;

    public UserInterestCellHolder(View itemView) {
        super(itemView);
        userInterestCell = (UserInterestCell)itemView;
    }

    public void BindView(Activity activity){
        userInterestCell.setActivity(activity);
    }
}

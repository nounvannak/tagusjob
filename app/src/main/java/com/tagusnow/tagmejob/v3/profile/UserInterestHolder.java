package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Category;

public class UserInterestHolder extends RecyclerView.ViewHolder {

    private UserInterest userInterest;

    public UserInterestHolder(View itemView) {
        super(itemView);
        userInterest = (UserInterest)itemView;
    }

    public void BindView(Activity activity, Category category,UserInterestListener userInterestListener){
        userInterest.setActivity(activity);
        userInterest.setCategory(category);
        userInterest.setUserInterestListener(userInterestListener);
    }
}

package com.tagusnow.tagmejob.v3.profile;

import com.tagusnow.tagmejob.feed.Category;

public interface UserInterestListener {
    void onItemSelected(UserInterest view, Category category);
}

package com.tagusnow.tagmejob.v3.profile;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;

public class UserMoreFragment extends BaseDialogFragment<UserHeader, SmUser, MoreUserListener> implements View.OnClickListener {
    private LinearLayout btnShowAllPhotos,btnShowAllFriends,btnCopyLink;

    public UserMoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void setListener(MoreUserListener listener) {
        super.setListener(listener);
    }

    @Override
    public void setUser(SmUser user) {
        super.setUser(user);
    }

    @Override
    public void setV(UserHeader authHeader) {
        super.setV(authHeader);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.user_more_fragment, null);
        builder.setView(dialogView);

        btnShowAllPhotos = (LinearLayout)dialogView.findViewById(R.id.btnShowAllPhotos);
        btnShowAllFriends = (LinearLayout)dialogView.findViewById(R.id.btnShowAllFriends);
        btnCopyLink = (LinearLayout)dialogView.findViewById(R.id.btnCopyLink);


        btnShowAllPhotos.setOnClickListener(this);
        btnShowAllFriends.setOnClickListener(this);
        btnCopyLink.setOnClickListener(this);

        return builder.create();
    }

    @Override
    public void onClick(View view) {
        if (view == btnShowAllPhotos){
            listener.displayAllPhotos(v,this,user);
        }else if (view == btnShowAllFriends){
            listener.displayAllFriends(v,this,user);
        }else if (view == btnCopyLink){
            listener.copyProfileLink(v,this,user);
        }
    }
}

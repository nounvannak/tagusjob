package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class UserPhoto extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private Photo photo;
    private ImageView imageView;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
        setAlbumPhoto(imageView, SuperUtil.getAlbumPicture(photo.getImage(),"400"));
    }

    public void setUserPhotoListener(UserPhotoListener userPhotoListener) {
        this.userPhotoListener = userPhotoListener;
    }

    private UserPhotoListener userPhotoListener;

    private void InitUI(Context context){
        imageView = new ImageView(context);
        imageView.setPadding(5,5,5,5);
        LayoutParams params = new LayoutParams(150, 200);
        params.setMargins(5, 5, 5, 5);
        imageView.setLayoutParams(params);
        this.addView(imageView, 150, 200);

        imageView.setOnClickListener(this);
    }

    public UserPhoto(Context context) {
        super(context);
        InitUI(context);
    }

    public UserPhoto(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserPhoto(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserPhoto(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == imageView){
            userPhotoListener.onItemSelected(this,photo);
        }
    }
}

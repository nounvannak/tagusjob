package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class UserPhotoCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    private RecyclerView recyclerView;
    private Button btnSeeAll;

    public void setUserPhotoListener(UserPhotoListener userPhotoListener) {
        this.userPhotoListener = userPhotoListener;
    }

    private UserPhotoListener userPhotoListener;

    public void setAdapter(UserPhotoCellAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    private UserPhotoCellAdapter adapter;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.user_photo_cell,this);
        this.context = context;

        btnSeeAll = (Button)findViewById(R.id.btnSeeAll);
        btnSeeAll.setOnClickListener(this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
    }

    public boolean hideButtonSeeAll(){
        btnSeeAll.setVisibility(GONE);
        return false;
    }

    public boolean showButtonSeeAll(){
        btnSeeAll.setVisibility(VISIBLE);
        return true;
    }

    public UserPhotoCell(Context context) {
        super(context);
        InitUI(context);
    }

    public UserPhotoCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserPhotoCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserPhotoCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnSeeAll){
            userPhotoListener.onSeeAll(this);
        }
    }
}

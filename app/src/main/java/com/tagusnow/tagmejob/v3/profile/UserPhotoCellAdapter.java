package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

public class UserPhotoCellAdapter extends RecyclerView.Adapter<UserPhotoHolder> {

    private Activity activity;
    private List<Photo> photos;

    public UserPhotoCellAdapter(Activity activity, List<Photo> photos, UserPhotoListener userPhotoListener) {
        this.activity = activity;
        this.photos = photos;
        this.userPhotoListener = userPhotoListener;
    }

    public void NextData(List<Photo> photos){
        this.photos.addAll(photos);
    }

    private UserPhotoListener userPhotoListener;

    @NonNull
    @Override
    public UserPhotoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserPhotoHolder(new UserPhoto(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull UserPhotoHolder holder, int position) {
        holder.BindView(activity,photos.get(position),userPhotoListener);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}

package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class UserPhotoCellHolder extends RecyclerView.ViewHolder {

    private UserPhotoCell userPhotoCell;

    public UserPhotoCellHolder(View itemView) {
        super(itemView);
        userPhotoCell = (UserPhotoCell)itemView;
    }

    public void BindView(Activity activity,UserPhotoListener userPhotoListener){
        userPhotoCell.setActivity(activity);
        userPhotoCell.setUserPhotoListener(userPhotoListener);
    }
}

package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class UserPhotoHolder extends RecyclerView.ViewHolder {

    private UserPhoto userPhoto;

    public UserPhotoHolder(View itemView) {
        super(itemView);
        userPhoto = (UserPhoto)itemView;
    }

    public void BindView(Activity activity,Photo photo,UserPhotoListener userPhotoListener){
        userPhoto.setActivity(activity);
        userPhoto.setPhoto(photo);
        userPhoto.setUserPhotoListener(userPhotoListener);
    }
}

package com.tagusnow.tagmejob.v3.profile;

public interface UserPhotoListener {
    void onItemSelected(UserPhoto view,Photo photo);
    void onSeeAll(UserPhotoCell view);
}

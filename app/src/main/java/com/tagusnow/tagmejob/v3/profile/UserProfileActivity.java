package com.tagusnow.tagmejob.v3.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.feed.FeedJob;
import com.tagusnow.tagmejob.v3.feed.FeedJobListener;
import com.tagusnow.tagmejob.v3.feed.FeedListener;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.v3.like.LikeResultCell;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.v3.page.adapter.UserProfileAdapter;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends TagUsJobActivity implements UserInterestListener, FeedCVListener, FeedJobListener, UserPhotoListener, FeedNormalListener, GridViewListener, UserFriendListener, UserHeaderListener, SwipeRefreshLayout.OnRefreshListener, LikeResultListener, FeedListener, MoreUserListener, LoadingListener<LoadingView> {

    private static final String TAG = UserProfileActivity.class.getSimpleName();
    public static final String USER = "UserProfileActivity_user";
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private FloatingActionButton fabTop;
    private SmUser Auth,user;
    private UserPhotoCell userPhotoCell;
    private UserPhotoCellAdapter userPhotoCellAdapter;
    private List<Photo> photos;
    private UserFriendCell userFriendCell;
    private UserFriendCellAdapter userFriendCellAdapter;
    private UserInterestCell userInterestCell;
    private UserInterestCellAdapter userInterestCellAdapter;
    private FeedResponse feedResponse;
    private UserPaginate userPaginate;
    private PhotoPaginate photoPaginate;
    private List<Feed> feeds;
    private UserProfileAdapter adapter;
    private LoadingView loadingView;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<FeedResponse> FetchFeedCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.e(TAG,response.message());
                fetchFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.message());
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<FeedResponse> FetchNextFeedCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchNextFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<PhotoPaginate> FetchUserPhotoCallback = new Callback<PhotoPaginate>() {
        @Override
        public void onResponse(Call<PhotoPaginate> call, Response<PhotoPaginate> response) {
            if (response.isSuccessful()){
                fetchUserPhoto(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<PhotoPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<PhotoPaginate> FetchNextUserPhotoCallback = new Callback<PhotoPaginate>() {
        @Override
        public void onResponse(Call<PhotoPaginate> call, Response<PhotoPaginate> response) {
            if (response.isSuccessful()){
                fetchNextUserPhoto(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<PhotoPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> FetchUserFriendCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                fetchUserFriend(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<List<Category>> FetchUserInterestCallback = new Callback<List<Category>>() {
        @Override
        public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
            if (response.isSuccessful()){
                fetchUserInterest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<List<Category>> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Runnable RunCallback = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(false);
            fetchFeed();
        }
    };

    /* Socket.io */
    Socket socket;
    private Emitter.Listener NEW_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
        }
    };
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalLikes = obj.getInt("count_likes");
                            int userID = obj.getInt("user_id");
                            String likesResult = obj.getString("like_result");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);
                                    feedDetail.setCount_likes(totalLikes);
                                    feedDetail.setLike_result(likesResult);
                                    feedDetail.setIs_like(Auth.getId() == userID);
                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);
                                        feedDetail.setCount_likes(totalLikes);
                                        feedDetail.setLike_result(likesResult);
                                        feedDetail.setIs_like(Auth.getId() == userID);
                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });

        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalComments = obj.getInt("count_comments");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);
                                    feedDetail.setCount_comments(totalComments);
                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);
                                        feedDetail.setCount_comments(totalComments);
                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        int index = feeds.indexOf(feed);
                                        Feed updateFeed = feeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        feeds.set(index,updateFeed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            int index = feeds.indexOf(feed);
                                            Feed updateFeed = feeds.get(index);
                                            updateFeed.setFeed(feedJson.getFeed());
                                            feeds.set(index,updateFeed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        feeds.remove(feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("user_id");
                            if (Auth.getId() == userID){
                                Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                                if (feedJson != null){
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                        List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                        if (lists.size() > 0){
                                            Feed feed = lists.get(0);
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }else {
                                        for (Feed feed: feeds){
                                            if (feed.getId() == feedJson.getId()){
                                                feeds.remove(feed);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private void InitSocket(){
        socket = new App().getSocket();
        socket.on("New Post",NEW_POST);
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.connect();
    }
    /* End Socket.io */

    private void CheckNextFeed(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    fetchNextFeed();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    boolean isNextUserPhotoRequest = false;
    int lastPageOfUserPhoto = 1;

    private void CheckNextUserPhoto(){
        if (userPhotoCell != null){
            userPhotoCell.getRecyclerView().addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (photoPaginate.getTo() - 1)) && (photoPaginate.getTo() < photoPaginate.getTotal())) {
                            if (photoPaginate.getNext_page_url()!=null || !photoPaginate.getNext_page_url().equals("")){
                                if (lastPageOfUserPhoto!=photoPaginate.getCurrent_page()){
                                    isNextUserPhotoRequest = true;
                                    if (isNextUserPhotoRequest){
                                        lastPageOfUserPhoto = photoPaginate.getCurrent_page();
                                        fetchNextUserPhoto();
                                    }
                                }else{
                                    isNextUserPhotoRequest = false;
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void fetchFeed(){
        service.Profile(Auth.getId(),user.getId(),10).enqueue(FetchFeedCallback);
    }

    private void fetchFeed(FeedResponse feedResponse){

        Log.e(TAG, new Gson().toJson(feedResponse));
        this.feedResponse = feedResponse;
        this.feeds = feedResponse.getData();
        CheckNextFeed();

        adapter.NewData(feeds);
        recyclerView.setAdapter(adapter);
        loadingView.setLoading(feedResponse.getTotal() > feedResponse.getTo());

        fetchUserPhoto();
        fetchUserFriend();
        fetchUserInterest();
    }

    private void fetchNextFeed(){
        boolean isNext = feedResponse.getCurrent_page() != feedResponse.getLast_page();
        if (isNext){
            int page = feedResponse.getCurrent_page() + 1;
            service.Profile(Auth.getId(),user.getId(),10,page).enqueue(FetchNextFeedCallback);
        }
    }

    private void fetchNextFeed(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds.addAll(feedResponse.getData());
        CheckNextFeed();
        loadingView.setLoading(feedResponse.getTotal() > feedResponse.getTo());
        this.adapter.NextData(feedResponse.getData());
        this.adapter.notifyDataSetChanged();

    }

    private void fetchUserPhoto(){
        service.GetUserImagePost(user.getId(),10).enqueue(FetchUserPhotoCallback);
    }

    private void fetchUserPhoto(PhotoPaginate photoPaginate){
        Log.e(TAG,new Gson().toJson(photoPaginate));
        this.photoPaginate = photoPaginate;
        this.photos = photoPaginate.getData();
        userPhotoCellAdapter = new UserPhotoCellAdapter(this,photoPaginate.getData(),this);
        CheckNextUserPhoto();

        if (photoPaginate.getTotal() > 0){
            userPhotoCell.showButtonSeeAll();
        }else {
            userPhotoCell.hideButtonSeeAll();
        }

        if (userPhotoCell != null){
            userPhotoCell.setAdapter(userPhotoCellAdapter);
        }else {

            userPhotoCell = adapter.getUserPhotoCell();
            userPhotoCell.setAdapter(userPhotoCellAdapter);

            Log.e("fetchUserPhoto adapter",String.valueOf(adapter != null));
            Log.e("and userPhotoCell",String.valueOf(userPhotoCell != null));

        }
    }

    private void fetchNextUserPhoto(){
        boolean isNext = photoPaginate.getCurrent_page() != photoPaginate.getLast_page();
        if (isNext){
            int page = photoPaginate.getCurrent_page() + 1;
            service.GetUserImagePost(user.getId(),10,page).enqueue(FetchNextUserPhotoCallback);
        }
    }

    private void fetchNextUserPhoto(PhotoPaginate photoPaginate){
        Log.e(TAG,new Gson().toJson(photoPaginate));
        this.photoPaginate = photoPaginate;
        this.photos.addAll(photoPaginate.getData());
        CheckNextUserPhoto();
        this.userPhotoCellAdapter.NextData(photoPaginate.getData());
        this.userPhotoCellAdapter.notifyDataSetChanged();
    }

    private void fetchUserFriend(){
        service.FriendList(Auth.getId(),user.getId(),0,"",6).enqueue(FetchUserFriendCallback);
    }

    private void fetchUserFriend(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        Log.e(TAG,new Gson().toJson(userPaginate));
        userFriendCellAdapter = new UserFriendCellAdapter(this,userPaginate.getData(),this);

        if (userPaginate.getTotal() > 0){
            userFriendCell.showButtonSeeAll();
        }else {
            userFriendCell.hideButtonSeeAll();
        }

        if (userFriendCell != null){
            userFriendCell.setAdapter(userFriendCellAdapter);
        }else {

            userFriendCell = adapter.getUserFriendCell();
            userFriendCell.setAdapter(userFriendCellAdapter);

            Log.e("fetchUserFriend adapter",String.valueOf(adapter != null));
            Log.e("and userFriendCell",String.valueOf(userFriendCell != null));

        }
    }

    private void fetchUserInterest(){
        service.CategoryByUser(user.getId()).enqueue(FetchUserInterestCallback);
    }

    private void fetchUserInterest(List<Category> categories){
        userInterestCellAdapter = new UserInterestCellAdapter(this,categories,this);
        if (userInterestCell != null){
            userInterestCell.setAdapter(userInterestCellAdapter);
        }else {

            userInterestCell = adapter.getUserInterestCell();
            userInterestCell.setAdapter(userInterestCellAdapter);

            Log.e("fetchUserInterest",String.valueOf(adapter != null));
            Log.e("and userInterestCell",String.valueOf(userInterestCell != null));

        }
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,UserProfileActivity.class);
    }

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();
        user = new Gson().fromJson(getIntent().getStringExtra(USER),SmUser.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        InitTemp();
        InitSocket();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string._s_profile,getName(user,NameType.FIRST)));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        fabTop = (FloatingActionButton)findViewById(R.id.fabTop);
        fabTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

        adapter = new UserProfileAdapter(this,user,this,this,this,this,this,this,this,this,this);
        loadingView = adapter.getLoadingView();
        userPhotoCell = adapter.getUserPhotoCell();
        userFriendCell = adapter.getUserFriendCell();
        userInterestCell = adapter.getUserInterestCell();

        fetchFeed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        new Handler().postDelayed(RunCallback,3000);
    }

    @Override
    public void onLike(FeedCV view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    @Override
    public void onComment(FeedCV view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onDownload(FeedCV view, Feed feed) {
        view.DisplayDownloadBox(feed);
    }

    @Override
    public void viewDetail(FeedCV view, Feed feed) {
        view.DetailFeedCV(feed);
    }

    @Override
    public void viewProfile(FeedCV view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(FeedCV view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void onClickedMore(FeedCV view, Feed feed, boolean isShow) {

    }

    @Override
    public void onLike(FeedJob view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    @Override
    public void onComment(FeedJob view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onShare(FeedJob view, Feed feed) {
        view.ShareFeed(feed);
    }

    @Override
    public void onMore(FeedJob view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void onDetail(FeedJob view, Feed feed) {
        view.DetailFeedJob(Auth,feed);
    }

    @Override
    public void onApplyNow(FeedJob view, Feed feed) {
        view.ApplyJob(feed);
    }

    @Override
    public void showProfile(FeedJob view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(FeedJob view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void onLike(TagUsJobRelativeLayout view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    private void LikeFeed(SmUser Auth, Feed feed){
        if (!feed.getFeed().isIs_like()){
            int index = feeds.indexOf(feed);
            FeedDetail temp = feed.getFeed();
            temp.setIs_like(true);
            temp.setLike_result("You liked this post.");
            temp.setCount_likes(1);
            feed.setFeed(temp);
            feeds.set(index,feed);
            adapter.notifyDataSetChanged();
            service.LikeFeed(feed.getId(),Auth.getId()).enqueue(new Callback<SmTagFeed.Like>() {
                @Override
                public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                    if (response.isSuccessful()){
                        Log.e(TAG,response.message());
                    }else {
                        try {
                            Log.e(TAG,response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onComment(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onShare(TagUsJobRelativeLayout view, Feed feed) {
        view.ShareFeed(feed);
    }

    @Override
    public void onMore(TagUsJobRelativeLayout view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void showDetail(TagUsJobRelativeLayout view, Feed feed) {
        view.DetailFeedNormal(feed);
    }

    @Override
    public void viewProfile(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(TagUsJobRelativeLayout view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void openWebsite(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenWebsite(feed.getFeed().getSource_url());
    }

    @Override
    public void showGallery(List<Album> albums, int index) {
        ArrayList<String> images = new ArrayList<>();
        for (Album album: albums){
            images.add(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"original"));
        }
        DisplayImage(images,index);
    }

    @Override
    public void onItemSelected(UserFriend view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void onSeeAll(UserFriendCell view) {
        view.OpenFriend(user);
    }

    @Override
    public void onItemSelected(UserInterest view, Category category) {
        view.OpenFilterCategory(category);
    }

    @Override
    public void onItemSelected(UserPhoto view, Photo photo) {
        ArrayList<String> images = new ArrayList<>();
        int index = photos.indexOf(photo);
        for (Photo album: photos){
            images.add(SuperUtil.getAlbumPicture(album.getImage(),"original"));
        }
        DisplayImage(images,index);
    }

    @Override
    public void onSeeAll(UserPhotoCell view) {
        view.OpenGallery(user);
    }

    @Override
    public void saveFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Save Post").setMessage("Are you sure to save this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!feed.getFeed().isIs_save()){
                    int index = feeds.indexOf(feed);
                    FeedDetail temp = feed.getFeed();
                    feed.setIs_save(1);
                    temp.setIs_save(true);
                    feed.setFeed(temp);
                    feeds.set(index,feed);
                    adapter.notifyDataSetChanged();
                    service.SaveFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                        @Override
                        public void onResponse(Call<Feed> call, Response<Feed> response) {
                            if (response.isSuccessful()){

                                Log.w(TAG,response.message());
                            }else {
                                try {
                                    Log.e(TAG,response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Feed> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void editFeed(SmUser user, Feed feed) {
        EditPostNormal(feed);
    }

    @Override
    public void hideFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Hide Post").setMessage("Are you sure to hide this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feeds.remove(feed);
                adapter.notifyDataSetChanged();
                service.HideFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void removeFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Post").setMessage("Are you sure to remove this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feeds.remove(feed);
                adapter.notifyDataSetChanged();
                service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void select(LikeResultCell cell, SmUser user) {
        cell.OpenProfile(Auth,user);
    }

    @Override
    public void connect(LikeResultCell cell, SmUser user) {
        cell.DoFollow(Auth,user);
    }

    @Override
    public void connect(UserHeader view, SmUser user) {
        view.DoFollow(Auth,user);
    }

    @Override
    public void displayProfile(UserHeader view, SmUser user) {
        if (user.getImage() != null){
            ArrayList<String> images = new ArrayList<>();
            images.add(user.getImage().contains("http") ? user.getImage() : SuperUtil.getProfilePicture(user.getImage(),"original"));
            DisplayImage(images,0);
        }
    }

    @Override
    public void displayCover(UserHeader view, SmUser user) {
        if (user.getCover() != null){
            ArrayList<String> images = new ArrayList<>();
            images.add(user.getCover().contains("http") ? user.getCover() : SuperUtil.getCover(user.getCover()));
            DisplayImage(images,0);
        }
    }

    @Override
    public void chat(UserHeader view, SmUser user) {

    }

    @Override
    public void more(UserHeader view, SmUser user) {
        view.MoreUser(view,user,this);
    }

    @Override
    public void about(UserHeader view, SmUser user) {
//        view.OpenWebsite(user.getProfile_link());
    }

    @Override
    public void displayAllPhotos(UserHeader view, DialogFragment fragment, SmUser user) {
        view.OpenGallery(user);
        fragment.dismiss();
    }

    @Override
    public void displayAllFriends(UserHeader view, DialogFragment fragment, SmUser user) {
        view.OpenFriend(user);
        fragment.dismiss();
    }

    @Override
    public void copyProfileLink(UserHeader view, DialogFragment fragment, SmUser user) {
        view.CopyProfileLink(user,fragment);
    }

    @Override
    public void reload(LoadingView view) {
        fetchNextFeed();
    }
}

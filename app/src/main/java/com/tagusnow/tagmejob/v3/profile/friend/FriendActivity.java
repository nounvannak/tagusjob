package com.tagusnow.tagmejob.v3.profile.friend;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.LoadingView;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, TextWatcher, FriendListenter, LoadingListener<LoadingView> {

    private static final String TAG = FriendActivity.class.getSimpleName();
    public static final String USER = "FriendActivity_user";
    private Toolbar toolbar;
    private Button btnAll,btnFollower,btnFollowing;
    private EditText searchBox;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private SmUser Auth,user;
    private FriendAdapter adapter;
    private List<SmUser> users;
    private UserPaginate userPaginate;
    private int type = 0;
    private String searchText = "";
    private LoadingView loadingView;
    private FeedService service = ServiceGenerator.createService(FeedService.class);

    /* Socket.io */
    Socket socket;
    private Emitter.Listener FOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("auth_user");
                            JSONObject json = obj.getJSONObject("user");
                            if (Auth.getId() == userID){
                                if (json != null){
                                    SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                                    SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                                    if (follower != null && following != null){
                                        int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                            List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                            if (lists.size() > 0){
                                                SmUser user = lists.get(0);
                                                int index = users.indexOf(user);
                                                follower.setIs_follow(true);
                                                if (following.getId() == userID){
                                                    users.set(index,follower);
                                                    adapter.notifyDataSetChanged();
                                                }
                                            }
                                        }else {
                                            for (SmUser user: users){
                                                if (user.getId() == id){
                                                    int index = users.indexOf(user);
                                                    follower.setIs_follow(true);
                                                    if (following.getId() == userID){
                                                        users.set(index,follower);
                                                        adapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener UNFOLLOW_USER = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("auth_user");
                            JSONObject json = obj.getJSONObject("user");
                            if (Auth.getId() == userID){
                                if (json != null){
                                    SmUser follower = new Gson().fromJson(json.get("follower").toString(),SmUser.class);
                                    SmUser following = new Gson().fromJson(json.get("following").toString(),SmUser.class);
                                    if (follower != null && following != null){
                                        int id = ((following.getId() == userID) ? follower.getId() : following.getId());
                                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                            List<SmUser> lists = users.stream().filter(t -> t.getId() == id).collect(Collectors.toList());
                                            if (lists.size() > 0){
                                                SmUser user = lists.get(0);
                                                int index = users.indexOf(user);
                                                follower.setIs_follow(false);
                                                if (following.getId() == userID){
                                                    users.set(index,follower);
                                                    adapter.notifyDataSetChanged();
                                                }
                                            }
                                        }else {
                                            for (SmUser user: users){
                                                if (user.getId() == id){
                                                    int index = users.indexOf(user);
                                                    follower.setIs_follow(false);
                                                    if (following.getId() == userID){
                                                        users.set(index,follower);
                                                        adapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Runnable RunCallback = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(false);
            fetchFriend(type,searchText);
        }
    };
    private Callback<UserPaginate> FetchFriendCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                fetchFriend(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };
    private Callback<UserPaginate> FetchNextFriendCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                fetchNextFriend(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    loadingView.setError(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
            loadingView.setError(true);
        }
    };

    private void InitSocket(){
        socket = new App().getSocket();
        socket.on("Follow User",FOLLOW_USER);
        socket.on("Unfollow User",UNFOLLOW_USER);
        socket.connect();
    }
    /* End Socket.io */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,FriendActivity.class);
    }

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();
        user = new Gson().fromJson(getIntent().getStringExtra(USER),SmUser.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);

        InitTemp();
        InitSocket();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_friend_activity,getName(user,NameType.FIRST)));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnAll = (Button)findViewById(R.id.btnAll);
        btnFollower = (Button)findViewById(R.id.btnFollower);
        btnFollowing = (Button)findViewById(R.id.btnFollowing);

        searchBox = (EditText)findViewById(R.id.searchBox);

        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        refreshLayout.setOnRefreshListener(this);
        btnAll.setOnClickListener(this);
        btnFollower.setOnClickListener(this);
        btnFollowing.setOnClickListener(this);

        searchBox.addTextChangedListener(this);

        adapter = new FriendAdapter(this,this,this);
        loadingView = adapter.getLoadingView();

        btnAll.callOnClick();

    }

    private void fetchFriend(int type,String searchText){

        if (type == 0){
            btnAll.setBackgroundResource(R.drawable.button_round_dark_gray_background);
            btnFollower.setBackgroundResource(R.drawable.button_round_light_gray_background);
            btnFollowing.setBackgroundResource(R.drawable.button_round_light_gray_background);
        }else if (type == 1){
            btnAll.setBackgroundResource(R.drawable.button_round_light_gray_background);
            btnFollower.setBackgroundResource(R.drawable.button_round_dark_gray_background);
            btnFollowing.setBackgroundResource(R.drawable.button_round_light_gray_background);
        }else {
            btnAll.setBackgroundResource(R.drawable.button_round_light_gray_background);
            btnFollower.setBackgroundResource(R.drawable.button_round_light_gray_background);
            btnFollowing.setBackgroundResource(R.drawable.button_round_dark_gray_background);
        }

        this.type = type;
        this.searchText = searchText;
        service.FriendList(Auth.getId(),user.getId(),type,searchText,10).enqueue(FetchFriendCallback);
    }

    private void fetchFriend(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users = userPaginate.getData();
        CheckNext();
        adapter.NewData(userPaginate.getData());
        recyclerView.setAdapter(adapter);
        loadingView.setLoading(userPaginate.getTotal() > userPaginate.getTo());
    }

    private void fetchNextFriend(){
        boolean isNext = userPaginate.getCurrent_page() != userPaginate.getLast_page();
        if (isNext){
            int page = userPaginate.getCurrent_page() + 1;
            service.FriendList(Auth.getId(),user.getId(),type,searchText,10,page).enqueue(FetchNextFriendCallback);
        }
    }

    private void fetchNextFriend(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users.addAll(userPaginate.getData());
        CheckNext();
        loadingView.setLoading(userPaginate.getTotal() > userPaginate.getTo());
        adapter.NextData(userPaginate.getData());
        adapter.notifyDataSetChanged();
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    fetchNextFriend();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        new Handler().postDelayed(RunCallback,2000);
    }

    @Override
    public void onClick(View view) {
        if (view == btnAll){
            fetchFriend(0,this.searchText);
        }else if (view == btnFollower){
            fetchFriend(1,this.searchText);
        }else if (view == btnFollowing){
            fetchFriend(2,this.searchText);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        this.searchText = charSequence.toString();
        fetchFriend(this.type,this.searchText);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void connect(FriendCell view, SmUser user) {
        view.DoFollow(Auth,user);
    }

    @Override
    public void selected(FriendCell view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void reload(LoadingView view) {
        fetchNextFriend();
    }
}

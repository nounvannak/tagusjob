package com.tagusnow.tagmejob.v3.profile.friend;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;
import com.tagusnow.tagmejob.view.LoadingHolder;
import com.tagusnow.tagmejob.view.LoadingView;

import java.util.List;

public class FriendAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<SmUser> users;
    private int other = 1;
    private static final int LOADING = 0;
    private static final int USER = 1;
    private static final int EMPTY = 2;

    private LoadingView  loadingView;
    private LoadingListener<LoadingView> loadingListener;

    public FriendAdapter(Activity activity,FriendListenter listenter,LoadingListener<LoadingView> loadingListener) {
        this.activity = activity;
        this.listenter = listenter;
        this.loadingListener = loadingListener;
        this.loadingView = new LoadingView(activity);
    }

    private FriendListenter listenter;

    public void NewData(List<SmUser> users){
        this.users = users;
    }

    public void NextData(List<SmUser> users){
        this.users.addAll(users);
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if (position == (getItemCount() - 1)){
            type = LOADING;
        }else {
            type = USER;
        }
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == LOADING){
            return new LoadingHolder(loadingView);
        }else {
            return new FriendHolder(new FriendCell(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == LOADING){
            ((LoadingHolder) holder).BindView(activity,loadingListener);
        }else {
            ((FriendHolder) holder).BindView(activity,users.get(position),listenter);
        }
    }

    @Override
    public int getItemCount() {
        return users.size() + other;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }
}

package com.tagusnow.tagmejob.v3.profile.friend;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private SmUser user;
    private Context context;

    public void setUser(SmUser user) {
        this.user = user;

        setUserProfile(profile,user);
        setName(name,user);
        counter.setText(context.getString(R.string.follower_counter,user.getFollower_counter()));

        if (user.isIs_follow()){
            btnConnect.setText(R.string.following);
        }else {
            btnConnect.setText(R.string.follow);
        }
    }

    public void setListenter(FriendListenter listenter) {
        this.listenter = listenter;
    }

    private FriendListenter listenter;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private CircleImageView profile;
    private TextView name,counter;
    private Button btnConnect;

    private void InitUI(Context context){
        inflate(context, R.layout.friend_cell,this);
        this.context = context;
        profile = (CircleImageView)findViewById(R.id.profile);
        name = (TextView)findViewById(R.id.name);
        counter = (TextView)findViewById(R.id.counter);
        btnConnect = (Button)findViewById(R.id.btnConnect);

        btnConnect.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public FriendCell(Context context) {
        super(context);
        InitUI(context);
    }

    public FriendCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public FriendCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public FriendCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (btnConnect == view){
            listenter.connect(this,user);
        }else if (this == view){
            listenter.selected(this,user);
        }
    }
}

package com.tagusnow.tagmejob.v3.profile.friend;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class FriendHolder extends RecyclerView.ViewHolder {

    private FriendCell friendCell;

    public FriendHolder(View itemView) {
        super(itemView);
        friendCell = (FriendCell)itemView;
    }

    public void BindView(Activity activity, SmUser user,FriendListenter friendListenter){
        friendCell.setActivity(activity);
        friendCell.setUser(user);
        friendCell.setListenter(friendListenter);
    }
}

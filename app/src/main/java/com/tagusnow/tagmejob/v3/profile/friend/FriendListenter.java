package com.tagusnow.tagmejob.v3.profile.friend;

import com.tagusnow.tagmejob.auth.SmUser;

public interface FriendListenter {
    void connect(FriendCell view, SmUser user);
    void selected(FriendCell view, SmUser user);
}

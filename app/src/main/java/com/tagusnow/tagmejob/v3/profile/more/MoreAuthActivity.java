package com.tagusnow.tagmejob.v3.profile.more;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class MoreAuthActivity extends TagUsJobActivity implements View.OnClickListener {

    private static final String TAG = MoreAuthActivity.class.getSimpleName();
    private SmUser Auth;
    private Toolbar toolbar;
    private RelativeLayout btnPosts,btnSaved,btnExpired;

    public static Intent createIntent(Activity activity){
        return new Intent(activity,MoreAuthActivity.class);
    }

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_auth);

        InitTemp();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnPosts = (RelativeLayout)findViewById(R.id.btnPosts);
        btnSaved = (RelativeLayout)findViewById(R.id.btnSaved);
        btnExpired = (RelativeLayout)findViewById(R.id.btnExpired);

        btnPosts.setOnClickListener(this);
        btnSaved.setOnClickListener(this);
        btnExpired.setOnClickListener(this);

//        if (Auth.getUser_type() == SmUser.FIND_STAFF){
//            btnPosts.setVisibility(View.VISIBLE);
//            btnSaved.setVisibility(View.VISIBLE);
//            btnExpired.setVisibility(View.VISIBLE);
//        }else {
//            btnPosts.setVisibility(View.GONE);
//            btnSaved.setVisibility(View.VISIBLE);
//            btnExpired.setVisibility(View.GONE);
//        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnPosts){
            startActivity(SavedActivity
                    .createIntent(this)
                    .putExtra(SavedActivity.TYPE,SavedActivity.POSTS)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (view == btnSaved){
            startActivity(SavedActivity
                    .createIntent(this)
                    .putExtra(SavedActivity.TYPE,SavedActivity.SAVED)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (view == btnExpired){
            startActivity(SavedActivity
                    .createIntent(this)
                    .putExtra(SavedActivity.TYPE,SavedActivity.EXPIRED)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}

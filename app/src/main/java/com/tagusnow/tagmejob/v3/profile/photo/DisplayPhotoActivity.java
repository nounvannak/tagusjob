package com.tagusnow.tagmejob.v3.profile.photo;

import android.os.Bundle;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class DisplayPhotoActivity extends TagUsJobActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_photo);
    }
}

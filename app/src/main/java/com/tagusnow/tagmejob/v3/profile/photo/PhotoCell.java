package com.tagusnow.tagmejob.v3.profile.photo;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.v3.profile.Photo;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class PhotoCell extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Photo photo;

    public void setPhoto(Photo photo) {
        this.photo = photo;
        setAlbumPhoto(imageView,photo.getImage(),"500",false);
    }

    public void setListener(PhotoListener listener) {
        this.listener = listener;
    }

    private PhotoListener listener;
    private ImageView imageView;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.photo_cell,this);
        imageView = (ImageView)findViewById(R.id.imageView);
        imageView.setOnClickListener(this);
    }

    public PhotoCell(Context context) {
        super(context);
        InitUI(context);
    }

    public PhotoCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public PhotoCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public PhotoCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == imageView){
            listener.selected(this,photo);
        }
    }
}

package com.tagusnow.tagmejob.v3.profile.photo;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.profile.Photo;

public class PhotoHolder extends RecyclerView.ViewHolder {

    private PhotoCell photoCell;

    public PhotoHolder(View itemView) {
        super(itemView);
        photoCell = (PhotoCell)itemView;
    }

    public void BindView(Activity activity, Photo photo,PhotoListener photoListener){
        photoCell.setActivity(activity);
        photoCell.setPhoto(photo);
        photoCell.setListener(photoListener);
    }
}

package com.tagusnow.tagmejob.v3.profile.photo;

import com.tagusnow.tagmejob.v3.profile.Photo;

public interface PhotoListener {
    void selected(PhotoCell view, Photo photo);
}

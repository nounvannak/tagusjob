package com.tagusnow.tagmejob.v3.profile.photo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.profile.Photo;
import com.tagusnow.tagmejob.v3.profile.PhotoPaginate;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserPhotoActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, PhotoListener {

    private static final String TAG = UserPhotoActivity.class.getSimpleName();
    public static final String USER = "UserPhotoActivity_user";
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private SmUser user;
    private List<Photo> photos;
    private PhotoPaginate photoPaginate;
    private UserPhotoAdapter adapter;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<PhotoPaginate> FetchPhotoCallback = new Callback<PhotoPaginate>() {
        @Override
        public void onResponse(Call<PhotoPaginate> call, Response<PhotoPaginate> response) {
            if (response.isSuccessful()){
                Log.d(TAG,new Gson().toJson(response.body()));

                fetchPhoto(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<PhotoPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<PhotoPaginate> FetchNextPhotoCallback = new Callback<PhotoPaginate>() {
        @Override
        public void onResponse(Call<PhotoPaginate> call, Response<PhotoPaginate> response) {
            if (response.isSuccessful()){
                fetchNextPhoto(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<PhotoPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Runnable RunCallback = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(false);
            fetchPhoto();
        }
    };

    public static Intent createIntent(Activity activity){
        return new Intent(activity,UserPhotoActivity.class);
    }

    private void InitTemp(){
        user = new Gson().fromJson(getIntent().getStringExtra(USER),SmUser.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_photo);

        InitTemp();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_photo_activity,getName(user,NameType.FIRST)));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this,3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        refreshLayout.setOnRefreshListener(this);

        fetchPhoto();
    }

    private void fetchPhoto(){
        service.GetUserImagePost(user.getId(),10).enqueue(FetchPhotoCallback);
    }

    private void fetchPhoto(PhotoPaginate photoPaginate){
        this.photoPaginate = photoPaginate;
        this.photos = photoPaginate.getData();
        CheckNext();
        adapter = new UserPhotoAdapter(this,photoPaginate.getData(),this);
        if (photos.size() == 0){
            RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(manager);
        }
        recyclerView.setAdapter(adapter);
    }

    private void fetchNextPhoto(){
        boolean isNext = photoPaginate.getCurrent_page() != photoPaginate.getLast_page();
        if (isNext){
            int page = photoPaginate.getCurrent_page() + 1;
            service.GetUserImagePost(user.getId(),10,page).enqueue(FetchNextPhotoCallback);
        }
    }

    private void fetchNextPhoto(PhotoPaginate photoPaginate){
        this.photoPaginate = photoPaginate;
        this.photos.addAll(photoPaginate.getData());
        CheckNext();
        adapter.NextData(photoPaginate.getData());
        adapter.notifyDataSetChanged();
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (photoPaginate.getTo() - 1)) && (photoPaginate.getTo() < photoPaginate.getTotal())) {
                        if (photoPaginate.getNext_page_url()!=null || !photoPaginate.getNext_page_url().equals("")){
                            if (lastPage!=photoPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = photoPaginate.getCurrent_page();
                                    fetchNextPhoto();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        new Handler().postDelayed(RunCallback,2000);
    }

    @Override
    public void selected(PhotoCell view, Photo photo) {
        ArrayList<String> images = new ArrayList<>();
        int index = photos.indexOf(photo);
        for (Photo album: photos){
            images.add(SuperUtil.getAlbumPicture(album.getImage(),"original"));
        }
        DisplayImage(images,index);
    }
}

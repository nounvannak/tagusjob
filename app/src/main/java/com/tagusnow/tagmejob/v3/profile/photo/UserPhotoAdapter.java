package com.tagusnow.tagmejob.v3.profile.photo;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.v3.empty.EmptyHolder;
import com.tagusnow.tagmejob.v3.empty.EmptyView;
import com.tagusnow.tagmejob.v3.profile.Photo;

import java.util.List;

public class UserPhotoAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<Photo> photos;

    public UserPhotoAdapter(Activity activity, List<Photo> photos, PhotoListener listener) {
        this.activity = activity;
        this.photos = photos;
        this.listener = listener;
    }

    public void NextData(List<Photo> photos){
        this.photos.addAll(photos);
    }

    private PhotoListener listener;
    private static final int EMPTY = 0;
    private static final int PHOTO = 1;

    @Override
    public int getItemViewType(int position) {
        return photos.size() > 0 ? PHOTO : EMPTY;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == PHOTO){
            return new PhotoHolder(new PhotoCell(activity));
        }else {
            return new EmptyHolder(new EmptyView(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == PHOTO){
            ((PhotoHolder) holder).BindView(activity,photos.get(position),listener);
        }else {
            ((EmptyHolder) holder).BindView(activity);
        }
    }

    @Override
    public int getItemCount() {
        return photos.size() > 0 ? photos.size() : 1;
    }
}

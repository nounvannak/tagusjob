package com.tagusnow.tagmejob.v3.search;

import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface OnItemListener<V extends TagUsJobRelativeLayout,Z extends Object> {
    void onItemSelected(V view,Z object);
}

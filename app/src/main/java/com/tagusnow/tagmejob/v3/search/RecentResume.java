package com.tagusnow.tagmejob.v3.search;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecentResume extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private CircleImageView profile;
    private TextView name,gender,salary,position;
    private RecentSearch recentSearch;
    private OnItemListener<RecentResume, RecentSearch> onItemListener;

    private void InitUI(Context context){
        inflate(context, R.layout.recent_resume,this);
        this.context = context;

        profile = (CircleImageView)findViewById(R.id.profile);
        name = (TextView)findViewById(R.id.name);
        gender = (TextView)findViewById(R.id.gender);
        salary = (TextView)findViewById(R.id.salary);
        position = (TextView)findViewById(R.id.position);

        this.setOnClickListener(this);
    }

    public RecentResume(Context context) {
        super(context);
        InitUI(context);
    }

    public RecentResume(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public RecentResume(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public RecentResume(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setOnItemListener(OnItemListener<RecentResume, RecentSearch> onItemListener) {
        this.onItemListener = onItemListener;
    }

    @Override
    public void onClick(View v) {
        if (v == this){
            onItemListener.onItemSelected(this,recentSearch);
        }
    }

    public void setRecentSearch(RecentSearch recentSearch) {
        this.recentSearch = recentSearch;
        setName(name,recentSearch.getFeed().getUser_post());
        setAlbumPhoto(profile,(recentSearch.getFeed().getFeed().getAlbum() != null && recentSearch.getFeed().getFeed().getAlbum().size() > 0) ? recentSearch.getFeed().getFeed().getAlbum().get(0) : null);
        gender.setText(recentSearch.getFeed().getUser_post().getGender());
        position.setText(recentSearch.getFeed().getFeed().getCategory());
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }
}

package com.tagusnow.tagmejob.v3.search;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.RecentSearch;

public class RecentResumeHolder extends RecyclerView.ViewHolder {

    private RecentResume recentResume;

    public RecentResumeHolder(View itemView) {
        super(itemView);
        recentResume = (RecentResume)itemView;
    }

    public void BindView(Activity activity, RecentSearch recentSearch, OnItemListener<RecentResume,RecentSearch> onItemListener){
        recentResume.setActivity(activity);
        recentResume.setRecentSearch(recentSearch);
        recentResume.setOnItemListener(onItemListener);
    }
}

package com.tagusnow.tagmejob.v3.search;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class RecentText extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private TextView text;
    private RecentSearch recentSearch;
    private OnItemListener<RecentText,RecentSearch> onItemClickListener;

    private void InitUI(Context context){
        inflate(context, R.layout.recent_text,this);
        this.context = context;
        text = (TextView)findViewById(R.id.text);

        this.setOnClickListener(this);
    }


    public RecentText(Context context) {
        super(context);
        InitUI(context);
    }

    public RecentText(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public RecentText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public RecentText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setRecentSearch(RecentSearch recentSearch) {
        this.recentSearch = recentSearch;
        text.setText(recentSearch.getSearch_text() != null ? recentSearch.getSearch_text() : "No text");
    }

    @Override
    public void onClick(View v) {
        if (v == this){
            onItemClickListener.onItemSelected(this,recentSearch);
        }
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setOnItemClickListener(OnItemListener<RecentText, RecentSearch> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}

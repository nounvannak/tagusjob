package com.tagusnow.tagmejob.v3.search;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.RecentSearch;

public class RecentTextHolder extends RecyclerView.ViewHolder {

    private RecentText recentText;

    public RecentTextHolder(View itemView) {
        super(itemView);
        recentText = (RecentText)itemView;
    }

    public void BindView(Activity activity, RecentSearch recentSearch, OnItemListener<RecentText, RecentSearch> onItemListener){
        recentText.setActivity(activity);
        recentText.setRecentSearch(recentSearch);
        recentText.setOnItemClickListener(onItemListener);
    }
}

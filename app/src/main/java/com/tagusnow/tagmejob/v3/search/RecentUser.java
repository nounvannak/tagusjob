package com.tagusnow.tagmejob.v3.search;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class RecentUser extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ImageView profile;
    private TextView name;
    private RecentSearch recentSearch;
    private OnItemListener<RecentUser, RecentSearch> onItemListener;

    private void InitUI(Context context){
        inflate(context, R.layout.recent_user,this);

        this.context = context;
        profile = (ImageView)findViewById(R.id.profile);
        name = (TextView)findViewById(R.id.name);

        this.setOnClickListener(this);
    }

    public RecentUser(Context context) {
        super(context);
        InitUI(context);
    }

    public RecentUser(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public RecentUser(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public RecentUser(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setOnItemListener(OnItemListener<RecentUser, RecentSearch> onItemListener) {
        this.onItemListener = onItemListener;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setRecentSearch(RecentSearch recentSearch) {
        this.recentSearch = recentSearch;

        setName(name,recentSearch.getUser());
        setUserProfile(profile,recentSearch.getUser());
    }

    @Override
    public void onClick(View v) {
        if (v == this){
            onItemListener.onItemSelected(this,recentSearch);
        }
    }
}

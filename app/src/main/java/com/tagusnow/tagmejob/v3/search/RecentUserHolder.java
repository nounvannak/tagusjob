package com.tagusnow.tagmejob.v3.search;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.RecentSearch;

public class RecentUserHolder extends RecyclerView.ViewHolder {

    private RecentUser recentUser;

    public RecentUserHolder(View itemView) {
        super(itemView);
        recentUser = (RecentUser)itemView;
    }

    public void BindView(Activity activity, RecentSearch recentSearch, OnItemListener<RecentUser, RecentSearch> onItemListener){
        recentUser.setActivity(activity);
        recentUser.setRecentSearch(recentSearch);
        recentUser.setOnItemListener(onItemListener);
    }
}

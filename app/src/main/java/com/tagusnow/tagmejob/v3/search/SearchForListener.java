package com.tagusnow.tagmejob.v3.search;

import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.SearchForLayout;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface SearchForListener<V extends TagUsJobRelativeLayout> {
    void SearchForPeople(V view);
    void SearchForCompany(V view);
    void SearchForStaffAndJob(V view);
    void SearchForContent(V view);
    void showMore(V view);
}

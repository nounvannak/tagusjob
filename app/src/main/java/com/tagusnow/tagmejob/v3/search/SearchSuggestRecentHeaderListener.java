package com.tagusnow.tagmejob.v3.search;

import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestRecentHeader;

public interface SearchSuggestRecentHeaderListener {
    void edit(SearchSuggestRecentHeader view);
}

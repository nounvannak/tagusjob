package com.tagusnow.tagmejob.v3.search;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public interface SuggestionListener<V extends TagUsJobRelativeLayout> {
    void showMore(V view);
    void applyJob(V view, Feed feed);
    void download(V view, Feed feed);
    void chat(V view, Feed feed);
    void viewDetail(V view, Feed feed);
}

package com.tagusnow.tagmejob.v3.search.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.view.Search.Main.Holder.JobsSuggestionHolder;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.JobsSuggestionView;

import java.util.List;

public class JobsSuggestionAdapter extends RecyclerView.Adapter<JobsSuggestionHolder> {

    private Activity activity;
    private List<Feed> feeds;
    private SuggestionListener suggestionListener;

    public JobsSuggestionAdapter(Activity activity, List<Feed> feeds, SuggestionListener suggestionListener) {
        this.activity = activity;
        this.feeds = feeds;
        this.suggestionListener = suggestionListener;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public JobsSuggestionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new JobsSuggestionHolder(new JobsSuggestionView(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull JobsSuggestionHolder holder, int position) {
        holder.BindView(activity,feeds.get(position),suggestionListener);
    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }
}

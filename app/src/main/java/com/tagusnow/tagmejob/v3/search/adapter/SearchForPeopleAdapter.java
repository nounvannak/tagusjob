package com.tagusnow.tagmejob.v3.search.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.user.UserCardThree;
import com.tagusnow.tagmejob.v3.user.UserCardThreeHolder;
import com.tagusnow.tagmejob.v3.user.UserCardThreeListener;

import java.util.List;

public class SearchForPeopleAdapter extends RecyclerView.Adapter<UserCardThreeHolder> {

    private Activity activity;
    private List<SmUser> users;
    private UserCardThreeListener userCardListener;

    public SearchForPeopleAdapter(Activity activity, List<SmUser> users, UserCardThreeListener userCardListener) {
        this.activity = activity;
        this.users = users;
        this.userCardListener = userCardListener;
    }

    public void NextData(List<SmUser> users){
        this.users.addAll(users);
    }

    @NonNull
    @Override
    public UserCardThreeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserCardThreeHolder(new UserCardThree(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull UserCardThreeHolder holder, int position) {
        holder.BindView(activity,users.get(position),userCardListener);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}

package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.v3.search.SearchForListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class SearchForCompany extends TagUsJobRelativeLayout implements View.OnClickListener {
    private Context context;
    public RecyclerView recyclerView;
    private SearchForCompanyAdapter adapter;
    private TextView title;
    private SearchForListener<SearchForCompany> searchForListener;

    private void InitUI(Context context){
        inflate(context, R.layout.search_for_company,this);
        this.context = context;
        title = (TextView)findViewById(R.id.title);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        title.setOnClickListener(this);
    }

    public void setAdapter(SearchForCompanyAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    public void setTotal(int total){
        title.setText(context.getString(R.string.company_d,total));
    }

    @Override
    public void onClick(View v) {
        if (v == title){
            searchForListener.showMore(this);
        }
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setSearchForListener(SearchForListener<SearchForCompany> searchForListener) {
        this.searchForListener = searchForListener;
    }

    public SearchForCompany(Context context) {
        super(context);
        InitUI(context);
    }

    public SearchForCompany(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public SearchForCompany(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public SearchForCompany(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }
}

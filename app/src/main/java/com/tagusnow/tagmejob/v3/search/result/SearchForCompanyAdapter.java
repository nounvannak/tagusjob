package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.v3.company.CompanyCard;
import com.tagusnow.tagmejob.v3.company.CompanyCardHolder;
import com.tagusnow.tagmejob.v3.company.CompanyListener;

import java.util.List;

public class SearchForCompanyAdapter extends RecyclerView.Adapter<CompanyCardHolder> {

    private Activity activity;
    private List<Company> companies;
    private CompanyListener<CompanyCard> companyListener;

    public SearchForCompanyAdapter(Activity activity, List<Company> companies, CompanyListener<CompanyCard> companyListener) {
        this.activity = activity;
        this.companies = companies;
        this.companyListener = companyListener;
    }

    public void NextData(List<Company> companies){
        this.companies.addAll(companies);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CompanyCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CompanyCardHolder(new CompanyCard(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyCardHolder holder, int position) {
        holder.BindView(activity,companies.get(position),companyListener);
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }
}

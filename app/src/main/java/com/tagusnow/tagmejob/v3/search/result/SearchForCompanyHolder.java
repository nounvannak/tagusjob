package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.search.SearchForListener;

public class SearchForCompanyHolder extends RecyclerView.ViewHolder {

    private SearchForCompany searchForCompany;

    public SearchForCompanyHolder(View itemView) {
        super(itemView);
        searchForCompany = (SearchForCompany)itemView;
    }

    public void BindView(Activity activity, SearchForListener<SearchForCompany> searchForListener){
        searchForCompany.setActivity(activity);
        searchForCompany.setSearchForListener(searchForListener);
    }
}

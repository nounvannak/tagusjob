package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.v3.search.SearchForListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class SearchForContent extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private TextView title;
    private RecyclerView recyclerView;
    private SearchForContentAdapter adapter;
    private SearchForListener<SearchForContent> searchForListener;

    private void InitUI(Context context){
        inflate(context, R.layout.search_for_content,this);
        this.context = context;
        title = (TextView)findViewById(R.id.title);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        title.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setTotal(int total){
        title.setText(context.getString(R.string.contents_d,total));
    }

    public SearchForContent(Context context) {
        super(context);
        InitUI(context);
    }

    public SearchForContent(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public SearchForContent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public SearchForContent(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View v) {
        if (v == title){
            searchForListener.showMore(this);
        }
    }

    public void setSearchForListener(SearchForListener<SearchForContent> searchForListener) {
        this.searchForListener = searchForListener;
    }

    public void setAdapter(SearchForContentAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }
}

package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.view.Search.UI.Face.Normal.SearchUIFaceNormal;
import com.tagusnow.tagmejob.view.Search.UI.Face.Normal.SearchUIFeedEmbedURL;
import com.tagusnow.tagmejob.view.Search.UI.Holder.Normal.SearchUIFaceNormalHolder;
import com.tagusnow.tagmejob.view.Search.UI.Holder.Normal.SearchUIFeedEmbedURLHolder;

import java.util.List;

public class SearchForContentAdapter extends RecyclerView.Adapter {

    private static final int NORMAL = 0;
    private static final int EMBED = 1;
    private Activity activity;
    private List<Feed> feeds;
    private FeedNormalListener feedNormalListener;
    private GridViewListener gridViewListener;

    public SearchForContentAdapter(Activity activity, List<Feed> feeds, FeedNormalListener feedNormalListener, GridViewListener gridViewListener) {
        this.activity = activity;
        this.feeds = feeds;
        this.feedNormalListener = feedNormalListener;
        this.gridViewListener = gridViewListener;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
    }

    @Override
    public int getItemViewType(int position) {
        if (feeds.get(position).getFeed().getType_link() != null && !feeds.get(position).getFeed().getType_link().equals("")){
            return EMBED;
        }else {
            return NORMAL;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == NORMAL){
            return new SearchUIFaceNormalHolder(new SearchUIFaceNormal(activity));
        }else {
            return new SearchUIFeedEmbedURLHolder(new SearchUIFeedEmbedURL(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==NORMAL){
            ((SearchUIFaceNormalHolder) holder).bindView(activity,feeds.get(position),feedNormalListener,gridViewListener);
        }else {
            ((SearchUIFeedEmbedURLHolder) holder).BindView(activity,feeds.get(position),feedNormalListener);
        }
    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }
}

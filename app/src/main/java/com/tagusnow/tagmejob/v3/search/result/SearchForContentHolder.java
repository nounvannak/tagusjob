package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.search.SearchForListener;

public class SearchForContentHolder extends RecyclerView.ViewHolder {

    private SearchForContent searchForContent;

    public SearchForContentHolder(View itemView) {
        super(itemView);
        searchForContent = (SearchForContent)itemView;
    }

    public void BindView(Activity activity, SearchForListener<SearchForContent> searchForListener){
        searchForContent.setActivity(activity);
        searchForContent.setSearchForListener(searchForListener);
    }
}

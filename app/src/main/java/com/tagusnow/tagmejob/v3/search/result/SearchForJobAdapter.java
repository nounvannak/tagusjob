package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJob;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobListener;

import java.util.List;

public class SearchForJobAdapter extends RecyclerView.Adapter<SmallFeedJobHolder> {

    private Activity activity;
    private List<Feed> feeds;

    public SearchForJobAdapter(Activity activity, List<Feed> feeds, SmallFeedJobListener smallFeedJobListener) {
        this.activity = activity;
        this.feeds = feeds;
        this.smallFeedJobListener = smallFeedJobListener;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
        notifyDataSetChanged();
    }

    private SmallFeedJobListener smallFeedJobListener;

    @NonNull
    @Override
    public SmallFeedJobHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SmallFeedJobHolder(new SmallFeedJob(activity));
    }

    @Override
    public void onBindViewHolder(@NonNull SmallFeedJobHolder holder, int position) {
        holder.BindView(activity,feeds.get(position),smallFeedJobListener);
    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }
}

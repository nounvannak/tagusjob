package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.search.SearchForListener;

public class SearchForJobHolder extends RecyclerView.ViewHolder {

    private SearchForJob searchForJob;

    public SearchForJobHolder(View itemView) {
        super(itemView);
        searchForJob = (SearchForJob)itemView;
    }

    public void BindView(Activity activity, SearchForListener<SearchForJob> searchForListener){
        searchForJob.setActivity(activity);
        searchForJob.setSearchForListener(searchForListener);
    }
}

package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.search.SearchForListener;

public class SearchForPeopleHolder extends RecyclerView.ViewHolder {

    private SearchForPeople searchForPeople;

    public SearchForPeopleHolder(View itemView) {
        super(itemView);
        searchForPeople = (SearchForPeople)itemView;
    }

    public void BindView(Activity activity, SearchForListener<SearchForPeople> searchForListener){
        searchForPeople.setActivity(activity);
        searchForPeople.setSearchForListener(searchForListener);
    }
}

package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.v3.search.SearchForListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class SearchForStaff extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private TextView title;
    private RecyclerView recyclerView;
    private SearchForListener<SearchForStaff> searchForListener;
    private SearchForStaffAdapter adapter;

    public void setSearchForListener(SearchForListener<SearchForStaff> searchForListener) {
        this.searchForListener = searchForListener;
    }

    public void setAdapter(SearchForStaffAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    public void setTotal(int total){
        title.setText(context.getString(R.string.staffs_d,total));
    }

    private void InitUI(Context context){
        try {
            inflate(context,R.layout.search_for_stafff,this);
            this.context = context;

            title = (TextView)findViewById(R.id.title);
            recyclerView = (RecyclerView)findViewById(R.id.recycler);
            RecyclerView.LayoutManager manager = new GridLayoutManager(context,3,LinearLayoutManager.VERTICAL,false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(manager);

            title.setOnClickListener(this);
        }catch (Exception e){
            Log.e("error",e.getMessage());
        }
    }

    public SearchForStaff(Context context) {
        super(context);
        InitUI(context);
    }

    public SearchForStaff(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public SearchForStaff(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public SearchForStaff(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view == title){
            searchForListener.showMore(this);
        }
    }
}

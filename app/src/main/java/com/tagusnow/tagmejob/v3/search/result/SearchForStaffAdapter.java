package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.company.CompanyCard;
import com.tagusnow.tagmejob.v3.company.CompanyCardHolder;
import com.tagusnow.tagmejob.v3.company.CompanyListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVHolder;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.view.Search.Main.Holder.StaffSuggestionViewHolder;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.StaffSuggestionView;

import java.util.List;

public class SearchForStaffAdapter extends RecyclerView.Adapter<StaffSuggestionViewHolder> {

    private Activity activity;
    private List<Feed> feeds;
    private StaffSuggestionView staffSuggestionView;

    public SearchForStaffAdapter(Activity activity, List<Feed> feeds, SuggestionListener<StaffSuggestionView> suggestionListener) {
        this.activity = activity;
        this.feeds = feeds;
        this.staffSuggestionView = new StaffSuggestionView(activity);
        this.suggestionListener = suggestionListener;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
        notifyDataSetChanged();
    }

    private SuggestionListener<StaffSuggestionView> suggestionListener;

    @NonNull
    @Override
    public StaffSuggestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StaffSuggestionViewHolder(staffSuggestionView);
    }

    @Override
    public void onBindViewHolder(@NonNull StaffSuggestionViewHolder holder, int position) {
        holder.BindView(activity,feeds.get(position),suggestionListener);
    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }

    public StaffSuggestionView getStaffSuggestionView() {
        return staffSuggestionView;
    }
}

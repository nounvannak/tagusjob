package com.tagusnow.tagmejob.v3.search.result;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.search.SearchForListener;

public class SearchForStaffHolder extends RecyclerView.ViewHolder {

    private SearchForStaff searchForStaff;

    public SearchForStaffHolder(View itemView) {
        super(itemView);
        searchForStaff = (SearchForStaff)itemView;
    }

    public void BindView(Activity activity, SearchForListener<SearchForStaff> searchForListener){
        searchForStaff.setActivity(activity);
        searchForStaff.setSearchForListener(searchForListener);
    }
}

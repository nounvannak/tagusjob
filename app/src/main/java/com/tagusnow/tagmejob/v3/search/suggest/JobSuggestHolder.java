package com.tagusnow.tagmejob.v3.search.suggest;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;

public class JobSuggestHolder extends RecyclerView.ViewHolder {

    private JobSuggest jobSuggest;

    public JobSuggestHolder(View itemView) {
        super(itemView);
        jobSuggest = (JobSuggest)itemView;
    }

    public void BindView(Activity activity, Feed feed, SuggestionListener<JobSuggest> suggestionListener){
        jobSuggest.setActivity(activity);
        jobSuggest.setFeed(feed);
        jobSuggest.setSuggestionListener(suggestionListener);
    }
}

package com.tagusnow.tagmejob.v3.search.suggest;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.StaffSuggestionView;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;

public class StaffSuggest extends TagUsJobRelativeLayout implements View.OnClickListener {
    private Context context;
    private ShapedImageView profile;
    private TextView name,type,location;
    private RelativeLayout btnChat,btnDownload;
    private Feed feed;
    private SuggestionListener<StaffSuggest> suggestionListener;

    private void InitUI(Context context){
        inflate(context, R.layout.staff_suggest,this);
        this.context = context;

        profile = (ShapedImageView)findViewById(R.id.profile);

        name = (TextView)findViewById(R.id.name);
        type = (TextView)findViewById(R.id.type);
        location = (TextView)findViewById(R.id.location);

        btnChat = (RelativeLayout)findViewById(R.id.btnChat);
        btnDownload = (RelativeLayout)findViewById(R.id.btnDownload);

        btnChat.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public StaffSuggest(Context context) {
        super(context);
        InitUI(context);
    }

    public StaffSuggest(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public StaffSuggest(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public StaffSuggest(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        setName(name,feed.getUser_post());
        type.setText(feed.getFeed().getTitle());
        location.setText(feed.getFeed().getCity_name());
        setAlbumPhoto(profile,(feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0) ? feed.getFeed().getAlbum().get(0) : null);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (btnDownload==v){
            suggestionListener.download(this,feed);
        }else if (btnChat==v){
            suggestionListener.chat(this,feed);
        }else if (this==v){
            suggestionListener.viewDetail(this,feed);
        }
    }

    public void setSuggestionListener(SuggestionListener<StaffSuggest> suggestionListener) {
        this.suggestionListener = suggestionListener;
    }
}

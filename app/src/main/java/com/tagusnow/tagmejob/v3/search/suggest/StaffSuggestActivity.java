package com.tagusnow.tagmejob.v3.search.suggest;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.V2.Model.Location;
import com.tagusnow.tagmejob.V2.Model.LocationPaginate;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.model.v2.feed.Setting;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.v3.category.Filter;
import com.tagusnow.tagmejob.v3.category.FilterAdapter;
import com.tagusnow.tagmejob.v3.category.FilterCategoryHeader;
import com.tagusnow.tagmejob.v3.category.FilterCategoryListener;
import com.tagusnow.tagmejob.v3.category.FilterCell;
import com.tagusnow.tagmejob.v3.category.FilterHeader;
import com.tagusnow.tagmejob.v3.category.FilterListener;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.like.LikeResultCell;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.v3.search.suggest.adapter.StaffSuggestAdapter;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffSuggestActivity extends TagUsJobActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, FilterListener, FilterCategoryListener, TextWatcher, SuggestionListener<StaffSuggest>, FeedCVListener, LikeResultListener {

    private static final String TAG = StaffSuggestActivity.class.getSimpleName();
    private Toolbar toolbar;
    private FilterCategoryHeader header;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private SmUser Auth;
    private FeedResponse feedResponse;
    private StaffSuggestAdapter adapter;
    private List<Feed> feeds;
    private String searchText = "";
    private List<String> categories,cities,levels,workTypes,salaries;
    private List<String> tagsOfCategory = new ArrayList<>(),
            tagsOfCity = new ArrayList<>(),
            tagsOfLevels = new ArrayList<>(),
            tagsOfWorkType = new ArrayList<>(),
            tagsOfSalary = new ArrayList<>();
    private List<Location> rawLocations;
    private Setting rawLevel,rawWorkType;
    private FilterAdapter filterAdapter;
    private List<Filter> filters = new ArrayList<>();
    private List<MultipartBody.Part> request = new ArrayList<>();
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<FeedResponse> FetchFeedCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchNextFeedCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchNextFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void preparedRequestBody(){

        if (this.categories!=null && this.categories.size() > 0){
            for (String value : this.categories){
                int id = this.categories.indexOf(value);
                request.add(okhttp3.MultipartBody.Part.createFormData("category_id[]",String.valueOf(id)));
            }
        }else {
            request.add(okhttp3.MultipartBody.Part.createFormData("category_id","0"));
        }

        if (this.cities!=null && this.cities.size() > 0){
            for (String value : this.cities){
                int id = this.cities.indexOf(value);
                request.add(okhttp3.MultipartBody.Part.createFormData("city[]",String.valueOf(id)));
            }
        }else {
            request.add(okhttp3.MultipartBody.Part.createFormData("city","0"));
        }

        if (this.levels!=null && this.levels.size() > 0){
            for (String value : this.levels){
                request.add(okhttp3.MultipartBody.Part.createFormData("level[]",value));
            }
        }else {
            request.add(okhttp3.MultipartBody.Part.createFormData("level","All Levels"));
        }

        if (this.workTypes!=null && this.workTypes.size() > 0){
            for (String value : this.workTypes){
                request.add(okhttp3.MultipartBody.Part.createFormData("work_time[]",value));
            }
        }else {
            request.add(okhttp3.MultipartBody.Part.createFormData("work_time","Other"));
        }

        if (this.salaries!=null && this.salaries.size() > 0){
            for (String value : this.salaries){
                request.add(okhttp3.MultipartBody.Part.createFormData("salary[]",value));
            }
        }else {
            request.add(okhttp3.MultipartBody.Part.createFormData("salary","Other"));
        }
    }

    /* Socket.io */
    Socket socket;
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalLikes = obj.getInt("count_likes");
                            int userID = obj.getInt("user_id");
                            String likesResult = obj.getString("like_result");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);

                                    feedDetail.setCount_likes(totalLikes);
                                    feedDetail.setLike_result(likesResult);
                                    feedDetail.setIs_like(Auth.getId() == userID);

                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);

                                        feedDetail.setCount_likes(totalLikes);
                                        feedDetail.setLike_result(likesResult);
                                        feedDetail.setIs_like(Auth.getId() == userID);

                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });

        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalComments = obj.getInt("count_comments");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);
                                    feedDetail.setCount_comments(totalComments);
                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);
                                        feedDetail.setCount_comments(totalComments);
                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        int index = feeds.indexOf(feed);
                                        Feed updateFeed = feeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        feeds.set(index,updateFeed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            int index = feeds.indexOf(feed);
                                            Feed updateFeed = feeds.get(index);
                                            updateFeed.setFeed(feedJson.getFeed());
                                            feeds.set(index,updateFeed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        feeds.remove(feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("user_id");
                            if (Auth.getId() == userID){
                                Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                                if (feedJson != null){
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                        List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                        if (lists.size() > 0){
                                            Feed feed = lists.get(0);
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }else {
                                        for (Feed feed: feeds){
                                            if (feed.getId() == feedJson.getId()){
                                                feeds.remove(feed);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Runnable RunCallback = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(false);
            fetchFeed();

        }
    };
    private void InitSocket(){
        socket = new App().getSocket();
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.connect();
    }
    /* End Socket.io */

    private void InitTemp(){
        Auth = new Auth(this).checkAuth().token();

        SmTagSubCategory smTagSubCategory = new Repository(this).restore().getCategory();

        if (smTagSubCategory != null){
            if (smTagSubCategory.getData() != null && smTagSubCategory.getData().size() > 0){
                for (Category category: smTagSubCategory.getData()){
                    tagsOfCategory.add(category.getTitle());
                }

                filters.add(new Filter(R.drawable.menu_color, FilterListener.FilterType.POSITION,"Position",tagsOfCategory));
            }
        }

        LocationPaginate locationPaginate = new Gson().fromJson(new Gson().toJson(new Repository(this).restore().getLocation()),LocationPaginate.class);
        if (locationPaginate != null){
            rawLocations = locationPaginate.getData();
        }

        if (rawLocations != null && rawLocations.size() > 0){
            for (Location location : rawLocations){
                tagsOfCity.add(location.getName());
            }
            filters.add(new Filter(R.drawable.map_color, FilterListener.FilterType.CITY,"Location",tagsOfCity));
        }

        rawLevel = new Repository(this).restore().getLevel();
        rawWorkType = new Repository(this).restore().getWorkingTime();

        if (rawLevel != null){
            tagsOfLevels.addAll(Arrays.asList(rawLevel.getValue().split(",")));
            filters.add(new Filter(R.drawable.list_color, FilterListener.FilterType.WORKTYPE,"Level",tagsOfLevels));
        }

        if (rawWorkType != null){
            tagsOfWorkType.addAll(Arrays.asList(rawWorkType.getValue().split(",")));
            filters.add(new Filter(R.drawable.alarm_clock_color, FilterListener.FilterType.WORKTYPE,"Work Type",tagsOfWorkType));
        }

        tagsOfSalary.add("Negotiation");
        tagsOfSalary.add("Less 500$");
        tagsOfSalary.add("Less 1,000$");
        tagsOfSalary.add("Less 2,000$");
        tagsOfSalary.add("2,000$ Up");
        tagsOfSalary.add("Other");
        filters.add(new Filter(R.drawable.diamond_color, FilterListener.FilterType.SALARY,"Salary",tagsOfSalary));
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,StaffSuggestActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_suggest);

        InitTemp();
        InitSocket();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab = (FloatingActionButton)findViewById(R.id.fabTop);
        fab.setOnClickListener(this);

        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView)findViewById(R.id.recycler1);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this,3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        header = (FilterCategoryHeader)findViewById(R.id.header);
        header.setActivity(this);
        header.setFilterListener(this);
        header.setListener(this);
        header.setTextWatcher(this);
//        header.hideControl();

        filterAdapter = new FilterAdapter(this,this,filters);
        header.setAdapter(filterAdapter);

        fetchFeed();

    }

    private void fetchFeed(){
        preparedRequestBody();
        if (request.size() > 0){
            service.Suggest(Auth.getId(),1,2,searchText,9,request).enqueue(FetchFeedCallback);
        }else {
            service.Suggest(Auth.getId(),1,2,searchText,9).enqueue(FetchFeedCallback);
        }
    }

    private void fetchFeed(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds = feedResponse.getData();
        CheckNext();
        adapter = new StaffSuggestAdapter(this,feedResponse.getData(),this, this);
        ChangeLayoutManager(false);
        header.setResult(feedResponse.getTotal());
        adapter.setBig(false);
        recyclerView.setAdapter(adapter);
    }

    private void ChangeLayoutManager(boolean isBig){
        if (feedResponse.getTotal() == 0){
            RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(manager);
        }else {
            if (isBig){
                RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(manager);
            }else {
                RecyclerView.LayoutManager manager = new GridLayoutManager(this,3);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(manager);
            }
        }
    }

    private void fetchNextFeed(){
        boolean isNext = feedResponse.getCurrent_page() != feedResponse.getLast_page();
        if (isNext){
            int page = feedResponse.getCurrent_page() + 1;
            preparedRequestBody();
            if (request.size() > 0){
                service.Suggest(Auth.getId(),1,2,searchText,9,page,request).enqueue(FetchNextFeedCallback);
            }else {
                service.Suggest(Auth.getId(),1,2,searchText,9,page).enqueue(FetchNextFeedCallback);
            }
        }
    }

    private void fetchNextFeed(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds.addAll(feedResponse.getData());
        CheckNext();
        adapter.NextData(feedResponse.getData());
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    fetchNextFeed();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(RunCallback,2000);
    }

    @Override
    public void onClick(View v) {
        if (v == fab){
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onItemSelected(FilterHeader view, FilterType filterType, String selected) {

    }

    @Override
    public void onItemSelected(FilterHeader view, FilterType filterType, List<String> lists, int position) {

    }

    @Override
    public void selected(FilterHeader view, FilterType filterType, List<String> selectedLists) {
        if (filterType == FilterListener.FilterType.CITY){
            cities = selectedLists;
        }else if (filterType == FilterListener.FilterType.LEVEL){
            levels = selectedLists;
        }else if (filterType == FilterListener.FilterType.WORKTYPE){
            workTypes = selectedLists;
        }else if (filterType == FilterListener.FilterType.SALARY){
            salaries = selectedLists;
        }else if (filterType == FilterListener.FilterType.POSITION){
            categories = selectedLists;
        }
        header.setDropdown(false);
        fetchFeed();
    }

    @Override
    public void onClick(FilterCell view, FilterType filterType) {
        header.setFilterType(filterType);
        if (filterType == FilterListener.FilterType.CITY){
            header.setTags(tagsOfCity);
        }else if (filterType == FilterListener.FilterType.LEVEL){
            header.setTags(tagsOfLevels);
        }else if (filterType == FilterListener.FilterType.WORKTYPE){
            header.setTags(tagsOfWorkType);
        }else if (filterType == FilterListener.FilterType.SALARY){
            header.setTags(tagsOfSalary);
        }else if (filterType == FilterListener.FilterType.POSITION){
            header.setTags(tagsOfCategory);
        }
        header.setDropdown(true);
    }

    @Override
    public void cancel(FilterHeader view, FilterType filterType) {
        header.setDropdown(false);
    }

    @Override
    public void changeFeedSize(FilterCategoryHeader view, boolean isBig) {
        ChangeLayoutManager(isBig);
        adapter.setBig(isBig);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        searchText = s.toString();
        fetchFeed();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void showMore(StaffSuggest view) {

    }

    @Override
    public void applyJob(StaffSuggest view, Feed feed) {

    }

    @Override
    public void download(StaffSuggest view, Feed feed) {
        view.DisplayDownloadBox(feed);
    }

    @Override
    public void chat(StaffSuggest view, Feed feed) {

    }

    @Override
    public void viewDetail(StaffSuggest view, Feed feed) {
        view.DetailFeedCV(feed);
    }

    @Override
    public void onLike(FeedCV view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    private void LikeFeed(SmUser Auth, Feed feed){
        if (!feed.getFeed().isIs_like()){
            int index = feeds.indexOf(feed);
            FeedDetail temp = feed.getFeed();
            temp.setIs_like(true);
            temp.setLike_result("You liked this post.");
            temp.setCount_likes(1);
            feed.setFeed(temp);
            feeds.set(index,feed);
            adapter.notifyDataSetChanged();
            service.LikeFeed(feed.getId(),Auth.getId()).enqueue(new Callback<SmTagFeed.Like>() {
                @Override
                public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                    if (response.isSuccessful()){
                        Log.e(TAG, response.message());
                    }else {
                        try {
                            Log.e(TAG,response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onComment(FeedCV view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onDownload(FeedCV view, Feed feed) {
        view.DisplayDownloadBox(feed);
    }

    @Override
    public void viewDetail(FeedCV view, Feed feed) {
        view.DetailFeedCV(feed);
    }

    @Override
    public void viewProfile(FeedCV view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(FeedCV view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void onClickedMore(FeedCV view, Feed feed, boolean isShow) {

    }

    @Override
    public void select(LikeResultCell cell, SmUser user) {
        cell.OpenProfile(Auth,user);
    }

    @Override
    public void connect(LikeResultCell cell, SmUser user) {
        cell.DoFollow(Auth,user);
    }
}

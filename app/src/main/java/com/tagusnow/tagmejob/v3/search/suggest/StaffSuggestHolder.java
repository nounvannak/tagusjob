package com.tagusnow.tagmejob.v3.search.suggest;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;

public class StaffSuggestHolder extends RecyclerView.ViewHolder {

    private StaffSuggest staffSuggest;

    public StaffSuggestHolder(View itemView) {
        super(itemView);
        staffSuggest = (StaffSuggest)itemView;
    }

    public void BindView(Activity activity, Feed feed, SuggestionListener<StaffSuggest> suggestionListener){
        staffSuggest.setActivity(activity);
        staffSuggest.setFeed(feed);
        staffSuggest.setSuggestionListener(suggestionListener);
    }
}

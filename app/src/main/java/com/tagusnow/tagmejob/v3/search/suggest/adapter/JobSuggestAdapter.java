package com.tagusnow.tagmejob.v3.search.suggest.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.empty.EmptyHolder;
import com.tagusnow.tagmejob.v3.empty.EmptyView;
import com.tagusnow.tagmejob.v3.feed.FeedJobListener;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJob;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobHolder;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobListener;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.v3.search.suggest.JobSuggest;
import com.tagusnow.tagmejob.v3.search.suggest.JobSuggestHolder;
import java.util.List;

public class JobSuggestAdapter extends RecyclerView.Adapter {

    private static final int EMPTY = 0;
    private static final int SMALL = 1;
    private static final int BIG = 2;
    private Activity activity;
    private List<Feed> feeds;
    private SuggestionListener<JobSuggest> suggestionListener;
    private SmallFeedJobListener feedJobListener;
    private boolean isBig;

    public JobSuggestAdapter(Activity activity, List<Feed> feeds, SuggestionListener<JobSuggest> suggestionListener, SmallFeedJobListener feedJobListener) {
        this.activity = activity;
        this.feeds = feeds;
        this.suggestionListener = suggestionListener;
        this.feedJobListener = feedJobListener;
        isBig = true;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (feeds.size() == 0){
            return EMPTY;
        }else {
            if (isBig){
                return BIG;
            }else {
                return SMALL;
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == SMALL) {
            return new JobSuggestHolder(new JobSuggest(activity));
        }else if (viewType == BIG){
            return new SmallFeedJobHolder(new SmallFeedJob(activity));
        }else {
            return new EmptyHolder(new EmptyView(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == SMALL) {
            ((JobSuggestHolder) holder).BindView(activity, feeds.get(position), suggestionListener);
        }else if (getItemViewType(position) == BIG){
            ((SmallFeedJobHolder) holder).BindView(activity,feeds.get(position),feedJobListener);
        }else {
            ((EmptyHolder) holder).BindView(activity);
        }
    }

    @Override
    public int getItemCount() {
        return feeds.size() == 0 ? 1 : feeds.size();
    }

    public void setBig(boolean big) {
        isBig = big;
        notifyDataSetChanged();
    }
}

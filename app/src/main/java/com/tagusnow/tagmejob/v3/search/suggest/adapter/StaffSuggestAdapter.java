package com.tagusnow.tagmejob.v3.search.suggest.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.empty.EmptyHolder;
import com.tagusnow.tagmejob.v3.empty.EmptyView;
import com.tagusnow.tagmejob.v3.feed.FeedCV;
import com.tagusnow.tagmejob.v3.feed.FeedCVHolder;
import com.tagusnow.tagmejob.v3.feed.FeedCVListener;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.v3.search.suggest.StaffSuggest;
import com.tagusnow.tagmejob.v3.search.suggest.StaffSuggestHolder;

import java.util.List;

public class StaffSuggestAdapter extends RecyclerView.Adapter {

    private static final int EMPTY = 0;
    private static final int SMLL = 1;
    private static final int BIG = 2;
    private Activity activity;
    private List<Feed> feeds;
    private SuggestionListener<StaffSuggest> suggestionListener;
    private FeedCVListener feedCVListener;
    private boolean isBig;

    public StaffSuggestAdapter(Activity activity, List<Feed> feeds, SuggestionListener<StaffSuggest> suggestionListener, FeedCVListener feedCVListener) {
        this.activity = activity;
        this.feeds = feeds;
        this.suggestionListener = suggestionListener;
        this.feedCVListener = feedCVListener;
        isBig = true;
    }

    public void NextData(List<Feed> feeds){
        this.feeds.addAll(feeds);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (feeds.size() == 0){
            return EMPTY;
        }else {
            if (isBig){
                return BIG;
            }else {
                return SMLL;
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == SMLL) {
            return new StaffSuggestHolder(new StaffSuggest(activity));
        }else if (viewType == BIG){
            return new FeedCVHolder(new FeedCV(activity));
        }else {
            return new EmptyHolder(new EmptyView(activity));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == SMLL) {
            ((StaffSuggestHolder) holder).BindView(activity, feeds.get(position), suggestionListener);
        }else if (getItemViewType(position) == BIG){
            ((FeedCVHolder) holder).BindView(activity,feeds.get(position),feedCVListener);
        }else {
            ((EmptyHolder) holder).BindView(activity);
        }
    }

    @Override
    public int getItemCount() {
        return feeds.size() == 0 ? 1 : feeds.size();
    }

    public void setBig(boolean big) {
        isBig = big;
        notifyDataSetChanged();
    }
}

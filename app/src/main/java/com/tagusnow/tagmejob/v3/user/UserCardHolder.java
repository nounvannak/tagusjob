package com.tagusnow.tagmejob.v3.user;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class UserCardHolder extends RecyclerView.ViewHolder {

    private UserCard userCard;

    public UserCardHolder(View itemView) {
        super(itemView);
        userCard = (UserCard)itemView;
    }

    public void BindView(Activity activity, SmUser user, UserCardListener userCardListener){
        userCard.setActivity(activity);
        userCard.setUser(user);
        userCard.setUserCardListener(userCardListener);
    }
}

package com.tagusnow.tagmejob.v3.user;

import com.tagusnow.tagmejob.auth.SmUser;

public interface UserCardListener {
    void connect(UserCard view, SmUser user);
    void viewProfile(UserCard view,SmUser user);
    void remove(UserCard view,SmUser user);
}

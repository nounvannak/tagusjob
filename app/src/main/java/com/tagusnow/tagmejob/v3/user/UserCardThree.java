package com.tagusnow.tagmejob.v3.user;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserCardThree extends TagUsJobRelativeLayout implements View.OnClickListener {
    private Context context;
    private SmUser user;
    private CircleImageView profile_picture;
    private TextView profile_name,follower_counter;
    private ImageButton btnClose;
    private Button btnFollow;

    public void setUser(SmUser user) {
        this.user = user;

        setUserProfile(profile_picture,user);
        setName(profile_name,user);
        follower_counter.setText(context.getString(R.string.following_,user.getFollowing_counter()));
        if (user.isIs_follow()){
            btnFollow.setText(context.getString(R.string.following));
        }else {
            btnFollow.setText(context.getString(R.string.follow));
        }
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }



    private UserCardThreeListener userCardThreeListener;

    private void InitUI(Context context){
        inflate(context, R.layout.user_card_three,this);
        this.context = context;
        profile_picture = (CircleImageView) findViewById(R.id.profile_picture);
        profile_name = (TextView)findViewById(R.id.profile_name);
        follower_counter = (TextView)findViewById(R.id.follower_counter);
        btnClose = (ImageButton)findViewById(R.id.btnClose);
        btnFollow = (Button)findViewById(R.id.btnFollow);
        btnClose.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
        profile_picture.setOnClickListener(this);
    }

    public UserCardThree(Context context) {
        super(context);
        InitUI(context);
    }

    public UserCardThree(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserCardThree(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserCardThree(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnClose){
            userCardThreeListener.remove(this,user);
        }else if (view == btnFollow){
            userCardThreeListener.connect(this,user);
        }else if (view == profile_picture){
            userCardThreeListener.viewProfile(this,user);
        }
    }

    public void setUserCardThreeListener(UserCardThreeListener userCardThreeListener) {
        this.userCardThreeListener = userCardThreeListener;
    }
}

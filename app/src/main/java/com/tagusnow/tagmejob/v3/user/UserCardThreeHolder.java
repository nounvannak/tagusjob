package com.tagusnow.tagmejob.v3.user;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class UserCardThreeHolder extends RecyclerView.ViewHolder {

    private UserCardThree userCardThree;

    public UserCardThreeHolder(View itemView) {
        super(itemView);
        userCardThree = (UserCardThree)itemView;
    }

    public void BindView(Activity activity, SmUser user, UserCardThreeListener userCardThreeListener){
        userCardThree.setActivity(activity);
        userCardThree.setUser(user);
        userCardThree.setUserCardThreeListener(userCardThreeListener);
    }
}

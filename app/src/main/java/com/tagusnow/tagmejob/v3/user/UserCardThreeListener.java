package com.tagusnow.tagmejob.v3.user;

import com.tagusnow.tagmejob.auth.SmUser;

public interface UserCardThreeListener {
    void connect(UserCardThree view, SmUser user);
    void viewProfile(UserCardThree view,SmUser user);
    void remove(UserCardThree view,SmUser user);
}

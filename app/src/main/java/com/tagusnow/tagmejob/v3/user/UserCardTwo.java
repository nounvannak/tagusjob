package com.tagusnow.tagmejob.v3.user;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserCardTwo extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ImageView cover,btnRemove;
    private CircleImageView profile;
    private TextView name,address;
    private Button btnConnect;
    private SmUser user;

    public void setUser(SmUser user) {
        this.user = user;

        setName(name,user);
        setUserProfile(profile,user);
        setUserCover(cover,user);

        if (user.isIs_follow()){
            btnConnect.setText("Connected");
        }else {
            btnConnect.setText("Connect");
        }

    }

    public void setUserCardTwoListener(UserCardTwoListener userCardTwoListener) {
        this.userCardTwoListener = userCardTwoListener;
    }

    private UserCardTwoListener userCardTwoListener;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.user_card_two,this);
        this.context = context;

        cover = (ImageView)findViewById(R.id.cover);
        profile = (CircleImageView)findViewById(R.id.profile);
        name = (TextView)findViewById(R.id.name);
        address = (TextView)findViewById(R.id.address);
        btnRemove = (ImageView)findViewById(R.id.btnRemove);
        btnConnect = (Button)findViewById(R.id.btnConnect);

        profile.setOnClickListener(this);
        btnConnect.setOnClickListener(this);
        btnRemove.setOnClickListener(this);
    }

    public UserCardTwo(Context context) {
        super(context);
        InitUI(context);
    }

    public UserCardTwo(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public UserCardTwo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public UserCardTwo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view == btnRemove){
            userCardTwoListener.remove(this,user);
        }else if (view == btnConnect){
            userCardTwoListener.connect(this,user);
        }else if (view == profile){
            userCardTwoListener.profile(this,user);
        }
    }
}

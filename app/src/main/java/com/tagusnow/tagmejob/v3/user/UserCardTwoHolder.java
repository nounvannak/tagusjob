package com.tagusnow.tagmejob.v3.user;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class UserCardTwoHolder extends RecyclerView.ViewHolder {

    private UserCardTwo userCardTwo;

    public UserCardTwoHolder(View itemView) {
        super(itemView);
        userCardTwo = (UserCardTwo)itemView;
    }

    public void BindView(Activity activity, SmUser user,UserCardTwoListener userCardTwoListener){
        userCardTwo.setActivity(activity);
        userCardTwo.setUser(user);
        userCardTwo.setUserCardTwoListener(userCardTwoListener);
    }
}

package com.tagusnow.tagmejob.v3.user;

import com.tagusnow.tagmejob.auth.SmUser;

public interface UserCardTwoListener {
    void connect(UserCardTwo view, SmUser user);
    void profile(UserCardTwo view, SmUser user);
    void remove(UserCardTwo view, SmUser user);
}

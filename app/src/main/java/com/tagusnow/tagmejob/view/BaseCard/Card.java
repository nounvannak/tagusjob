package com.tagusnow.tagmejob.view.BaseCard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.JobDetail;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.PostJobActivity;
import com.tagusnow.tagmejob.PrivacyActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewDetailPostActivity;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourFour;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourOne;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourThree;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourTwo;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeFour;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeOne;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeThree;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeTwo;
import com.tagusnow.tagmejob.view.Grid.Two.GridTwoOne;
import com.tagusnow.tagmejob.view.Grid.Two.GridTwoTwo;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.whalemare.sheetmenu.SheetMenu;

public class Card extends TagUsJobRelativeLayout implements View.OnClickListener{

    private static final String TAG = Card.class.getSimpleName();
    private static final int REQ_PRIVACY = 903;
    CircleImageView mProfilePicture;
    TextView mFeedStoryTitle,mFeedStoryTime,like_result,comment_result,like_text;
    ImageView like_icon,icon_saved;
    LinearLayout feed_text_box,layout_job_description,like_button,comment_button,share_button;
    RelativeLayout profileBar,like_and_comment_information;
    FrameLayout image_frame;
    Button mButtonMoreFeed;
    TextView mFeedTitle;
    private TextView job_description;
    private Context context;
    private Feed feed;
    private int position;
    private SmUser authUser;
    private RecyclerView.Adapter adapter;
    private GridTwoOne gridTwoOne;
    private GridTwoTwo gridTwoTwo;
    private GridThreeOne gridThreeOne;
    private GridThreeTwo gridThreeTwo;
    private GridThreeThree gridThreeThree;
    private GridThreeFour gridThreeFour;
    private GridFourTwo gridFourTwo;
    private GridFourThree gridFourThree;
    private GridFourFour gridFourFour;
    private GridFourOne gridFourOne;


    /*Service*/
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private JobsService jobsService = ServiceGenerator.createService(JobsService.class);

    /*Socket.IO*/
    Socket socket;
    private MenuItem.OnMenuItemClickListener mClick = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.optionSavePost :
                    /*code*/
                    savePost();
                    break;
                case R.id.optionEditPost :
                    editPost();
                    break;
                case R.id.optionHide :
                    hidePost();
                    break;
                case R.id.optionDelete :
                    removePost();
                    break;
            }
            return false;
        }
    };
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (!activity.isFinishing()){
                int menu = 0;
                if (authUser!=null && authUser.getId()==feed.getUser_post().getId()){
                    menu = R.menu.menu_post_option_auth;
                }else{
                    menu = R.menu.menu_post_option_not_auth;
                }
                SheetMenu.with(activity)
                        .setAutoCancel(true)
                        .setMenu(menu)
                        .setClick(mClick).showIcons(true).show();
            }
        }
    };
    private Callback<SmTagFeed.Like> mLikeCallback = new Callback<SmTagFeed.Like>() {
        @Override
        public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
            if (response.isSuccessful()){
                feed.getFeed().setIs_like(response.body().isIs_like());
                feed.getFeed().setCount_likes(response.body().getCount_likes());
                adapter.notifyItemChanged(position);
                adapter.notifyDataSetChanged();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    like_text.setTextColor(context.getColor(R.color.colorIconSelect));
                }
                like_icon.setImageResource(R.drawable.ic_action_like_blue);
                like_result.setText(feed.getFeed().getLike_result());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Count Like "+feed.getId(),mCountLike);
        socket.on("Count Comment "+feed.getId(),mCountComment);
        socket.connect();
    }

    private Emitter.Listener mCountLike = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setIs_like(obj.getInt("is_like") == 1);
                                feed.getFeed().setCount_likes(obj.getInt("count_likes"));
                                adapter.notifyDataSetChanged();
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountLike");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setCount_comments(obj.getInt("count_comments"));
                                adapter.notifyDataSetChanged();
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountComment");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Callback<Feed> mJobCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                feed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void callJob(){
        int user = this.authUser!=null ? this.authUser.getId() : 0;
        int id = this.feed!=null ? this.feed.getId() : 0;
        jobsService.callJob(user,id).enqueue(mJobCallback);
    }

    public Card Auth(SmUser user){
        this.authUser = user;
        return this;
    }

    public Card Position(int position){
        this.position = position;
        return this;
    }

    public Card Adapter(RecyclerView.Adapter adapter){
        this.adapter = adapter;
        return this;
    }

    public Card Feed(int id){
        if (this.feed!=null){
            this.feed.setId(id);
        }else {
            this.feed = new Feed();
            this.feed.setId(id);
        }
        this.callJob();
        return this;
    }

    public Card Activity(Activity activity){
        this.setActivity(activity);
        return this;
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void initUI(Context context){
        try {
            this.context = context;
            inflate(context, R.layout.card,this);
            mProfilePicture = (CircleImageView) findViewById(R.id.mProfilePicture);
            mFeedStoryTitle = (TextView) findViewById(R.id.mFeedStoryTitle);
            mFeedStoryTime = (TextView) findViewById(R.id.mFeedStoryTime);
            mButtonMoreFeed = (Button) findViewById(R.id.mButtonMoreFeed);
            mFeedTitle = (TextView) findViewById(R.id.mFeedTitle);
            job_description = (TextView) findViewById(R.id.job_description);
            feed_text_box = (LinearLayout) findViewById(R.id.feed_text_box);
            layout_job_description = (LinearLayout) findViewById(R.id.layout_job_description);
            profileBar = (RelativeLayout) findViewById(R.id.profileBar);
            icon_saved = (ImageView) findViewById(R.id.icon_saved);

            image_frame = (FrameLayout)findViewById(R.id.image_frame);
            like_and_comment_information = (RelativeLayout)findViewById(R.id.like_and_comment_information);
            like_result = (TextView) findViewById(R.id.like_result);
            like_text = (TextView)findViewById(R.id.like_text);
            like_icon = (ImageView)findViewById(R.id.like_icon);
            like_button = (LinearLayout)findViewById(R.id.like_button);
            comment_result = (TextView)findViewById(R.id.comment_result);
            comment_button = (LinearLayout) findViewById(R.id.comment_button);
            share_button = (LinearLayout)findViewById(R.id.share_button);

            like_button.setOnClickListener(this);
            comment_button.setOnClickListener(this);
            share_button.setOnClickListener(this);
            image_frame.setOnClickListener(this);

            mButtonMoreFeed.setOnClickListener(this);
            profileBar.setOnClickListener(this);
            job_description.setOnClickListener(this);
            mFeedStoryTitle.setOnClickListener(this);
            mProfilePicture.setOnClickListener(this);
            mFeedTitle.setOnClickListener(this);

            gridTwoOne = new GridTwoOne(this.context);
            gridTwoTwo = new GridTwoTwo(this.context);
            gridThreeOne = new GridThreeOne(this.context);
            gridThreeTwo = new GridThreeTwo(this.context);
            gridThreeThree = new GridThreeThree(this.context);
            gridThreeFour = new GridThreeFour(this.context);
            gridFourTwo = new GridFourTwo(this.context);
            gridFourThree = new GridFourThree(this.context);
            gridFourFour = new GridFourFour(this.context);
            gridFourOne = new GridFourOne(this.context);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Card(Context context) {
        super(context);
        this.initUI(context);
    }

    public Card(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public Card(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public Card(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    private void likePost(){
        if (authUser==null){
            Intent loginIntent = new Intent(context, LoginActivity.class);
            context.startActivity(loginIntent);
        }
        service.LikeFeed(feed.getId(),authUser.getId()).enqueue(mLikeCallback);
    }

    private void viewComment(){
        if (this.activity!=null){
            this.activity.startActivity(CommentActivity.createIntent(this.activity)
                    .putExtra(CommentActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("count_likes",feed.getFeed().getCount_likes())
                    .putExtra("is_like",feed.getFeed().isIs_like())
                    .putExtra("count_comments",feed.getFeed().getCount_comments())
                    .putExtra("feed_id",feed.getId())
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void shareContent(){
        originalShare();
    }

    private void originalShare(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,feed.getDetail());
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    private void viewPost(){
        String searchTitle = "";
        if (feed.getFeed().getOriginalHastTagText()==null || feed.getFeed().getOriginalHastTagText().equals("")){
            searchTitle = "";
        }else{
            searchTitle = mFeedTitle.getText().toString();
        }
        if (this.activity!=null){
            this.activity.startActivity(ViewPostActivity.createIntent(this.activity)
                    .putExtra(ViewPostActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("searchTitle",searchTitle)
                    .putExtra("position",this.position)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewPostDetail(){
        if (this.activity!=null){
            this.activity.startActivity(ViewDetailPostActivity.createIntent(this.activity)
                    .putExtra(ViewDetailPostActivity.FEED,new Gson().toJson(this.feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void showMore(){
        if(this.activity!=null){
            this.activity.runOnUiThread(mRunnable);
        }
    }

    private void removePost(){
        service.RemoveFeed(feed.getId(),authUser.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setDelete(1);
                    adapter.notifyDataSetChanged();
                }else {
                    ((TagUsJobActivity)activity).ShowAlert("Remove Feed",response.message());
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void hidePost(){
        service.HideFeed(feed.getId(),feed.getUser_id(),authUser.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setIs_hide(1);
                }else {
                    ((TagUsJobActivity)activity).ShowAlert("Remove Feed",response.message());
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void editPrivacy(){
        Intent intent = new Intent(this.activity, PrivacyActivity.class);
        intent.putExtra(SuperConstance.PRIVACY,feed.getPrivacy());
        intent.putExtra("position",position);
        this.activity.startActivityForResult(intent,REQ_PRIVACY);
    }

    private void editPost(){
        if (this.feed!=null){
            if (this.feed.getFeed().getType().equals(SuperConstance.POST_JOB)){
                if (this.activity!=null){
                    this.activity.startActivity(new PostJobActivity()
                            .isNew(false)
                            .setTitle("Edit post job")
                            .createIntent(this.activity)
                            .putExtra(PostJobActivity.FEED,new Gson().toJson(this.feed))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Acvitity",Toast.LENGTH_SHORT).show();
                }
            }else {
                if (this.activity!=null){
                    this.activity.startActivity(CreatePostActivity.createIntent(this.activity)
                            .putExtra(CreatePostActivity.FEED,new Gson().toJson(this.feed))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Acvitity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    private void savePost(){
        service.SaveFeed(feed.getId(),feed.getUser_id(),authUser.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setIs_save(1);
                    adapter.notifyDataSetChanged();
                }else {
                    ((TagUsJobActivity)activity).ShowAlert("Remove Feed",response.message());
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void showProfile(){
        if (this.authUser!=null){
            if (authUser.getId()==feed.getUser_post().getId()){
                if (this.activity!=null){
                    this.activity.startActivity(ProfileActivity
                            .createIntent(activity)
                            .putExtra(ProfileActivity.PROFILEA_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }else{
                if (this.activity!=null){
                    this.activity.startActivity(ViewProfileActivity
                            .createIntent(activity)
                            .putExtra(ViewProfileActivity.PROFILE_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==mProfilePicture){
            showProfile();
        }else if (view==mFeedStoryTitle){
            showProfile();
        }else if (view==share_button){
            shareContent();
        }else if (view==job_description){
            viewPostDetail();
        }else if (view==profileBar){
            viewPostDetail();
        }else if (view==image_frame){
            viewPostDetail();
        }else if (view==mButtonMoreFeed){
            showMore();
        }else if (view==like_button){
            likePost();
        }else if (view==comment_button){
            viewComment();
        }else if (view==mFeedTitle){
            viewPostDetail();
        }
    }

    private void NEW_SHOW_MORE(){
        if (this.activity!=null){
            this.activity.startActivity(JobDetail.createIntent(this.activity).putExtra(JobDetail.FEED,this.feed.getId()).putExtra(JobDetail.USER,this.authUser.getId()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    public TextView getmFeedTitle() {
        return mFeedTitle;
    }

    public ImageView getIcon_saved() {
        return icon_saved;
    }

    public Button getmButtonMoreFeed() {
        return mButtonMoreFeed;
    }

    public RelativeLayout getProfileBar() {
        return profileBar;
    }

    public TextView getJob_description() {
        return job_description;
    }

    public TextView getmFeedStoryTitle() {
        return mFeedStoryTitle;
    }

    public CircleImageView getmProfilePicture() {
        return mProfilePicture;
    }

    public Card feed(Feed feed){
        this.feed = feed;
        initSocket();
        if (feed!=null){

            layout_job_description.setVisibility(View.GONE);

            mFeedStoryTitle.setText(feed.getUser_post().getUser_name());

            if (feed.getUser_post().getImage()!=null){
                if (feed.getUser_post().getImage().contains("http")) {
                    Glide.with(context)
                            .load(feed.getUser_post().getImage())
                            .error(R.mipmap.ic_launcher_circle)
                            .centerCrop()
                            .override(100, 100)
                            .into(mProfilePicture);
                } else {
                    Glide.with(context)
                            .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage(), "200"))
                            .error(R.mipmap.ic_launcher_circle)
                            .centerCrop()
                            .override(100, 100)
                            .into(mProfilePicture);
                }
            }else {
                Glide.with(context)
                        .load(R.mipmap.ic_launcher_circle)
                        .centerCrop()
                        .into(mProfilePicture);
            }

            if (feed.getIs_save() == 0) {
                icon_saved.setVisibility(View.GONE);
            } else {
                icon_saved.setVisibility(View.VISIBLE);
            }

            if (feed.getFeed().getType().equals(FeedDetail.NORMAL_POST)) {
                if (feed.getFeed()!=null && feed.getFeed().getType().equals(FeedDetail.NORMAL_POST)) {
                    layout_job_description.setVisibility(View.GONE);
                    mFeedStoryTime.setText(feed.getFeed().getTimeline());
                    if (feed.getFeed().getOriginalHastTagText() == null || feed.getFeed().getOriginalHastTagText().equals("")) {
                        feed_text_box.setVisibility(View.GONE);
                    } else {
                        feed_text_box.setVisibility(View.VISIBLE);
                        mFeedTitle.setText(feed.getFeed().getOriginalHastTagText());
                    }

                    if (feed.getFeed().isIs_like()) {
                        like_icon.setImageResource(R.drawable.ic_action_like_blue);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            like_text.setTextColor(context.getColor(R.color.colorAccent));
                        }

                    } else {
                        like_icon.setImageResource(R.drawable.ic_action_like_black);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            like_text.setTextColor(context.getColor(R.color.colorIcon));
                        }
                    }

                    if (feed.getFeed().getCount_likes() > 0) {
                        like_result.setVisibility(View.VISIBLE);
                        like_result.setText(feed.getFeed().getLike_result());
                    } else {
                        like_result.setVisibility(View.GONE);
                    }

                    if (feed.getFeed().getCount_comments() > 0) {
                        comment_result.setVisibility(View.VISIBLE);
                        comment_result.setText(context.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                    } else {
                        comment_result.setVisibility(View.GONE);
                    }

                    if (feed.getFeed().getCount_likes()==0 && feed.getFeed().getCount_comments()==0){
                        like_and_comment_information.setVisibility(GONE);
                    }else {
                        like_and_comment_information.setVisibility(VISIBLE);
                    }

                    if (feed.getFeed().getAlbum()!=null){

                        gridTwoOne.setActivity(this.activity);
                        gridTwoTwo.setActivity(this.activity);
                        gridThreeOne.setActivity(this.activity);
                        gridThreeTwo.setActivity(this.activity);
                        gridThreeThree.setActivity(this.activity);
                        gridThreeFour.setActivity(this.activity);
                        gridFourTwo.setActivity(this.activity);
                        gridFourThree.setActivity(this.activity);
                        gridFourFour.setActivity(this.activity);
                        gridFourOne.setActivity(this.activity);

                        if (feed.getFeed().getAlbum() == null || feed.getFeed().getAlbum().size() == 1 && (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc() == null || feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().equals(""))) {
                            image_frame.setVisibility(GONE);
                        } else {
                            image_frame.setVisibility(VISIBLE);
                            String Grid[][] = {{}, {},
                                    {GridTwoOne.class.getSimpleName(), GridTwoTwo.class.getSimpleName()},
                                    {GridThreeOne.class.getSimpleName(), GridThreeTwo.class.getSimpleName(), GridThreeThree.class.getSimpleName(), GridThreeFour.class.getSimpleName()},
                                    {GridFourOne.class.getSimpleName(), GridFourTwo.class.getSimpleName(), GridThreeThree.class.getSimpleName(), GridFourFour.class.getSimpleName()}};
                            if (feed.getFeed().getAlbum().size() >= 4) {
                                int size = new Random().nextInt(4);
                                image_frame.removeAllViews();
                                if (Grid[4][size].equals(GridFourOne.class.getSimpleName())) {
                                    image_frame.addView(gridFourOne.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                } else if (Grid[4][size].equals(GridFourTwo.class.getSimpleName())) {
                                    image_frame.addView(gridFourTwo.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                } else if (Grid[4][size].equals(GridFourThree.class.getSimpleName())) {
                                    image_frame.addView(gridFourThree.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                } else {
                                    if (feed.getFeed().getAlbum().size() >= 5) {
                                        image_frame.addView(gridFourFour.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                    } else {
                                        image_frame.addView(gridFourThree.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                    }
                                }
                            } else {
                                switch (feed.getFeed().getAlbum().size()) {
                                    case 1:
                                        image_frame.removeAllViews();
                                        ImageView imageView = new ImageView(this.context);
                                        Glide.with(this.context)
                                                .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                                .fitCenter()
                                                .error(R.drawable.ic_error)
                                                .into(imageView);
                                        image_frame.removeAllViews();
                                        image_frame.addView(imageView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                                        break;
                                    case 2:
                                        int size = new Random().nextInt(2);
                                        image_frame.removeAllViews();
                                        if (Grid[feed.getFeed().getAlbum().size()][size].equals(GridTwoOne.class.getSimpleName()))
                                            image_frame.addView(gridTwoOne.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                        else
                                            image_frame.addView(gridTwoTwo.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                        break;
                                    case 3:
                                        int sizes = new Random().nextInt(4);
                                        image_frame.removeAllViews();
                                        if (Grid[feed.getFeed().getAlbum().size()][sizes].equals(GridThreeOne.class.getSimpleName())) {
                                            image_frame.addView(gridThreeOne.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                        } else if (Grid[feed.getFeed().getAlbum().size()][sizes].equals(GridThreeTwo.class.getSimpleName())) {
                                            image_frame.addView(gridThreeTwo.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                        } else if (Grid[feed.getFeed().getAlbum().size()][sizes].equals(GridThreeThree.class.getSimpleName())) {
                                            image_frame.addView(gridThreeThree.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                        } else {
                                            image_frame.addView(gridThreeFour.setAlbums(feed.getFeed().getAlbum()), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                        }
                                        break;
                                }
                            }
                        }
                    }else{
                        image_frame.setVisibility(View.GONE);
                    }

                }
            }
        }
        return this;
    }
}

package com.tagusnow.tagmejob.view.BaseCard;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.PostJobActivity;
import com.tagusnow.tagmejob.PrivacyActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewCardCVActivity;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.adapter.FeedAdapter;
import com.tagusnow.tagmejob.adapter.ProfileAdapter;
import com.tagusnow.tagmejob.adapter.ProfileHomeAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.view.CVAndResumeLayout;
import org.json.JSONObject;
import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CardCV extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = CardCV.class.getSimpleName();
    private static final int REQ_PRIVACY = 903;
    private Activity activity;
    private Context context;
    /*View*/
    private ImageView user_image;
    private RelativeLayout body,body_content,show_more,like_and_comment_information,footer,description;
    private TextView name,title,tel,email,location,description_text,text_show_more,like_result,comment_result,like_text;
    private ImageView icon_show_more,like_icon;
    private LinearLayout like_button,comment_button,download_button;
    /*Object*/
    private SmUser user;
    private Feed feed;
    private int position;
    private RecyclerView.Adapter adapter;
    private String adapterTag;
    private FeedAdapter feedAdapter;
    private ProfileAdapter profileAdapter;
    private ProfileHomeAdapter profileHomeAdapter;
    private boolean isShowHeader,isShowIconSave,isShowLikeAndCommentInfo,isShowComment,isShowLike,isReponse,isShowMore = true;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FeedService service = retrofit.create(FeedService.class);
    private JobsService jobsService = retrofit.create(JobsService.class);
    private DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialogInterface) {
            dialogInterface.dismiss();
        }
    };

    private void initUI(Context context){
        inflate(context, R.layout.card_cv_layout,this);
        this.context = context;
        user_image = (ImageView)findViewById(R.id.user_image);
        body = (RelativeLayout)findViewById(R.id.body);
        body_content = (RelativeLayout)findViewById(R.id.body_content);
        show_more = (RelativeLayout)findViewById(R.id.show_more);
        like_and_comment_information = (RelativeLayout)findViewById(R.id.like_and_comment_information);
        footer = (RelativeLayout)findViewById(R.id.footer);
        description = (RelativeLayout)findViewById(R.id.description);
        icon_show_more = (ImageView)findViewById(R.id.icon_show_more);
        like_icon = (ImageView)findViewById(R.id.like_icon);
        like_button = (LinearLayout)findViewById(R.id.like_button);
        comment_button = (LinearLayout)findViewById(R.id.comment_button);
        download_button = (LinearLayout)findViewById(R.id.download_button);

        name = (TextView)findViewById(R.id.name);
        title = (TextView)findViewById(R.id.title);
        tel = (TextView)findViewById(R.id.tel);
        email = (TextView)findViewById(R.id.email);
        location = (TextView)findViewById(R.id.location);
        description_text = (TextView)findViewById(R.id.description_text);
        text_show_more = (TextView)findViewById(R.id.text_show_more);
        like_result = (TextView)findViewById(R.id.like_result);
        comment_result = (TextView)findViewById(R.id.comment_result);
        like_text = (TextView)findViewById(R.id.like_text);

        description.setVisibility(GONE);

        user_image.setOnClickListener(this);
        show_more.setOnClickListener(this);
        body_content.setOnClickListener(this);
        like_button.setOnClickListener(this);
        comment_button.setOnClickListener(this);
        download_button.setOnClickListener(this);
    }


    public CardCV(Context context) {
        super(context);
        this.initUI(context);
    }

    public CardCV(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public CardCV(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public CardCV(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view==user_image){
            this.showProfile();
        }else if (view==show_more){
            if (this.isShowMore){
                this.icon_show_more.setBackgroundResource(R.drawable.ic_arrow_drop_up_black_24dp);
                this.text_show_more.setText("Show Less");
                this.description.setVisibility(VISIBLE);
                this.isShowMore = false;
            }else {
                this.icon_show_more.setBackgroundResource(R.drawable.ic_arrow_drop_down_black_24dp);
                this.text_show_more.setText("Show More");
                this.description.setVisibility(GONE);
                this.isShowMore = true;
            }
        }else if (view==body_content){
            this.viewPostDetail();
        }else if (view==like_button){
            this.likePost();
        }else if (view==comment_button){
            this.viewComment();
        }else if (view==download_button){
            this.PopupDownload();
        }
    }


    private void PopupDownload(){
        if (this.activity!=null){
            CVAndResumeLayout layout = new CVAndResumeLayout(this.context);
            layout.setActivity(this.activity);
            layout.setFeed(this.feed);
            new AlertDialog.Builder(this.context).setView(layout).setOnDismissListener(onDismissListener).create().show();
        }
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public SmUser getUser() {
        return user;
    }

    public void setUser(SmUser user) {
        this.user = user;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initFeed(feed);
    }

    public void setFeed(int id) {
        if (this.feed!=null){
            this.feed.setId(id);
        }else {
            this.feed = new Feed();
            this.feed.setId(id);
        }
        this.callJob();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    public String getAdapterTag() {
        return adapterTag;
    }

    public void setAdapterTag(String adapterTag) {
        this.adapterTag = adapterTag;
    }

    public FeedAdapter getFeedAdapter() {
        return feedAdapter;
    }

    public void setFeedAdapter(FeedAdapter feedAdapter) {
        this.feedAdapter = feedAdapter;
    }

    private Callback<SmTagFeed.Like> mLikeCallback = new Callback<SmTagFeed.Like>() {
        @Override
        public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
            if (response.isSuccessful()){
                feed.getFeed().setIs_like(response.body().isIs_like());
                feed.getFeed().setCount_likes(response.body().getCount_likes());
                adapter.notifyItemChanged(position);
                adapter.notifyDataSetChanged();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    like_text.setTextColor(context.getColor(R.color.colorIconSelect));
                }
                like_result.setText(feed.getFeed().getLike_result());
                like_icon.setImageResource(R.drawable.ic_action_like_blue);
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<Feed> mJobCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                setFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void callJob(){
        int user = this.user!=null ? this.user.getId() : 0;
        int id = this.feed!=null ? this.feed.getId() : 0;
        jobsService.callJob(user,id).enqueue(mJobCallback);
    }

    private void likePost(){
        if (this.user==null){
            Intent loginIntent = new Intent(context, LoginActivity.class);
            context.startActivity(loginIntent);
        }
        service.LikeFeed(feed.getId(),this.user.getId()).enqueue(mLikeCallback);
    }

    private void viewComment(){
        if (this.activity!=null){
            this.activity.startActivity(CommentActivity.createIntent(this.activity)
                    .putExtra(CommentActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("count_likes",feed.getFeed().getCount_likes())
                    .putExtra("is_like",feed.getFeed().isIs_like())
                    .putExtra("count_comments",feed.getFeed().getCount_comments())
                    .putExtra("feed_id",feed.getId())
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewPost(){
        String searchTitle = "";
        if (feed.getFeed().getOriginalHastTagText()==null || feed.getFeed().getOriginalHastTagText().equals("")){
            searchTitle = "";
        }

        if (this.activity!=null){
            this.activity.startActivity(ViewPostActivity.createIntent(this.activity)
                    .putExtra(ViewPostActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("searchTitle",searchTitle)
                    .putExtra("position",this.position)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewPostDetail(){
        if (this.activity!=null){
            this.activity.startActivity(ViewCardCVActivity.createIntent(this.activity)
                    .putExtra(ViewCardCVActivity.FEED, (Serializable) this.feed));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void removePost(){
        service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setDelete(1);
                    adapter.notifyDataSetChanged();
                }else {
                    try {
                        call.execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void hidePost(){
        service.HideFeed(feed.getId(),feed.getUser_id(),user.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setIs_hide(1);
                    adapter.notifyDataSetChanged();
                }else {
                    try {
                        call.execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void editPrivacy(){
        Intent intent = new Intent(this.activity, PrivacyActivity.class);
        intent.putExtra(SuperConstance.PRIVACY,feed.getPrivacy());
        intent.putExtra("position",position);
        this.activity.startActivityForResult(intent,REQ_PRIVACY);
    }

    private void editPost(){
        if (this.feed!=null){
            if (this.feed.getFeed().getType().equals(SuperConstance.POST_JOB)){
                if (this.activity!=null){
                    this.activity.startActivity(new PostJobActivity()
                            .isNew(false)
                            .setTitle("Edit post job")
                            .createIntent(this.activity)
                            .putExtra(PostJobActivity.FEED,new Gson().toJson(this.feed))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Acvitity",Toast.LENGTH_SHORT).show();
                }
            }else {
                if (this.activity!=null){
                    this.activity.startActivity(CreatePostActivity.createIntent(this.activity)
                            .putExtra(CreatePostActivity.FEED,new Gson().toJson(this.feed))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Acvitity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    private void showProfile(){
        if (this.user!=null){
            if (user.getId()==feed.getUser_post().getId()){
                if (this.activity!=null){
                    this.activity.startActivity(ProfileActivity
                            .createIntent(activity)
                            .putExtra(ProfileActivity.PROFILEA_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }else{
                if (this.activity!=null){
                    this.activity.startActivity(ViewProfileActivity
                            .createIntent(activity)
                            .putExtra(ViewProfileActivity.PROFILE_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
        }
    }

    /*Socket.IO*/
    Socket socket;
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Count Like "+feed.getId(),mCountLike);
        socket.on("Count Comment "+feed.getId(),mCountComment);
        socket.connect();
    }

    private Emitter.Listener mCountLike = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setIs_like(obj.getInt("is_like") == 1);
                                feed.getFeed().setCount_likes(obj.getInt("count_likes"));
                                isShowLike = true;
                                like_result.setVisibility(VISIBLE);
                                like_result.setText(feed.getFeed().getLike_result());
                                adapter.notifyDataSetChanged();
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountLike");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setCount_comments(obj.getInt("count_comments"));
                                isShowComment = true;
                                comment_result.setVisibility(VISIBLE);
                                comment_result.setText(activity.getString(R.string._d_comments,obj.getInt("count_comments")));
                                adapter.notifyDataSetChanged();
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountComment");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private void initFeed(Feed feed){
        this.initSocket();
        if (feed!=null && feed.getDelete()==0 && feed.getIs_hide()==0){
            /*Profile Information*/
            if (feed.getUser_post()!=null){
                if (feed.getUser_post().getImage()!=null){
                    if (feed.getUser_post().getImage().contains("http")){
                        Glide.with(this.context)
                                .load(feed.getUser_post().getImage())
                                .fitCenter()
                                .error(R.drawable.ic_user_male_icon)
                                .into(user_image);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage()))
                                .fitCenter()
                                .error(R.drawable.ic_user_male_icon)
                                .into(user_image);
                    }
                }else {
                    Glide.with(this.context).load(R.drawable.ic_user_male_icon).into(user_image);
                }

            }else {
                Toast.makeText(this.context,"No profile data",Toast.LENGTH_SHORT).show();
            }

            /*Body*/
            name.setText(feed.getUser_post().getUser_name());
            title.setText(feed.getFeed().getTitle());
            tel.setText(feed.getFeed().getPhone());
            email.setText(feed.getFeed().getEmail());
            location.setText(feed.getFeed().getCity_name());

            if (feed.getFeed().getOriginalHastTagText()!=null && !feed.getFeed().getOriginalHastTagText().equals("")){
                show_more.setVisibility(VISIBLE);
                description_text.setText(feed.getFeed().getOriginalHastTagText());
            }else {
                show_more.setVisibility(GONE);
            }

            if (feed.getFeed().getCount_comments()==0 && feed.getFeed().getCount_likes()==0){
                isShowLikeAndCommentInfo = false;
            }else {
                isShowLikeAndCommentInfo = true;
            }

            if (isShowLikeAndCommentInfo){
                like_and_comment_information.setVisibility(VISIBLE);

                if (feed.getFeed().getCount_likes()==0){
                    isShowLike = false;
                }else {
                    isShowLike = true;
                }

                if (isShowLike){
                    like_result.setVisibility(VISIBLE);
                    like_result.setText(feed.getFeed().getLike_result());
                }else {
                    like_result.setVisibility(GONE);

                }

                if (feed.getFeed().isIs_like()){
                    like_icon.setImageResource(R.drawable.ic_action_like_blue);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        like_text.setTextColor(this.context.getColor(R.color.colorPrimary));
                    }
                }else {
                    like_icon.setImageResource(R.drawable.ic_action_like_black);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        like_text.setTextColor(this.context.getColor(R.color.colorIcon));
                    }
                }

                if (feed.getFeed().getCount_comments()==0){
                    isShowComment = false;
                }else {
                    isShowComment = true;
                }

                if (isShowComment){
                    comment_result.setVisibility(VISIBLE);
                    comment_result.setText(this.activity.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                }else {
                    comment_result.setVisibility(GONE);
                }

            }else {
                like_and_comment_information.setVisibility(GONE);
            }

        }
    }

    public boolean isShowHeader() {
        return isShowHeader;
    }

    public void setShowHeader(boolean showHeader) {
        isShowHeader = showHeader;
    }

    public boolean isShowIconSave() {
        return isShowIconSave;
    }

    public void setShowIconSave(boolean showIconSave) {
        isShowIconSave = showIconSave;
    }

    public boolean isShowLikeAndCommentInfo() {
        return isShowLikeAndCommentInfo;
    }

    public void setShowLikeAndCommentInfo(boolean showLikeAndCommentInfo) {
        isShowLikeAndCommentInfo = showLikeAndCommentInfo;
    }

    public boolean isShowComment() {
        return isShowComment;
    }

    public void setShowComment(boolean showComment) {
        isShowComment = showComment;
    }

    public boolean isShowLike() {
        return isShowLike;
    }

    public void setShowLike(boolean showLike) {
        isShowLike = showLike;
    }

    public boolean isReponse() {
        return isReponse;
    }

    public void setReponse(boolean reponse) {
        isReponse = reponse;
    }

    public boolean isShowMore() {
        return isShowMore;
    }

    public void setShowMore(boolean showMore) {
        isShowMore = showMore;
    }
}

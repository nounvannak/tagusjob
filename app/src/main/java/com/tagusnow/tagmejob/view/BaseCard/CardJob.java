package com.tagusnow.tagmejob.view.BaseCard;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.AllPostActivity;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.ApplyJobActivity;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.JobDetail;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.PrivacyActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewDetailPostActivity;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.adapter.FeedAdapter;
import com.tagusnow.tagmejob.adapter.JobsAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.whalemare.sheetmenu.SheetMenu;

public class CardJob extends TagUsJobRelativeLayout implements View.OnClickListener{

    private static final String TAG = CardJob.class.getSimpleName();
    private static final int REQ_PRIVACY = 903;
    private Context context;
    /*Profile Information*/
    RelativeLayout profileBar;
    CircleImageView mProfilePicture;
    TextView mFeedStoryTitle,mFeedStoryTime;
    ImageView icon_saved;
    Button mButtonMoreFeed;
    /*Header*/
    private RelativeLayout header;
    private ImageView background,logo;

    private TextView txtTitle,txtCloseDate,txtHiring,txtQualification,txtWorkingTime,txtSalary,txtPosition;
    private Button btnShowDetail;

    /*Body*/
    private RelativeLayout like_and_comment_information;
//    private RelativeLayout body,body_content,layout_title,layout_salary,layout_close_date,layout_location,like_and_comment_information;
//    private Button apply_job;
//    private TextView title,salary,close_date,location,like_result,comment_result;
    private TextView like_result,comment_result;
    /*Footer*/
    private RelativeLayout footer;
    private LinearLayout like_button,comment_button,share_button;
    private ImageView like_icon;
    private TextView like_text;
    /*Object*/
    private SmUser user;
    private Feed feed;
    private int position;
    private RecyclerView.Adapter adapter;
    private FeedAdapter feedAdapter;
    private String AdapterTag;
    private boolean isShowHeader,isShowIconSave,isShowLikeAndCommentInfo,isShowComment,isShowLike,isReponse = true;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FeedService service = retrofit.create(FeedService.class);
    private JobsService jobsService = retrofit.create(JobsService.class);

    private MenuItem.OnMenuItemClickListener mClick = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.optionSavePost :
                    /*code*/
                    savePost();
                    break;
                case R.id.optionEditPost :
                    editPost();
                    break;
                case R.id.optionHide :
                    hidePost();
                    break;
                case R.id.optionDelete :
                    removePost();
                    break;
            }
            return false;
        }
    };
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (!activity.isFinishing()){
                int menu = 0;
                if (user!=null && user.getId()==feed.getUser_post().getId()){
                    menu = R.menu.menu_post_option_auth;
                }else{
                    menu = R.menu.menu_post_option_not_auth;
                }
                SheetMenu.with(activity)
                        .setAutoCancel(true)
                        .setMenu(menu)
                        .setClick(mClick).showIcons(true).show();
            }
        }
    };
    private Callback<SmTagFeed.Like> mLikeCallback = new Callback<SmTagFeed.Like>() {
        @Override
        public void onResponse(@NonNull Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
            if (response.isSuccessful()){
                feed.getFeed().setIs_like(response.body().isIs_like());
                feed.getFeed().setCount_likes(response.body().getCount_likes());
                adapter.notifyItemChanged(position);
                adapter.notifyDataSetChanged();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    like_text.setTextColor(context.getColor(R.color.colorIconSelect));
                }
                like_icon.setImageResource(R.drawable.ic_action_like_blue);
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Feed> mJobCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                setFeed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private DialogInterface.OnDismissListener mDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialogInterface) {
            dialogInterface.dismiss();
        }
    };

    private void callJob(){
        int user = this.user!=null ? this.user.getId() : 0;
        int id = this.feed!=null ? this.feed.getId() : 0;
        jobsService.callJob(user,id).enqueue(mJobCallback);
    }

    private void likePost(){
        if (this.user==null){
            Intent loginIntent = new Intent(context, LoginActivity.class);
            context.startActivity(loginIntent);
        }
        service.LikeFeed(feed.getId(),this.user.getId()).enqueue(mLikeCallback);
    }

    private void viewComment(){
        if (this.activity!=null){
            this.activity.startActivity(CommentActivity.createIntent(this.activity)
                    .putExtra(CommentActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("count_likes",feed.getFeed().getCount_likes())
                    .putExtra("is_like",feed.getFeed().isIs_like())
                    .putExtra("count_comments",feed.getFeed().getCount_comments())
                    .putExtra("feed_id",feed.getId())
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void shareContent(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,feed.getDetail());
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    private void viewPost(){
        String searchTitle = "";
        if (feed.getFeed().getOriginalHastTagText()==null || feed.getFeed().getOriginalHastTagText().equals("")){
            searchTitle = "";
        }
        
        if (this.activity!=null){
            this.activity.startActivity(ViewPostActivity.createIntent(this.activity)
                    .putExtra(ViewPostActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("searchTitle",searchTitle)
                    .putExtra("position",this.position)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewPostDetail(){
        if (this.activity!=null){
            this.activity.startActivity(ViewDetailPostActivity.createIntent(this.activity)
                    .putExtra(ViewDetailPostActivity.FEED,new Gson().toJson(this.feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void showMore(){
        if(this.activity!=null){
            this.activity.runOnUiThread(mRunnable);
        }
    }

    private void removePost(){
        service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setDelete(1);
                    adapter.notifyDataSetChanged();
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void hidePost(){
        service.HideFeed(feed.getId(),feed.getUser_id(),user.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setIs_hide(1);
                    adapter.notifyDataSetChanged();
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void editPrivacy(){
        Intent intent = new Intent(this.activity, PrivacyActivity.class);
        intent.putExtra(SuperConstance.PRIVACY,feed.getPrivacy());
        intent.putExtra("position",position);
        this.activity.startActivityForResult(intent,REQ_PRIVACY);
    }

    private void editPost(){
        if (this.feed!=null){
            this.activity.startActivity(AllPostActivity
                    .createIntent(this.activity)
                    .putExtra(AllPostActivity.FRAGMENT,AllPostActivity.EDIT_JOBS)
                    .putExtra(AllPostActivity.FEED,this.feed)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    private void savePost(){
        service.SaveFeed(feed.getId(),feed.getUser_id(),user.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setIs_save(1);
                    adapter.notifyDataSetChanged();
                }else {
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void showProfile(){
        if (this.user!=null){
            if (user.getId()==feed.getUser_post().getId()){
                if (this.activity!=null){
                    this.activity.startActivity(ProfileActivity
                            .createIntent(activity)
                            .putExtra(ProfileActivity.PROFILEA_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }else{
                if (this.activity!=null){
                    this.activity.startActivity(ViewProfileActivity
                            .createIntent(activity)
                            .putExtra(ViewProfileActivity.PROFILE_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
        }
    }

    private void initUI(Context context){
        inflate(context, R.layout.card_job_layout,this);
        this.context = context;

        txtTitle = (TextView)findViewById(R.id.txtTitle);
        txtCloseDate = (TextView)findViewById(R.id.txtCloseDate);
        txtHiring = (TextView)findViewById(R.id.txtHiring);
        txtPosition = (TextView)findViewById(R.id.txtPosition);
        txtQualification = (TextView)findViewById(R.id.txtQualification);
        txtSalary = (TextView)findViewById(R.id.txtSalary);
        txtWorkingTime = (TextView)findViewById(R.id.txtWorkingTime);

        btnShowDetail = (Button) findViewById(R.id.btnShowDetails);
        btnShowDetail.setOnClickListener(this);

        /*Profile Information*/
        profileBar = (RelativeLayout)findViewById(R.id.profileBar);
        mProfilePicture = (CircleImageView)findViewById(R.id.mProfilePicture);
        mFeedStoryTitle = (TextView)findViewById(R.id.mFeedStoryTitle);
        mFeedStoryTime = (TextView)findViewById(R.id.mFeedStoryTime);
        icon_saved = (ImageView)findViewById(R.id.icon_saved);
        mButtonMoreFeed = (Button)findViewById(R.id.mButtonMoreFeed);
        mProfilePicture.setOnClickListener(this);
        mButtonMoreFeed.setOnClickListener(this);
        profileBar.setOnClickListener(this);
        /*Header*/
        header = (RelativeLayout)findViewById(R.id.header);
        background = (ImageView)findViewById(R.id.background);
        logo = (ImageView)findViewById(R.id.logo);
        logo.setOnClickListener(this);

        like_and_comment_information = (RelativeLayout)findViewById(R.id.like_and_comment_information);

        like_result = (TextView)findViewById(R.id.like_result);
        comment_result = (TextView)findViewById(R.id.comment_result);
        /*Footer*/
        footer = (RelativeLayout)findViewById(R.id.footer);
        like_button = (LinearLayout)findViewById(R.id.like_button);
        comment_button = (LinearLayout)findViewById(R.id.comment_button);
        share_button = (LinearLayout)findViewById(R.id.share_button);
        like_icon = (ImageView)findViewById(R.id.like_icon);
        like_text = (TextView)findViewById(R.id.like_text);
        like_button.setOnClickListener(this);
        comment_button.setOnClickListener(this);
        share_button.setOnClickListener(this);
    }

    public CardJob(Context context) {
        super(context);
        this.initUI(context);
    }

    public CardJob(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public CardJob(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public CardJob(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    @Override
    public void onClick(View view) {
//        if (view==body_content){
//            this.NEW_SHOW_MORE();
////        }else if (view==apply_job){
////            this.ApplyJob();
//        }else
        if (view==like_button){
            this.likePost();
        }else if (view==comment_button){
            this.viewComment();
        }else if (view==share_button){
            this.shareContent();
        }else if (view==logo){
            this.NEW_SHOW_MORE();
        }else if (view==profileBar){
            this.NEW_SHOW_MORE();
        }else if (view==mProfilePicture){
            this.showProfile();
        }else if (view==mButtonMoreFeed){
            this.showMore();
        }else if (view==btnShowDetail){
            this.NEW_SHOW_MORE();
        }
    }

    private void ApplyJob() {
        if (this.feed==null){
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }

        if (this.activity!=null){
            this.activity.startActivity(ApplyJobActivity.createIntent(this.activity).putExtra(ApplyJobActivity.FEED,this.feed));
        }
    }

    /*Socket.IO*/
    Socket socket;
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Count Like "+feed.getId(),mCountLike);
        socket.on("Count Comment "+feed.getId(),mCountComment);
        socket.connect();
    }

    private Emitter.Listener mCountLike = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setIs_like(obj.getInt("is_like") == 1);
                                feed.getFeed().setCount_likes(obj.getInt("count_likes"));
                                isShowLike = true;
                                like_result.setVisibility(VISIBLE);
                                like_result.setText(feed.getFeed().getLike_result());
                                adapter.notifyDataSetChanged();
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountLike");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setCount_comments(obj.getInt("count_comments"));
                                isShowComment = true;
                                comment_result.setVisibility(VISIBLE);
                                comment_result.setText(activity.getString(R.string._d_comments,obj.getInt("count_comments")));
                                adapter.notifyDataSetChanged();
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountComment");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private void initFeed(Feed feed){
        this.initSocket();
        if (feed!=null){
            /*Profile Information*/
            if (feed.getUser_post()!=null){
                if (feed.getUser_post().getImage()!=null){
                    if (feed.getUser_post().getImage().contains("http")){
                        Glide.with(this.context)
                                .load(feed.getUser_post().getImage())
                                .centerCrop()
                                .error(R.drawable.ic_user_male_icon)
                                .into(mProfilePicture);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage()))
                                .centerCrop()
                                .error(R.drawable.ic_user_male_icon)
                                .into(mProfilePicture);
                    }
                }else {
                    Glide.with(this.context).load(R.drawable.ic_user_male_icon).into(mProfilePicture);
                }

                mFeedStoryTitle.setText(feed.getUser_post().getUser_name());
                mFeedStoryTime.setText(feed.getFeed().getTimeline());

                if (feed.getIs_save()==1){
                    icon_saved.setVisibility(VISIBLE);
                }else {
                    icon_saved.setVisibility(GONE);
                }
            }else {
                Toast.makeText(this.context,"No profile data",Toast.LENGTH_SHORT).show();
            }
            /*Header*/
            if (feed.getFeed().getAlbum()!=null && feed.getFeed().getAlbum().size() > 0){
                header.setVisibility(VISIBLE);
                if (this.feedAdapter!=null){
                    Glide.with(this.context)
                            .load(SuperUtil.getBaseUrl(SuperConstance.RANDOM_BACKGROUND[this.feedAdapter.getRandom_index()]))
                            .centerCrop()
                            .into(background);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getBaseUrl(SuperConstance.RANDOM_BACKGROUND[new Random().nextInt(4)]))
                            .centerCrop()
                            .into(background);
                }

                if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                    Glide.with(this.context)
                            .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                            .fitCenter()
                            .error(R.mipmap.ic_launcher_square)
                            .into(logo);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                            .fitCenter()
                            .error(R.mipmap.ic_launcher_square)
                            .into(logo);
                }
            }else {
                header.setVisibility(GONE);
            }

            /*Body*/
//            title.setText(feed.getFeed().getTitle());
//            salary.setText(feed.getFeed().getSalary());
//            close_date.setText(feed.getFeed().getCloseDate());
//            location.setText(feed.getFeed().getCity_name());

            txtTitle.setText(feed.getFeed().getTitle());
            txtCloseDate.setText(feed.getFeed().getCloseDate());
            txtPosition.setText(feed.getFeed().getCategory());

            if (feed.getFeed().getNum_employee()!=null && !feed.getFeed().getNum_employee().equals("")) {
                txtHiring.setText(feed.getFeed().getNum_employee());
            }else{
                txtHiring.setText("N/A");
            }

            if (feed.getFeed().getLevel()!=null && !feed.getFeed().getLevel().equals("")){
                txtQualification.setText(feed.getFeed().getLevel());
            }else {
                txtQualification.setText("N/A");
            }

            if (feed.getFeed().getWorking_time()!=null && !feed.getFeed().getWorking_time().equals("")){
                txtWorkingTime.setText(feed.getFeed().getWorking_time());
            }else {
                txtWorkingTime.setText("N/A");
            }

            if (feed.getFeed().getSalary()!=null && !feed.getFeed().getSalary().equals("")){
                txtSalary.setText(feed.getFeed().getSalary());
            }else {
                txtSalary.setText("N/A");
            }


            if (feed.getFeed().getCount_comments()==0 && feed.getFeed().getCount_likes()==0){
                isShowLikeAndCommentInfo = false;
            }else {
                isShowLikeAndCommentInfo = true;
            }

            if (isShowLikeAndCommentInfo){
                like_and_comment_information.setVisibility(VISIBLE);

                if (feed.getFeed().getCount_likes()==0){
                    isShowLike = false;
                }else {
                    isShowLike = true;
                }

                if (isShowLike){
                    like_result.setVisibility(VISIBLE);
                    like_result.setText(feed.getFeed().getLike_result());
                }else {
                    like_result.setVisibility(GONE);
                }

                if (feed.getFeed().getCount_comments()==0){
                    isShowComment = false;
                }else {
                    isShowComment = true;
                }

                if (isShowComment){
                    comment_result.setVisibility(VISIBLE);
                    comment_result.setText(this.activity.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                }else {
                    comment_result.setVisibility(GONE);
                }

            }else {
                like_and_comment_information.setVisibility(GONE);
            }

        }
    }

    private void NEW_SHOW_MORE(){
        if (this.activity!=null){
            this.activity.startActivity(JobDetail.createIntent(this.activity).putExtra(JobDetail.FEED,this.feed).putExtra(JobDetail.USER,this.user.getId()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    public SmUser getUser() {
        return user;
    }

    public void setUser(SmUser user) {
        this.user = user;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initFeed(feed);
    }

    public void setFeed(int id){
        if (this.feed!=null){
            this.feed.setId(id);
        }else {
            this.feed = new Feed();
            this.feed.setId(id);
        }
        this.callJob();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
        if (this.AdapterTag.equals(FeedAdapter.class.getSimpleName())){
            this.feedAdapter = (FeedAdapter)adapter;
        }else if (this.AdapterTag.equals(JobsAdapter.class.getSimpleName())){

        }
    }

    public Activity getActivity() {
        return activity;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public boolean isShowHeader() {
        return isShowHeader;
    }

    public void setShowHeader(boolean showHeader) {
        isShowHeader = showHeader;
    }

    public boolean isShowIconSave() {
        return isShowIconSave;
    }

    public void setShowIconSave(boolean showIconSave) {
        isShowIconSave = showIconSave;
    }

    public boolean isShowLikeAndCommentInfo() {
        return isShowLikeAndCommentInfo;
    }

    public void setShowLikeAndCommentInfo(boolean showLikeAndCommentInfo) {
        isShowLikeAndCommentInfo = showLikeAndCommentInfo;
    }

    public boolean isShowComment() {
        return isShowComment;
    }

    public void setShowComment(boolean showComment) {
        isShowComment = showComment;
    }

    public boolean isShowLike() {
        return isShowLike;
    }

    public void setShowLike(boolean showLike) {
        isShowLike = showLike;
    }

    public String getAdapterTag() {
        return AdapterTag;
    }

    public void setAdapterTag(String adapterTag) {
        AdapterTag = adapterTag;
    }

    public void setReponse(boolean reponse) {
        isReponse = reponse;
    }
}

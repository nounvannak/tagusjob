package com.tagusnow.tagmejob.view.BaseCard;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.PostJobActivity;
import com.tagusnow.tagmejob.PrivacyActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewDetailPostActivity;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;
import java.io.IOException;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.whalemare.sheetmenu.SheetMenu;

public class CardNoPadding extends RelativeLayout implements View.OnClickListener {
    private static final String TAG = CardNoPadding.class.getSimpleName();
    private static final int REQ_PRIVACY = 903;
    CircleImageView mProfilePicture;
    TextView mFeedStoryTitle,mFeedStoryTime,mLikeText,mLikeCounter,mCommentText,mCommentCounter,albumMoreThan4_text;
    ImageView mFeedPicture,mLikeIcon,mCommentIcon,album2_1,album2_2,album3_1,album3_2,album3_3,album4_1,album4_2,album4_3,album4_4,albumMoreThan4_1,albumMoreThan4_2,albumMoreThan4_3,albumMoreThan4_4;
    LinearLayout mFeedLikeButton,mFeedCommentButton,shareButton,album2,album3,album4,albumMoreThan4;
    LinearLayout feed_text_box,layout_job_description;
    RelativeLayout profileBar;
    Button mButtonMoreFeed;
    HtmlTextView mFeedTitle;
    private TextView job_description;
    ImageView icon_saved;
    private Context context;
    private Feed feed;
    private int position;
    private SmUser authUser;
    private Activity activity;
    private RecyclerView.Adapter adapter;
    /*Socket.IO*/
    Socket socket;
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (!activity.isFinishing()){
                int menu = 0;
                if (authUser!=null && authUser.getId()==feed.getUser_post().getId()){
                    menu = R.menu.menu_post_option_auth;
                }else{
                    menu = R.menu.menu_post_option_not_auth;
                }
                SheetMenu.with(context)
                        .setAutoCancel(true)
                        .setMenu(menu)
                        .setClick(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                switch (menuItem.getItemId()){
                                    case R.id.optionSavePost :
                                        /*code*/
                                        savePost();
                                        break;
                                    case R.id.optionEditPost :
                                        editPost();
                                        break;
                                    case R.id.optionHide :
                                        hidePost();
                                        break;
                                    case R.id.optionDelete :
                                        removePost();
                                        break;
                                }
                                return false;
                            }
                        }).showIcons(true).show();
            }
        }
    };

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Count Like "+feed.getId(),mCountLike);
        socket.on("Count Comment "+feed.getId(),mCountComment);
        socket.connect();
    }

    private Emitter.Listener mCountLike = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setIs_like(obj.getInt("is_like") == 1);
                                feed.getFeed().setCount_likes(obj.getInt("count_likes"));
                                adapter.notifyItemChanged(0);
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountLike");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setCount_comments(obj.getInt("count_comments"));
                                adapter.notifyItemChanged(0);
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountComment");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    /*Service*/
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    private JobsService jobsService = retrofit.create(JobsService.class);
    private Callback<Feed> mJobCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                feed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void callJob(){
        int user = this.authUser!=null ? this.authUser.getId() : 0;
        int id = this.feed!=null ? this.feed.getId() : 0;
        jobsService.callJob(user,id).enqueue(mJobCallback);
    }

    public CardNoPadding Auth(SmUser user){
        this.authUser = user;
        return this;
    }

    public CardNoPadding Position(int position){
        this.position = position;
        return this;
    }

    public CardNoPadding Adapter(RecyclerView.Adapter adapter){
        this.adapter = adapter;
        return this;
    }

    public CardNoPadding Feed(int id){
        if (this.feed!=null){
            this.feed.setId(id);
        }else {
            this.feed = new Feed();
            this.feed.setId(id);
        }
        this.callJob();
        return this;
    }

    public CardNoPadding Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public CardNoPadding(@NonNull Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.card_no_padding,this);
        mProfilePicture = (CircleImageView) findViewById(R.id.mProfilePicture);
        mFeedStoryTitle = (TextView) findViewById(R.id.mFeedStoryTitle);
        mFeedStoryTime = (TextView) findViewById(R.id.mFeedStoryTime);
        mButtonMoreFeed = (Button) findViewById(R.id.mButtonMoreFeed);
        mFeedPicture = (ImageView) findViewById(R.id.mFeedPicture);
        mFeedTitle = (HtmlTextView) findViewById(R.id.mFeedTitle);
        job_description = (HtmlTextView) findViewById(R.id.job_description);
        feed_text_box = (LinearLayout) findViewById(R.id.feed_text_box);
        layout_job_description = (LinearLayout) findViewById(R.id.layout_job_description);
        profileBar = (RelativeLayout) findViewById(R.id.profileBar);
        /*Album More 4*/
        albumMoreThan4_text = (TextView) findViewById(R.id.albumMoreThan4_text);
        albumMoreThan4 = (LinearLayout) findViewById(R.id.albumMoreThan4);
        albumMoreThan4_1 = (ImageView) findViewById(R.id.albumMoreThan4_1);
        albumMoreThan4_2 = (ImageView) findViewById(R.id.albumMoreThan4_2);
        albumMoreThan4_3 = (ImageView) findViewById(R.id.albumMoreThan4_3);
        albumMoreThan4_4 = (ImageView) findViewById(R.id.albumMoreThan4_4);
        /*Album 4*/
        album4 = (LinearLayout) findViewById(R.id.album4);
        album4_1 = (ImageView) findViewById(R.id.album4_1);
        album4_2 = (ImageView) findViewById(R.id.album4_2);
        album4_3 = (ImageView) findViewById(R.id.album4_3);
        album4_4 = (ImageView) findViewById(R.id.album4_4);
        /*Album 3*/
        album3 = (LinearLayout) findViewById(R.id.album3);
        album3_1 = (ImageView) findViewById(R.id.album3_1);
        album3_2 = (ImageView) findViewById(R.id.album3_2);
        album3_3 = (ImageView) findViewById(R.id.album3_3);
        /*Album 2*/
        album2 = (LinearLayout) findViewById(R.id.album2);
        album2_1 = (ImageView) findViewById(R.id.album2_1);
        album2_2 = (ImageView) findViewById(R.id.album2_2);

        //*Like Button*//
        mFeedLikeButton = (LinearLayout) findViewById(R.id.mFeedLikeButton);
        mLikeIcon = (ImageView) findViewById(R.id.likeIcon);
        mLikeText = (TextView) findViewById(R.id.likeText);
        mLikeCounter = (TextView) findViewById(R.id.likeCounter);
        //*Like Button*//
        mFeedCommentButton = (LinearLayout) findViewById(R.id.mFeedCommentButton);
        mCommentIcon = (ImageView) findViewById(R.id.commentIcon);
        mCommentText = (TextView) findViewById(R.id.commentText);
        mCommentCounter = (TextView) findViewById(R.id.commentCounter);
        shareButton = (LinearLayout) findViewById(R.id.shareButton);
        icon_saved = (ImageView) findViewById(R.id.icon_saved);

        mFeedCommentButton.setOnClickListener(this);
        mFeedLikeButton.setOnClickListener(this);
        mButtonMoreFeed.setOnClickListener(this);
        mFeedPicture.setOnClickListener(this);
        album2.setOnClickListener(this);
        album3.setOnClickListener(this);
        album4.setOnClickListener(this);
        albumMoreThan4.setOnClickListener(this);
        profileBar.setOnClickListener(this);
        job_description.setOnClickListener(this);
        shareButton.setOnClickListener(this);
        mFeedStoryTitle.setOnClickListener(this);
        mProfilePicture.setOnClickListener(this);
        mFeedTitle.setOnClickListener(this);
    }

    private void likePost(){
        if (authUser==null){
            Intent loginIntent = new Intent(context, LoginActivity.class);
            context.startActivity(loginIntent);
        }
        AsyncHttpClient mLikeFeedClient = new AsyncHttpClient();
        RequestParams mLikeFeedParam = new RequestParams();
        mLikeFeedParam.put("feed_id",feed.getId());
        mLikeFeedParam.put("user_id",authUser.getId());
        if (!feed.getFeed().isIs_like()){
            mLikeFeedClient.post(SuperUtil.getBaseUrl(SuperConstance.LIKE_FEED), mLikeFeedParam, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                    Log.w("like response",rawJsonResponse);
                    SmTagFeed.Like mLikeClick = new Gson().fromJson(rawJsonResponse, SmTagFeed.Like.class);
                    Log.w("like",mLikeClick.getCount_likes()+"");
                    feed.getFeed().setIs_like(mLikeClick.isIs_like());
                    feed.getFeed().setCount_likes(mLikeClick.getCount_likes());
//                    adapter.notifyDataSetChanged();
                    mLikeCounter.setVisibility(View.VISIBLE);
                    mLikeCounter.setText(String.valueOf(feed.getFeed().getCount_likes()));
                    if (mLikeClick.isIs_like()){
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIconSelect));
                        mLikeText.setTextColor(context.getColor(R.color.colorIconSelect));
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_blue);
                    }else {
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIcon));
                        mLikeText.setTextColor(context.getColor(R.color.colorIcon));
                        mLikeIcon.setImageResource(R.drawable.ic_action_like_black);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                    Log.w("like response",rawJsonData);
                }

                @Override
                protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                    return null;
                }
            });
        }
    }

    private void viewComment(){
        if (this.activity!=null){
            this.activity.startActivity(CommentActivity.createIntent(this.activity)
                    .putExtra(CommentActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("count_likes",feed.getFeed().getCount_likes())
                    .putExtra("is_like",feed.getFeed().isIs_like())
                    .putExtra("count_comments",feed.getFeed().getCount_comments())
                    .putExtra("feed_id",feed.getId())
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void shareContent(){
        originalShare();
    }

    private void originalShare(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,feed.getDetail());
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    private void viewPost(){
        String searchTitle = "";
        if (feed.getFeed().getOriginalHastTagText()==null || feed.getFeed().getOriginalHastTagText().equals("")){
            searchTitle = "";
        }else{
            searchTitle = mFeedTitle.getText().toString();
        }
        if (this.activity!=null){
            this.activity.startActivity(ViewPostActivity.createIntent(this.activity)
                    .putExtra(ViewPostActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("",searchTitle)
                    .putExtra("",this.position)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewPostDetail(){
        if (this.activity!=null){
            this.activity.startActivity(ViewDetailPostActivity.createIntent(this.activity)
                    .putExtra(ViewDetailPostActivity.FEED,new Gson().toJson(this.feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void showMore(){
        this.activity.runOnUiThread(mRunnable);
    }

    private void removePost(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("post_id",feed.getId());
        params.put("auth_user",authUser.getId());
        client.put(SuperUtil.getBaseUrl(SuperConstance.REMOVE_POST), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                feed.setDelete(1);
//                adapter.notifyItemRemoved(position);
                Log.w(TAG,rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void hidePost(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("user_post_id",feed.getUser_id());
        params.put("post_id",feed.getId());
        params.put("user_hide_id",authUser.getId());
        client.put(SuperUtil.getBaseUrl(SuperConstance.HIDE_POST), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                feed.setIs_hide(1);
//                adapter.notifyItemRemoved(position);
                Log.w(TAG,rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void editPrivacy(){
        Intent intent = new Intent(this.activity, PrivacyActivity.class);
        intent.putExtra(SuperConstance.PRIVACY,feed.getPrivacy());
        intent.putExtra("position",position);
        this.activity.startActivityForResult(intent,REQ_PRIVACY);
    }

    private void editPost(){
        if (this.feed!=null){
            if (this.feed.getFeed().getType().equals(SuperConstance.POST_JOB)){
                if (this.activity!=null){
                    this.activity.startActivity(new PostJobActivity()
                            .isNew(false)
                            .setTitle("Edit post job")
                            .createIntent(this.activity)
                            .putExtra(PostJobActivity.FEED,new Gson().toJson(this.feed))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Acvitity",Toast.LENGTH_SHORT).show();
                }
            }else {
                if (this.activity!=null){
                    this.activity.startActivity(CreatePostActivity.createIntent(this.activity)
                            .putExtra(CreatePostActivity.FEED,new Gson().toJson(this.feed))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    private void savePost(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("user_post_id",feed.getUser_id());
        params.put("post_id",feed.getId());
        params.put("user_save_id",authUser.getId());
        client.put(SuperUtil.getBaseUrl(SuperConstance.SAVE_POST), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                feed.setIs_save(1);
//                adapter.notifyDataSetChanged();
                icon_saved.setVisibility(View.VISIBLE);
                Log.w(TAG,rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void showProfile(){
        if (this.authUser!=null){
            if (authUser.getId()==feed.getUser_post().getId()){
                if (this.activity!=null){
                    this.activity.startActivity(ProfileActivity
                            .createIntent(activity)
                            .putExtra(ProfileActivity.PROFILEA_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }else{
                if (this.activity!=null){
                    this.activity.startActivity(ViewProfileActivity
                            .createIntent(activity)
                            .putExtra(ViewProfileActivity.PROFILE_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==mProfilePicture){
            showProfile();
        }else if (view==mFeedStoryTitle){
            showProfile();
        }else if (view==shareButton){
            shareContent();
        }else if (view==job_description){
            viewPostDetail();
        }else if (view==profileBar){
            viewPost();
        }else if (view==albumMoreThan4){
            viewPostDetail();
        }else if (view==album4){
            viewPostDetail();
        }else if (view==album3){
            viewPostDetail();
        }else if (view==album2){
            viewPostDetail();
        }else if (view==mFeedPicture){
            viewPostDetail();
        }else if (view==mButtonMoreFeed){
            showMore();
        }else if (view==mFeedLikeButton){
            likePost();
        }else if (view==mFeedCommentButton){
            viewComment();
        }else if (view==mFeedTitle){
//            viewPostDetail();
        }
    }

    public HtmlTextView getmFeedTitle() {
        return mFeedTitle;
    }

    public ImageView getIcon_saved() {
        return icon_saved;
    }

    public LinearLayout getmFeedCommentButton() {
        return mFeedCommentButton;
    }

    public LinearLayout getmFeedLikeButton() {
        return mFeedLikeButton;
    }

    public Button getmButtonMoreFeed() {
        return mButtonMoreFeed;
    }

    public ImageView getmFeedPicture() {
        return mFeedPicture;
    }

    public LinearLayout getAlbum2() {
        return album2;
    }

    public LinearLayout getAlbum3() {
        return album3;
    }

    public LinearLayout getAlbum4() {
        return album4;
    }

    public LinearLayout getAlbumMoreThan4() {
        return albumMoreThan4;
    }

    public RelativeLayout getProfileBar() {
        return profileBar;
    }

    public TextView getJob_description() {
        return job_description;
    }

    public LinearLayout getShareButton() {
        return shareButton;
    }

    public TextView getmFeedStoryTitle() {
        return mFeedStoryTitle;
    }

    public CircleImageView getmProfilePicture() {
        return mProfilePicture;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public CardNoPadding feed(Feed feed){
        this.feed = feed;
        initSocket();
        mFeedPicture.setVisibility(View.GONE);
        album2.setVisibility(View.GONE);
        album3.setVisibility(View.GONE);
        album4.setVisibility(View.GONE);
        albumMoreThan4.setVisibility(View.GONE);
        layout_job_description.setVisibility(View.GONE);

        mFeedStoryTitle.setText(feed.getUser_post().getUser_name());
        if (feed.getUser_post().getImage().contains("http")) {
            Glide.with(context)
                    .load(feed.getUser_post().getImage())
                    .error(android.R.drawable.ic_menu_report_image)
                    .centerCrop()
                    .override(100, 100)
                    .into(mProfilePicture);
        } else {
            Glide.with(context)
                    .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage(), "200"))
                    .error(android.R.drawable.ic_menu_report_image)
                    .centerCrop()
                    .override(100, 100)
                    .into(mProfilePicture);
        }

        if (feed.getIs_save() == 0) {
            icon_saved.setVisibility(View.GONE);
        } else {
            icon_saved.setVisibility(View.VISIBLE);
        }

        if (feed.getType().equals(Feed.TAGUSJOB)) {
            if (feed.getFeed()!=null && feed.getFeed().getType().equals(FeedDetail.NORMAL_POST)) {
                layout_job_description.setVisibility(View.GONE);
                mFeedStoryTime.setText(feed.getFeed().getTimeline());
                if (feed.getFeed().getOriginalHastTagText() == null || feed.getFeed().getOriginalHastTagText().equals("")) {
                    feed_text_box.setVisibility(View.GONE);
                } else {
                    feed_text_box.setVisibility(View.VISIBLE);
                    mFeedTitle.setHtml(feed.getFeed().getOriginalHastTagText());
                }

                if (feed.getFeed().isIs_like()) {
                    mLikeIcon.setImageResource(R.drawable.ic_action_like_blue);
                    mLikeText.setTextColor(context.getColor(R.color.colorAccent));
                    mLikeCounter.setTextColor(context.getColor(R.color.colorAccent));
                } else {
                    mLikeIcon.setImageResource(R.drawable.ic_action_like_black);
                    mLikeText.setTextColor(context.getColor(R.color.colorIcon));
                    mLikeCounter.setTextColor(context.getColor(R.color.colorIcon));
                }

                if (feed.getFeed().getCount_likes() > 0) {
                    mLikeCounter.setVisibility(View.VISIBLE);
                    mLikeCounter.setText(String.valueOf(feed.getFeed().getCount_likes()));
                } else {
                    mLikeCounter.setVisibility(View.GONE);
                }

                if (feed.getFeed().getCount_comments() > 0) {
                    mCommentCounter.setVisibility(View.VISIBLE);
                    mCommentCounter.setText(String.valueOf(feed.getFeed().getCount_comments()));
                } else {
                    mCommentCounter.setVisibility(View.GONE);
                }

                if (feed.getFeed().getAlbum()!=null){
                    if (feed.getFeed().getAlbum() == null || feed.getFeed().getAlbum().size() == 1 && (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc() == null || feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().equals(""))) {
                        mFeedPicture.setVisibility(View.GONE);
                    } else {
                        if (feed.getFeed().getAlbum().size() == 1) {
                            mFeedPicture.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(mFeedPicture);
                        } else if (feed.getFeed().getAlbum().size() == 2) {
                            album2.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album2_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(1).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album2_2);
                        } else if (feed.getFeed().getAlbum().size() == 3) {
                            album3.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album3_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(1).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album3_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(2).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album3_3);
                        } else if (feed.getFeed().getAlbum().size() == 4) {
                            album4.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album4_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(1).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album4_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(2).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album4_3);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(3).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album4_4);
                        } else if (feed.getFeed().getAlbum().size() > 4) {
                            albumMoreThan4.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(albumMoreThan4_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(1).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(albumMoreThan4_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(2).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(albumMoreThan4_3);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(3).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(albumMoreThan4_4);
                            albumMoreThan4_text.setText(context.getString(R.string.view_all_image, feed.getFeed().getAlbum().size()));
                        }
                    }
                }else{
                    mFeedPicture.setVisibility(View.GONE);
                }

            } else {
                layout_job_description.setVisibility(View.VISIBLE);
                /*Job Description*/
                job_description.setText(feed.getFeed().getJobDescription());

                mFeedStoryTime.setText(feed.getFeed().getTimeline());
                if (feed.getFeed().getOriginalHastTagText() == null || feed.getFeed().getOriginalHastTagText().equals("")) {
                    feed_text_box.setVisibility(View.GONE);
                } else {
                    feed_text_box.setVisibility(View.VISIBLE);
                    mFeedTitle.setHtml(feed.getFeed().getOriginalHastTagText());
                }

                if (feed.getFeed().isIs_like()) {
                    mLikeIcon.setImageResource(R.drawable.ic_action_like_blue);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mLikeText.setTextColor(context.getColor(R.color.colorAccent));
                        mLikeCounter.setTextColor(context.getColor(R.color.colorAccent));
                    }
                } else {
                    mLikeIcon.setImageResource(R.drawable.ic_action_like_black);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mLikeText.setTextColor(context.getColor(R.color.colorIcon));
                        mLikeCounter.setTextColor(context.getColor(R.color.colorIcon));
                    }
                }

                if (feed.getFeed().getCount_likes() > 0) {
                    mLikeCounter.setVisibility(View.VISIBLE);
                    mLikeCounter.setText(String.valueOf(feed.getFeed().getCount_likes()));
                } else {
                    mLikeCounter.setVisibility(View.GONE);
                }

                if (feed.getFeed().getCount_comments() > 0) {
                    mCommentCounter.setVisibility(View.VISIBLE);
                    mCommentCounter.setText(String.valueOf(feed.getFeed().getCount_comments()));
                } else {
                    mCommentCounter.setVisibility(View.GONE);
                }

                if (feed.getFeed().getAlbum()!=null){
                    if (feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() == 1 && (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc() == null || feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().equals(""))) {
                        mFeedPicture.setVisibility(View.GONE);
                    } else {
                        if (feed.getFeed().getAlbum().size() == 1) {
                            mFeedPicture.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .fitCenter()
                                    .into(mFeedPicture);
                        } else if (feed.getFeed().getAlbum().size() == 2) {
                            album2.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album2_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(1).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album2_2);
                        } else if (feed.getFeed().getAlbum().size() == 3) {
                            album3.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album3_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(1).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album3_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(2).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album3_3);
                        } else if (feed.getFeed().getAlbum().size() == 4) {
                            album4.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album4_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(1).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album4_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(2).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album4_3);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(3).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(album4_4);
                        } else if (feed.getFeed().getAlbum().size() > 4) {
                            albumMoreThan4.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(albumMoreThan4_1);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(1).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(albumMoreThan4_2);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(2).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(albumMoreThan4_3);
                            Glide.with(context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(3).getMedia().getImage().getSrc(), "500"))
                                    .error(android.R.drawable.ic_menu_report_image)
                                    .centerCrop()
                                    .into(albumMoreThan4_4);
                            albumMoreThan4_text.setText(context.getString(R.string.view_all_image, feed.getFeed().getAlbum().size()));
                        }
                    }
                }else{
                    mFeedPicture.setVisibility(View.GONE);
                }
            }
        } else if (feed.getType().equals(SuperConstance.POST_JOB)) {

        }

        return this;
    }
}

package com.tagusnow.tagmejob.view.BaseCard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.AllPostActivity;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.JobDetail;
import com.tagusnow.tagmejob.PostJobActivity;
import com.tagusnow.tagmejob.PrivacyActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.whalemare.sheetmenu.SheetMenu;

public class CardSmall extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = CardSmall.class.getSimpleName();
    private static final int REQ_PRIVACY = 903;
    private Context context;
    private Feed feed;
    private ImageView image;
    private ImageButton btnMore;
    private TextView title,salary,close_date,location;
    private boolean isRecent = false, isExpired = false;
    private RecentSearch recentSearch;
    private SmUser user;
    private int position;
    private RecyclerView.Adapter adapter;
    /*Service*/
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private JobsService jobsService = ServiceGenerator.createService(JobsService.class);
    private RecentSearchService recentSearchService = ServiceGenerator.createService(RecentSearchService.class);
    private Callback<RecentSearch> mCallback = new Callback<RecentSearch>() {
        @Override
        public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                recentSearch = response.body();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearch> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Feed> mJobCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                Feed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void callJob(){
        int user = this.user!=null ? this.user.getId() : 0;
        int id = this.feed!=null ? this.feed.getId() : 0;
        jobsService.callJob(user,id).enqueue(mJobCallback);
    }

    private void saveRecent(){
        int user = this.user!=null ? this.user.getId() : 0;
        recentSearchService.save(this.recentSearch,user).enqueue(mCallback);
    }

    public CardSmall(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.card_small,this);
        image = (ImageView)findViewById(R.id.image);
        title = (TextView)findViewById(R.id.title);
        salary = (TextView)findViewById(R.id.salary);
        close_date = (TextView)findViewById(R.id.close_date);
        btnMore = (ImageButton)findViewById(R.id.btnMore);
        location = (TextView)findViewById(R.id.location);
        this.recentSearch = new RecentSearch();
        btnMore.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public CardSmall Activity(Activity activity){
        setActivity(activity);
        return this;
    }

    private void initFeed(Feed feed){
        this.feed = feed;
        if (feed!=null){
            if (feed.getFeed().getAlbum()!=null){
                if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                    this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                    Glide.with(this.context)
                            .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                            .error(R.mipmap.ic_launcher_square)
                            .fitCenter()
                            .into(image);
                }else {
                    this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                    Glide.with(this.context)
                            .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"300"))
                            .error(R.mipmap.ic_launcher_square)
                            .fitCenter()
                            .into(image);
                }
            }else {
                this.recentSearch.setSearch_image("");
                Glide.with(this.context)
                        .load(R.mipmap.ic_launcher_square)
                        .centerCrop()
                        .into(image);
            }
            title.setText(feed.getFeed().getTitle());
            salary.setText(feed.getFeed().getSalary());
            close_date.setText(feed.getFeed().getCloseDate());
            location.setText(feed.getFeed().getCity_name());
            this.recentSearch.setMaster_id(this.feed.getId());
            this.recentSearch.setSearch_type(RecentSearch.JOBS);
            this.recentSearch.setSearch_text(this.feed.getFeed().getTitle());
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    public CardSmall Feed(int id){
        if (this.feed!=null){
            this.feed.setId(id);
        }else {
            this.feed = new Feed();
            this.feed.setId(id);
        }
        this.callJob();
        return this;
    }

    public CardSmall Feed(Feed feed){
        this.feed = feed;
        if (feed!=null){
            if (feed.getFeed().getAlbum()!=null){
                if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                    this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                    Glide.with(this.context)
                            .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                            .error(R.mipmap.ic_launcher_square)
                            .fitCenter()
                            .into(image);
                }else {
                    this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                    Glide.with(this.context)
                            .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"300"))
                            .error(R.mipmap.ic_launcher_square)
                            .fitCenter()
                            .into(image);
                }
            }else {
                this.recentSearch.setSearch_image("");
                Glide.with(this.context)
                        .load(R.mipmap.ic_launcher_square)
                        .fitCenter()
                        .into(image);
            }
            title.setText(feed.getFeed().getTitle());
            salary.setText(feed.getFeed().getSalary());
            close_date.setText(feed.getFeed().getCloseDate());
            location.setText(feed.getFeed().getCity_name());
            this.recentSearch.setMaster_id(this.feed.getId());
            this.recentSearch.setSearch_type(RecentSearch.JOBS);
            this.recentSearch.setSearch_text(this.feed.getFeed().getTitle());
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
        return this;
    }

    private void viewDetail(){

        if (isRecent){
            saveRecent();
        }

        if (this.activity!=null){
            this.activity.startActivity(JobDetail.createIntent(this.activity).putExtra(JobDetail.FEED,this.feed).putExtra(JobDetail.USER,this.user.getId()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            viewDetail();
        }else if (view==btnMore){
            showMore();
        }
    }

    private void showMore(){
        if(this.activity!=null){
            this.activity.runOnUiThread(mRunnable);
        }
    }

    private void removePost(){
        service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setDelete(1);
                    adapter.notifyDataSetChanged();
                }else {
                    ((TagUsJobActivity)activity).ShowAlert("Remove Feed",response.message());
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void hidePost(){
        service.HideFeed(feed.getId(),feed.getUser_id(),user.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setIs_hide(1);
                }else {
                    ((TagUsJobActivity)activity).ShowAlert("Remove Feed",response.message());
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void editPrivacy(){
        Intent intent = new Intent(this.activity, PrivacyActivity.class);
        intent.putExtra(SuperConstance.PRIVACY,feed.getPrivacy());
        intent.putExtra("position",position);
        this.activity.startActivityForResult(intent,REQ_PRIVACY);
    }

    private void editPost(){
        if (this.feed!=null){
            this.activity.startActivity(AllPostActivity
                    .createIntent(this.activity)
                    .putExtra(AllPostActivity.FRAGMENT,AllPostActivity.EDIT_JOBS)
                    .putExtra(AllPostActivity.FEED,this.feed)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    private void savePost(){
        service.SaveFeed(feed.getId(),feed.getUser_id(),user.getId()).enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if (response.isSuccessful()){
                    feed.setIs_save(1);
                    adapter.notifyDataSetChanged();
                }else {
                    ((TagUsJobActivity)activity).ShowAlert("Remove Feed",response.message());
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
    private MenuItem.OnMenuItemClickListener mClick = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.optionSavePost :
                    /*code*/
                    savePost();
                    break;
                case R.id.optionEditPost :
                    editPost();
                    break;
                case R.id.optionHide :
                    hidePost();
                    break;
                case R.id.optionDelete :
                    removePost();
                    break;
            }
            return false;
        }
    };
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (!activity.isFinishing()){
                int menu = 0;
                if (user!=null && user.getId()==feed.getUser_post().getId()){
                    if (isExpired){
                        menu = R.menu.menu_post_option_auth;
                    }else {
                        menu = R.menu.menu_post_option_auth;
                    }
                }else{
                    menu = R.menu.menu_post_option_not_auth;
                }
                SheetMenu.with(activity)
                        .setAutoCancel(true)
                        .setMenu(menu)
                        .setClick(mClick).showIcons(true).show();
            }
        }
    };

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setRecent(boolean recent) {
        isRecent = recent;
    }

    public CardSmall Auth(SmUser user) {
        this.user = user;
        this.recentSearch.setSearch_user_id(user.getId());
        return this;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    public void setExpired(boolean expired) {
        isExpired = expired;
    }
}

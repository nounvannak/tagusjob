package com.tagusnow.tagmejob.view.BaseCard;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.ExpiredJobsActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import java.io.IOException;
import java.util.Calendar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ExpiredJobsFace extends RelativeLayout implements View.OnLongClickListener, View.OnClickListener {

    private static final String TAG = ExpiredJobsFace.class.getSimpleName();
    private Activity activity;
    private Context context;
    private Feed feed;
    private SmUser user;
    private int pos;
    private RecyclerView.Adapter adapter;
    private ImageView image;
    private Button btnRenew;
    private TextView title,position,close_date;
    private Calendar mCalendar;
    private EditText txtClose_date;
    /*Service*/
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    private JobsService service = retrofit.create(JobsService.class);
    private Callback<Feed> mCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initRenewJobs(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            new ExpiredJobsActivity().dismissProgress();
            t.printStackTrace();
        }
    };
    private DialogInterface.OnClickListener mRenew = new DialogInterface.OnClickListener() {
        @Override
        public void onClick (DialogInterface dialog,int id){
            String close_date = txtClose_date.getText().toString();
            if (close_date.isEmpty()) {
                Toast.makeText(context, "Please, set date to renew.", Toast.LENGTH_LONG).show();
                return;
            } else {
                renewJobs();
                dialog.dismiss();
            }
        }
    };
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (!activity.isFinishing()){
                // get prompts.xml view
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                View promptView = layoutInflater.inflate(R.layout.renew_face, null);
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                // set prompts.xml to be the layout file of the alertdialog builder
                alertDialogBuilder.setTitle("Set date to renew").setView(promptView);
                txtClose_date = (EditText) promptView.findViewById(R.id.close_date);
                txtClose_date.setOnClickListener(ExpiredJobsFace.this);
                alertDialogBuilder.setCancelable(false).setPositiveButton("Renew",mRenew).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick (DialogInterface dialog,int id){
                        dialog.cancel();
                    }
                });
                // create an alert dialog
                AlertDialog alertD = alertDialogBuilder.create();
                alertD.show();
            }
        }
    };

    private Runnable mShowCard = new Runnable() {
        @Override
        public void run() {
            if (!activity.isFinishing()){
                new AlertDialog.Builder(context).setView(new Card(context).feed(feed).Activity(activity).Auth(user).Adapter(adapter)).setCancelable(true).create().show();
            }
        }
    };

    private void showCard(){
        this.activity.runOnUiThread(mShowCard);
    }

    private void initRenewJobs(Feed feed){
        this.feed = feed;
        this.adapter.notifyItemRemoved(this.pos);
        this.adapter.notifyDataSetChanged();
    }

    private void renewJobs(){
        int user = this.user!=null ? this.user.getId() : 0;
        service.renewJobs(this.feed,user).enqueue(mCallback);
    }

    private void onRenew(){
        this.activity.runOnUiThread(mRunnable);
    }

    public ExpiredJobsFace(Context context) {
        super(context);
        inflate(context, R.layout.expired_jobs_face,this);
        this.context = context;
        image = (ImageView)findViewById(R.id.image);
        btnRenew = (Button)findViewById(R.id.btnRenew);
        title = (TextView)findViewById(R.id.title);
        position = (TextView)findViewById(R.id.position);
        close_date = (TextView)findViewById(R.id.close_date);
        this.setOnLongClickListener(this);
        btnRenew.setOnClickListener(this);
    }

    public ExpiredJobsFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public ExpiredJobsFace Adapter(RecyclerView.Adapter adapter,int pos){
        this.adapter = adapter;
        this.pos = pos;
        return this;
    }

    public ExpiredJobsFace Auth(SmUser user){
        this.user = user;
        return this;
    }

    public ExpiredJobsFace Feed(Feed feed){
        this.feed = feed;
        if (feed!=null){
            if (feed.getFeed().getAlbum()!=null){
                if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                    Glide.with(this.context).load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                            .error(R.mipmap.ic_launcher_square)
                            .fitCenter()
                            .into(image);
                }else {
                    Glide.with(this.context).load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"300"))
                            .error(R.mipmap.ic_launcher_square)
                            .fitCenter()
                            .into(image);
                }
            }else {
                Glide.with(this.context)
                        .load(R.mipmap.ic_launcher_square)
                        .fitCenter()
                        .into(image);
            }
            title.setText(feed.getFeed().getTitle());
            position.setText(feed.getFeed().getCategory());
            close_date.setText(feed.getFeed().getClose_date());
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
        return this;
    }

    public Feed getFeed() {
        return feed;
    }

    @Override
    public boolean onLongClick(View view) {
        if (view==this){
            showCard();
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view==btnRenew){
            onRenew();
        }else if (view==txtClose_date){
            mCalendar = Calendar.getInstance();
            DatePickerDialog dpd = new DatePickerDialog(context,new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {
                    txtClose_date.setText(String.valueOf(dayOfMonth +"/"+ (monthOfYear + 1) + "/" + year));
                    String setCloseDate = String.valueOf(year +"-"+ (monthOfYear > 9 ? (monthOfYear + 1) : ("0" + (monthOfYear + 1))) + "-" + dayOfMonth);
                    feed.getFeed().setClose_date(setCloseDate);
                }
            }, mCalendar.get(Calendar.YEAR),mCalendar.get(Calendar.MONTH),mCalendar.get(Calendar.DATE));
            dpd.show();
        }
    }
}

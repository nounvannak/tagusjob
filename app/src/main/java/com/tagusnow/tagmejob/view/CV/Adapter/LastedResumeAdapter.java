package com.tagusnow.tagmejob.view.CV.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.view.CV.LastedResume;
import com.tagusnow.tagmejob.view.CV.LastedResumeHolder;
import com.tagusnow.tagmejob.view.CV.LastedResumeInfo;
import com.tagusnow.tagmejob.view.CV.LastedResumeInfoHolder;

import java.util.List;

public class LastedResumeAdapter extends RecyclerView.Adapter {

    private static final int MORE_VIEW = 0;
    private static final int CV = 2;
    private static final int INFO = 3;
    private Activity activity;
    private Context context;
    private FeedResponse feedResponse;
    private List<Feed> feeds;
    private String title,sub_title;

    public LastedResumeAdapter(Activity activity,FeedResponse feedResponse){
        this.activity = activity;
        this.context = activity;
        this.feedResponse = feedResponse;
        this.feeds = feedResponse.getData();
    }

    public LastedResumeAdapter(Activity activity,FeedResponse feedResponse,String title,String sub_title){
        this.activity = activity;
        this.context = activity;
        this.title = title;
        this.sub_title = sub_title;
        this.feedResponse = feedResponse;
        this.feeds = feedResponse.getData();
    }

    public LastedResumeAdapter Update(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds.addAll(feedResponse.getData());
        return this;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;

        if (position==0){
            type = CV;
        }else {
            type = CV;
        }

        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==CV){
            return new LastedResumeHolder(new LastedResume(this.context));
        }else {
            return new LastedResumeInfoHolder(new LastedResumeInfo(this.context));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==CV){
            ((LastedResumeHolder) holder).bindView(this.activity,this.feeds.get(position - MORE_VIEW));
        }else {
            ((LastedResumeInfoHolder) holder).bindView(this.activity,this.title,this.sub_title);
        }
    }

    @Override
    public int getItemCount() {
        return this.feedResponse.getTo() + MORE_VIEW;
    }
}

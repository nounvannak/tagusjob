package com.tagusnow.tagmejob.view.CV.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.CV.RelatedCVFace;
import com.tagusnow.tagmejob.view.CV.RelatedCVHolder;
import java.util.ArrayList;

public class RelatedCVAdapter extends RecyclerView.Adapter {

    private static final String TAG = RelatedCVAdapter.class.getSimpleName();
    private Context context;
    private Activity activity;
    private ArrayList<Feed> feeds;

    public RelatedCVAdapter(Activity activity,ArrayList<Feed> feeds){
        this.activity = activity;
        this.context = activity;
        this.feeds = feeds;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RelatedCVHolder(new RelatedCVFace(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((RelatedCVHolder) holder).bindView(this.activity,this.feeds.get(position));
    }

    @Override
    public int getItemCount() {
        return this.feeds.size();
    }
}

package com.tagusnow.tagmejob.view.CV;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.CVService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.adapter.EducationList;
import com.tagusnow.tagmejob.adapter.ExperienceList;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.model.v2.feed.Education;
import com.tagusnow.tagmejob.model.v2.feed.Experience;
import com.tagusnow.tagmejob.view.CV.Adapter.RelatedCVAdapter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CardCVDetail extends RelativeLayout {

    private static final String TAG = CardCVDetail.class.getSimpleName();
    private Activity activity;
    private Context context;
    private RelativeLayout cv_info,cv_description,cv_education,cv_experience,cv_relate;
    private ImageView user_image;
    private TextView description,name,title,tel,email,location;
    private ListView education,experience;
    private RecyclerView relate;
    private Feed feed;
    private SmUser user;
    private ArrayList<Feed> relateCVs;
    private ArrayList<Education> educations;
    private ArrayList<Experience> experiences;
    private RelatedCVAdapter adapter;
    /*Service*/
    private CVService service = ServiceGenerator.createService(CVService.class);
    private Callback<ArrayList<Education>> mEducation = new Callback<ArrayList<Education>>() {
        @Override
        public void onResponse(Call<ArrayList<Education>> call, Response<ArrayList<Education>> response) {
            if (response.isSuccessful()){
                educations = response.body();
                if (educations!=null){
                    if (educations.size() > 0){
                        cv_education.setVisibility(VISIBLE);
                        education.setAdapter(new EducationList(context,R.layout.layout_education_face,educations));
                    }else {
                        cv_education.setVisibility(GONE);
                    }
                }else {
                    cv_education.setVisibility(GONE);
                }
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<ArrayList<Education>> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<ArrayList<Experience>> mExperience = new Callback<ArrayList<Experience>>() {
        @Override
        public void onResponse(Call<ArrayList<Experience>> call, Response<ArrayList<Experience>> response) {
            if (response.isSuccessful()){
                experiences = response.body();
                if (experiences!=null){
                    if (experiences.size() > 0){
                        cv_experience.setVisibility(VISIBLE);
                        experience.setAdapter(new ExperienceList(context,R.layout.layout_experience_face,experiences));
                    }else {
                        cv_experience.setVisibility(GONE);
                    }
                }else {
                    cv_experience.setVisibility(GONE);
                }
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<ArrayList<Experience>> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<ArrayList<Feed>> mRelateCV = new Callback<ArrayList<Feed>>() {
        @Override
        public void onResponse(Call<ArrayList<Feed>> call, Response<ArrayList<Feed>> response) {
            if (response.isSuccessful()){
                relateCVs = response.body();
                if (relateCVs.size() > 0){
                    cv_relate.setVisibility(VISIBLE);
                    adapter = new RelatedCVAdapter(activity,relateCVs);
                    relate.setAdapter(adapter);
                }else {
                    cv_relate.setVisibility(GONE);
                }
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<ArrayList<Feed>> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void GetEducation(Feed feed){
        service.GetEducation(feed.getUser_post().getId()).enqueue(mEducation);
    }

    private void GetExperience(Feed feed){
        service.GetExperience(feed.getUser_post().getId()).enqueue(mExperience);
    }

    private void GetRelateCV(Feed feed){
        service.GetRelateCV(feed.getUser_post().getId(),feed.getFeed().getCategory_id(),feed.getId()).enqueue(mRelateCV);
    }

    private void initUI(Context context){
        inflate(context, R.layout.layout_card_cv_detail,this);
        this.context = context;
        cv_info = (RelativeLayout)findViewById(R.id.cv_info);
        cv_description = (RelativeLayout)findViewById(R.id.cv_description);
        cv_education = (RelativeLayout)findViewById(R.id.cv_education);
        cv_experience = (RelativeLayout)findViewById(R.id.cv_experience);
        cv_relate = (RelativeLayout)findViewById(R.id.cv_relate);
        user_image = (ImageView)findViewById(R.id.user_image);
        description = (TextView)findViewById(R.id.description);
        name = (TextView)findViewById(R.id.name);
        title = (TextView)findViewById(R.id.title);
        tel = (TextView)findViewById(R.id.tel);
        email = (TextView)findViewById(R.id.email);
        location = (TextView)findViewById(R.id.location);

        education = (ListView)findViewById(R.id.education);
        experience = (ListView)findViewById(R.id.experience);

        relate = (RecyclerView)findViewById(R.id.relate);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        relate.setHasFixedSize(true);
        relate.setLayoutManager(manager);
    }

    public CardCVDetail(Context context) {
        super(context);
        this.initUI(context);
    }

    public CardCVDetail(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public CardCVDetail(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public CardCVDetail(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Feed getFeed() {
        return feed;
    }

    private void initFeed(Feed feed){
        if (feed!=null){
            this.GetEducation(feed);
            this.GetExperience(feed);
            this.GetRelateCV(feed);
            if (feed.getUser_post().getImage()!=null){
                if (feed.getUser_post().getImage().contains("http")){
                    Glide.with(this.context)
                            .load(feed.getUser_post().getImage())
                            .centerCrop()
                            .error(R.drawable.ic_user_male_icon)
                            .into(user_image);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage(),"300"))
                            .centerCrop()
                            .error(R.drawable.ic_user_male_icon)
                            .into(user_image);
                }
            }

            name.setText(feed.getUser_post().getUser_name());
            title.setText(feed.getFeed().getTitle());
            tel.setText(feed.getFeed().getPhone());
            email.setText(feed.getFeed().getEmail());
            location.setText(feed.getFeed().getCity_name());

            if (feed.getFeed().getOriginalHastTagText()!=null && !feed.getFeed().getOriginalHastTagText().equals("")){
                cv_description.setVisibility(VISIBLE);
                description.setText(feed.getFeed().getOriginalHastTagText());
            }else {
                cv_description.setVisibility(GONE);
            }

        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initFeed(feed);
    }

    public ArrayList<Feed> getRelateCVs() {
        return relateCVs;
    }

    public void setRelateCVs(ArrayList<Feed> relateCVs) {
        this.relateCVs = relateCVs;
    }

    public ArrayList<Education> getEducations() {
        return educations;
    }

    public void setEducations(ArrayList<Education> educations) {
        this.educations = educations;
    }

    public ArrayList<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(ArrayList<Experience> experiences) {
        this.experiences = experiences;
    }

    public SmUser getUser() {
        return user;
    }

    public void setUser(SmUser user) {
        this.user = user;
    }
}

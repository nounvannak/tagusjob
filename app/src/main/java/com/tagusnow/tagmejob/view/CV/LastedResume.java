package com.tagusnow.tagmejob.view.CV;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewCardCVActivity;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;

public class LastedResume extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ShapedImageView profile;
    private TextView user_name,skill,location;
    private Feed feed;

    private void initUI(Context context){
        inflate(context, R.layout.layout_lasted_resume,this);
        this.context = context;
        profile = (ShapedImageView)findViewById(R.id.profile);
        user_name = (TextView)findViewById(R.id.user_name);
        skill = (TextView)findViewById(R.id.skill);
        location = (TextView)findViewById(R.id.location);
        this.setOnClickListener(this);
    }

    public LastedResume(Context context) {
        super(context);
        this.initUI(context);
    }

    public LastedResume(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public LastedResume(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public LastedResume(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.Setup(feed);
    }

    private void Setup(Feed feed){
        if (feed!=null){
            if (feed.getUser_post().getImage()!=null){
                if (feed.getUser_post().getImage().contains("http")){
                    Glide.with(this.context)
                            .load(feed.getUser_post().getImage())
                            .centerCrop()
                            .error(R.drawable.employee)
                            .into(profile);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage()))
                            .centerCrop()
                            .error(R.drawable.employee)
                            .into(profile);
                }
            }else {
                Glide.with(this.context)
                        .load(R.drawable.employee)
                        .centerCrop()
                        .into(profile);
            }

            user_name.setText(feed.getUser_post().getUser_name());
            skill.setText(feed.getFeed().getTitle());
            location.setText(feed.getFeed().getCity_name());
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            this.activity.startActivity(ViewCardCVActivity.createIntent(this.activity).putExtra(ViewCardCVActivity.FEED,this.feed).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}

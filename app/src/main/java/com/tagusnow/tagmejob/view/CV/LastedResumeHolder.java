package com.tagusnow.tagmejob.view.CV;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tagusnow.tagmejob.feed.Feed;

public class LastedResumeHolder extends RecyclerView.ViewHolder {

    private LastedResume resume;

    public LastedResumeHolder(View itemView) {
        super(itemView);
        this.resume = (LastedResume)itemView;
    }

    public void bindView(Activity activity, Feed feed){
        this.resume.setActivity(activity);
        this.resume.setFeed(feed);
    }
}

package com.tagusnow.tagmejob.view.CV;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class LastedResumeInfo extends TagUsJobRelativeLayout {

    private Context context;
    private TextView title,sub_title;

    private void initUI(Context context){
        inflate(context, R.layout.widget_lasted_resume_info,this);
        this.context = context;
        title = (TextView)findViewById(R.id.title);
        sub_title = (TextView)findViewById(R.id.sub_title);
    }

    public LastedResumeInfo(Context context) {
        super(context);
        this.initUI(context);
    }

    public LastedResumeInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public LastedResumeInfo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public LastedResumeInfo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    public void setSubTitle(String sub_title) {
        this.sub_title.setText(sub_title);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }
}

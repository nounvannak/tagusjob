package com.tagusnow.tagmejob.view.CV;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class LastedResumeInfoHolder extends RecyclerView.ViewHolder {

    private LastedResumeInfo lastedResumeInfo;

    public LastedResumeInfoHolder(View itemView) {
        super(itemView);
        this.lastedResumeInfo = (LastedResumeInfo)itemView;
    }

    public void bindView(Activity activity,String title,String sub_title){
        this.lastedResumeInfo.setActivity(activity);
        this.lastedResumeInfo.setTitle(title);
        this.lastedResumeInfo.setSubTitle(sub_title);
    }
}

package com.tagusnow.tagmejob.view.CV;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewCardCVActivity;
import com.tagusnow.tagmejob.feed.Feed;
import java.io.Serializable;

public class RelatedCVFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = RelatedCVFace.class.getSimpleName();
    private Context context;
    private Activity activity;
    private ImageView user_image;
    private RelativeLayout like_and_comment_information;
    private View like_and_comment_border;
    private TextView name,title,tel,email,location,like_result,comment_result;
    private Feed feed;

    private void initUI(Context context){
        inflate(context, R.layout.layout_related_cv_face,this);
        this.context = context;
        user_image = (ImageView)findViewById(R.id.user_image);
        like_and_comment_information = (RelativeLayout)findViewById(R.id.like_and_comment_information);
        like_and_comment_border = (View)findViewById(R.id.like_and_comment_border);
        name = (TextView)findViewById(R.id.name);
        title = (TextView)findViewById(R.id.title);
        tel = (TextView)findViewById(R.id.tel);
        email = (TextView)findViewById(R.id.email);
        location = (TextView)findViewById(R.id.location);
        like_result = (TextView)findViewById(R.id.like_result);
        comment_result = (TextView)findViewById(R.id.comment_result);
        this.setOnClickListener(this);
    }

    public RelatedCVFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public RelatedCVFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public RelatedCVFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public RelatedCVFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initFeed(feed);
    }

    private void initFeed(Feed feed){
        if (feed!=null){
            /*Profile Information*/
            if (feed.getUser_post()!=null){
                if (feed.getUser_post().getImage()!=null){
                    if (feed.getUser_post().getImage().contains("http")){
                        Glide.with(this.context)
                                .load(feed.getUser_post().getImage())
                                .centerCrop()
                                .error(R.drawable.employee)
                                .into(user_image);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage()))
                                .centerCrop()
                                .error(R.drawable.employee)
                                .into(user_image);
                    }
                }else {
                    Glide.with(this.context).load(R.drawable.employee).into(user_image);
                }

            }else {
                Toast.makeText(this.context,"No profile data",Toast.LENGTH_SHORT).show();
            }

            /*Body*/
            name.setText(feed.getUser_post().getUser_name());
            title.setText(feed.getFeed().getTitle());
            tel.setText(feed.getUser_post().getPhone());
            email.setText(feed.getUser_post().getEmail());
            location.setText(feed.getFeed().getCity_name());

            if (feed.getFeed().getCount_comments() > 0 && feed.getFeed().getCount_likes() > 0){
                like_and_comment_information.setVisibility(VISIBLE);
                like_and_comment_border.setVisibility(VISIBLE);

                if (feed.getFeed().getCount_likes() > 0){
                    like_result.setVisibility(VISIBLE);
                    like_result.setText(this.activity.getString(R.string._d_like,(feed.getFeed().getCount_likes())));
                }else {
                    like_result.setVisibility(GONE);
                }

                if (feed.getFeed().getCount_comments() > 0){
                    comment_result.setVisibility(VISIBLE);
                    comment_result.setText(this.activity.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                }else {
                    comment_result.setVisibility(GONE);
                }

            }else {
                like_and_comment_information.setVisibility(GONE);
                like_and_comment_border.setVisibility(GONE);
            }
        }else {
            Log.e(TAG,"No Feed");
        }
    }

    private void viewPostDetail(){
        if (this.activity!=null){
            this.activity.startActivity(ViewCardCVActivity.createIntent(this.activity)
                    .putExtra(ViewCardCVActivity.FEED, (Serializable) this.feed));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            this.viewPostDetail();
        }
    }
}

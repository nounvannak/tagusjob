package com.tagusnow.tagmejob.view.CV;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;

public class RelatedCVHolder extends RecyclerView.ViewHolder {

    private RelatedCVFace face;

    public RelatedCVHolder(View itemView) {
        super(itemView);
        this.face = (RelatedCVFace)itemView;
    }

    public void bindView(Activity activity, Feed feed){
        this.face.setActivity(activity);
        this.face.setFeed(feed);
    }
}

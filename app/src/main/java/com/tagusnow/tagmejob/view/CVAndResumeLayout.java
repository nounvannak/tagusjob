package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.adapter.UIListCVAndResumeAdapter;
import com.tagusnow.tagmejob.feed.CV;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;

import java.util.ArrayList;

public class CVAndResumeLayout extends RelativeLayout {

    private static final String TAG = CVAndResumeLayout.class.getSimpleName();
    private Activity activity;
    private Context context;
    private RecyclerView recyclerView;
    private UIListCVAndResumeAdapter adapter;
    private Feed feed;

    private void initUI(Context context){
        inflate(context, R.layout.layout_cv_resume_download_list,this);
        this.context = context;
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public CVAndResumeLayout(Context context) {
        super(context);
        this.initUI(context);
    }

    public CVAndResumeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public CVAndResumeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public CVAndResumeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Feed getFeed() {
        return feed;
    }

    private void initFeed(Feed feed){
        if (feed!=null){
            if (feed.getFeed().getFeed_type().equals(FeedDetail.STAFF_POST)){
                if (feed.getFeed().getStaff_file().size() > 0){
                    this.adapter = new UIListCVAndResumeAdapter(this.activity,feed.getFeed().getStaff_file());
                    this.recyclerView.setAdapter(this.adapter);
                }
            }else {
                Toast.makeText(this.context,feed.getFeed().getFeed_type(),Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initFeed(feed);
    }
}

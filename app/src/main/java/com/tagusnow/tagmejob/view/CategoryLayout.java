package com.tagusnow.tagmejob.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;

public class CategoryLayout extends TagUsJobRelativeLayout {

    ImageView imageView;
    TextView textView,jobCounter;
    Context context;

    String image;
    String title;
    int counter;

    public CategoryLayout(Context context) {
        super(context);
        inflate(context, R.layout.layout_category,this);
        imageView = (ImageView)findViewById(R.id.ivCategory);
        textView = (TextView)findViewById(R.id.txtCategory);
        jobCounter = (TextView)findViewById(R.id.jobCounter);
        this.context = context;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        textView.setText(this.title);
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
        jobCounter.setText(String.valueOf(this.counter));
        if (this.counter > 0){
            jobCounter.setVisibility(VISIBLE);
        }else{
            jobCounter.setVisibility(GONE);
        }
    }

    public void setImage(String image) {
        this.image = image;
        if (this.image!=null){
            if (this.image.contains("http")){
                Glide.with(context)
                        .load(this.image)
                        .error(R.drawable.ic_action_bookmark_border)
                        .fitCenter()
                        .into(this.imageView);
            }else{
                Glide.with(context)
                        .load(SuperUtil.getCategoryPath(this.image))
                        .error(R.drawable.ic_action_bookmark_border)
                        .fitCenter()
                        .into(this.imageView);
            }
        }else{
            Glide.with(context)
                    .load(R.drawable.ic_action_bookmark_border)
                    .fitCenter()
                    .into(this.imageView);
        }
    }

    public String getImage() {
        return image;
    }
}

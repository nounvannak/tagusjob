package com.tagusnow.tagmejob.view.Chat.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tagusnow.tagmejob.view.Chat.UI.ChatFile;

import java.io.File;

public class ChatFileHolder extends RecyclerView.ViewHolder {

    private ChatFile chatFile;

    public ChatFileHolder(View itemView) {
        super(itemView);
        this.chatFile = (ChatFile)itemView;
    }

    public void bindView(Activity activity, File res, RecyclerView.Adapter adapter){
        this.chatFile.setActivity(activity);
        this.chatFile.setAdapter(adapter);
        this.chatFile.setPosition(getAdapterPosition());
        this.chatFile.setResource(res);
    }

    public void bindView(Activity activity,int res,RecyclerView.Adapter adapter){
        this.chatFile.setActivity(activity);
        this.chatFile.setAdapter(adapter);
        this.chatFile.setPosition(getAdapterPosition());
        this.chatFile.setResource(res);
    }
}

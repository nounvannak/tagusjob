package com.tagusnow.tagmejob.view.Chat.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.view.Chat.UI.ChatUserUI;

public class ChatUserUIHolder extends RecyclerView.ViewHolder {

    private ChatUserUI chatUserUI;

    public ChatUserUIHolder(View itemView) {
        super(itemView);
        this.chatUserUI = (ChatUserUI)itemView;
    }

    public void bindView(Activity activity, SmUser auth, SmUser user,RecyclerView.Adapter adapter){
        this.chatUserUI.setActivity(activity);
        this.chatUserUI.setAuth(auth);
        this.chatUserUI.setUser(user);
        this.chatUserUI.setConversation(user.getConversation());
        this.chatUserUI.setPosition(getAdapterPosition());
        this.chatUserUI.setAdapter(adapter);
    }
}

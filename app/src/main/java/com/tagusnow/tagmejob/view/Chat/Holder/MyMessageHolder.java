package com.tagusnow.tagmejob.view.Chat.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.view.Chat.UI.MyMessage;

public class MyMessageHolder extends RecyclerView.ViewHolder {

    private MyMessage myMessage;

    public MyMessageHolder(View itemView) {
        super(itemView);
        this.myMessage = (MyMessage)itemView;
    }

    public void bindView(Activity activity, SmUser user, Conversation conversation){
        this.myMessage.setActivity(activity);
        this.myMessage.setUser(user);
        this.myMessage.setConversation(conversation);
    }
}

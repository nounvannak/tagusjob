package com.tagusnow.tagmejob.view.Chat.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.view.Chat.UI.TheirMessage;

public class TheirMessageHolder extends RecyclerView.ViewHolder {

    private TheirMessage theirMessage;

    public TheirMessageHolder(View itemView) {
        super(itemView);
        this.theirMessage = (TheirMessage)itemView;
    }

    public void bindView(Activity activity, SmUser user, Conversation conversation){
        theirMessage.setActivity(activity);
        theirMessage.setUser(user);
        theirMessage.setConversation(conversation);
    }
}

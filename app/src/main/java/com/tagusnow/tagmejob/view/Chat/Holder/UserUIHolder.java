package com.tagusnow.tagmejob.view.Chat.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Chat.UI.UserUI;

public class UserUIHolder extends RecyclerView.ViewHolder {

    private UserUI userUI;

    public UserUIHolder(View itemView) {
        super(itemView);
        this.userUI = (UserUI)itemView;
    }

    public void bindView(Activity activity, SmUser user,RecyclerView.Adapter adapter){
        this.userUI.setActivity(activity);
        this.userUI.setUser(user);
        this.userUI.setAdapter(adapter);
        this.userUI.setPosition(getAdapterPosition());
    }
}

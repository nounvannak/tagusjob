package com.tagusnow.tagmejob.view.Chat.UI;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import java.io.File;

public class ChatFile extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ImageView image;
    private ImageButton remove;
    private RecyclerView.Adapter adapter;
    private int position;

    private void InitUI(Context context){
        inflate(context,R.layout.chat_file,this);
        this.context = context;

        image = (ImageView)findViewById(R.id.image);
        remove = (ImageButton)findViewById(R.id.remove);

        remove.setOnClickListener(this);
    }

    public ChatFile(Context context) {
        super(context);
        this.InitUI(context);
    }

    public ChatFile(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public ChatFile(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public ChatFile(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setResource(File res) {
        Glide.with(this.context)
                .load(res)
                .fitCenter()
                .error(R.drawable.ic_error)
                .into(image);
    }

    public void setResource(int res){
        Glide.with(this.context)
                .load(res)
                .fitCenter()
                .error(R.drawable.ic_error)
                .into(image);
    }

    @Override
    public void onClick(View view) {
        if (view==remove){
            this.adapter.notifyItemRemoved(this.position);
        }
    }
}

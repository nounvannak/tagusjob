package com.tagusnow.tagmejob.view.Chat.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Message.MessageBoxV2;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;
import com.tagusnow.tagmejob.V2.Message.Model.Message;
import com.tagusnow.tagmejob.V2.Message.Model.User;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;
import cn.nekocode.badge.BadgeDrawable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatUserUI extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private AvatarView profile;
    private AvatarPlaceholder refreshableAvatarPlaceholder;
    private IImageLoader imageLoader;
    private TextView group_name,user_name,story,time,counter;
    private int position;
    private SmUser user,auth;
    private Conversation conversation;
    private RecyclerView.Adapter adapter;
    private Socket socket;
    private UserService service = ServiceGenerator.createService(UserService.class);

    private void initSocket() {
        socket = new App().getSocket();
        socket.on("Typing Message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject obj = (JSONObject) args[0];
                ((TagUsJobActivity)activity).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("Typing",new Gson().toJson(obj));
                            String typing = obj.getString("user_name") + " is typing...";
                            if (obj.getInt("conversation_id")==conversation.getId() && obj.getInt("user_id")==user.getId()){
                                story.setVisibility(VISIBLE);
                                story.setText(typing);
                            }else {
                                if (obj.getInt("user_id")==user.getId()){
                                    story.setVisibility(VISIBLE);
                                    story.setText(typing);
                                }else {
                                    UpdateUI(conversation);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.on("Stop Typing Message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject obj = (JSONObject) args[0];
                ((TagUsJobActivity)activity).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("Stop Typing",new Gson().toJson(obj));
                            if (obj.getInt("conversation_id")==conversation.getId() && obj.getInt("user_id")==user.getId()){
                                UpdateUI(conversation);
                            }else {
                                if (obj.getInt("user_id")==user.getId()){
                                    UpdateUI(conversation);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        socket.connect();
    }

    private void UpdateUI(Conversation conversation){
        if (conversation.getMessage()!=null){
            if (conversation.getMessage_file()!=null){
                if (conversation.getMessage_file().size() > 0){
                    if (conversation.getMessage_file().get(0).getFile_type()==Conversation.IMAGE){
                        if (conversation.getMessage_file().size()==1){
                            if (auth.getId()==conversation.getUser_id()){
                                story.setText("You : send a photo");
                            }else {
                                story.setText(user.getUser_name()+" : send a photo");
                            }
                        }else {
                            if (auth.getId()==conversation.getUser_id()){
                                story.setText("You : send "+(conversation.getMessage_file().size())+" photos");
                            }else {
                                story.setText(user.getUser_name()+" : send "+(conversation.getMessage_file().size())+" photos");
                            }
                        }
                    }else {
                        if (auth.getId()==conversation.getUser_id()){
                            story.setText("You : send a file");
                        }else {
                            story.setText(user.getUser_name()+" : send a file");
                        }
                    }
                }
            }else {
                if (auth.getId()==conversation.getUser_id()){
                    story.setText("You : "+conversation.getMessage());
                }else {
                    story.setText(user.getUser_name()+" : "+conversation.getMessage());
                }
            }
        }else {
            story.setText(conversation.getStory());
        }
    }

    private void ChatRoom(){
        service.ChatRoom(auth.getId(),user.getId()).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if (response.isSuccessful()){
                    Log.e("Chat Room",new Gson().toJson(response.body()));
                    JSONObject obj = response.body();
                    if (obj!=null){
                        try {
                            conversation.setConversation_id(obj.getInt("id"));
                            ShowChatRoom();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    try {
                        ((TagUsJobActivity) activity).ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                ((TagUsJobActivity) activity).ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private void InitUI(Context context) {
        inflate(context,R.layout.chat_user_iu,this);
        this.context = context;

        profile = (AvatarView)findViewById(R.id.profile);
        group_name = (TextView)findViewById(R.id.group_name);
        user_name = (TextView)findViewById(R.id.user_name);
        story = (TextView)findViewById(R.id.story);
        time = (TextView)findViewById(R.id.time);
        counter = (TextView)findViewById(R.id.counter);

        group_name.setVisibility(GONE);
        this.setOnClickListener(this);

    }

    public ChatUserUI(Context context) {
        super(context);
        this.InitUI(context);
    }

    public ChatUserUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public ChatUserUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public ChatUserUI(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    public void setUser(SmUser user) {
        this.user = user;
        this.setConversation(user.getConversation());
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
        this.initChat(conversation);
        this.initSocket();
    }

    private void initChat(Conversation conversation){

        if (this.user!=null){
            refreshableAvatarPlaceholder = new AvatarPlaceholder(this.user.getUser_name(), 50);
            imageLoader = new GlideLoader();
            if (this.user.getImage()!=null){
                if (this.user.getImage().contains("http")){
                    imageLoader.loadImage(profile, refreshableAvatarPlaceholder,this.user.getImage());
                }else {
                    imageLoader.loadImage(profile, refreshableAvatarPlaceholder,SuperUtil.getProfilePicture(this.user.getImage(),"400"));
                }
            }else {
                imageLoader.loadImage(profile, refreshableAvatarPlaceholder,null);
            }

            this.user_name.setText(this.user.getUser_name());
        }

        if (conversation!=null && conversation.getId() > 0){
            time.setVisibility(VISIBLE);
            counter.setVisibility(VISIBLE);
            if (this.conversation.getCounter() > 0){
                final BadgeDrawable drawable;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    drawable = new BadgeDrawable.Builder()
                            .type(BadgeDrawable.TYPE_NUMBER)
                            .badgeColor(context.getColor(R.color.colorAccent))
                            .number(this.conversation.getCounter())
                            .build();
                }else {
                    drawable = new BadgeDrawable.Builder()
                            .type(BadgeDrawable.TYPE_NUMBER)
                            .number(this.conversation.getCounter())
                            .build();
                }
                SpannableString spannableString = new SpannableString(drawable.toSpannable());
                counter.setText(spannableString);
            }else {
                counter.setVisibility(GONE);
            }

            if (this.conversation.getTime()!=null){
                time.setText(this.conversation.getTimeStory());
            }else {
                time.setText("Never");
            }

            if (this.conversation.getMessage()!=null){
                if (this.conversation.getMessage_file()!=null){
                    if (this.conversation.getMessage_file().size() > 0){
                        if (this.conversation.getMessage_file().get(0).getFile_type()==Conversation.IMAGE){
                            if (this.conversation.getMessage_file().size()==1){
                                if (this.auth.getId()==this.conversation.getUser_id()){
                                    story.setText("You : send a photo");
                                }else {
                                    story.setText(this.user.getUser_name()+" : send a photo");
                                }
                            }else {
                                if (this.auth.getId()==this.conversation.getUser_id()){
                                    story.setText("You : send "+(this.conversation.getMessage_file().size())+" photos");
                                }else {
                                    story.setText(this.user.getUser_name()+" : send "+(this.conversation.getMessage_file().size())+" photos");
                                }
                            }
                        }else {
                            if (this.auth.getId()==this.conversation.getUser_id()){
                                story.setText("You : send a file");
                            }else {
                                story.setText(this.user.getUser_name()+" : send a file");
                            }
                        }
                    }
                }else {
                    if (this.auth.getId()==this.conversation.getUser_id()){
                        story.setText("You : "+this.conversation.getMessage());
                    }else {
                        story.setText(this.user.getUser_name()+" : "+this.conversation.getMessage());
                    }
                }
            }else {
                story.setText(this.conversation.getStory());
            }

        }else {
            this.story.setText("Start first message with "+this.user.getUser_name());
            time.setVisibility(GONE);
            counter.setVisibility(GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v==this){
            this.ShowChatRoom();
//            if (this.conversation.getId()==0){
//                this.ChatRoom();
//            }else {
//                this.ShowChatRoom();
//            }
        }
    }

    private void ShowChatRoom(){
//        this.activity.startActivity(ChatRoomActivity
//                .createIntent(this.activity)
//                .putExtra(ChatRoomActivity.USER_TWO,this.user)
//                .putExtra(ChatRoomActivity.CHAT_ID,this.conversation.getConversation_id())
//                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

        String image = "";
        String text = this.user.getConversation().getMessage()!=null && !this.user.getConversation().getMessage().equals("") ? this.user.getConversation().getMessage() : null;
        Date date = this.user.getConversation().getTime()!=null ? this.user.getConversation().getDateCreated() : null;
        if (this.user.getImage()!=null && !this.user.getImage().equals("")){
            if (this.user.getImage().contains("http")){
                image = this.user.getImage();
            }else {
                image = SuperUtil.getProfilePicture(this.user.getImage(),"200");
            }
        }
        ArrayList<User> users = new ArrayList<>();
        User user1 = new User(this.user.getProfile_id(),this.user.getUser_name(),image,true,this.user);
        Message message = new Message(String.valueOf(this.user.getConversation().getConversation_id()),user1,text,date);
        users.add(user1);
        Dialog dialog = new Dialog(this.user.getProfile_id(),this.user.getUser_name(),image,users,message,this.user.getConversation().getCounter());
        this.activity.startActivity(MessageBoxV2
                .createIntent(this.activity)
                .putExtra(MessageBoxV2.USER_TWO,new Gson().toJson(dialog))
                .putExtra(MessageBoxV2.CHAT_ID,this.user.getConversation().getConversation_id())
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void setAuth(SmUser auth) {
        this.auth = auth;
    }
}

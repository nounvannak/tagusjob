package com.tagusnow.tagmejob.view.Chat.UI;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourFour;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourOne;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourThree;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourTwo;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeFour;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeOne;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeThree;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeTwo;
import com.tagusnow.tagmejob.view.Grid.Two.GridTwoOne;
import com.tagusnow.tagmejob.view.Grid.Two.GridTwoTwo;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import java.util.Random;

public class MyMessage extends TagUsJobRelativeLayout {

    private Context context;
    private TextView message;
    private RelativeLayout main_view;
    private Conversation conversation;
    private SmUser user;
    private GridTwoOne gridTwoOne;
    private GridTwoTwo gridTwoTwo;
    private GridThreeOne gridThreeOne;
    private GridThreeTwo gridThreeTwo;
    private GridThreeThree gridThreeThree;
    private GridThreeFour gridThreeFour;
    private GridFourTwo gridFourTwo;
    private GridFourThree gridFourThree;
    private GridFourFour gridFourFour;
    private GridFourOne gridFourOne;

    private void InitUI(Context context){
        inflate(context, R.layout.my_message,this);
        this.context = context;
        message = (TextView)findViewById(R.id.message_body);
        main_view = (RelativeLayout)findViewById(R.id.main_view);

        gridTwoOne = new GridTwoOne(this.context);
        gridTwoTwo = new GridTwoTwo(this.context);
        gridThreeOne = new GridThreeOne(this.context);
        gridThreeTwo = new GridThreeTwo(this.context);
        gridThreeThree = new GridThreeThree(this.context);
        gridThreeFour = new GridThreeFour(this.context);
        gridFourTwo = new GridFourTwo(this.context);
        gridFourThree = new GridFourThree(this.context);
        gridFourFour = new GridFourFour(this.context);
        gridFourOne = new GridFourOne(this.context);
    }

    public MyMessage(Context context) {
        super(context);
        this.InitUI(context);
    }

    public MyMessage(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public MyMessage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public MyMessage(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
        this.InitMessage(conversation);
    }

    public void setUser(SmUser user) {
        this.user = user;
    }

    private void InitMessage(Conversation conversation){
        if (conversation!=null){
            if (conversation.getMessage()!=null){
                message.setVisibility(VISIBLE);
                message.setText(conversation.getMessage());
            }else {
                message.setVisibility(GONE);
            }

            if (conversation.getMessage_file()!=null){

                gridTwoOne.setActivity(this.activity);
                gridTwoTwo.setActivity(this.activity);
                gridThreeOne.setActivity(this.activity);
                gridThreeTwo.setActivity(this.activity);
                gridThreeThree.setActivity(this.activity);
                gridThreeFour.setActivity(this.activity);
                gridFourTwo.setActivity(this.activity);
                gridFourThree.setActivity(this.activity);
                gridFourFour.setActivity(this.activity);
                gridFourOne.setActivity(this.activity);

                main_view.setVisibility(VISIBLE);
                String Grid[][] = {{},{},
                        {GridTwoOne.class.getSimpleName(),GridTwoTwo.class.getSimpleName()},
                        {GridThreeOne.class.getSimpleName(),GridThreeTwo.class.getSimpleName(),GridThreeThree.class.getSimpleName(),GridThreeFour.class.getSimpleName()},
                        {GridFourOne.class.getSimpleName(),GridFourTwo.class.getSimpleName(),GridThreeThree.class.getSimpleName(),GridFourFour.class.getSimpleName()}};
                if (conversation.getMessage_file().size() >= 4){
                    int size = new Random().nextInt(4);
                    main_view.removeAllViews();
                    if (Grid[4][size].equals(GridFourOne.class.getSimpleName())){
                        main_view.addView(gridFourOne.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                    }else if (Grid[4][size].equals(GridFourTwo.class.getSimpleName())){
                        main_view.addView(gridFourTwo.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                    }else if (Grid[4][size].equals(GridFourThree.class.getSimpleName())){
                        main_view.addView(gridFourThree.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                    }else {
                        if (conversation.getMessage_file().size() >= 5){
                            main_view.addView(gridFourFour.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                        }else{
                            main_view.addView(gridFourThree.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                        }
                    }
                }else {
                    switch (conversation.getMessage_file().size()){
                        case 1:
                            main_view.removeAllViews();
                            if (conversation.getMessage_file().get(0).getFile_type()==Conversation.IMAGE){
                                ImageView imageView = new ImageView(this.context);
                                Glide.with(this.context).load(SuperUtil.getMessageResource(conversation.getMessage_file().get(0).getNew_name(),
                                        conversation.getMessage_file().get(0).getFile_type(),"500")).fitCenter().error(R.drawable.ic_error).into(imageView);
                                main_view.addView(imageView,200,LayoutParams.WRAP_CONTENT);
                            }else {
                                main_view.addView(this.CheckExtension(SuperUtil.getMessageResource(conversation.getMessage_file().get(0).getNew_name(),
                                        conversation.getMessage_file().get(0).getFile_type())),
                                        200,
                                        LayoutParams.WRAP_CONTENT);
                            }
                            break;
                        case 2:
                            int size =  new Random().nextInt(2);
                            main_view.removeAllViews();
                            if (Grid[conversation.getMessage_file().size()][size].equals(GridTwoOne.class.getSimpleName()))
                                main_view.addView(gridTwoOne.setMessageFile(conversation.getMessage_file()),400,LayoutParams.WRAP_CONTENT);
                            else
                                main_view.addView(gridTwoTwo.setMessageFile(conversation.getMessage_file()),400,LayoutParams.WRAP_CONTENT);
                            break;
                        case 3:
                            int sizes = new Random().nextInt(4);
                            main_view.removeAllViews();
                            if (Grid[conversation.getMessage_file().size()][sizes].equals(GridThreeOne.class.getSimpleName())){
                                main_view.addView(gridThreeOne.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                            }else if (Grid[conversation.getMessage_file().size()][sizes].equals(GridThreeTwo.class.getSimpleName())) {
                                main_view.addView(gridThreeTwo.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                            }else if (Grid[conversation.getMessage_file().size()][sizes].equals(GridThreeThree.class.getSimpleName())){
                                main_view.addView(gridThreeThree.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                            }else {
                                main_view.addView(gridThreeFour.setMessageFile(conversation.getMessage_file()),500,LayoutParams.WRAP_CONTENT);
                            }
                            break;
                    }
                }
            }else {
                main_view.setVisibility(GONE);
            }
        }
    }
}

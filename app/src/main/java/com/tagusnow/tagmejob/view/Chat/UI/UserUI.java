package com.tagusnow.tagmejob.view.Chat.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Message.MessageBoxV2;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;
import com.tagusnow.tagmejob.V2.Message.Model.Message;
import com.tagusnow.tagmejob.V2.Message.Model.User;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.util.ArrayList;
import java.util.Date;

import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;

public class UserUI extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private AvatarView profile;
    private AvatarPlaceholder refreshableAvatarPlaceholder;
    private IImageLoader imageLoader;
    private TextView user_name;
    private SmUser user;
    private RecyclerView.Adapter adapter;
    private int position;

    private void InitUI(Context context){
        inflate(context, R.layout.user_ui,this);
        this.context = context;

        profile = (AvatarView)findViewById(R.id.profile);
        user_name = (TextView)findViewById(R.id.user_name);

        this.setOnClickListener(this);
    }

    public UserUI(Context context) {
        super(context);
        this.InitUI(context);
    }

    public UserUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public UserUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public UserUI(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    public void setUser(SmUser user) {
        this.user = user;
        this.InitUser(user);
    }

    private void InitUser(SmUser user){
        if (user!=null){
            refreshableAvatarPlaceholder = new AvatarPlaceholder(this.user.getUser_name(), 50);
            imageLoader = new GlideLoader();
            if (user.getImage()!=null){
                if (user.getImage().contains("http")){
                    imageLoader.loadImage(profile, refreshableAvatarPlaceholder,this.user.getImage());
                }else {
                    imageLoader.loadImage(profile, refreshableAvatarPlaceholder,SuperUtil.getProfilePicture(this.user.getImage(),"400"));
                }
            }else {
                imageLoader.loadImage(profile, refreshableAvatarPlaceholder,null);
            }

            user_name.setText(user.getUser_name());
        }
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            this.ShowChatRoom();
        }
    }
    private void ShowChatRoom(){
//        this.activity.startActivity(ChatRoomActivity
//                .createIntent(this.activity)
//                .putExtra(ChatRoomActivity.USER_TWO,this.user)
//                .putExtra(ChatRoomActivity.CHAT_ID,this.user.getConversation().getConversation_id())
//                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        String image = "";
        String text = this.user.getConversation().getMessage()!=null && !this.user.getConversation().getMessage().equals("") ? this.user.getConversation().getMessage() : null;
        Date date = this.user.getConversation().getTime()!=null ? this.user.getConversation().getDateCreated() : null;
        if (this.user.getImage()!=null && !this.user.getImage().equals("")){
            if (this.user.getImage().contains("http")){
                image = this.user.getImage();
            }else {
                image = SuperUtil.getProfilePicture(this.user.getImage(),"200");
            }
        }
        ArrayList<User> users = new ArrayList<>();
        User user1 = new User(this.user.getProfile_id(),this.user.getUser_name(),image,true,this.user);
        Message message = new Message(String.valueOf(this.user.getConversation().getConversation_id()),user1,text,date);
        users.add(user1);
        Dialog dialog = new Dialog(this.user.getProfile_id(),this.user.getUser_name(),image,users,message,this.user.getConversation().getCounter());
        this.activity.startActivity(MessageBoxV2
                .createIntent(this.activity)
                .putExtra(MessageBoxV2.USER_TWO,new Gson().toJson(dialog))
                .putExtra(MessageBoxV2.CHAT_ID,this.user.getConversation().getConversation_id())
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}

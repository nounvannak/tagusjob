package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.SocketService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.model.v2.feed.Socket.SocketTyping;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CommentBox extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = CommentBox.class.getSimpleName();
    private Context context;
    private RelativeLayout edit_comment_image_layout;
    private ImageView edit_comment_image;
    private ImageButton edit_comment_remove_image,camera,send;
    private EmojiconEditText edit_comment_box;
    private View edit_comment_border;
    private SmUser authUser;
    private Feed feed;
    private Uri mUri;
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit socketRetrofit = new Retrofit.Builder().baseUrl(SuperConstance.SOCKET_IO_).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private CommentService service = ServiceGenerator.createService(CommentService.class);
    private FeedService feedService = ServiceGenerator.createService(FeedService.class);
    private SocketService _SocketService = socketRetrofit.create(SocketService.class);

    private Callback<Comment> mSaveBack = new Callback<Comment>() {
        @Override
        public void onResponse(Call<Comment> call, Response<Comment> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                ((TagUsJobActivity) activity).dismissProgress();
                clearForm();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    ((TagUsJobActivity) activity).dismissProgress();
                    clearForm();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Comment> call, Throwable t) {
            ((TagUsJobActivity) activity).dismissProgress();
            clearForm();
            t.printStackTrace();
        }
    };


    private void initUI(Context context){
        inflate(context,R.layout.comment_box,this);
        this.context = context;

        edit_comment_image_layout = (RelativeLayout)findViewById(R.id.edit_comment_image_layout);
        edit_comment_image = (ImageView)findViewById(R.id.edit_comment_image);
        edit_comment_remove_image = (ImageButton)findViewById(R.id.edit_comment_remove_image);
        camera = (ImageButton)findViewById(R.id.camera);
        send = (ImageButton)findViewById(R.id.send);
        edit_comment_box = (EmojiconEditText)findViewById(R.id.edit_comment_box);
        edit_comment_border = (View)findViewById(R.id.edit_comment_border);

        edit_comment_remove_image.setOnClickListener(this);
        camera.setOnClickListener(this);
        send.setOnClickListener(this);
        edit_comment_box.setUseSystemDefault(true);
        edit_comment_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                conditionCommentBox();
                onReqCommenting();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                conditionCommentBox();
                requestTriggerComment();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                conditionCommentBox();
                onReqCommenting();
            }
        });
        viewCondition();
    }

    private void onReqCommenting(){
//        socketService.checkCommenting(this.feed_id,false).enqueue(mCheckCommentingCallback);
    }

    private void requestTriggerComment(){
        _SocketService.SOCKET_TYPING(feed.getId(),authUser.getUser_name(),authUser.getId()).enqueue(new Callback<SocketTyping>() {
            @Override
            public void onResponse(Call<SocketTyping> call, Response<SocketTyping> response) {
                if (response.isSuccessful()){
                    Log.w(TAG,response.message());
                }else {
                    try {
                        Log.e(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SocketTyping> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void conditionCommentBox(){
        if (!edit_comment_box.getText().toString().equals("")){
            send.setVisibility(View.VISIBLE);
            if (mFileSelected!=null){
                edit_comment_image_layout.setVisibility(View.VISIBLE);
                edit_comment_border.setVisibility(View.VISIBLE);
            }else{
                edit_comment_image_layout.setVisibility(View.GONE);
                edit_comment_border.setVisibility(View.GONE);
            }
            if (edit_comment_box.getText().length() > 30){
                edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.MORE_TEXT);
            }else{
                edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
            }
        }else {
            if (mFileSelected!=null){
                edit_comment_image_layout.setVisibility(View.VISIBLE);
                edit_comment_border.setVisibility(View.VISIBLE);
                send.setVisibility(View.VISIBLE);
            }else {
                edit_comment_image_layout.setVisibility(View.GONE);
                edit_comment_border.setVisibility(View.GONE);
                send.setVisibility(View.GONE);
            }
            edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
        }
    }


    public CommentBox(Context context) {
        super(context);
        this.initUI(context);
    }

    public CommentBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public CommentBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public CommentBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    @Override
    protected Activity getActivity() {
        return super.getActivity();
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void send(){
        ((TagUsJobActivity)activity).showProgress();
        comment();
    }

    private void comment(){
        RequestBody feedId    = createPartFromString(String.valueOf(this.feed.getId()));
        RequestBody auth_user = createPartFromString(String.valueOf(this.authUser!=null ? this.authUser.getId() : 0));
        RequestBody text      = createPartFromString(edit_comment_box.getText().toString());
        if (this.mFileSelected!=null){
            MultipartBody.Part part = prepareFilePart("image",this.mFileSelected);
            service.save(auth_user,feedId,text,part).enqueue(mSaveBack);
        }else {
            service.save(auth_user,feedId,text).enqueue(mSaveBack);
        }
    }

    private void removeImage(){
        mFileSelected = null;
        viewCondition();
    }

    protected void selectImage(){
        Glide.with(this.context)
                .load(mFileSelected)
                .error(R.color.colorBorder)
                .into(edit_comment_image);
        viewCondition();
    }

    private void viewCondition(){
        edit_comment_image_layout.setVisibility(View.GONE);
        edit_comment_border.setVisibility(View.GONE);
        send.setVisibility(View.GONE);
        conditionCommentBox();
    }

    private void OpenImagePicker(){
        AndroidImagePicker.getInstance().pickSingle(activity, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                mFileSelected = new File(items.get(0).path);
                mUri = Uri.parse(items.get(0).path);
                selectImage();
            }
        });
    }

    private void clearForm(){
        mFileSelected = null;
        edit_comment_box.getText().clear();
        viewCondition();
    }

    @Override
    public void onClick(View v) {
        if (v==edit_comment_remove_image){
            this.removeImage();
        }else if (v==camera){
            this.OpenImagePicker();
        }else if (v==send){
            this.send();
        }
    }

    public CommentBox setFeed(Feed feed) {
        this.feed = feed;
        return this;
    }

    public CommentBox setAuthUser(SmUser authUser) {
        this.authUser = authUser;
        return this;
    }
}

package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.UpdateCommentActivity;
import com.tagusnow.tagmejob.ViewComment;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import org.sufficientlysecure.htmltextview.HtmlTextView;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.whalemare.sheetmenu.SheetMenu;

public class CommentFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = CommentFace.class.getSimpleName();
    private static final String COPY = "comment_text";
    private Activity activity;
    private Context context;
    private CircleImageView commentUserProfile;
    private TextView comment_username,comment_time;
    private ImageButton comment_btn_option;
    private LinearLayout layout_comment_image;
    private HtmlTextView comment_text;
    private ImageView comment_image;
    private final static int REQUEST_EDIT = 8203;
    private SmUser user;
    private Comment comment;
    private int position;
    private RecyclerView.Adapter adapter;
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private CommentService service = retrofit.create(CommentService.class);
    private MenuItem.OnMenuItemClickListener mOnClick = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.btnCopyComment :
                    /*code*/
                    copy();
                    break;
                case R.id.btnEditComment :
                    edit();
                    break;
                case R.id.btnDeleteComment :
                    alertDelete();
                    break;
                    /*case R.id.btnHideComment :
                        hide();
                        break;*/
            }
            return false;
        }
    };
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            SheetMenu.with(context)
                    .setAutoCancel(true)
                    .setMenu(R.menu.menu_comment_option)
                    .setClick(mOnClick).showIcons(true).show();
        }
    };
    private Runnable mAlert = new Runnable() {
        @Override
        public void run() {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setTitle("Delete Comment");
            alertDialog.setMessage("Are you sure?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    delete();
                }
            });
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (dialogInterface !=null){
                        dialogInterface.dismiss();
                    }
                }
            });
            alertDialog.show();
        }
    };
    private Callback<Comment> mDelete = new Callback<Comment>() {
        @Override
        public void onResponse(Call<Comment> call, Response<Comment> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                Comment(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Comment> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Comment> mHideBack = new Callback<Comment>() {
        @Override
        public void onResponse(Call<Comment> call, Response<Comment> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                Comment(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Comment> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public CommentFace(Context context) {
        super(context);
        inflate(context, R.layout.comment_component_layout,this);
        this.context = context;
        commentUserProfile = (CircleImageView)findViewById(R.id.commentUserProfile);
        comment_username = (TextView)findViewById(R.id.comment_username);
        comment_text = (HtmlTextView)findViewById(R.id.comment_text);
        comment_image = (ImageView)findViewById(R.id.comment_image);
        layout_comment_image = (LinearLayout)findViewById(R.id.layout_comment_image);
        comment_time = (TextView) findViewById(R.id.comment_time);
        comment_btn_option = (ImageButton) findViewById(R.id.comment_btn_option);
        comment_image.setOnClickListener(this);
        comment_btn_option.setOnClickListener(this);
        commentUserProfile.setOnClickListener(this);
        comment_username.setOnClickListener(this);
    }

    public CommentFace Auth(SmUser user){
        this.user = user;
        return this;
    }

    public CommentFace Adapter(RecyclerView.Adapter adapter){
        this.adapter = adapter;
        return this;
    }

    public CommentFace Position(int position){
        this.position = position;
        return this;
    }

    public CommentFace Comment(Comment comment){
        this.comment = comment;
        if (comment!=null){
            if (comment.getTrash()==0 && comment.getStatus()==1){
                if (comment.getComment_user().getImage()!=null){
                    if (comment.getComment_user().getImage().contains("http")){
                        Glide.with(context)
                                .load(comment.getComment_user().getImage())
                                .centerCrop()
                                .error(R.mipmap.ic_launcher_circle)
                                .into(commentUserProfile);
                    }else{
                        Glide.with(context)
                                .load(SuperUtil.getProfilePicture(comment.getComment_user().getImage(),"200"))
                                .centerCrop()
                                .error(R.mipmap.ic_launcher_circle)
                                .into(commentUserProfile);
                    }
                }else {
                    Glide.with(context)
                            .load(R.mipmap.ic_launcher_circle)
                            .centerCrop()
                            .into(commentUserProfile);
                }
                comment_username.setText(comment.getComment_user().getUser_name());
                if (!comment.isNullDescription()){
                    comment_text.setVisibility(View.VISIBLE);
                    comment_text.setHtml(comment.getDes());
                }else {
                    comment_text.setVisibility(View.GONE);
                }
                if (!comment.isNullImage()){
                    layout_comment_image.setVisibility(View.VISIBLE);
                    Glide.with(context)
                            .load(SuperUtil.getCommentPicture(comment.getImage(),"100"))
                            .fitCenter()
                            .error(android.R.drawable.ic_menu_report_image)
                            .into(comment_image);
                }else {
                    layout_comment_image.setVisibility(View.GONE);
                }

                comment_time.setText(comment.getTimeline());

                if (this.user!=null && this.user.getId()==comment.getComment_user().getId()){
                    comment_btn_option.setVisibility(View.VISIBLE);
                }else{
                    comment_btn_option.setVisibility(View.GONE);
                }
            }else {
//                this.adapter.notifyItemRemoved(this.position);
//                this.adapter.notifyDataSetChanged();
            }
        }else {
            Toast.makeText(this.context,"No Comment",Toast.LENGTH_SHORT).show();
        }
        return this;
    }

    public CommentFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    private void viewComment(){
        Intent mZoomImageIntent = new Intent(this.activity, ViewComment.class);
        mZoomImageIntent.putExtra("smComment",new Gson().toJson(comment));
        mZoomImageIntent.putExtra("commentUser",new Gson().toJson(comment.getComment_user()));
        mZoomImageIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.activity.startActivity(mZoomImageIntent);
    }

    private void viewProfile(){
        if (comment.getUser_id()!=this.user.getId()){
            context.startActivity(ViewProfileActivity
                    .createIntent(activity)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra("profileUser",new Gson().toJson(comment.getComment_user())));
        }
    }

    private void delete(){
        this.comment.setTrash(1);
        service.delete(this.user.getId(),this.comment.getFeed_id(),this.comment.getId()).enqueue(mDelete);
    }

    private void alertDelete(){
        this.activity.runOnUiThread(mAlert);
    }

    private void hide(){
        this.comment.setStatus(1);
//        this.service.update(this.comment,this.user.getId()).enqueue(mHideBack);
    }

    private void edit(){
        activity.startActivityForResult(UpdateCommentActivity
                .createIntent(activity)
                .putExtra("Comment",new Gson().toJson(comment))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra("position",position),REQUEST_EDIT);
    }

    private void copy(){
        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        String comment_text = "";
        if (comment.isNullDescription()){
            if (!comment.isNullImage()){
                if (comment.getImage().contains("http")){
                    comment_text = comment.getImage();
                }else{
                    comment_text = SuperUtil.getCommentPicture(comment.getImage(),"200");
                }
            }
        }else{
            comment_text = comment.getDes();
        }
        ClipData clip = ClipData.newPlainText(COPY,comment_text);
        assert clipboard != null;
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context,"Copies",Toast.LENGTH_SHORT).show();
    }

    private void showOption(){
        if (this.activity!=null){
            this.activity.runOnUiThread(mRun);
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==comment_image){
            viewComment();
        }else if (view==comment_btn_option){
            showOption();
        }else if (view==commentUserProfile){
            viewProfile();
        }else if (view==comment_username){
            viewProfile();
        }
    }
}

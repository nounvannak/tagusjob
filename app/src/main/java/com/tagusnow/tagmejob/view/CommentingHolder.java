package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class CommentingHolder extends RecyclerView.ViewHolder {

    private CommentingLayout layout;

    public CommentingHolder(View itemView) {
        super(itemView);
        this.layout = (CommentingLayout)itemView;
    }

    public void bindView(Activity activity,String image,String name){
        this.layout.setActivity(activity);
        this.layout.setImage(image);
        this.layout.setName(name);
    }
}

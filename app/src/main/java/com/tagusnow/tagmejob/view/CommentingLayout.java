package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class CommentingLayout extends TagUsJobRelativeLayout{

    private Context context;
    private CircleImageView profile;
    private TextView user_name;
    private String image,name;

    private void initUI(Context context){
        inflate(context, R.layout.layout_commenting,this);
        this.context = context;
        profile = (CircleImageView)findViewById(R.id.profile);
        user_name = (TextView)findViewById(R.id.user_name);
    }

    public CommentingLayout(Context context) {
        super(context);
        this.initUI(context);
    }

    public CommentingLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public CommentingLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public CommentingLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setImage(String image) {
        this.image = image;
        Glide.with(this.context)
                .load(image)
                .centerCrop()
                .error(R.mipmap.ic_launcher_circle)
                .into(profile);
    }

    public void setName(String name) {
        this.name = name;
        user_name.setText(name);
    }
}

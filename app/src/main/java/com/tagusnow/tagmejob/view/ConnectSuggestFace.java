package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import java.util.concurrent.TimeUnit;

import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConnectSuggestFace extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = ConnectSuggestFace.class.getSimpleName();
    private Activity activity;
    private Context context;
    private SmUser user1,auth;
    private ImageButton btnClose_1;
    private AvatarView profile_picture_1;
    private TextView profile_name_1,follower_counter_1;
    private Button btnFollow_1;
    private RelativeLayout layout_1;
    private RecyclerView.Adapter adapter;
    private int position;
    private AvatarPlaceholder refreshableAvatarPlaceholder;
    private IImageLoader imageLoader;
    /*Service*/
    private FollowService service = ServiceGenerator.createService(FollowService.class);
    private Callback<SmUser> mFollowCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                User(response.body());
                adapter.notifyItemChanged(position);
                adapter.notifyDataSetChanged();
            }else {
                Log.e(TAG,response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<SmUser> mUnfollowCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                User(response.body());
                adapter.notifyItemChanged(position);
                adapter.notifyDataSetChanged();
            }else {
                Log.e(TAG,response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void hasFollower(SmUser user){
        int follower = auth!=null ? auth.getId() : 0;
        int following = user!=null ? user.getId() : 0;
        service.hasFollow(follower,following).enqueue(mFollowCallback);
    }

    private void hasUnfollow(SmUser user){
        int follower = auth!=null ? auth.getId() : 0;
        int following = user!=null ? user.getId() : 0;
        service.hasUnfollow(follower,following).enqueue(mUnfollowCallback);
    }


    public ConnectSuggestFace Adapter(RecyclerView.Adapter adapter){
        this.adapter = adapter;
        return this;
    }

    public ConnectSuggestFace Position(int position){
        this.position = position;
        return this;
    }

    public ConnectSuggestFace(Context context) {
        super(context);
        inflate(context, R.layout.connect_suggest_face,this);
        this.context = context;
        btnClose_1 = (ImageButton)findViewById(R.id.btnClose_1);
        profile_picture_1 = (AvatarView)findViewById(R.id.profile_picture_1);
        profile_name_1 = (TextView)findViewById(R.id.profile_name_1);
        follower_counter_1 = (TextView)findViewById(R.id.follower_counter_1);
        btnFollow_1 = (Button)findViewById(R.id.btnFollow_1);
        layout_1 = (RelativeLayout)findViewById(R.id.layout_1);
        layout_1.setOnClickListener(this);
        btnFollow_1.setOnClickListener(this);
        btnClose_1.setOnClickListener(this);
    }

    public ConnectSuggestFace Auth(SmUser auth){
        this.auth = auth;
        return this;
    }

    public ConnectSuggestFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public ConnectSuggestFace User(SmUser user1){
        this.user1 = user1;

        if (user1!=null){

            if (user1.isIs_follow()){
                btnFollow_1.setText("Unfollow");
            }else {
                btnFollow_1.setText("Follow");
            }
            refreshableAvatarPlaceholder = new AvatarPlaceholder(user1.getUser_name(), 50);
            imageLoader = new GlideLoader();
            if (user1.getImage()!=null){
                if (user1.getImage().contains("http")){
                    imageLoader.loadImage(profile_picture_1, refreshableAvatarPlaceholder,user1.getImage());
                }else {
                    imageLoader.loadImage(profile_picture_1, refreshableAvatarPlaceholder,SuperUtil.getProfilePicture(user1.getImage()));
                }
            }else {
                imageLoader.loadImage(profile_picture_1, refreshableAvatarPlaceholder,null);
            }
            profile_name_1.setText(user1.getUser_name());
            follower_counter_1.setText(this.activity.getString(R.string.follower_counter,user1.getFollower_counter()));
        }else {
            Toast.makeText(this.context,"No SmUser",Toast.LENGTH_SHORT).show();
        }

        return this;
    }

    private void viewProfile(SmUser user){
        if (this.activity!=null){
            this.activity.startActivity(ViewProfileActivity
                    .createIntent(this.activity)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(ViewProfileActivity.PROFILE_USER,new Gson().toJson(user)));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void follow(SmUser user){
        if (user.isIs_follow()){
            hasUnfollow(user);
        }else {
            hasFollower(user);
        }
    }

    @Override
    public void onClick(View view) {
        if (view==layout_1){
            viewProfile(this.user1);
        }else if (view==btnClose_1){
            layout_1.setVisibility(GONE);
        }else if (view==btnFollow_1){
            follow(this.user1);
        }
    }
}

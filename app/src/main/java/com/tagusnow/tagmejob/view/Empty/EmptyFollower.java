package com.tagusnow.tagmejob.view.Empty;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;

public class EmptyFollower extends RelativeLayout{

    private Activity activity;
    private Context context;

    public EmptyFollower(Context context) {
        super(context);
        inflate(context, R.layout.empty_follower,this);
        this.context = context;
    }
}

package com.tagusnow.tagmejob.view.Empty;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.UserListActivity;

public class EmptyFollowing extends RelativeLayout implements View.OnClickListener {

    private Activity activity;
    private Context context;
    private Button btnNewFollowing;

    public EmptyFollowing(Context context) {
        super(context);
        inflate(context, R.layout.empty_following,this);
        this.context = context;
        btnNewFollowing = (Button)findViewById(R.id.btnNewFollowing);
        btnNewFollowing.setOnClickListener(this);
    }

    public EmptyFollowing Activity(Activity activity){
        this.activity = activity;
        return this;
    }


    private void viewConnectPeople(){
        if (this.activity!=null){
            this.activity.startActivity(UserListActivity.createIntent(this.activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnNewFollowing){
            this.viewConnectPeople();
        }
    }
}

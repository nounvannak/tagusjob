package com.tagusnow.tagmejob.view.Empty;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;

public class EmptyJob extends RelativeLayout{

    private Activity activity;
    private Context context;
    private TextView text;

    public EmptyJob(Context context) {
        super(context);
        inflate(context, R.layout.empty_job,this);
        this.context = context;
        this.text = (TextView)findViewById(R.id.text);
    }

    public EmptyJob setText(String str){
        this.text.setText(str);
        return this;
    }
}

package com.tagusnow.tagmejob.view.Empty;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;

public class EmptyNotification extends RelativeLayout{

    private Activity activity;
    private Context context;

    public EmptyNotification(Context context) {
        super(context);
        inflate(context, R.layout.empty_notification,this);
        this.context = context;
    }
}

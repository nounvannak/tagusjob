package com.tagusnow.tagmejob.view.Empty;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;

public class EmptySearch extends RelativeLayout{

    private Activity activity;
    private Context context;
    private TextView text;

    public EmptySearch(Context context) {
        super(context);
        inflate(context, R.layout.empty_search,this);
        this.context = context;
        this.text = (TextView)findViewById(R.id.text);
        this.text.setText("Not Found!");
    }

    public EmptySearch Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public EmptySearch Query(String query){
        if (query!=null){
            text.setText(this.activity.getString(R.string.not_found_for,query));
        }
        return this;
    }

}

package com.tagusnow.tagmejob.view.FirstLogin.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tagusnow.tagmejob.CategoryDrawer;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompleteActivity extends TagUsJobActivity implements View.OnClickListener {

    private static final String TAG = CompleteActivity.class.getSimpleName();
    public static final String UST = "user_type";
    public static final String INJ = "interest_job";
    public static final String SUA = "suggest_user";
    private SmUser user;
    private int UserType;
    private ArrayList<Integer> InterestJob;
    private ArrayList<SmUser> SuggestUser;
    private Button btnComplete;

    // Service
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build();
    private AuthService service = retrofit.create(AuthService.class);
    private Callback<SmUser> mCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                user = response.body();
                if (user.getUser_type() > 0){
                    dismissProgress();
                    startActivity(CategoryDrawer.createIntent(CompleteActivity.this));
                }
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                    dismissProgress();
                    ShowAlert(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete);
        this.GetTemp();
        this.initUI();
    }

    private void initUI(){
        btnComplete = (Button)findViewById(R.id.btnComplete);
        btnComplete.setOnClickListener(this);
    }

    private void GetTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.UserType = getIntent().getIntExtra(UST,0);
        this.InterestJob = getIntent().getIntegerArrayListExtra(INJ);
        this.SuggestUser = (ArrayList<SmUser>) getIntent().getSerializableExtra(SUA);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,CompleteActivity.class);
    }

    private void Save(){
        List<RequestBody> InJob = new ArrayList<>(),SuUser = new ArrayList<>();
        for (int cateId : this.InterestJob){
            InJob.add(createPartFromString(String.valueOf(cateId)));
        }
        if (this.SuggestUser.size() > 0){
            for (SmUser aSuUser : this.SuggestUser){
                SuUser.add(createPartFromString(String.valueOf(aSuUser.getId())));
            }
            service.FirstLogin(this.user.getId(),this.UserType,InJob,SuUser).enqueue(mCallback);
        }else {
            service.FirstLogin(this.user.getId(),this.UserType,InJob).enqueue(mCallback);
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnComplete){
            showProgress();
            this.Save();
        }
    }
}

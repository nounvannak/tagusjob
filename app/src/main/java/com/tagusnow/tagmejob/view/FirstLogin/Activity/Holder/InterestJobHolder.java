package com.tagusnow.tagmejob.view.FirstLogin.Activity.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.adapter.InterestJobAdapter;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.Widget.InterestJobFace;

public class InterestJobHolder extends RecyclerView.ViewHolder {

    private InterestJobFace face;

    public InterestJobHolder(View itemView) {
        super(itemView);
        this.face = (InterestJobFace)itemView;
    }

    public InterestJobHolder Adapter(InterestJobAdapter adapter){
        this.face.setAdapter(adapter);
        return this;
    }

    public void Adapter(InterestJobAdapter adapter,int position){
        this.face.setAdapter(adapter,position);
    }

    public InterestJobHolder bindView(Activity activity, Category category){
        this.face.Activity(activity).Category(category);
        return this;
    }

    public void onOther(Activity activity){
        this.face.Activity(activity).isOther().setIcon(R.drawable.ic_btn_plus_icon);
        this.face.setTile("Select more");
    }
}

package com.tagusnow.tagmejob.view.FirstLogin.Activity.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tagusnow.tagmejob.adapter.SuggestUserAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.Widget.SuggestUser;

public class SuggestUserHolder extends RecyclerView.ViewHolder {

    private SuggestUser face;

    public SuggestUserHolder(View itemView) {
        super(itemView);
        this.face = (SuggestUser)itemView;
    }

    public void bindView(Activity activity, SuggestUserAdapter adapter, SmUser auth,SmUser[] users,int position){
        try {
            this.face.setAdapterTag(adapter.getClass().getSimpleName());
            this.face.setAdapter(adapter);
            this.face.setPosition(position);
            this.face.Activity(activity).Auth(auth).User(users[SuggestUser.ONE],SuggestUser.ONE).User(users[SuggestUser.TWO],SuggestUser.TWO).User(users[SuggestUser.THREE],SuggestUser.THREE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void bindView(Activity activity, SuggestUserAdapter adapter, SmUser auth,SmUser user,int position){
        this.face.setAdapterTag(adapter.getClass().getSimpleName());
        this.face.setAdapter(adapter);
        this.face.setPosition(position);
        this.face.Activity(activity).Auth(auth).User(user,SuggestUser.ONE);
    }
}

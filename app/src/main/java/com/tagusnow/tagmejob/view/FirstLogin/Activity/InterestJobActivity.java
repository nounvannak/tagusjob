package com.tagusnow.tagmejob.view.FirstLogin.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.adapter.InterestJobAdapter;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class InterestJobActivity extends TagUsJobActivity implements View.OnClickListener {

    private static final String TAG = InterestJobActivity.class.getSimpleName();
    public static final String USER_TYPE = "user_type";
    private Button btnNext;
    private RecyclerView recycler;
    private SmTagSubCategory category;
    private ArrayList<Category> categories = new ArrayList<>();
    private InterestJobAdapter adapter;
    private int CatID[] = SuperConstance.INTEREST_JOB;
    private int UserType = 0;
    private ArrayList<Integer> InterestJob = new ArrayList<>();

    private void temp(){
        this.UserType = getIntent().getIntExtra(USER_TYPE,0);
    }

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_job);
        try {
            this.temp();
            this.initUI();
            this.initCategory();
            this.initRecycler();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initCategory(){
        this.category = new Repository(this).getCategory();
        if (this.category!=null){
            for (int aCatID : CatID) {
                for (Category aCate : this.category.getData()){
                    if (aCate.getId()==aCatID){
                        this.categories.add(aCate);
                    }
                }
            }
        }
    }

    private void initRecycler(){
        this.categories = this.category.getData();
        if (this.categories.size() > 0){
            this.adapter = new InterestJobAdapter(this,this.categories);
            this.recycler.setAdapter(this.adapter);
        }else {
            Log.e(TAG,"Category is nulled");
        }
    }

    private void initUI(){
        btnNext = (Button)findViewById(R.id.btnNext);
        recycler = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(layoutManager);
        btnNext.setOnClickListener(this);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,InterestJobActivity.class);
    }

    private void GoNext(){
        if (this.adapter!=null){
            for (int i=0;i < this.adapter.listChecked.size();i++){
                if (this.adapter.listChecked.get(i)){
                    this.InterestJob.add(i);
                }
            }

            if (this.InterestJob.size() > 0){
                if (this.UserType==UserTypeActivity.USER_POST_CV){
                    startActivity(SuggestUserActivity.createIntent(this)
                            .putExtra(SuggestUserActivity.UST,this.UserType)
                            .putExtra(SuggestUserActivity.INJ,this.InterestJob));
                }else if (this.UserType==UserTypeActivity.USER_POST_JOB){
                    startActivity(CompleteActivity.createIntent(this)
                            .putExtra(CompleteActivity.UST,this.UserType)
                            .putExtra(CompleteActivity.INJ,this.InterestJob)
                            .putExtra(CompleteActivity.SUA,new ArrayList<>()));
                }
            }else {
                ShowAlert("Interest Job","Please, select your interest job!");
            }

        }else {
            Toast.makeText(this,"No Adapter",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnNext){
            this.GoNext();
        }
    }
}

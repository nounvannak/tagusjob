package com.tagusnow.tagmejob.view.FirstLogin.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.adapter.ConnectSuggestFaceAdapter;
import com.tagusnow.tagmejob.adapter.SuggestUserAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SuggestUserActivity extends TagUsJobActivity implements View.OnClickListener {

    private static final String TAG = SuggestUserActivity.class.getSimpleName();
    public static final String UST = "user_type";
    public static final String INJ = "interest_job";
    private int UserType;
    private ArrayList<Integer> InterestJob;

    private Button btnSkip,btnNext;
    private RecyclerView recycler;
    private ArrayList<SmUser> FollowList = new ArrayList<>();
    private UserPaginate userPaginate;
    private SuggestUserAdapter adapter;
    private SmUser user;

    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService service = retrofit.create(FollowService.class);

    private Callback<UserPaginate> mNewCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewConnectSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<UserPaginate> mNextCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextConnectSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_user);
        this.GetTemp();
        this.initUI();
        this.newConnectSuggest();
    }

    private void initUI(){
        btnNext = (Button)findViewById(R.id.btnNext);
        btnSkip = (Button)findViewById(R.id.btnSkip);

        btnNext.setOnClickListener(this);
        btnSkip.setOnClickListener(this);

        recycler = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(layoutManager);
    }

    private void GetTemp(){
        this.UserType = getIntent().getIntExtra(UST,0);
        this.InterestJob = getIntent().getIntegerArrayListExtra(INJ);
        this.user = new Auth(this).checkAuth().token();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SuggestUserActivity.class);
    }

    private void initNewConnectSuggest(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.checkNext();
        this.adapter = new SuggestUserAdapter(this,this.userPaginate);
        this.FollowList = this.adapter.getFollowList();
        recycler.setAdapter(this.adapter);
    }

    private void initNextConnectSuggest(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.checkNext();
        this.adapter.Next(this,this.userPaginate).notifyDataSetChanged();
        this.FollowList = this.adapter.getFollowList();
    }

    private void newConnectSuggest(){
        int user = this.user!=null ? this.user.getId() : 0;
        int limit = 6;
        service.callSuggest(user,limit,1,1).enqueue(mNewCallback);
    }

    private void nextConnectSuggest(){
        int user = this.user!=null ? this.user.getId() : 0;
        int limit = 6;
        int page = this.userPaginate.getCurrent_page() + 1;
        service.callSuggest(user,limit,1,page).enqueue(mNextCallback);
    }

    static int lastPage;
    static boolean isRequest = false;
    private void checkNext(){
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (adapter.getTotal() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    nextConnectSuggest();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void GoNext(){
//        Toast.makeText(this,String.valueOf(this.FollowList.size()),Toast.LENGTH_SHORT).show();
        if (this.FollowList.size() > 0){
            startActivity(CompleteActivity.createIntent(this)
                    .putExtra(CompleteActivity.UST,this.UserType)
                    .putExtra(CompleteActivity.INJ,this.InterestJob)
                    .putExtra(CompleteActivity.SUA,this.FollowList));
        }else {
            ShowAlert("Company Suggestion","If you want to Next please, following someone or Skip it.");
        }
    }

    private void GoSkip(){
        this.FollowList = new ArrayList<>();
        startActivity(CompleteActivity.createIntent(this)
                .putExtra(CompleteActivity.UST,this.UserType)
                .putExtra(CompleteActivity.INJ,this.InterestJob)
                .putExtra(CompleteActivity.SUA,this.FollowList));
    }

    @Override
    public void onClick(View view) {
        if (view==btnSkip){
            this.GoSkip();
        }else if (view==btnNext){
            this.GoNext();
        }
    }
}

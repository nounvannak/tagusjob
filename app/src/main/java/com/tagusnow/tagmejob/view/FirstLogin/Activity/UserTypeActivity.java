package com.tagusnow.tagmejob.view.FirstLogin.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class UserTypeActivity extends TagUsJobActivity implements View.OnClickListener {

    private static final String TAG = UserTypeActivity.class.getSimpleName();
    public static final int USER_POST_CV = 2;
    public static final int USER_POST_JOB = 1;
    public static final String USN = "user_type";
    private CardView card_post_cv,card_post_job;
    private Button btnNext;
    private ImageView check_post_cv,check_post_job;
    private int USER_TYPE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type);
        this.initUI();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,UserTypeActivity.class);
    }

    private void initUI(){
        card_post_cv = (CardView)findViewById(R.id.card_post_cv);
        card_post_job = (CardView)findViewById(R.id.card_post_job);
        check_post_cv = (ImageView)findViewById(R.id.check_post_cv);
        check_post_job = (ImageView)findViewById(R.id.check_post_job);
        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);

        check_post_job.setVisibility(View.GONE);
        check_post_cv.setVisibility(View.GONE);

        card_post_job.setOnClickListener(this);
        card_post_cv.setOnClickListener(this);
    }

    private void GoNext(){
        if (this.USER_TYPE==0){
            ShowAlert("Notice !","Please choose your type !");
        }else {
            startActivity(InterestJobActivity.createIntent(this).putExtra(InterestJobActivity.USER_TYPE,USER_TYPE));
        }
    }

    @Override
    public void onClick(View view) {
        if (card_post_cv==view){
            check_post_cv.setVisibility(View.VISIBLE);
            check_post_job.setVisibility(View.GONE);
            this.USER_TYPE = USER_POST_CV;
        }else if (card_post_job==view){
            check_post_cv.setVisibility(View.GONE);
            check_post_job.setVisibility(View.VISIBLE);
            this.USER_TYPE = USER_POST_JOB;
        }else if (btnNext==view){
            this.GoNext();
        }
    }
}

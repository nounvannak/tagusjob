package com.tagusnow.tagmejob.view.FirstLogin.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobActivity;


public class WelcomeActivity extends TagUsJobActivity implements View.OnClickListener {

    private Button button_start;
    private TextView welcome_user;
    private SmUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        this.user = new Auth(this).checkAuth().token();
        this.initUI();
    }

    private void initUI(){
        welcome_user = (TextView)findViewById(R.id.welcome_user);
        welcome_user.setText(getString(R.string.welcome_to_tagusjob_com,this.user.getFirs_name()));
        button_start = (Button)findViewById(R.id.buttom_start);
        button_start.setOnClickListener(this);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,WelcomeActivity.class);
    }

    @Override
    public void onClick(View view) {
        if (view==button_start){
            startActivity(UserTypeActivity.createIntent(this));
        }
    }
}

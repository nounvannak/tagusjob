package com.tagusnow.tagmejob.view.FirstLogin.Activity.Widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RepositoryService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.adapter.InterestJobAdapter;
import com.tagusnow.tagmejob.feed.Category;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InterestJobFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = InterestJobFace.class.getSimpleName();
    private Context context;
    private Activity activity;
    private CircleImageView icon;
    private TextView title;
    private ImageView checkbox;
    private Category category;
    private int category_id = 0;
    private boolean isChecked,isOther = false;
    private InterestJobAdapter adapter;
    private int position;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private RepositoryService repositoryService = retrofit.create(RepositoryService.class);
    private Callback<Category> mCallback = new Callback<Category>() {
        @Override
        public void onResponse(Call<Category> call, Response<Category> response) {
            if (response.isSuccessful()){
                Category(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Category> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void GetCategory(){
        repositoryService.GetCategory(this.category_id).enqueue(mCallback);
    }

    private void initUI(Context context){
        inflate(context, R.layout.interest_job_face_layout,this);
        this.context = context;
        icon = (CircleImageView)findViewById(R.id.icon);
        title = (TextView)findViewById(R.id.title);
        checkbox = (ImageView)findViewById(R.id.checkbox);
        this.setOnClickListener(this);
    }

    public InterestJobFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public InterestJobFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public InterestJobFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public InterestJobFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public InterestJobFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public InterestJobFace Category(Category category){
        this.category = category;
        this.initData(this.category);
        return this;
    }

    public InterestJobFace isOther(){
        this.isOther = true;
        this.checkbox.setVisibility(GONE);
        return this;
    }

    public void setIcon(int icon){
        Glide.with(this.context)
                .load(icon)
                .error(R.mipmap.ic_launcher_circle)
                .into(this.icon);
    }

    public void setIcon(String icon){
        if (icon.contains("http")){
            Glide.with(this.context)
                    .load(icon)
                    .error(R.mipmap.ic_launcher_circle)
                    .into(this.icon);
        }else {
            Glide.with(this.context)
                    .load(SuperUtil.getCategoryPath(icon))
                    .error(R.mipmap.ic_launcher_circle)
                    .into(this.icon);
        }
    }

    public void setTile(String title){
        this.title.setText(title);
    }

    private void initData(Category category){
        this.category = category;
        if (category!=null){
            this.title.setText(category.getTitle());
            if (category.getImage()!=null){
                if (category.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(category.getImage())
                            .error(R.mipmap.ic_launcher_circle)
                            .into(this.icon);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getCategoryPath(category.getImage()))
                            .error(R.mipmap.ic_launcher_circle)
                            .into(this.icon);
                }
            }else {
                Glide.with(this.context).load(R.mipmap.ic_launcher_circle).into(this.icon);
            }
        }else {
            Toast.makeText(this.context,"No Category",Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("ResourceAsColor")
    public InterestJobFace checked(){
        checkbox.setBackgroundResource(R.drawable.ic_action_check_circle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            checkbox.setBackgroundTintList(ColorStateList.valueOf(R.color.colorHeader));
        }
        return this;
    }

    @SuppressLint("ResourceAsColor")
    public InterestJobFace unchecked(){
        checkbox.setBackgroundResource(R.drawable.ic_action_radio_button_unchecked);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            checkbox.setBackgroundTintList(ColorStateList.valueOf(R.color.colorHeader));
        }
        return this;
    }

    @Override
    public void onClick(View view) {
        if (this==view){
            if (this.isOther){

            }else {
                if (this.adapter.listChecked.get(this.category.getId())){
                    this.isChecked = false;
                    this.adapter.listChecked.add(this.category.getId(),false);
                    this.unchecked();
                }else {
                    this.isChecked = true;
                    this.adapter.listChecked.add(this.category.getId(),true);
                    this.checked();
                }
            }
        }
    }

    public int getCategoryId() {
        return category_id;
    }

    public void setCategoryId(int category_id) {
        this.category_id = category_id;
        this.GetCategory();
    }

    public InterestJobAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(InterestJobAdapter adapter) {
        this.adapter = adapter;
    }

    public void setAdapter(InterestJobAdapter adapter,int position) {
        this.adapter = adapter;
        this.setPosition(position);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        Log.d(TAG,"listChecked["+String.valueOf(this.position)+"] = "+String.valueOf(this.adapter.listChecked.size() > 0 ? this.adapter.listChecked.get(this.category.getId()) : "null"));
        if (this.adapter.listChecked.get(this.category.getId())){
            this.checked();
        }else {
            this.unchecked();
        }
    }
}

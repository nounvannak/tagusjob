package com.tagusnow.tagmejob.view.FirstLogin.Activity.Widget;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.adapter.FollowSuggestHorizontalAdapter;
import com.tagusnow.tagmejob.adapter.SuggestUserAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestFace;

public class SuggestUser extends RelativeLayout {

    private static final String TAG = SuggestUser.class.getSimpleName();
    public static final int ONE = 0;
    public static final int TWO = 1;
    public static final int THREE = 2;
    private SmUser[] users = {null,null,null};
    private Activity activity;
    private Context context;
    private SmUser auth;
    private int position;
    private RecyclerView.Adapter adapter;
    private SuggestUserAdapter suggestUserAdapter;
    private FollowSuggestHorizontalAdapter followSuggestHorizontalAdapter;
    private FollowSuggestFace user_1,user_2,user_3;
    private String adapterTag = TAG;

    private void initUI(Context context){
        inflate(context, R.layout.suggest_user_layout,this);
        this.context = context;
        user_1 = (FollowSuggestFace)findViewById(R.id.user_1);
        user_2 = (FollowSuggestFace)findViewById(R.id.user_2);
        user_3 = (FollowSuggestFace)findViewById(R.id.user_3);
    }

    public SuggestUser Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public SuggestUser Auth(SmUser auth){
        this.auth = auth;
        return this;
    }

    public SuggestUser User(SmUser user,int index){
        try {
            this.users[index] = user;
            this.initUser();
        }catch (Exception e){
            e.printStackTrace();
        }
        return this;
    }

    private void initUser(){
        try {
            if (this.users[ONE]!=null){
                user_1.setVisibility(VISIBLE);
                this.BuildUser(this.users[ONE],user_1);
            }else {
                user_1.setVisibility(GONE);
            }

            if (this.users[TWO]!=null){
                user_2.setVisibility(VISIBLE);
                this.BuildUser(this.users[TWO],user_2);
            }else {
                user_2.setVisibility(GONE);
            }

            if (this.users[THREE]!=null){
                user_3.setVisibility(VISIBLE);
                this.BuildUser(this.users[THREE],user_3);
            }else {
                user_3.setVisibility(INVISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void BuildUser(SmUser user,FollowSuggestFace face){
        if (user!=null){
            try {
                face.setShowProfile(false);
                face.setShowCloseButton(false);
                face.setShowCounterFollowing(false);
                face.setShowCounterPost(true);
                face.setAdapterTag(this.adapterTag);
                face.setResponse(false);
                face.Activity(this.activity).Adapter(this.adapter).Position(this.position).Auth(this.auth).User(user);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            Toast.makeText(this.context,"No user data",Toast.LENGTH_SHORT).show();
        }
    }

    public SuggestUser(Context context) {
        super(context);
        this.initUI(context);
    }

    public SuggestUser(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public SuggestUser(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public SuggestUser(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public SmUser getAuth() {
        return auth;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
        if (this.adapterTag.equals(SuggestUserAdapter.class.getSimpleName())){
            this.suggestUserAdapter = (SuggestUserAdapter)adapter;
        }else if (this.adapterTag.equals(FollowSuggestHorizontalAdapter.class.getSimpleName())){
            this.followSuggestHorizontalAdapter = (FollowSuggestHorizontalAdapter)adapter;
        }
    }

    public String getAdapterTag() {
        return adapterTag;
    }

    public void setAdapterTag(String adapterTag) {
        this.adapterTag = adapterTag;
    }
}

package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SeeMoreSuggestFollowActivity;
import com.tagusnow.tagmejob.adapter.UserFollowSuggestAdapter;
import com.tagusnow.tagmejob.auth.SmUser;

import java.util.List;

public class FollowSuggestFaceLayout extends LinearLayout implements View.OnClickListener{

    private Context context;
    private Activity activity;
    private ImageButton btnImprove;
    private RecyclerView recyclerSuggestFollow;
    private LinearLayout btnSeeAll;

    private List<SmUser> listSuggestFollows;

    public FollowSuggestFaceLayout(Context context) {
        super(context);
        inflate(context, R.layout.layout_suggest_follow,this);
        btnImprove = (ImageButton)findViewById(R.id.btnImprove);
        btnSeeAll = (LinearLayout)findViewById(R.id.btnSeeAll);
        btnSeeAll.setOnClickListener(this);
        btnImprove.setOnClickListener(this);
        recyclerSuggestFollow = (RecyclerView)findViewById(R.id.recyclerSuggestFollow);
        recyclerSuggestFollow.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerSuggestFollow.setLayoutManager(mLayoutManager);
        this.context = context;
    }

    public FollowSuggestFaceLayout with(Activity activity){
        this.activity = activity;
        return this;
    }

    private void seeMoreSuggest(Activity activity){
        if (activity!=null){
            this.context.startActivity(SeeMoreSuggestFollowActivity.createIntent(activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSeeAll:
                seeMoreSuggest(this.activity);
                break;
            case R.id.btnImprove:
                break;
        }
    }

    public List<SmUser> getListSuggestFollows() {
        return listSuggestFollows;
    }

    public void setListSuggestFollows(List<SmUser> listSuggestFollows) {
        this.listSuggestFollows = listSuggestFollows;
        UserFollowSuggestAdapter adapter = new UserFollowSuggestAdapter(this.context).with(activity).suggestUser(listSuggestFollows);
        recyclerSuggestFollow.setAdapter(adapter);
    }
}

package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FollowerFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = FollowerFace.class.getSimpleName();
    private Context context;
    private Activity activity;
    private ImageView profile_picture;
    private TextView profile_name,follower_counter;
    private Button btnUnfollow;
    private SmUser user,auth;
    private RecyclerView.Adapter adapter;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService service = retrofit.create(FollowService.class);
    private Callback<SmUser> mFollowCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                User(response.body());
            }else {
                Log.e(TAG,response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<SmUser> mUnfollowCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                User(response.body());
            }else {
                Log.e(TAG,response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void hasFollower(){
        int follower = auth!=null ? auth.getId() : 0;
        int following = user!=null ? user.getId() : 0;
        service.hasFollow(follower,following).enqueue(mFollowCallback);
    }

    private void hasUnfollow(){
        int follower = auth!=null ? auth.getId() : 0;
        int following = user!=null ? user.getId() : 0;
        service.hasUnfollow(follower,following).enqueue(mUnfollowCallback);
    }

    public FollowerFace(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.follower_face_layout,this);
        profile_picture = (ImageView)findViewById(R.id.profile_picture);
        profile_name = (TextView)findViewById(R.id.profile_name);
        follower_counter = (TextView)findViewById(R.id.follower_counter);
        btnUnfollow = (Button)findViewById(R.id.btnUnfollow);
        btnUnfollow.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public FollowerFace User(SmUser user){
        this.user = user;
        if (user!=null){
            if (user.getImage()!=null){
                if (user.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(user.getImage())
                            .error(R.drawable.ic_user_male_icon)
                            .centerCrop()
                            .into(profile_picture);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(user.getImage()))
                            .error(R.drawable.ic_user_male_icon)
                            .centerCrop()
                            .into(profile_picture);
                }
            }else{
                Glide.with(this.context)
                        .load(R.drawable.ic_user_male_icon)
                        .centerCrop()
                        .into(profile_picture);
            }
            profile_name.setText(user.getUser_name());
            follower_counter.setText(this.context.getString(R.string.follower_counter,user.getFollower_counter()));
            if (user.isIs_follow()){
                btnUnfollow.setText(this.context.getString(R.string.unfollow));
            }else {
                btnUnfollow.setText(this.context.getString(R.string.following));
            }
        }else {
            Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
        }
        return this;
    }

    public FollowerFace Auth(SmUser auth){
        this.auth = auth;
        return this;
    }

    public FollowerFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public FollowerFace Adapter(RecyclerView.Adapter adapter){
        this.adapter = adapter;
        return this;
    }

    private void onFollow(){
        if (this.user!=null){
            if (this.user.isIs_follow()){
                hasUnfollow();
            }else {
                hasFollower();
            }
        }else {
            Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewProfile(){
        if (this.user!=null){
            if (this.activity!=null){
                this.context.startActivity(ViewProfileActivity.createIntent(this.activity).putExtra("profileUser",new Gson().toJson(this.user)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }else {
                Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnUnfollow){
            onFollow();
        }else if(view==this){
            viewProfile();
        }
    }
}

package com.tagusnow.tagmejob.view.Grid.Three;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewImageActivity;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.model.v2.feed.MessageFile;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import java.util.List;

public class GridThreeTwo extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ImageView image1,image2,image3;
    private List<Album> albums;
    private String[] photos;

    private void initUI(Context context){
        inflate(context, R.layout.grid_three_two,this);
        this.context = context;
        image1 = (ImageView)findViewById(R.id.image1);
        image2 = (ImageView)findViewById(R.id.image2);
        image3 = (ImageView)findViewById(R.id.image3);

        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public GridThreeTwo(Context context) {
        super(context);
        this.initUI(context);
    }

    public GridThreeTwo(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public GridThreeTwo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public GridThreeTwo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    public GridThreeTwo setAlbums(List<Album> albums) {
        this.albums = albums;
        if (albums!=null){

            this.photos = new String[albums.size()];

            for (int i=0;i<albums.size();i++){
                this.photos[i] = SuperUtil.getAlbumPicture(albums.get(i).getMedia().getImage().getSrc(),"original");
            }

            if (albums.size()==3){
                Glide.with(this.context)
                        .load(SuperUtil.getAlbumPicture(albums.get(0).getMedia().getImage().getSrc(),"500"))
                        .centerCrop()
                        .error(R.drawable.ic_error)
                        .into(image1);
                Glide.with(this.context)
                        .load(SuperUtil.getAlbumPicture(albums.get(1).getMedia().getImage().getSrc(),"500"))
                        .centerCrop()
                        .error(R.drawable.ic_error)
                        .into(image2);
                Glide.with(this.context)
                        .load(SuperUtil.getAlbumPicture(albums.get(2).getMedia().getImage().getSrc(),"500"))
                        .centerCrop()
                        .error(R.drawable.ic_error)
                        .into(image3);
            }
        }
        return this;
    }

    public GridThreeTwo setMessageFile(List<MessageFile> urls) {
        if (urls!=null){

            this.photos = new String[urls.size()];
            for (int i=0;i<urls.size();i++){
                this.photos[i] = SuperUtil.getMessageResource(urls.get(i).getNew_name(),Conversation.IMAGE,"original");
            }

            Glide.with(this.context)
                    .load(urls.get(0).getFile_type()==Conversation.IMAGE ? SuperUtil.getMessageResource(urls.get(0).getNew_name(),Conversation.IMAGE,"500") : ExtensionResource(urls.get(0).getNew_name()))
                    .centerCrop()
                    .error(R.drawable.ic_error)
                    .into(image1);

            Glide.with(this.context)
                    .load(urls.get(1).getFile_type()==Conversation.IMAGE ? SuperUtil.getMessageResource(urls.get(1).getNew_name(),Conversation.IMAGE,"500") : ExtensionResource(urls.get(1).getNew_name()))
                    .centerCrop()
                    .error(R.drawable.ic_error)
                    .into(image2);

            Glide.with(this.context)
                    .load(urls.get(2).getFile_type()==Conversation.IMAGE ? SuperUtil.getMessageResource(urls.get(2).getNew_name(),Conversation.IMAGE,"500") : ExtensionResource(urls.get(2).getNew_name()))
                    .centerCrop()
                    .error(R.drawable.ic_error)
                    .into(image3);

        }
        return this;
    }

    private void ViewImage(int position){
        this.activity.startActivity(ViewImageActivity.createIntent(this.activity).putExtra(ViewImageActivity.PHOTOS,this.photos).putExtra(ViewImageActivity.POSITION,position).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onClick(View view) {
        if (view==image1){
            this.ViewImage(0);
        }else if (view==image2){
            this.ViewImage(1);
        }else if (view==image3){
            this.ViewImage(2);
        }
    }
}

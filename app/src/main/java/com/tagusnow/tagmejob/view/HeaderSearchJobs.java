package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;

public class HeaderSearchJobs extends RelativeLayout implements View.OnClickListener {

    private Activity activity;
    private Context context;
    private TextView title;
    private ImageButton btnSeeMore;

    public HeaderSearchJobs(Context context) {
        super(context);
        inflate(context, R.layout.header_search_jobs,this);
        this.context = context;
        title = (TextView)findViewById(R.id.title);
        btnSeeMore = (ImageButton)findViewById(R.id.btnSeeMore);
        btnSeeMore.setOnClickListener(this);
    }

    public HeaderSearchJobs Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public void setTitle(int result){
        if (result!=0){
            title.setText(this.activity.getString(R.string.search_result,result));
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnSeeMore){

        }
    }
}

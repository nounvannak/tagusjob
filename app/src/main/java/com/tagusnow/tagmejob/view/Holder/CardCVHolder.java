package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.adapter.FeedAdapter;
import com.tagusnow.tagmejob.adapter.JobsAdapter;
import com.tagusnow.tagmejob.adapter.ProfileAdapter;
import com.tagusnow.tagmejob.adapter.ProfileHomeAdapter;
import com.tagusnow.tagmejob.adapter.SeeSingleTopJobsAdapter;
import com.tagusnow.tagmejob.adapter.StaffAdapter;
import com.tagusnow.tagmejob.adapter.ViewProfileUserAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.CardCV;

public class CardCVHolder extends RecyclerView.ViewHolder {

    private CardCV cardCV;

    public CardCVHolder(View itemView) {
        super(itemView);
        this.cardCV = (CardCV)itemView;
    }

    public void bindView(Activity activity, Feed feed, FeedAdapter feedAdapter, SmUser user){
        this.cardCV.setActivity(activity);
        this.cardCV.setUser(user);
        this.cardCV.setFeed(feed);
        this.cardCV.setAdapter(feedAdapter);
        this.cardCV.setAdapterTag(feedAdapter.getClass().getSimpleName());
        this.cardCV.setPosition(getAdapterPosition());
    }

    public void bindView(Activity activity, Feed feed, ProfileHomeAdapter feedAdapter, SmUser user){
        this.cardCV.setActivity(activity);
        this.cardCV.setUser(user);
        this.cardCV.setFeed(feed);
        this.cardCV.setAdapter(feedAdapter);
        this.cardCV.setAdapterTag(feedAdapter.getClass().getSimpleName());
        this.cardCV.setPosition(getAdapterPosition());
    }

    public void bindView(Activity activity, Feed feed, ProfileAdapter feedAdapter, SmUser user){
        this.cardCV.setActivity(activity);
        this.cardCV.setUser(user);
        this.cardCV.setFeed(feed);
        this.cardCV.setAdapter(feedAdapter);
        this.cardCV.setAdapterTag(feedAdapter.getClass().getSimpleName());
        this.cardCV.setPosition(getAdapterPosition());
    }

    public void bindView(Activity activity, Feed feed, ViewProfileUserAdapter feedAdapter, SmUser user){
        this.cardCV.setActivity(activity);
        this.cardCV.setUser(user);
        this.cardCV.setFeed(feed);
        this.cardCV.setAdapter(feedAdapter);
        this.cardCV.setAdapterTag(feedAdapter.getClass().getSimpleName());
        this.cardCV.setPosition(getAdapterPosition());
    }

    public void bindView(Activity activity, Feed feed, JobsAdapter jobsAdapter, SmUser authUser) {
        this.cardCV.setActivity(activity);
        this.cardCV.setUser(authUser);
        this.cardCV.setFeed(feed);
        this.cardCV.setAdapter(jobsAdapter);
        this.cardCV.setAdapterTag(jobsAdapter.getClass().getSimpleName());
        this.cardCV.setPosition(getAdapterPosition());
    }

    public void bindView(Activity activity, Feed feed, SeeSingleTopJobsAdapter seeSingleTopJobsAdapter, SmUser user) {
        this.cardCV.setActivity(activity);
        this.cardCV.setUser(user);
        this.cardCV.setFeed(feed);
        this.cardCV.setAdapter(seeSingleTopJobsAdapter);
        this.cardCV.setAdapterTag(seeSingleTopJobsAdapter.getClass().getSimpleName());
        this.cardCV.setPosition(getAdapterPosition());
    }

    public void bindView(Activity activity, Feed feed, StaffAdapter staffAdapter, SmUser user) {
        this.cardCV.setActivity(activity);
        this.cardCV.setUser(user);
        this.cardCV.setFeed(feed);
        this.cardCV.setAdapter(staffAdapter);
        this.cardCV.setAdapterTag(staffAdapter.getClass().getSimpleName());
        this.cardCV.setPosition(getAdapterPosition());
    }
}

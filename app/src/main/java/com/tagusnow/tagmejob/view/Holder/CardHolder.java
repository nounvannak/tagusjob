package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.adapter.SearchResultAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.Card;

public class CardHolder extends RecyclerView.ViewHolder{

    private Card card;

    public CardHolder(View itemView) {
        super(itemView);
        this.card = (Card)itemView;
    }

    public void bindView(Activity activity, SmUser user, Feed feed){
        this.card.Activity(activity).Auth(user).feed(feed);
    }

    public CardHolder Adapter(RecyclerView.Adapter adapter){
        this.card.Adapter(adapter).Position(getAdapterPosition());
        return this;
    }

    public void bindView(Activity activity, SmUser user,int feedID){
        this.card.Activity(activity).Auth(user).Feed(feedID);
    }
}

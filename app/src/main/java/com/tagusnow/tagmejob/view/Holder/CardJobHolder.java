package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.adapter.FeedAdapter;
import com.tagusnow.tagmejob.adapter.JobsAdapter;
import com.tagusnow.tagmejob.adapter.ProfileAdapter;
import com.tagusnow.tagmejob.adapter.SeeSingleTopJobsAdapter;
import com.tagusnow.tagmejob.adapter.ViewProfileUserAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.CardJob;

public class CardJobHolder extends RecyclerView.ViewHolder {

    private CardJob cardJob;

    public CardJobHolder(View itemView) {
        super(itemView);
        this.cardJob = (CardJob)itemView;
    }

    public void bindView(Activity activity, Feed feed, FeedAdapter feedAdapter, SmUser user){
        this.cardJob.setActivity(activity);
        this.cardJob.setFeed(feed);
        this.cardJob.setAdapterTag(feedAdapter.getClass().getSimpleName());
        this.cardJob.setAdapter(feedAdapter);
        this.cardJob.setPosition(getAdapterPosition());
        this.cardJob.setUser(user);
    }

    public void bindView(Activity activity, Feed feed, JobsAdapter feedAdapter, SmUser user){
        this.cardJob.setActivity(activity);
        this.cardJob.setFeed(feed);
        this.cardJob.setAdapterTag(feedAdapter.getClass().getSimpleName());
        this.cardJob.setAdapter(feedAdapter);
        this.cardJob.setPosition(getAdapterPosition());
        this.cardJob.setUser(user);
    }

    public void bindView(Activity activity, Feed feed, ProfileAdapter feedAdapter, SmUser user){
        this.cardJob.setActivity(activity);
        this.cardJob.setFeed(feed);
        this.cardJob.setAdapterTag(feedAdapter.getClass().getSimpleName());
        this.cardJob.setAdapter(feedAdapter);
        this.cardJob.setPosition(getAdapterPosition());
        this.cardJob.setUser(user);
    }

    public void bindView(Activity activity, Feed feed, ViewProfileUserAdapter feedAdapter, SmUser user){
        this.cardJob.setActivity(activity);
        this.cardJob.setFeed(feed);
        this.cardJob.setAdapterTag(feedAdapter.getClass().getSimpleName());
        this.cardJob.setAdapter(feedAdapter);
        this.cardJob.setPosition(getAdapterPosition());
        this.cardJob.setUser(user);
    }

    public void bindView(Activity activity, Feed feed, SeeSingleTopJobsAdapter seeSingleTopJobsAdapter, SmUser user) {
        this.cardJob.setActivity(activity);
        this.cardJob.setFeed(feed);
        this.cardJob.setAdapterTag(seeSingleTopJobsAdapter.getClass().getSimpleName());
        this.cardJob.setAdapter(seeSingleTopJobsAdapter);
        this.cardJob.setPosition(getAdapterPosition());
        this.cardJob.setUser(user);
    }
}

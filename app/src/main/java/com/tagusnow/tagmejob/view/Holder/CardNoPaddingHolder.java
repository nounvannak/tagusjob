package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.adapter.ViewDetailPostAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.CardNoPadding;

public class CardNoPaddingHolder extends RecyclerView.ViewHolder{

    private CardNoPadding padding;

    public CardNoPaddingHolder(View itemView) {
        super(itemView);
        this.padding = (CardNoPadding)itemView;
    }

    public void  bindView(Activity activity, SmUser user,int feedId){
        this.padding.Activity(activity).Auth(user).Feed(feedId);
    }

    public CardNoPaddingHolder Adapter(RecyclerView.Adapter adapter){
        this.padding.Adapter(adapter).Position(getAdapterPosition());
        return this;
    }

    public void bindView(Activity activity, SmUser user, Feed feed){
        this.padding.Activity(activity).Auth(user).feed(feed);
    }
}

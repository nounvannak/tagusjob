package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.CardSmall;

public class CardSmallHolder extends RecyclerView.ViewHolder{

    private CardSmall cardSmall;

    public CardSmallHolder(View itemView) {
        super(itemView);
        this.cardSmall = (CardSmall)itemView;
    }

    public void bindView(Activity activity, RecyclerView.Adapter adapter, Feed feed,SmUser user){
        this.cardSmall.setAdapter(adapter);
        this.cardSmall.setPosition(getAdapterPosition());
        this.cardSmall.Auth(user).Activity(activity).Feed(feed);
    }

    public void bindView(Activity activity, RecyclerView.Adapter adapter, SmUser user, int feedId, boolean isRecent){
        this.cardSmall.setAdapter(adapter);
        this.cardSmall.setPosition(getAdapterPosition());
        this.cardSmall.Activity(activity).Auth(user).Feed(feedId).setRecent(isRecent);
    }

    public void bindView(Activity activity, RecyclerView.Adapter adapter, Feed feed, SmUser user, boolean isRecent){
        this.cardSmall.setAdapter(adapter);
        this.cardSmall.setPosition(getAdapterPosition());
        this.cardSmall.Activity(activity).Feed(feed).Auth(user).setRecent(isRecent);
    }
}

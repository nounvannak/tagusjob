package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.view.CommentFace;

public class CommentFaceHolder extends RecyclerView.ViewHolder{

    private CommentFace face;

    public CommentFaceHolder(View itemView) {
        super(itemView);
        this.face = (CommentFace)itemView;
    }

    public void bindView(Activity activity, SmUser user, Comment comment,RecyclerView.Adapter adapter){
        this.face.Activity(activity).Auth(user).Comment(comment).Adapter(adapter).Position(getAdapterPosition());
    }
}

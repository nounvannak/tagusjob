package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.ConnectSuggestFace;

public class ConnectSuggestFaceHolder extends RecyclerView.ViewHolder{

    private ConnectSuggestFace face;

    public ConnectSuggestFaceHolder(View itemView) {
        super(itemView);
        this.face = (ConnectSuggestFace)itemView;
    }

    public void bindView(Activity activity,RecyclerView.Adapter adapter, SmUser user1,SmUser auth){
        this.face.Adapter(adapter).Position(getAdapterPosition()).Activity(activity).Auth(auth).User(user1);
    }
}

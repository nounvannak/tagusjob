package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.CounterFollowerAndFollowingWidget;

public class CounterHolder  extends RecyclerView.ViewHolder{

    private Activity activity;
    private CounterFollowerAndFollowingWidget followingWidget;
    private SmUser smUser;

    public CounterHolder user(SmUser smUser){
        this.smUser = smUser;
        return this;
    }

    public CounterHolder activity(Activity activity){
        this.activity = activity;
        return  this;
    }

    public void bindView(){
        if (this.smUser!=null){
            followingWidget.setFollwer(this.smUser.getFollower_counter());
            followingWidget.setFollowing(this.smUser.getFollowing_counter());
        }
    }

    public CounterHolder(View itemView) {
        super(itemView);
        followingWidget = (CounterFollowerAndFollowingWidget)itemView;
    }
}

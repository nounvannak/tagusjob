package com.tagusnow.tagmejob.view.Holder.Empty;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Empty.EmptyComment;

public class EmptyCommentHolder extends RecyclerView.ViewHolder{

    private EmptyComment emptyComment;

    public EmptyCommentHolder(View itemView) {
        super(itemView);
        this.emptyComment = (EmptyComment)itemView;
    }

    public void bindView(){

    }
}

package com.tagusnow.tagmejob.view.Holder.Empty;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Empty.EmptyFollower;

public class EmptyFollowerHolder extends RecyclerView.ViewHolder{

    private EmptyFollower emptyFollower;

    public EmptyFollowerHolder(View itemView) {
        super(itemView);
        this.emptyFollower = (EmptyFollower)itemView;
    }

    public void bindView(){}
}

package com.tagusnow.tagmejob.view.Holder.Empty;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Empty.EmptyFollowing;

public class EmptyFollowingHolder extends RecyclerView.ViewHolder{

    private EmptyFollowing emptyFollowing;

    public EmptyFollowingHolder(View itemView) {
        super(itemView);
        this.emptyFollowing = (EmptyFollowing)itemView;
    }

    public void bindView(Activity activity){
        this.emptyFollowing.Activity(activity);
    }
}

package com.tagusnow.tagmejob.view.Holder.Empty;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Empty.EmptyJob;

public class EmptyJobHolder extends RecyclerView.ViewHolder{

    private EmptyJob emptyJob;

    public EmptyJobHolder(View itemView) {
        super(itemView);
        this.emptyJob = (EmptyJob)itemView;
    }

    public void bindView(){

    }

    public void bindView(String str){
        this.emptyJob.setText(str);
    }
}

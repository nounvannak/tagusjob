package com.tagusnow.tagmejob.view.Holder.Empty;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Empty.EmptyNotification;

public class EmptyNotificationHolder extends RecyclerView.ViewHolder{

    private EmptyNotification emptyNotification;

    public EmptyNotificationHolder(View itemView) {
        super(itemView);
        this.emptyNotification = (EmptyNotification)itemView;
    }

    public void bindView(){

    }
}

package com.tagusnow.tagmejob.view.Holder.Empty;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Empty.EmptySearch;

public class EmptySearchHolder extends RecyclerView.ViewHolder{

    private EmptySearch emptySearch;

    public EmptySearchHolder(View itemView) {
        super(itemView);
        this.emptySearch = (EmptySearch)itemView;
    }

    public void bindView(Activity activity){
        this.emptySearch.Activity(activity);
    }

    public void bindView(Activity activity,String query){
        this.emptySearch.Activity(activity).Query(query);
    }

}

package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.ExpiredJobsFace;

public class ExpiredJobsHolder extends RecyclerView.ViewHolder{

    private ExpiredJobsFace face;

    public ExpiredJobsHolder(View itemView) {
        super(itemView);
        face = (ExpiredJobsFace)itemView;
    }

    public void bindView(Activity activity, RecyclerView.Adapter adapter, int position, SmUser user, Feed feed){
        this.face.Activity(activity).Adapter(adapter,position).Auth(user).Feed(feed);
    }
}

package com.tagusnow.tagmejob.view.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Widget.FollowListHeaderLayout;

public class FollowHeaderHolder extends RecyclerView.ViewHolder{

    FollowListHeaderLayout followListHeaderLayout;
    public FollowHeaderHolder(View itemView) {
        super(itemView);
        followListHeaderLayout = (FollowListHeaderLayout)itemView;
    }

    public void bindView(){

    }
}

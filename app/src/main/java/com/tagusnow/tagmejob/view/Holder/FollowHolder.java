package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.UserFollowSuggestLayout;

public class FollowHolder extends RecyclerView.ViewHolder {

    private Context context;
    private Activity activity;
    private SmUser user;
    private UserFollowSuggestLayout userFollowSuggestLayout;

    public FollowHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();
        this.userFollowSuggestLayout = (UserFollowSuggestLayout) itemView;
    }

    public FollowHolder with(Activity activity){
        this.activity = activity;
        return this;
    }

    public FollowHolder user(SmUser smUser){
        this.user = smUser;
        return this;
    }

    public void bindView(){
        this.userFollowSuggestLayout.setUserFollowSuggest(user);
    }
}

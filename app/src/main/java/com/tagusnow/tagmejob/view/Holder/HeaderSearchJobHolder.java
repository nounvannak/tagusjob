package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.HeaderSearchJobs;

public class HeaderSearchJobHolder extends RecyclerView.ViewHolder{

    private HeaderSearchJobs headerSearchJobs;

    public HeaderSearchJobHolder(View itemView) {
        super(itemView);
        this.headerSearchJobs = (HeaderSearchJobs)itemView;
    }

    public void bindView(Activity activity,int result){
        this.headerSearchJobs.Activity(activity).setTitle(result);
    }
}

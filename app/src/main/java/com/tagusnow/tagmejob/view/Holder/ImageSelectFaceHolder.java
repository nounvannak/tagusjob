package com.tagusnow.tagmejob.view.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.ImageSelectFace;
import com.tagusnow.tagmejob.view.Post.LayoutSelectImageHandler;

public class ImageSelectFaceHolder extends RecyclerView.ViewHolder{

    private ImageSelectFace face;

    public ImageSelectFaceHolder(View itemView) {
        super(itemView);
        this.face = (ImageSelectFace)itemView;
    }

    public void bindView(LayoutSelectImageHandler layoutSelectImageHandler, RecyclerView.Adapter adapter, String file){
        this.face.setLayoutSelectImageHandler(layoutSelectImageHandler);
        this.face.Adapter(adapter,getAdapterPosition()).setFile(file);
    }
}

package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.JobDetailView;

public class JobDetailHolder extends RecyclerView.ViewHolder{

    private JobDetailView view;

    public JobDetailHolder(View itemView) {
        super(itemView);
        this.view = (JobDetailView)itemView;
    }

    public void bindView(Activity activity, Feed feed){
        this.view.setActivity(activity);
        this.view.Feed(feed);
    }
}

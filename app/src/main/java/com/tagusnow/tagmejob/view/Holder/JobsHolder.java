package com.tagusnow.tagmejob.view.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.BaseCard.Card;

public class JobsHolder extends RecyclerView.ViewHolder{

    private Card card;

    public JobsHolder(View itemView) {
        super(itemView);
        this.card = (Card)itemView;
    }

    public void bindView(Feed feed){
        this.card.feed(feed);
    }
}

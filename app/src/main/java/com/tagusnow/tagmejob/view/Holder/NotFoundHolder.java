package com.tagusnow.tagmejob.view.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Search.NotFound;

public class NotFoundHolder extends RecyclerView.ViewHolder{

    private NotFound notFound;

    public NotFoundHolder(View itemView) {
        super(itemView);
        notFound = (NotFound)itemView;
    }

    public void bindView(){}
}

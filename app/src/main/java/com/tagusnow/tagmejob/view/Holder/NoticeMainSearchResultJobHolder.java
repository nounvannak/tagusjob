package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultJob;

public class NoticeMainSearchResultJobHolder extends RecyclerView.ViewHolder{

    private NoticeMainSearchResultJob job;

    public NoticeMainSearchResultJobHolder(View itemView) {
        super(itemView);
        this.job = (NoticeMainSearchResultJob)itemView;
    }

    public void bindView(Activity activity, SmUser user,String query){
        this.job.Activity(activity).Auth(user).Query(query);
    }

    public void bindView(Activity activity, SmUser user,String query,int limit){
        this.job.Activity(activity).Limit(limit).Auth(user).Query(query);
    }
}

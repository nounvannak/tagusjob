package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultPeople;

public class NoticeMainSearchResultPeopleHolder extends RecyclerView.ViewHolder{

    private NoticeMainSearchResultPeople people;

    public NoticeMainSearchResultPeopleHolder(View itemView) {
        super(itemView);
        this.people = (NoticeMainSearchResultPeople)itemView;
    }

    public void bindView(Activity activity, SmUser user,String query){
       this.people.Activity(activity).Auth(user).Query(query);
    }

    public void bindView(Activity activity, SmUser user,String query,int limit){
        this.people.Activity(activity).Limit(limit).Auth(user).Query(query);
    }

}

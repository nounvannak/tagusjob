package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultPost;

public class NoticeMainSearchResultPostHolder extends RecyclerView.ViewHolder{

    private NoticeMainSearchResultPost post;

    public NoticeMainSearchResultPostHolder(View itemView) {
        super(itemView);
        this.post = (NoticeMainSearchResultPost)itemView;
    }

    public void bindView(Activity activity, SmUser user,String query){
        this.post.Activity(activity).Auth(user).Query(query);
    }

    public void bindView(Activity activity, SmUser user,String query,int limit){
        this.post.Activity(activity).Limit(limit).Auth(user).Query(query);
    }
}

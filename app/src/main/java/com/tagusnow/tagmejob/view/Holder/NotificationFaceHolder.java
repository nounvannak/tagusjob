package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Notification;
import com.tagusnow.tagmejob.view.NotificationFace;

public class NotificationFaceHolder extends RecyclerView.ViewHolder{

    private NotificationFace face;

    public NotificationFaceHolder(View itemView) {
        super(itemView);
        this.face = (NotificationFace)itemView;
    }

    public void bindView(Activity activity, Notification notification, SmUser user){
        this.face.Notification(notification);
        this.face.setActivity(activity);
        this.face.setUser(user);
    }
}

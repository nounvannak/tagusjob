package com.tagusnow.tagmejob.view.Holder.Profile.User;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.PostJobActivity;
import com.tagusnow.tagmejob.PrivacyActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewDetailPostActivity;
import com.tagusnow.tagmejob.ViewPostActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.view.BaseCard.Card;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.whalemare.sheetmenu.SheetMenu;

public class BodyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private static final String TAG = BodyHolder.class.getSimpleName();
    private final static int REQ_PRIVACY = 3729;
    private Activity activity;
    private Context context;
    private Card card;
    private Feed feed;
    Socket socket;
    private SmUser authUser;
    private RecyclerView.Adapter adapter;

    CircleImageView mProfilePicture;
    TextView mFeedStoryTitle;
    ImageView mFeedPicture;
    LinearLayout mFeedLikeButton,mFeedCommentButton,shareButton,album2,album3,album4,albumMoreThan4;
    RelativeLayout profileBar;
    Button mButtonMoreFeed;
    TextView mFeedTitle;
    TextView job_description;
    ImageView icon_saved;

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Count Like "+feed.getId(),mCountLike);
        socket.on("Count Comment "+feed.getId(),mCountComment);
        socket.connect();
    }

    private Emitter.Listener mCountLike = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,obj.toString());
                        if (obj.getInt("feed_id")==feed.getId()){
                            feed.getFeed().setIs_like(obj.getInt("is_like") == 1);
                            feed.getFeed().setCount_likes(obj.getInt("count_likes"));
                            adapter.notifyDataSetChanged();
                            bindView();
                        }
                    }catch (Exception e){
                        SuperUtil.errorTrancking(TAG+"::mCountLike");
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,obj.toString());
                        if (obj.getInt("feed_id")==feed.getId()){
                            feed.getFeed().setCount_comments(obj.getInt("count_comments"));
                            adapter.notifyDataSetChanged();
                            bindView();
                        }
                    }catch (Exception e){
                        SuperUtil.errorTrancking(TAG+"::mCountComment");
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    public BodyHolder(View itemView) {
        super(itemView);
        this.authUser = new Auth(itemView.getContext()).checkAuth().token();
        this.context = itemView.getContext();
        this.card = (Card)itemView;
    }

    public BodyHolder adapter(RecyclerView.Adapter adapter){
        this.adapter = adapter;
        return this;
    }

    private void removePost(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("post_id",feed.getId());
        params.put("auth_user",authUser.getId());
        client.put(SuperUtil.getBaseUrl(SuperConstance.REMOVE_POST), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                feed.setDelete(1);
                adapter.notifyItemRemoved(getAdapterPosition());
                Log.w(TAG,rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void hidePost(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("user_post_id",feed.getUser_id());
        params.put("post_id",feed.getId());
        params.put("user_hide_id",authUser.getId());
        client.put(SuperUtil.getBaseUrl(SuperConstance.HIDE_POST), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                feed.setIs_hide(1);
                adapter.notifyItemRemoved(getAdapterPosition());
                Log.w(TAG,rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void editPrivacy(){
        Intent intent = new Intent(this.activity, PrivacyActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(SuperConstance.PRIVACY,feed.getPrivacy());
        intent.putExtra("position",getAdapterPosition());
        this.activity.startActivityForResult(intent,REQ_PRIVACY);
    }

    private void editPost(){

        if (this.feed.getFeed().getType().equals(SuperConstance.POST_JOB)){
            Intent intent = new PostJobActivity().isNew(false).setTitle("Edit post job").createIntent(this.activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Feed",new Gson().toJson(this.feed));
            intent.putExtra(SuperConstance.PRIVACY,this.feed.getPrivacy());
            context.startActivity(intent);
        }else {
            Intent mPostIntent = new Intent(context, CreatePostActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mPostIntent.putExtra("Feed",new Gson().toJson(feed));
            context.startActivity(mPostIntent);
        }
    }

    private void savePost(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("user_post_id",feed.getUser_id());
        params.put("post_id",feed.getId());
        params.put("user_save_id",authUser.getId());
        client.put(SuperUtil.getBaseUrl(SuperConstance.SAVE_POST), params, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                feed.setIs_save(1);
                adapter.notifyItemChanged(getAdapterPosition(),feed);
                icon_saved.setVisibility(View.VISIBLE);
                Log.w(TAG,rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                Log.w(TAG,rawJsonData);
            }

            @Override
            protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }

    private void showProfile(){
        if (authUser.getId()==feed.getUser_post().getId()){
            context.startActivity(ProfileActivity
                    .createIntent(activity)
                    .putExtra("profileUser",new Gson().toJson(feed.getUser_post())).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else{
            context.startActivity(ViewProfileActivity
                    .createIntent(activity)
                    .putExtra("profileUser",new Gson().toJson(feed.getUser_post())).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    private void likePost(){
        if (authUser==null){
            Intent loginIntent = new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(loginIntent);
        }
        AsyncHttpClient mLikeFeedClient = new AsyncHttpClient();
        RequestParams mLikeFeedParam = new RequestParams();
        mLikeFeedParam.put("feed_id",feed.getId());
        mLikeFeedParam.put("user_id",authUser.getId());
        if (!feed.getFeed().isIs_like()){
            mLikeFeedClient.post(SuperUtil.getBaseUrl(SuperConstance.LIKE_FEED), mLikeFeedParam, new BaseJsonHttpResponseHandler<AsyncHttpResponseHandler>() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, AsyncHttpResponseHandler response) {
                    SmTagFeed.Like mLikeClick = new Gson().fromJson(rawJsonResponse, SmTagFeed.Like.class);
                    feed.getFeed().setIs_like(mLikeClick.isIs_like());
                    feed.getFeed().setCount_likes(mLikeClick.getCount_likes());
                    adapter.notifyItemChanged(getAdapterPosition());
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, AsyncHttpResponseHandler errorResponse) {
                    Log.w("like response",rawJsonData);
                }

                @Override
                protected AsyncHttpResponseHandler parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                    return null;
                }
            });
        }
    }

    private void viewComment(){
        Intent commentIntent = new Intent(context, CommentActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        commentIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
        commentIntent.putExtra("count_likes",feed.getFeed().getCount_likes());
        commentIntent.putExtra("is_like",feed.getFeed().isIs_like());
        commentIntent.putExtra("count_comments",feed.getFeed().getCount_comments());
        commentIntent.putExtra("feed_id",feed.getId());
        context.startActivity(commentIntent);
    }

    private void shareContent(){
        originalShare();
    }

    private void originalShare(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,SuperUtil.getBaseUrl("feed_detail?feedvalue="+feed.getId()));
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    private void viewPost(){
        String searchTitle = "";
        if (feed.getFeed().getOriginalHastTagText()==null || feed.getFeed().getOriginalHastTagText().equals("")){
            searchTitle = "";
        }else{
            searchTitle = mFeedTitle.getText().toString();
        }
        Intent mViewPostIntent = new Intent(context, ViewPostActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mViewPostIntent.putExtra("Feed",new Gson().toJson(feed));
        mViewPostIntent.putExtra("position",getAdapterPosition());
        mViewPostIntent.putExtra("searchTitle",searchTitle);
        context.startActivity(mViewPostIntent);
    }

    private void viewPostDetail(){
        Intent mViewPostIntent = new Intent(context, ViewDetailPostActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mViewPostIntent.putExtra(SuperConstance.USER_SESSION,new Gson().toJson(authUser));
        mViewPostIntent.putExtra("Feed",new Gson().toJson(feed));
        this.context.startActivity(mViewPostIntent);
    }

    private void showMore(){
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!activity.isFinishing()){
                    int menu = 0;
                    if (authUser!=null && authUser.getId()==feed.getUser_post().getId()){
                        menu = R.menu.menu_post_option_auth;
                    }else{
                        menu = R.menu.menu_post_option_not_auth;
                    }
                    SheetMenu.with(activity)
                            .setAutoCancel(true)
                            .setMenu(menu)
                            .setClick(new MenuItem.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem menuItem) {
                                    switch (menuItem.getItemId()){
                                        case R.id.optionSavePost :
                                            /*code*/
                                            savePost();
                                            break;
                                        case R.id.optionEditPost :
                                            editPost();
                                            break;
                                        case R.id.optionHide :
                                            hidePost();
                                            break;
                                        case R.id.optionDelete :
                                            removePost();
                                            break;
                                    }
                                    return false;
                                }
                            }).showIcons(true).show();
                }
            }
        });
    }

    public BodyHolder activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public BodyHolder feed(Feed feed){
        this.feed = feed;
        return this;
    }

    public void bindView(){
        this.card.feed(this.feed);
        this.icon_saved = this.card.getIcon_saved();
        this.mFeedTitle = this.card.getmFeedTitle();
        this.mButtonMoreFeed = this.card.getmButtonMoreFeed();
        this.profileBar = this.card.getProfileBar();
        this.job_description = this.card.getJob_description();
        this.mFeedStoryTitle = this.card.getmFeedStoryTitle();
        this.mProfilePicture = this.card.getmProfilePicture();
        /*Event Click*/
        this.mFeedTitle.setOnClickListener(this);
        this.mFeedCommentButton.setOnClickListener(this);
        this.mFeedLikeButton.setOnClickListener(this);
        this.mButtonMoreFeed.setOnClickListener(this);
        this.mFeedPicture.setOnClickListener(this);
        this.album2.setOnClickListener(this);
        this.album3.setOnClickListener(this);
        this.album4.setOnClickListener(this);
        this.albumMoreThan4.setOnClickListener(this);
        this.profileBar.setOnClickListener(this);
        this.job_description.setOnClickListener(this);
        this.shareButton.setOnClickListener(this);
        this.mFeedStoryTitle.setOnClickListener(this);
        this.mProfilePicture.setOnClickListener(this);
        initSocket();
    }

    @Override
    public void onClick(View view) {
       if (view==this.mFeedTitle){
           viewPostDetail();
       }else if(view==this.mFeedCommentButton){
           viewComment();
       }else if(view==this.mFeedLikeButton){
           likePost();
       }else if(view==this.mButtonMoreFeed){
           showMore();
       }else if(view==this.mFeedPicture){
           viewPostDetail();
       }else if(view==this.album2){
           viewPostDetail();
       }else if(view==this.album3){
           viewPostDetail();
       }else if(view==this.album4){
           viewPostDetail();
       }else if(view==this.albumMoreThan4){
           viewPostDetail();
       }else if(view==this.profileBar){
           viewPost();
       }else if(view==this.job_description){
           viewPostDetail();
       }else if(view==this.shareButton){
           shareContent();
       }else if(view==this.mFeedStoryTitle){
           showProfile();
       }else if(view==this.mProfilePicture){
           showProfile();
       }
    }
}

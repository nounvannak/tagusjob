package com.tagusnow.tagmejob.view.Holder.Profile.User;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Profile.User.HeaderLayout;

public class HeaderHolder extends RecyclerView.ViewHolder{

    private HeaderLayout headerLayout;
    private boolean isSelectLike = false;

    public HeaderHolder(View itemView) {
        super(itemView);
        this.headerLayout = (HeaderLayout) itemView;
        isSelectLike = true;
    }

    public void BindView(Activity activity,SmUser user,SmUser auth){
        this.headerLayout.setUser(user).setActivity(activity);
        this.headerLayout.setAuth(auth);
    }
}

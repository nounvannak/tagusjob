package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.ProfileHeader;

public class ProfileHeaderHolder extends RecyclerView.ViewHolder{

    private ProfileHeader header;

    public ProfileHeaderHolder(View itemView) {
        super(itemView);
        this.header = (ProfileHeader)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.header.Activity(activity).Auth(user);
    }
}

package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.ProfileInfo;

public class ProfileInfoHolder extends RecyclerView.ViewHolder{

    private ProfileInfo info;

    public ProfileInfoHolder(View itemView) {
        super(itemView);
        this.info = (ProfileInfo)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.info.Activity(activity).Auth(user);
    }
}

package com.tagusnow.tagmejob.view.Holder.Search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SeeAllSimilarSearchActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.NoticeMainSearchResultHeader;

import de.hdodenhof.circleimageview.CircleImageView;

public class FastSearchFilter extends RelativeLayout implements View.OnClickListener {

    private Activity activity;
    private Context context;
    private SmUser user;
    private String query;
    private RelativeLayout filter_people,filter_job,filter_post,filter_location;
    private CircleImageView people_icon,job_icon,post_icon,location_icon;

    public FastSearchFilter(Context context) {
        super(context);
        inflate(context, R.layout.fast_search_filter,this);
        this.context = context;
        filter_people = (RelativeLayout)findViewById(R.id.filter_people);
        filter_job = (RelativeLayout)findViewById(R.id.filter_job);
        filter_post = (RelativeLayout)findViewById(R.id.filter_post);
        filter_location = (RelativeLayout)findViewById(R.id.filter_location);
        people_icon = (CircleImageView)findViewById(R.id.people_icon);
        job_icon = (CircleImageView)findViewById(R.id.job_icon);
        post_icon = (CircleImageView)findViewById(R.id.post_icon);
        location_icon = (CircleImageView)findViewById(R.id.location_icon);
        filter_people.setOnClickListener(this);
        filter_job.setOnClickListener(this);
        filter_post.setOnClickListener(this);
        filter_location.setOnClickListener(this);
    }

    public FastSearchFilter Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public FastSearchFilter Auth(SmUser user){
        this.user = user;
        return this;
    }

    public FastSearchFilter Query(String query){
        this.query = query;
        return this;
    }

    private void showPeople(){
        if (this.activity!=null){
            this.activity.startActivity(SeeAllSimilarSearchActivity
                    .createIntent(this.activity)
                    .putExtra(SeeAllSimilarSearchActivity.QUERY,this.query)
                    .putExtra(SeeAllSimilarSearchActivity.TYPE, NoticeMainSearchResultHeader.PEOPLE)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void showJob(){
        if (this.activity!=null){
            this.activity.startActivity(SeeAllSimilarSearchActivity
                    .createIntent(this.activity)
                    .putExtra(SeeAllSimilarSearchActivity.QUERY,this.query)
                    .putExtra(SeeAllSimilarSearchActivity.TYPE, NoticeMainSearchResultHeader.JOB)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void showPost(){
        if (this.activity!=null){
            this.activity.startActivity(SeeAllSimilarSearchActivity
                    .createIntent(this.activity)
                    .putExtra(SeeAllSimilarSearchActivity.QUERY,this.query)
                    .putExtra(SeeAllSimilarSearchActivity.TYPE, NoticeMainSearchResultHeader.POST)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void showLocation(){
        if (this.activity!=null){
            this.activity.startActivity(SeeAllSimilarSearchActivity
                    .createIntent(this.activity)
                    .putExtra(SeeAllSimilarSearchActivity.QUERY,this.query)
                    .putExtra(SeeAllSimilarSearchActivity.TYPE, NoticeMainSearchResultHeader.LOCATION)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==filter_people){
            showPeople();
        }else if (view==filter_job){
            showJob();
        }else if (view==filter_post){
            showPost();
        }else if (view==filter_location){
            showLocation();
        }
    }
}

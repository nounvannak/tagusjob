package com.tagusnow.tagmejob.view.Holder.Search;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class FastSearchFilterHolder extends RecyclerView.ViewHolder{

    private FastSearchFilter filter;

    public FastSearchFilterHolder(View itemView) {
        super(itemView);
        this.filter = (FastSearchFilter)itemView;
    }

    public void bindView(Activity activity, SmUser user,String query){
        this.filter.Auth(user).Query(query).Activity(activity);
    }
}

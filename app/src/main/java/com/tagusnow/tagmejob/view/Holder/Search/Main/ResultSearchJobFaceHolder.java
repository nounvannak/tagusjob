package com.tagusnow.tagmejob.view.Holder.Search.Main;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.Search.Main.ResultSearchJobFace;

public class ResultSearchJobFaceHolder extends RecyclerView.ViewHolder{

    private ResultSearchJobFace face;

    public ResultSearchJobFaceHolder(View itemView) {
        super(itemView);
        this.face = (ResultSearchJobFace)itemView;
    }

    public void bindView(Activity activity, Feed feed, SmUser user,boolean isRecent){
        this.face.Activity(activity).Feed(feed).Auth(user).isRecent(isRecent);
    }
}

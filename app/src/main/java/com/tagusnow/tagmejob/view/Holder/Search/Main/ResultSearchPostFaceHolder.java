package com.tagusnow.tagmejob.view.Holder.Search.Main;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.Search.Main.ResultSearchPostFace;

public class ResultSearchPostFaceHolder extends RecyclerView.ViewHolder{

    private ResultSearchPostFace face;

    public ResultSearchPostFaceHolder(View itemView) {
        super(itemView);
        this.face = (ResultSearchPostFace)itemView;
    }

    public void bindView(Activity activity, SmUser user, Feed feed,boolean isRecent){
        this.face.Activity(activity).Auth(user).Feed(feed).isRecent(isRecent);
    }
}

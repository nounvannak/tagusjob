package com.tagusnow.tagmejob.view.Holder.Search.Main;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.view.Search.Main.SearchFace;

public class SearchFaceHolder extends RecyclerView.ViewHolder {

    private SearchFace face;

    public SearchFaceHolder(View itemView) {
        super(itemView);
        this.face = (SearchFace)itemView;
    }

    public void bindView(Activity activity, RecentSearch recentSearch, SmUser user){
        this.face.setActivity(activity);
        this.face.setRecentSearch(recentSearch);
        this.face.setUser(user);
        this.face.setRecent(false);
    }

    public void bindView(Activity activity, Feed feed, SmUser user){
        this.face.setActivity(activity);
        this.face.setFeed(feed);
        this.face.setUser(user);
        this.face.setRecent(true);
    }
}

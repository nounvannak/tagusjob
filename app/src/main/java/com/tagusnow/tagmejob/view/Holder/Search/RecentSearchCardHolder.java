package com.tagusnow.tagmejob.view.Holder.Search;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.view.Search.RecentSearchesFace;

public class RecentSearchCardHolder extends RecyclerView.ViewHolder{

    private RecentSearchesFace face;

    public RecentSearchCardHolder(View itemView) {
        super(itemView);
        this.face = (RecentSearchesFace)itemView;
    }

    public void bindView(Activity activity, SmUser auth, RecentSearch recentSearch){
        this.face.Activity(activity).Auth(auth).RecentSearch(recentSearch);
    }
}

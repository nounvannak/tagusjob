package com.tagusnow.tagmejob.view.Holder.Search;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

import com.tagusnow.tagmejob.adapter.RecentSearchCardAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.view.Search.RecentSearchLoadMore;

public class RecentSearchCardLoadMoreHolder extends ViewHolder{

    private RecentSearchLoadMore more;

    public RecentSearchCardLoadMoreHolder(View itemView) {
        super(itemView);
        this.more = (RecentSearchLoadMore)itemView;
    }

    public void bindView(Activity activity, RecentSearchPaginate recentSearchPaginate, RecyclerView.Adapter adapter, SmUser user){
        this.more.Activity(activity).RecentSearchPaginate(recentSearchPaginate).Adapter((RecentSearchCardAdapter) adapter).Auth(user);
    }
}

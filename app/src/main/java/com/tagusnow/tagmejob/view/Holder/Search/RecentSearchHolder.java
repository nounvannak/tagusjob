package com.tagusnow.tagmejob.view.Holder.Search;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;
import com.tagusnow.tagmejob.view.Search.RecentSearchCard;

import java.util.List;

public class RecentSearchHolder extends RecyclerView.ViewHolder{

    private RecentSearchCard card;

    public RecentSearchHolder(View itemView) {
        super(itemView);
        this.card = (RecentSearchCard)itemView;
    }

    public void bindView(Activity activity, SmUser auth, String date, RecentSearchPaginate recentSearchPaginate){
        this.card.setActivity(activity).setAuth(auth).setDate(date).setRecentSearchPaginate(recentSearchPaginate);
    }
}

package com.tagusnow.tagmejob.view.Holder.Search.Similar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.SearchResultActivity;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.RecentSearch;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SimilarSearchFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = SimilarSearchFace.class.getSimpleName();
    private Activity activity;
    private Context context;
    private ImageView image;
    private TextView title,position,salary,location;
    private SmUser user;
    private Feed feed;
    private RecentSearch recentSearch;
    private boolean isRecent;
    private int id;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private RecentSearchService service = retrofit.create(RecentSearchService.class);
    private JobsService jobsService = retrofit.create(JobsService.class);
    private Callback<RecentSearch> mSave = new Callback<RecentSearch>() {
        @Override
        public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                recentSearch = response.body();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearch> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<Feed> mCallJob = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                Feed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void callJob(){
        jobsService.callJob(this.user.getId(),this.id).enqueue(mCallJob);
    }

    private void save(){
        service.save(this.recentSearch,this.user.getId()).enqueue(mSave);
    }

    public SimilarSearchFace(Context context) {
        super(context);
        inflate(context, R.layout.similar_search_face,this);
        this.context = context;
        image = (ImageView)findViewById(R.id.image);
        title = (TextView)findViewById(R.id.title);
        position = (TextView)findViewById(R.id.position);
        salary = (TextView)findViewById(R.id.salary);
        location = (TextView)findViewById(R.id.location);
        this.recentSearch = new RecentSearch();
        this.setOnClickListener(this);
    }

    public SimilarSearchFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public SimilarSearchFace Auth(SmUser user){
        this.user = user;
        if (user!=null){
            this.recentSearch.setUser(user);
            this.recentSearch.setSearch_user_id(user.getId());
        }
        return this;
    }

    public SimilarSearchFace Feed(Feed feed){
        this.feed = feed;
        if (feed!=null){
            if(feed.getType().equals(Feed.TAGUSJOB)){
                if (feed.getFeed().getType().equals(FeedDetail.JOB_POST)){
                    this.recentSearch.setSearch_type(RecentSearch.JOBS);
                    this.recentSearch.setSearch_text(feed.getFeed().getTitle());
                    title.setText(feed.getFeed().getTitle());
                    position.setText(feed.getFeed().getCategory());
                    if (feed.getFeed().getSalary()!=null){
                        salary.setVisibility(VISIBLE);
                        salary.setText(feed.getFeed().getSalary());
                    }else {
                        salary.setVisibility(GONE);
                    }
                    if (feed.getFeed().getCity_name()!=null){
                        location.setVisibility(VISIBLE);
                        location.setText(feed.getFeed().getCity_name());
                    }else {
                        location.setVisibility(GONE);
                    }

                    if (feed.getFeed().getAlbum()!=null && feed.getFeed().getAlbum().size() > 0 && feed.getFeed().getAlbum().get(0)!=null){
                        if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                            Glide.with(this.context)
                                    .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                                    .fitCenter()
                                    .error(R.mipmap.ic_launcher_circle)
                                    .into(image);
                            this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                        }else {
                            Glide.with(this.context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .fitCenter()
                                    .error(R.mipmap.ic_launcher_circle)
                                    .into(image);
                            this.recentSearch.setSearch_image(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"));
                        }
                    }
                }else {
                    this.recentSearch.setSearch_type(RecentSearch.JOBS);
                    this.recentSearch.setSearch_text(feed.getSearch_query());
                    title.setText(feed.getProfile_name());
                    position.setText(feed.getFeed().getCategory());
                    if (feed.getFeed().getSalary()!=null){
                        salary.setVisibility(VISIBLE);
                        salary.setText(feed.getFeed().getSalary());
                    }else {
                        salary.setVisibility(GONE);
                    }
                    if (feed.getFeed().getCity_name()!=null){
                        location.setVisibility(VISIBLE);
                        location.setText(feed.getFeed().getCity_name());
                    }else {
                        location.setVisibility(GONE);
                    }

                    if (feed.getFeed().getAlbum()!=null && feed.getFeed().getAlbum().size() > 0 && feed.getFeed().getAlbum().get(0)!=null){
                        if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                            Glide.with(this.context)
                                    .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                                    .fitCenter()
                                    .error(R.mipmap.ic_launcher_circle)
                                    .into(image);
                            this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                        }else {
                            Glide.with(this.context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .fitCenter()
                                    .error(R.mipmap.ic_launcher_circle)
                                    .into(image);
                            this.recentSearch.setSearch_image(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"));
                        }
                    }
                }
            }
        }
        return this;
    }

    public SimilarSearchFace Feed(int id){
        this.id = id;
        this.callJob();
        return this;
    }

    public SimilarSearchFace isRecent(boolean isRecent){
        this.isRecent = isRecent;
        return this;
    }

    private void viewJob(){
        if (this.isRecent){
            save();
        }

        if (this.activity!=null){
            this.activity.startActivity(SearchResultActivity.createIntent(this.activity).putExtra(SearchResultActivity.FEED,new Gson().toJson(this.feed)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            viewJob();
        }
    }
}

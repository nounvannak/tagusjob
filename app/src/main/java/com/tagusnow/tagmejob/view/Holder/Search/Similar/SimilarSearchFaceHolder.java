package com.tagusnow.tagmejob.view.Holder.Search.Similar;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;

public class SimilarSearchFaceHolder extends RecyclerView.ViewHolder{

    private SimilarSearchFace face;

    public SimilarSearchFaceHolder(View itemView) {
        super(itemView);
        this.face = (SimilarSearchFace)itemView;
    }

    public void bindView(Activity activity, SmUser user, Feed feed,boolean isRecent){
        this.face.Activity(activity).Auth(user).Feed(feed).isRecent(isRecent);
    }

    public void bindView(Activity activity, SmUser user, int feed,boolean isRecent){
        this.face.Activity(activity).Auth(user).Feed(feed).isRecent(isRecent);
    }

}

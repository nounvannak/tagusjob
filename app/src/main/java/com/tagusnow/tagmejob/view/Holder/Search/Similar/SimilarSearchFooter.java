package com.tagusnow.tagmejob.view.Holder.Search.Similar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SeeAllSimilarSearchActivity;
import com.tagusnow.tagmejob.ViewNoticeMainSearchResultActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;

public class SimilarSearchFooter extends RelativeLayout implements View.OnClickListener {

    private Activity activity;
    private Context context;
    private Feed feed;
    private SmUser user;
    private RelativeLayout btnSeeAll;

    public SimilarSearchFooter(Context context) {
        super(context);
        inflate(context, R.layout.similar_search_footer,this);
        this.context = context;
        btnSeeAll = (RelativeLayout)findViewById(R.id.btnSeeAll);
        btnSeeAll.setOnClickListener(this);
    }

    public SimilarSearchFooter Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public SimilarSearchFooter Auth(SmUser user){
        this.user = user;
        return this;
    }

    public SimilarSearchFooter Feed(Feed feed){
        this.feed = feed;
        return this;
    }

    private void seeAll(){
        if (this.activity!=null){
            this.activity.startActivity(ViewNoticeMainSearchResultActivity
                    .createIntent(this.activity)
                    .putExtra(ViewNoticeMainSearchResultActivity.TYPE_TITLE,ViewNoticeMainSearchResultActivity.SEE_ALL)
                    .putExtra(ViewNoticeMainSearchResultActivity.TITLE,this.feed.getSearch_query())
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnSeeAll){
            seeAll();
        }
    }
}

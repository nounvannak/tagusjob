package com.tagusnow.tagmejob.view.Holder.Search.Similar;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;

public class SimilarSearchFooterHolder extends RecyclerView.ViewHolder{

    private SimilarSearchFooter footer;

    public SimilarSearchFooterHolder(View itemView) {
        super(itemView);
        this.footer = (SimilarSearchFooter)itemView;
    }

    public void bindView(Activity activity, SmUser user, Feed feed){
        this.footer.Activity(activity).Auth(user).Feed(feed);
    }
}

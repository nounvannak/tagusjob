package com.tagusnow.tagmejob.view.Holder.Search.Similar;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;

public class SimilarSearchHeader extends RelativeLayout{

    private Activity activity;
    private Context context;

    public SimilarSearchHeader(Context context) {
        super(context);
        inflate(context, R.layout.similar_search_header,this);
        this.context = context;
    }
}

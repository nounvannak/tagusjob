package com.tagusnow.tagmejob.view.Holder.Search.Similar;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SimilarSearchHeaderHolder extends RecyclerView.ViewHolder{

    private SimilarSearchHeader header;

    public SimilarSearchHeaderHolder(View itemView) {
        super(itemView);
        this.header = (SimilarSearchHeader)itemView;
    }

    public void bindView(){

    }
}

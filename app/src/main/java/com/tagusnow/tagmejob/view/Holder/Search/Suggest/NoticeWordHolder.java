package com.tagusnow.tagmejob.view.Holder.Search.Suggest;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestWordsNotice;

public class NoticeWordHolder extends RecyclerView.ViewHolder{

    private Activity activity;
    private Context context;
    private SearchSuggestWordsNotice notice;
    private SmUser user;
    private String search_text;

    public NoticeWordHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();
        notice = (SearchSuggestWordsNotice)itemView;
    }

    public void bindView(Activity activity,String search_text){
        this.activity = activity;
        this.user = user;
        this.search_text = search_text;
        notice.setActivity(activity).setStrSearch_result(search_text);
    }

    public void bindView(Activity activity,String search_text,SmUser user,boolean isMainSearch){
        this.activity = activity;
        this.user = user;
        this.search_text = search_text;
        notice.setActivity(activity).setUser(user).isMainSearch(isMainSearch).setStrSearch_result(search_text);
    }
}

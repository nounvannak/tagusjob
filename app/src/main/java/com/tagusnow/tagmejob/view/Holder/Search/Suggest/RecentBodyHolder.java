package com.tagusnow.tagmejob.view.Holder.Search.Suggest;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestRecentFace;

public class RecentBodyHolder extends RecyclerView.ViewHolder{
    private Activity activity;
    private RecentSearch recentSearch;
    private SearchSuggestRecentFace face;

    public RecentBodyHolder(View itemView) {
        super(itemView);
        this.face = (SearchSuggestRecentFace)itemView;
    }

    public void bindView(Activity activity,RecentSearch recentSearch){
        this.activity = activity;
        this.recentSearch = recentSearch;
        this.face.Activity(activity).RecentSearch(recentSearch);
    }

    public void bindView(Activity activity,RecentSearch recentSearch,int type){
        this.activity = activity;
        this.recentSearch = recentSearch;
        this.face.Activity(activity).Type(type).RecentSearch(recentSearch);
    }
}

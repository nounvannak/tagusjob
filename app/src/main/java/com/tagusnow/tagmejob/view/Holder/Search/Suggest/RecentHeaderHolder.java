package com.tagusnow.tagmejob.view.Holder.Search.Suggest;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.search.SearchSuggestRecentHeaderListener;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestRecentHeader;

public class RecentHeaderHolder extends RecyclerView.ViewHolder{

    private SearchSuggestRecentHeader header;

    public RecentHeaderHolder(View itemView) {
        super(itemView);
        header = (SearchSuggestRecentHeader)itemView;
    }

    public void bindView(Activity activity, SearchSuggestRecentHeaderListener searchSuggestRecentHeaderListener){
        header.setActivity(activity);
        header.setSearchSuggestRecentHeaderListener(searchSuggestRecentHeaderListener);
    }

}

package com.tagusnow.tagmejob.view.Holder.Search.Suggest;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchSuggestResultFace;

import java.net.ConnectException;

public class ResultHolder extends RecyclerView.ViewHolder{
    private Activity activity;
    private Context context;
    private SearchSuggestResultFace face;
    private SmUser user;

    public ResultHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();
        face = (SearchSuggestResultFace)itemView;
    }

    public void bindView(@NonNull Activity activity,@NonNull SmUser user){
        this.activity = activity;
        this.user = user;
        this.face.Activity(activity).User(user);
    }
}

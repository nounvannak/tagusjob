package com.tagusnow.tagmejob.view.Holder.Search.Suggest;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Search.UserSuggest.SearchViewNoticeFace;

public class SearchViewNoticeHolder extends RecyclerView.ViewHolder{

    private SearchViewNoticeFace searchViewNoticeFace;

    public SearchViewNoticeHolder(View itemView) {
        super(itemView);
        searchViewNoticeFace = (SearchViewNoticeFace)itemView;
    }

    public void bindView(Activity activity,SmUser auth, SmUser user){
        searchViewNoticeFace.Activity(activity).Auth(auth).User(user);
    }
}

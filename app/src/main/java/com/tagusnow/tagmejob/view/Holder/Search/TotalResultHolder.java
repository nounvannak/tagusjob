package com.tagusnow.tagmejob.view.Holder.Search;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Search.TotalResult;

public class TotalResultHolder extends RecyclerView.ViewHolder{

    private TotalResult totalResult;

    public TotalResultHolder(View itemView) {
        super(itemView);
        totalResult = (TotalResult)itemView;
    }

    public void bindView(int total){
        totalResult.setTotal(total);
    }
}

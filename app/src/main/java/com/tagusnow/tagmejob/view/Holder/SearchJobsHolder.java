package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.SearchJobsFace;

public class SearchJobsHolder extends RecyclerView.ViewHolder{

    private SearchJobsFace face;

    public SearchJobsHolder(View itemView) {
        super(itemView);
        this.face = (SearchJobsFace)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.face.Activity(activity).Auth(user);
    }
}

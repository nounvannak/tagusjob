package com.tagusnow.tagmejob.view.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Space;

public class SpaceHolder extends RecyclerView.ViewHolder{

    private Space space;

    public SpaceHolder(View itemView) {
        super(itemView);
        this.space = (Space)itemView;
    }

    public void bindView(){}
}

package com.tagusnow.tagmejob.view.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.FollowerFace;

public class ViewFollowerHolder extends RecyclerView.ViewHolder{

    private FollowerFace face;

    public ViewFollowerHolder(View itemView) {
        super(itemView);
        face = (FollowerFace)itemView;
    }

    public void bindView(Activity activity,SmUser auth, SmUser user){
        face.Activity(activity).Auth(auth).User(user);
    }
}

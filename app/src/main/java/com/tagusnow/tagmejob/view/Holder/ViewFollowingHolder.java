package com.tagusnow.tagmejob.view.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.FollowerFace;

public class ViewFollowingHolder extends RecyclerView.ViewHolder{

    private FollowerFace face;

    public ViewFollowingHolder(View itemView) {
        super(itemView);
        face = (FollowerFace)itemView;
    }

    public void bindView(SmUser auth,SmUser user){
        face.Auth(auth).User(user);
    }
}

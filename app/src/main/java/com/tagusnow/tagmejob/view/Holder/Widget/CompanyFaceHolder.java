package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.page.resume.CompanyFaceListener;
import com.tagusnow.tagmejob.view.Widget.CompanyFace;

public class CompanyFaceHolder extends RecyclerView.ViewHolder {

    private CompanyFace companyFace;

    public CompanyFaceHolder(View itemView) {
        super(itemView);
        this.companyFace = (CompanyFace)itemView;
    }

    public void bindView(Activity activity, Feed feed, CompanyFaceListener companyFaceListener){
        this.companyFace.setActivity(activity);
        this.companyFace.setCompanyFaceListener(companyFaceListener);
        this.companyFace.setFeed(feed);
    }
}

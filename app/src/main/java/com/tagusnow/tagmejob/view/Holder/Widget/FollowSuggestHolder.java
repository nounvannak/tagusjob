package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.adapter.FollowSuggestHorizontalAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestFace;

public class FollowSuggestHolder extends RecyclerView.ViewHolder{

    private FollowSuggestFace face;

    public FollowSuggestHolder(View itemView) {
        super(itemView);
        this.face = (FollowSuggestFace)itemView;
    }

    public void bindView(Activity activity, SmUser auth,SmUser user,RecyclerView.Adapter adapter){
        this.face.setShowProfile(true);
        this.face.setShowCloseButton(true);
        this.face.setShowCounterFollowing(false);
        this.face.setShowCounterPost(false);
        this.face.setAdapterTag(FollowSuggestHorizontalAdapter.class.getSimpleName());
        this.face.setResponse(true);
        this.face.Activity(activity).Adapter(adapter).Position(getAdapterPosition()).Auth(auth).User(user);
    }
}

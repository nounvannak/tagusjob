package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.FollowSuggestHorizontalLayout;

public class FollowSuggestHorizontalHolder extends RecyclerView.ViewHolder{

    private FollowSuggestHorizontalLayout layout;

    public FollowSuggestHorizontalHolder(View itemView) {
        super(itemView);
        this.layout = (FollowSuggestHorizontalLayout)itemView;
    }

    public void bindView(Activity activity,SmUser user){
        this.layout.Auth(user).Activity(activity);
    }
}

package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.SearchByLocation;

public class SearchByLocationHolder extends RecyclerView.ViewHolder{

    private SearchByLocation location;

    public SearchByLocationHolder(View itemView) {
        super(itemView);
        this.location = (SearchByLocation)itemView;
    }

    public void bindView(Activity activity, SmUser user,int category){
        this.location.Activity(activity).Auth(user).Category(category);
    }
}

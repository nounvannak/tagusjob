package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.WidgetTopJobs;

public class TopJobsHolder extends RecyclerView.ViewHolder{

    private WidgetTopJobs widgetTopJobs;

    public TopJobsHolder(View itemView) {
        super(itemView);
        this.widgetTopJobs = (WidgetTopJobs)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.widgetTopJobs.Activity(activity).Auth(user);
    }
}

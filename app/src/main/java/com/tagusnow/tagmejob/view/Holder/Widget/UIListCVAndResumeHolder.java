package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.CV;
import com.tagusnow.tagmejob.view.Widget.UIListCVAndResume;

public class UIListCVAndResumeHolder extends RecyclerView.ViewHolder {

    private UIListCVAndResume resume;

    public UIListCVAndResumeHolder(View itemView) {
        super(itemView);
        this.resume = (UIListCVAndResume)itemView;
    }

    public void bindView(Activity activity, CV cv){
        this.resume.setActivity(activity);
        this.resume.setCv(cv);
    }
}

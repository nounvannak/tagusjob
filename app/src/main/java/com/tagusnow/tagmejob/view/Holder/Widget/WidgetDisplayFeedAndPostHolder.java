package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.WidgetDisplayFeedAndPost;

public class WidgetDisplayFeedAndPostHolder extends RecyclerView.ViewHolder{

    private WidgetDisplayFeedAndPost post;

    public WidgetDisplayFeedAndPostHolder(View itemView) {
        super(itemView);
        this.post = (WidgetDisplayFeedAndPost)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.post.Activity(activity).Auth(user);
    }

    public void bindView(Activity activity, SmUser user,RecyclerView.Adapter adapter,int count){
        this.post.Activity(activity).Auth(user).Adapter(adapter).Counter(count);
    }
}

package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.WidgetImagePost;

public class WidgetImagePostHolder extends RecyclerView.ViewHolder{

    private WidgetImagePost widgetImagePost;

    public WidgetImagePostHolder(View itemView) {
        super(itemView);
        this.widgetImagePost = (WidgetImagePost)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.widgetImagePost.Activity(activity).User(user);
    }
}

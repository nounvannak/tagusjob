package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.WidgetJobsCounter;

public class WidgetJobsCounterHolder extends RecyclerView.ViewHolder{

    private WidgetJobsCounter widgetJobsCounter;

    public WidgetJobsCounterHolder(View itemView) {
        super(itemView);
        this.widgetJobsCounter = (WidgetJobsCounter)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.widgetJobsCounter.Activity(activity).Auth(user);
    }
}

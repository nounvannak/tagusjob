package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.view.Widget.WidgetPost;

public class WidgetPostHolder extends RecyclerView.ViewHolder{

    private WidgetPost widgetPost;

    public WidgetPostHolder(View itemView) {
        super(itemView);
        this.widgetPost = (WidgetPost)itemView;
    }

    public void bindView(Activity activity){
        this.widgetPost.Activity(activity);
    }
}

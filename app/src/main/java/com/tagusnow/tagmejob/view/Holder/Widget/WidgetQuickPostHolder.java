package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.WidgetQuickPost;

public class WidgetQuickPostHolder extends RecyclerView.ViewHolder{

    private WidgetQuickPost widgetQuickPost;

    public WidgetQuickPostHolder(View itemView) {
        super(itemView);
        this.widgetQuickPost = (WidgetQuickPost)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.widgetQuickPost.Activity(activity).Auth(user);
    }
}

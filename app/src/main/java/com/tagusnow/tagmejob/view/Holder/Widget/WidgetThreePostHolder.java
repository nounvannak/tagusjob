package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Widget.WidgetThreePost;

public class WidgetThreePostHolder extends RecyclerView.ViewHolder{

    private WidgetThreePost post;

    public WidgetThreePostHolder(View itemView) {
        super(itemView);
        this.post = (WidgetThreePost)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.post.Activity(activity).Auth(user);
    }
}

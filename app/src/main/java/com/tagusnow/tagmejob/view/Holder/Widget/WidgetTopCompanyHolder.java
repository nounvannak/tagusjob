package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.page.resume.CompanyFaceListener;
import com.tagusnow.tagmejob.view.Widget.WidgetTopCompany;

public class WidgetTopCompanyHolder extends RecyclerView.ViewHolder {

    private WidgetTopCompany company;

    public WidgetTopCompanyHolder(View itemView) {
        super(itemView);
        this.company = (WidgetTopCompany)itemView;
    }

    public void BindView(Activity activity, CompanyFaceListener companyFaceListener){
        company.setActivity(activity);
        company.setCompanyFaceListener(companyFaceListener);
    }
}

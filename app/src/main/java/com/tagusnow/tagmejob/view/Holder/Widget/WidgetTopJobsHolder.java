package com.tagusnow.tagmejob.view.Holder.Widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.view.Widget.WidgetTopJobsFace;

public class WidgetTopJobsHolder extends RecyclerView.ViewHolder{

    private WidgetTopJobsFace face;

    public WidgetTopJobsHolder(View itemView) {
        super(itemView);
        this.face = (WidgetTopJobsFace)itemView;
    }

    public void bindView(Activity activity, Category category, SmUser auth){
        this.face.Activity(activity).Category(category).Auth(auth);
    }
}

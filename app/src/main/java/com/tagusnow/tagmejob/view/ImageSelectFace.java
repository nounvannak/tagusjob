package com.tagusnow.tagmejob.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.Post.LayoutSelectImageHandler;

public class ImageSelectFace extends RelativeLayout implements View.OnClickListener {

    private Context context;
    private ImageButton remove;
    private ImageView image;
    private Button file_extension;
    private String file;
    private RecyclerView.Adapter adapter;
    private int position;

    public void setLayoutSelectImageHandler(LayoutSelectImageHandler layoutSelectImageHandler) {
        this.layoutSelectImageHandler = layoutSelectImageHandler;
    }

    private LayoutSelectImageHandler layoutSelectImageHandler;

    public ImageSelectFace(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.image_select_face,this);
        remove = (ImageButton)findViewById(R.id.remove);
        image = (ImageView)findViewById(R.id.image);
        file_extension = (Button)findViewById(R.id.file_extension);
        remove.setOnClickListener(this);
    }

    public ImageSelectFace Adapter(RecyclerView.Adapter adapter,int position){
        this.adapter = adapter;
        this.position = position;
        return this;
    }

    @Override
    public void onClick(View view) {
        if (view==remove){
            this.layoutSelectImageHandler.remove(this,this.adapter,this.position);
        }
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
        if (file!=null){
            Glide.with(this.context)
                    .load(file)
                    .centerCrop()
                    .error(R.drawable.ic_filter_vintage_grey_600_24dp)
                    .into(image);
            String extension = file.substring(file.lastIndexOf("."));
            file_extension.setText(extension.substring(1));
        }
    }
}

package com.tagusnow.tagmejob.view.Job;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.ApplyJobActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.model.v2.feed.ApplyJob;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApplyJobFace extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = ApplyJobFace.class.getSimpleName();
    private Context context;
    private SmUser user;
    private Feed feed;
    private AlertDialog dialog;
    private EditText txtName,txtEmail,txtSubject,txtDescription,file_cv,file_cover_letter;
    private Button btnSend;
    private File mFileCV;
    private File mFileCoverLetter;
    private String error_msg;
    private ApplyJob applyJob;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private JobsService service = retrofit.create(JobsService.class);
    private Callback<ApplyJob> mSave = new Callback<ApplyJob>() {
        @Override
        public void onResponse(@NonNull Call<ApplyJob> call, Response<ApplyJob> response) {
            if (response.isSuccessful()){
                ((ApplyJobActivity) activity).dismissProgress();
                setApplyJob(response.body());
                ((ApplyJobActivity) activity).onBackPressed();
            }else {
                try {
                    ((ApplyJobActivity) activity).dismissProgress();
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(@NonNull Call<ApplyJob> call, Throwable t) {
            ((ApplyJobActivity) activity).ShowAlert("Error",t.getMessage());
            t.printStackTrace();
        }
    };

    public EditText getFile_cover_letter() {
        return file_cover_letter;
    }

    public EditText getFile_cv() {
        return file_cv;
    }

    private void initUI(Context context){
        inflate(context, R.layout.layout_apply_job,this);
        this.context = context;
        btnSend  = (Button)findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

        txtName = (EditText)findViewById(R.id.txtName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtSubject = (EditText)findViewById(R.id.txtSubject);
        txtDescription = (EditText)findViewById(R.id.txtDescription);
        file_cv = (EditText)findViewById(R.id.file_cv);
        file_cover_letter = (EditText)findViewById(R.id.file_cover_letter);
    }


    private void initData(){
        if (this.user!=null){
            txtName.setText(this.user.getUser_name());
            if (this.user.getEmail()!=null){
                txtEmail.setText(this.user.getEmail());
            }else {
                Log.e(TAG,"No Email");
            }
        }else {
            Log.e(TAG,"No User");
        }

        if (this.feed!=null){
            txtSubject.setText(this.activity.getString(R.string.apply_for_,this.feed.getFeed().getTitle()));
            txtDescription.setText(R.string.apply_desc);
        }else {
            Log.e(TAG,"No Feed");
        }

        if (mFileCV!=null){
            file_cv.setText(mFileCV.getName());
        }

        if (mFileCoverLetter!=null){
            file_cover_letter.setText(mFileCoverLetter.getName());
        }
    }

    public ApplyJobFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public ApplyJobFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public ApplyJobFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public ApplyJobFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public SmUser getUser() {
        return user;
    }

    public void setUser(SmUser user) {
        this.user = user;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initData();
    }

    @Override
    public void onClick(View view) {
        if (view==btnSend){
            this.OnSend();
        }
    }

    private boolean isValid(){
        if (txtName.getText().toString().equals("")) {
            this.error_msg = "Field Name invalid.";
            return false;
        }else if (txtEmail.getText().toString().equals("")){
            this.error_msg = "Field Email invalid.";
            return false;
        }else if (txtDescription.getText().toString().equals("")){
            this.error_msg = "Field Description invalid.";
            return false;
        }else if (txtSubject.getText().toString().equals("")) {
            this.error_msg = "Field Subject invalid.";
            return false;
        }else if (this.mFileCV==null){
            this.error_msg = "Field CV invalid.";
            return false;
        }else if (this.mFileCoverLetter==null){
            this.error_msg = "Field Cover Letter invalid.";
            return false;
        }else {
            return true;
        }
    }

    private void OnSend(){
        if (this.isValid()){
            ((ApplyJobActivity) activity).showProgress();
            this.OnSubmit();
        }else {
            new AlertDialog.Builder(this.context).setTitle("Form invalid").setMessage(this.error_msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
        }
    }

    private void OnSubmit(){
        RequestBody feed_id = createPartFromString(String.valueOf(this.feed.getId()));
        RequestBody user_id = createPartFromString(String.valueOf(this.user.getId()));
        RequestBody name    = createPartFromString(this.txtName.getText().toString());
        RequestBody email   = createPartFromString(this.txtEmail.getText().toString());
        RequestBody subject = createPartFromString(this.txtSubject.getText().toString());
        RequestBody desc    = createPartFromString(this.txtDescription.getText().toString());
        MultipartBody.Part cv = null,cover_letter = null;
        if (this.mFileCV!=null){
            cv = prepareFilePart("cv",this.mFileCV);
        }else {
            Toast.makeText(this.context,"CV NULL",Toast.LENGTH_SHORT).show();
        }
        if (this.mFileCoverLetter!=null){
            cover_letter = prepareFilePart("cover_letter",this.mFileCoverLetter);
        }else {
            Toast.makeText(this.context,"Cover letter NULL",Toast.LENGTH_SHORT).show();
        }
        service.ApplyJob(feed_id,user_id,name,email,subject,desc,cv,cover_letter).enqueue(mSave);
    }

    public AlertDialog getDialog() {
        return dialog;
    }

    public void setDialog(AlertDialog dialog) {
        this.dialog = dialog;
    }

    public ApplyJob getApplyJob() {
        return applyJob;
    }

    public void setApplyJob(ApplyJob applyJob) {
        this.applyJob = applyJob;
    }

    public void setFileCV(File mFileCV) {
        this.mFileCV = mFileCV;
        if (this.mFileCV !=null){
            file_cv.setText(this.mFileCV.getName());
        }
    }

    public void setFileCoverLetter(File mFileCoverLetter) {
        this.mFileCoverLetter = mFileCoverLetter;
        if (this.mFileCoverLetter!=null){
            file_cover_letter.setText(this.mFileCoverLetter.getName());
        }
    }

    private File generateFile(File myFile){
        String path = myFile.getPath();
        String uriString = Uri.fromFile(myFile).toString();
        String displayName = null;
        Log.w(TAG,uriString);
        if (uriString.startsWith("content:/")) {
            Cursor cursor = null;
            try {
                cursor = this.activity.getContentResolver().query(Uri.fromFile(myFile), null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    String newPath = path.replace(myFile.getName(),displayName);
                    Log.w(TAG,newPath);
                    myFile = new File(newPath);
                }
            } finally {
                cursor.close();
            }
        }
        return myFile;
    }

    public File getFileCoverLetter() {
        return mFileCoverLetter;
    }

    public File getFileCV() {
        return mFileCV;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}

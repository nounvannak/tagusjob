package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;

public class JobDetailView extends TagUsJobRelativeLayout implements View.OnClickListener, OnMapReadyCallback {

    public void setJobDetailViewAction(JobDetailViewAction jobDetailViewAction) {
        this.jobDetailViewAction = jobDetailViewAction;
    }

    JobDetailViewAction jobDetailViewAction;
    private static final String TAG = JobDetailView.class.getSimpleName();
    private Context context;
    private Feed feed;
    private ImageView logo;
    private Button btnApply;
    private TextView address,txtTitle,txtLocation,txtCloseDate,txtHiring,txtQualification,txtWorkingTime,txtContact,txtEmail,txtWebsite,txtSalary,txtPosition,txtDescription,txtRequirement;

    private void init(Context context){
        inflate(context, R.layout.job_detail_layout,this);
        this.context = context;
        logo = (ImageView)findViewById(R.id.logo);
        txtTitle = (TextView)findViewById(R.id.txtTitle);
        address = (TextView)findViewById(R.id.address);
        txtLocation = (TextView)findViewById(R.id.txtLocation);
        txtCloseDate = (TextView)findViewById(R.id.txtCloseDate);
        txtHiring = (TextView)findViewById(R.id.txtHiring);
        txtQualification = (TextView)findViewById(R.id.txtQualification);
        txtWorkingTime = (TextView)findViewById(R.id.txtWorkingTime);
        txtPosition = (TextView)findViewById(R.id.txtPosition);
        txtSalary = (TextView)findViewById(R.id.txtSalary);
        txtDescription = (TextView)findViewById(R.id.txtDescription);
        txtRequirement = (TextView)findViewById(R.id.txtRequirement);
        txtContact = (TextView)findViewById(R.id.txtContactNumber);
        txtEmail = (TextView)findViewById(R.id.txtEmail);
        txtWebsite = (TextView)findViewById(R.id.txtWebsite);
        btnApply = (Button)findViewById(R.id.btnApply);
        btnApply.setOnClickListener(this);
    }

    public JobDetailView(Context context) {
        super(context);
        init(context);
    }

    public JobDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public JobDetailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public JobDetailView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public JobDetailView Feed(Feed feed){
        this.feed = feed;
        SupportMapFragment mapFragment =
                (SupportMapFragment) ((TagUsJobActivity) activity).getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        this.initView(feed);
        return this;
    }

    private void initView(Feed feed){
        if (feed!=null){
            if (feed.getType().contains(Feed.TAGUSJOB)){
                if (feed.getFeed().getType().equals(FeedDetail.JOB_POST)){
                    /*Title*/
                    txtTitle.setText(feed.getFeed().getTitle());
                    txtLocation.setText(feed.getFeed().getCity_name());
                    txtCloseDate.setText(feed.getFeed().getCloseDate());
                    txtEmail.setText(feed.getFeed().getEmail());
                    txtPosition.setText(feed.getFeed().getCategory());

                    if (feed.getFeed().getNum_employee()!=null && !feed.getFeed().getNum_employee().equals("")){
                        txtHiring.setText(feed.getFeed().getNum_employee());
                    }else  {
                        txtHiring.setText("N/A");
                    }

                    if (feed.getFeed().getSalary()!=null && !feed.getFeed().getSalary().equals("")){
                        txtSalary.setText(feed.getFeed().getSalary());
                    }else{
                        txtSalary.setText("N/A");
                    }

                    if (feed.getFeed().getPhone()!=null && !feed.getFeed().getPhone().equals("")){
                        txtContact.setText(feed.getFeed().getPhone());
                    }else{
                        txtContact.setText("N/A");
                    }

                    if (feed.getFeed().getWebsite()!=null && !feed.getFeed().getWebsite().equals("")){
                        txtWebsite.setText(feed.getFeed().getWebsite());
                    }else{
                        txtWebsite.setText(SuperUtil.getBaseUrl(feed.getUser_post().getSlug()));
                    }

                    if (feed.getFeed().getLevel()!=null && !feed.getFeed().getLevel().equals("")){
                        txtQualification.setText(feed.getFeed().getLevel());
                    }else {
                        txtQualification.setText("N/A");
                    }

                    if (feed.getFeed().getWorking_time()!=null && !feed.getFeed().getWorking_time().equals("")){
                        txtWorkingTime.setText(feed.getFeed().getWorking_time());
                    }else {
                        txtWorkingTime.setText("N/A");
                    }

                    if (feed.getFeed().getDescription()!=null && !feed.getFeed().getDescription().equals("")){
                        txtDescription.setText(feed.getFeed().getDescription());
                    }else {
                        txtDescription.setText("N/A");
                    }

                    if (feed.getFeed().getRequirement()!=null && !feed.getFeed().getRequirement().equals("")){
                        txtRequirement.setText(feed.getFeed().getRequirement());
                    }else {
                        txtRequirement.setText("N/A");
                    }

                    if (feed.getFeed().getLocation_des()!= null && !feed.getFeed().getLocation_des().equals("")){
                        address.setText(feed.getFeed().getLocation_des());
                    }else {
                        address.setVisibility(GONE);
                    }

                    if (feed.getFeed().getAlbum()!=null && feed.getFeed().getAlbum().size() > 0 ){
                        if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                            Glide.with(this.context)
                                    .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                                    .fitCenter()
                                    .error(R.drawable.not_available_)
                                    .into(logo);
                        }else {
                            Glide.with(this.context)
                                    .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                    .fitCenter()
                                    .error(R.drawable.not_available_)
                                    .into(logo);
                        }
                    }else {
                        Glide.with(this.context)
                                .load(R.drawable.not_available_)
                                .fitCenter()
                                .error(R.drawable.not_available_)
                                .into(logo);
                    }

                }
            }

        }else {
            Log.e(TAG,"No Feed");
        }
    }

    @Override
    public void onClick(View v) {
        if (v==btnApply){
            this.jobDetailViewAction.onApply(this.feed);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        String title = feed.getFeed().getTitle_name();

//        if (feed.getFeed().getLocation_des()!=null && !feed.getFeed().getLocation_des().equals("")){
//            title += "\n"+feed.getFeed().getLocation_des();
//        }

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(this.feed.getFeed().getLat(), feed.getFeed().getLng()))
                .title(title));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.feed.getFeed().getLat(), feed.getFeed().getLng()), feed.getFeed().getZoom()));
    }
}

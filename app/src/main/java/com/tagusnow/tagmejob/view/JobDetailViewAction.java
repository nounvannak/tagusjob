package com.tagusnow.tagmejob.view;

import com.tagusnow.tagmejob.feed.Feed;

public interface JobDetailViewAction {
    void onApply(Feed feed);
}

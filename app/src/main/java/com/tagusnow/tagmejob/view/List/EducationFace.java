package com.tagusnow.tagmejob.view.List;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.model.v2.feed.Education;

public class EducationFace extends RelativeLayout {

    private static final String TAG = EducationFace.class.getSimpleName();
    private Context context;
    private TextView period,school,subject;
    private Education education;

    private void initUI(Context context){
        inflate(context, R.layout.layout_education_face,this);
        this.context = context;
        period = (TextView)findViewById(R.id.period);
        school = (TextView)findViewById(R.id.school);
        subject = (TextView)findViewById(R.id.subject);
    }

    public EducationFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public EducationFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public EducationFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public EducationFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public Education getEducation() {
        return education;
    }

    private void initEducation(Education education){
        if (education!=null){
            String year_from[] = education.getFrom().split("-");
            String year_to[] = education.getTo().split("-");
            period.setText(year_from[0]+" - "+year_to[0]);
            school.setText(education.getSchool());
            subject.setText(education.getDegree()+" - "+education.getField_study());
        }
    }

    public EducationFace setEducation(Education education) {
        this.education = education;
        this.initEducation(education);
        return this;
    }
}

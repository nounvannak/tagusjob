package com.tagusnow.tagmejob.view.List;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.model.v2.feed.Experience;

public class ExperienceFace extends RelativeLayout {

    private static final String TAG = ExperienceFace.class.getSimpleName();
    private Context context;
    private TextView period,company,position,responsibility;
    private Experience experience;

    private void initUI(Context context){
        inflate(context, R.layout.layout_experience_face,this);
        this.context = context;
        period = (TextView)findViewById(R.id.period);
        company = (TextView)findViewById(R.id.company);
        position = (TextView)findViewById(R.id.position);
        responsibility = (TextView)findViewById(R.id.responsibility);
    }

    public ExperienceFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public ExperienceFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public ExperienceFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public ExperienceFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public Experience getExperience() {
        return experience;
    }

    private void initExperience(Experience experience){
        if (experience!=null){
            String year_from[] = experience.getFrom().split("-");
            String year_to[] = experience.getTo().split("-");
            period.setText(year_from[0]+" - "+year_to[0]);
            company.setText(experience.getCompany());
            position.setText(experience.getName());
            responsibility.setText(experience.getHeadline());
        }
    }

    public ExperienceFace setExperience(Experience experience) {
        this.experience = experience;
        this.initExperience(experience);
        return this;
    }
}

package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.v3.empty.LoadingListener;

public class LoadingHolder extends RecyclerView.ViewHolder {

    private LoadingView loadingView;

    public LoadingHolder(View itemView) {
        super(itemView);
        this.loadingView = (LoadingView)itemView;
    }

    public void BindView(Activity activity, LoadingListener<LoadingView> loadingListener){
        loadingView.setActivity(activity);
        loadingView.setLoadingListener(loadingListener);
    }
}

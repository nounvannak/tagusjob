package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.v3.empty.LoadingListener;

public class LoadingView extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ProgressBar progressBar;
    private ImageView icon;
    private TextView textView;
    private boolean isError = false;
    private boolean isLoading = false;
    private LoadingListener<LoadingView> loadingListener;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void initUI(Context context){
        inflate(context, R.layout.layout_loading,this);

        this.context = context;

        progressBar = (ProgressBar)findViewById(R.id.progress);
        icon = (ImageView)findViewById(R.id.icon);
        textView = (TextView)findViewById(R.id.text);

        icon.setOnClickListener(this);
    }

    public LoadingView(Context context) {
        super(context);
        this.initUI(context);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    @Override
    public void onClick(View v) {
        if (v == icon){
            if (isError){
                loadingListener.reload(this);
            }
        }
    }

    public void setError(boolean error) {
        isError = error;
        if (error){
            progressBar.setVisibility(GONE);
            icon.setVisibility(VISIBLE);
            icon.setBackgroundResource(R.drawable.refresh_black);
            textView.setText("Network error.");
            icon.setAnimation(null);
        }
    }

    public void setLoadingListener(LoadingListener<LoadingView> loadingListener) {
        this.loadingListener = loadingListener;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
        if (!isError){
            if (loading){
                progressBar.setVisibility(VISIBLE);
                icon.setVisibility(GONE);
                textView.setText("Loading...");
            }else {
                progressBar.setVisibility(GONE);
                icon.setVisibility(VISIBLE);
                icon.setBackgroundResource(R.drawable.image_black);
                textView.setText("No more data.");
            }
        }
    }

}

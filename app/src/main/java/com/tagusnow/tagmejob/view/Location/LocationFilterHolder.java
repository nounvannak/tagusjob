package com.tagusnow.tagmejob.view.Location;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class LocationFilterHolder extends RecyclerView.ViewHolder {

    private LocationFilterLayout layout;

    public LocationFilterHolder(View itemView) {
        super(itemView);
        this.layout = (LocationFilterLayout)itemView;
    }

    public void BindView(Activity activity,int city){
        this.layout.setActivity(activity);
        this.layout.setCity(city);
    }
}

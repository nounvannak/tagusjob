package com.tagusnow.tagmejob.view.Location;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SearchActivity;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import java.util.List;

public class LocationFilterLayout extends TagUsJobRelativeLayout {

    private static final String TAG = LocationFilterLayout.class.getSimpleName();
    private RecyclerView recyclerView;
    private Context context;
    private int city;
    private SmLocation location;
    private Adapter adapter;

    private void InitUI(Context context){
        inflate(context, R.layout.location_filter_layout,this);
        this.context = context;

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        this.location = new Repository(context).restore().getLocation();

    }

    public LocationFilterLayout(Context context) {
        super(context);
        InitUI(context);
    }

    public LocationFilterLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public LocationFilterLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public LocationFilterLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);

        if (this.location!=null){
            this.adapter = new Adapter(this.activity,this.location,city);
            recyclerView.setAdapter(this.adapter);
        }
    }

    public class Adapter extends RecyclerView.Adapter{

        private Activity activity;
        private Context context;
        private SmLocation location;
        private int city;
        private List<SmLocation.Data> data;

        public Adapter(Activity activity,SmLocation location,int city){
            this.activity = activity;
            this.context = activity;
            this.city = city;
            this.location = location;
            this.data = location.getData();
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new Holder(new Button(this.context));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (position==0){
                ((Holder) holder).BindView(this.activity,this,this.data.get(position),position,this.city);
            }else{
                ((Holder) holder).BindView(this.activity,this,this.data.get(position - 1),position,this.city);
            }
        }

        @Override
        public int getItemCount() {
            return this.data.size() + 1;
        }
    }

    public class Holder extends RecyclerView.ViewHolder{

        private Button button;

        public Holder(View itemView) {
            super(itemView);
            this.button = (Button)itemView;
        }

        public void BindView(Activity activity,Adapter adapter,SmLocation.Data location,int position,int city){
            if (position==0){
                this.button.setTag(0);
                this.button.setText("All");
                this.button.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,100));
                if (position==city){
                    this.button.setTextColor(Color.WHITE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        this.button.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.colorAccent)));
                    }
                }else {
                    this.button.setTextColor(Color.BLACK);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        this.button.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
                    }
                }
            }else {
                this.button.setTag(location.getId());
                this.button.setText(location.getName());
                this.button.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,100));
                Log.e("position!=0",String.valueOf(location.getId()==city));
                if (location.getId()==city){
                    this.button.setTextColor(Color.WHITE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        this.button.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.colorAccent)));
                    }
                }else {
                    this.button.setTextColor(Color.BLACK);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        this.button.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
                    }
                }
            }

            this.button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.notifyDataSetChanged();
//                    ((SearchActivity) activity).Reload((int)button.getTag());
                }
            });
        }
    }
}

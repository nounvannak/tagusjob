package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SeeAllSimilarSearchActivity;
import com.tagusnow.tagmejob.auth.SmUser;

public class NoticeMainSearchResultHeader extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = NoticeMainSearchResultHeader.class.getSimpleName();
    public static final int PEOPLE = 0;
    public static final int JOB = 1;
    public static final int POST = 2;
    public static final int LOCATION = 3;
    private Activity activity;
    private Context context;
    private String query;
    private ImageView icon;
    private TextView title;
    private ImageButton btnSeeAll;
    private int type;
    private SmUser user;

    public NoticeMainSearchResultHeader(Context context) {
        super(context);
        inflate(context, R.layout.notice_main_search_result_header,this);
        this.context = context;
        icon = (ImageView)findViewById(R.id.icon);
        title = (TextView)findViewById(R.id.title);
        btnSeeAll = (ImageButton)findViewById(R.id.btnSeeAll);
        btnSeeAll.setOnClickListener(this);
    }

    public NoticeMainSearchResultHeader setType(int type) {
        this.type = type;
        if (this.type==PEOPLE){
            Glide.with(this.context)
                    .load(R.drawable.ic_user_id_icon)
                    .fitCenter()
                    .into(icon);
            title.setText(R.string.people);
        }else if (this.type==JOB) {
            Glide.with(this.context)
                    .load(R.drawable.ic_bullhorn_icon)
                    .fitCenter()
                    .into(icon);
            title.setText(R.string.jobs);
        }else if (this.type==LOCATION){
            Glide.with(this.context)
                    .load(R.drawable.ic_map_marker_icon)
                    .fitCenter()
                    .into(icon);
            title.setText(R.string.location);
        }else {
            Glide.with(this.context)
                    .load(R.drawable.ic_heart_icon)
                    .fitCenter()
                    .into(icon);
            title.setText(R.string.post_);
        }
        return this;
    }

    public NoticeMainSearchResultHeader Auth(SmUser user){
        this.user = user;
        return this;
    }

    public NoticeMainSearchResultHeader Query(String query){
        this.query = query;
        return this;
    }

    public NoticeMainSearchResultHeader Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    private void seeAll(){
        if (this.activity!=null){
            this.activity.startActivity(SeeAllSimilarSearchActivity
                    .createIntent(this.activity)
                    .putExtra(SeeAllSimilarSearchActivity.TYPE,this.type)
                    .putExtra(SeeAllSimilarSearchActivity.QUERY,this.query)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnSeeAll){
            seeAll();
        }
    }
}

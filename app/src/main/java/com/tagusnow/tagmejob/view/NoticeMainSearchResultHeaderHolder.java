package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;

public class NoticeMainSearchResultHeaderHolder extends RecyclerView.ViewHolder{

    private NoticeMainSearchResultHeader header;

    public NoticeMainSearchResultHeaderHolder(View itemView) {
        super(itemView);
        this.header = (NoticeMainSearchResultHeader)itemView;
    }

    public void bindView(Activity activity, SmUser user, int type, String query){
        this.header.Activity(activity).Auth(user).setType(type).Query(query);
    }
}

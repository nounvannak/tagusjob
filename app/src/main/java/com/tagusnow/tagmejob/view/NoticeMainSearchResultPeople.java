package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.adapter.NoticeMainSearchResultPeopleAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.feed.RecentSearch;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NoticeMainSearchResultPeople extends RelativeLayout{

    private static final String TAG = NoticeMainSearchResultPeople.class.getSimpleName();
    private Activity activity;
    private Context context;
    private RecyclerView recyclerView;
    private String query;
    private UserPaginate userPaginate;
    private SmUser user;
    private int limit = 3;
    private int total;
    private NoticeMainSearchResultPeopleAdapter adapter;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService service = retrofit.create(FollowService.class);
    private Callback<UserPaginate> mBack = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initSearchSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void initSearchSuggest(UserPaginate userPaginate){
        this.total = userPaginate.getTotal();
        if (userPaginate.getTotal() > 0){
            userPaginate.setTo(userPaginate.getTo() + NoticeMainSearchResultPeopleAdapter.MORE_VIEW);
            this.userPaginate = userPaginate;
            this.adapter = new NoticeMainSearchResultPeopleAdapter(this.activity,this.userPaginate,this.user,this.query);
            recyclerView.setAdapter(this.adapter);
        }
    }

    private void hasSearchSuggest(){
        int user = this.user!=null ? this.user.getId() : 0;
        service.hasSearchSuggest(user,this.query,this.limit).enqueue(mBack);
    }

    public NoticeMainSearchResultPeople Query(String query){
        this.query = query;
        if (this.query!=null){
            hasSearchSuggest();
        }
        return this;
    }

    public NoticeMainSearchResultPeople Limit(int limit){
        this.limit = limit;
        return this;
    }

    public NoticeMainSearchResultPeople(Context context) {
        super(context);
        inflate(context, R.layout.notice_main_search_result_people,this);
        this.context = context;
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.context);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public NoticeMainSearchResultPeople Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public NoticeMainSearchResultPeople Auth(SmUser user){
        this.user = user;
        return this;
    }

    public int getTotal() {
        return total;
    }
}

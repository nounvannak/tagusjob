package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.adapter.NoticeMainSearchResultJobAdapter;
import com.tagusnow.tagmejob.adapter.NoticeMainSearchResultPostAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NoticeMainSearchResultPost extends RelativeLayout{

    private static final String TAG = NoticeMainSearchResultPost.class.getSimpleName();
    private Activity activity;
    private Context context;
    private SmUser user;
    private FeedResponse feedResponse;
    private String query;
    private RecyclerView recyclerView;
    private int limit = 3;
    private NoticeMainSearchResultPostAdapter adapter;
    private int total;
    /*Service*/
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    private JobsService service = retrofit.create(JobsService.class);
    private Callback<FeedResponse> mBack = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initJob(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void initJob(FeedResponse feedResponse){
        this.total = feedResponse.getTotal();
        if (feedResponse.getTotal() > 0){
            feedResponse.setTo(feedResponse.getTo() + NoticeMainSearchResultPostAdapter.MORE_VIEW);
            this.feedResponse = feedResponse;
            this.adapter = new NoticeMainSearchResultPostAdapter(this.activity,this.feedResponse,this.user,this.query);
            recyclerView.setAdapter(this.adapter);
        }
    }

    private void fastSearch(){
        int user = this.user!=null ? this.user.getId() : 0;
        int type = FeedDetail.POST;
        service.fastSearch(user,type,this.query,this.limit).enqueue(mBack);
    }

    public NoticeMainSearchResultPost(Context context) {
        super(context);
        inflate(context, R.layout.notice_main_search_result_post,this);
        this.context = context;
        recyclerView =  (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.context);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public NoticeMainSearchResultPost Limit(int limit){
        this.limit = limit;
        return this;
    }

    public NoticeMainSearchResultPost Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public NoticeMainSearchResultPost Auth(SmUser user){
        this.user = user;
        return this;
    }

    public NoticeMainSearchResultPost Query(String query){
        this.query = query;
        this.fastSearch();
        return this;
    }

    public int getTotal() {
        return total;
    }
}

package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.JobDetail;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewCardCVActivity;
import com.tagusnow.tagmejob.ViewDetailPostActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.Notification;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationFace extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = NotificationFace.class.getSimpleName();
    private Activity activity;
    private Context context;
    private CircleImageView user_profile;
    private TextView notification_body,timeline;
    private Notification notification;
    private SmUser user;

    public NotificationFace(Context context) {
        super(context);
        inflate(context, R.layout.notification_face,this);
        this.context = context;
        user_profile = (CircleImageView)findViewById(R.id.user_profile);
        notification_body = (TextView)findViewById(R.id.notification_body);
        timeline = (TextView)findViewById(R.id.timeline);
        this.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void Notification(Notification notification){
        this.notification = notification;
        if (notification!=null){
            notification_body.setText(this.notification.getBody());
            timeline.setText(this.notification.getTimeline());
            if (!this.notification.isSeen()){
                this.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    timeline.setTextColor(context.getColor(R.color.colorIcon));
                }
            }else {
                this.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    timeline.setTextColor(context.getColor(R.color.colorIcon));
                }
            }

            if (this.notification.getType().equals("system")){
                Glide.with(context).load(R.mipmap.ic_launcher_circle).into(user_profile);
            }else{
                if (this.notification.getTarget_user().getImage()!=null){
                    if (this.notification.getTarget_user().getImage().contains("http")){
                        Glide.with(context)
                                .load(this.notification.getTarget_user().getImage())
                                .error(R.mipmap.ic_launcher_circle)
                                .into(user_profile);
                    }else{
                        Glide.with(context)
                                .load(SuperUtil.getProfilePicture(this.notification.getTarget_user().getImage(),"100"))
                                .error(R.mipmap.ic_launcher_circle)
                                .into(user_profile);
                    }
                }else {
                    Glide.with(context)
                            .load(R.mipmap.ic_launcher_circle)
                            .into(user_profile);
                }
            }
        }else {
            Toast.makeText(this.context,"No Notification",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewNotification(){
        switch (notification.getType()) {
            case Notification.LIKE_POST:
                if (notification.getFeed() != null) {
                    viewLike();
                } else {
                    Log.w(TAG, "feed null");
                }
                break;
            case Notification.COMMENT_POST:
                viewComment();
                break;
            case Notification.LIKE_USER:
                viewProfile();
                break;
            case Notification.FOLLOW_USER:
                viewProfile();
                break;
            default:
                viewSystemNotification();
                break;
        }
    }

    private void viewSystemNotification(){}

    private void viewProfile(){
        context.startActivity(ViewProfileActivity.createIntent(activity).putExtra("profileUser",new Gson().toJson(notification.getTarget_user())).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private void viewComment(){
        context.startActivity(CommentActivity.createIntent(activity).putExtra("feed_id",notification.getFeed().getId()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private void viewLike(){
        if (this.notification.getFeed().getFeed().getType().equals(FeedDetail.NORMAL_POST)){
            context.startActivity(ViewDetailPostActivity
                    .createIntent(this.activity)
                    .putExtra(ViewDetailPostActivity.FEED,new Gson().toJson(this.notification.getFeed()))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (this.notification.getFeed().getFeed().getType().equals(FeedDetail.STAFF_POST)){
            context.startActivity(ViewCardCVActivity
                    .createIntent(this.activity)
                    .putExtra(ViewCardCVActivity.FEED,this.notification.getFeed())
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            context.startActivity(JobDetail.createIntent(this.activity)
                    .putExtra(JobDetail.FEED,this.notification.getFeed())
                    .putExtra(JobDetail.USER,this.user.getId())
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            viewNotification();
        }
    }

    public void setUser(SmUser user) {
        this.user = user;
    }
}

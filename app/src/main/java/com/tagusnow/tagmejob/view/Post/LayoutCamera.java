package com.tagusnow.tagmejob.view.Post;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class LayoutCamera extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;

    public void setHandler(LayoutCameraHandler handler) {
        this.handler = handler;
    }

    private LayoutCameraHandler handler;
    private static final int requestCode = 93;

    private void InitUI(Context context){
        inflate(context, R.layout.layout_camera,this);
        this.context = context;

        this.setOnClickListener(this);
    }

    public LayoutCamera(Context context) {
        super(context);
        this.InitUI(context);
    }

    public LayoutCamera(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public LayoutCamera(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public LayoutCamera(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v==this){
            this.handler.openCamera(this,requestCode);
        }
    }
}

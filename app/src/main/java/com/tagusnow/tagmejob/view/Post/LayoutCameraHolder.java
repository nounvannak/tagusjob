package com.tagusnow.tagmejob.view.Post;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class LayoutCameraHolder extends RecyclerView.ViewHolder {

    private LayoutCamera layoutCamera;

    public LayoutCameraHolder(View itemView) {
        super(itemView);
        layoutCamera = (LayoutCamera) itemView;
    }

    public void bindView(LayoutCameraHandler handler){
        layoutCamera.setHandler(handler);
    }
}

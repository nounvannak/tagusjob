package com.tagusnow.tagmejob.view.Post;

import android.support.v7.widget.RecyclerView;

import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.view.ImageSelectFace;

import java.util.List;

public interface LayoutSelectImageHandler {
    void remove(ImageSelectFace imageSelectFace, RecyclerView.Adapter adapter, int position);
    void selected(List<ImageItem> items);
}

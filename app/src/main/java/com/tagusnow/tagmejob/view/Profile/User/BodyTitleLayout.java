package com.tagusnow.tagmejob.view.Profile.User;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;

public class BodyTitleLayout extends RelativeLayout{

    public BodyTitleLayout(Context context) {
        super(context);
        inflate(context, R.layout.profile_user_title_body_layout,this);
    }
}

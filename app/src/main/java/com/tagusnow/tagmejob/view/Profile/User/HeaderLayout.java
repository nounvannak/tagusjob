package com.tagusnow.tagmejob.view.Profile.User;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.StrictMode;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.tagusnow.tagmejob.MoreViewProfileFragment;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class HeaderLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = HeaderLayout.class.getSimpleName();
    private Context context;
    private RelativeLayout layout_profile,rlProfileImage,btnLike,btnFollow,btnSee,btnMore;
    private CircleImageView pro_profile_picture;
    private ImageView pro_profile_cover,vLike,vFollow,vSee;
    private TextView user_name,email,location,likeCounter,followCounter,tLike,tFollow,tSee;
    private SmUser user,auth;
    private UserService service = ServiceGenerator.createService(UserService.class);

    public HeaderLayout(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.profile_user_header_layout,this);
        this.InitUI();
    }

    public HeaderLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        inflate(context, R.layout.profile_user_header_layout,this);
        this.InitUI();
    }

    public HeaderLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        inflate(context, R.layout.profile_user_header_layout,this);
        this.InitUI();
    }

    public HeaderLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        inflate(context, R.layout.profile_user_header_layout,this);
        this.InitUI();
    }

    private void InitUI(){
        layout_profile = (RelativeLayout)findViewById(R.id.layout_profile);
        rlProfileImage = (RelativeLayout)findViewById(R.id.rlProfileImage);
        pro_profile_picture = (CircleImageView)findViewById(R.id.pro_profile_picture);
        pro_profile_cover = (ImageView)findViewById(R.id.pro_profile_cover);
        user_name = (TextView)findViewById(R.id.user_name);
        email = (TextView)findViewById(R.id.email);
        location = (TextView)findViewById(R.id.location);
        likeCounter = (TextView)findViewById(R.id.likeCounter);
        followCounter = (TextView)findViewById(R.id.followCounter);

        btnLike = (RelativeLayout)findViewById(R.id.btnLike);
        btnFollow = (RelativeLayout)findViewById(R.id.btnFollow);
        btnSee = (RelativeLayout)findViewById(R.id.btnSee);
        btnMore = (RelativeLayout)findViewById(R.id.btnMore);

        vLike = (ImageView)findViewById(R.id.vLike);
        vFollow = (ImageView)findViewById(R.id.vFollow);
        vSee = (ImageView)findViewById(R.id.vSee);

        tLike = (TextView)findViewById(R.id.tLike);
        tFollow = (TextView)findViewById(R.id.tFollow);
        tSee = (TextView)findViewById(R.id.tSee);

        btnLike.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
        btnSee.setOnClickListener(this);
        btnMore.setOnClickListener(this);
    }

    public HeaderLayout setUser(SmUser user){
        this.user = user;

        if (user!=null){
            user_name.setText(user.getUser_name());
            if (user.getImage()!=null){
                if (user.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(user.getImage())
                            .error(R.mipmap.ic_launcher_circle)
                            .centerCrop()
                            .into(pro_profile_picture);
                }else{
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(user.getImage()))
                            .error(R.mipmap.ic_launcher_circle)
                            .centerCrop()
                            .into(pro_profile_picture);
                }
            }else{
                Glide.with(this.context)
                        .load(R.mipmap.ic_launcher_circle)
                        .centerCrop()
                        .into(pro_profile_picture);
            }

            if (user.getCover()!=null){
                if (user.getCover().contains("http")){
                    Glide.with(this.context)
                            .load(user.getCover())
                            .error(R.drawable.background_main_image)
                            .centerCrop()
                            .into(pro_profile_cover);
                }else{
                    Glide.with(this.context)
                            .load(SuperUtil.getCover(user.getCover()))
                            .error(R.drawable.background_main_image)
                            .centerCrop()
                            .into(pro_profile_cover);
                }
            }else {
                if (this.user.getUser_type()==SmUser.FIND_JOB){
                    Glide.with(getContext())
                            .load(SuperUtil.getBaseUrl("coverdefault1.png"))
                            .error(R.drawable.background_main_image)
                            .centerCrop()
                            .into(pro_profile_cover);
                }else {
                    Glide.with(getContext())
                            .load(SuperUtil.getBaseUrl("coverfindstaffdefual.png"))
                            .error(R.drawable.background_main_image)
                            .centerCrop()
                            .into(pro_profile_cover);
                }
            }

            if (user.getEmail()!=null){
                email.setText(user.getEmail());
            }else{
                email.setText(R.string.unknow);
            }
            if (user.getLocation()!=null){
                location.setText(user.getLocation());
            }else{
                location.setText(R.string.unknow);
            }
            likeCounter.setText(this.context.getString(R.string.liked_by,0));
            followCounter.setText(this.context.getString(R.string.followed_by,user.getFollower_counter()));

            if (user.isIs_like()){
                vLike.setImageResource(R.drawable.ic_action_like_blue);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tLike.setTextColor(this.context.getColor(R.color.colorPrimary));
                }
            }else {
                vLike.setImageResource(R.drawable.ic_action_like_black);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tLike.setTextColor(this.context.getColor(R.color.colorIcon));
                }
            }

            if (user.isIs_follow()){
                vFollow.setImageResource(R.drawable.ic_person_blue_24dp);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tFollow.setText("Following");
                    tFollow.setTextColor(this.context.getColor(R.color.colorPrimary));
                }
            }else {
                vFollow.setImageResource(R.drawable.ic_person_add_grey_700_24dp);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tFollow.setTextColor(this.context.getColor(R.color.colorIcon));
                }
            }

            if (user.isIs_see_first()){
                vSee.setImageResource(R.drawable.ic_remove_red_eye_grey_700_24dp);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        vSee.setBackgroundTintList(ColorStateList.valueOf(this.context.getColor(R.color.colorPrimary)));
                    }
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tSee.setTextColor(this.context.getColor(R.color.colorPrimary));
                }
            }else {
                vSee.setImageResource(R.drawable.ic_remove_red_eye_grey_700_24dp);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        vSee.setBackgroundTintList(ColorStateList.valueOf(this.context.getColor(R.color.colorIcon)));
                    }
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tSee.setTextColor(this.context.getColor(R.color.colorIcon));
                }
            }

        }else{
            Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
        }

        return this;
    }

    private void LikeUser(){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            boolean isLike = service.LikeUser(auth.getId(),user.getId()).execute().isSuccessful();
            if (isLike){
                user.setIs_like(true);
                setUser(user);
                Toast.makeText(this.context,"You have been liked "+this.user.getUser_name(),Toast.LENGTH_SHORT).show();
                dismissProgress();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void UnlikeUser(){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            boolean isLike = service.UnlikeUser(auth.getId(),user.getId()).execute().isSuccessful();
            if (isLike){
                user.setIs_like(false);
                setUser(user);
                Toast.makeText(this.context,"You have been unliked "+this.user.getUser_name(),Toast.LENGTH_SHORT).show();
                dismissProgress();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void FollowUser(){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            boolean isLike = service.FollowUser(auth.getId(),user.getId()).execute().isSuccessful();
            if (isLike){
                user.setIs_follow(true);
                setUser(user);
                Toast.makeText(this.context,"You have been following "+this.user.getUser_name(),Toast.LENGTH_SHORT).show();
                dismissProgress();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void UnfollowUser(){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            boolean isLike = service.UnfollowUser(auth.getId(),user.getId()).execute().isSuccessful();
            if (isLike){
                user.setIs_follow(false);
                setUser(user);
                Toast.makeText(this.context,"You have been unfollow "+this.user.getUser_name(),Toast.LENGTH_SHORT).show();
                dismissProgress();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void SeeFirst(){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            boolean isLike = service.SeeFirst(auth.getId(),user.getId()).execute().isSuccessful();
            if (isLike){
                user.setIs_see_first(true);
                setUser(user);
                Toast.makeText(this.context,"You have been see first "+this.user.getUser_name(),Toast.LENGTH_SHORT).show();
                dismissProgress();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void UnseeFirst(){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            boolean isLike = service.UnseeFirst(auth.getId(),user.getId()).execute().isSuccessful();
            if (isLike){
                user.setIs_see_first(false);
                setUser(user);
                Toast.makeText(this.context,"You have been removed see first from "+this.user.getUser_name(),Toast.LENGTH_SHORT).show();
                dismissProgress();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void more(){
        android.app.FragmentManager fragmentManager = this.activity.getFragmentManager();
        MoreViewProfileFragment.newInstance(this.auth,this.user).show(fragmentManager,TAG);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view==btnLike){
//            showProgress();
            if (this.user.isIs_like()){
                this.UnlikeUser();
            }else {
                this.LikeUser();
            }
        }else if (view==btnFollow){
//            showProgress();
            if (this.user.isIs_follow()){
                this.UnfollowUser();
            }else {
                this.FollowUser();
            }
        }else if (view==btnSee){
//            showProgress();
            if (this.user.isIs_see_first()){
                this.UnseeFirst();
            }else {
                this.SeeFirst();
            }
        }else if (view==btnMore){
            this.more();
        }
    }

    public void setAuth(SmUser auth) {
        this.auth = auth;
    }
}

package com.tagusnow.tagmejob.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.pedro.library.AutoPermissions;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;
import com.tagusnow.tagmejob.ChangeCoverActivity;
import com.tagusnow.tagmejob.ChangeProfileActivity;
import com.tagusnow.tagmejob.Gallery;
import com.tagusnow.tagmejob.MoreFragment;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.adapter.ProfileAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Image;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import permissions.dispatcher.NeedsPermission;

public class ProfileHeader extends TagUsJobRelativeLayout implements View.OnClickListener{

    private static final String TAG = ProfileHeader.class.getSimpleName();
    public final String OPEN_MEDIA_TITLE = "title";
    public final String OPEN_MEDIA_MODE = "mode";
    public final String OPEN_MEDIA_MAX = "maxSelection";
    public static final int CHANGE_PROFILE = 1894;
    public static final int CHANGE_COVER = 1893;
    private Context context;
    private SmUser user;
    private LinearLayout btnEditCover;
    private RelativeLayout btnEditProfilePicture;
    private ImageView pro_profile_cover;
    private CircleImageView pro_profile_picture;
    private TextView user_name;
    private BottomBar barProfile;
    private BottomBarTab tabHome,tabInfo,tabMore;

    public ProfileHeader(Context context) {
        super(context);
        inflate(context, R.layout.profile_header,this);
        this.context = context;
        btnEditCover = (LinearLayout)findViewById(R.id.btnEditCover);
        btnEditProfilePicture = (RelativeLayout)findViewById(R.id.btnEditProfilePicture);
        pro_profile_cover = (ImageView)findViewById(R.id.pro_profile_cover);
        pro_profile_picture = (CircleImageView)findViewById(R.id.pro_profile_picture);
        user_name = (TextView)findViewById(R.id.user_name);
        barProfile = (BottomBar)findViewById(R.id.barProfile);
        tabHome = (BottomBarTab)barProfile.findViewById(R.id.tab_home);
        tabInfo = (BottomBarTab)barProfile.findViewById(R.id.tab_info);
        tabMore = (BottomBarTab)barProfile.findViewById(R.id.tab_more);
        btnEditCover.setOnClickListener(this);
        btnEditProfilePicture.setOnClickListener(this);
        pro_profile_picture.setOnClickListener(this);
        pro_profile_cover.setOnClickListener(this);
    }

    public LinearLayout getBtnEditCover() {
        return btnEditCover;
    }

    public RelativeLayout getBtnEditProfilePicture() {
        return btnEditProfilePicture;
    }

    public ProfileHeader Activity(Activity activity){
        setActivity(activity);
        return this;
    }

    public ProfileHeader AppCompatActivity(AppCompatActivity appCompatActivity){
        setActivity(appCompatActivity);
        return this;
    }

    public ProfileHeader Auth(SmUser user){
        this.user = user;
        this.initUser(user);
        return this;
    }

    private void initUser(SmUser user){
        if (user!=null){

            user_name.setText(user.getUser_name());

            if (user.getImage()!=null){
                if (user.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(user.getImage())
                            .centerCrop()
                            .error(R.drawable.ic_user_male_icon)
                            .into(pro_profile_picture);
                }else {
                    Glide.with(this.context).load(SuperUtil.getProfilePicture(user.getImage()))
                            .centerCrop()
                            .error(R.drawable.ic_user_male_icon)
                            .into(pro_profile_picture);
                }
            }

            if (user.getCover()!=null){
                if (user.getCover().contains("http")){
                    Glide.with(this.context).load(user.getCover())
                            .centerCrop()
                            .error(R.drawable.background_main_image)
                            .into(pro_profile_cover);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getCover(user.getCover()))
                            .centerCrop()
                            .error(R.drawable.background_main_image)
                            .into(pro_profile_cover);
                }
            }else {
                Glide.with(this.context).load(R.drawable.background_main_image)
                        .centerCrop()
                        .error(R.drawable.background_main_image)
                        .into(pro_profile_cover);
            }

        }else {
            Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
        }
    }


    private void changeCover(){
        AndroidImagePicker.getInstance().pickSingle(this.activity, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                ((ProfileActivity) activity).sourceUri = Uri.fromFile(new File(items.get(0).path));
                ((ProfileActivity) activity).openCover();
            }
        });
    }

    private void changeProfile(){
        AndroidImagePicker.getInstance().pickSingle(this.activity, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                ((ProfileActivity) activity).sourceUri = Uri.fromFile(new File(items.get(0).path));
                ((ProfileActivity) activity).openProfile();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view==btnEditCover){
            changeCover();
        }else if (view==btnEditProfilePicture){
            changeProfile();
        }else if (view==pro_profile_cover){

        }else if (view==pro_profile_picture){

        }
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public BottomBarTab getTabHome() {
        return tabHome;
    }

    public BottomBarTab getTabInfo() {
        return tabInfo;
    }

    public BottomBarTab getTabMore() {
        return tabMore;
    }

    public BottomBar getBarProfile() {
        return barProfile;
    }
}

package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.SmUser;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileInfo extends RelativeLayout implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = ProfileInfo.class.getSimpleName();
    private Activity activity;
    private Context context;
    private SmUser user;
    private Calendar mCalendar;
    private String mGender;
    String[] mGenderList = new String[]{"male","female"};
    private TextView info_first_name,info_last_name,info_full_name,info_dob,info_gender,info_location,info_phone;
    private ImageButton info_edit_button,button_edit_info_last_name,button_edit_info_full_name,button_edit_info_gender,button_edit_info_dob,button_edit_info_location,button_edit_info_phone;
    private ImageButton button_no_info_first_name,button_no_info_last_name,button_no_info_full_name,button_no_info_dob,button_no_info_gender,button_no_info_phone,button_no_info_location;
    private ImageButton button_yes_info_first_name,button_yes_info_last_name,button_yes_info_full_name,button_yes_info_dob,button_yes_info_gender,button_yes_info_phone,button_yes_info_location;
    private Spinner txt_info_gender;
    private EditText txt_info_first_name,txt_info_last_name,txt_info_full_name,txt_info_dob,txt_info_location,txt_info_phone;

    /*Service*/
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build();
    private AuthService service = retrofit.create(AuthService.class);

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            if (mCalendar==null){
                mCalendar = Calendar.getInstance();
            }
            new DatePickerDialog(activity, dateSetListener, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    private Callback<SmUser> mBack = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                Auth(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public ProfileInfo(Context context) {
        super(context);
        inflate(context, R.layout.profile_info,this);
        this.context = context;
        this.initView();
        this.onStartUI();
    }

    public ProfileInfo Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public ProfileInfo Auth(SmUser user){
        this.user = user;
        this.setTempUser();
        return this;
    }

    private void initView(){
        info_first_name = (TextView)findViewById(R.id.info_first_name);
        info_last_name = (TextView)findViewById(R.id.info_last_name);
        info_full_name = (TextView)findViewById(R.id.info_full_name);
        info_dob = (TextView)findViewById(R.id.info_dob);
        info_gender = (TextView)findViewById(R.id.info_gender);
        info_location = (TextView)findViewById(R.id.info_location);
        info_phone = (TextView)findViewById(R.id.info_phone);
        info_edit_button = (ImageButton)findViewById(R.id.button_edit_info_first_name);
        txt_info_first_name = (EditText)findViewById(R.id.txt_info_first_name);
        txt_info_last_name = (EditText)findViewById(R.id.txt_info_last_name);
        txt_info_full_name = (EditText)findViewById(R.id.txt_info_full_name);
        txt_info_dob = (EditText)findViewById(R.id.txt_info_dob);
        txt_info_gender = (Spinner)findViewById(R.id.txt_info_gender);
        txt_info_location = (EditText)findViewById(R.id.txt_info_location);
        txt_info_phone = (EditText)findViewById(R.id.txt_info_phone);
        button_edit_info_last_name = (ImageButton)findViewById(R.id.button_edit_info_last_name);
        button_edit_info_full_name = (ImageButton)findViewById(R.id.button_edit_info_full_name);
        button_edit_info_dob = (ImageButton)findViewById(R.id.button_edit_info_dob);
        button_edit_info_gender = (ImageButton)findViewById(R.id.button_edit_info_gender);
        button_edit_info_location = (ImageButton)findViewById(R.id.button_edit_info_location);
        button_edit_info_phone = (ImageButton)findViewById(R.id.button_edit_info_phone);
        button_no_info_first_name = (ImageButton)findViewById(R.id.button_no_info_first_name);
        button_no_info_full_name = (ImageButton)findViewById(R.id.button_no_info_full_name);
        button_no_info_dob = (ImageButton)findViewById(R.id.button_no_info_dob);
        button_no_info_last_name = (ImageButton)findViewById(R.id.button_no_info_last_name);
        button_no_info_gender = (ImageButton)findViewById(R.id.button_no_info_gender);
        button_no_info_location = (ImageButton)findViewById(R.id.button_no_info_location);
        button_no_info_phone = (ImageButton)findViewById(R.id.button_no_info_phone);
        button_yes_info_first_name = (ImageButton)findViewById(R.id.button_yes_info_first_name);
        button_yes_info_full_name = (ImageButton)findViewById(R.id.button_yes_info_full_name);
        button_yes_info_dob = (ImageButton)findViewById(R.id.button_yes_info_dob);
        button_yes_info_last_name = (ImageButton)findViewById(R.id.button_yes_info_last_name);
        button_yes_info_gender = (ImageButton)findViewById(R.id.button_yes_info_gender);
        button_yes_info_location = (ImageButton)findViewById(R.id.button_yes_info_location);
        button_yes_info_phone = (ImageButton)findViewById(R.id.button_yes_info_phone);

        button_yes_info_phone.setOnClickListener(this);
        button_yes_info_location.setOnClickListener(this);
        button_yes_info_gender.setOnClickListener(this);
        button_yes_info_last_name.setOnClickListener(this);
        button_yes_info_dob.setOnClickListener(this);
        button_yes_info_full_name.setOnClickListener(this);
        button_yes_info_first_name.setOnClickListener(this);
        button_no_info_phone.setOnClickListener(this);
        button_no_info_location.setOnClickListener(this);
        button_no_info_gender.setOnClickListener(this);
        button_no_info_last_name.setOnClickListener(this);
        button_no_info_dob.setOnClickListener(this);
        button_no_info_full_name.setOnClickListener(this);
        button_no_info_first_name.setOnClickListener(this);
        button_edit_info_phone.setOnClickListener(this);
        button_edit_info_location.setOnClickListener(this);
        button_edit_info_gender.setOnClickListener(this);
        button_edit_info_dob.setOnClickListener(this);
        button_edit_info_full_name.setOnClickListener(this);
        button_edit_info_last_name.setOnClickListener(this);
        info_edit_button.setOnClickListener(this);

        List<String> listGender = new ArrayList<String>();
        listGender.add("Male");
        listGender.add("Female");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,listGender);
        txt_info_gender.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        txt_info_gender.setOnItemSelectedListener(this);
        txt_info_dob.setOnClickListener(this);
    }

    private void onStartUI(){
        /*GONE*/
        txt_info_first_name.setVisibility(View.GONE);
        txt_info_last_name.setVisibility(View.GONE);
        txt_info_full_name.setVisibility(View.GONE);
        txt_info_dob.setVisibility(View.GONE);
        txt_info_gender.setVisibility(View.GONE);
        txt_info_location.setVisibility(View.GONE);
        txt_info_phone.setVisibility(View.GONE);
        button_no_info_first_name.setVisibility(View.GONE);
        button_no_info_last_name.setVisibility(View.GONE);
        button_no_info_full_name.setVisibility(View.GONE);
        button_no_info_dob.setVisibility(View.GONE);
        button_no_info_gender.setVisibility(View.GONE);
        button_no_info_location.setVisibility(View.GONE);
        button_no_info_phone.setVisibility(View.GONE);
        button_yes_info_first_name.setVisibility(View.GONE);
        button_yes_info_last_name.setVisibility(View.GONE);
        button_yes_info_full_name.setVisibility(View.GONE);
        button_yes_info_dob.setVisibility(View.GONE);
        button_yes_info_gender.setVisibility(View.GONE);
        button_yes_info_location.setVisibility(View.GONE);
        button_yes_info_phone.setVisibility(View.GONE);
        /*VISIBLE*/
        info_edit_button.setVisibility(View.VISIBLE);
        button_edit_info_last_name.setVisibility(View.VISIBLE);
        button_edit_info_full_name.setVisibility(View.VISIBLE);
        button_edit_info_dob.setVisibility(View.VISIBLE);
        button_edit_info_gender.setVisibility(View.VISIBLE);
        button_edit_info_location.setVisibility(View.VISIBLE);
        button_edit_info_phone.setVisibility(View.VISIBLE);
        info_first_name.setVisibility(View.VISIBLE);
        info_last_name.setVisibility(View.VISIBLE);
        info_full_name.setVisibility(View.VISIBLE);
        info_dob.setVisibility(View.VISIBLE);
        info_gender.setVisibility(View.VISIBLE);
        info_location.setVisibility(View.VISIBLE);
        info_phone.setVisibility(View.VISIBLE);
    }

    private void onEditUI(){}

    private void onYes(){
        String dob = "";
        if (mCalendar!=null){
            dob = mCalendar.get(Calendar.YEAR)+"-"+(mCalendar.get(Calendar.MONTH)+1)+"-"+mCalendar.get(Calendar.DAY_OF_MONTH);
        }
        this.user.setFirs_name(txt_info_first_name.getText().toString());
        this.user.setLast_name(txt_info_last_name.getText().toString());
        this.user.setUser_name(txt_info_full_name.getText().toString());
        this.user.setDob(dob);
        this.user.setGender(mGender.toLowerCase().equals("male") ? "1" : "2");
        this.user.setLocation(txt_info_location.getText().toString());
        this.user.setPhone(txt_info_phone.getText().toString());
        onEditInfo();
    }

    private void initDatePicker(){
        if (!this.activity.isFinishing()){
            this.activity.runOnUiThread(mRun);
        }
    }

    private void onFailure(String error){
        final PrettyDialog pDialog = new PrettyDialog(getContext());
        pDialog.setTitle("Update Account")
                .setMessage(error)
                .setIcon(R.drawable.pdlg_icon_close)
                .setIconTint(R.color.pdlg_color_red)
                .addButton(
                        "OK",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        }
                )
                .show();
    }

    private void onEditInfo(){
        service.updateAccount(this.user).enqueue(mBack);
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int month, int day) {
            mCalendar = Calendar.getInstance();
            mCalendar.set(Calendar.YEAR,year);
            mCalendar.set(Calendar.MONTH,month);
            mCalendar.set(Calendar.DAY_OF_MONTH,day);
            setDob();
        }
    };

    private void setDob(){
        String format = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        if (mCalendar.getTime()!=null) {
            txt_info_dob.setText(simpleDateFormat.format(mCalendar.getTime()));
        }
    }

    private void setTempUser(){
        this.user.setAuth_user(this.user.getId());
        if (this.user.getFirs_name()!=null){
            info_first_name.setText(this.user.getFirs_name());
            txt_info_first_name.setText(this.user.getFirs_name());
        }
        if (this.user.getLast_name()!=null){
            info_last_name.setText(this.user.getLast_name());
            txt_info_last_name.setText(this.user.getLast_name());
        }
        if (this.user.getUser_name()!=null){
            info_full_name.setText(this.user.getUser_name());
            txt_info_full_name.setText(this.user.getUser_name());
        }
        if (this.user.getDob()!=null){
            info_dob.setText(this.user.getDateOfBirth());
            txt_info_dob.setText(this.user.getDateOfBirth());
            String format = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format,Locale.ENGLISH);
            try {
                mCalendar = Calendar.getInstance();
                mCalendar.setTimeInMillis(simpleDateFormat.parse(this.user.getDateOfBirth()).getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (this.user.getGender()!=null){
            info_gender.setText(this.user.getGender());
            txt_info_gender.setPrompt(this.user.getGender());
            mGender = this.user.getGender().toLowerCase();
        }
        if (this.user.getLocation()!=null){
            info_location.setText(this.user.getLocation());
            txt_info_location.setText(this.user.getLocation());
        }
        if (this.user.getPhone()!=null){
            info_phone.setText(this.user.getPhone());
            txt_info_phone.setText(this.user.getPhone());
        }
    }

    private void checkValid(String value){
        if (value.length() > 0){
            onYes();
        }else {
            onFailure("Invalid input");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_edit_info_first_name:
                onStartUI();
                info_first_name.setVisibility(View.GONE);
                info_edit_button.setVisibility(View.GONE);
                button_no_info_first_name.setVisibility(View.VISIBLE);
                button_yes_info_first_name.setVisibility(View.VISIBLE);
                txt_info_first_name.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_last_name:
                onStartUI();
                info_last_name.setVisibility(View.GONE);
                button_edit_info_last_name.setVisibility(View.GONE);
                button_no_info_last_name.setVisibility(View.VISIBLE);
                button_yes_info_last_name.setVisibility(View.VISIBLE);
                txt_info_last_name.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_full_name:
                onStartUI();
                info_full_name.setVisibility(View.GONE);
                button_edit_info_full_name.setVisibility(View.GONE);
                button_no_info_full_name.setVisibility(View.VISIBLE);
                button_yes_info_full_name.setVisibility(View.VISIBLE);
                txt_info_full_name.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_dob:
                onStartUI();
                info_dob.setVisibility(View.GONE);
                button_edit_info_dob.setVisibility(View.GONE);
                button_no_info_dob.setVisibility(View.VISIBLE);
                button_yes_info_dob.setVisibility(View.VISIBLE);
                txt_info_dob.setVisibility(View.VISIBLE);
                txt_info_dob.setFocusable(false);
                break;
            case R.id.button_edit_info_gender:
                onStartUI();
                info_gender.setVisibility(View.GONE);
                button_edit_info_gender.setVisibility(View.GONE);
                button_no_info_gender.setVisibility(View.VISIBLE);
                button_yes_info_gender.setVisibility(View.VISIBLE);
                txt_info_gender.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_location:
                onStartUI();
                info_location.setVisibility(View.GONE);
                button_edit_info_location.setVisibility(View.GONE);
                button_no_info_location.setVisibility(View.VISIBLE);
                button_yes_info_location.setVisibility(View.VISIBLE);
                txt_info_location.setVisibility(View.VISIBLE);
                break;
            case R.id.button_edit_info_phone:
                onStartUI();
                info_phone.setVisibility(View.GONE);
                button_edit_info_phone.setVisibility(View.GONE);
                button_no_info_phone.setVisibility(View.VISIBLE);
                button_yes_info_phone.setVisibility(View.VISIBLE);
                txt_info_phone.setVisibility(View.VISIBLE);
                break;
            case R.id.txt_info_dob:
                initDatePicker();
                break;
            case R.id.button_yes_info_first_name:
                checkValid(txt_info_first_name.getText().toString());
                break;
            case R.id.button_yes_info_last_name:
                checkValid(txt_info_last_name.getText().toString());
                break;
            case R.id.button_yes_info_full_name:
                checkValid(txt_info_full_name.getText().toString());
                break;
            case R.id.button_yes_info_dob:
                checkValid(txt_info_dob.getText().toString());
                break;
            case R.id.button_yes_info_gender:
                onYes();
                break;
            case R.id.button_yes_info_location:
                checkValid(txt_info_location.getText().toString());
                break;
            case R.id.button_yes_info_phone:
                checkValid(txt_info_phone.getText().toString());
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mGender = mGenderList[i];
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

package com.tagusnow.tagmejob.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.adapter.FeedAdapter;

public class ProgressViewHolder extends RecyclerView.ViewHolder {
    ProgressBar progressBar;
    public ProgressViewHolder(View itemView) {
        super(itemView);
        progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);
    }
}

package com.tagusnow.tagmejob.view.Search.Main.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.V2.Model.CompanyPaginate;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.v3.company.CompanyCard;
import com.tagusnow.tagmejob.v3.company.CompanyListener;
import com.tagusnow.tagmejob.v3.feed.FeedListener;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJob;
import com.tagusnow.tagmejob.v3.feed.SmallFeedJobListener;
import com.tagusnow.tagmejob.v3.search.SearchForListener;
import com.tagusnow.tagmejob.v3.search.result.SearchForCompany;
import com.tagusnow.tagmejob.v3.search.result.SearchForCompanyHolder;
import com.tagusnow.tagmejob.v3.search.result.SearchForJob;
import com.tagusnow.tagmejob.v3.search.result.SearchForJobAdapter;
import com.tagusnow.tagmejob.v3.search.result.SearchForJobHolder;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchForCompanyActivity extends TagUsJobActivity implements TextWatcher, CompanyListener<CompanyCard>, SmallFeedJobListener, SearchForListener, FeedListener {

    private static final String TAG = SearchForCompanyActivity.class.getSimpleName();
    public static final String SEARCH_TEXT = "search_text";
    private Toolbar toolbar;
    private EditText txtSearch;
    private String search = "";
    private RecyclerView recyclerView;
    private SmUser Auth;
    private List<Feed> feeds;
    private FeedResponse jobsPaginate;
    private CompanyPaginate companyPaginate;
    private List<Company> companies;
    private SearchForCompanyAdapter adapter;
    private SearchForCompany searchForCompany;
    private SearchForJob searchForJob;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private com.tagusnow.tagmejob.v3.search.result.SearchForCompanyAdapter searchForCompanyAdapter;
    private SearchForJobAdapter searchForJobAdapter;
    private Callback<CompanyPaginate> FetchCompanyCallback = new Callback<CompanyPaginate>() {
        @Override
        public void onResponse(Call<CompanyPaginate> call, Response<CompanyPaginate> response) {
            if (response.isSuccessful()){
                fetchCompany(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CompanyPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<CompanyPaginate> FetchNextCompanyCallback = new Callback<CompanyPaginate>() {
        @Override
        public void onResponse(Call<CompanyPaginate> call, Response<CompanyPaginate> response) {
            if (response.isSuccessful()){
                fetchNextCompany(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CompanyPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchJobCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchJob(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> FetchNextJobCallback = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                fetchNextJob(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchForCompanyActivity.class);
    }

    /* Socket.io */
    Socket socket;
    private Emitter.Listener NEW_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
        }
    };
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int feedID = obj.getInt("feed_id");
                    int totalLikes = obj.getInt("count_likes");
                    int userID = obj.getInt("user_id");
                    String likesResult = obj.getString("like_result");
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                        if (lists.size() > 0){
                            Feed feed = lists.get(0);
                            FeedDetail feedDetail = feed.getFeed();
                            int index = feeds.indexOf(feed);

                            feedDetail.setCount_likes(totalLikes);
                            feedDetail.setLike_result(likesResult);
                            feedDetail.setIs_like(Auth.getId() == userID);

                            feed.setFeed(feedDetail);
                            feeds.set(index,feed);
                            searchForJobAdapter.notifyDataSetChanged();
                        }
                    }else {
                        for (Feed feed: feeds){
                            if (feed.getId() == feedID){
                                FeedDetail feedDetail = feed.getFeed();
                                int index = feeds.indexOf(feed);

                                feedDetail.setCount_likes(totalLikes);
                                feedDetail.setLike_result(likesResult);
                                feedDetail.setIs_like(Auth.getId() == userID);

                                feed.setFeed(feedDetail);
                                feeds.set(index,feed);
                                searchForJobAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int feedID = obj.getInt("feed_id");
                    int totalComments = obj.getInt("count_comments");
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                        if (lists.size() > 0){
                            Feed feed = lists.get(0);
                            FeedDetail feedDetail = feed.getFeed();
                            int index = feeds.indexOf(feed);
                            feedDetail.setCount_comments(totalComments);
                            feed.setFeed(feedDetail);
                            feeds.set(index,feed);
                            searchForJobAdapter.notifyDataSetChanged();
                        }
                    }else {
                        for (Feed feed: feeds){
                            if (feed.getId() == feedID){
                                FeedDetail feedDetail = feed.getFeed();
                                int index = feeds.indexOf(feed);
                                feedDetail.setCount_comments(totalComments);
                                feed.setFeed(feedDetail);
                                feeds.set(index,feed);
                                searchForJobAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                    if (feedJson != null){
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                            if (lists.size() > 0){
                                Feed feed = lists.get(0);
                                int index = feeds.indexOf(feed);
                                Feed updateFeed = feeds.get(index);
                                updateFeed.setFeed(feedJson.getFeed());
                                feeds.set(index,updateFeed);
                                searchForJobAdapter.notifyDataSetChanged();
                            }
                        }else {
                            for (Feed feed: feeds){
                                if (feed.getId() == feedJson.getId()){
                                    int index = feeds.indexOf(feed);
                                    Feed updateFeed = feeds.get(index);
                                    updateFeed.setFeed(feedJson.getFeed());
                                    feeds.set(index,updateFeed);
                                    searchForJobAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                    if (feedJson != null){
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                            if (lists.size() > 0){
                                Feed feed = lists.get(0);
                                feeds.remove(feed);
                                searchForJobAdapter.notifyDataSetChanged();
                            }
                        }else {
                            for (Feed feed: feeds){
                                if (feed.getId() == feedJson.getId()){
                                    feeds.remove(feed);
                                    searchForJobAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (obj != null){
                try {
                    int userID = obj.getInt("user_id");
                    if (Auth.getId() == userID){
                        Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                        if (feedJson != null){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    feeds.remove(feed);
                                    searchForJobAdapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedJson.getId()){
                                        feeds.remove(feed);
                                        searchForJobAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
    };
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("New Post",NEW_POST);
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.connect();
    }
    /* End Socket.io */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_for_company);

        InitTemp();
        initSocket();
        InitUI();
    }

    private void InitTemp(){
        this.Auth = new Auth(this).checkAuth().token();
        if (getIntent().getStringExtra(SEARCH_TEXT)!=null){
            this.search = getIntent().getStringExtra(SEARCH_TEXT);
        }
    }

    private void InitUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtSearch = (EditText)findViewById(R.id.txtSearch);
        txtSearch.addTextChangedListener(this);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        txtSearch.setText(this.search);
        adapter = new SearchForCompanyAdapter(this,this);
        searchForCompany = adapter.getSearchForCompany();
        searchForJob = adapter.getSearchForJob();
        recyclerView.setAdapter(adapter);
        Search();
    }

    private void Search(){
        fetchCompany();
        fetchJob();
    }

    private void fetchCompany(){
        service.GetCompany(Auth.getId(),search,5).enqueue(FetchCompanyCallback);
    }

    private void fetchCompany(CompanyPaginate companyPaginate){
        this.companyPaginate = companyPaginate;
        this.companies = companyPaginate.getData();
        CheckNextCompany();
        searchForCompanyAdapter = new com.tagusnow.tagmejob.v3.search.result.SearchForCompanyAdapter(this,companyPaginate.getData(),this);

        if (searchForCompany != null){
            searchForCompany.setTotal(companyPaginate.getTotal());
            searchForCompany.setAdapter(searchForCompanyAdapter);
        }else {
            searchForCompany = adapter.getSearchForCompany();
            searchForCompany.setTotal(companyPaginate.getTotal());
            searchForCompany.setAdapter(searchForCompanyAdapter);
        }
    }

    private void fetchNextCompany(){
        boolean isNext = companyPaginate.getCurrent_page() != companyPaginate.getLast_page();
        if (isNext){
            int page = companyPaginate.getCurrent_page() + 1;
            service.GetCompany(Auth.getId(),search,5,page).enqueue(FetchNextCompanyCallback);
        }
    }

    private void fetchNextCompany(CompanyPaginate companyPaginate){
        this.companyPaginate = companyPaginate;
        this.companies.addAll(companyPaginate.getData());
        CheckNextCompany();
        searchForCompanyAdapter.NextData(companyPaginate.getData());
    }

    private void fetchJob(){
        service.Search(Auth.getId(),1,search,0,0,10).enqueue(FetchJobCallback);
    }

    private void fetchJob(FeedResponse jobsPaginate){
        this.jobsPaginate = jobsPaginate;
        this.feeds = jobsPaginate.getData();
        CheckNextJob();
        searchForJobAdapter = new SearchForJobAdapter(this,jobsPaginate.getData(),this);

        if (searchForJob != null){
            searchForJob.setTotal(jobsPaginate.getTotal());
            searchForJob.setAdapter(searchForJobAdapter);
        }else {
            searchForJob = adapter.getSearchForJob();
            searchForJob.setTotal(jobsPaginate.getTotal());
            searchForJob.setAdapter(searchForJobAdapter);
        }
    }

    private void fetchNextJob(){
        boolean isNext = jobsPaginate.getCurrent_page() != jobsPaginate.getLast_page();
        if (isNext){
            int page = jobsPaginate.getCurrent_page() + 1;
            service.Search(Auth.getId(),1,search,0,0,10,page).enqueue(FetchNextJobCallback);
        }
    }

    private void fetchNextJob(FeedResponse jobsPaginate){
        this.jobsPaginate = jobsPaginate;
        this.feeds.addAll(jobsPaginate.getData());
        CheckNextJob();
        searchForJobAdapter.NextData(jobsPaginate.getData());
    }

    private void CheckNextJob(){
        if (searchForJob != null){
            searchForJob.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (jobsPaginate.getTo() - 1)) && (jobsPaginate.getTo() < jobsPaginate.getTotal())) {
                            if (jobsPaginate.getNext_page_url()!=null || !jobsPaginate.getNext_page_url().equals("")){
                                if (lastPage!=jobsPaginate.getCurrent_page()){
                                    isRequest = true;
                                    if (isRequest){
                                        lastPage = jobsPaginate.getCurrent_page();
                                        fetchNextJob();
                                    }
                                }else{
                                    isRequest = false;
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private static boolean isRequestCompany = false;
    private static int lastPageOfCompany = 0;
    private void CheckNextCompany(){
        if (searchForCompany != null){
            searchForCompany.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    try {
                        if ((lastCompletelyVisibleItemPosition == (companyPaginate.getTo() - 1)) && (companyPaginate.getTo() < companyPaginate.getTotal())) {
                            if (companyPaginate.getNext_page_url()!=null || !companyPaginate.getNext_page_url().equals("")){
                                if (lastPageOfCompany!=companyPaginate.getCurrent_page()){
                                    isRequestCompany = true;
                                    if (isRequestCompany){
                                        lastPageOfCompany = companyPaginate.getCurrent_page();
                                        fetchNextCompany();
                                    }
                                }else{
                                    isRequestCompany = false;
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        search = s.toString();
        Search();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onSubcribe(CompanyCard view, Company company) {

    }

    @Override
    public void onDetail(CompanyCard view, Company company) {
        view.OpenCompany(company);
    }

    @Override
    public void onMore(SmallFeedJob view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void viewDetail(SmallFeedJob view, Feed feed) {
        view.DetailFeedJob(Auth,feed);
    }

    @Override
    public void SearchForPeople(TagUsJobRelativeLayout view) {

    }

    @Override
    public void SearchForCompany(TagUsJobRelativeLayout view) {

    }

    @Override
    public void SearchForStaffAndJob(TagUsJobRelativeLayout view) {

    }

    @Override
    public void SearchForContent(TagUsJobRelativeLayout view) {

    }

    @Override
    public void showMore(TagUsJobRelativeLayout view) {
        if (view == searchForCompany){
            view.SearchForCompany(search);
        }else if (view == searchForJob){
            view.SearchForJob(search);
        }
    }

    @Override
    public void saveFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Save Post").setMessage("Are you sure to save this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!feed.getFeed().isIs_save()){
                    int index = feeds.indexOf(feed);
                    FeedDetail temp = feed.getFeed();
                    feed.setIs_save(1);
                    temp.setIs_save(true);
                    feed.setFeed(temp);
                    feeds.set(index,feed);
                    searchForJobAdapter.notifyDataSetChanged();
                    service.SaveFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                        @Override
                        public void onResponse(Call<Feed> call, Response<Feed> response) {
                            if (response.isSuccessful()){
                                Log.w(TAG,response.message());
                            }else {
                                try {
                                    Log.e(TAG,response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Feed> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }

            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void editFeed(SmUser user, Feed feed) {
        EditPostNormal(feed);
    }

    @Override
    public void hideFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Hide Post").setMessage("Are you sure to hide this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feeds.remove(feed);
                searchForJobAdapter.notifyDataSetChanged();
                service.HideFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void removeFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Post").setMessage("Are you sure to remove this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feeds.remove(feed);
                searchForJobAdapter.notifyDataSetChanged();
                service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public class SearchForCompanyAdapter extends RecyclerView.Adapter {

        private static final int COMPANY = 0;
        private static final int JOB = 1;
        private Activity activity;
        private SearchForCompany searchForCompany;
        private SearchForJob searchForJob;
        private SearchForListener searchForListener;

        SearchForCompanyAdapter(Activity activity, SearchForListener searchForListener) {
            this.activity = activity;
            this.searchForListener = searchForListener;

            searchForCompany = new SearchForCompany(activity);
            searchForJob = new SearchForJob(activity);
        }

        SearchForCompany getSearchForCompany() {
            return searchForCompany;
        }

        SearchForJob getSearchForJob() {
            return searchForJob;
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? COMPANY : JOB;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            if (viewType == COMPANY){
                return new SearchForCompanyHolder(searchForCompany);
            }else {
                return new SearchForJobHolder(searchForJob);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (getItemViewType(position) == COMPANY){
                ((SearchForCompanyHolder) holder).BindView(activity,searchForListener);
            }else {
                ((SearchForJobHolder) holder).BindView(activity,searchForListener);
            }
        }

        @Override
        public int getItemCount() {
            return 2;
        }
    }
}

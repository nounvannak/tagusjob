package com.tagusnow.tagmejob.view.Search.Main.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.v3.feed.FeedListener;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.v3.like.LikeResultCell;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.v3.search.result.SearchForContentAdapter;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchForContentActivity extends TagUsJobActivity implements GridViewListener, FeedNormalListener, TextWatcher, LikeResultListener, FeedListener {

    private static final String TAG = SearchForContentActivity.class.getSimpleName();
    public static final String SEARCH_TEXT = "search_text";
    public static final String CITY = "city";
    private Toolbar toolbar;
    private EditText txtSearch;
    private TextView txtResult;
    private String search = "";
    private RecyclerView recyclerView;
    private SmUser Auth;
    private List<Feed> feeds;
    private FeedResponse feedResponse;
    private SearchForContentAdapter adapter;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<FeedResponse> NEW = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Search(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<FeedResponse> NEXT = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Next(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    /* Socket.io */
    Socket socket;
    private Emitter.Listener NEW_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
        }
    };
    private Emitter.Listener COUNT_LIKE = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalLikes = obj.getInt("count_likes");
                            int userID = obj.getInt("user_id");
                            String likesResult = obj.getString("like_result");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);
                                    feedDetail.setCount_likes(totalLikes);
                                    feedDetail.setLike_result(likesResult);
                                    feedDetail.setIs_like(Auth.getId() == userID);
                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);
                                        feedDetail.setCount_likes(totalLikes);
                                        feedDetail.setLike_result(likesResult);
                                        feedDetail.setIs_like(Auth.getId() == userID);
                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });

        }
    };
    private Emitter.Listener COUNT_COMMENT = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int feedID = obj.getInt("feed_id");
                            int totalComments = obj.getInt("count_comments");
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedID).collect(Collectors.toList());
                                if (lists.size() > 0){
                                    Feed feed = lists.get(0);
                                    FeedDetail feedDetail = feed.getFeed();
                                    int index = feeds.indexOf(feed);
                                    feedDetail.setCount_comments(totalComments);
                                    feed.setFeed(feedDetail);
                                    feeds.set(index,feed);
                                    adapter.notifyDataSetChanged();
                                }
                            }else {
                                for (Feed feed: feeds){
                                    if (feed.getId() == feedID){
                                        FeedDetail feedDetail = feed.getFeed();
                                        int index = feeds.indexOf(feed);
                                        feedDetail.setCount_comments(totalComments);
                                        feed.setFeed(feedDetail);
                                        feeds.set(index,feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener UPDATE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        int index = feeds.indexOf(feed);
                                        Feed updateFeed = feeds.get(index);
                                        updateFeed.setFeed(feedJson.getFeed());
                                        feeds.set(index,updateFeed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            int index = feeds.indexOf(feed);
                                            Feed updateFeed = feeds.get(index);
                                            updateFeed.setFeed(feedJson.getFeed());
                                            feeds.set(index,updateFeed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener REMOVE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            Feed feedJson = new Gson().fromJson(obj.toString(),Feed.class);
                            if (feedJson != null){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                    if (lists.size() > 0){
                                        Feed feed = lists.get(0);
                                        feeds.remove(feed);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    for (Feed feed: feeds){
                                        if (feed.getId() == feedJson.getId()){
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private Emitter.Listener HIDE_POST = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (obj != null){
                        try {
                            int userID = obj.getInt("user_id");
                            if (Auth.getId() == userID){
                                Feed feedJson = new Gson().fromJson(obj.get("feed").toString(),Feed.class);
                                if (feedJson != null){
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                        List<Feed> lists = feeds.stream().filter(t -> t.getId() == feedJson.getId()).collect(Collectors.toList());
                                        if (lists.size() > 0){
                                            Feed feed = lists.get(0);
                                            feeds.remove(feed);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }else {
                                        for (Feed feed: feeds){
                                            if (feed.getId() == feedJson.getId()){
                                                feeds.remove(feed);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    };
    private void initSocket(){
        socket = new App().getSocket();
        socket.on("New Post",NEW_POST);
        socket.on("Count Like",COUNT_LIKE);
        socket.on("Count Comment",COUNT_COMMENT);
        socket.on("Update Post",UPDATE_POST);
        socket.on("Remove Post",REMOVE_POST);
        socket.on("Hide Post",HIDE_POST);
        socket.connect();
    }
    /* End Socket.io */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchForContentActivity.class);
    }

    private void InitTemp(){
        this.Auth = new Auth(this).checkAuth().token();
        if (getIntent().getStringExtra(SEARCH_TEXT)!=null){
            this.search = getIntent().getStringExtra(SEARCH_TEXT);
        }
    }

    private void InitUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtSearch = (EditText)findViewById(R.id.txtSearch);
        txtResult = (TextView)findViewById(R.id.txtResult);
        txtSearch.addTextChangedListener(this);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        txtSearch.setText(this.search);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_for_content);

        InitTemp();
        InitUI();
        initSocket();
        Search();
    }

    private void Search(){
        service.Search(this.Auth.getId(),3,this.search,0,0,10).enqueue(NEW);
    }

    private void Search(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds = feedResponse.getData();
        txtResult.setText(getString(R.string.search_result,feedResponse.getTotal()));
        this.checkNext();
        this.adapter = new SearchForContentAdapter(this,feedResponse.getData(),this,this);
        recyclerView.setAdapter(this.adapter);
    }

    private void Next(){
        boolean isNext = feedResponse.getCurrent_page() != feedResponse.getLast_page();
        if (isNext){
            int page = this.feedResponse.getCurrent_page() + 1;
            service.Search(this.Auth.getId(),3,this.search,0,0,10,page).enqueue(NEXT);
        }
    }

    private void Next(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.feeds.addAll(feedResponse.getData());
        txtResult.setText(getString(R.string.search_result,feedResponse.getTotal()));
        checkNext();
        adapter.NextData(feedResponse.getData());
        adapter.notifyDataSetChanged();
    }

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    Next();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void showGallery(List<Album> albums, int index) {
        ArrayList<String> images = new ArrayList<>();
        for (Album album: albums){
            images.add(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"original"));
        }
        DisplayImage(images,index);
    }

    private void LikeFeed(SmUser Auth, Feed feed){
        if (!feed.getFeed().isIs_like()){
            int index = feeds.indexOf(feed);
            FeedDetail temp = feed.getFeed();
            temp.setIs_like(true);
            temp.setCount_likes(1);
            temp.setLike_result("You liked this post.");
            feed.setFeed(temp);
            feeds.set(index,feed);
            adapter.notifyDataSetChanged();
            service.LikeFeed(feed.getId(),Auth.getId()).enqueue(new Callback<SmTagFeed.Like>() {
                @Override
                public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                    if (response.isSuccessful()){
                        Log.e(TAG,response.message());
                    }else {
                        try {
                            Log.e(TAG,response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onLike(TagUsJobRelativeLayout view, Feed feed) {
        LikeFeed(Auth,feed);
    }

    @Override
    public void onComment(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenComment(feed);
    }

    @Override
    public void onShare(TagUsJobRelativeLayout view, Feed feed) {
        view.ShareFeed(feed);
    }

    @Override
    public void onMore(TagUsJobRelativeLayout view, Feed feed) {
        view.MoreFeed(Auth,feed,this);
    }

    @Override
    public void showDetail(TagUsJobRelativeLayout view, Feed feed) {
        view.DetailFeedNormal(feed);
    }

    @Override
    public void viewProfile(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenProfile(Auth,feed.getUser_post());
    }

    @Override
    public void showAllUserLiked(TagUsJobRelativeLayout view, Feed feed) {
        view.ShowAllUserLiked(Auth,feed,this);
    }

    @Override
    public void openWebsite(TagUsJobRelativeLayout view, Feed feed) {
        view.OpenWebsite(feed.getFeed().getSource_url());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        this.search = s.toString();
        Search();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void select(LikeResultCell cell, SmUser user) {
        cell.OpenProfile(Auth,user);
    }

    @Override
    public void connect(LikeResultCell cell, SmUser user) {
        cell.DoFollow(Auth,user);
    }

    @Override
    public void saveFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Save Post").setMessage("Are you sure to save this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (!feed.getFeed().isIs_save()){
                    int index = feeds.indexOf(feed);
                    FeedDetail temp = feed.getFeed();
                    feed.setIs_save(1);
                    temp.setIs_save(true);
                    feed.setFeed(temp);
                    feeds.set(index,feed);
                    adapter.notifyDataSetChanged();
                    service.SaveFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                        @Override
                        public void onResponse(Call<Feed> call, Response<Feed> response) {
                            if (response.isSuccessful()){
                                Log.w(TAG,response.message());
                            }else {
                                try {
                                    Log.e(TAG,response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Feed> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void editFeed(SmUser user, Feed feed) {
        EditPostNormal(feed);
    }

    @Override
    public void hideFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Hide Post").setMessage("Are you sure to hide this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feeds.remove(feed);
                adapter.notifyDataSetChanged();
                service.HideFeed(feed.getId(),feed.getUser_post().getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    @Override
    public void removeFeed(SmUser user, Feed feed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Post").setMessage("Are you sure to remove this post?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feeds.remove(feed);
                adapter.notifyDataSetChanged();
                service.RemoveFeed(feed.getId(),user.getId()).enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        if (response.isSuccessful()){
                            Log.w(TAG,response.message());
                        }else {
                            try {
                                Log.e(TAG,response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }
}

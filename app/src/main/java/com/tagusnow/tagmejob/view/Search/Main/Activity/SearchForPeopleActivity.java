package com.tagusnow.tagmejob.view.Search.Main.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.v3.search.adapter.SearchForPeopleAdapter;
import com.tagusnow.tagmejob.v3.user.UserCard;
import com.tagusnow.tagmejob.v3.user.UserCardListener;
import com.tagusnow.tagmejob.v3.user.UserCardThree;
import com.tagusnow.tagmejob.v3.user.UserCardThreeListener;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import java.io.IOException;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchForPeopleActivity extends TagUsJobActivity implements UserCardThreeListener {

    private static final String TAG = SearchForPeopleActivity.class.getSimpleName();
    public static final String SEARCH_TEXT = "search_text";
    public static final String CITY = "city";
    private Toolbar toolbar;
    private EditText txtSearch;
    private TextView txtResult;
    private RecyclerView recyclerView;
    private SmUser Auth;
    private String search = "";
    private UserPaginate userPaginate;
    private List<SmUser> users;
    private SearchForPeopleAdapter adapter;
    private UserService service = ServiceGenerator.createService(UserService.class);
    private Callback<UserPaginate> NEW = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Search(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> NEXT = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Next(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void Search(){
        service.UserList(this.Auth.getId(),0,this.search,10).enqueue(NEW);
    }

    private void Search(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users = userPaginate.getData();
        checkNext();
        txtResult.setText(getString(R.string.search_result,userPaginate.getTotal()));
        this.adapter = new SearchForPeopleAdapter(this,users,this);
        recyclerView.setAdapter(this.adapter);
    }

    private void Next(){
        int page = this.userPaginate.getCurrent_page() + 1;
        service.UserList(this.Auth.getId(),0,this.search,10,page).enqueue(NEXT);
    }

    private void Next(UserPaginate userPaginate){
        this.userPaginate = userPaginate;
        this.users.addAll(userPaginate.getData());
        checkNext();
        txtResult.setText(getString(R.string.search_result,userPaginate.getTotal()));
        this.adapter.NextData(userPaginate.getData());
        this.adapter.notifyDataSetChanged();
    }

    private void InitTemp(){
        this.Auth = new Auth(this).checkAuth().token();
        if (getIntent().getStringExtra(SEARCH_TEXT)!=null){
            this.search = getIntent().getStringExtra(SEARCH_TEXT);
        }
    }

    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    Next();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void InitUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtSearch = (EditText)findViewById(R.id.txtSearch);
        txtResult = (TextView)findViewById(R.id.txtResult);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,3, LinearLayoutManager.VERTICAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        txtSearch.setText(this.search);

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search = s.toString();
                Search();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SearchForPeopleActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_for_people);

        InitTemp();
        InitUI();
        Search();
    }

    @Override
    public void connect(UserCardThree view, SmUser user) {
        view.DoFollow(Auth,user);
    }

    @Override
    public void viewProfile(UserCardThree view, SmUser user) {
        view.OpenProfile(Auth,user);
    }

    @Override
    public void remove(UserCardThree view, SmUser user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Close User").setMessage("Are you sure to close this user?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                users.remove(user);
                adapter.notifyDataSetChanged();
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }
}

package com.tagusnow.tagmejob.view.Search.Main.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.JobsSuggestionView;

public class JobsSuggestionHolder extends RecyclerView.ViewHolder {

    private JobsSuggestionView view;

    public JobsSuggestionHolder(View itemView) {
        super(itemView);
        this.view = (JobsSuggestionView)itemView;
    }

    public void BindView(Activity activity, Feed feed, SuggestionListener<JobsSuggestionView> suggestionListener){
        this.view.setActivity(activity);
        this.view.setFeed(feed);
        this.view.setSuggestionListener(suggestionListener);
    }
}

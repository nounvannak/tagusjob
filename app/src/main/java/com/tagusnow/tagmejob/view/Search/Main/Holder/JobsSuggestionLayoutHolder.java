package com.tagusnow.tagmejob.view.Search.Main.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.JobsSuggestionLayout;

public class JobsSuggestionLayoutHolder extends RecyclerView.ViewHolder {

    private JobsSuggestionLayout layout;

    public JobsSuggestionLayoutHolder(View itemView) {
        super(itemView);
        layout = (JobsSuggestionLayout)itemView;
    }

    public void BindView(Activity activity, SuggestionListener<JobsSuggestionLayout> suggestionListener){
        layout.setActivity(activity);
        layout.setSuggestionListener(suggestionListener);
    }
}

package com.tagusnow.tagmejob.view.Search.Main.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.search.SearchForListener;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.SearchForLayout;

public class SearchForHolder extends RecyclerView.ViewHolder {

    private SearchForLayout layout;

    public SearchForHolder(View itemView) {
        super(itemView);
        this.layout = (SearchForLayout)itemView;
    }

    public void BindView(Activity activity, SmUser user, SearchForListener searchForListener){
        layout.setActivity(activity);
        layout.setUser(user);
        layout.setSearchForListener(searchForListener);
    }
}

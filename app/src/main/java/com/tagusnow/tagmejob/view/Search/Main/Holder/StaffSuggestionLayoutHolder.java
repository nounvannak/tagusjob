package com.tagusnow.tagmejob.view.Search.Main.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.StaffSuggestionLayout;

public class StaffSuggestionLayoutHolder extends RecyclerView.ViewHolder {

    private StaffSuggestionLayout layout;

    public StaffSuggestionLayoutHolder(View itemView) {
        super(itemView);
        this.layout = (StaffSuggestionLayout)itemView;
    }

    public void BindView(Activity activity, SuggestionListener<StaffSuggestionLayout> suggestionListener){
        this.layout.setActivity(activity);
        this.layout.setSuggestionListener(suggestionListener);
    }
}

package com.tagusnow.tagmejob.view.Search.Main.Result.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Search.Main.Result.Layout.ContentLayout;

public class ContentHolder extends RecyclerView.ViewHolder {

    private ContentLayout layout;

    public ContentHolder(View itemView) {
        super(itemView);
        this.layout = (ContentLayout)itemView;
    }

    public void BindView(Activity activity, SmUser user,String search){
        this.layout.setActivity(activity);
        this.layout.setSearch(search);
        this.layout.setUser(user);
    }
}

package com.tagusnow.tagmejob.view.Search.Main.Result.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Search.Main.Result.Layout.PeopleLayout;

public class PeopleHolder extends RecyclerView.ViewHolder {

    private PeopleLayout layout;

    public PeopleHolder(View itemView) {
        super(itemView);
        this.layout = (PeopleLayout)itemView;
    }

    public void BindView(Activity activity, SmUser user,String search){
        this.layout.setActivity(activity);
        this.layout.setSearch(search);
        this.layout.setUser(user);
    }
}

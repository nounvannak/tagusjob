package com.tagusnow.tagmejob.view.Search.Main.Result.Holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.Search.Main.Result.Layout.StaffLayout;

public class StaffHolder extends RecyclerView.ViewHolder {

    private StaffLayout layout;

    public StaffHolder(View itemView) {
        super(itemView);
        this.layout = (StaffLayout)itemView;
    }

    public void BindView(Activity activity, SmUser user,int city,String search){
        this.layout.setActivity(activity);
        this.layout.setCity(city);
        this.layout.setSearch(search);
        this.layout.setUser(user);
    }
}

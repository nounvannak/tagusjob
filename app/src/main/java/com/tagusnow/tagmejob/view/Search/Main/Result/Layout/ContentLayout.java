package com.tagusnow.tagmejob.view.Search.Main.Result.Layout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.v3.search.result.SearchForContentAdapter;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForContentActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContentLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = ContentLayout.class.getSimpleName();
    private static final int LIMIT = 3;
    private Context context;
    private int city = 0;
    private String search = "";
    private SmUser user;
    private FeedResponse feedResponse;
    private SearchForContentAdapter adapter;
    private TextView title;
    private RecyclerView recyclerView;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<FeedResponse> SEARCH = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Search(response.body());
            }else {
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private void Search(){
        service.Search(this.user.getId(),3,this.search,0,0,LIMIT).enqueue(SEARCH);
    }

    private void Search(FeedResponse feedResponse){
        if (feedResponse.getTotal()==0){
            this.setVisibility(VISIBLE);
        }else {
            this.setVisibility(VISIBLE);
        }
        this.feedResponse = feedResponse;
        title.setText(this.context.getString(R.string.contents_d,feedResponse.getTotal()));
//        this.adapter = new SearchForContentActivity.Adapter(this.activity,feedResponse,this.user);
//        recyclerView.setAdapter(this.adapter);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.search_result_content_layout,this);
        this.context = context;

        title = (TextView)findViewById(R.id.title);
        title.setOnClickListener(this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public ContentLayout(Context context) {
        super(context);
        InitUI(context);
    }

    public ContentLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public ContentLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public ContentLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public SmUser getUser() {
        return user;
    }

    public void setUser(SmUser user) {
        this.user = user;
        Search();
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v==title){
            this.activity.startActivity(SearchForContentActivity.createIntent(this.activity)
                    .putExtra(SearchForContentActivity.SEARCH_TEXT,this.search)
                    .putExtra(SearchForContentActivity.CITY,this.city)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}

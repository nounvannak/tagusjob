package com.tagusnow.tagmejob.view.Search.Main.Result.Layout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.adapter.ConnectSuggestFaceAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.v3.user.UserCardTwo;
import com.tagusnow.tagmejob.v3.user.UserCardTwoListener;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForPeopleActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PeopleLayout extends TagUsJobRelativeLayout implements View.OnClickListener, UserCardTwoListener {

    private static final String TAG = PeopleLayout.class.getSimpleName();
    private static final int LIMIT = 6;
    private TextView title;
    private RecyclerView recyclerView;
    private Context context;
    private SmUser user;
    private String search = "";
    private UserPaginate userPaginate;
    private List<SmUser> users;
    private ConnectSuggestFaceAdapter adapter;
    private int city = 0;
    private UserService service = ServiceGenerator.createService(UserService.class);
    private Callback<UserPaginate> SEARCH = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Search(response.body());
            }else {
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private void Search(){
        service.UserList(this.user.getId(),0,this.search,LIMIT).enqueue(SEARCH);
    }

    private void Search(UserPaginate userPaginate){
        if (userPaginate.getTotal()==0){
            this.setVisibility(VISIBLE);
        }else {
            this.setVisibility(VISIBLE);
        }
        this.userPaginate = userPaginate;
        this.users = userPaginate.getData();
        title.setText(this.context.getString(R.string.peoples_d,userPaginate.getTotal()));
        this.adapter = new ConnectSuggestFaceAdapter(activity,users,this);
        recyclerView.setAdapter(this.adapter);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.search_result_people_layout,this);
        this.context = context;

        title = (TextView)findViewById(R.id.title);
        title.setOnClickListener(this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,3, LinearLayoutManager.VERTICAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public PeopleLayout(Context context) {
        super(context);
        InitUI(context);
    }

    public PeopleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public PeopleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public PeopleLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setUser(SmUser user) {
        this.user = user;
        Search();
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v==title){
            this.activity.startActivity(SearchForPeopleActivity.createIntent(this.activity)
                    .putExtra(SearchForPeopleActivity.SEARCH_TEXT,this.search)
                    .putExtra(SearchForPeopleActivity.CITY,this.city)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    @Override
    public void connect(UserCardTwo view, SmUser user) {
        view.DoFollow(this.user,user);
    }

    @Override
    public void profile(UserCardTwo view, SmUser user) {
        view.OpenProfile(this.user,user);
    }

    @Override
    public void remove(UserCardTwo view, SmUser user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Close User").setMessage("Are you sure to close this user?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                users.remove(user);
                adapter.notifyDataSetChanged();
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }
}

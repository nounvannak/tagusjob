package com.tagusnow.tagmejob.view.Search.Main.Result.Layout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.v3.search.adapter.StaffSuggestionAdapter;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForJobsActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForStaffActivity;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.StaffSuggestionLayout;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = StaffLayout.class.getSimpleName();
    private static final int LIMIT = 6;
    private Context context;
    private int city = 0;
    private String search = "";
    private SmUser user;
    private FeedResponse feedResponse;
    private StaffSuggestionAdapter adapter;
    private TextView title;
    private RecyclerView recyclerView;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private Callback<FeedResponse> SEARCH = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                Search(response.body());
            }else {
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private void Search(){
        service.Search(this.user.getId(),2,this.search,0,this.city,LIMIT).enqueue(SEARCH);
    }

    private void Search(FeedResponse feedResponse){
        if (feedResponse.getTotal()==0){
            this.setVisibility(VISIBLE);
        }else {
            this.setVisibility(VISIBLE);
        }
        this.feedResponse = feedResponse;
//        title.setText(this.context.getString(R.string.staffs_d,feedResponse.getTotal()));
//        this.adapter = new StaffSuggestionAdapter(this.activity,this.user,feedResponse);
//        recyclerView.setAdapter(this.adapter);
    }

    private void InitUI(Context context){
        inflate(context, R.layout.search_result_staff_layout,this);
        this.context = context;

        title = (TextView)findViewById(R.id.title);
        title.setOnClickListener(this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    public StaffLayout(Context context) {
        super(context);
        InitUI(context);
    }

    public StaffLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public StaffLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public StaffLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public SmUser getUser() {
        return user;
    }

    public void setUser(SmUser user) {
        this.user = user;
        Search();
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v==title){
//            this.activity.startActivity(SearchForStaffActivity.createIntent(this.activity)
//                    .putExtra(SearchForStaffActivity.SEARCH_TEXT,this.search)
//                    .putExtra(SearchForStaffActivity.CITY,this.city)
//                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}

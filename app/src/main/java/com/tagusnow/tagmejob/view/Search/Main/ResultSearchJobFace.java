package com.tagusnow.tagmejob.view.Search.Main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.SearchResultActivity;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.RecentSearch;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResultSearchJobFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = ResultSearchJobFace.class.getSimpleName();
    private Activity activity;
    private Context context;
    private Feed feed;
    private ImageView job_image;
    private TextView title,position,salary,location;
    private CircleImageView profile_picture;
    private boolean isRecent;
    private RecentSearch recentSearch;
    private SmUser user;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private RecentSearchService service = retrofit.create(RecentSearchService.class);
    private Callback<RecentSearch> mCallback = new Callback<RecentSearch>() {
        @Override
        public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                recentSearch = response.body();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearch> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private void saveRecent(){
        int user = this.user!=null ? this.user.getId() : 0;
        service.save(this.recentSearch,user).enqueue(mCallback);
    }

    public ResultSearchJobFace(Context context) {
        super(context);
        inflate(context, R.layout.result_search_job_face,this);
        this.context = context;
        job_image = (ImageView)findViewById(R.id.job_image);
        title = (TextView)findViewById(R.id.title);
        position = (TextView)findViewById(R.id.position);
        salary = (TextView)findViewById(R.id.salary);
        location = (TextView)findViewById(R.id.location);
        profile_picture = (CircleImageView)findViewById(R.id.profile_picture);
        this.recentSearch = new RecentSearch();
        this.setOnClickListener(this);
    }

    public ResultSearchJobFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public ResultSearchJobFace Feed(Feed feed){
        this.feed = feed;
        if (feed!=null){
            if (feed.getType().equals(Feed.TAGUSJOB)){
                if (feed.getFeed().getType().equals(FeedDetail.JOB_POST)){
                    if (feed.getFeed()!=null){
                        if (feed.getFeed().getAlbum()!=null){
                            if (feed.getFeed().getAlbum().get(0)!=null){
                                if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                                    this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                                    Glide.with(this.context)
                                            .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                                            .fitCenter()
                                            .into(job_image);
                                }else {
                                    this.recentSearch.setSearch_image(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"));
                                    Glide.with(this.context)
                                            .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                            .fitCenter()
                                            .into(job_image);
                                }
                            }
                        }

                        title.setText(feed.getFeed().getTitle());
                        this.recentSearch.setSearch_text(feed.getFeed().getTitle());
                        this.recentSearch.setSearch_type(RecentSearch.JOBS);
                        this.recentSearch.setMaster_id(feed.getId());
                        position.setText(feed.getFeed().getCategory());
                        if (feed.getFeed().getSalary()!=null){
                            salary.setVisibility(VISIBLE);
                            salary.setText(feed.getFeed().getSalary());
                        }else {
                            salary.setVisibility(GONE);
                        }

                        if (feed.getFeed().getCity_name()!=null){
                            location.setVisibility(VISIBLE);
                            location.setText(feed.getFeed().getCity_name());
                        }else {
                            location.setVisibility(GONE);
                        }
                    }else {
                        Toast.makeText(this.context,"No Feed Detail",Toast.LENGTH_SHORT).show();
                    }

                    if (feed.getUser_post()!=null){
                        if (feed.getUser_post().getImage()!=null){
                            if (feed.getUser_post().getImage().contains("http")){
                                Glide.with(this.context)
                                        .load(feed.getUser_post().getImage())
                                        .centerCrop()
                                        .error(R.drawable.user_not_found)
                                        .into(profile_picture);
                            }else {
                                Glide.with(this.context)
                                        .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage()))
                                        .centerCrop()
                                        .error(R.drawable.user_not_found)
                                        .into(profile_picture);
                            }
                        }else {
                            Glide.with(this.context)
                                    .load(R.drawable.user_not_found)
                                    .centerCrop()
                                    .error(R.drawable.user_not_found)
                                    .into(profile_picture);
                        }
                    }else {
                        Toast.makeText(this.context,"No User Post",Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Toast.makeText(this.context,"Facebook Feed",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }

        return this;
    }

    private void viewPost(){
        if (this.isRecent){
            saveRecent();
        }
        if (this.activity!=null){
            this.activity.startActivity(SearchResultActivity.createIntent(this.activity).putExtra(SearchResultActivity.FEED,new Gson().toJson(this.feed)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    public ResultSearchJobFace Auth(SmUser user){
        this.user = user;
        if (user!=null){
            this.recentSearch.setSearch_user_id(user.getId());
            this.recentSearch.setUser(user);
        }
        return this;
    }

    public ResultSearchJobFace isRecent(boolean isRecent){
        this.isRecent = isRecent;
        return this;
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            viewPost();
        }
    }
}

package com.tagusnow.tagmejob.view.Search.Main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.SearchResultActivity;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.RecentSearch;
import org.sufficientlysecure.htmltextview.HtmlTextView;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResultSearchPostFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = ResultSearchPostFace.class.getSimpleName();
    private Activity activity;
    private Context context;
    private Feed feed;
    private SmUser user;
    private RecentSearch recentSearch;
    private CircleImageView user_profile;
    private TextView user_name,post_date,follower_counter;
    private HtmlTextView post_description;
    private ImageView post_image;
    private boolean isRecent;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private RecentSearchService service = retrofit.create(RecentSearchService.class);
    private Callback<RecentSearch> mCallback = new Callback<RecentSearch>() {
        @Override
        public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                recentSearch = response.body();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearch> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private void saveRecent(){
        int user = this.user!=null ? this.user.getId() : 0;
        service.save(this.recentSearch,user).enqueue(mCallback);
    }

    public ResultSearchPostFace(Context context) {
        super(context);
        inflate(context, R.layout.result_search_post_face,this);
        this.context = context;
        user_profile = (CircleImageView)findViewById(R.id.user_profile);
        user_name = (TextView)findViewById(R.id.user_name);
        post_date = (TextView)findViewById(R.id.post_date);
        follower_counter = (TextView)findViewById(R.id.follower_counter);
        post_description = (HtmlTextView) findViewById(R.id.post_description);
        post_image = (ImageView)findViewById(R.id.post_image);
        this.recentSearch = new RecentSearch();
        this.setOnClickListener(this);
    }

    public ResultSearchPostFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public ResultSearchPostFace Auth(SmUser user){
        this.user = user;
        if (user!=null){
            this.recentSearch.setSearch_user_id(user.getId());
            this.recentSearch.setUser(user);
        }
        return this;
    }

    public ResultSearchPostFace isRecent(boolean isRecent){
        this.isRecent = isRecent;
        return this;
    }

    public ResultSearchPostFace Feed(Feed feed){
        this.feed = feed;
        if (feed!=null){
            user_name.setText(feed.getUser_post().getUser_name());
            post_date.setText(feed.getFeed().getTimeline());
            this.recentSearch.setMaster_id(feed.getId());
            this.recentSearch.setSearch_type(RecentSearch.JOBS);
            follower_counter.setText(this.context.getString(R.string.follower_counter,feed.getUser_post().getFollower_counter()));
            if (feed.getDescription()!=null){
                post_description.setVisibility(VISIBLE);
                post_description.setHtml(feed.getDescription());
                this.recentSearch.setSearch_text(feed.getFeed().getTitle());
            }else {
                this.recentSearch.setSearch_text(feed.getProfile_name());
                post_description.setVisibility(GONE);
            }
            if (feed.getUser_post().getImage()!=null){
                if (feed.getUser_post().getImage().contains("http")){
                    Glide.with(this.context)
                            .load(feed.getUser_post().getImage())
                            .centerCrop()
                            .error(R.drawable.ic_employee)
                            .into(user_profile);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage()))
                            .centerCrop()
                            .error(R.drawable.ic_employee)
                            .into(user_profile);
                }
            }
            if (feed.getFeed().getAlbum()!=null){
                post_image.setVisibility(VISIBLE);
                if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                    this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                    Glide.with(this.context)
                            .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                            .centerCrop()
                            .into(post_image);
                }else {
                    this.recentSearch.setSearch_image(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"));
                    Glide.with(this.context)
                            .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                            .centerCrop()
                            .error(R.drawable.ic_employee)
                            .into(post_image);
                }
            }else {
                post_image.setVisibility(GONE);
            }
        }
        return this;
    }

    private void viewPost(){
        if (this.isRecent){
            saveRecent();
        }
        if (this.activity!=null){
            this.activity.startActivity(SearchResultActivity.createIntent(this.activity).putExtra(SearchResultActivity.FEED,new Gson().toJson(this.feed)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            viewPost();
        }
    }
}

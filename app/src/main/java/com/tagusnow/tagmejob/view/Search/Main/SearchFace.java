package com.tagusnow.tagmejob.view.Search.Main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.JobDetail;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFace extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = SearchFace.class.getSimpleName();
    private Context context;
    private RecentSearch recentSearch;
    private Feed feed;
    private SmUser user;
    private TextView title,category,location;
    private ImageView logo;
    private boolean isRecent = false;
    private RecentSearchService service = ServiceGenerator.createService(RecentSearchService.class);

    public void setRecent(boolean recent) {
        isRecent = recent;
    }

    private void SaveRecent(){
        service.save(this.recentSearch,this.user.getId()).enqueue(new Callback<RecentSearch>() {
            @Override
            public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
                if (response.isSuccessful()){
                    Log.w(TAG,response.message());
                }else {
                    try {
                        ((TagUsJobActivity)activity).ShowAlert("Error",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RecentSearch> call, Throwable t) {
                ((TagUsJobActivity)activity).ShowAlert("Connection Error",t.getMessage());
            }
        });
    }

    private void initUI(Context context){
        inflate(context, R.layout.search_face,this);
        this.context = context;
        this.recentSearch = new RecentSearch();
        title = (TextView)findViewById(R.id.title);
        category = (TextView)findViewById(R.id.category);
        location = (TextView)findViewById(R.id.location);
        logo = (ImageView)findViewById(R.id.logo);
        this.setOnClickListener(this);
    }

    public SearchFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public SearchFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public SearchFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public SearchFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setRecentSearch(RecentSearch recentSearch) {
        this.recentSearch = recentSearch;
        this.setFeed(recentSearch.getFeed());
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            this.NEW_SHOW_MORE();
            if (this.isRecent){
                this.SaveRecent();
            }
        }
    }

    private void NEW_SHOW_MORE(){
        if (this.activity!=null){
            this.activity.startActivity(JobDetail.createIntent(this.activity).putExtra(JobDetail.FEED,this.feed).putExtra(JobDetail.USER,this.user.getId()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void initFace(Feed feed){
        if (feed!=null){
            if (feed.getFeed().getAlbum()!=null){
                this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                    Glide.with(this.context)
                            .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                            .fitCenter()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(logo);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                            .fitCenter()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(logo);
                }
            }else {
                Glide.with(this.context)
                        .load(R.mipmap.ic_launcher_circle)
                        .fitCenter()
                        .into(logo);
            }
            title.setText(feed.getFeed().getTitle());
            this.recentSearch.setSearch_text(feed.getFeed().getTitle());
            this.recentSearch.setMaster_id(feed.getId());
            this.recentSearch.setSearch_type(RecentSearch.JOBS);
            this.recentSearch.setFeed(feed);
            if (feed.getFeed().getClose_date()!=null){
                category.setVisibility(VISIBLE);
                category.setText(feed.getFeed().getCloseDate());
            }else {
                category.setVisibility(GONE);
            }
            location.setText(feed.getFeed().getCity_name());
        }
    }

    public void setUser(SmUser user) {
        this.user = user;
        if (this.user!=null){
            this.recentSearch.setUser(this.user);
            this.recentSearch.setSearch_user_id(this.user.getId());
        }
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initFace(feed);
    }
}

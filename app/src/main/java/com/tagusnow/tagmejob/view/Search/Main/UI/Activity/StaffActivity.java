package com.tagusnow.tagmejob.view.Search.Main.UI.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.JobsSuggestionLayout;
import com.tagusnow.tagmejob.view.Search.Main.UI.Layout.StaffSuggestionLayout;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffActivity extends TagUsJobActivity implements View.OnClickListener {

    private static final String TAG = StaffActivity.class.getSimpleName();
    private Toolbar toolbar;
    private Spinner spnCity,spnCategory;
    private Button btnSearch;
    private RecyclerView recyclerView;
    private int city = 0;int category = 0;
    private SmLocation cityObj;
    private SmTagSubCategory categoryObj;
    private SmUser user;
    private FeedResponse feedResponse;
//    private StaffSuggestionLayout.Adapter adapter;
    private FeedService service = ServiceGenerator.createService(FeedService.class);

    private Callback<FeedResponse> NEW = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                NewData(response.body());
            }else {
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private  Callback<FeedResponse> NEXT = new Callback<FeedResponse>() {
        @Override
        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
            if (response.isSuccessful()){
                NextData(response.body());
            }else {
                try {
                    ShowAlert("Error!",response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<FeedResponse> call, Throwable t) {
            ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private void NewData(){
        service.Suggest(this.user.getId(),1,2,this.category,this.city,10).enqueue(NEW);
    }

    private void NewData(FeedResponse feedResponse){
        dismissProgress();
        this.feedResponse = feedResponse;
        this.CheckNext();
//        this.adapter = new StaffSuggestionLayout.Adapter(this,this.user,feedResponse);
//        this.recyclerView.setAdapter(this.adapter);
    }

    private void NextData(){
        int page = this.feedResponse.getCurrent_page() + 1;
        service.Suggest(this.user.getId(),1,2,this.category,this.city,10,page).enqueue(NEXT);
    }

    private void NextData(FeedResponse feedResponse){
        this.feedResponse = feedResponse;
        this.CheckNext();
//        this.adapter.Update(feedResponse).notifyDataSetChanged();
    }

    private void CheckNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (feedResponse.getTo() - 1)) && (feedResponse.getTo() < feedResponse.getTotal())) {
                        if (feedResponse.getNext_page_url()!=null || !feedResponse.getNext_page_url().equals("")){
                            if (lastPage!=feedResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = feedResponse.getCurrent_page();
                                    NextData();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void InitTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.cityObj = new Repository(this).restore().getLocation();
        this.categoryObj = new Repository(this).restore().getCategory();
    }

    private void InitUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSearch = (Button)findViewById(R.id.btnSearch);
        spnCategory = (Spinner)findViewById(R.id.spnCategory);
        spnCity = (Spinner)findViewById(R.id.spnCity);
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,2, LinearLayoutManager.VERTICAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        btnSearch.setOnClickListener(this);

        if (this.cityObj!=null){
            List<String> listCity = new ArrayList<String>();
            listCity.add("All Location");
            for (SmLocation.Data data : this.cityObj.getData()){
                listCity.add(data.getName());
            }
            spnCity.setAdapter(new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,listCity));
            spnCity.setSelection(this.city);
        }

        if (this.categoryObj!=null){
            List<String> listCategory = new ArrayList<String>();
            listCategory.add("All Position");
            for (Category data : this.categoryObj.getData()){
                listCategory.add(data.getTitle());
            }
            spnCategory.setAdapter(new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,listCategory));
            spnCategory.setSelection(this.category);
        }

        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                city = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,StaffActivity.class);
    }

    @Override
    public void onClick(View v) {
        if (v==btnSearch){
            showProgress();
            NewData();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff);

        InitTemp();
        InitUI();
        NewData();
    }
}

package com.tagusnow.tagmejob.view.Search.Main.UI.Layout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.v3.search.adapter.JobsSuggestionAdapter;
import com.tagusnow.tagmejob.view.Search.Main.Holder.JobsSuggestionHolder;
import com.tagusnow.tagmejob.view.Search.Main.UI.Activity.JobsSuggestActivity;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.JobsSuggestionView;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import java.io.IOException;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobsSuggestionLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    public RecyclerView recyclerView;
    private ImageButton btnMore;
    private JobsSuggestionAdapter adapter;
    private SuggestionListener<JobsSuggestionLayout> suggestionListener;

    private void InitUI(Context context){
        inflate(context, R.layout.main_search_suggest_job_layout,this);
        this.context = context;

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        btnMore = (ImageButton)findViewById(R.id.btnMore);
        btnMore.setOnClickListener(this);
    }

    public JobsSuggestionLayout(Context context) {
        super(context);
        this.InitUI(context);
    }

    public JobsSuggestionLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public JobsSuggestionLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public JobsSuggestionLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v==btnMore){
            suggestionListener.showMore(this);
        }
    }

    public void setSuggestionListener(SuggestionListener<JobsSuggestionLayout> suggestionListener) {
        this.suggestionListener = suggestionListener;
    }

    public void setAdapter(JobsSuggestionAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }
}

package com.tagusnow.tagmejob.view.Search.Main.UI.Layout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.v3.search.SearchForListener;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForContentActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForJobsActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForPeopleActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForStaffActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class SearchForLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private TextView title;
    private Context context;
    private RelativeLayout people,company,staffAndJob,content;
    private ImageView iconOfStaffAndJob;
    private TextView titleOfStaffAndJob;
    private SmUser user;
    private SearchForListener searchForListener;

    private void InitUI(Context context){
        inflate(context, R.layout.main_search_search_for_layout,this);
        this.context = context;

        title = (TextView)findViewById(R.id.title);

        people = (RelativeLayout)findViewById(R.id.people);
        company = (RelativeLayout)findViewById(R.id.company);
        staffAndJob = (RelativeLayout)findViewById(R.id.staffAndJob);
        content = (RelativeLayout)findViewById(R.id.content);

        iconOfStaffAndJob = (ImageView)findViewById(R.id.icon2);
        titleOfStaffAndJob = (TextView)findViewById(R.id.title2);

        people.setOnClickListener(this);
        company.setOnClickListener(this);
        staffAndJob.setOnClickListener(this);
        content.setOnClickListener(this);
    }

    public SearchForLayout(Context context) {
        super(context);
        this.InitUI(context);
    }

    public SearchForLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public SearchForLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public SearchForLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v == people){
            searchForListener.SearchForPeople(this);
        }else if (v == company){
            searchForListener.SearchForCompany(this);
        }else if (v == staffAndJob){
            searchForListener.SearchForStaffAndJob(this);
        }else if (v == content){
            searchForListener.SearchForContent(this);
        }
    }

    public void setSearchForListener(SearchForListener searchForListener) {
        this.searchForListener = searchForListener;
    }

    public void setUser(SmUser user) {
        this.user = user;

        if (user.getUser_type() == SmUser.FIND_JOB){
            Glide.with(context).load(R.drawable._suitcase).into(iconOfStaffAndJob);
            titleOfStaffAndJob.setText(context.getText(R.string.jobs));
        }else {
            Glide.with(context).load(R.drawable._person).into(iconOfStaffAndJob);
            titleOfStaffAndJob.setText(context.getText(R.string.staff));
        }
    }
}

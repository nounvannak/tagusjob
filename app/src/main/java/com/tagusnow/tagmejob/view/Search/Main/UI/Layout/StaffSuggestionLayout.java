package com.tagusnow.tagmejob.view.Search.Main.UI.Layout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedResponse;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.v3.search.adapter.StaffSuggestionAdapter;
import com.tagusnow.tagmejob.view.Search.Main.Holder.JobsSuggestionHolder;
import com.tagusnow.tagmejob.view.Search.Main.Holder.StaffSuggestionViewHolder;
import com.tagusnow.tagmejob.view.Search.Main.UI.Activity.JobsSuggestActivity;
import com.tagusnow.tagmejob.view.Search.Main.UI.Activity.StaffActivity;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.JobsSuggestionView;
import com.tagusnow.tagmejob.view.Search.Main.UI.View.StaffSuggestionView;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffSuggestionLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    public RecyclerView recyclerView;
    private ImageButton btnMore;
    private StaffSuggestionAdapter adapter;
    private SuggestionListener<StaffSuggestionLayout> suggestionListener;

    private void InitUI(Context context){
        inflate(context, R.layout.main_search_suggest_staff_layout,this);
        this.context = context;

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        btnMore = (ImageButton)findViewById(R.id.btnMore);
        btnMore.setOnClickListener(this);
    }

    public StaffSuggestionLayout(Context context) {
        super(context);
        InitUI(context);
    }

    public StaffSuggestionLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public StaffSuggestionLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public StaffSuggestionLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v==btnMore){
            suggestionListener.showMore(this);
        }
    }

    public void setSuggestionListener(SuggestionListener<StaffSuggestionLayout> suggestionListener) {
        this.suggestionListener = suggestionListener;
    }

    public void setAdapter(StaffSuggestionAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }
}

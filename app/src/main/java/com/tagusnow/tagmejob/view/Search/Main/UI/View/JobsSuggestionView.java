package com.tagusnow.tagmejob.view.Search.Main.UI.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.ApplyJobActivity;
import com.tagusnow.tagmejob.JobDetail;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class JobsSuggestionView extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = JobsSuggestionView.class.getSimpleName();
    private Context context;
    private TextView title,salary,location,close_date;
    private ImageView image;
    private Button btnApply;
    private Feed feed;
    private SuggestionListener<JobsSuggestionView> suggestionListener;

    private void InitUI(Context context){
        inflate(context, R.layout.main_search_jobs_view,this);
        this.context = context;

        title = (TextView)findViewById(R.id.title);
        salary = (TextView)findViewById(R.id.salary);
        location = (TextView)findViewById(R.id.location);
        close_date = (TextView)findViewById(R.id.close_date);

        image = (ImageView)findViewById(R.id.image);

        btnApply = (Button)findViewById(R.id.btnApply);
        btnApply.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public JobsSuggestionView(Context context) {
        super(context);
        InitUI(context);
    }

    public JobsSuggestionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public JobsSuggestionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public JobsSuggestionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (v==btnApply){
            suggestionListener.applyJob(this,feed);
        }else if (v==this){
            suggestionListener.viewDetail(this,feed);
        }
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        title.setText(feed.getFeed().getTitle());
        salary.setText((feed.getFeed().getSalary() != null && !feed.getFeed().getSalary().equals("")) ? feed.getFeed().getSalary() : "N/A");
        close_date.setText(GetDate(feed.getFeed().getClose_date(),DATE_FORMAT_yyyy_MM_dd_HH_mm_ss,DATE_FORMAT_dd_s_MM_s_yyyy));
        location.setText((feed.getFeed().getCity_name() != null && !feed.getFeed().getCity_name().equals("")) ? feed.getFeed().getCity_name() : "N/A");
        setAlbumPhoto(image,(feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0) ? feed.getFeed().getAlbum().get(0) : null,true);
    }

    public void setSuggestionListener(SuggestionListener<JobsSuggestionView> suggestionListener) {
        this.suggestionListener = suggestionListener;
    }
}

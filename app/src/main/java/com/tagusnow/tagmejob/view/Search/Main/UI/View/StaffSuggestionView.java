package com.tagusnow.tagmejob.view.Search.Main.UI.View;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Message.MessageBoxV2;
import com.tagusnow.tagmejob.V2.Message.Model.Dialog;
import com.tagusnow.tagmejob.V2.Message.Model.Message;
import com.tagusnow.tagmejob.V2.Message.Model.User;
import com.tagusnow.tagmejob.ViewCardCVActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.v3.search.SuggestionListener;
import com.tagusnow.tagmejob.view.CVAndResumeLayout;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffSuggestionView extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ShapedImageView profile;
    private TextView name,type,location;
    private RelativeLayout btnChat,btnDownload;
    private Feed feed;
    private SuggestionListener<StaffSuggestionView> suggestionListener;

    private void InitUI(Context context){
        try {
            inflate(context, R.layout.main_search_suggestion_staff_view,this);
            this.context = context;

            profile = (ShapedImageView)findViewById(R.id.profile);

            name = (TextView)findViewById(R.id.name);
            type = (TextView)findViewById(R.id.type);
            location = (TextView)findViewById(R.id.location);

            btnChat = (RelativeLayout)findViewById(R.id.btnChat);
            btnDownload = (RelativeLayout)findViewById(R.id.btnDownload);

            btnChat.setOnClickListener(this);
            btnDownload.setOnClickListener(this);
            this.setOnClickListener(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public StaffSuggestionView(Context context) {
        super(context);
        InitUI(context);
    }

    public StaffSuggestionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public StaffSuggestionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public StaffSuggestionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        setName(name,feed.getUser_post());
        type.setText(feed.getFeed().getTitle());
        location.setText(feed.getFeed().getCity_name());
        setAlbumPhoto(profile,(feed.getFeed().getAlbum() != null && feed.getFeed().getAlbum().size() > 0) ? feed.getFeed().getAlbum().get(0) : null);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View v) {
        if (btnDownload==v){
            suggestionListener.download(this,feed);
        }else if (btnChat==v){
            suggestionListener.chat(this,feed);
        }else if (this==v){
            suggestionListener.viewDetail(this,feed);
        }
    }

    public void setSuggestionListener(SuggestionListener<StaffSuggestionView> suggestionListener) {
        this.suggestionListener = suggestionListener;
    }
}

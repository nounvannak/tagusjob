package com.tagusnow.tagmejob.view.Search;

import android.content.Context;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;

public class NotFound extends RelativeLayout{

    public NotFound(Context context) {
        super(context);
        inflate(context, R.layout.not_found_layout,this);
    }
}

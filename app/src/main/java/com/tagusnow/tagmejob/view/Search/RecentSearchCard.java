package com.tagusnow.tagmejob.view.Search;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.adapter.RecentSearchCardAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;

import java.util.List;

public class RecentSearchCard extends CardView{
    private Activity activity;
    private Context context;
    private TextView date;
    private String strDate;
    private RecyclerView subRecycler;
    private SmUser auth;
    private RecentSearchPaginate recentSearchPaginate;
    private RecentSearchCardAdapter adapter;

    public RecentSearchCard(@NonNull Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.recent_search_card,this);
        date = (TextView)findViewById(R.id.date);
        subRecycler = (RecyclerView)findViewById(R.id.subRecycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        subRecycler.setHasFixedSize(true);
        subRecycler.setLayoutManager(layoutManager);
    }

    public String getDate() {
        return strDate;
    }

    public RecentSearchCard setDate(String strDate) {
        this.strDate = strDate;
        if (strDate!=null){
            date.setText(strDate);
        }
        return this;
    }

    public SmUser getAuth() {
        return auth;
    }

    public RecentSearchCard setAuth(SmUser auth) {
        this.auth = auth;
        return this;
    }

    public RecentSearchPaginate getRecentSearchPaginate() {
        return recentSearchPaginate;
    }

    public void setRecentSearchPaginate(RecentSearchPaginate recentSearchPaginate) {
        if (recentSearchPaginate.getTo() < recentSearchPaginate.getTotal()){
            recentSearchPaginate.setTo(recentSearchPaginate.getTo() + RecentSearchCardAdapter.FOOTER);
        }
        this.recentSearchPaginate = recentSearchPaginate;
        this.adapter = new RecentSearchCardAdapter(this.activity,recentSearchPaginate,this.auth);
        this.subRecycler.setAdapter(adapter);
    }

    public RecentSearchCard setActivity(Activity activity) {
        this.activity = activity;
        return this;
    }
}

package com.tagusnow.tagmejob.view.Search;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.adapter.RecentSearchAdapter;
import com.tagusnow.tagmejob.adapter.RecentSearchCardAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearchPaginate;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecentSearchLoadMore extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = RecentSearchLoadMore.class.getSimpleName();
    private Button btnLoadMore;
    private Activity activity;
    private Context context;
    private RecentSearchPaginate recentSearchPaginate;
    private RecentSearchCardAdapter adapter;
    private SmUser auth;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private RecentSearchService service = retrofit.create(RecentSearchService.class);
    private Callback<RecentSearchPaginate> mCallback = new Callback<RecentSearchPaginate>() {
        @Override
        public void onResponse(Call<RecentSearchPaginate> call, Response<RecentSearchPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNext(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearchPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public RecentSearchLoadMore(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.recent_search_load_more,this);
        btnLoadMore = (Button)findViewById(R.id.btnLoadMore);
        btnLoadMore.setOnClickListener(this);
    }

    public RecentSearchLoadMore Adapter(RecentSearchCardAdapter adapter){
        this.adapter = adapter;
        return this;
    }

    public RecentSearchLoadMore Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public RecentSearchLoadMore Auth(SmUser auth){
        this.auth = auth;
        return this;
    }

    public RecentSearchLoadMore RecentSearchPaginate(RecentSearchPaginate recentSearchPaginate){
        this.recentSearchPaginate = recentSearchPaginate;
        return this;
    }

    private void onLoadMore(){
        if (this.recentSearchPaginate!=null){
            int user = this.auth!=null ? this.auth.getId() : 0;
            int limit = Integer.parseInt(this.recentSearchPaginate.getPer_page());
            int page = this.recentSearchPaginate.getCurrent_page() + 1;
            this.service.hasRecentSearch(user,limit,page).enqueue(mCallback);
        }
    }

    private void initNext(RecentSearchPaginate recentSearchPaginate){
        if (recentSearchPaginate.getTotal() > recentSearchPaginate.getTo()){
            recentSearchPaginate.setTo(recentSearchPaginate.getTo() + RecentSearchCardAdapter.FOOTER);
        }
        this.recentSearchPaginate = recentSearchPaginate;
        this.adapter.update(this.activity,recentSearchPaginate).notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (view==btnLoadMore){
            onLoadMore();
        }
    }
}

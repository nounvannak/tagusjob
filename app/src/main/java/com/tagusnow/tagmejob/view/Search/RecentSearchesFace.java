package com.tagusnow.tagmejob.view.Search;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.SearchViewNoticeActivity;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;
import java.io.IOException;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecentSearchesFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = RecentSearchesFace.class.getSimpleName();
    private CircleImageView search_picture;
    private TextView search_text;
    private Button btnRemove;
    private Context context;
    private Activity activity;
    private SmUser auth;
    private RecentSearch recentSearch;
    /*Service*/
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    private RecentSearchService service = retrofit.create(RecentSearchService.class);
    private Callback<RecentSearch> mCallback = new Callback<RecentSearch>() {
        @Override
        public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                recentSearch = response.body();
                if (recentSearch.getTrash()==1){
                    RecentSearchesFace.this.setVisibility(GONE);
                }else {
                    RecentSearchesFace.this.setVisibility(VISIBLE);
                }
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<RecentSearch> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public RecentSearch getRecentSearch() {
        return recentSearch;
    }

    public RecentSearchesFace(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.recent_searches_face,this);
        search_picture = (CircleImageView)findViewById(R.id.search_picture);
        search_text = (TextView)findViewById(R.id.search_text);
        btnRemove = (Button)findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public RecentSearchesFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public RecentSearchesFace Auth(SmUser auth){
        this.auth = auth;
        return this;
    }

    public RecentSearchesFace RecentSearch(RecentSearch recentSearch){
        this.recentSearch = recentSearch;

        if (recentSearch!=null){
            search_text.setText(recentSearch.getSearch_text());
            if (recentSearch.getSearch_type()==RecentSearch.WORD){
                Glide.with(this.context)
                        .load(R.drawable.ic_search_black_24dp)
                        .centerCrop()
                        .into(search_picture);
            }else if (recentSearch.getSearch_type()==RecentSearch.USER){
                if (recentSearch.getSearch_image()!=null){
                    if (recentSearch.getSearch_image().contains("http")){
                        Glide.with(this.context)
                                .load(recentSearch.getSearch_image())
                                .centerCrop()
                                .error(R.drawable.ic_user_male_icon)
                                .into(search_picture);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getProfilePicture(recentSearch.getSearch_image()))
                                .centerCrop()
                                .error(R.drawable.ic_user_male_icon)
                                .into(search_picture);
                    }
                }else{
                    Glide.with(this.context)
                            .load(R.drawable.ic_search_black_24dp)
                            .centerCrop()
                            .into(search_picture);
                }
            }else if(recentSearch.getSearch_type()==RecentSearch.JOBS){

            }else{

            }
        }else{
            Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
        }

        return this;
    }

    private void show(){
        if (this.recentSearch.getSearch_type()==RecentSearch.WORD){
            if (this.activity!=null){
                this.context.startActivity(SearchViewNoticeActivity.createIntent(this.activity,this.recentSearch.getSearch_text()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }else {
                Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
            }
        }else if (this.recentSearch.getSearch_type()==RecentSearch.USER){
            viewProfile();
        }else if(this.recentSearch.getSearch_type()==RecentSearch.JOBS){

        }else{

        }
    }

    private void viewProfile(){
        if (this.activity!=null){
            if (this.recentSearch.getUser()!=null && this.auth!=null){
                if (this.recentSearch.getUser().getId()==this.auth.getId()){
                    this.context.startActivity(ProfileActivity.createIntent(this.activity));
                }else {
                    this.context.startActivity(ViewProfileActivity.createIntent(this.activity).putExtra("profileUser",new Gson().toJson(this.recentSearch.getUser())).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            }else {
                Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void remove(){
        if (this.recentSearch!=null){
            int user = this.auth!=null ? this.auth.getId() : 0;
            this.recentSearch.setTrash(1);
            service.destroy(this.recentSearch,user).enqueue(mCallback);
        }else {
            Toast.makeText(this.context,"No Recent Search",Toast.LENGTH_SHORT).show();
        }
    }

    private DialogInterface.OnClickListener mYes = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            remove();
        }
    };

    private DialogInterface.OnClickListener mNo = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    };

    private void onRemove(){
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!activity.isFinishing()){
                    new AlertDialog.Builder(activity)
                            .setNegativeButton("No",mNo)
                            .setPositiveButton("Yes",mYes)
                            .setTitle("Clear searches histories")
                            .setMessage("Are you sure to clear this histories ?")
                            .setCancelable(false)
                            .create()
                            .show();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view==btnRemove){
            onRemove();
        }else if(view==this){
            show();
        }
    }
}

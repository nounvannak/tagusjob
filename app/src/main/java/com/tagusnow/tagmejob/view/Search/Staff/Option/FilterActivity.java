package com.tagusnow.tagmejob.view.Search.Staff.Option;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class FilterActivity extends TagUsJobActivity {

    private static final String TAG = FilterActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_resume);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,FilterActivity.class);
    }
}

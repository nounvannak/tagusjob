package com.tagusnow.tagmejob.view.Search.Staff.Option;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class MoreActivity extends TagUsJobActivity {

    private static final String TAG = MoreActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_resume);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,MoreActivity.class);
    }
}

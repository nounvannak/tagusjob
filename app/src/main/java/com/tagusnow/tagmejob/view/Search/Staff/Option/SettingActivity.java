package com.tagusnow.tagmejob.view.Search.Staff.Option;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

public class SettingActivity extends TagUsJobActivity {

    private static final String TAG = SettingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,SettingActivity.class);
    }
}

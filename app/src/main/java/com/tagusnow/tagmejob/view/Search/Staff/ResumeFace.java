package com.tagusnow.tagmejob.view.Search.Staff;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import de.hdodenhof.circleimageview.CircleImageView;

public class ResumeFace extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private CircleImageView profile;
    private TextView user_name,location,skill;
    private SmUser user;

    private void initUI(Context context){
        inflate(context, R.layout.layout_resume_face,this);
        this.context = context;
        profile = (CircleImageView)findViewById(R.id.profile);
        user_name = (TextView)findViewById(R.id.user_name);
        location = (TextView)findViewById(R.id.location);
        skill = (TextView)findViewById(R.id.skill);
        this.setOnClickListener(this);
    }

    public ResumeFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public ResumeFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public ResumeFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public ResumeFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view==this){

        }
    }

    public SmUser getUser() {
        return user;
    }

    @Override
    protected Activity getActivity() {
        return super.getActivity();
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setUser(SmUser user) {
        this.user = user;

        if (user!=null){
            if (user.getImage()!=null){
                if (user.getImage().contains("http")){
                    Glide.with(this.context).load(user.getImage()).centerCrop().error(R.mipmap.ic_launcher_circle).into(profile);
                }else {
                    Glide.with(this.context).load(SuperUtil.getProfilePicture(user.getImage(),"200")).centerCrop().error(R.mipmap.ic_launcher_circle).into(profile);
                }
            }else {
                Glide.with(this.context).load(R.mipmap.ic_launcher_circle).centerCrop().into(profile);
            }

            user_name.setText(user.getUser_name());
            if (user.getLocation()!=null){
                location.setText(user.getLocation());
            }

            if (user.getSkill_name()!=null){
                skill.setText(user.getSkill_name());
            }
        }
    }
}

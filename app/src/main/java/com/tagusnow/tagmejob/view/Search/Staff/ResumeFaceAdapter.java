package com.tagusnow.tagmejob.view.Search.Staff;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.tagusnow.tagmejob.auth.SmUser;
import java.util.List;

public class ResumeFaceAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private Context context;
    private List<SmUser> users;

    public ResumeFaceAdapter(Activity activity,List<SmUser> users){
        this.activity = activity;
        this.context = activity;
        this.users = users;
    }

    public ResumeFaceAdapter Update(Activity activity,List<SmUser> users){
        this.activity = activity;
        this.context = activity;
        this.users.addAll(users);
        return this;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ResumeFaceHolder(new ResumeFace(this.context));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ResumeFaceHolder) holder).bindView(this.activity,this.users.get(position));
    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }
}

package com.tagusnow.tagmejob.view.Search.Staff;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;

public class ResumeFaceHolder extends RecyclerView.ViewHolder {

    private ResumeFace resumeFace;

    public ResumeFaceHolder(View itemView) {
        super(itemView);
        this.resumeFace = (ResumeFace)itemView;
    }

    public void bindView(Activity activity, SmUser user){
        this.resumeFace.setActivity(activity);
        this.resumeFace.setUser(user);
    }
}

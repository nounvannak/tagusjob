package com.tagusnow.tagmejob.view.Search.Staff;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.CVService;
import com.tagusnow.tagmejob.REST.Service.PostService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;
import com.tagusnow.tagmejob.view.Search.Staff.Option.FilterActivity;
import com.tagusnow.tagmejob.view.Search.Staff.Option.MoreActivity;
import com.tagusnow.tagmejob.view.Search.Staff.Option.SettingActivity;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResumeSearchActivity extends TagUsJobActivity implements View.OnClickListener {

    private static final String TAG = ResumeSearchActivity.class.getSimpleName();
    private static final int LIMIT = 10;
    private Toolbar toolbar;
    private EditText search_box;
    private TextView result,search_text;
    private RelativeLayout btnSort,btnFilter,btnMore;
    private LinearLayout layout_result;
    private RecyclerView recycler;
    private ResumeFaceAdapter adapter;
    private UserPaginate paginate;
    private SmUser user;
    private String query;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private CVService service = retrofit.create(CVService.class);
    private Callback<UserPaginate> mNew = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                New(response.body());
            }else {
                try {
                    ShowAlert(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> mNext = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Next(response.body());
            }else {
                try {
                    ShowAlert(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void First(){
//        this.query = "a";
        service.ResumeSearch(this.user.getId(),CVService.FIRST_REQUEST,"").enqueue(mNew);
    }

    private void New(String query){
        this.query = query;
        service.ResumeSearch(this.user.getId(),CVService.REQUEST,query).enqueue(mNew);
    }

    private void Next(String query){
        int page = this.paginate.getCurrent_page() + 1;
        service.ResumeSearch(this.user.getId(),CVService.REQUEST,query,LIMIT,page).enqueue(mNext);
    }

    private void Result(UserPaginate paginate){
        if (paginate!=null){
            this.result.setText(getString(R.string._24_results,paginate.getTotal()));
            this.search_text.setText(this.query);
        }
    }

    private void New(UserPaginate paginate){
        this.paginate = paginate;
        if (paginate==null){
            this.layout_result.setVisibility(View.GONE);
        }else {
            this.layout_result.setVisibility(View.VISIBLE);
            this.result.setText(getString(R.string._24_results,paginate.getTotal()));
            this.search_text.setText(this.query);
        }
        this.CheckNext();
        this.Result(paginate);
        this.adapter = new ResumeFaceAdapter(this,paginate.getData());
        this.recycler.setAdapter(this.adapter);
    }

    private void Next(UserPaginate paginate){
        this.paginate = paginate;
        if (paginate==null){
            this.layout_result.setVisibility(View.GONE);
        }else {
            this.layout_result.setVisibility(View.VISIBLE);
            this.result.setText(getString(R.string._24_results,paginate.getTotal()));
            this.search_text.setText(this.query);
        }
        this.CheckNext();
        this.Result(paginate);
        this.adapter.Update(this,paginate.getData()).notifyDataSetChanged();
    }

    private void CheckNext(){
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (paginate.getTo() - 1)) && (paginate.getTo() < paginate.getTotal())) {
                        if (paginate.getNext_page_url()!=null || !paginate.getNext_page_url().equals("")){
                            if (lastPage!=paginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = paginate.getCurrent_page();
                                    Next(query);
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_search);
        this.initTemp();
        this.initUI();
        if (this.paginate==null){
            this.layout_result.setVisibility(View.GONE);
            this.First();
        }else {
            this.layout_result.setVisibility(View.VISIBLE);
            this.New(this.query);
        }
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,ResumeSearchActivity.class);
    }

    private void initTemp(){
        this.user = new Auth(this).checkAuth().token();
    }

    private void initUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        search_box = (EditText)findViewById(R.id.search_box);
        result = (TextView)findViewById(R.id.result);
        search_text = (TextView)findViewById(R.id.search_text);

        layout_result = (LinearLayout) findViewById(R.id.layout_result);

        btnSort = (RelativeLayout)findViewById(R.id.btnSort);
        btnFilter = (RelativeLayout)findViewById(R.id.btnFilter);
        btnMore = (RelativeLayout)findViewById(R.id.btnMore);

        btnSort.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        btnMore.setOnClickListener(this);

        recycler = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(manager);

        search_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                New(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_resume,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.btnSetting:
                startActivity(SettingActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (btnSort==view){
            Toast.makeText(this,"Sort",Toast.LENGTH_SHORT).show();
        }else if (btnFilter==view){
            startActivity(FilterActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (btnMore==view){
            startActivity(MoreActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}

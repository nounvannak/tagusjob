package com.tagusnow.tagmejob.view.Search;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;

public class TotalResult extends RelativeLayout{

    private TextView textView;
    private Context context;

    public TotalResult(Context context) {
        super(context);
        this.context = context;
        inflate(context,R.layout.total_result_layout,this);
        textView = (TextView)findViewById(R.id.total_result);
    }

    public void setTotal(int total){
        textView.setText(this.context.getString(R.string.results,total));
    }
}

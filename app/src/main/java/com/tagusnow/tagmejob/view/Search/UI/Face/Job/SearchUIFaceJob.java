package com.tagusnow.tagmejob.view.Search.UI.Face.Job;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.ApplyJobActivity;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.JobDetail;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourFour;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourOne;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourThree;
import com.tagusnow.tagmejob.view.Grid.Four.GridFourTwo;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeFour;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeOne;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeThree;
import com.tagusnow.tagmejob.view.Grid.Three.GridThreeTwo;
import com.tagusnow.tagmejob.view.Grid.Two.GridTwoOne;
import com.tagusnow.tagmejob.view.Grid.Two.GridTwoTwo;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUIFaceJob extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private Feed feed;
    private SmUser user;
    private RecyclerView.Adapter adapter;
    private int position;
    private CircleImageView profile_picture;
    private TextView like_result,comment_result,like_text;
    private View like_and_comment_border,border_body;
    private RelativeLayout like_and_comment_information;
    private LinearLayout like_button,comment_button,share_button;
    private ImageView like_icon;
    private ImageView job_image;
    private TextView title,category,salary,location;
    private Button btnApply;
    private Socket socket;
    private boolean isRecent = false;
    private RecentSearch recentSearch;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private RecentSearchService searchService = ServiceGenerator.createService(RecentSearchService.class);

    private void initUI(Context context){
        inflate(context, R.layout.search_ui_face_job,this);
        this.context = context;
        profile_picture = (CircleImageView)findViewById(R.id.profile_picture);
        like_result  = (TextView)findViewById(R.id.like_result);
        comment_result = (TextView)findViewById(R.id.comment_result);
        like_text =(TextView)findViewById(R.id.like_text);
        like_and_comment_border = (View)findViewById(R.id.like_and_comment_border);
        border_body = (View)findViewById(R.id.border_body);
        like_and_comment_information = (RelativeLayout)findViewById(R.id.like_and_comment_information);
        like_button = (LinearLayout)findViewById(R.id.like_button);
        comment_button = (LinearLayout)findViewById(R.id.comment_button);
        share_button = (LinearLayout)findViewById(R.id.share_button);
        like_icon = (ImageView)findViewById(R.id.like_icon);
        job_image = (ImageView)findViewById(R.id.job_image);
        title = (TextView)findViewById(R.id.title);
        category = (TextView)findViewById(R.id.position);
        salary = (TextView)findViewById(R.id.salary);
        location =(TextView)findViewById(R.id.location);
        btnApply = (Button)findViewById(R.id.btnApply);
        recentSearch = new RecentSearch();
        profile_picture.setOnClickListener(this);
        like_button.setOnClickListener(this);
        comment_button.setOnClickListener(this);
        share_button.setOnClickListener(this);
        btnApply.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public SearchUIFaceJob(Context context) {
        super(context);
        this.initUI(context);
    }

    public SearchUIFaceJob(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public SearchUIFaceJob(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public SearchUIFaceJob(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initSocket();
        this.initFeed(feed);
    }

    public void setUser(SmUser user) {
        this.user = user;
        this.recentSearch.setSearch_user_id(this.user.getId());
        this.recentSearch.setUser(this.user);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private void initFeed(Feed feed){
        if (feed!=null){
            if (feed.getUser_post().getImage()!=null){
                if (feed.getUser_post().getImage().contains("http")){
                    Glide.with(this.context)
                            .load(feed.getUser_post().getImage())
                            .centerCrop()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(profile_picture);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage(),"500"))
                            .centerCrop()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(profile_picture);
                }
            }else {
                Glide.with(this.context)
                        .load(R.mipmap.ic_launcher_circle)
                        .centerCrop()
                        .into(profile_picture);
            }

            this.recentSearch.setMaster_id(feed.getId());
            this.recentSearch.setSearch_text(feed.getFeed().getTitle());
            this.recentSearch.setSearch_type(RecentSearch.JOBS);

            title.setText(feed.getFeed().getTitle());
            category.setText(feed.getFeed().getCategory());
            if (feed.getFeed().getSalary()!=null){
                salary.setVisibility(VISIBLE);
                salary.setText(feed.getFeed().getSalary());
            }else {
                salary.setVisibility(GONE);
            }

            if (feed.getFeed().getCity_name()!=null){
                location.setVisibility(VISIBLE);
                location.setText(feed.getFeed().getCity_name());
            }else {
                location.setVisibility(GONE);
            }

            if (feed.getFeed().getCount_comments() > 0 || feed.getFeed().getCount_likes() > 0){
                like_and_comment_border.setVisibility(VISIBLE);
                like_and_comment_information.setVisibility(VISIBLE);
            }else {
                like_and_comment_border.setVisibility(GONE);
                like_and_comment_information.setVisibility(GONE);
            }

            if (feed.getFeed().getCount_likes() > 0){
                like_result.setVisibility(VISIBLE);
            }else {
                like_result.setVisibility(GONE);
            }

            if (feed.getFeed().getCount_comments() > 0){
                comment_result.setVisibility(VISIBLE);
            }else {
                comment_result.setVisibility(GONE);
            }

            like_result.setText(feed.getFeed().getLike_result());
            comment_result.setText(this.context.getString(R.string._d_comments,feed.getFeed().getCount_comments()));

            if (feed.getFeed().isIs_like()){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    like_text.setTextColor(this.context.getColor(R.color.colorAccent));
                }
                like_icon.setImageResource(R.drawable.ic_action_like_blue);
            }else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    like_text.setTextColor(this.context.getColor(R.color.colorIcon));
                }
                like_icon.setImageResource(R.drawable.ic_action_like_black);
            }

            if (this.user.getUser_type()==1){
                btnApply.setVisibility(VISIBLE);
            }else{
                btnApply.setVisibility(GONE);
            }

            if (feed.getFeed().getAlbum()!=null){
                if (feed.getFeed().getAlbum().size() > 0){
                    if (feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc().contains("http")){
                        this.recentSearch.setSearch_image(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc());
                        Glide.with(this.context)
                                .load(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc())
                                .fitCenter()
                                .error(R.drawable.ic_error)
                                .into(job_image);
                    }else {
                        this.recentSearch.setSearch_image(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"));
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(feed.getFeed().getAlbum().get(0).getMedia().getImage().getSrc(),"500"))
                                .fitCenter()
                                .error(R.drawable.ic_error)
                                .into(job_image);
                    }
                }else {
                    Glide.with(this.context)
                            .load(R.drawable.ic_error)
                            .fitCenter()
                            .into(job_image);
                }
            }else {
                Glide.with(this.context)
                        .load(R.drawable.ic_error)
                        .fitCenter()
                        .into(job_image);
            }
        }
    }

    private Callback<RecentSearch> mCallback = new Callback<RecentSearch>() {
        @Override
        public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
            if (response.isSuccessful()){
                recentSearch = response.body();
            }else {
                call.enqueue(this);
            }
        }

        @Override
        public void onFailure(Call<RecentSearch> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void saveRecent(){
        int user = this.user!=null ? this.user.getId() : 0;
        searchService.save(this.recentSearch,user).enqueue(mCallback);
    }

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Count Like "+feed.getId(),mCountLike);
        socket.on("Count Comment "+feed.getId(),mCountComment);
        socket.connect();
    }

    private Emitter.Listener mCountLike = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setIs_like(obj.getInt("is_like") == 1);
                                feed.getFeed().setCount_likes(obj.getInt("count_likes"));
                                initFeed(feed);
                                adapter.notifyItemChanged(position);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setCount_comments(obj.getInt("count_comments"));
                                initFeed(feed);
                                adapter.notifyItemChanged(position);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private void likePost(){
        if (this.user==null){
            Intent loginIntent = new Intent(context, LoginActivity.class);
            context.startActivity(loginIntent);
        }
        service.LikeFeed(feed.getId(),this.user.getId()).enqueue(new Callback<SmTagFeed.Like>() {
            @Override
            public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                if (response.isSuccessful()){
                    feed.getFeed().setIs_like(response.body().isIs_like());
                    feed.getFeed().setCount_likes(response.body().getCount_likes());
                    initFeed(feed);
                    adapter.notifyItemChanged(position);
                }else {
                    call.enqueue(this);
                }
            }

            @Override
            public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void viewComment(){
        if (this.activity!=null){
            this.activity.startActivity(CommentActivity.createIntent(this.activity)
                    .putExtra(CommentActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("count_likes",feed.getFeed().getCount_likes())
                    .putExtra("is_like",feed.getFeed().isIs_like())
                    .putExtra("count_comments",feed.getFeed().getCount_comments())
                    .putExtra("feed_id",feed.getId())
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void originalShare(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,feed.getDetail());
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    private void showProfile(){
        if (this.user!=null){
            if (user.getId()==feed.getUser_post().getId()){
                if (this.activity!=null){
                    this.activity.startActivity(ProfileActivity
                            .createIntent(activity)
                            .putExtra(ProfileActivity.PROFILEA_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }else{
                if (this.activity!=null){
                    this.activity.startActivity(ViewProfileActivity
                            .createIntent(activity)
                            .putExtra(ViewProfileActivity.PROFILE_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
        }
    }

    private void ApplyJob() {
        if (this.feed==null){
            Toast.makeText(this.context,"No Feed",Toast.LENGTH_SHORT).show();
        }

        if (this.activity!=null){
            this.activity.startActivity(ApplyJobActivity.createIntent(this.activity).putExtra(ApplyJobActivity.FEED,this.feed));
        }
    }

    private void NEW_SHOW_MORE(){
        if (this.activity!=null){
            this.activity.startActivity(JobDetail.createIntent(this.activity).putExtra(JobDetail.FEED,this.feed).putExtra(JobDetail.USER,this.user.getId()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==profile_picture){
            this.showProfile();
        }else if (view==like_button){
            this.likePost();
        }else if (view==comment_button){
            this.viewComment();
        }else if (view==share_button){
            this.originalShare();
        }else if (view==btnApply){
            this.ApplyJob();
        }else if (view==this){
            this.NEW_SHOW_MORE();
            if (this.isRecent){
                this.saveRecent();
            }
        }
    }

    public void setRecent(boolean recent) {
        isRecent = recent;
    }
}

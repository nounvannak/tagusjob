package com.tagusnow.tagmejob.view.Search.UI.Face.Normal;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.grid.GridView;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchUIFaceNormal extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private CircleImageView profile;
    private TextView user_name,story,description,like_result,comment_result,like_text;
    private View like_and_comment_border,border_body;
    private LinearLayout like_and_comment_information;
    private LinearLayout like_button,comment_button,share_button;
    private ImageView like_icon;
    private FrameLayout image_frame;
    private Feed feed;
    private FeedNormalListener<SearchUIFaceNormal> feedNormalListener;
    private GridViewListener gridViewListener;

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void initUI(Context context){
        inflate(context, R.layout.search_ui_face_normal,this);
        this.context = context;
        profile = (CircleImageView)findViewById(R.id.profile);
        user_name = (TextView)findViewById(R.id.user_name);
        story = (TextView)findViewById(R.id.story);
        description = (TextView)findViewById(R.id.description);
        like_result = (TextView)findViewById(R.id.like_result);
        comment_result = (TextView)findViewById(R.id.comment_result);
        like_text = (TextView)findViewById(R.id.like_text);
        like_and_comment_border = (View)findViewById(R.id.like_and_comment_border);
        border_body = (View)findViewById(R.id.border_body);
        image_frame = (FrameLayout) findViewById(R.id.image_frame);
        like_and_comment_information = (LinearLayout)findViewById(R.id.like_and_comment_information);
        like_button = (LinearLayout)findViewById(R.id.like_button);
        comment_button = (LinearLayout)findViewById(R.id.comment_button);
        share_button = (LinearLayout)findViewById(R.id.share_button);
        like_icon = (ImageView)findViewById(R.id.like_icon);
        like_button.setOnClickListener(this);
        comment_button.setOnClickListener(this);
        share_button.setOnClickListener(this);
        profile.setOnClickListener(this);
        description.setOnClickListener(this);
        like_result.setOnClickListener(this);
        comment_result.setOnClickListener(this);
    }

    public SearchUIFaceNormal(Context context) {
        super(context);
        this.initUI(context);
    }

    public SearchUIFaceNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public SearchUIFaceNormal(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public SearchUIFaceNormal(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initFeed(feed);
    }

    private void initFeed(Feed feed) {
        setName(user_name,feed.getUser_post());
        setUserProfile(profile,feed.getUser_post());
        String storyText = "";
        if (feed.getFeed().getOriginalHastTagText() == null || feed.getFeed().getOriginalHastTagText().equals("")) {
            this.description.setVisibility(View.GONE);
        } else {
            this.description.setVisibility(View.VISIBLE);

            String desc = feed.getFeed().getOriginalHastTagText();
            if (desc.length() > 200){
                desc = desc.substring(0,197) + "...";
            }
            this.description.setText(desc);
        }

        if (feed.getFeed().isIs_like()) {
            like_icon.setImageResource(R.drawable.ic_action_like_blue);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                like_text.setTextColor(context.getColor(R.color.colorAccent));
            }

        } else {
            like_icon.setImageResource(R.drawable.ic_action_like_black);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                like_text.setTextColor(context.getColor(R.color.colorIcon));
            }
        }

        if (feed.getFeed().getCount_likes() > 0) {
            like_result.setVisibility(View.VISIBLE);
            like_result.setText(feed.getFeed().getLike_result());
        } else {
            like_result.setVisibility(View.GONE);
        }

        if (feed.getFeed().getCount_comments() > 0) {
            comment_result.setVisibility(View.VISIBLE);
            comment_result.setText(context.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
        } else {
            comment_result.setVisibility(View.GONE);
        }

        if (feed.getFeed().getCount_likes()==0 && feed.getFeed().getCount_comments()==0){
            like_and_comment_information.setVisibility(GONE);
        }else {
            like_and_comment_information.setVisibility(VISIBLE);
        }

        if (feed.getFeed().getAlbum()!=null){
            storyText = "posted " + feed.getFeed().getAlbum().size() + " photos, ";
            image_frame.setVisibility(VISIBLE);
            image_frame.removeAllViews();
            GridView gridView = new GridView(context);
            gridView.setActivity(activity);
            gridView.setGridViewListener(gridViewListener);
            gridView.setAlbums(feed.getFeed().getAlbum());
            image_frame.addView(gridView.getSubView(),gridView.getSubViewWidth(),gridView.getSubViewHeight());
        }else{
            image_frame.setVisibility(View.GONE);
        }

        story.setText(storyText + feed.getFeed().getTimeline());
    }

    @Override
    public void onClick(View view) {
        if (view==description){
            feedNormalListener.showDetail(this,feed);
        }else if (view==like_button){
            feedNormalListener.onLike(this,feed);
        }else if (view==comment_button){
            feedNormalListener.onComment(this,feed);
        }else if (view==share_button){
            feedNormalListener.onShare(this,feed);
        }else if (view==profile){
            feedNormalListener.viewProfile(this,feed);
        }else if (view == like_result){
            feedNormalListener.showAllUserLiked(this,feed);
        }else if (view == comment_result){
            feedNormalListener.onComment(this,feed);
        }
    }

    public void setFeedNormalListener(FeedNormalListener<SearchUIFaceNormal> feedNormalListener) {
        this.feedNormalListener = feedNormalListener;
    }

    public void setGridViewListener(GridViewListener gridViewListener) {
        this.gridViewListener = gridViewListener;
    }
}

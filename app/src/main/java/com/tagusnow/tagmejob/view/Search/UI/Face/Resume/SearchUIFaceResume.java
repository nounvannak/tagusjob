package com.tagusnow.tagmejob.view.Search.UI.Face.Resume;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewCardCVActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.RecentSearch;
import com.tagusnow.tagmejob.feed.SmTagFeed;
import com.tagusnow.tagmejob.view.CVAndResumeLayout;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONObject;

import java.io.Serializable;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUIFaceResume extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private SmUser user;
    private Feed feed;
    private RecyclerView.Adapter adapter;
    private RecentSearch recentSearch;
    private int position;
    private boolean isRecent = true;
    private CircleImageView profile;
    private TextView user_name,location,skill,like_result,comment_result,like_text;
    private RelativeLayout like_and_comment_information;
    private View like_and_comment_border,border_body;
    private LinearLayout like_button,comment_button,download_button;
    private ImageView like_icon;
    private Socket socket;
    private FeedService service = ServiceGenerator.createService(FeedService.class);
    private RecentSearchService searchService = ServiceGenerator.createService(RecentSearchService.class);


    private void initUI(Context context){
        inflate(context, R.layout.search_ui_face_resume,this);
        this.context = context;
        this.recentSearch = new RecentSearch();
        profile = (CircleImageView)findViewById(R.id.profile);
        user_name = (TextView)findViewById(R.id.user_name);
        location = (TextView)findViewById(R.id.location);
        skill = (TextView)findViewById(R.id.skill);
        like_result = (TextView)findViewById(R.id.like_result);
        comment_result = (TextView)findViewById(R.id.comment_result);
        like_text = (TextView)findViewById(R.id.like_text);
        like_and_comment_information = (RelativeLayout)findViewById(R.id.like_and_comment_information);
        like_and_comment_border = (View)findViewById(R.id.like_and_comment_border);
        border_body = (View)findViewById(R.id.border_body);
        like_button = (LinearLayout)findViewById(R.id.like_button);
        comment_button = (LinearLayout)findViewById(R.id.comment_button);
        download_button = (LinearLayout)findViewById(R.id.download_button);
        like_icon = (ImageView)findViewById(R.id.like_icon);

        like_button.setOnClickListener(this);
        comment_button.setOnClickListener(this);
        download_button.setOnClickListener(this);
        profile.setOnClickListener(this);
        this.setOnClickListener(this);

    }

    public SearchUIFaceResume(Context context) {
        super(context);
        this.initUI(context);
    }

    public SearchUIFaceResume(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public SearchUIFaceResume(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public SearchUIFaceResume(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setUser(SmUser user) {
        this.user = user;
        if (this.user!=null){
            this.recentSearch.setUser(this.user);
            this.recentSearch.setSearch_user_id(this.user.getId());
        }
    }

    private void initFeed(Feed feed){
        if (feed!=null){
            if (feed.getUser_post().getImage()!=null){
                this.recentSearch.setSearch_image(feed.getUser_post().getImage());
                if (feed.getUser_post().getImage().contains("http")){
                    Glide.with(this.context)
                            .load(feed.getUser_post().getImage())
                            .centerCrop()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(profile);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(feed.getUser_post().getImage(),"500"))
                            .centerCrop()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(profile);
                }
            }else {
                Glide.with(this.context)
                        .load(R.mipmap.ic_launcher_circle)
                        .centerCrop()
                        .into(profile);
            }

            this.recentSearch.setMaster_id(feed.getId());
            this.recentSearch.setSearch_text(feed.getFeed().getCategory());
            this.recentSearch.setSearch_type(RecentSearch.RESUME);

            user_name.setText(feed.getUser_post().getUser_name());
            location.setText(feed.getFeed().getCity_name());
            skill.setText(feed.getUser_post().getSkill_text());

            if (!skill.getText().toString().contains(feed.getFeed().getCategory())){
                String skillStr = skill.getText().toString();
                skill.setText(skillStr + ","+feed.getFeed().getCategory());
            }

            if (feed.getFeed().getCount_comments() > 0 || feed.getFeed().getCount_likes() > 0){
                like_and_comment_border.setVisibility(VISIBLE);
                like_and_comment_information.setVisibility(VISIBLE);
            }else {
                like_and_comment_border.setVisibility(GONE);
                like_and_comment_information.setVisibility(GONE);
            }

            if (feed.getFeed().getCount_likes() > 0){
                like_result.setVisibility(VISIBLE);
            }else {
                like_result.setVisibility(GONE);
            }

            if (feed.getFeed().getCount_comments() > 0){
                comment_result.setVisibility(VISIBLE);
            }else {
                comment_result.setVisibility(GONE);
            }

            like_result.setText(feed.getFeed().getLike_result());
            comment_result.setText(this.context.getString(R.string._d_comments,feed.getFeed().getCount_comments()));

            if (feed.getFeed().isIs_like()){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    like_text.setTextColor(this.context.getColor(R.color.colorAccent));
                }
                like_icon.setImageResource(R.drawable.ic_action_like_blue);
            }else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    like_text.setTextColor(this.context.getColor(R.color.colorIcon));
                }
                like_icon.setImageResource(R.drawable.ic_action_like_black);
            }

            if (this.user.getUser_type()==2){
                download_button.setVisibility(VISIBLE);
            }else{
                download_button.setVisibility(GONE);
            }

        }
    }

    private Callback<RecentSearch> mCallback = new Callback<RecentSearch>() {
        @Override
        public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
            if (response.isSuccessful()){
                recentSearch = response.body();
            }else {
                call.enqueue(this);
            }
        }

        @Override
        public void onFailure(Call<RecentSearch> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void saveRecent(){
        int user = this.user!=null ? this.user.getId() : 0;
        searchService.save(this.recentSearch,user).enqueue(mCallback);
    }

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Count Like "+feed.getId(),mCountLike);
        socket.on("Count Comment "+feed.getId(),mCountComment);
        socket.connect();
    }

    private Emitter.Listener mCountLike = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setIs_like(obj.getInt("is_like") == 1);
                                feed.getFeed().setCount_likes(obj.getInt("count_likes"));
                                initFeed(feed);
                                adapter.notifyItemChanged(position);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setCount_comments(obj.getInt("count_comments"));
                                initFeed(feed);
                                adapter.notifyItemChanged(position);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private void likePost(){
        if (this.user==null){
            Intent loginIntent = new Intent(context, LoginActivity.class);
            context.startActivity(loginIntent);
        }
        service.LikeFeed(feed.getId(),this.user.getId()).enqueue(new Callback<SmTagFeed.Like>() {
            @Override
            public void onResponse(Call<SmTagFeed.Like> call, Response<SmTagFeed.Like> response) {
                if (response.isSuccessful()){
                    feed.getFeed().setIs_like(response.body().isIs_like());
                    feed.getFeed().setCount_likes(response.body().getCount_likes());
                    initFeed(feed);
                    adapter.notifyItemChanged(position);
                }else {
                    call.enqueue(this);
                }
            }

            @Override
            public void onFailure(Call<SmTagFeed.Like> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void viewComment(){
        if (this.activity!=null){
            this.activity.startActivity(CommentActivity.createIntent(this.activity)
                    .putExtra(CommentActivity.FEED,new Gson().toJson(this.feed))
                    .putExtra("count_likes",feed.getFeed().getCount_likes())
                    .putExtra("is_like",feed.getFeed().isIs_like())
                    .putExtra("count_comments",feed.getFeed().getCount_comments())
                    .putExtra("feed_id",feed.getId())
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void showProfile(){
        if (this.user!=null){
            if (user.getId()==feed.getUser_post().getId()){
                if (this.activity!=null){
                    this.activity.startActivity(ProfileActivity
                            .createIntent(activity)
                            .putExtra(ProfileActivity.PROFILEA_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }else{
                if (this.activity!=null){
                    this.activity.startActivity(ViewProfileActivity
                            .createIntent(activity)
                            .putExtra(ViewProfileActivity.PROFILE_USER,new Gson().toJson(feed.getUser_post()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
        }
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        this.initSocket();
        this.initFeed(feed);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setRecent(boolean recent) {
        isRecent = recent;
    }

    private void PopupDownload(){
        if (this.activity!=null){
            CVAndResumeLayout layout = new CVAndResumeLayout(this.context);
            layout.setActivity(this.activity);
            layout.setFeed(this.feed);
            new AlertDialog.Builder(this.context).setView(layout).setOnDismissListener(onDismissListener).create().show();
        }
    }

    private DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialogInterface) {
            dialogInterface.dismiss();
        }
    };

    private void viewPostDetail(){
        if (this.activity!=null){
            this.activity.startActivity(ViewCardCVActivity.createIntent(this.activity)
                    .putExtra(ViewCardCVActivity.FEED, (Serializable) this.feed));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==like_button){
            this.likePost();
        }else if (view==comment_button){
            this.viewComment();
        }else if (view==download_button){
            this.PopupDownload();
        }else if (view==profile){
            this.showProfile();
        }else if (view==this){
            this.viewPostDetail();
            if (this.isRecent){
                this.saveRecent();
            }
        }
    }
}

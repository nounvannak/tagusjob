package com.tagusnow.tagmejob.view.Search.UI.Holder.Job;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.Search.UI.Face.Job.SearchUIFaceJob;

public class SearchUIFaceJobHolder extends RecyclerView.ViewHolder {

    private SearchUIFaceJob searchUIFaceJob;

    public SearchUIFaceJobHolder(View itemView) {
        super(itemView);
        this.searchUIFaceJob = (SearchUIFaceJob)itemView;
    }

    public void bindView(Activity activity, RecyclerView.Adapter adapter, Feed feed, SmUser user,boolean isRecent){
        this.searchUIFaceJob.setActivity(activity);
        this.searchUIFaceJob.setAdapter(adapter);
        this.searchUIFaceJob.setPosition(getAdapterPosition());
        this.searchUIFaceJob.setUser(user);
        this.searchUIFaceJob.setFeed(feed);
        this.searchUIFaceJob.setRecent(isRecent);
    }
}

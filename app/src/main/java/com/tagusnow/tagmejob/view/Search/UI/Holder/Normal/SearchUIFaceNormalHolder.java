package com.tagusnow.tagmejob.view.Search.UI.Holder.Normal;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.v3.grid.GridViewListener;
import com.tagusnow.tagmejob.view.Search.UI.Face.Normal.SearchUIFaceNormal;

public class SearchUIFaceNormalHolder extends RecyclerView.ViewHolder {

    private SearchUIFaceNormal searchUIFaceNormal;

    public SearchUIFaceNormalHolder(View itemView) {
        super(itemView);
        this.searchUIFaceNormal = (SearchUIFaceNormal)itemView;
    }

    public void bindView(Activity activity, Feed feed, FeedNormalListener feedNormalListener, GridViewListener gridViewListener){
        this.searchUIFaceNormal.setActivity(activity);
        this.searchUIFaceNormal.setFeedNormalListener(feedNormalListener);
        this.searchUIFaceNormal.setGridViewListener(gridViewListener);
        this.searchUIFaceNormal.setFeed(feed);
    }
}

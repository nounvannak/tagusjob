package com.tagusnow.tagmejob.view.Search.UI.Holder.Normal;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.feed.FeedNormalListener;
import com.tagusnow.tagmejob.view.Search.UI.Face.Normal.SearchUIFeedEmbedURL;

public class SearchUIFeedEmbedURLHolder extends RecyclerView.ViewHolder {

    private SearchUIFeedEmbedURL searchUIFeedEmbedURL;

    public SearchUIFeedEmbedURLHolder(View itemView) {
        super(itemView);
        searchUIFeedEmbedURL = (SearchUIFeedEmbedURL)itemView;
    }

    public void BindView(Activity activity, Feed feed, FeedNormalListener feedNormalListener){
        searchUIFeedEmbedURL.setActivity(activity);
        searchUIFeedEmbedURL.setFeed(feed);
        searchUIFeedEmbedURL.setFeedNormalListener(feedNormalListener);
    }
}

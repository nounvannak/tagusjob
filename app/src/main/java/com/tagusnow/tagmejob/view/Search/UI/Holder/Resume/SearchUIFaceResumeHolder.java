package com.tagusnow.tagmejob.view.Search.UI.Holder.Resume;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.Search.UI.Face.Resume.SearchUIFaceResume;

public class SearchUIFaceResumeHolder extends RecyclerView.ViewHolder {

    private SearchUIFaceResume searchUIFaceResume;

    public SearchUIFaceResumeHolder(View itemView) {
        super(itemView);
        this.searchUIFaceResume = (SearchUIFaceResume)itemView;
    }

    public void bindView(Activity activity, SmUser user, Feed feed,RecyclerView.Adapter adapter,boolean isRecent){
        this.searchUIFaceResume.setActivity(activity);
        this.searchUIFaceResume.setUser(user);
        this.searchUIFaceResume.setAdapter(adapter);
        this.searchUIFaceResume.setPosition(getAdapterPosition());
        this.searchUIFaceResume.setFeed(feed);
        this.searchUIFaceResume.setRecent(isRecent);
    }
}

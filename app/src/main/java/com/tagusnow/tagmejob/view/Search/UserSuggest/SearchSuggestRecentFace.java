package com.tagusnow.tagmejob.view.Search.UserSuggest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SearchResultActivity;
import com.tagusnow.tagmejob.SearchViewNoticeActivity;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewNoticeMainSearchResultActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.feed.RecentSearch;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchSuggestRecentFace extends RelativeLayout implements View.OnClickListener {

    private CircleImageView circleImageView;
    private TextView textView;
    private Activity activity;
    private RecentSearch recentSearch;
    private static final int WORD = 0;
    private static final int USER = 1;
    private static final int JOBS = 2;
    private static final int OHTER = 3;
    public static final int MAIN_SEARCH = 1;
    private int type;

    public SearchSuggestRecentFace(Context context) {
        super(context);
        inflate(context,R.layout.search_suggest_result_face,this);
        circleImageView = (CircleImageView)findViewById(R.id.search_picture);
        textView = (TextView)findViewById(R.id.search_text);
        this.setOnClickListener(this);
    }

    public SearchSuggestRecentFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public SearchSuggestRecentFace Type(int type){
        this.type = type;
        return this;
    }

    public SearchSuggestRecentFace RecentSearch(RecentSearch recentSearch){
        this.recentSearch = recentSearch;
        if (recentSearch!=null){
            textView.setText(recentSearch.getSearch_text());
            if (recentSearch.getSearch_image()!=null){
                if (recentSearch.getSearch_type()==USER){
                    if (recentSearch.getSearch_image().contains("http")) {
                        Glide.with(this.activity.getApplicationContext())
                                .load(recentSearch.getSearch_image())
                                .error(R.drawable.ic_user_male_icon)
                                .centerCrop()
                                .into(circleImageView);
                    }else{
                        Glide.with(this.activity.getApplicationContext())
                                .load(SuperUtil.getProfilePicture(recentSearch.getSearch_image()))
                                .error(R.drawable.ic_user_male_icon)
                                .centerCrop()
                                .into(circleImageView);
                    }
                }else if (recentSearch.getSearch_type()==WORD){
                    Glide.with(this.activity.getApplicationContext())
                            .load(R.drawable.ic_search_black_24dp)
                            .centerCrop()
                            .into(circleImageView);
                }else if (recentSearch.getSearch_type()==JOBS){
                    if (recentSearch.getSearch_image().contains("http")){
                        Glide.with(this.activity.getApplicationContext())
                                .load(recentSearch.getSearch_image())
                                .error(R.drawable.ic_bullhorn_icon)
                                .centerCrop()
                                .into(circleImageView);
                    }else{
                        Glide.with(this.activity.getApplicationContext())
                                .load(SuperUtil.getAlbumPicture(recentSearch.getSearch_image()))
                                .error(R.drawable.ic_bullhorn_icon)
                                .centerCrop()
                                .into(circleImageView);
                    }
                }else {
                    Glide.with(this.activity.getApplicationContext())
                            .load(R.drawable.ic_action_bookmark_border)
                            .centerCrop()
                            .into(circleImageView);
                }
            }else{
                if (recentSearch.getSearch_type()==USER){
                    Glide.with(this.activity.getApplicationContext())
                            .load(R.drawable.ic_user_male_icon)
                            .centerCrop()
                            .into(circleImageView);
                }else if (recentSearch.getSearch_type()==WORD){
                    Glide.with(this.activity.getApplicationContext())
                            .load(R.drawable.ic_search_black_24dp)
                            .centerCrop()
                            .into(circleImageView);
                }else if (recentSearch.getSearch_type()==JOBS){
                    Glide.with(this.activity.getApplicationContext())
                            .load(R.drawable.ic_bullhorn_icon)
                            .centerCrop()
                            .into(circleImageView);
                }else {
                    Glide.with(this.activity.getApplicationContext())
                            .load(R.drawable.ic_action_bookmark_border)
                            .centerCrop()
                            .into(circleImageView);
                }
            }
        }else {
            Toast.makeText(this.activity.getApplicationContext(),"No Recent Search",Toast.LENGTH_SHORT).show();
        }
        return this;
    }

    private void viewProfile(Activity activity){
        if (activity!=null && recentSearch!=null){
            if (recentSearch.getSearch_type()==USER){
                if (recentSearch.getUser()!=null){
                    this.activity.startActivity(ViewProfileActivity
                            .createIntent(activity)
                            .putExtra("profileUser",new Gson().toJson(recentSearch.getUser()))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    Toast.makeText(this.activity.getApplicationContext(),"No User",Toast.LENGTH_SHORT).show();
                }
            }else if(recentSearch.getSearch_type()==WORD){
                if (this.type==MAIN_SEARCH){
                    this.activity.startActivity(ViewNoticeMainSearchResultActivity
                            .createIntent(this.activity)
                            .putExtra(ViewNoticeMainSearchResultActivity.TITLE,this.recentSearch.getSearch_text())
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    this.activity.startActivity(SearchViewNoticeActivity
                            .createIntent(this.activity,this.recentSearch.getSearch_text())
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            }else if(recentSearch.getSearch_type()==JOBS){
                this.activity.startActivity(SearchResultActivity
                        .createIntent(this.activity)
                        .putExtra(SearchResultActivity.FEED,new Gson().toJson(this.recentSearch.getFeed()))
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }else if(recentSearch.getSearch_type()==OHTER){

            }
        }else{
            if (activity==null){
                Toast.makeText(this.activity.getApplicationContext(),"No Activity",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this.activity.getApplicationContext(),"No Recent Search",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            viewProfile(this.activity);
        }
    }
}

package com.tagusnow.tagmejob.view.Search.UserSuggest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.RecentSearchActivity;
import com.tagusnow.tagmejob.v3.search.SearchSuggestRecentHeaderListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class SearchSuggestRecentHeader extends TagUsJobRelativeLayout implements View.OnClickListener {
    private Context context;
    private Button btnEditRecent;
    private SearchSuggestRecentHeaderListener searchSuggestRecentHeaderListener;

    public SearchSuggestRecentHeader(Context context) {
        super(context);
        InitUI(context);
    }

    public SearchSuggestRecentHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public SearchSuggestRecentHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public SearchSuggestRecentHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    private void InitUI(Context context){
        this.context = context;
        inflate(context, R.layout.search_suggest_recent_header,this);
        btnEditRecent = (Button)findViewById(R.id.btnEditRecent);
        btnEditRecent.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view==btnEditRecent){
            searchSuggestRecentHeaderListener.edit(this);
        }
    }

    public void setSearchSuggestRecentHeaderListener(SearchSuggestRecentHeaderListener searchSuggestRecentHeaderListener) {
        this.searchSuggestRecentHeaderListener = searchSuggestRecentHeaderListener;
    }
}

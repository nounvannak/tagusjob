package com.tagusnow.tagmejob.view.Search.UserSuggest;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchSuggestResultFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = SearchSuggestResultFace.class.getSimpleName();
    private CircleImageView circleImageView;
    private TextView textView;
    private SmUser user,authUser;
    private Activity activity;
    private Context context;
    private static RecentSearch recentSearch;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private RecentSearchService recentSearchService = retrofit.create(RecentSearchService.class);

    public SearchSuggestResultFace(Context context) {
        super(context);
        this.context = context;
        inflate(context,R.layout.search_suggest_result_face,this);
        recentSearch = new RecentSearch();
        authUser = new Auth(context).checkAuth().token();
        circleImageView = (CircleImageView)findViewById(R.id.search_picture);
        textView = (TextView)findViewById(R.id.search_text);
        this.setOnClickListener(this);
    }

    public SearchSuggestResultFace Activity(@NonNull Activity activity){
        this.activity = activity;
        return this;
    }

    public SearchSuggestResultFace User(@NonNull SmUser user){
        this.user = user;
        if (user.getImage()==null){
            Glide.with(this.context)
                    .load(R.drawable.ic_search_black_24dp)
                    .centerCrop()
                    .error(R.drawable.ic_search_black_24dp)
                    .into(circleImageView);
        }else{
            if (user.getImage().contains("http")){
                Glide.with(this.context)
                        .load(user.getImage())
                        .centerCrop()
                        .error(R.drawable.ic_user_male_icon)
                        .into(circleImageView);
            }else{
                Glide.with(this.context)
                        .load(SuperUtil.getProfilePicture(user.getImage()))
                        .centerCrop()
                        .error(R.drawable.ic_user_male_icon)
                        .into(circleImageView);
            }
        }

        String search_text = "";
        if (user.getUser_name()!=null){
            search_text += user.getUser_name();
        }

        if (user.getEmail()!=null){
            search_text += ", "+user.getEmail();
        }

        if (user.getLocation()!=null){
            search_text += ", "+user.getLocation();
        }

        if (user.getAddress()!=null){
            search_text += ", "+user.getAddress();
        }
        textView.setText(search_text);

        return this;
    }

    private void viewProfile(Activity activity){
        if (activity!=null && user!=null){
            save(user);
            this.activity.startActivity(ViewProfileActivity.createIntent(activity).putExtra("profileUser",new Gson().toJson(user)));
        }else{
            if (activity==null){
                Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void save(SmUser user){
        if (user!=null && authUser!=null){
            recentSearch.setSearch_type(RecentSearch.USER);
            recentSearch.setSearch_text(user.getUser_name());
            if (user.getImage()!=null){
                recentSearch.setSearch_image(user.getImage());
            }
            recentSearch.setSearch_user_id(authUser.getId());
            recentSearch.setMaster_id(user.getId());
            recentSearch.setUser(user);
            recentSearchService.save(recentSearch,user.getId()).enqueue(new Callback<RecentSearch>() {
                @Override
                public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
                    if (response.isSuccessful()){
                        Log.w(TAG,response.message());
                        recentSearch = response.body();
                    }else{
                        try {
                            Log.w(TAG,response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<RecentSearch> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            viewProfile(this.activity);
        }
    }
}

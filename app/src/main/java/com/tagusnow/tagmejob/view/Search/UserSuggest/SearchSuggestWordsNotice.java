package com.tagusnow.tagmejob.view.Search.UserSuggest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RecentSearchService;
import com.tagusnow.tagmejob.SearchViewNoticeActivity;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.ViewNoticeMainSearchResultActivity;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.RecentSearch;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchSuggestWordsNotice extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = SearchSuggestWordsNotice.class.getSimpleName();
    private Activity activity;
    private Context context;
    private SmUser user,authUser;
    private TextView search_result;
    private String strSearch_result;
    private static RecentSearch recentSearch;
    private boolean isMainSearch = false;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private RecentSearchService recentSearchService = retrofit.create(RecentSearchService.class);

    public SearchSuggestWordsNotice(Context context) {
        super(context);
        this.context = context;
        recentSearch = new RecentSearch();
        this.authUser = new Auth(context).checkAuth().token();
        inflate(context, R.layout.search_suggest_words_notice,this);
        search_result = (TextView)findViewById(R.id.search_result);
        this.setOnClickListener(this);
    }

    public Activity getActivity() {
        return activity;
    }

    public SearchSuggestWordsNotice setActivity(Activity activity) {
        this.activity = activity;
        return this;
    }

    public SmUser getUser() {
        return user;
    }

    public SearchSuggestWordsNotice setUser(SmUser user) {
        this.user = user;
        return this;
    }

    public String getStrSearch_result() {
        return strSearch_result;
    }

    public void setStrSearch_result(String strSearch_result) {
        this.strSearch_result = strSearch_result;
        if (strSearch_result!=null){
            search_result.setText(this.activity.getString(R.string.see_all_results_for_s,strSearch_result));
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            save(this.authUser);
            viewSearchResult();
        }
    }

    private void save(SmUser user){
        int userId = user!=null ? user.getId() : 0;
        recentSearch.setUser(user);
        recentSearch.setMaster_id(0);
        recentSearch.setSearch_user_id(userId);
        recentSearch.setSearch_type(RecentSearch.WORD);
        recentSearch.setSearch_text(this.strSearch_result);
        recentSearch.setSearch_image(null);
        recentSearchService.save(recentSearch,userId).enqueue(new Callback<RecentSearch>() {
            @Override
            public void onResponse(Call<RecentSearch> call, Response<RecentSearch> response) {
                if (response.isSuccessful()){
                    Log.w(TAG,response.message());
                    recentSearch = response.body();
                }else {
                    try {
                        Log.w(TAG,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RecentSearch> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void showProfile(){
        this.context.startActivity(SearchViewNoticeActivity.createIntent(this.activity,this.strSearch_result).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public SearchSuggestWordsNotice isMainSearch(boolean isMainSearch){
        this.isMainSearch = isMainSearch;
        return this;
    }

    private void viewNoticeMainSearchResult(){
        this.activity.startActivity(ViewNoticeMainSearchResultActivity.createIntent(this.activity).putExtra(ViewNoticeMainSearchResultActivity.TITLE,this.strSearch_result).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private void viewSearchResult() {
        if (this.activity!=null){
            if (this.isMainSearch){
                viewNoticeMainSearchResult();
            }else {
                showProfile();
            }
        }else{
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }
}

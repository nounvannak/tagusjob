package com.tagusnow.tagmejob.view.Search.UserSuggest;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import java.util.concurrent.TimeUnit;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchViewNoticeFace extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = SearchViewNoticeFace.class.getSimpleName();
    private SmUser user,authUser;
    private Context context;
    private Activity activity;
    private CircleImageView user_picture;
    private TextView user_name,user_email,user_location;
    private RelativeLayout layout_location,btnFollowing;
    private ImageView image_following;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService followService = retrofit.create(FollowService.class);

    public SearchViewNoticeFace(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.search_view_notice_face,this);
        user_picture = (CircleImageView)findViewById(R.id.user_picture);
        user_name = (TextView)findViewById(R.id.user_name);
        user_email = (TextView)findViewById(R.id.user_email);
        user_location = (TextView)findViewById(R.id.user_location);
        layout_location = (RelativeLayout)findViewById(R.id.layout_location);
        btnFollowing = (RelativeLayout)findViewById(R.id.btnFollowing);
        image_following = (ImageView)findViewById(R.id.image_following);
        btnFollowing.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public SearchViewNoticeFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public SearchViewNoticeFace Auth(SmUser authUser){
        this.authUser = authUser;
        return this;
    }

    public SearchViewNoticeFace User(SmUser user){
        this.user = user;
        if (user!=null){
            if (user.getImage()!=null){
                if (user.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(user.getImage())
                            .centerCrop()
                            .error(R.drawable.ic_person_outline_black_24dp)
                            .into(user_picture);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(user.getImage()))
                            .centerCrop()
                            .error(R.drawable.ic_person_outline_black_24dp)
                            .into(user_picture);
                }
            }else {
                Glide.with(this.context)
                        .load(R.drawable.ic_person_outline_black_24dp)
                        .centerCrop()
                        .into(user_picture);
            }
            user_name.setText(user.getUser_name());
            if (user.getEmail()!=null){
                user_email.setVisibility(VISIBLE);
                user_email.setText(user.getEmail());
            }else {
                user_email.setVisibility(GONE);
            }

            if (user.getLocation()!=null){
                layout_location.setVisibility(VISIBLE);
                user_location.setText(user.getLocation());
            }else {
                layout_location.setVisibility(GONE);
            }

            if (user.isIs_follow()){
                image_following.setBackgroundResource(R.drawable.ic_person_blue_24dp);
            }
        }
        return this;
    }

    private void onFollowing(){
        if (this.user!=null && this.authUser!=null){
            followService.hasFollow(this.user.getId(),this.authUser.getId()).enqueue(new Callback<SmUser>() {
                @Override
                public void onResponse(Call<SmUser> call, Response<SmUser> response) {
                    if (response.isSuccessful()){
                        Log.w(TAG,response.message());
                        User(response.body());
                    }else{
                        Log.w(TAG,response.errorBody().toString());
                    }
                }

                @Override
                public void onFailure(Call<SmUser> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }else{
            if (this.user==null){
                Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void viewProfile(){
        if (this.activity!=null){
            if (this.user!=null && this.authUser!=null){
                if (this.authUser.getId()==this.user.getId()){
                    this.context.startActivity(ProfileActivity.createIntent(this.activity));
                }else {
                    this.context.startActivity(ViewProfileActivity.createIntent(this.activity).putExtra("profileUser",new Gson().toJson(this.user)));
                }
            }else{
                if (this.user==null){
                    Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this.context,"No Auth",Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnFollowing){
            onFollowing();
        }else if(view==this){
            viewProfile();
        }
    }
}

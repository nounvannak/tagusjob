package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.repository.Repository;
import java.util.ArrayList;
import java.util.List;

public class SearchJobsFace extends RelativeLayout implements AdapterView.OnItemSelectedListener {


    private static final String TAG = SearchJobsFace.class.getSimpleName();
    private Activity activity;
    private Context context;
    private Button btnSearch;
    private EditText txtSearch;
    private Spinner spnLocation;
    private SmUser user;
    private SmLocation location;
    private int location_id = 0;
    private List<String> locationList = new ArrayList<>();

    public int getLocation(){
        return this.location_id;
    }

    public String getSearchText(){
        return this.txtSearch.getText().toString();
    }

    public Button getBtnSearch() {
        return btnSearch;
    }

    public SearchJobsFace(Context context) {
        super(context);
        inflate(context, R.layout.search_jobs_face,this);
        this.context = context;
        btnSearch = (Button)findViewById(R.id.btnSearch);
        txtSearch = (EditText)findViewById(R.id.txtSearch);
        initTemp();
        spnLocation = (Spinner)findViewById(R.id.spnLocation);
        spnLocation.setOnItemSelectedListener(this);
        initLocation();
    }

    private void initTemp(){
        this.location = new Repository(this.context).restore().getLocation();
    }

    private void initLocation(){
        if (this.location!=null){
            this.locationList.add(0,"All Location");
            for (int i=1;i<=this.location.getData().size();i++){
                this.locationList.add(i,this.location.getData().get(i-1).getName());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.context, R.layout.tagusjob_spinner,android.R.id.text1,this.locationList);
        this.spnLocation.setAdapter(adapter);
    }

    public SearchJobsFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public SearchJobsFace Auth(SmUser user){
        this.user = user;
        return this;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i > 0){
            this.location_id = this.location.getData().get(i-1).getId();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

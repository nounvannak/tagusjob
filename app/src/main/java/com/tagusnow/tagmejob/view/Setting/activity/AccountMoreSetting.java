package com.tagusnow.tagmejob.view.Setting.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.AuthService;
import com.tagusnow.tagmejob.REST.Service.RepositoryService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.adapter.CityAdapter;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.model.v2.feed.Language;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.Spinner.MultiSpinner;
import com.tagusnow.tagmejob.view.Spinner.MultiSpinnerListener;
import com.tagusnow.tagmejob.view.TagUsJobActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountMoreSetting extends TagUsJobActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageButton btnEditName,btnEditSlug,btnEditEmail,btnEditPhone,btnEditDob,btnEditGender,btnEditLocation,btnEditLanguage,btnEditBiography;
    private Button btnSaveName,btnSaveSlug,btnSavePhone,btnSaveDob,btnSaveGender,btnSaveLocation,btnSaveLanguage,btnSaveBiography;
    private Button btnCancelName,btnCancelSlug,btnCancelPhone,btnCancelDob,btnCancelGender,btnCancelLocation,btnCancelLanguage,btnCancelBiography;
    private TextView textName,textSlug,textEmail,textPhone,textDob,textGender,textLocation,textLanguage,textBiography;
    private EditText txtFirstName,txtLastName,txtSlug,txtPhone,txtDob,txtBiography;
    private Spinner spnGender,spnLocation;
    private MultiSpinner spnLanguage;
    private RelativeLayout layout_show_name,layout_show_slug,layout_show_phone,layout_show_dob,layout_show_gender,layout_show_location,layout_show_language,layout_show_biography;
    private RelativeLayout layout_edit_name,layout_edit_slug,layout_edit_phone,layout_edit_dob,layout_edit_gender,layout_edit_location,layout_edit_language,layout_edit_biography;
    private TextView current_location,current_language,label_note_edit_slug;
    private RelativeLayout layout_avalaible_slug,layout_not_avalaible_slug,layout_avalaible_phone,layout_not_avalaible_phone;
    private SmUser user,SaveUser;
    private List<Language> languages;
    private SmLocation location;
    private boolean[] language_selected;
    private String city;
    private String langs;
    private String error_message;
    private String gender;
    private String DOB;
    private int ViewAction;
    private Calendar DOBCalendar = Calendar.getInstance();
    private RepositoryService service = ServiceGenerator.createService(RepositoryService.class);
    private AuthService userService = ServiceGenerator.createService(AuthService.class);
    private boolean isAvailableSlug = false;
    private boolean isAvailablePhone = false;

    private void OnCheckSlug(String slug){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            this.isAvailableSlug = userService.CheckSlug(this.user.getId(),slug).execute().isSuccessful();
            if (this.isAvailableSlug){
                layout_avalaible_slug.setVisibility(View.VISIBLE);
                layout_not_avalaible_slug.setVisibility(View.GONE);
            }else {
                layout_avalaible_slug.setVisibility(View.GONE);
                layout_not_avalaible_slug.setVisibility(View.VISIBLE);
                isAvailableSlug = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void OnCheckPhone(String phone){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            this.isAvailablePhone = userService.CheckPhone(this.user.getId(),phone).execute().isSuccessful();
            if (this.isAvailablePhone){
                layout_avalaible_phone.setVisibility(View.VISIBLE);
                layout_not_avalaible_phone.setVisibility(View.GONE);
            }else {
                layout_avalaible_phone.setVisibility(View.GONE);
                layout_not_avalaible_phone.setVisibility(View.VISIBLE);
                isAvailablePhone = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void Save(){
        SaveUser.setAuth_user(this.user.getId());
        SaveUser.setImage(null);
        SaveUser.setCover(null);
        userService.updateAccount(SaveUser).enqueue(new Callback<SmUser>() {
            @Override
            public void onResponse(Call<SmUser> call, Response<SmUser> response) {
                if (response.isSuccessful()){
                    user = response.body();
                    new Auth(AccountMoreSetting.this).save(user);
                    Setup();
                    dismissProgress();
                    ShowAlert("Account","Your account were updated.");
                }else {
                    dismissProgress();
                    try {
                        ShowAlert("Error!",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SmUser> call, Throwable t) {
                ShowAlert("Connection Error!",t.getMessage());
            }
        });
    }

    private boolean isValid(){
        boolean is_ok = false;

        if (this.ViewAction==btnEditName.getId()){
            if (txtLastName.getText().toString().length() > 0 && txtFirstName.getText().toString().length() > 0){
                is_ok = true;
                this.SaveUser.setFirs_name(txtFirstName.getText().toString());
                this.SaveUser.setLast_name(txtLastName.getText().toString());
                this.SaveUser.setUser_name(txtFirstName.getText().toString() + " " + txtLastName.getText().toString());
            }else {
                this.error_message = "First & Last Name not empty.";
            }
        }else if (this.ViewAction==btnEditSlug.getId()){
//            OnCheckSlug();
            if (txtSlug.getText().toString().length() > 0 && this.isAvailableSlug){
                is_ok = true;
                this.SaveUser.setSlug(txtSlug.getText().toString());
            }else {
                this.error_message = "Username not empty.";
            }
        }else if (this.ViewAction==btnEditPhone.getId()){
//            OnCheckPhone();
            if (txtPhone.getText().toString().length() >= 9 && this.isAvailablePhone){
                is_ok = true;
                this.SaveUser.setPhone(txtPhone.getText().toString());
            }else {
                this.error_message = "Phone not validated.";
            }
        }else if (this.ViewAction==btnEditDob.getId()){
            if (txtDob.getText().toString().length() > 0){
                is_ok = true;
                this.SaveUser.setDob(this.DOB);
            }else {
                this.error_message = "Date of Birth not empty.";
            }
        }else if (this.ViewAction==btnEditGender.getId()){
            if (this.gender!=null){
                is_ok = true;
                this.SaveUser.setGender(this.gender);
            }else {
                this.error_message = "Gender not select.";
            }
        }else if (this.ViewAction==btnEditLocation.getId()){
            if (this.city!=null){
                is_ok = true;
                this.SaveUser.setLocation(this.city);
            }else {
                this.error_message = "Location not select.";
            }
        }else if (this.ViewAction==btnEditLanguage.getId()){
            Log.e("langs",String.valueOf(this.langs));
            if (this.langs!=null){
               is_ok = true;
               this.SaveUser.setLanguage(this.langs);
            }else {
                this.error_message = "Language not select.";
            }
        }else if (this.ViewAction==btnEditBiography.getId()){
            if (this.txtBiography.getText().toString().length() > 0){
                is_ok = true;
                this.SaveUser.setBiography(txtBiography.getText().toString());
            }else {
                this.error_message = "Biography not empty.";
            }
        }
        return is_ok;
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            DOBCalendar.set(Calendar.YEAR,i);
            DOBCalendar.set(Calendar.MONTH,i1);
            DOBCalendar.set(Calendar.DAY_OF_MONTH,i2);
            setupDate();
        }
    };

    private void setupDate(){
        if (this.DOBCalendar!=null){
            String date = this.DOBCalendar.get(Calendar.DAY_OF_MONTH)+"/"+((this.DOBCalendar.get(Calendar.MONTH) + 1) > 9 ? (this.DOBCalendar.get(Calendar.MONTH) + 1) : "0"+(this.DOBCalendar.get(Calendar.MONTH) + 1))+"/"+this.DOBCalendar.get(Calendar.YEAR);
            this.DOB = this.DOBCalendar.get(Calendar.YEAR)+"-"+((this.DOBCalendar.get(Calendar.MONTH) + 1) > 9 ? (this.DOBCalendar.get(Calendar.MONTH) + 1) : "0"+(this.DOBCalendar.get(Calendar.MONTH) + 1))+"-"+this.DOBCalendar.get(Calendar.DAY_OF_MONTH);
            txtDob.setText(date);
        }
    }

    private void InitDatePicker(){
        if (this.DOBCalendar==null){
            this.DOBCalendar = Calendar.getInstance();
        }
        new DatePickerDialog(this, dateSetListener, this.DOBCalendar.get(Calendar.YEAR), this.DOBCalendar.get(Calendar.MONTH), this.DOBCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void InitTemp(){
        this.user = new Auth(this).checkAuth().token();
        this.SaveUser = new Auth(this).checkAuth().token();
        this.location = new Repository(this).restore().getLocation();
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            this.languages = service.GetLanguage().execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void Setup(){
        if (this.user!=null){
            textName.setText(this.user.getUser_name());
            String[] ArrName = this.user.getUser_name().split(" ");
            if (ArrName.length > 0){
                if (ArrName.length == 2){
                    txtFirstName.setText(ArrName[0]);
                    txtLastName.setText(ArrName[1]);
                }else if (ArrName.length==1){
                    txtFirstName.setText(ArrName[0]);
                }else {
                    txtFirstName.setText(ArrName[0]);
                    txtLastName.setText(ArrName[ArrName.length - 1]);
                }
            }
            textSlug.setText(this.user.getSlug());
            txtSlug.setText(this.user.getSlug());
            if (this.user.getSlug()!=null){
                this.isAvailableSlug = true;
            }
            label_note_edit_slug.setText(getString(R.string.note_edit_slug,this.user.getSlug()));
            textEmail.setText(this.user.getEmail());

            if (this.user.getPhone()!=null){
                textPhone.setText(this.user.getPhone());
                txtPhone.setText(this.user.getPhone());
            }else {
                textPhone.clearComposingText();
                txtPhone.clearComposingText();
            }

            if (this.user.getDob()!=null){
                textDob.setText(this.user.getDateOfBirth());
                txtDob.setText(this.user.getDateOfBirth());
                String[] ArrDOB = this.user.getDateOfBirth().split("/");
                DOBCalendar = Calendar.getInstance();
                DOBCalendar.set(Calendar.DAY_OF_MONTH,Integer.parseInt(ArrDOB[0]));
                DOBCalendar.set(Calendar.MONTH,Integer.parseInt(ArrDOB[1]) - 1);
                DOBCalendar.set(Calendar.YEAR,Integer.parseInt(ArrDOB[2]));
            }else {
                textDob.clearComposingText();
                txtDob.clearComposingText();
            }

            if (this.user.getGenderId() > 0){
                textGender.setText(this.user.getGender());
                spnGender.setPrompt(this.user.getGender());
                gender = this.user.getGender().toLowerCase();
            }else {
                gender = null;
                textGender.clearComposingText();
            }

            if (this.user.getLocation()!=null){
                textLocation.setText(this.user.getLocation());
                spnLocation.setPrompt(this.user.getLocation());
                current_location.setText(getString(R.string.current_location_phnom_penh,this.user.getLocation()));
            }else {
                textLocation.clearComposingText();
                current_location.setText(getString(R.string.current_location_phnom_penh,"current location"));
            }

            Log.e("lang",new Gson().toJson(this.user.getLanguage()));
            if (this.user.getLanguage()!=null){
                try {
                    JSONArray array = new JSONArray(Arrays.asList(this.user.getLanguage().toString().split(",")));
                    Log.e("array",array.getJSONArray(0).getString(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                current_language.setText(getString(R.string.current_languages,"Ex:English"));
            }

            if (this.user.getBiography()!=null){
                textBiography.setText(this.user.getBiography());
                txtBiography.setText(this.user.getBiography());
            }else {
                textBiography.clearComposingText();
                txtBiography.clearComposingText();
            }
        }

        if (this.languages!=null){
            List<CharSequence> items = new ArrayList<CharSequence>();
            for (Language lang : this.languages){
                items.add(lang.getName());
            }
            spnLanguage.setItems(items, "Select All", new MultiSpinnerListener() {
                @Override
                public void onItemsSelected(boolean[] selected) {
                    language_selected = selected;
                    if (spnLanguage.getSelected_text().length() > 0){
                        langs = spnLanguage.getSelected_text().substring(0,spnLanguage.getSelected_text().length() - 1);
                        Log.e("Language",langs);
                    }
                }
            });
        }

        if (this.location!=null){
            List<String> list = new ArrayList<>();
            for (SmLocation.Data loc : this.location.getData()){
                list.add(loc.getName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,list);
            spnLocation.setAdapter(adapter);
        }
    }

    private void ResetView(){
        layout_avalaible_slug.setVisibility(View.GONE);
        layout_not_avalaible_slug.setVisibility(View.GONE);
        layout_avalaible_phone.setVisibility(View.GONE);
        layout_not_avalaible_phone.setVisibility(View.GONE);

        layout_edit_name.setVisibility(View.GONE);
        layout_edit_slug.setVisibility(View.GONE);
        layout_edit_phone.setVisibility(View.GONE);
        layout_edit_dob.setVisibility(View.GONE);
        layout_edit_gender.setVisibility(View.GONE);
        layout_edit_location.setVisibility(View.GONE);
        layout_edit_language.setVisibility(View.GONE);
        layout_edit_biography.setVisibility(View.GONE);

        btnEditEmail.setVisibility(View.GONE);
    }

    private void CondView(View view){
        ResetView();
        Setup();
        this.ViewAction = view.getId();
        if (view==btnEditName){
            layout_edit_name.setVisibility(View.VISIBLE);
        }else if (view==btnEditSlug){
            layout_edit_slug.setVisibility(View.VISIBLE);
        }else if (view==btnEditPhone){
            layout_edit_phone.setVisibility(View.VISIBLE);
        }else if (view==btnEditDob){
            layout_edit_dob.setVisibility(View.VISIBLE);
        }else if (view==btnEditGender){
            layout_edit_gender.setVisibility(View.VISIBLE);
        }else if (view==btnEditLocation){
            layout_edit_location.setVisibility(View.VISIBLE);
        }else if (view==btnEditLanguage){
            layout_edit_language.setVisibility(View.VISIBLE);
        }else if (view==btnEditBiography){
            layout_edit_biography.setVisibility(View.VISIBLE);
        }

        if (this.isAvailableSlug){
            layout_avalaible_slug.setVisibility(View.VISIBLE);
            layout_not_avalaible_slug.setVisibility(View.GONE);
        }else {
            layout_avalaible_slug.setVisibility(View.GONE);
            layout_not_avalaible_slug.setVisibility(View.VISIBLE);
            isAvailableSlug = false;
        }

        if (this.isAvailablePhone){
            layout_avalaible_phone.setVisibility(View.VISIBLE);
            layout_not_avalaible_phone.setVisibility(View.GONE);
        }else {
            layout_avalaible_phone.setVisibility(View.GONE);
            layout_not_avalaible_phone.setVisibility(View.VISIBLE);
            isAvailablePhone = false;
        }
    }

    private void InitUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        layout_avalaible_slug = (RelativeLayout)findViewById(R.id.layout_avalaible_slug);
        layout_not_avalaible_slug = (RelativeLayout)findViewById(R.id.layout_not_avalaible_slug);
        layout_avalaible_phone = (RelativeLayout)findViewById(R.id.layout_avalaible_phone);
        layout_not_avalaible_phone = (RelativeLayout)findViewById(R.id.layout_not_avalaible_phone);

        layout_avalaible_slug.setVisibility(View.GONE);
        layout_not_avalaible_slug.setVisibility(View.GONE);
        layout_avalaible_phone.setVisibility(View.GONE);
        layout_not_avalaible_phone.setVisibility(View.GONE);

        layout_show_name = (RelativeLayout)findViewById(R.id.layout_show_name);
        layout_show_slug = (RelativeLayout)findViewById(R.id.layout_show_slug);
        layout_show_phone = (RelativeLayout)findViewById(R.id.layout_show_phone);
        layout_show_dob = (RelativeLayout)findViewById(R.id.layout_show_dob);
        layout_show_gender = (RelativeLayout)findViewById(R.id.layout_show_gender);
        layout_show_location = (RelativeLayout)findViewById(R.id.layout_show_location);
        layout_show_language = (RelativeLayout)findViewById(R.id.layout_show_language);
        layout_show_biography = (RelativeLayout)findViewById(R.id.layout_show_biography);

        layout_edit_name = (RelativeLayout)findViewById(R.id.layout_edit_name);
        layout_edit_slug = (RelativeLayout)findViewById(R.id.layout_edit_slug);
        layout_edit_phone = (RelativeLayout)findViewById(R.id.layout_edit_phone);
        layout_edit_dob = (RelativeLayout)findViewById(R.id.layout_edit_dob);
        layout_edit_gender = (RelativeLayout)findViewById(R.id.layout_edit_gender);
        layout_edit_location = (RelativeLayout)findViewById(R.id.layout_edit_location);
        layout_edit_language = (RelativeLayout)findViewById(R.id.layout_edit_language);
        layout_edit_biography = (RelativeLayout)findViewById(R.id.layout_edit_biography);

        layout_edit_name.setVisibility(View.GONE);
        layout_edit_slug.setVisibility(View.GONE);
        layout_edit_phone.setVisibility(View.GONE);
        layout_edit_dob.setVisibility(View.GONE);
        layout_edit_gender.setVisibility(View.GONE);
        layout_edit_location.setVisibility(View.GONE);
        layout_edit_language.setVisibility(View.GONE);
        layout_edit_biography.setVisibility(View.GONE);

        btnEditName = (ImageButton)findViewById(R.id.btnEditName);
        btnEditSlug = (ImageButton)findViewById(R.id.btnEditSlug);
        btnEditEmail = (ImageButton)findViewById(R.id.btnEditEmail);
        btnEditPhone = (ImageButton)findViewById(R.id.btnEditPhone);
        btnEditDob = (ImageButton)findViewById(R.id.btnEditDob);
        btnEditGender = (ImageButton)findViewById(R.id.btnEditGender);
        btnEditLocation = (ImageButton)findViewById(R.id.btnEditLocation);
        btnEditLanguage = (ImageButton)findViewById(R.id.btnEditLanguage);
        btnEditBiography = (ImageButton)findViewById(R.id.btnEditBiography);

        btnEditEmail.setVisibility(View.GONE);

        btnEditName.setOnClickListener(this);
        btnEditSlug.setOnClickListener(this);
        btnEditEmail.setOnClickListener(this);
        btnEditPhone.setOnClickListener(this);
        btnEditDob.setOnClickListener(this);
        btnEditGender.setOnClickListener(this);
        btnEditLocation.setOnClickListener(this);
        btnEditLanguage.setOnClickListener(this);
        btnEditBiography.setOnClickListener(this);

        btnSaveName = (Button)findViewById(R.id.btnSaveName);
        btnSaveSlug = (Button)findViewById(R.id.btnSaveSlug);
        btnSavePhone = (Button)findViewById(R.id.btnSavePhone);
        btnSaveDob = (Button)findViewById(R.id.btnSaveDOB);
        btnSaveGender = (Button)findViewById(R.id.btnSaveGender);
        btnSaveLocation = (Button)findViewById(R.id.btnSaveLocation);
        btnSaveLanguage = (Button)findViewById(R.id.btnSaveLanuguage);
        btnSaveBiography = (Button)findViewById(R.id.btnSaveBiography);

        btnSaveName.setOnClickListener(this);
        btnSaveSlug.setOnClickListener(this);
        btnSavePhone.setOnClickListener(this);
        btnSaveDob.setOnClickListener(this);
        btnSaveGender.setOnClickListener(this);
        btnSaveLocation.setOnClickListener(this);
        btnSaveLanguage.setOnClickListener(this);
        btnSaveBiography.setOnClickListener(this);

        btnCancelName = (Button)findViewById(R.id.btnCancelName);
        btnCancelSlug = (Button)findViewById(R.id.btnCancelSlug);
        btnCancelPhone = (Button)findViewById(R.id.btnCancelPhone);
        btnCancelDob = (Button)findViewById(R.id.btnCancelDOB);
        btnCancelGender = (Button)findViewById(R.id.btnCancelGender);
        btnCancelLocation = (Button)findViewById(R.id.btnCancelLocation);
        btnCancelLanguage = (Button)findViewById(R.id.btnCancelLanguage);
        btnCancelBiography = (Button)findViewById(R.id.btnCancelBiography);

        btnCancelName.setOnClickListener(this);
        btnCancelSlug.setOnClickListener(this);
        btnCancelPhone.setOnClickListener(this);
        btnCancelDob.setOnClickListener(this);
        btnCancelGender.setOnClickListener(this);
        btnCancelLocation.setOnClickListener(this);
        btnCancelLanguage.setOnClickListener(this);
        btnCancelBiography.setOnClickListener(this);

        textName = (TextView)findViewById(R.id.text_name);
        textSlug = (TextView)findViewById(R.id.text_slug);
        textEmail = (TextView)findViewById(R.id.text_email);
        textPhone = (TextView)findViewById(R.id.text_phone);
        textDob = (TextView)findViewById(R.id.text_dob);
        textGender = (TextView)findViewById(R.id.text_gender);
        textLocation = (TextView)findViewById(R.id.text_location);
        textLanguage  = (TextView)findViewById(R.id.text_language);
        textBiography = (TextView)findViewById(R.id.text_biography);

        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtLastName = (EditText)findViewById(R.id.txtLastName);
        txtSlug = (EditText)findViewById(R.id.txtSlug);
        txtPhone = (EditText)findViewById(R.id.txtPhone);

        txtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                OnCheckPhone(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        txtSlug.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                OnCheckSlug(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        txtDob = (EditText)findViewById(R.id.txtDob);
        txtBiography = (EditText)findViewById(R.id.txtBiography);

        current_language = (TextView)findViewById(R.id.current_language);
        current_location = (TextView)findViewById(R.id.current_location);
        label_note_edit_slug = (TextView)findViewById(R.id.label_note_edit_slug);

        txtDob.setOnClickListener(this);

        spnGender = (Spinner)findViewById(R.id.spnGender);
        spnLocation  = (Spinner)findViewById(R.id.spnLocation);
        spnLanguage = (MultiSpinner)findViewById(R.id.spnLanguage);

        spnGender.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,new ArrayList<String>(){{add("Male");add("Female");}}));
        spnLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                city = location.getData().get(i).getName();
                Log.e("city",city);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spnGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==0){
                    gender = "male";
                }else if (i==1){
                    gender = "female";
                }

                Log.e("gender",gender);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_more_setting);
        InitTemp();
        InitUI();
        Setup();
    }

    public static Intent createIntent(Activity activity){
        return new Intent(activity,AccountMoreSetting.class);
    }

    @Override
    public void onClick(View v) {
        if (v==txtDob) {
            InitDatePicker();
        }else if (v==btnSaveName){
            OnSave();
        }else if (v==btnSaveSlug){
            OnSave();
        }else if (v==btnSavePhone){
            OnSave();
        }else if (v==btnSaveDob){
            OnSave();
        }else if (v==btnSaveGender){
            OnSave();
        }else if (v==btnSaveLocation){
            OnSave();
        }else if (v==btnSaveLanguage){
            OnSave();
        }else if (v==btnSaveBiography){
            OnSave();
        }else {
            CondView(v);
        }
    }

    private void OnSave(){
        if (this.isValid()){
            showProgress();
            Save();
        }else {
            ShowAlert("Account Setting",this.error_message);
        }
    }

}

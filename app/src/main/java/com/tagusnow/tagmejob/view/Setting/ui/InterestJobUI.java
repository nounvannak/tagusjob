package com.tagusnow.tagmejob.view.Setting.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SeeSingleTopJobsActivity;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserInterest;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import com.tagusnow.tagmejob.repository.Repository;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class InterestJobUI extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ImageView image;
    private TextView name,counter;
    private ImageButton btnRemove;
    private UserInterest interest;
    private SmUser user;
    private SmTagSubCategory category;

    private void InitUI(Context context){
        inflate(context, R.layout.interest_job_ui,this);
        this.context = context;
        this.category = new Repository(this.context).getCategory();
        image = (ImageView)findViewById(R.id.image);
        name = (TextView)findViewById(R.id.name);
        counter = (TextView)findViewById(R.id.counter);
        btnRemove = (ImageButton)findViewById(R.id.btnRemove);
        this.setOnClickListener(this);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public InterestJobUI(Context context) {
        super(context);
        InitUI(context);
    }

    public InterestJobUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public InterestJobUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public InterestJobUI(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    public void setInterest(UserInterest interest) {
        this.interest = interest;
        InitInterest(interest);
    }

    private void InitInterest(UserInterest interest){
        if (interest!=null){
            if (this.user!=null){
                if (this.user.getUser_type()==SmUser.FIND_JOB){
                    counter.setText("available " + interest.getCounter() +" jobs.");
                }else {
                    counter.setText("available " + interest.getCounter() +" looking for jobs.");
                }

                name.setText(interest.getName());
                if (interest.getImage()!=null){
                    if (interest.getImage().contains("http")){
                        Glide.with(this.context)
                                .load(interest.getImage())
                                .fitCenter()
                                .error(R.mipmap.ic_launcher_circle)
                                .into(image);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getCategoryPath(interest.getImage()))
                                .fitCenter()
                                .error(R.mipmap.ic_launcher_circle)
                                .into(image);
                    }
                }else {
                    Glide.with(this.context)
                            .load(R.mipmap.ic_launcher_circle)
                            .fitCenter()
                            .into(image);
                }
            }
        }
    }

    public void setUser(SmUser user) {
        this.user = user;
    }

    private void ShowCategory(){
        for (Category cat : this.category.getData()){
            if (cat.getId()==this.interest.getCat_id()){
                OpenFilterCategory(cat);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            this.ShowCategory();
        }
    }

    public UserInterest getInterest() {
        return interest;
    }
}

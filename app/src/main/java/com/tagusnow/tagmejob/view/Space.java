package com.tagusnow.tagmejob.view;

import android.content.Context;
import android.widget.RelativeLayout;
import com.tagusnow.tagmejob.R;

public class Space extends RelativeLayout{

    public Space(Context context) {
        super(context);
        inflate(context, R.layout.space,this);
    }
}

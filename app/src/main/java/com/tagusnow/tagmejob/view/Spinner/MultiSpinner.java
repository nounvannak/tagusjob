package com.tagusnow.tagmejob.view.Spinner;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.List;

public class MultiSpinner extends android.support.v7.widget.AppCompatSpinner implements DialogInterface.OnMultiChoiceClickListener, DialogInterface.OnCancelListener {

    private List<CharSequence> items = new ArrayList<CharSequence>();
    private boolean[] selected;
    private static String defaultText;
    private MultiSpinnerListener listener;
    private String selected_text;
    private boolean allselected = false;
    int listsize;
    int totalItems;

    public MultiSpinner(Context context) {
        super(context);
    }

    public MultiSpinner(Context context, int mode) {
        super(context, mode);
    }

    public MultiSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MultiSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public MultiSpinner(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public MultiSpinner(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, int mode, Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode);
    }

    @Override
    public boolean performClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select Languages");
        builder.setMultiChoiceItems(items.toArray(new CharSequence[items.size()]), selected, this);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.setOnCancelListener(this);
        builder.create().show();
        return true;
    }

    public void setItems(List<CharSequence> items, String allText,MultiSpinnerListener listener) {
        this.items.clear();
        items.add(0,"Select All");
        this.items.addAll(items);
        listsize=0;
        totalItems = items.size();
        selected = new boolean[items.size()];
        defaultText = allText;
        this.listener = listener;

        deSelectAll();
        setDefaultText(allText);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // refresh text on spinner
        StringBuffer spinnerBuffer = new StringBuffer();
        boolean someUnselected = false;
        for (int i = 0; i < items.size(); i++) {
            if (selected[i]) {
                spinnerBuffer.append(items.get(i));
                spinnerBuffer.append(",");
            } else {
                someUnselected = true;
            }
        }

        String spinnerText;
        if (someUnselected) {
            spinnerText = spinnerBuffer.toString();
            if (spinnerText.length() > 2)
            spinnerText = spinnerText.substring(0, spinnerText.length() - 2);
        } else {
            spinnerText = defaultText;
        }
        ArrayAdapter adapter = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,new String[]{spinnerText});
        setAdapter(adapter);
        this.selected_text = spinnerBuffer.toString();
        listener.onItemsSelected(selected);
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (which==0){
            if (isChecked){
                for (int i=0;i<selected.length;i++){
                    selected[i] = true;
                    listsize = totalItems - 1;
                    ((AlertDialog) dialog).getListView().setItemChecked(i,true);
                }
            }else {
                for (int i=0;i<selected.length;i++){
                    selected[i] = false;
                    listsize = 0;
                    ((AlertDialog) dialog).getListView().setItemChecked(i,false);
                }
            }
        }else {
            if (isChecked){
                selected[which] = true;
                ++listsize;
            }else {
                selected[which] = false;
                --listsize;
            }
        }

        if (listsize==totalItems-1){
            selected[0] = true;
            ((AlertDialog) dialog).getListView().setItemChecked(0,true);
        }else {
            selected[0] = false;
            ((AlertDialog) dialog).getListView().setItemChecked(0,false);
        }
    }

    public void setDefaultText(String text) {
        defaultText = text;
        ArrayAdapter adapter = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1, new String[]{text});
        setAdapter(adapter);
    }

    public void deSelectAll() {
        for (int i = 0; i < selected.length; i++)
            selected[i] = false;
    }

    public String getSelected_text() {
        return selected_text;
    }
}

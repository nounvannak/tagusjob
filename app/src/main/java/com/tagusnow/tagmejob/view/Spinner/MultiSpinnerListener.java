package com.tagusnow.tagmejob.view.Spinner;

public interface MultiSpinnerListener {
    public void onItemsSelected(boolean[] selected);
}

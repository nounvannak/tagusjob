package com.tagusnow.tagmejob.view.Subcribe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FeedService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SeeSingleTopJobsActivity;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.v3.category.SubcribeListener;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubcribeLayout extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private Category category;
    private ImageView image;
    private TextView title,job_counter;
    private Button btnSubcribe;
    private SubcribeListener<SubcribeLayout> subcribeListener;

    private void InitUI(Context context){
        inflate(context, R.layout.layout_job_subcribe,this);
        this.context = context;

        title = (TextView)findViewById(R.id.title);
        job_counter = (TextView)findViewById(R.id.job_counter);
        image = (ImageView)findViewById(R.id.image);
        btnSubcribe = (Button)findViewById(R.id.btnSubcribe);

        btnSubcribe.setOnClickListener(this);
        this.setOnClickListener(this);
    }

    public SubcribeLayout(Context context) {
        super(context);
        InitUI(context);
    }

    public SubcribeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitUI(context);
    }

    public SubcribeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitUI(context);
    }

    public SubcribeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public void setCategory(Category category) {
        this.category = category;
        InitCategory(category);
    }

    private void InitCategory(Category category){
        if (category!=null){
            title.setText(category.getTitle());
            job_counter.setText(String.valueOf(category.getJob_counter()));
            setCategoryPhoto(image,category.getImage());
            if (category.isSubscribe()){
                btnSubcribe.setText(this.context.getString(R.string.subcribed));
                btnSubcribe.setBackgroundResource(R.color.fbutton_color_green_sea);
            }else {
                btnSubcribe.setBackgroundResource(R.color.colorAccent);
                btnSubcribe.setText(this.context.getString(R.string.subcribe));
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnSubcribe){
            subcribeListener.subcribe(this,category);
        }else if (view==this){
            subcribeListener.position(this,category);
        }
    }

    public void setSubcribeListener(SubcribeListener<SubcribeLayout> subcribeListener) {
        this.subcribeListener = subcribeListener;
    }
}

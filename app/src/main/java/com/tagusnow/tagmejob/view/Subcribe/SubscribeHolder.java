package com.tagusnow.tagmejob.view.Subcribe;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.v3.category.SubcribeListener;

public class SubscribeHolder extends RecyclerView.ViewHolder {

    private SubcribeLayout layout;

    public SubscribeHolder(View itemView) {
        super(itemView);
        layout = (SubcribeLayout)itemView;
    }

    public void BindView(Activity activity, Category category, SubcribeListener<SubcribeLayout> subcribeListener){
        layout.setActivity(activity);
        layout.setCategory(category);
        layout.setSubcribeListener(subcribeListener);
    }
}

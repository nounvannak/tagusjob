package com.tagusnow.tagmejob.view;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.provider.OpenableColumns;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.CategoryDrawer;
import com.tagusnow.tagmejob.ProgressDialogFragment;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.v3.image.ImageViewActivity;
import com.tagusnow.tagmejob.v3.login.auth.AuthReponse;
import com.tagusnow.tagmejob.v3.login.other.ConfirmSMSCodeActivity;
import com.tagusnow.tagmejob.v3.login.other.UpdatePasswordActivity;
import com.tagusnow.tagmejob.v3.login.phone.LoginWithPhoneActivity;
import com.tagusnow.tagmejob.v3.post.PostJobActivity;
import com.tagusnow.tagmejob.v3.post.PostNormalActivity;
import com.tagusnow.tagmejob.v3.post.PostStaffActivity;
import com.tagusnow.tagmejob.view.FirstLogin.Activity.WelcomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@SuppressLint("Registered")
public class TagUsJobActivity extends AppCompatActivity{

    protected static final int OPEN_MEDIA_PICKER = 1;
    protected final String OPEN_MEDIA_TITLE = "title";
    protected final String OPEN_MEDIA_MODE = "mode";
    protected final String OPEN_MEDIA_MAX = "maxSelection";
    protected final static int IMAGE_ONLY = 2;
    protected final static int REQUEST_EDIT = 8203;
    protected JSONObject settings;
    protected boolean isRequest = true;
    protected int lastPage = 0;

    private final static String PROGRESS_DIALOG = "ProgressDialog";

    public static final String DATE_FORMAT_yyyy_MM_dd = "yyyy-MM-dd";
    public static final String DATE_FORMAT_dd_s_MM_s_yyyy = "dd/MM/yyyy";
    public static final String DATE_FORMAT_yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_MM_dd_yyyy = "MM dd, yyyy";
    public static final String DATE_FORMAT_E_MMM_dd_yyyy = "E, MMM dd yyyy";
    public static final String DATE_FORMAT_E_MMM_dd_yyyy_HH_mm_ss = "E, MMM dd yyyy HH:mm:ss";
    public static final String DATE_FORMAT_MMM_yyyy = "MMM yyyy";

    public boolean isPornSite(String url){
        boolean isPorn = false;
        InputStream ins = getResources().openRawResource(getResources().getIdentifier("website_block", "raw", getPackageName()));
        String websiteBlock = SuperUtil.readTextFile(ins);

        String urlWith_WWW = url.toLowerCase().replaceFirst("www.","");
        String urlWith_http = url.toLowerCase().replaceFirst("http://","");
        String urlWith_https = url.toLowerCase().replaceFirst("https://","");
        websiteBlock = websiteBlock.toLowerCase();

        if (websiteBlock.contains(url.toLowerCase())){
            isPorn = true;
        }

        if (websiteBlock.contains(urlWith_http)){
            isPorn = true;
        }

        if (websiteBlock.contains(urlWith_WWW)){
            isPorn = true;
        }

        if (websiteBlock.contains(urlWith_https)){
            isPorn = true;
        }

        return isPorn;
    }

    public void ChangeStatusBar(@ColorInt int color){
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(color);
        }
    }

    public String GetDate(String date,String oldDateFormat,String newDateFormat){
        String newDate = "";
        SimpleDateFormat f = new SimpleDateFormat(oldDateFormat, Locale.ENGLISH);
        SimpleDateFormat newFormat = new SimpleDateFormat(newDateFormat,Locale.ENGLISH);
        try {
            Date d = f.parse(date);
            newDate = newFormat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        return newDate;
    }

    public String GetDate(String date,String format){
        String newDate = "";
        SimpleDateFormat f = new SimpleDateFormat(DATE_FORMAT_yyyy_MM_dd, Locale.ENGLISH);
        SimpleDateFormat newFormat = new SimpleDateFormat(format,Locale.ENGLISH);
        try {
            Date d = f.parse(date);
            newDate = newFormat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        return newDate;
    }

    public String GetDate(Date date,String format){
        String newDate = "";
        SimpleDateFormat newFormat = new SimpleDateFormat(format,Locale.ENGLISH);
        newDate = newFormat.format(date);
        return newDate;
    }

    public void setName(TextView textView, SmUser user){
        if (user.getFirs_name() != null && !user.getFirs_name().equals("") && user.getLast_name() != null && !user.getLast_name().equals("")){
            textView.setText((user.getFirs_name() + " " + user.getLast_name()));
        }else {
            textView.setText(user.getUser_name());
        }
    }

    public void setUserCover(ImageView cover, SmUser user){
        if (user.getCover() != null && !user.getCover().equals("")){
            if (user.getCover().contains("http")){
                Glide.with(this)
                        .load(user.getCover())
                        .error((user.getUser_type() == SmUser.FIND_STAFF) ? R.drawable.coverfindstaffdefual : R.drawable.coverdefault1)
                        .centerCrop().into(cover);
            }else {
                Glide.with(this)
                        .load(SuperUtil.getCover(user.getCover()))
                        .error((user.getUser_type() == SmUser.FIND_STAFF) ? R.drawable.coverfindstaffdefual : R.drawable.coverdefault1)
                        .centerCrop().into(cover);
            }
        }else {
            Glide.with(this)
                    .load((user.getUser_type() == SmUser.FIND_STAFF) ? R.drawable.coverfindstaffdefual : R.drawable.coverdefault1)
                    .centerCrop().into(cover);
        }
    }

    public void setUserProfile(CircleImageView imageView, SmUser user){

        if (user.getImage() != null){
            if (user.getImage().contains("http")){
                Glide.with(this).load(user.getImage()).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }else{
                Glide.with(this).load(SuperUtil.getProfilePicture(user.getImage(),"400")).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }
        }else{
            Glide.with(this).load(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).centerCrop().into(imageView);
        }

    }

    public void setUserProfile(ImageView imageView, SmUser user){

        if (user.getImage() != null){
            if (user.getImage().contains("http")){
                Glide.with(this).load(user.getImage()).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }else{
                Glide.with(this).load(SuperUtil.getProfilePicture(user.getImage(),"400")).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }
        }else{
            Glide.with(this).load(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).centerCrop().into(imageView);
        }

    }

    public void setAlbumPhoto(ImageView imageView, Album album, String size){

        if (album != null){
            if (album.getMedia().getImage().getSrc().contains("http")){
                Glide.with(this).load(album.getMedia().getImage().getSrc()).fitCenter().error(R.drawable.not_available_).into(imageView);
            }else {
                Glide.with(this).load(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),size)).fitCenter().error(R.drawable.not_available_).into(imageView);
            }
        }else {
            Glide.with(this).load(R.drawable.not_available_).fitCenter().into(imageView);
        }

    }

    public void setAlbumPhoto(ImageView imageView, Album album, boolean isFitCenter){
        if (album != null){
            if (album.getMedia().getImage().getSrc().contains("http")){
                if (isFitCenter){
                    Glide.with(this).load(album.getMedia().getImage().getSrc()).fitCenter().error(R.drawable.not_available_).into(imageView);
                }else {
                    Glide.with(this).load(album.getMedia().getImage().getSrc()).centerCrop().error(R.drawable.not_available_).into(imageView);
                }
            }else {
                if (isFitCenter){
                    Glide.with(this).load(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"700")).fitCenter().error(R.drawable.not_available_).into(imageView);
                }else {
                    Glide.with(this).load(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"700")).centerCrop().error(R.drawable.not_available_).into(imageView);
                }
            }
        }else {
            Glide.with(this).load(R.drawable.not_available_).fitCenter().into(imageView);
        }
    }

    public void setAlbumPhoto(ImageView imageView, Album album){
        if (album != null){
            if (album.getMedia().getImage().getSrc().contains("http")){
                Glide.with(this).load(album.getMedia().getImage().getSrc()).fitCenter().error(R.drawable.not_available_).into(imageView);
            }else {
                Glide.with(this).load(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"700")).fitCenter().error(R.drawable.not_available_).into(imageView);
            }
        }else {
            Glide.with(this).load(R.drawable.not_available_).fitCenter().into(imageView);
        }
    }

    public void setAlbumPhoto(ImageView imageView, String path, String size){
        Glide.with(this).load(SuperUtil.getAlbumPicture(path,size)).fitCenter().error(R.drawable.not_available_).into(imageView);
    }

    public void setCategoryPhoto(ImageView imageView, String path){
        Glide.with(this).load(SuperUtil.getCategoryPath(path)).fitCenter().error(R.drawable.not_available_).into(imageView);
    }

    public void setAlbumPhoto(ImageView imageView, String path){
        if (path.contains("http")){
            Glide.with(this).load(path).fitCenter().error(R.drawable.not_available_).into(imageView);
        }else {
            Glide.with(this).load(SuperUtil.getAlbumPicture(path,"700")).fitCenter().error(R.drawable.not_available_).into(imageView);
        }
    }

    public void EditPostNormal(Feed feed){

        if (feed.getFeed().getType().equals(FeedDetail.NORMAL_POST)){
            startActivity(PostNormalActivity
                    .createIntent(this)
                    .putExtra(PostNormalActivity.IS_EDIT,true)
                    .putExtra(PostNormalActivity.FEED,new Gson().toJson(feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (feed.getFeed().getType().equals(FeedDetail.JOB_POST)){
            startActivity(PostJobActivity
                    .createIntent(this)
                    .putExtra(PostJobActivity.IS_EDIT,true)
                    .putExtra(PostJobActivity.FEED,new Gson().toJson(feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            startActivity(PostStaffActivity
                    .createIntent(this)
                    .putExtra(PostStaffActivity.IS_EDIT,true)
                    .putExtra(PostStaffActivity.FEED,new Gson().toJson(feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    public void AccessApp(SmUser user){
        if (user != null){
            if (user.getActive() == 1){
                // Go to confirm code
                startActivity(ConfirmSMSCodeActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }else if (user.getUser_type() == 0){
                // Go to welcome screen & select type
                startActivity(WelcomeActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }else if (user.getUser_type() > 0){
                // Go to Home page
                startActivity(CategoryDrawer.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        }else {
            // Go to login
            startActivity(LoginWithPhoneActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    public void GoToConfirmSMSCode(){
        // Go to confirm code
        startActivity(ConfirmSMSCodeActivity.createIntent(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void GoToUpdatePassword(String token_type,String access_token){
        // Go to update password
        startActivity(UpdatePasswordActivity.createIntent(this).putExtra(SuperUtil.TOKEN_TYPE,token_type).putExtra(SuperUtil.ACCESS_TOKEN,access_token).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void GoToConfirmSMSCode(boolean isForget){
        // Go to confirm code
        startActivity(ConfirmSMSCodeActivity.createIntent(this).putExtra(ConfirmSMSCodeActivity.IS_FORGET,isForget).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void DisplayImage(ArrayList<String> images, int index){
        startActivity(ImageViewActivity.createIntent(this)
                .putStringArrayListExtra(ImageViewActivity.IMAGES,images)
                .putExtra(ImageViewActivity.INDEX,index)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void showProgress() {
        ProgressDialogFragment f = ProgressDialogFragment.getInstance();
        getSupportFragmentManager().beginTransaction().add(f,PROGRESS_DIALOG).commitAllowingStateLoss();
    }

    public void PlayMessageSound(){
        MediaPlayer player = MediaPlayer.create(this,R.raw.message_sound);
        if (!player.isPlaying()){
            player.start();
        }
    }

    public boolean IsMessageSound(){
        boolean isMessage = false;
        try {
            settings = SuperUtil.GetPreference(this,SuperConstance.SETTING_PREF);
            isMessage =  this.settings.getBoolean("isMessageSound");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isMessage;
    }

    public boolean IsNotificationAlert(){
        boolean isMessage = false;
        try {
            settings = SuperUtil.GetPreference(this,SuperConstance.SETTING_PREF);
            isMessage =  this.settings.getBoolean("isNotificationAlert");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isMessage;
    }

    public void dismissProgress() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager == null) return;
        ProgressDialogFragment f = (ProgressDialogFragment) manager.findFragmentByTag(PROGRESS_DIALOG);
        if (f != null) {
            getSupportFragmentManager().beginTransaction().remove(f).commitAllowingStateLoss();
        }
    }

    public enum NameType {FULL,LAST,FIRST}

    public String getName(SmUser user, NameType type){
        if (type == NameType.FULL){
            if (user.getUser_name() != null && !user.getUser_name().equals("")){
                return user.getUser_name();
            }else {
                if (user.getFirs_name() != null && !user.getFirs_name().equals("") && user.getLast_name() != null && !user.getLast_name().equals("")){
                    return user.getFirs_name() + " " + user.getLast_name();
                }else {
                    if (user.getFirs_name() != null && !user.getFirs_name().equals("")){
                        return user.getFirs_name();
                    }else if (user.getLast_name() != null && !user.getLast_name().equals("")){
                        return  user.getLast_name();
                    }else {
                        return "";
                    }
                }
            }
        }else if (type == NameType.FIRST){
            if (user.getFirs_name() != null && !user.getFirs_name().equals("")){
                return user.getFirs_name();
            }else {
                if (user.getUser_name() != null && !user.getUser_name().equals("")){
                    String[] nameArr = user.getUser_name().split(" ");

                    if (nameArr.length > 0){
                        return nameArr[0];
                    }else {
                        return "";
                    }
                }
                return "";
            }
        }else if (type == NameType.LAST){
            if (user.getLast_name() != null && !user.getLast_name().equals("")){
                return  user.getLast_name();
            }else {
                if (user.getUser_name() != null && !user.getUser_name().equals("")){
                    String[] nameArr = user.getUser_name().split(" ");

                    if (nameArr.length > 1){
                        return nameArr[1];
                    }else {
                        return "";
                    }
                }
                return "";
            }
        }else {
            return "";
        }
    }

    protected void getDocument(int REQUEST_CODE_DOC)
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("application/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        // Only the system receives the ACTION_OPEN_DOCUMENT, so no need to test.
        startActivityForResult(intent, REQUEST_CODE_DOC);
    }

    protected String getFileNameByUri(Context context, Uri uri)
    {
        String uriString = uri.toString();
        File myFile = new File(uriString).getAbsoluteFile();
        String path = myFile.getPath();
        String displayName = null;

        Log.w("Path",path);

        if (uriString.startsWith("content://")) {
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    path = path.replace(myFile.getName(),displayName);
                }
            } finally {
                cursor.close();
            }
        } else if (uriString.startsWith("file://")) {
            path = myFile.getAbsolutePath();
        }

        return path;
    }

    public void ShowAlert(String title,String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(TagUsJobActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public String GotErrorMsg(String errorString){
        String error = "Somethings when wrong.";
        AuthReponse authReponse = new Gson().fromJson(errorString,AuthReponse.class);
        String message = authReponse != null ? authReponse.getError() : error;
        return message;
    }

    public String getMimeTypeFromMediaContentUri(Uri uri) {
        String mimeType;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }
    @NonNull
    protected RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    protected MultipartBody.Part prepareFilePart(String partName, File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse(getMimeTypeFromMediaContentUri(Uri.fromFile(file))),file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

}

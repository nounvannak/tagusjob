package com.tagusnow.tagmejob.view;

import android.annotation.SuppressLint;
import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tagusnow.tagmejob.model.v2.feed.PhotoPaginate;

@SuppressLint("Registered")
public class TagUsJobImagePostActivity extends TagUsJobActivity{
    protected String[] strPhoto, descriptions;
    protected void showPicker(int startPosition) {

    }

    protected void init() {
//        posters = photoPaginate.getPosters();
//        descriptions = Demo.getDescriptions();

        for (int i = 0; i < strPhoto.length; i++) {
            SimpleDraweeView drawee = new SimpleDraweeView(this);
            initDrawee(drawee, i);
        }
    }

    private void initDrawee(SimpleDraweeView drawee, final int startPosition) {
        drawee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPicker(startPosition);
            }
        });
        drawee.setImageURI(strPhoto[startPosition]);
    }
}

package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.ApplyJobActivity;
import com.tagusnow.tagmejob.CommentActivity;
import com.tagusnow.tagmejob.JobDetail;
import com.tagusnow.tagmejob.LastedPostActivity;
import com.tagusnow.tagmejob.LoginActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.RegisterActivity;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.V2.Company.CompanyProfileActivity;
import com.tagusnow.tagmejob.V2.Model.Company;
import com.tagusnow.tagmejob.ViewCardCVActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Album;
import com.tagusnow.tagmejob.feed.Category;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.v3.category.FilterCategoryActivity;
import com.tagusnow.tagmejob.v3.comment.CommentListener;
import com.tagusnow.tagmejob.v3.feed.FeedListener;
import com.tagusnow.tagmejob.v3.feed.detail.DetailFeedActivity;
import com.tagusnow.tagmejob.v3.like.LikeResultListener;
import com.tagusnow.tagmejob.v3.like.LikeResultView;
import com.tagusnow.tagmejob.v3.post.PostJobActivity;
import com.tagusnow.tagmejob.v3.post.PostNormalActivity;
import com.tagusnow.tagmejob.v3.post.PostStaffActivity;
import com.tagusnow.tagmejob.v3.profile.AuthHeader;
import com.tagusnow.tagmejob.v3.profile.AuthMoreFragment;
import com.tagusnow.tagmejob.v3.profile.AuthProfileActivity;
import com.tagusnow.tagmejob.v3.profile.MoreAuthListener;
import com.tagusnow.tagmejob.v3.profile.MoreUserListener;
import com.tagusnow.tagmejob.v3.profile.UserHeader;
import com.tagusnow.tagmejob.v3.profile.UserMoreFragment;
import com.tagusnow.tagmejob.v3.profile.UserProfileActivity;
import com.tagusnow.tagmejob.v3.profile.friend.FriendActivity;
import com.tagusnow.tagmejob.v3.profile.photo.UserPhotoActivity;
import com.tagusnow.tagmejob.v3.search.suggest.JobSuggestActivity;
import com.tagusnow.tagmejob.v3.search.suggest.StaffSuggestActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForCompanyActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForContentActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForJobsActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForPeopleActivity;
import com.tagusnow.tagmejob.view.Search.Main.Activity.SearchForStaffActivity;
import com.thefinestartist.finestwebview.FinestWebView;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.whalemare.sheetmenu.SheetMenu;

public class TagUsJobRelativeLayout extends RelativeLayout{

    protected File mFileSelected;
    protected AlertDialog.Builder builder;
    protected DialogInterface dialog;
    protected Activity activity;
    protected boolean isRequest = false;
    protected int lastPage = 0;
    private int exetension[] = {R.drawable.aac,R.drawable.ai,R.drawable.avi,R.drawable.bmp,R.drawable.cad,R.drawable.cdr,R.drawable.css,R.drawable.dat,R.drawable.dll,
            R.drawable.dmg,R.drawable.doc,R.drawable.eps,R.drawable.fla,R.drawable.flv,R.drawable.gif,R.drawable.html,R.drawable.indd,R.drawable.iso,R.drawable.jpg,
            R.drawable.js,R.drawable.midi,R.drawable.mov,R.drawable.mp3,R.drawable.mpg,R.drawable.pdf,R.drawable.php,R.drawable.png,R.drawable.ppt,R.drawable.ps,
            R.drawable.psd,R.drawable.raw,R.drawable.sql,R.drawable.svg,R.drawable.tif,R.drawable.txt,R.drawable.wmv,R.drawable.xls,R.drawable.xml,R.drawable.zip};
    private List<String> StrExt = new ArrayList<String>() {{
        add("acc");add("ai");add("avi");add("bmp");add("cad");add("cdr");add("css");add("dat");add("dll");add("dmg");
        add("doc");add("eps");add("fla");add("flv");add("gif");add("html");add("indd");add("iso");add("jpg");add("js");
        add("midi");add("mov");add("mp3");add("mpg");add("pdf");add("php");add("png");add("ppt");add("ps");add("psd");
        add("raw");add("sql");add("svg");add("tif");add("txt");add("wmv");add("xls");add("xml");add("zip");
    }};

    private FollowService followService = ServiceGenerator.createService(FollowService.class);

    public static final String DATE_FORMAT_yyyy_MM_dd = "yyyy-MM-dd";
    public static final String DATE_FORMAT_dd_s_MM_s_yyyy = "dd/MM/yyyy";
    public static final String DATE_FORMAT_yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_MM_dd_yyyy = "MM dd, yyyy";
    public static final String DATE_FORMAT_E_MMM_dd_yyyy = "E, MMM dd yyyy";
    public static final String DATE_FORMAT_E_MMM_dd_yyyy_HH_mm_ss = "E, MMM dd yyyy HH:mm:ss";
    public static final String DATE_FORMAT_MMM_yyyy = "MMM yyyy";

    public String GetDate(String date,String oldDateFormat,String newDateFormat){
        String newDate = "";
        SimpleDateFormat f = new SimpleDateFormat(oldDateFormat, Locale.ENGLISH);
        SimpleDateFormat newFormat = new SimpleDateFormat(newDateFormat,Locale.ENGLISH);
        try {
            Date d = f.parse(date);
            newDate = newFormat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        return newDate;
    }

    public String GetDate(String date,String format){
        String newDate = "";
        SimpleDateFormat f = new SimpleDateFormat(DATE_FORMAT_yyyy_MM_dd, Locale.ENGLISH);
        SimpleDateFormat newFormat = new SimpleDateFormat(format,Locale.ENGLISH);
        try {
            Date d = f.parse(date);
            newDate = newFormat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        return newDate;
    }

    public String GetDate(Date date,String format){
        String newDate = "";
        SimpleDateFormat newFormat = new SimpleDateFormat(format,Locale.ENGLISH);
        newDate = newFormat.format(date);
        return newDate;
    }

    public void MoreAuth(AuthHeader v, SmUser user, MoreAuthListener listener){
        FragmentManager fm = ((AppCompatActivity) activity).getSupportFragmentManager();
        AuthMoreFragment fragment = new AuthMoreFragment();
        fragment.setV(v);
        fragment.setUser(user);
        fragment.setListener(listener);
        fragment.show(fm,"MoreAuthFragment");
    }

    public void MoreUser(UserHeader view, SmUser user, MoreUserListener listener){
        FragmentManager fm = ((AppCompatActivity) activity).getSupportFragmentManager();
        UserMoreFragment fragment = new UserMoreFragment();
        fragment.setV(view);
        fragment.setUser(user);
        fragment.setListener(listener);
        fragment.show(fm,"UserMoreFragment");
    }

    private static final String PROFILE_LINK = "tagusjob_profile_link";

    public void CopyProfileLink(SmUser user, DialogFragment fragment){
        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(PROFILE_LINK,user.getProfile_link());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(activity,"Copied",Toast.LENGTH_SHORT).show();
        fragment.dismiss();
    }

    public void CopyText(String text){
        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(text,text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(activity,"Copied",Toast.LENGTH_SHORT).show();
    }

    public void LoginPage(){
        activity.startActivity(LoginActivity.createIntent(activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void OpenWebsite(String url){
        new FinestWebView.Builder(activity).show(url);
    }

    public void ShowAllUserLiked(SmUser user, Feed feed, LikeResultListener listener){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LikeResultView view = new LikeResultView(activity);
        view.setActivity(activity);
        view.setAuth(user);
        view.setFeed(feed);
        view.setListener(listener);
        view.fetchData();
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        alertDialog.getWindow().setLayout((int) (displayWidth * 0.9f),(int) (displayHeight * 0.7f));
    }

    public void Follow(int follower,int following){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Follow").setMessage("Do you want to follow this user?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                followService.hasFollow(follower,following).enqueue(new Callback<SmUser>() {
                    @Override
                    public void onResponse(Call<SmUser> call, Response<SmUser> response) {
                        if (response.isSuccessful()){
                            Log.w("Follow",response.message());
                        }else {
                            try {
                                Log.e("Follow",response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SmUser> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public void Unfollow(int follower,int following){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Unfollow").setMessage("Do you want to unfollow this user?").setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                followService.hasUnfollow(follower,following).enqueue(new Callback<SmUser>() {
                    @Override
                    public void onResponse(Call<SmUser> call, Response<SmUser> response) {
                        if (response.isSuccessful()){
                            Log.w("Unfollow",response.message());
                        }else {
                            try {
                                Log.e("Unfollow",response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SmUser> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public void DoFollow(SmUser Auth,SmUser user){
        if (user.isIs_follow()){
            Unfollow(user.getId(),Auth.getId());
        }else {
            Follow(user.getId(),Auth.getId());
        }
    }

    public void OpenFriend(SmUser user){
        activity.startActivity(FriendActivity.createIntent(activity).putExtra(FriendActivity.USER,new Gson().toJson(user)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void OpenGallery(SmUser user){
        activity.startActivity(UserPhotoActivity.createIntent(activity).putExtra(UserPhotoActivity.USER,new Gson().toJson(user)));
    }

    public void OpenFilterCategory(Category category){
        activity.startActivity(FilterCategoryActivity
                .createIntent(activity)
                .putExtra(FilterCategoryActivity.CATEGORY,new Gson().toJson(category))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void setName(TextView textView,SmUser user){
        if (user.getFirs_name() != null && !user.getFirs_name().equals("") && user.getLast_name() != null && !user.getLast_name().equals("")){
            textView.setText((user.getFirs_name() + " " + user.getLast_name()));
        }else {
            textView.setText(user.getUser_name());
        }
    }

    public enum NameType {FULL,LAST,FIRST}

    public String getName(SmUser user,NameType type){
        if (type == NameType.FULL){
            if (user.getUser_name() != null && !user.getUser_name().equals("")){
                return user.getUser_name();
            }else {
                if (user.getFirs_name() != null && !user.getFirs_name().equals("") && user.getLast_name() != null && !user.getLast_name().equals("")){
                    return user.getFirs_name() + " " + user.getLast_name();
                }else {
                    if (user.getFirs_name() != null && !user.getFirs_name().equals("")){
                        return user.getFirs_name();
                    }else if (user.getLast_name() != null && !user.getLast_name().equals("")){
                        return  user.getLast_name();
                    }else {
                        return "";
                    }
                }
            }
        }else if (type == NameType.FIRST){
            if (user.getFirs_name() != null && !user.getFirs_name().equals("")){
                return user.getFirs_name();
            }else {
                return "";
            }
        }else if (type == NameType.LAST){
            if (user.getLast_name() != null && !user.getLast_name().equals("")){
                return  user.getLast_name();
            }else {
                return "";
            }
        }else {
            return "";
        }
    }

    public void setUserCover(ImageView cover,SmUser user){
        if (user.getCover() != null && !user.getCover().equals("")){
            if (user.getCover().contains("http")) {
                Glide.with(activity)
                        .load(user.getCover())
                        .error((user.getUser_type() == SmUser.FIND_STAFF) ? R.drawable.coverfindstaffdefual : R.drawable.coverdefault1)
                        .centerCrop().into(cover);
            }else if (user.getCover().contains("storage/emulated/0/")){
                Glide.with(activity)
                        .load(user.getCover())
                        .error((user.getUser_type() == SmUser.FIND_STAFF) ? R.drawable.coverfindstaffdefual : R.drawable.coverdefault1)
                        .centerCrop().into(cover);
            }else {
                Glide.with(activity)
                        .load(SuperUtil.getCover(user.getCover()))
                        .error((user.getUser_type() == SmUser.FIND_STAFF) ? R.drawable.coverfindstaffdefual : R.drawable.coverdefault1)
                        .centerCrop().into(cover);
            }
        }else {
            Glide.with(activity)
                    .load((user.getUser_type() == SmUser.FIND_STAFF) ? R.drawable.coverfindstaffdefual : R.drawable.coverdefault1)
                    .centerCrop().into(cover);
        }
    }

    public void setUserProfile(CircleImageView imageView, SmUser user){

        if (user.getImage() != null){
            if (user.getImage().contains("http")) {
                Glide.with(activity).load(user.getImage()).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }else if (user.getImage().contains("storage/emulated/0/")){
                Glide.with(activity).load(user.getImage()).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }else{
                Glide.with(activity).load(SuperUtil.getProfilePicture(user.getImage(),"400")).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }
        }else{
            Glide.with(activity).load(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).centerCrop().into(imageView);
        }

    }

    public void setUserProfile(ImageView imageView, SmUser user){

        if (user.getImage() != null){
            if (user.getImage().contains("http")){
                Glide.with(activity).load(user.getImage()).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }else{
                Glide.with(activity).load(SuperUtil.getProfilePicture(user.getImage(),"400")).centerCrop().error(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).into(imageView);
            }
        }else{
            Glide.with(activity).load(user.getUser_type() == SmUser.FIND_STAFF ? R.drawable.employer : (user.getGenderId() == 2 ? R.drawable.female_color : R.drawable.employee)).centerCrop().into(imageView);
        }

    }

    public void setAlbumPhoto(ImageView imageView, Album album, String size){

        if (album != null){
            if (album.getMedia().getImage().getSrc().contains("http")){
                Glide.with(activity).load(album.getMedia().getImage().getSrc()).fitCenter().error(R.drawable.not_available_).into(imageView);
            }else {
                Glide.with(activity).load(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),size)).fitCenter().error(R.drawable.not_available_).into(imageView);
            }
        }else {
            Glide.with(activity).load(R.drawable.not_available_).fitCenter().into(imageView);
        }

    }

    public void setAlbumPhoto(ImageView imageView, Album album){
        if (album != null){
            if (album.getMedia().getImage().getSrc().contains("http")){
                Glide.with(activity).load(album.getMedia().getImage().getSrc()).centerCrop().error(R.drawable.not_available_).into(imageView);
            }else {
                Glide.with(activity).load(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"700")).centerCrop().error(R.drawable.not_available_).into(imageView);
            }
        }else {
            Glide.with(activity).load(R.drawable.not_available_).fitCenter().into(imageView);
        }
    }

    public void setAlbumPhoto(ImageView imageView, Album album, boolean isFitCenter){
        if (album != null){
            if (album.getMedia().getImage().getSrc().contains("http")){
                if (isFitCenter){
                    Glide.with(activity).load(album.getMedia().getImage().getSrc()).fitCenter().error(R.drawable.not_available_).into(imageView);
                }else {
                    Glide.with(activity).load(album.getMedia().getImage().getSrc()).centerCrop().error(R.drawable.not_available_).into(imageView);
                }
            }else {
                if (isFitCenter){
                    Glide.with(activity).load(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"700")).fitCenter().error(R.drawable.not_available_).into(imageView);
                }else {
                    Glide.with(activity).load(SuperUtil.getAlbumPicture(album.getMedia().getImage().getSrc(),"700")).centerCrop().error(R.drawable.not_available_).into(imageView);
                }
            }
        }else {
            Glide.with(activity).load(R.drawable.not_available_).fitCenter().into(imageView);
        }
    }

    public void setAlbumPhoto(ImageView imageView, String path, String size){
        Glide.with(activity).load(SuperUtil.getAlbumPicture(path,size)).fitCenter().error(R.drawable.not_available_).into(imageView);
    }

    public void setAlbumPhoto(ImageView imageView, String path, String size,boolean isFitCenter){
        if (isFitCenter){
            Glide.with(activity).load(SuperUtil.getAlbumPicture(path,size)).fitCenter().error(R.drawable.not_available_).into(imageView);
        }else {
            Glide.with(activity).load(SuperUtil.getAlbumPicture(path,size)).centerCrop().error(R.drawable.not_available_).into(imageView);
        }
    }

    public void setCategoryPhoto(ImageView imageView, String path){
        Glide.with(activity).load(SuperUtil.getCategoryPath(path)).fitCenter().error(R.drawable.not_available_).into(imageView);
    }

    public void setAlbumPhoto(ImageView imageView, String path){
        if (path.contains("http")){
            Glide.with(activity).load(path).fitCenter().error(R.drawable.not_available_).into(imageView);
        }else {
            Glide.with(activity).load(SuperUtil.getAlbumPicture(path,"700")).fitCenter().error(R.drawable.not_available_).into(imageView);
        }
    }

    public void ApplyJob(Feed feed) {
        activity.startActivity(ApplyJobActivity.createIntent(this.activity).putExtra(ApplyJobActivity.FEED,feed));
    }

    public void ShareFeed(Feed feed){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,feed.getDetail());
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    public void MoreFeed(SmUser Auth, Feed feed, FeedListener listener){
        int menu = 0;
        if (Auth!=null && Auth.getId()==feed.getUser_post().getId()){
            menu = R.menu.menu_post_option_auth;
        }else{
            menu = R.menu.menu_post_option_not_auth;
        }
        SheetMenu.with(activity)
                .setAutoCancel(true)
                .setMenu(menu)
                .setClick(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.optionSavePost :
                                /*code*/
                                listener.saveFeed(Auth,feed);
                                break;
                            case R.id.optionEditPost :
                                listener.editFeed(Auth,feed);
                                break;
                            case R.id.optionHide :
                                listener.hideFeed(Auth,feed);
                                break;
                            case R.id.optionDelete :
                                listener.removeFeed(Auth,feed);
                                break;
                        }
                        return false;
                    }
                })
                .showIcons(true).show();
    }

    public void MoreRecentResume(int city,int category, String cityName,String categoryName){
        activity.startActivity(LastedPostActivity
                .createIntent(activity)
                .putExtra(LastedPostActivity.CITY,city)
                .putExtra(LastedPostActivity.CATEGORY,category)
                .putExtra(LastedPostActivity.CITY_NAME,cityName)
                .putExtra(LastedPostActivity.CATEGORY_NAME,categoryName)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void PostNormal(){
        activity.startActivity(PostNormalActivity
                .createIntent(this.activity)
                .putExtra(PostNormalActivity.IS_EDIT,false)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void EditPostNormal(Feed feed){

        if (feed.getFeed().getType().equals(FeedDetail.NORMAL_POST)){
            activity.startActivity(PostNormalActivity
                    .createIntent(this.activity)
                    .putExtra(PostNormalActivity.IS_EDIT,true)
                    .putExtra(PostNormalActivity.FEED,new Gson().toJson(feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else if (feed.getFeed().getType().equals(FeedDetail.JOB_POST)){
            activity.startActivity(PostJobActivity
                    .createIntent(this.activity)
                    .putExtra(PostJobActivity.IS_EDIT,true)
                    .putExtra(PostJobActivity.FEED,new Gson().toJson(feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            activity.startActivity(PostStaffActivity
                    .createIntent(this.activity)
                    .putExtra(PostStaffActivity.IS_EDIT,true)
                    .putExtra(PostStaffActivity.FEED,new Gson().toJson(feed))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    public void PostJob(SmUser Auth){
        if (Auth.getUser_type() == SmUser.FIND_STAFF){
            activity.startActivity(PostJobActivity.createIntent(activity)
                    .putExtra(PostJobActivity.IS_EDIT,false)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle("Post Job").setMessage("Please login or Register as job poster to use this function.").setNegativeButton("Register", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    OpenRegister();
                }
            }).setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create().show();
        }
    }

    public void OpenRegister(){
        activity.startActivity(RegisterActivity.createIntent(activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void PostResume(SmUser Auth){
        if (Auth.getUser_type() == SmUser.FIND_JOB){
            activity.startActivity(PostStaffActivity.createIntent(activity)
                    .putExtra(PostStaffActivity.IS_EDIT,false)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle("Post Resume").setMessage("Please Login or Register as job finder to use this function.").setNegativeButton("Register", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    OpenRegister();
                }
            }).setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create().show();
        }
    }

    public void DetailFeedNormal(Feed feed){
        activity.startActivity(DetailFeedActivity.createIntent(activity)
                .putExtra(DetailFeedActivity.FEED,new Gson().toJson(feed))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void DetailFeedJob(SmUser user,Feed feed){
        activity.startActivity(JobDetail.createIntent(this.activity)
                .putExtra(JobDetail.FEED,feed)
                .putExtra(JobDetail.USER,user.getId())
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void DetailFeedCV(Feed feed){
        activity.startActivity(ViewCardCVActivity.createIntent(activity)
                .putExtra(ViewCardCVActivity.FEED,new Gson().toJson(feed))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void OpenProfile(SmUser Auth, SmUser user){
        if (Auth.getId()==user.getId()){
            activity.startActivity(AuthProfileActivity.createIntent(activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else{
            activity.startActivity(UserProfileActivity.createIntent(activity).putExtra(UserProfileActivity.USER,new Gson().toJson(user)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    public void OpenComment(Feed feed){
        activity.startActivity(CommentActivity.createIntent(this.activity)
                .putExtra(CommentActivity.FEED,new Gson().toJson(feed))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void OpenCompany(Company company){
        activity.startActivity(CompanyProfileActivity.createIntent(activity)
                .putExtra(CompanyProfileActivity.COMPANY,new Gson().toJson(company))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void DisplayDownloadBox(Feed feed){
        CVAndResumeLayout layout = new CVAndResumeLayout(activity);
        layout.setActivity(activity);
        layout.setFeed(feed);
        new AlertDialog.Builder(activity).setView(layout).setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public void SearchForPeople(){
        activity.startActivity(SearchForPeopleActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForPeople(String searchText){
        activity.startActivity(SearchForPeopleActivity.createIntent(this.activity).putExtra(SearchForPeopleActivity.SEARCH_TEXT,searchText).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForCompany(){
        activity.startActivity(SearchForCompanyActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForCompany(String searchText){
        activity.startActivity(SearchForCompanyActivity.createIntent(this.activity).putExtra(SearchForCompanyActivity.SEARCH_TEXT,searchText).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForJob(){
        activity.startActivity(SearchForJobsActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForJob(String searchText){
        activity.startActivity(SearchForJobsActivity.createIntent(this.activity).putExtra(SearchForJobsActivity.SEARCH_TEXT,searchText).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForStaff(){
        activity.startActivity(SearchForStaffActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForStaff(String searchText){
        activity.startActivity(SearchForStaffActivity.createIntent(this.activity).putExtra(SearchForStaffActivity.SEARCH_TEXT,searchText).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForContent(){
        activity.startActivity(SearchForContentActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void SearchForContent(String searchText){
        activity.startActivity(SearchForContentActivity.createIntent(this.activity).putExtra(SearchForContentActivity.SEARCH_TEXT,searchText).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void JobSuggestion(){
        activity.startActivity(JobSuggestActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void StaffSuggestion(){
        activity.startActivity(StaffSuggestActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void OnOptionComment(Comment comment, CommentListener.OnOptionSelected onOptionSelected){
        SheetMenu.with(activity)
                .setAutoCancel(true)
                .setMenu(R.menu.menu_comment_option)
                .setClick(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.btnCopyComment :
                                /*code*/
                                onOptionSelected.copy(TagUsJobRelativeLayout.this,comment);
                                break;
                            case R.id.btnEditComment :
                                onOptionSelected.edit(TagUsJobRelativeLayout.this,comment);
                                break;
                            case R.id.btnDeleteComment :
                                onOptionSelected.delete(TagUsJobRelativeLayout.this,comment);
                                break;
                        }
                        return false;
                    }
                }).showIcons(true).show();
    }

    public boolean IsMessageSound(){
        return ((TagUsJobActivity) activity).IsMessageSound();
    }

    public boolean IsNotificationAlert(){
        return ((TagUsJobActivity) activity).IsNotificationAlert();
    }

    public void showProgress(){
        ((TagUsJobActivity) activity).showProgress();
    }

    public void dismissProgress(){
        ((TagUsJobActivity) activity).dismissProgress();
    }

    public void ShowAlert(String title,String msg){
        ((TagUsJobActivity) activity).ShowAlert(title,msg);
    }

    public ImageView CheckExtension(String url){
        ImageView imageView = new ImageView(this.activity);
        String extension = url.substring(url.lastIndexOf("."));
        Glide.with(this.activity)
                .load(exetension[StrExt.indexOf(extension.substring(1).toLowerCase())])
                .fitCenter()
                .error(R.drawable.ic_error)
                .into(imageView);
        return imageView;
    }

    public int ExtensionResource(String url){
        String extension = url.substring(url.lastIndexOf("."));
        return exetension[StrExt.indexOf(extension.substring(1).toLowerCase())];
    }

    public void PlayMessageSound(){
        ((TagUsJobActivity) activity).PlayMessageSound();
    }

    private void init(Context context){

    }

    public TagUsJobRelativeLayout(Context context) {
        super(context);
        init(context);
    }

    public TagUsJobRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TagUsJobRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public TagUsJobRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private String getMimeTypeFromMediaContentUri(Uri uri) {
        String mimeType;
        if (Objects.equals(uri.getScheme(), ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = activity.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    @NonNull
    protected RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    protected MultipartBody.Part prepareFilePart(String partName, File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse(getMimeTypeFromMediaContentUri(Uri.fromFile(file))),file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    protected Activity getActivity(){
        return activity;
    }

    protected void setActivity(Activity activity){
        this.activity = activity;
    }

    protected void selectImage(){}
}

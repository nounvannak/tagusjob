package com.tagusnow.tagmejob.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserFollowSuggestLayout extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = UserFollowSuggestLayout.class.getSimpleName();
    private SmUser userFollowSuggest,authUser;
    private ImageView profile_picture;
    private TextView profile_name,follower_counter;
    private ImageButton btnFollow;
    private Context context;
    private Activity activity;
    /*Init Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService service = retrofit.create(FollowService.class);

    public UserFollowSuggestLayout(Context context) {
        super(context);
        inflate(context, R.layout.face_follow_suggest,this);
        profile_picture = findViewById(R.id.profile_picture);
        profile_name = findViewById(R.id.profile_name);
        follower_counter = findViewById(R.id.follower_counter);
        btnFollow = findViewById(R.id.btnFollow);
        profile_name.setOnClickListener(this);
        profile_picture.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
        this.setOnClickListener(this);
        this.context = context;
        this.authUser = new Auth(context).checkAuth().token();
    }

    public UserFollowSuggestLayout with(Activity activity){
        this.activity = activity;
        return this;
    }

    public void setUserFollowSuggest(SmUser userFollowSuggest) {
        this.userFollowSuggest = userFollowSuggest;
        if (this.userFollowSuggest!=null){

            if (this.userFollowSuggest.isIs_follow()){
                btnFollow.setBackgroundResource(R.drawable.ic_action_following);
            }

            if (this.userFollowSuggest.getImage()!=null){
                if (this.userFollowSuggest.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(this.userFollowSuggest.getImage())
                            .centerCrop()
                            .error(R.drawable.ic_user_male_icon)
                            .into(profile_picture);
                }else{
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(this.userFollowSuggest.getImage(),"200"))
                            .centerCrop()
                            .error(R.drawable.ic_user_male_icon)
                            .into(profile_picture);
                }
            }else{
                Glide.with(this.context)
                        .load(R.drawable.ic_user_male_icon)
                        .centerCrop()
                        .into(profile_picture);
            }
            profile_name.setText(this.userFollowSuggest.getUser_name());
            follower_counter.setText(this.context.getString(R.string.follower_counter,this.userFollowSuggest.getFollower_counter()));
        }
    }

    public ImageButton getBtnFollow() {
        return btnFollow;
    }

    private void viewProfile(Activity activity,SmUser userFollowSuggest){
        if (activity!=null && userFollowSuggest!=null){
            this.context.startActivity(ViewProfileActivity.createIntent(activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("profileUser",new Gson().toJson(userFollowSuggest)));
        }else {
            if (activity==null){
                Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this.context,"No Access to User",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void changeUI(SmUser smUser){
        if (smUser!=null){
            btnFollow.setBackgroundResource(R.drawable.ic_person_blue_24dp);
        }
    }

    private void hasFollow(){
        int following = userFollowSuggest!=null ? userFollowSuggest.getId() : 0;
        int follower = authUser!=null ? authUser.getId() : 0;
        service.hasFollow(follower,following).enqueue(new Callback<SmUser>() {
            @Override
            public void onResponse(Call<SmUser> call, Response<SmUser> response) {
                Log.w(TAG,response.message());
                changeUI(response.body());
            }

            @Override
            public void onFailure(Call<SmUser> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void onFollow(){
        if (userFollowSuggest!=null && authUser!=null){
            hasFollow();
        }else{
            if (userFollowSuggest==null){
                Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this.context,"No Auth User",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this.profile_picture){
            viewProfile(this.activity,this.userFollowSuggest);
        }else if(view==this.profile_name){
            viewProfile(this.activity,this.userFollowSuggest);
        }else if(view==this.btnFollow){
            onFollow();
        }else if(view==this){
            viewProfile(this.activity,this.userFollowSuggest);
        }
    }
}

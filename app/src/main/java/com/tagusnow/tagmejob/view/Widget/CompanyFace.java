package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.v3.page.resume.CompanyFaceListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;

public class CompanyFace extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private ShapedImageView profile;
    private TextView name;
    private RelativeLayout btnView;
    private Feed feed;

    public void setCompanyFaceListener(CompanyFaceListener companyFaceListener) {
        this.companyFaceListener = companyFaceListener;
    }

    private CompanyFaceListener companyFaceListener;

    private void initUI(Context context){
        inflate(context, R.layout.company_face,this);
        this.context = context;
        profile = (ShapedImageView)findViewById(R.id.profile);
        name = (TextView)findViewById(R.id.name);
        btnView = (RelativeLayout)findViewById(R.id.btnView);
        btnView.setOnClickListener(this);
    }

    public CompanyFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public CompanyFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public CompanyFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public CompanyFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view==btnView){
            companyFaceListener.viewStaff(this,feed);
        }
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
        setName(name,feed.getUser_post());
        setAlbumPhoto(profile,(feed.getFeed().getAlbum() != null ? feed.getFeed().getAlbum().get(0) : null));
    }
}

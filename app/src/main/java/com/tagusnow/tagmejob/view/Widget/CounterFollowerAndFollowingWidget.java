package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.ViewFollower;
import com.tagusnow.tagmejob.ViewFollowing;

public class CounterFollowerAndFollowingWidget extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = CounterFollowerAndFollowingWidget.class.getSimpleName();

    private TextView following_counter,follower_counter;
    private Context context;
    private Activity activity;
    private int following,follower = 0;

    public CounterFollowerAndFollowingWidget(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.counter_follower_and_following_widget,this);
        follower_counter = (TextView)findViewById(R.id.follower_counter);
        following_counter = (TextView)findViewById(R.id.following_counter);
        RelativeLayout btnFollower = (RelativeLayout) findViewById(R.id.btnFollower);
        RelativeLayout btnFollowing = (RelativeLayout) findViewById(R.id.btnFollowing);
        btnFollowing.setOnClickListener(this);
        btnFollower.setOnClickListener(this);
    }

    public CounterFollowerAndFollowingWidget with(Activity activity){
        this.activity = activity;
        return this;
    }

    private void viewFollower(Activity activity){
        this.activity = activity;
        if (activity!=null){
            this.context.startActivity(ViewFollower.createIntent(activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else{
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
            Log.w(TAG,"viewFollower::No Activity");
        }
    }

    private void viewFollowing(Activity activity){
        this.activity = activity;
        if (activity!=null){
            this.context.startActivity(ViewFollowing.createIntent(activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else{
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
            Log.w(TAG,"viewFollowing::No Activity");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnFollower:
                viewFollower(this.activity);
                break;
            case R.id.btnFollowing:
                viewFollowing(this.activity);
                break;
        }
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
        this.following_counter.setText(String.valueOf(following));
    }

    public int getFollwer() {
        return follower;
    }

    public void setFollwer(int follower) {
        this.follower = follower;
        this.follower_counter.setText(String.valueOf(follower));
    }
}

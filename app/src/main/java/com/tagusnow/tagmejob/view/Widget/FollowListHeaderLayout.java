package com.tagusnow.tagmejob.view.Widget;

import android.content.Context;
import android.widget.LinearLayout;

import com.tagusnow.tagmejob.R;

public class FollowListHeaderLayout extends LinearLayout{

    public FollowListHeaderLayout(Context context) {
        super(context);
        inflate(context, R.layout.follow_lists_header_layout,this);
    }
}

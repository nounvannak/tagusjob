package com.tagusnow.tagmejob.view.Widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import com.tagusnow.tagmejob.R;

public class FollowListLayout extends LinearLayout {

    private RecyclerView recyclerFollow;
    private Context context;

    public FollowListLayout(Context context) {
        super(context);
        inflate(context, R.layout.follow_lists_layout,this);
        this.context = context;
        recyclerFollow = (RecyclerView)findViewById(R.id.recyclerFollow);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerFollow.setHasFixedSize(true);
        recyclerFollow.setLayoutManager(layoutManager);
    }

    public RecyclerView getRecyclerFollow() {
        return recyclerFollow;
    }

    public void setAdapter(RecyclerView.Adapter adapter){
        this.recyclerFollow.setAdapter(adapter);
    }
}

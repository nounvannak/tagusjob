package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tagusnow.tagmejob.Handler.User.UserHandler;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewProfileActivity;
import com.tagusnow.tagmejob.adapter.FollowSuggestHorizontalAdapter;
import com.tagusnow.tagmejob.adapter.SuggestUserAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import java.util.ArrayList;

import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowSuggestFace extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = FollowSuggestFace.class.getSimpleName();
    private Context context;
    private SmUser auth,user;
    private AvatarView profile_picture;
    private TextView profile_name,follower_counter;
    private ImageButton btnClose;
    private Button btnFollow;
    private RecyclerView.Adapter adapter;
    private SuggestUserAdapter suggestUserAdapter;
    private FollowSuggestHorizontalAdapter followSuggestHorizontalAdapter;
    private String adapterTag = TAG;
    private int position;
    private AvatarPlaceholder refreshableAvatarPlaceholder;
    private IImageLoader imageLoader;
    private boolean isSuggestUserAdapter = false;

    public void setUserHandler(UserHandler userHandler) {
        this.userHandler = userHandler;
    }

    private UserHandler userHandler;
    private boolean isFollowSuggestHorizontalAdapte = false;
    private ArrayList<SmUser> FollowList = new ArrayList<>();
    private boolean isShowCloseButton,isShowProfile,isShowCounterFollowing,isShowCounterPost,isResponse = true;
    /*Service*/
    private FollowService service = ServiceGenerator.createService(FollowService.class);
    private Callback<SmUser> mFollowCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                User(response.body());
                FollowList.add(response.body());
                if (isSuggestUserAdapter){
                    suggestUserAdapter.setFollowList(FollowList);
                }
                btnFollow.setText("Unfollow");
            }else {
                Log.e(TAG,response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<SmUser> mUnfollowCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                User(response.body());
                FollowList.remove(response.body());
                if (isSuggestUserAdapter){
                    suggestUserAdapter.setFollowList(FollowList);
                }
                btnFollow.setText("Follow");
            }else {
                Log.e(TAG,response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    public FollowSuggestFace(Context context) {
        super(context);
        this.initUI(context);
    }

    public FollowSuggestFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public FollowSuggestFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public FollowSuggestFace(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    private void initUI(Context context){
        inflate(context, R.layout.follow_suggest_face,this);
        this.context = context;
        profile_picture = (AvatarView)findViewById(R.id.profile_picture);
        profile_name = (TextView)findViewById(R.id.profile_name);
        follower_counter = (TextView)findViewById(R.id.follower_counter);
        btnClose = (ImageButton)findViewById(R.id.btnClose);
        btnFollow = (Button)findViewById(R.id.btnFollow);
        btnClose.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
        this.setOnClickListener(this);
        if (this.isShowCloseButton){
            btnClose.setVisibility(VISIBLE);
        }
    }

    private void hasFollower(){
        int follower = auth!=null ? auth.getId() : 0;
        int following = user!=null ? user.getId() : 0;
        if (this.isResponse){
            service.hasFollow(follower,following).enqueue(mFollowCallback);
        }else {
            FollowList.add(this.user);
            if (isSuggestUserAdapter){
                suggestUserAdapter.setFollowList(FollowList);
            }
            btnFollow.setText("Unfollow");
        }
    }

    private void hasUnfollow(){
        int follower = auth!=null ? auth.getId() : 0;
        int following = user!=null ? user.getId() : 0;
        if (this.isResponse){
            service.hasUnfollow(follower,following).enqueue(mUnfollowCallback);
        }else {
            FollowList.remove(this.user);
            if (isSuggestUserAdapter){
                suggestUserAdapter.setFollowList(FollowList);
            }
            btnFollow.setText("Follow");
        }
    }

    public FollowSuggestFace User(SmUser user){
        this.user = user;
        if (user!=null){
            this.setVisibility(VISIBLE);
            refreshableAvatarPlaceholder = new AvatarPlaceholder(this.user.getUser_name(), 50);
            imageLoader = new GlideLoader();
            if (user.getImage()!=null){
                if (user.getImage().contains("http")){
                    imageLoader.loadImage(profile_picture, refreshableAvatarPlaceholder,user.getImage());
                }else {
                    imageLoader.loadImage(profile_picture, refreshableAvatarPlaceholder,SuperUtil.getProfilePicture(user.getImage()));
                }
            }else {
                imageLoader.loadImage(profile_picture, refreshableAvatarPlaceholder,null);
            }
            profile_name.setText(user.getUser_name());

            if (this.isShowCounterFollowing){
                follower_counter.setVisibility(VISIBLE);
                follower_counter.setText(this.activity.getString(R.string.follower_counter,user.getFollower_counter()));
            }else {
                follower_counter.setVisibility(GONE);
                if (this.isShowCounterPost){
                    follower_counter.setVisibility(VISIBLE);
                    follower_counter.setText(this.activity.getString(R.string.counter_post_jobs,user.getCounter_post_jobs()));
                }else {
                    follower_counter.setVisibility(GONE);
                }
            }

        }else {
            this.setVisibility(GONE);
            Toast.makeText(this.context,"No SmUser",Toast.LENGTH_SHORT).show();
        }
        return this;
    }

    public FollowSuggestFace Auth(SmUser auth){
        this.auth = auth;
        return this;
    }

    public FollowSuggestFace Activity(Activity activity){
        super.setActivity(activity);
        return this;
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    private void onFollow(){
        if (this.user!=null){
            if (this.user.isIs_follow()){
//                this.userHandler.unfollow(this,this.user,this.position);
                hasUnfollow();
            }else {
//                this.userHandler.follow(this,this.user,this.position);
                hasFollower();
            }
        }else {
            Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewProfile(){
        if (this.user!=null){
            if (this.activity!=null){
                this.context.startActivity(ViewProfileActivity.createIntent(this.activity).putExtra("profileUser",new Gson().toJson(this.user)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }else {
                Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            if (this.isShowProfile){
                viewProfile();
            }
        }else if(view==btnClose){
            if (this.isShowCloseButton){
//                this.userHandler.remove(this,this.user,this.position);
            }
        }else if (view==btnFollow){
            onFollow();
        }
    }

    public FollowSuggestFace Position(int position){
        this.position = position;
        return this;
    }

    public FollowSuggestFace Adapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
        if (this.adapterTag.equals(SuggestUserAdapter.class.getSimpleName())){
            this.isSuggestUserAdapter = true;
            this.suggestUserAdapter = (SuggestUserAdapter)adapter;
            this.FollowList = this.suggestUserAdapter.getFollowList();
        }else if (this.adapterTag.equals(FollowSuggestHorizontalAdapter.class.getSimpleName())){
            this.isFollowSuggestHorizontalAdapte = true;
            this.followSuggestHorizontalAdapter = (FollowSuggestHorizontalAdapter)adapter;
        }
        return this;
    }

    public void setShowProfile(boolean showProfile) {
        isShowProfile = showProfile;
    }

    public void setShowCloseButton(boolean showCloseButton) {
        isShowCloseButton = showCloseButton;
        if (this.isShowCloseButton){
            btnClose.setVisibility(VISIBLE);
        }else {
            btnClose.setVisibility(GONE);
        }
    }

    public void setShowCounterFollowing(boolean showCounterFollowing) {
        isShowCounterFollowing = showCounterFollowing;
    }

    public void setShowCounterPost(boolean showCounterPost) {
        isShowCounterPost = showCounterPost;
    }

    public String getAdapterTag() {
        return adapterTag;
    }

    public void setAdapterTag(String adapterTag) {
        this.adapterTag = adapterTag;
    }

    public void setResponse(boolean response) {
        isResponse = response;
    }
}

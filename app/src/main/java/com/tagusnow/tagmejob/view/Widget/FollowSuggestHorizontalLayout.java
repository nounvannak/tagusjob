package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.Handler.User.UserHandler;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.FollowService;
import com.tagusnow.tagmejob.SeeMoreSuggestFollowActivity;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.adapter.FollowSuggestHorizontalAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.UserPaginate;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FollowSuggestHorizontalLayout extends RelativeLayout implements View.OnClickListener, UserHandler {

    private static final String TAG = FollowSuggestHorizontalLayout.class.getSimpleName();
    private Activity activity;
    private Context context;
    private Button btnSeeMore;
    private RecyclerView recyclerView;
    private SmUser auth;
    private UserPaginate userPaginate;
    private List<SmUser> users;
    private FollowSuggestHorizontalAdapter adapter;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private FollowService service = retrofit.create(FollowService.class);
    private Callback<UserPaginate> mNewCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private Callback<UserPaginate> mNextCallback = new Callback<UserPaginate>() {
        @Override
        public void onResponse(Call<UserPaginate> call, Response<UserPaginate> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextSuggest(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<UserPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };

    static int lastPage;
    static boolean isRequest = false;
    private void checkNext(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (userPaginate.getTo() - 1)) && (userPaginate.getTo() < userPaginate.getTotal())) {
                        if (userPaginate.getNext_page_url()!=null || !userPaginate.getNext_page_url().equals("")){
                            if (lastPage!=userPaginate.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = userPaginate.getCurrent_page();
                                    callNextSuggest();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initNextSuggest(UserPaginate body) {
        this.userPaginate = body;
        this.checkNext();
        this.adapter.update(this.activity,body,this.auth).notifyDataSetChanged();
    }

    private void initNewSuggest(UserPaginate body) {
        this.userPaginate = body;
        this.checkNext();
        this.adapter = new FollowSuggestHorizontalAdapter(this.activity,body,this.auth);
        recyclerView.setAdapter(this.adapter);
    }

    private void callSuggest(){
        int user = this.auth!=null ? this.auth.getId() : 0;
        int limit = 5;
        service.callSuggest(user,limit).enqueue(mNewCallback);
    }

    private void callNextSuggest(){
        int user = this.auth!=null ? this.auth.getId() : 0;
        int limit = 5;
        int page = this.userPaginate.getCurrent_page() + 1;
        service.callSuggest(user,limit,page).enqueue(mNextCallback);
    }

    public FollowSuggestHorizontalLayout Auth(SmUser auth){
        this.auth = auth;
        callSuggest();
        return this;
    }

    public FollowSuggestHorizontalLayout Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public FollowSuggestHorizontalLayout(Context context) {
        super(context);
        inflate(context, R.layout.follow_suggest_horizontal_layout,this);
        this.context = context;
        this.btnSeeMore = (Button)findViewById(R.id.btnSeeMore);
        this.btnSeeMore.setOnClickListener(this);
        this.recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(layoutManager);
    }

    private void seeMoreSuggest(Activity activity){
        this.activity = activity;
        if (activity!=null){
            this.context.startActivity(SeeMoreSuggestFollowActivity.createIntent(activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnSeeMore){
            seeMoreSuggest(this.activity);
        }
    }

    @Override
    public void follow(FollowSuggestFace followSuggestFace, SmUser user, int position) {

    }

    @Override
    public void unfollow(FollowSuggestFace followSuggestFace, SmUser user, int position) {

    }

    @Override
    public void remove(FollowSuggestFace followSuggestFace, SmUser user, int position) {
        this.users.remove(position);
        this.adapter.setUserList(this.users);
        this.adapter.notifyDataSetChanged();
    }
}

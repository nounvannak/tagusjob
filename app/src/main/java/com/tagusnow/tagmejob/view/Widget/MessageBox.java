package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.SocketService;
import com.tagusnow.tagmejob.REST.Service.SocketServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.UserService;
import com.tagusnow.tagmejob.adapter.ChatFileAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Conversation;
import com.tagusnow.tagmejob.model.v2.feed.Socket.MessageTyping;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageBox extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private SmUser user1,user2;
    private ImageButton file,camera,send;
    private EmojiconEditText message_box;
    private View border;
    private RelativeLayout layout_image;
    private File my_file;
    private List<File> images = new ArrayList<>();
    public  int conversation_id = 0;
    private Conversation conversation;
    private ChatFileAdapter adapter;
    private RecyclerView recyclerView;
    private Socket socket;
    private UserService service = ServiceGenerator.createService(UserService.class);
    private SocketService socketService = SocketServiceGenerator.createService(SocketService.class);
    private Callback<Conversation> ChatCallback = new Callback<Conversation>() {
        @Override
        public void onResponse(Call<Conversation> call, Response<Conversation> response) {
            if (response.isSuccessful()){
                ((TagUsJobActivity) activity).dismissProgress();
                clearForm();
                conversation = response.body();
                conversation_id = conversation.getConversation_id();
                Log.w("Message",new Gson().toJson(response.body()));
            }else {
                ((TagUsJobActivity) activity).dismissProgress();
                ((TagUsJobActivity) activity).ShowAlert("Error!",response.message());
            }
        }

        @Override
        public void onFailure(Call<Conversation> call, Throwable t) {
            ((TagUsJobActivity) activity).dismissProgress();
            ((TagUsJobActivity) activity).ShowAlert("Connection Error!",t.getMessage());
        }
    };

    private void InitSocket(){
        this.socket = new App().getSocket();
        this.socket.connect();
    }

    private void OnTypingMessage(Object obj){
        this.socket.emit("Typing Message",obj);
    }

    public ImageButton getFile() {
        return file;
    }

    public void setMy_file(File my_file) {
        this.my_file = my_file;
        this.selectFile();
    }

    private void Chat(){
        if (my_file!=null && message_box.getText().toString().length() > 0){
            MultipartBody.Part file = prepareFilePart("file",my_file);
            RequestBody message = createPartFromString(message_box.getText().toString());
            service.Chat(conversation_id,Conversation.FILE,this.user1.getId(),this.user2.getId(),message,file).enqueue(ChatCallback);
        }else {
            if (my_file!=null) {
                MultipartBody.Part file = prepareFilePart("file", my_file);
                service.Chat(conversation_id, Conversation.FILE, this.user1.getId(), this.user2.getId(), file).enqueue(ChatCallback);
            }else if (images.size() > 0  && message_box.getText().toString().length() > 0) {
                List<MultipartBody.Part> imageParts = new ArrayList<>();
                RequestBody message = createPartFromString(message_box.getText().toString());
                for (File file : this.images) {
                    imageParts.add(prepareFilePart("images[]", file));
                }
                service.Chat(conversation_id, Conversation.IMAGE, this.user1.getId(), this.user2.getId(), message, imageParts).enqueue(ChatCallback);
            }else if (images.size() > 0){
                List<MultipartBody.Part> imageParts = new ArrayList<>();
                for (File file : this.images) {
                    imageParts.add(prepareFilePart("images[]", file));
                }
                service.Chat(conversation_id, Conversation.IMAGE, this.user1.getId(), this.user2.getId(),imageParts).enqueue(ChatCallback);
            }else {
                RequestBody message = createPartFromString(message_box.getText().toString());
                service.Chat(conversation_id,Conversation.TEXT,this.user1.getId(),this.user2.getId(),message).enqueue(ChatCallback);
            }
        }
    }

    private void InitUI(Context context){
        inflate(context, R.layout.message_box,this);
        this.context = context;

        camera = (ImageButton)findViewById(R.id.camera);
        send = (ImageButton)findViewById(R.id.send);

        message_box = (EmojiconEditText)findViewById(R.id.message_body);
        border = (View)findViewById(R.id.border);
        layout_image = (RelativeLayout)findViewById(R.id.layout_image);

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this.context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        camera.setOnClickListener(this);
        send.setOnClickListener(this);

        this.viewCondition();
        this.InitSocket();

        message_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                viewCondition();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("conversation_id",conversation_id);
                    object.put("user_name",user1.getUser_name());
                    object.put("user_id",user1.getId());
                    OnTypingMessage(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                viewCondition();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                viewCondition();
            }
        });
    }

    public MessageBox(Context context) {
        super(context);
        this.InitUI(context);
    }

    public MessageBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.InitUI(context);
    }

    public MessageBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.InitUI(context);
    }

    public MessageBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.InitUI(context);
    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view==camera){
            this.OpenImagePicker();
        }else if (view==send){
            this.SendMessage();
        }
    }

    private void clearForm(){
        images = new ArrayList<>();
        my_file = null;
        message_box.getText().clear();
        viewCondition();
    }

    private boolean isValid(){
        if (message_box.getText().toString().length()==0 && my_file==null && images.size()==0){
            if (message_box.getText().toString().length()==0){
                ((TagUsJobActivity)activity).ShowAlert("Warning!","Message is empty");
            }else if (my_file==null){
                ((TagUsJobActivity)activity).ShowAlert("Warning!","File is empty");
            }else if (images.size()==0){
                ((TagUsJobActivity)activity).ShowAlert("Warning!","Image is empty");
            }
            return false;
        }else {
            return true;
        }
    }

    private void SendMessage(){
        if (isValid()){
            ((TagUsJobActivity)activity).showProgress();
            this.Chat();
        }
    }

    private void OpenImagePicker(){
        AndroidImagePicker.getInstance().pickMulti(activity, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                for (ImageItem image : items){
                    images.add(new File(image.path));
                }
                selectImage();
            }
        });
    }

    private void removeImage(){
        images = null;
        viewCondition();
    }

    protected void selectImage(){
        this.adapter = new ChatFileAdapter(this.activity,this.images);
        this.recyclerView.setAdapter(this.adapter);
        viewCondition();
    }

    public void selectFile(){
        int res[] = {ExtensionResource(this.my_file.getName())};
        this.adapter = new ChatFileAdapter(this.activity,res);
        this.recyclerView.setAdapter(this.adapter);
        viewCondition();
    }

    private void viewCondition(){
        layout_image.setVisibility(View.GONE);
        border.setVisibility(View.GONE);
        send.setVisibility(View.GONE);
        Condition();
    }

    private void TYPING_SOCKET(){
        socketService.MessageTyping(this.user1.getId(),this.user1.getUser_name(),conversation_id).enqueue(new Callback<MessageTyping>() {
            @Override
            public void onResponse(Call<MessageTyping> call, Response<MessageTyping> response) {
                Log.w("Typing Socket",response.message());
            }

            @Override
            public void onFailure(Call<MessageTyping> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void Condition(){
        if (!message_box.getText().toString().equals("")){
            send.setVisibility(View.VISIBLE);
            if (my_file!=null || images.size() > 0){
                layout_image.setVisibility(View.VISIBLE);
                border.setVisibility(View.VISIBLE);
            }else{
                layout_image.setVisibility(View.GONE);
                border.setVisibility(View.GONE);
            }
            if (message_box.getText().length() > 30){
                message_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.MORE_TEXT);
            }else{
                message_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
            }
        }else {
            if (my_file!=null || images.size() > 0){
                layout_image.setVisibility(View.VISIBLE);
                border.setVisibility(View.VISIBLE);
                send.setVisibility(View.VISIBLE);
            }else {
                layout_image.setVisibility(View.GONE);
                border.setVisibility(View.GONE);
                send.setVisibility(View.GONE);
            }
            message_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
        }
    }

    public void setUser2(SmUser user2) {
        this.user2 = user2;
    }

    public void setUser1(SmUser user1) {
        this.user1 = user1;
    }

    public void setConversation_id(int conversation_id) {
        this.conversation_id = conversation_id;
    }
}

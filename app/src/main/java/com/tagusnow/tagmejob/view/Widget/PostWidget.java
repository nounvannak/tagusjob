package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import de.hdodenhof.circleimageview.CircleImageView;

public class PostWidget extends RelativeLayout implements View.OnClickListener{

    private Context context;
    private Activity activity;
    private CircleImageView profile_picture;
    private EditText post_text;
    private ImageButton btnSelectPostImage;

    private String picture;

    public PostWidget(Context context) {
        super(context);
        inflate(context, R.layout.post_widget,this);
        this.context = context;
        profile_picture = (CircleImageView)findViewById(R.id.profile_picture);
        post_text = (EditText)findViewById(R.id.post_text);
        btnSelectPostImage = (ImageButton)findViewById(R.id.btnSelectPostImage);
        profile_picture.setOnClickListener(this);
        post_text.setOnClickListener(this);
        btnSelectPostImage.setOnClickListener(this);
    }

    public PostWidget with(Activity activity){
        this.activity = activity;
        return this;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.profile_picture:
                this.context.startActivity(ProfileActivity.createIntent(this.activity));
                break;
            case R.id.post_text:
                this.context.startActivity(CreatePostActivity.createIntent(this.activity));
                break;
            case R.id.btnSelectPostImage:
                break;
        }
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;

        if (picture!=null){
            if (picture.contains("http")){
                Glide.with(this.context)
                        .load(picture)
                        .centerCrop()
                        .error(R.drawable.ic_person_blue_24dp)
                        .into(profile_picture);
            }else{
                Glide.with(this.context)
                        .load(SuperUtil.getProfilePicture(picture,"200"))
                        .centerCrop()
                        .error(R.drawable.ic_person_blue_24dp)
                        .into(profile_picture);
            }
        }
    }
}

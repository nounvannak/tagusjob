package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SeeSingleTopJobsActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.auth.Token;
import com.tagusnow.tagmejob.feed.SmLocation;
import com.tagusnow.tagmejob.repository.Repository;
import java.util.ArrayList;
import java.util.List;

public class SearchByLocation extends RelativeLayout implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Activity activity;
    private Context context;
    private int category;
    private SmUser user;
    private SmLocation location;
    private Button btnSearch;
    private Spinner spnLocation;
    private int location_id = 0;
    private SeeSingleTopJobsActivity seeSingleTopJobsActivity;
    private List<String> locationList = new ArrayList<>();

    public SearchByLocation(Context context) {
        super(context);
        inflate(context, R.layout.search_by_location,this);
        btnSearch = (Button)findViewById(R.id.btnSearch);
        spnLocation = (Spinner)findViewById(R.id.spnCategory);
        spnLocation.setOnItemSelectedListener(this);
        btnSearch.setOnClickListener(this);
        this.context = context;
        this.initTemp();
    }

    private void initTemp(){
        this.location = new Repository(this.context).restore().getLocation();
        locationList.add(0,"All Location");
        if (this.location!=null){
            for (int i=1;i <= this.location.getData().size();i++){
                locationList.add(i,this.location.getData().get(i-1).getName());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.context,android.R.layout.simple_spinner_dropdown_item,this.locationList);
        spnLocation.setAdapter(adapter);
    }

    public SearchByLocation Category(int category){
        this.category = category;
        return this;
    }

    public SearchByLocation Activity(Activity activity){
        this.activity = activity;
        this.seeSingleTopJobsActivity = (SeeSingleTopJobsActivity) activity;
        return this;
    }

    public SearchByLocation Auth(SmUser user){
        this.user = user;
        return this;
    }

    @Override
    public void onClick(View view) {
        if (view==btnSearch){
            searchJob();
        }
    }

    private void searchJob(){
        if (this.seeSingleTopJobsActivity!=null){
            this.seeSingleTopJobsActivity.setLocation_id(this.location_id);
            this.seeSingleTopJobsActivity.getRefreshLayout().setRefreshing(true);
            this.seeSingleTopJobsActivity.onRefresh();
        }else {
            Toast.makeText(this.context,"No SeeSingleTopJobsActivity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(i==0){
            this.location_id = 0;
        }else {
            this.location_id = this.location.getData().get(i-1).getId();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

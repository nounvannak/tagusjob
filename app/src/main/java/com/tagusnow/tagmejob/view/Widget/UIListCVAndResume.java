package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.artjimlop.altex.AltexImageDownloader;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.feed.CV;

public class UIListCVAndResume extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = UIListCVAndResume.class.getSimpleName();
    private Activity activity;
    private Context context;
    private ImageView file_image;
//    private static final String DOWNLOAD_FOLDER = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    private TextView file_extension,file_name,file_size;
    private RelativeLayout btnDownload;
    private CV cv;

    private void initUI(Context context){
        inflate(context, R.layout.ui_list_cv_and_resume,this);
        this.context = context;

        file_image = (ImageView)findViewById(R.id.file_image);
        file_extension = (TextView)findViewById(R.id.file_extension);
        file_name = (TextView)findViewById(R.id.file_name);
        file_size = (TextView)findViewById(R.id.file_size);
        btnDownload = (RelativeLayout)findViewById(R.id.btnDownload);

        btnDownload.setOnClickListener(this);
    }

    public UIListCVAndResume(Context context) {
        super(context);
        this.initUI(context);
    }

    public UIListCVAndResume(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public UIListCVAndResume(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public UIListCVAndResume(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view==btnDownload){
            this.download();
        }
    }

    public CV getCv() {
        return cv;
    }

    public void setCv(CV cv) {
        this.cv = cv;
        this.initCV(cv);
    }

    private void download(){
        Toast.makeText(this.context,"Downloading...",Toast.LENGTH_SHORT).show();
        AltexImageDownloader.writeToDisk(this.context,SuperUtil.getBaseUrl(this.cv.getFile()),Environment.DIRECTORY_DOWNLOADS);
    }

    private void initCV(CV cv){
        if (cv!=null && cv.getFile()!=null){
            Log.d(TAG,cv.getFile());
            String fileName[] = cv.getFile().split("/");
            String extension = cv.getFile().substring(cv.getFile().lastIndexOf("."));
            file_name.setText(fileName[fileName.length - 1]);
            file_extension.setText(extension.substring(1));
            if (cv.getType() == CV.CV) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (extension.substring(1).toLowerCase().equals("pdf")) {
                        file_image.setImageDrawable(context.getDrawable(R.drawable.pdf));
                    } else if (extension.substring(1).toLowerCase().equals("doc")) {
                        file_image.setImageDrawable(context.getDrawable(R.drawable.doc));
                    } else if (extension.substring(1).toLowerCase().equals("docx")) {
                        file_image.setImageDrawable(context.getDrawable(R.drawable.doc));
                    } else {
                        file_image.setImageDrawable(context.getDrawable(R.drawable.icon_resume));
                    }
                    file_size.setText(context.getString(R.string.resume));
                }
            }else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (extension.substring(1).toLowerCase().equals("pdf")) {
                        file_image.setImageDrawable(context.getDrawable(R.drawable.pdf));
                    } else if (extension.substring(1).toLowerCase().equals("doc")) {
                        file_image.setImageDrawable(context.getDrawable(R.drawable.doc));
                    } else if (extension.substring(1).toLowerCase().equals("docx")) {
                        file_image.setImageDrawable(context.getDrawable(R.drawable.doc));
                    } else {
                        file_image.setImageDrawable(context.getDrawable(R.drawable.icon_resume));
                    }
                    file_size.setText(context.getString(R.string.resume));
                }
                file_size.setText(context.getString(R.string.cover_letter));
            }
        }
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}

package com.tagusnow.tagmejob.view.Widget;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.Gallery;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.model.v2.feed.Socket.SocketTyping;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WidgetCommentBox extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = WidgetCommentBox.class.getSimpleName();
    private Context context;
    private int feed_id,user_id;
    private String comment_text;
    private File comment_picture;
    private RelativeLayout edit_comment_image_layout;
    private ImageView edit_comment_image;
    private ImageButton edit_comment_remove_image,camera,send;
    private EmojiconEditText edit_comment_box;
    private View edit_comment_border;
    private SmUser authUser;
    private Socket socket;
    protected final String OPEN_MEDIA_TITLE = "title";
    protected final String OPEN_MEDIA_MODE = "mode";
    protected final String OPEN_MEDIA_MAX = "maxSelection";
    private CommentService service = ServiceGenerator.createService(CommentService.class);


    private Callback<Comment> mSaveBack = new Callback<Comment>() {
        @Override
        public void onResponse(Call<Comment> call, Response<Comment> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
//                dismissProgress();
                clearForm();
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
//                    dismissProgress();
                    clearForm();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Comment> call, Throwable t) {
//            dismissProgress();
            clearForm();
            t.printStackTrace();
        }
    };

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Typing",TYPING);
        socket.on("Stop Typing",STOP_TYPING);
        socket.connect();
    }

    private Emitter.Listener TYPING = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,String.valueOf(obj.getInt("feed_id")));
                        if (feed_id==obj.getInt("feed_id")){
                            if (obj.has("user_id") && (obj.getInt("user_id")!=authUser.getId())){
                                displayUserComment(obj.getString("user_name"));
                            }else {
                                hideUserComment();
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener STOP_TYPING = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.w(TAG,String.valueOf(obj.getInt("feed_id")));
                        if (feed_id==obj.getInt("feed_id")){
                            hideUserComment();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void init(Context context){
        inflate(context, R.layout.comment_box,this);
        this.context = context;
        edit_comment_image_layout = (RelativeLayout)findViewById(R.id.edit_comment_image_layout);
        edit_comment_image = (ImageView)findViewById(R.id.edit_comment_image);
        edit_comment_remove_image = (ImageButton)findViewById(R.id.edit_comment_remove_image);
        camera = (ImageButton)findViewById(R.id.camera);
        send = (ImageButton)findViewById(R.id.send);
        edit_comment_box = (EmojiconEditText)findViewById(R.id.edit_comment_box);
        edit_comment_border = (View)findViewById(R.id.edit_comment_border);

        /*Event Click*/
        camera.setOnClickListener(this);
        send.setOnClickListener(this);
        edit_comment_remove_image.setOnClickListener(this);

        edit_comment_box.addTextChangedListener(mTextWatch);

        this.viewCondition();
        this.initSocket();
    }

    private TextWatcher mTextWatch = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            conditionCommentBox();
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            conditionCommentBox();
            requestTriggerComment();
        }

        @Override
        public void afterTextChanged(Editable editable) {
            conditionCommentBox();
        }
    };

    public WidgetCommentBox(Context context) {
        super(context);
        this.init(context);
    }

    public WidgetCommentBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public WidgetCommentBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    public WidgetCommentBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    public WidgetCommentBox Activity(Activity activity){
        this.setActivity(activity);
        return this;
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    public WidgetCommentBox Main(int feed_id, int user_id){
        this.feed_id = feed_id;
        this.user_id = user_id;
        return this;
    }

    public WidgetCommentBox User(SmUser authUser){
        this.authUser = authUser;
        return this;
    }

    @NeedsPermission({android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    private void openGallery(){
        AndroidImagePicker.getInstance().pickSingle(this.activity, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {

            }
        });
    }

    private void performCamera(){this.openGallery();}

    private void performSend(){this.send();}

    private void performRemove(){this.removeImage();}

    private void viewCondition(){
        edit_comment_image_layout.setVisibility(View.GONE);
        edit_comment_border.setVisibility(View.GONE);
        send.setVisibility(View.GONE);
        conditionCommentBox();
    }

    private void displayUserComment(String message){
//        rlTriggerComment.setVisibility(View.VISIBLE);
//        trigger_msg.setText(message);
    }

    private void hideUserComment(){
//        rlTriggerComment.setVisibility(View.GONE);
    }

    private void requestTriggerComment(){
        try {
            JSONObject object = new JSONObject();
            object.put("feed_id",feed_id);
            object.put("user_name",authUser.getUser_name());
            object.put("user_id",authUser.getId());
            socket.emit("Typing",object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void clearForm(){
        mFileSelected = null;
        edit_comment_box.getText().clear();
        viewCondition();
    }

    private void conditionCommentBox(){
        if (!edit_comment_box.getText().toString().equals("")){
            send.setVisibility(View.VISIBLE);
            if (mFileSelected!=null){
                edit_comment_image_layout.setVisibility(View.VISIBLE);
                edit_comment_border.setVisibility(View.VISIBLE);
            }else{
                edit_comment_image_layout.setVisibility(View.GONE);
                edit_comment_border.setVisibility(View.GONE);
            }
            if (edit_comment_box.getText().length() > 30){
                edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.MORE_TEXT);
            }else{
                edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
            }
        }else {
            if (mFileSelected!=null){
                edit_comment_image_layout.setVisibility(View.VISIBLE);
                edit_comment_border.setVisibility(View.VISIBLE);
                send.setVisibility(View.VISIBLE);
            }else {
                edit_comment_image_layout.setVisibility(View.GONE);
                edit_comment_border.setVisibility(View.GONE);
                send.setVisibility(View.GONE);
            }
            edit_comment_box.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.NORMAL);
        }
    }

    private void send(){
        showProgress();
        comment();
    }

    private void comment(){
        RequestBody feedId    = createPartFromString(String.valueOf(this.feed_id));
        RequestBody auth_user = createPartFromString(String.valueOf(this.authUser!=null ? this.authUser.getId() : 0));
        RequestBody text      = createPartFromString(edit_comment_box.getText().toString());
        if (this.mFileSelected!=null){
            MultipartBody.Part part = prepareFilePart("image",this.mFileSelected);
            service.save(auth_user,feedId,text,part).enqueue(mSaveBack);
        }else {
            service.save(auth_user,feedId,text).enqueue(mSaveBack);
        }
    }

    private void removeImage(){
        mFileSelected = null;
        viewCondition();
    }

    @Override
    protected void selectImage() {
        Glide.with(this.context)
                .load(mFileSelected)
                .error(R.color.colorBorder)
                .into(edit_comment_image);
        viewCondition();
        super.selectImage();
    }

    @Override
    public void onClick(View view) {
        if (view==camera){
            performCamera();
        }else if (view==send){
            performSend();
        }else if (view==edit_comment_remove_image){
            performRemove();
        }
    }
}

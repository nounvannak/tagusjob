package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.CommentAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Comment;
import com.tagusnow.tagmejob.feed.CommentResponse;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WidgetCommentPopup extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = WidgetCommentPopup.class.getSimpleName();
    private Context context;
    private SmUser user;
    private int feed_id;
    private Button btnDone;
    private WidgetCommentBox comment_box;
    private RecyclerView recycler;
    private CommentAdapter adapter;
    private CommentResponse commentResponse;
    private Socket socket;
    private int limit = 3;
    private CommentService service = ServiceGenerator.createService(CommentService.class);
    private Callback<CommentResponse> mNewCallback = new Callback<CommentResponse>() {
        @Override
        public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNewComment(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CommentResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<CommentResponse> mNextCallback = new Callback<CommentResponse>() {
        @Override
        public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initNextComment(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<CommentResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void getNewComment(){
        service.getList(this.feed_id,this.limit).enqueue(mNewCallback);
    }

    private void getNextComment(){
        int page = this.commentResponse.getCurrent_page() + 1;
        service.getList(this.feed_id,this.limit,page).enqueue(mNextCallback);
    }

    private void initNewComment(CommentResponse commentResponse){
//        commentResponse.setTo(commentResponse.getTo() + CommentAdapter.MORE_VIEW);
//        this.commentResponse = commentResponse;
//        this.onScroll();
//        this.adapter = new CommentAdapter(activity,this.commentResponse,this.user);
//        recycler.setAdapter(this.adapter);
    }

    private void initNextComment(CommentResponse commentResponse){
        this.commentResponse = commentResponse;
        this.onScroll();
        this.adapter.NextData(commentResponse.getData());
    }

    int lastPage = 0;
    boolean isRequest = true;
    private void onScroll(){
        /*recycler view event*/
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;
                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                try {
                    if ((lastCompletelyVisibleItemPosition == (commentResponse.getTo() - 1)) && (commentResponse.getTo() < commentResponse.getTotal())) {
                        if (commentResponse.getNext_page_url()!=null || !commentResponse.getNext_page_url().equals("")){
                            if (lastPage!=commentResponse.getCurrent_page()){
                                isRequest = true;
                                if (isRequest){
                                    lastPage = commentResponse.getCurrent_page();
                                    getNextComment();
                                }
                            }else{
                                isRequest = false;
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            dialog.dismiss();
        }
    };

    private void initNewComment(Comment comment){
//        int to = commentResponse.getTo() + 1;
//        int from = commentResponse.getFrom() + 1;
//        int total = commentResponse.getTotal() + 1;
//        ArrayList<Comment> mComments = new ArrayList<>();
//        commentResponse.setTo(to);
//        commentResponse.setFrom(from);
//        commentResponse.setTotal(total);
//        mComments.add(comment);
//        commentResponse.setData(mComments);
//        this.onScroll();
//        this.adapter.update(this.activity,this.commentResponse,this.user).notifyDataSetChanged();
//        recycler.scrollToPosition(commentResponse.getTo() -1);
    }

    private Emitter.Listener mNewComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
           activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (obj!=null){
                            Comment comment = new Gson().fromJson(obj.toString(),Comment.class);
                            initNewComment(comment);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("New Comment "+feed_id,mNewComment);
        socket.connect();
    }

    @Override
    protected void selectImage() {
        super.selectImage();
    }

    public void ADD_PHOTO(File file){
        this.mFileSelected = file;
        this.selectImage();
    }

    private void init(Context context){
        inflate(context, R.layout.widget_comment_popup,this);
        this.context = context;
        btnDone = (Button)findViewById(R.id.btnDone);
        comment_box = (WidgetCommentBox)findViewById(R.id.comment_box);
        recycler = (RecyclerView)findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recycler.setHasFixedSize(false);
        recycler.setLayoutManager(layoutManager);

        btnDone.setOnClickListener(this);

        this.initSocket();
    }

    public WidgetCommentPopup(Context context) {
        super(context);
        this.init(context);
    }

    public WidgetCommentPopup(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public WidgetCommentPopup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    public WidgetCommentPopup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    public WidgetCommentPopup Auth(SmUser user){
        this.user = user;
        comment_box.Main(feed_id,user.getId()).User(user).Activity(this.activity);
        return this;
    }

    public WidgetCommentPopup Activity(Activity activity){
        this.setActivity(activity);
        this.comment_box.Activity(activity).Main(this.feed_id,user.getId());
        return this;
    }

    @Override
    protected void setActivity(Activity activity) {
        super.setActivity(activity);
    }

    @Override
    public void onClick(View view) {
        if (view==btnDone){
            this.dialog.cancel();
        }
    }

    public WidgetCommentPopup setFeed_id(int feed_id) {
        this.feed_id = feed_id;
        this.getNewComment();
        return this;
    }
}

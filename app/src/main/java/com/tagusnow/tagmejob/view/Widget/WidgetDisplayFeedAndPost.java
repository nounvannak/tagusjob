package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.adapter.SeeSingleTopJobsAdapter;
import com.tagusnow.tagmejob.auth.SmUser;

public class WidgetDisplayFeedAndPost extends RelativeLayout implements View.OnClickListener {

    private Activity activity;
    private Context context;
    private SeeSingleTopJobsAdapter adapter;
    private ImageButton viewBig,viewSmall;
    private TextView counter;
    private SmUser user;
    private int count = 0;

    public WidgetDisplayFeedAndPost(@NonNull Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.widget_display_feed_and_post,this);
        viewBig = (ImageButton)findViewById(R.id.viewBig);
        viewSmall = (ImageButton)findViewById(R.id.viewSmall);
        counter = (TextView)findViewById(R.id.counter);
        counter.setText(String.valueOf(this.count));
        viewBig.setOnClickListener(this);
        viewSmall.setOnClickListener(this);
    }

    public WidgetDisplayFeedAndPost Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public WidgetDisplayFeedAndPost Auth(SmUser user){
        this.user = user;
        return this;
    }

    public WidgetDisplayFeedAndPost Adapter(RecyclerView.Adapter adapter){
        this.adapter = (SeeSingleTopJobsAdapter)adapter;
        return this;
    }

    public void Counter(int count){
        this.count = count;
        counter.setText(String.valueOf(count));
    }

    @Override
    public void onClick(View view) {
        if (view==viewBig){
            this.adapter.setDisplay_type(SeeSingleTopJobsAdapter.BIG);
            this.adapter.notifyDataSetChanged();
        }else if(view==viewSmall){
            this.adapter.setDisplay_type(SeeSingleTopJobsAdapter.SMALL);
            this.adapter.notifyDataSetChanged();
        }
    }
}

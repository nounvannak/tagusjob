package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.tagusnow.tagmejob.App;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.CommentService;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.REST.Service.SocketService;
import com.tagusnow.tagmejob.REST.Service.SocketServiceGenerator;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.feed.FeedDetail;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WidgetFeedFooter extends TagUsJobRelativeLayout implements View.OnClickListener {

    private static final String TAG = WidgetFeedFooter.class.getSimpleName();
    private Context context;
    private LinearLayout mFeedLikeButton,mFeedCommentButton,shareButton;
    private ImageView mLikeIcon,mCommentIcon;
    private TextView mLikeText,mLikeCounter,mCommentText,mCommentCounter;
    private RelativeLayout like_block,layout_info;
    private Feed feed;
    private int feed_id;
    private SmUser authUser;
    private WidgetCommentPopup widgetCommentPopup;
    /*Service*/
    private CommentService service = ServiceGenerator.createService(CommentService.class);
    private SocketService socketService = SocketServiceGenerator.createService(SocketService.class);
    private JobsService jobsService = ServiceGenerator.createService(JobsService.class);
    /*Socket.io*/
    private Socket socket;

    private void initSocket(){
        socket = new App().getSocket();
        socket.on("Count Like "+feed.getId(),mCountLike);
        socket.on("Count Comment "+feed.getId(),mCountComment);
        socket.connect();
    }

    private Emitter.Listener mCountLike = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setIs_like(obj.getInt("is_like") == 1);
                                feed.getFeed().setCount_likes(obj.getInt("count_likes"));
                                mLikeCounter.setText(feed.getFeed().getLike_result());
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountLike");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener mCountComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject obj = (JSONObject) args[0];
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.w(TAG,obj.toString());
                            if (obj.getInt("feed_id")==feed.getId()){
                                feed.getFeed().setCount_comments(obj.getInt("count_comments"));
                                mCommentCounter.setText(activity.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                            }
                        }catch (Exception e){
                            SuperUtil.errorTrancking(TAG+"::mCountComment");
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Callback<Feed> mJobCallback = new Callback<Feed>() {
        @Override
        public void onResponse(Call<Feed> call, Response<Feed> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                Feed(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Feed> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void callJob(){
        int user = this.authUser!=null ? this.authUser.getId() : 0;
        jobsService.callJob(user,this.feed_id).enqueue(mJobCallback);
    }

    public WidgetFeedFooter(Context context) {
        super(context);
        this.init(context);
        this.initSocket();
    }

    public WidgetFeedFooter(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public WidgetFeedFooter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    public WidgetFeedFooter(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context,attrs,defStyleAttr);
        this.init(context);
    }

    private void init(Context context){
        inflate(context, R.layout.widget_feed_footer,this);
        this.context = context;

        //*Like Button*//
        mFeedLikeButton = (LinearLayout) findViewById(R.id.mFeedLikeButton);
        mLikeIcon = (ImageView) findViewById(R.id.likeIcon);
        mLikeText = (TextView) findViewById(R.id.likeText);
        mLikeCounter = (TextView) findViewById(R.id.likeCounter);
        //*Like Button*//
        mFeedCommentButton = (LinearLayout) findViewById(R.id.mFeedCommentButton);
        mCommentIcon = (ImageView) findViewById(R.id.commentIcon);
        mCommentText = (TextView) findViewById(R.id.commentText);
        mCommentCounter = (TextView) findViewById(R.id.commentCounter);
        shareButton = (LinearLayout) findViewById(R.id.shareButton);

        like_block = (RelativeLayout)findViewById(R.id.like_block);
        layout_info = (RelativeLayout)findViewById(R.id.layout_info);

        shareButton.setOnClickListener(this);
        mFeedCommentButton.setOnClickListener(this);
        mFeedLikeButton.setOnClickListener(this);
        like_block.setOnClickListener(this);
    }

    public WidgetFeedFooter User(SmUser authUser){
        this.authUser = authUser;
        return this;
    }

    public WidgetFeedFooter Activity(Activity activity){
        this.setActivity(activity);
        return this;
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public WidgetFeedFooter Feed(Feed feed){
        this.feed = feed;
        this.init(this.feed);
        return this;
    }

    public WidgetFeedFooter Feed(int feed_id){
        this.feed_id = feed_id;
        this.callJob();
        return this;
    }

    private void init(Feed feed){
        layout_info.setVisibility(VISIBLE);
        if (feed!=null){
            if (feed.getType()!=null && feed.getType().equals(Feed.TAGUSJOB)){
                if (feed.getFeed()!=null){
                    /*Normal Post*/
                    if (feed.getFeed().getType().equals(FeedDetail.NORMAL_POST)){
                        Log.w(TAG,"Normal Post");

                        if (this.feed.getFeed().getCount_likes() > 0 || this.feed.getFeed().getCount_comments() > 0){
                            layout_info.setVisibility(VISIBLE);
                        }else {
                            layout_info.setVisibility(GONE);
                        }

                        /*Count Comment*/
                        if (feed.getFeed().getCount_comments() > 0){
                            mCommentCounter.setVisibility(VISIBLE);
                            mCommentCounter.setText(this.activity.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                        }else {
                            mCommentCounter.setVisibility(GONE);
                        }

                        /*Count Like*/
                        if (feed.getFeed().getCount_likes() > 0){
                            like_block.setVisibility(VISIBLE);
                            mLikeCounter.setText(this.feed.getFeed().getLike_result());
                        }else {
                            like_block.setVisibility(GONE);
                        }

                        /*Is Auth User Liked*/
                        if (feed.getFeed().isIs_like()){
                            Glide.with(this.context).load(R.drawable.ic_action_like_blue).into(mLikeIcon);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mLikeText.setTextColor(this.activity.getColor(R.color.colorAccent));
                            }
                        }else {
                            Glide.with(this.context).load(R.drawable.ic_action_like_white).into(mLikeIcon);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mLikeText.setTextColor(this.activity.getColor(R.color.white));
                            }
                        }
                    }
                    /*Job Post*/
                    else if (feed.getFeed().getType().equals(FeedDetail.JOB_POST)){
                        Log.w(TAG,"Job Post");

                        if (this.feed.getFeed().getCount_likes() > 0 || this.feed.getFeed().getCount_comments() > 0){
                            layout_info.setVisibility(VISIBLE);
                        }else {
                            layout_info.setVisibility(GONE);
                        }

                        /*Count Comment*/
                        if (feed.getFeed().getCount_comments() > 0){
                            mCommentCounter.setVisibility(VISIBLE);
                            mCommentCounter.setText(this.activity.getString(R.string._d_comments,feed.getFeed().getCount_comments()));
                        }else {
                            mCommentCounter.setVisibility(GONE);
                        }

                        /*Count Like*/
                        if (feed.getFeed().getCount_likes() > 0){
                            like_block.setVisibility(VISIBLE);
                            mLikeCounter.setText(this.feed.getFeed().getLike_result());
                        }else {
                            like_block.setVisibility(GONE);
                        }

                        /*Is Auth User Liked*/
                        if (feed.getFeed().isIs_like()){
                            Glide.with(this.context).load(R.drawable.ic_action_like_blue).into(mLikeIcon);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mLikeText.setTextColor(this.activity.getColor(R.color.colorAccent));
                            }
                        }else {
                            Glide.with(this.context).load(R.drawable.ic_action_like_white).into(mLikeIcon);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mLikeText.setTextColor(this.activity.getColor(R.color.white));
                            }
                        }
                    }else {
                        Log.w(TAG,"Other Type");
                    }
                }else {
                    Log.e(TAG,"No FeedDetail");
                }
            }else {
                Log.e(TAG,String.valueOf(feed.getId())+ "is Facebook");
            }
        }else {
            Log.e(TAG,"No Feed");
        }
    }

    private void shareContent(){
        originalShare();
    }

    private void originalShare(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,this.feed.getDetail());
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    private Runnable mCommentRun = new Runnable() {
        @Override
        public void run() {
            widgetCommentPopup = new WidgetCommentPopup(context).Auth(authUser).Activity(activity).setFeed_id(feed_id);
            new AlertDialog.Builder(context).setView(widgetCommentPopup).setCancelable(true).create().show();
        }
    };

    private void comment(){
        this.activity.runOnUiThread(mCommentRun);
    }

    public void ADD_PHOTO(File file){
        if (this.widgetCommentPopup!=null){
            this.widgetCommentPopup.ADD_PHOTO(file);
        }else {
            Log.e(TAG,"WidgetCommentPopup is null");
        }
    }

    private void like(){}

    @Override
    public void onClick(View view) {
        if (view==shareButton){
            this.shareContent();
        }else if (view==mFeedCommentButton){
            this.comment();
        }else if (view==mFeedLikeButton){
            this.like();
        }else if (view==like_block){
            this.comment();
        }
    }
}

package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.UserPostImageService;
import com.tagusnow.tagmejob.ShowAllUserImagePostActivity;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.ViewImageActivity;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.model.v2.feed.Photo;
import com.tagusnow.tagmejob.model.v2.feed.PhotoPaginate;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WidgetImagePost extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = WidgetImagePost.class.getSimpleName();
    private Activity activity;
    private Context context;
    private SmUser user;
    private ImageView image_1,image_2,image_3,image_4,image_5;
    private ImageButton btnSeeMore;
    private LinearLayout block_a,block_b;
    private List<Photo> photos;
    private PhotoPaginate photoPaginate;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private UserPostImageService service = retrofit.create(UserPostImageService.class);
    private Callback<PhotoPaginate> mFirstFiveImageCallback = new Callback<PhotoPaginate>() {
        @Override
        public void onResponse(Call<PhotoPaginate> call, Response<PhotoPaginate> response) {
            if (response.isSuccessful()){
                photoPaginate = response.body();
                initUserImage(response.body().getData());
            }else {
                try {
                    Log.w(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<PhotoPaginate> call, Throwable t) {
            t.printStackTrace();
        }
    };
    private String[] strPhoto;

    private void initPhoto(List<Photo> photos){
        if (photos!=null){
            if (this.photos.size() > 0){
                this.strPhoto = new String[this.photos.size()];
                for (int i=0;i < this.photos.size();i++){
                    if (this.photos.get(i).getImage()!=null){
                        if (this.photos.get(i).getImage().contains("http")){
                            this.strPhoto[i] = photos.get(i).getImage();
                        }else {
                            this.strPhoto[i] = SuperUtil.getAlbumPicture(photos.get(i).getImage(),"original");
                        }
                    }
                }
            }
        }
    }

    private void initUserImage(List<Photo> photos){
        this.photos = photos;
        this.initPhoto(photos);
        this.block_a.setVisibility(VISIBLE);
        this.block_b.setVisibility(VISIBLE);
        this.image_1.setVisibility(VISIBLE);
        this.image_2.setVisibility(VISIBLE);
        this.image_3.setVisibility(VISIBLE);
        this.image_4.setVisibility(VISIBLE);
        this.image_5.setVisibility(VISIBLE);
        if (this.photos!=null){
            if (this.photos.size()==1){
                if (this.photos.get(0).getImage()!=null){
                    if (this.photos.get(0).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(0).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(0).getImage(),"300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }
                    this.image_2.setVisibility(GONE);
                    this.block_b.setVisibility(GONE);
                }else {
                    Toast.makeText(this.context,"No Photo 1",Toast.LENGTH_SHORT).show();
                }
            }else if (this.photos.size()==2){
                if (this.photos.get(0).getImage()!=null){
                    if (this.photos.get(0).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(0).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(0).getImage(),"300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 1",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(1).getImage()!=null){
                    if (this.photos.get(1).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(1).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_2);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(1).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_2);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 2",Toast.LENGTH_SHORT).show();
                }
                this.block_b.setVisibility(GONE);
            }else if (this.photos.size()==3){
                if (this.photos.get(0).getImage()!=null){
                    if (this.photos.get(0).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(0).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(0).getImage(),"300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 1",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(1).getImage()!=null){
                    if (this.photos.get(1).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(1).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_2);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(1).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_2);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 2",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(2).getImage()!=null){
                    if (this.photos.get(2).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(2).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_3);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(2).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_3);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 3",Toast.LENGTH_SHORT).show();
                }
                this.image_4.setVisibility(GONE);
                this.image_5.setVisibility(GONE);
            }else if (this.photos.size()==4){
                if (this.photos.get(0).getImage()!=null){
                    if (this.photos.get(0).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(0).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(0).getImage(),"300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 1",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(1).getImage()!=null){
                    if (this.photos.get(1).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(1).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_2);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(1).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_2);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 2",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(2).getImage()!=null){
                    if (this.photos.get(2).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(2).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_3);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(2).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_3);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 3",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(3).getImage()!=null){
                    if (this.photos.get(3).getImage().contains("http")){
                        Glide.with(this.context).load(this.photos.get(3).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_4);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(3).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_4);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 4",Toast.LENGTH_SHORT).show();
                }
                this.image_5.setVisibility(GONE);
            }else if(this.photos.size()==5){
                if (this.photos.get(0).getImage()!=null){
                    if (this.photos.get(0).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(0).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(0).getImage(),"300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_1);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 1",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(1).getImage()!=null){
                    if (this.photos.get(1).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(1).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_2);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(1).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_2);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 2",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(2).getImage()!=null){
                    if (this.photos.get(2).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(2).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_3);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(2).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_3);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 3",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(3).getImage()!=null){
                    if (this.photos.get(3).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(3).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_4);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(3).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_4);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 4",Toast.LENGTH_SHORT).show();
                }
                if (this.photos.get(4).getImage()!=null){
                    if (this.photos.get(4).getImage().contains("http")){
                        Glide.with(this.context)
                                .load(this.photos.get(4).getImage())
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_5);
                    }else {
                        Glide.with(this.context)
                                .load(SuperUtil.getAlbumPicture(this.photos.get(4).getImage(), "300"))
                                .centerCrop()
                                .error(R.drawable.ic_file_picture_icon)
                                .into(image_5);
                    }
                }else {
                    Toast.makeText(this.context,"No Photo 4",Toast.LENGTH_SHORT).show();
                }
            }else {
                this.getLayoutParams().height = 0;
                this.getLayoutParams().width = 0;
                this.setVisibility(GONE);
//                Toast.makeText(this.context,"No Photos",Toast.LENGTH_SHORT).show();
            }
        }else {
            this.getLayoutParams().height = 0;
            this.getLayoutParams().width = 0;
            this.setVisibility(GONE);
//            Toast.makeText(this.context,"No Photos",Toast.LENGTH_SHORT).show();
        }
    }

    public WidgetImagePost(Context context) {
        super(context);
        inflate(context, R.layout.widget_image_post,this);
        this.context = context;
        image_1 = (ImageView)findViewById(R.id.image_1);
        image_2 = (ImageView)findViewById(R.id.image_2);
        image_3 = (ImageView)findViewById(R.id.image_3);
        image_4 = (ImageView)findViewById(R.id.image_4);
        image_5 = (ImageView)findViewById(R.id.image_5);
        btnSeeMore = (ImageButton)findViewById(R.id.btnSeeMore);
        block_a = (LinearLayout)findViewById(R.id.block_a);
        block_b = (LinearLayout)findViewById(R.id.block_b);

        /*Set Event Click*/
        btnSeeMore.setOnClickListener(this);
        image_1.setOnClickListener(this);
        image_2.setOnClickListener(this);
        image_3.setOnClickListener(this);
        image_4.setOnClickListener(this);
        image_5.setOnClickListener(this);

    }

    private void getUserImagePost(){
        int user_id = this.user!=null ? this.user.getId() : 0;
        int limit = 5;
        service.getUserImagePost(user_id,limit).enqueue(mFirstFiveImageCallback);
    }

    public WidgetImagePost User(SmUser user){
        this.user = user;
        this.getUserImagePost();
        return this;
    }

    public WidgetImagePost Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    private void SEE_MORE(){
        if (this.activity!=null){
            this.activity.startActivity(ShowAllUserImagePostActivity.createIntent(this.activity).putExtra(ShowAllUserImagePostActivity.USER_S,this.user.getId()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewImage(int position){
        this.activity.startActivity(ViewImageActivity.createIntent(this.activity)
                .putExtra(ViewImageActivity.POSITION,position)
                .putExtra(ViewImageActivity.PHOTOS,this.strPhoto)
                .putExtra(ViewImageActivity.PARCEL,this.photoPaginate)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onClick(View view) {
        if (view==btnSeeMore){
            SEE_MORE();
        }else if (view==image_1){
            viewImage(0);
        }else if (view==image_2){
            viewImage(1);
        }else if (view==image_3){
            viewImage(2);
        }else if (view==image_4){
            viewImage(3);
        }else if (view==image_5){
            viewImage(4);
        }
    }
}

package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.widget.RelativeLayout;

import com.tagusnow.tagmejob.R;

public class WidgetImageView extends RelativeLayout{

    private Context context;
    private Activity activity;
    private ViewPager pager;

    public WidgetImageView(Context context) {
        super(context);
        inflate(context, R.layout.widget_image_view,this);
        this.context = context;
        this.pager = (ViewPager)findViewById(R.id.pager);
    }
}

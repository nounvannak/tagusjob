package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tagusnow.tagmejob.ExpiredJobsActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.JobsService;
import com.tagusnow.tagmejob.SavedJobsActivity;
import com.tagusnow.tagmejob.SuperConstance;
import com.tagusnow.tagmejob.YourJobsActivity;
import com.tagusnow.tagmejob.auth.SmUser;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WidgetJobsCounter extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = WidgetJobsCounter.class.getSimpleName();
    private Activity activity;
    private Context context;
    private SmUser user;
    private RelativeLayout btnYourPostJobs,btnSavedJobs,btnExpiredJobs;
    private TextView counter_your_jobs,counter_saved_jobs,counter_expired_jobs;
    /*Service*/
    private Gson gson = new GsonBuilder().setLenient().create();
    private OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100,TimeUnit.SECONDS).build();
    private Retrofit retrofit = new Retrofit.Builder().baseUrl(SuperConstance.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
    private JobsService service = retrofit.create(JobsService.class);
    private Callback<SmUser> mCallback = new Callback<SmUser>() {
        @Override
        public void onResponse(Call<SmUser> call, Response<SmUser> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initJobsDashboard(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmUser> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            counter_your_jobs.setText(String.valueOf(user.getCounter_post_jobs()));
            counter_saved_jobs.setText(String.valueOf(user.getCounter_saved_jobs()));
            counter_expired_jobs.setText(String.valueOf(user.getCounter_expired_jobs()));
        }
    };

    public WidgetJobsCounter(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.widget_jobs_counter,this);
        btnYourPostJobs = (RelativeLayout)findViewById(R.id.btnYourJobs);
        btnSavedJobs = (RelativeLayout)findViewById(R.id.btnSavedJobs);
        btnExpiredJobs = (RelativeLayout)findViewById(R.id.btnExpiredJobs);
        counter_your_jobs = (TextView) findViewById(R.id.counter_your_jobs);
        counter_saved_jobs = (TextView)findViewById(R.id.counter_saved_jobs);
        counter_expired_jobs = (TextView)findViewById(R.id.counter_expired_jobs);
        btnYourPostJobs.setOnClickListener(this);
        btnSavedJobs.setOnClickListener(this);
        btnExpiredJobs.setOnClickListener(this);
    }

    private void jobsDashboard(){
        int user = this.user!=null ? this.user.getId() : 0;
        service.jobsDashboard(user).enqueue(mCallback);
    }

    private void initJobsDashboard(SmUser user){
        this.user = user;
        if (user!=null){
            this.activity.runOnUiThread(mRunnable);
        }else {
            Toast.makeText(this.context,"No User",Toast.LENGTH_SHORT).show();
        }
    }

    public WidgetJobsCounter Auth(SmUser user){
        this.user = user;
        this.jobsDashboard();
        return this;
    }

    public WidgetJobsCounter Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    private void showYourPostJobs(){
        if (this.activity!=null){
            this.activity.startActivity(YourJobsActivity.createIntent(this.activity).putExtra(YourJobsActivity.COUNTER,this.user.getCounter_post_jobs()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }


    private void showSavedJobs(){
        if (this.activity!=null){
            this.activity.startActivity(SavedJobsActivity.createIntent(this.activity).putExtra(SavedJobsActivity.COUNTER,this.user.getCounter_saved_jobs()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void showExpiredJobs(){
        if (this.activity!=null){
            this.activity.startActivity(ExpiredJobsActivity.createIntent(this.activity).putExtra(ExpiredJobsActivity.COUNTER,this.user.getCounter_expired_jobs()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnYourPostJobs){
            showYourPostJobs();
        }else if(view==btnSavedJobs){
            showSavedJobs();
        }else if(view==btnExpiredJobs){
            showExpiredJobs();
        }
    }
}

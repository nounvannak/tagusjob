package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tagusnow.tagmejob.AllPostActivity;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.auth.Auth;
import com.tagusnow.tagmejob.auth.SmUser;

public class WidgetPost extends RelativeLayout implements View.OnClickListener {

    private Activity activity;
    private Context context;
    private RelativeLayout btnArticle,btnJobs,btnResume;
    private TextView btnPost;
    private SmUser user;

    public WidgetPost(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.widget_post,this);
        btnArticle = (RelativeLayout)findViewById(R.id.btnArticle);
        btnJobs = (RelativeLayout)findViewById(R.id.btnPostJobs);
        btnResume = (RelativeLayout)findViewById(R.id.btnPostCV);
        btnPost = (TextView)findViewById(R.id.btnPost);
        btnArticle.setOnClickListener(this);
        btnJobs.setOnClickListener(this);
        btnResume.setOnClickListener(this);
        btnPost.setOnClickListener(this);
        this.setOnClickListener(this);
        this.user = new Auth(context).checkAuth().token();
        if (this.user!=null){
            if (this.user.getUser_type()==SmUser.FIND_JOB){
                btnJobs.setVisibility(GONE);
                btnResume.setVisibility(VISIBLE);
            }else {
                btnJobs.setVisibility(VISIBLE);
                btnResume.setVisibility(GONE);
            }
        }
    }

    public WidgetPost Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    private void onCreatePost(){
        if (this.activity!=null){
            this.activity.startActivity(CreatePostActivity.createIntent(this.activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void postJobs(){
        if (this.activity!=null){
            this.activity.startActivity(AllPostActivity.createIntent(this.activity).putExtra(AllPostActivity.FRAGMENT,AllPostActivity.JOBS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void postResume(){
        if (this.activity!=null){
            this.activity.startActivity(AllPostActivity.createIntent(this.activity).putExtra(AllPostActivity.FRAGMENT,AllPostActivity.RESUME).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnArticle){
            onCreatePost();
        }else if(view==btnJobs){
            postJobs();
        }else if (view==btnResume){
            postResume();
        }else if (view==btnPost){
            onCreatePost();
        }else if (view==this){
            onCreatePost();
        }
    }
}

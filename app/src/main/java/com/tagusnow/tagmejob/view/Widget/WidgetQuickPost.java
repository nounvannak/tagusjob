package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.adapter.WidgetQuickPostPagerAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Feed;
import com.tagusnow.tagmejob.view.TagUsJobActivity;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class WidgetQuickPost extends TagUsJobRelativeLayout implements View.OnClickListener, TabLayout.OnTabSelectedListener {

    private static final String TAG = WidgetQuickPost.class.getSimpleName();
    public static final int NORMAL_POST = 0;
    public static final int JOBS_POST = 1;
    public static final int POST_CV = 2;
    private Context context;
    private SmUser user;
    private Feed feed;
    private TabLayout tab_layout;
    private ViewPager pager;
    private Button btnPost;
    private RelativeLayout optionPrivacy;
    private ImageView optPrivacyIcon;
    private TextView optPrivacyTitle;
    private WidgetQuickPostPagerAdapter pagerAdapter;

    public WidgetQuickPost(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.widget_quick_post,this);
        tab_layout = (TabLayout)findViewById(R.id.tab_layout);
        tab_layout.addTab(tab_layout.newTab().setIcon(R.drawable.ic_mode_edit_light_blue_24dp),NORMAL_POST);
        tab_layout.addTab(tab_layout.newTab().setIcon(R.drawable.ic_work_light_blue_24dp),JOBS_POST);
        tab_layout.addTab(tab_layout.newTab().setIcon(R.drawable.ic_border_color_light_blue_24dp),POST_CV);
        tab_layout.setTabGravity(TabLayout.GRAVITY_FILL);
        pager = (ViewPager)findViewById(R.id.pager);
        btnPost = (Button)findViewById(R.id.btnPost);
        optionPrivacy = (RelativeLayout)findViewById(R.id.optionPrivacy);
        optPrivacyIcon = (ImageView)findViewById(R.id.optPrivacyIcon);
        optPrivacyTitle = (TextView)findViewById(R.id.optPrivacyTitle);
        btnPost.setOnClickListener(this);
        optionPrivacy.setOnClickListener(this);
    }

    public WidgetQuickPost Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public WidgetQuickPost Auth(SmUser user){
        this.user = user;
        return this;
    }

    @Override
    public void onClick(View view) {
        if (view==btnPost){

        }else if (view==optionPrivacy){

        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
        pagerAdapter = new WidgetQuickPostPagerAdapter(((TagUsJobActivity)this.activity).getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_layout));
        tab_layout.addOnTabSelectedListener(this);
        pager.setCurrentItem(NORMAL_POST);
    }
}

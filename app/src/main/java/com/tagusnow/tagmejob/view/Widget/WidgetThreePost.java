package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.tagusnow.tagmejob.AllPostActivity;
import com.tagusnow.tagmejob.CreatePostActivity;
import com.tagusnow.tagmejob.ProfileActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import de.hdodenhof.circleimageview.CircleImageView;

public class WidgetThreePost extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = WidgetThreePost.class.getSimpleName();
    private Activity activity;
    private Context context;
    private SmUser user;
    private CircleImageView user_profile;
    private ImageButton btnPost,btnPostJobs,btnPostCV;

    public WidgetThreePost(Context context) {
        super(context);
        inflate(context, R.layout.widget_three_post,this);
        this.context = context;
        user_profile = (CircleImageView)findViewById(R.id.user_profile);
        btnPost = (ImageButton)findViewById(R.id.btnPost);
        btnPostJobs = (ImageButton)findViewById(R.id.btnPostJobs);
        btnPostCV = (ImageButton)findViewById(R.id.btnPostCV);
        user_profile.setOnClickListener(this);
        btnPost.setOnClickListener(this);
        btnPostJobs.setOnClickListener(this);
        btnPostCV.setOnClickListener(this);
    }

    public WidgetThreePost Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public WidgetThreePost Auth(SmUser user){
        this.user = user;
        if (this.user!=null){

            if (user.getUser_type()==SmUser.FIND_JOB){
                btnPostJobs.setVisibility(GONE);
                btnPostCV.setVisibility(VISIBLE);
            }else {
                btnPostJobs.setVisibility(VISIBLE);
                btnPostCV.setVisibility(GONE);
            }

            if (this.user.getImage()!=null){
                if (this.user.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(this.user.getImage())
                            .fitCenter()
                            .into(user_profile);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getProfilePicture(this.user.getImage()))
                            .fitCenter()
                            .into(user_profile);
                }
            }
        }
        return this;
    }

    private void onCreatePost(){
        if (this.activity!=null){
            this.activity.startActivity(CreatePostActivity.createIntent(this.activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void postJobs(){
        if (this.activity!=null){
            this.activity.startActivity(AllPostActivity.createIntent(this.activity).putExtra(AllPostActivity.FRAGMENT,AllPostActivity.JOBS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void postResume(){
        if (this.activity!=null){
            this.activity.startActivity(AllPostActivity.createIntent(this.activity).putExtra(AllPostActivity.FRAGMENT,AllPostActivity.RESUME).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewProfile(){
        if (this.activity!=null){
            this.activity.startActivity(ProfileActivity.createIntent(this.activity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==user_profile){
            viewProfile();
        }else if (view==btnPost){
            onCreatePost();
        }else if (view==btnPostJobs){
            postJobs();
        }else if (view==btnPostCV){
            postResume();
        }
    }
}

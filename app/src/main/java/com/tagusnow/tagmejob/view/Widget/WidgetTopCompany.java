package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.adapter.WidgetTopCompanyAdapter;
import com.tagusnow.tagmejob.v3.page.resume.CompanyFaceListener;
import com.tagusnow.tagmejob.view.TagUsJobRelativeLayout;

public class WidgetTopCompany extends TagUsJobRelativeLayout implements View.OnClickListener {

    private Context context;
    private TextView title;
    private Button btnEdit;
    private RecyclerView company_list;
    private CompanyFaceListener companyFaceListener;
    private WidgetTopCompanyAdapter adapter;

    public void setCompanyFaceListener(CompanyFaceListener companyFaceListener) {
        this.companyFaceListener = companyFaceListener;
    }

    public void setAdapter(WidgetTopCompanyAdapter adapter) {
        this.adapter = adapter;
        this.company_list.setAdapter(this.adapter);
    }

    private void initUI(Context context){
        inflate(context, R.layout.widget_top_company,this);
        this.context = context;
        title = (TextView)findViewById(R.id.title);
        btnEdit = (Button)findViewById(R.id.btnEdit);
        company_list = (RecyclerView)findViewById(R.id.company_list);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        company_list.setHasFixedSize(true);
        company_list.setLayoutManager(manager);
        btnEdit.setOnClickListener(this);
    }

    public WidgetTopCompany(Context context) {
        super(context);
        this.initUI(context);
    }

    public WidgetTopCompany(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initUI(context);
    }

    public WidgetTopCompany(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initUI(context);
    }

    public WidgetTopCompany(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.initUI(context);
    }

    @Override
    public void onClick(View view) {
        if (view==btnEdit){
            companyFaceListener.viewMore(this);
        }
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setButtonTitle(String title){
        this.btnEdit.setText(title);
    }
    @Override
    public void setActivity(Activity activity) {
        super.setActivity(activity);
    }

}

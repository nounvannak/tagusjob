package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tagusnow.tagmejob.JobListActivity;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.REST.Service.RepositoryService;
import com.tagusnow.tagmejob.REST.Service.ServiceGenerator;
import com.tagusnow.tagmejob.adapter.WidgetTopJobsAdapter;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.SmTagSubCategory;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WidgetTopJobs extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = WidgetTopJobs.class.getSimpleName();
    private Activity activity;
    private Context context;
    private RecyclerView recyclerView;
    private Button btnSeeMore;
    private SmTagSubCategory category;
    private WidgetTopJobsAdapter adapter;
    private SmUser auth;
    /*Service*/
    private RepositoryService service = ServiceGenerator.createService(RepositoryService.class);
    private Callback<SmTagSubCategory> mCallback = new Callback<SmTagSubCategory>() {
        @Override
        public void onResponse(Call<SmTagSubCategory> call, Response<SmTagSubCategory> response) {
            if (response.isSuccessful()){
                Log.w(TAG,response.message());
                initAdapter(response.body());
            }else {
                try {
                    Log.e(TAG,response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SmTagSubCategory> call, Throwable t) {
            t.printStackTrace();
        }
    };

    private void initAdapter(SmTagSubCategory category){
        this.category = category;
        if (category!=null){
            this.adapter = new WidgetTopJobsAdapter(this.activity,category,this.auth);
            recyclerView.setAdapter(adapter);
        }else {
            Toast.makeText(this.context,"No Category",Toast.LENGTH_SHORT).show();
        }
    }

    private void callCategory(){
        service.callCategory().enqueue(mCallback);
    }

    public WidgetTopJobs(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.widget_top_jobs,this);
        btnSeeMore = (Button)findViewById(R.id.btnSeeMore);
        recyclerView = (RecyclerView)findViewById(R.id.topRecycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        btnSeeMore.setOnClickListener(this);
        callCategory();
    }

    public WidgetTopJobs Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public WidgetTopJobs Auth(SmUser auth){
        this.auth = auth;
        return this;
    }

    @Override
    public void onClick(View view) {
        if (view==btnSeeMore){
            this.activity.startActivity(JobListActivity.createIntent(this.activity).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}

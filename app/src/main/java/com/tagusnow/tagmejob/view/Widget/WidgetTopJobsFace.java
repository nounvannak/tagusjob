package com.tagusnow.tagmejob.view.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tagusnow.tagmejob.R;
import com.tagusnow.tagmejob.SeeSingleTopJobsActivity;
import com.tagusnow.tagmejob.SuperUtil;
import com.tagusnow.tagmejob.auth.SmUser;
import com.tagusnow.tagmejob.feed.Category;
import de.hdodenhof.circleimageview.CircleImageView;

public class WidgetTopJobsFace extends RelativeLayout implements View.OnClickListener {

    private Activity activity;
    private Context context;
    private CircleImageView job_icon;
    private TextView job_counter,job_title;
    private SmUser auth;
    private Category category;

    public WidgetTopJobsFace(Context context) {
        super(context);
        this.context = context;
        inflate(context, R.layout.widget_top_jobs_face,this);
        job_icon = (CircleImageView)findViewById(R.id.job_icon);
        job_counter = (TextView)findViewById(R.id.job_counter);
        job_title = (TextView)findViewById(R.id.job_title);
        this.setOnClickListener(this);
    }

    public WidgetTopJobsFace Activity(Activity activity){
        this.activity = activity;
        return this;
    }

    public WidgetTopJobsFace Category(Category category){
        this.category = category;
        if (category!=null){
            if (category.getImage()!=null){
                if (category.getImage().contains("http")){
                    Glide.with(this.context)
                            .load(category.getImage())
                            .centerCrop()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(job_icon);
                }else {
                    Glide.with(this.context)
                            .load(SuperUtil.getCategoryPath(category.getImage()))
                            .centerCrop()
                            .error(R.mipmap.ic_launcher_circle)
                            .into(job_icon);
                }
            }else {
                Glide.with(this.context)
                        .load(R.mipmap.ic_launcher_circle)
                        .centerCrop()
                        .into(job_icon);
            }
            job_counter.setText(String.valueOf(category.getJob_counter()));
            String title = category.getTitle();
            if (title.length() <= 5){
                title = "#"+title.toLowerCase();
            }else{
                title = "#"+title.substring(0,3).toLowerCase();
            }
            job_title.setText(title);
        }else {
            Toast.makeText(this.context,"No Category",Toast.LENGTH_SHORT).show();
        }
        return this;
    }

    public WidgetTopJobsFace Auth(SmUser auth){
        this.auth = auth;
        return this;
    }

    private void showSingleTopJobs(){
        if (this.activity!=null){
            this.activity.startActivity(SeeSingleTopJobsActivity.createIntent(this.activity).putExtra(SeeSingleTopJobsActivity.CATEGORY,new Gson().toJson(this.category)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            Toast.makeText(this.context,"No Activity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==this){
            showSingleTopJobs();
        }
    }
}
